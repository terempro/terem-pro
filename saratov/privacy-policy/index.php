<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Privacy Policy");
?><section class="content text-page white" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side + my-margin clearfix">
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<div class="my-text-title text-center text-uppercase desgin-h1">
								<h1>
									ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ И ПРАВИЛА ПОЛЬЗОВАНИЯ САЙТОМ
								</h1>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p><strong>Общие положения</strong></p>
							<p>Администратором интернет-сайта <a href="http://www.terem-pro.ru/">www.terem-pro.ru</a> (далее – Сайт) является ООО «Теремъ» (ИНН

5040095336, ОГРН 1095040006632), далее – Администратор Сайта.</p>
<p>Все сведения, содержащиеся на Сайте, носят исключительно информационный характер. Информация,

представленная на Сайте, не является исчерпывающей. Для получения более полной и подробной

информации Пользователь может посетить выставку деревянных домов в Кузьминках, расположенную по

адресу: Россия, г. Москва, ул. Зеленодольская, вл. 42, или узнать по телефону +7 (495) 721-18-00.</p>
<p>Администратор Сайта оставляет за собой право без уведомления вносить в любое время изменения в

размещенную на Сайте информацию.</p>
<p>Пользователь Сайта - любой посетитель Сайта (далее – «Пользователь»).</p>
<p>Материалы Сайта предоставлены Администратором Сайта для Пользователей Сайта и могут быть

использованы только в личных целях, не связанных с предпринимательской и/либо иной оплачиваемой

деятельностью, с учётом сохранения авторских, исключительных прав и других отметок о принадлежности

таких прав законному владельцу. Любое изменение материалов Сайта и/либо их использование по иному

назначению является нарушением авторских прав и прав владельцев исключительных прав на результаты

интеллектуальной деятельности. Использование материалов Сайта на других, в том числе, Интернет-

ресурсах, а также в иных средствах информации запрещается без письменного разрешения Администратора

Сайта или непосредственного законного владельца прав на результаты интеллектуальной деятельности

(правообладателя). Используя любые материалы (информацию) Сайта, Пользователь автоматически

соглашается с настоящей политикой конфиденциальности и правилами пользования Сайтом (далее –

Правила).</p>
<p>Настоящие Правила вступают в силу с момента их опубликования на Сайте.</p>
<p>Администратор сайта вправе без предварительного уведомления вносить любые изменения и дополнения в

Правила. По всем вопросам, не нашедшим отражения в Правилах, Администратор сайта оставляет за собой

право публиковать дополнительную информацию на Сайте по необходимости.</p>
<p><strong>Запрет на использование товарных знаков, фирменного наименования, коммерческого обозначения</strong></p>

<p>Все права на фирменные наименования, коммерческие обозначения, товарные знаки, включая товарные знаки:
<ul>
<li><img src="/bitrix/templates/.default/assets/img/privacy-policy/Terem_L_R.png" alt="" vspace="10" height="50"></li>
<li><img src="/bitrix/templates/.default/assets/img/privacy-policy/Terem_Logo_R.png" alt="" vspace="10" height="50"></li>
<li><img src="/bitrix/templates/.default/assets/img/privacy-policy/TEREM_R.png" alt="" vspace="10" height="20"></li>
</ul>
<p>принадлежат на законных основаниях Администратору Сайта и/или иным правообладателям.

Любое использование товарного знака, коммерческого обозначения и/или фирменного наименования без

получения предварительного письменного согласия не допускается и является нарушением

соответствующих прав их правообладателей.</p>
<p>Незаконное использование товарного знака, коммерческого обозначения и/или фирменного наименования

влечёт за собой гражданскую, административную, уголовную ответственность в соответствии с

законодательством РФ.</p>
<p><strong>Запрет на использование объектов авторских и смежных прав</strong></p>
<p>Все права на тексты, музыку, звуки, видео, фотографии и иные материалы (далее – «Контент Сайта»),

размещаемые на Сайте (за исключением сообщений и материалов, размещаемых Пользователями), а также

на программную оболочку и дизайн Сайта, принадлежат Администратору Сайта, и охраняются в

соответствии с законодательством РФ.</p>
<p>Пользователь не может использовать Контент Сайта в каких-либо коммерческих целях. Пользователь также

не может удалять надписи об авторских правах или прочие надписи о принадлежности любых прав из

информации, размещённой на Сайте.</p>
<p>Администратор Сайта не предоставляет Пользователям каких-либо исключительных прав на использование

своих объектов интеллектуальной собственности. Незаконное и несанкционированное использование

любых материалов Сайта не допускается и влечёт нарушение законодательства об авторских и смежных

правах, а также является основанием наступления ответственности в соответствии с гражданским,

административным и уголовным законодательством РФ.</p>
<p><strong>Запреты и ограничения</strong></p>
<p>На Сайте запрещается:</p>
<ul>
	<li>
		<p>Накапливать или собирать адреса электронной почты или другую контактную информацию Пользователей

Сайта автоматизированными или иными способами для целей рассылки не запрошенной почты (спама) или

другой нежелательной информации;</p>
	</li>
	<li>
		<p>Использовать Сайт любым способом, который может помешать нормальному функционированию Сайта и

его сервисов;</p>
	</li>
	<li>
		<p>Использовать автоматизированные скрипты (программы) для сбора информации и (или) взаимодействия с

Сайтом и его сервисами;</p>
	</li>
	<li>
		<p>Загружать, публиковать, передавать, иным образом доводить до всеобщего сведения (далее — размещать)

любую информацию, которая содержит угрозу, дискредитирует или оскорбляет других Пользователей или

третьих лиц, является вульгарной, непристойной, носит мошеннический характер, посягает на личные или

публичные интересы, пропагандирует расовую, религиозную, этническую ненависть или вражду, любую

иную информацию, нарушающую охраняемые законом права человека и гражданина;</p>
	</li>
	<li>
		<p>Размещать любую коммерческую рекламу, коммерческие предложения, агитационные материалы,

распространять спам, сообщения-цепочки (сообщения, требующие их передачи одному или нескольким

Пользователям), схемы финансовых пирамид или призывы в них участвовать, любую другую навязчивую

информацию, кроме случаев, когда размещение такой информации было согласовано с Администратором

Сайта;</p>
	</li>
	<li>
		<p>Размещать на Сайте домашние адреса, номера телефонов, адреса электронной почты, паспортные данные и

прочую личную информацию других Пользователей или любых третьих лиц без их личного согласия на

такие действия;</p>
	</li>
	<li>
		<p>Размещать любые файлы, которые содержат или могут содержать вирусы и другие вредоносные

программы;</p>
	</li>
	<li>
		<p>Описывать или пропагандировать преступную деятельность, размещать инструкции или руководства по

совершению преступных действий;</p>
	</li>
	<li>
		<p>Любым способом, в том числе путём взлома, пытаться получить доступ к чужой учетной записи (аккаунту)

вопреки воле лица, которому она принадлежит;</p>
	</li>
	<li>
		<p>Размещать любую информацию, нарушающую права Пользователей или третьих лиц на объекты

интеллектуальной собственности;</p>
	</li>
	<li>
		<p>Размещать любую другую информацию, которая, по мнению Администратора Сайта является

нежелательной, не соответствует целям создания Сайта, ущемляет интересы Пользователей, или по другим

причинам является нежелательной для размещения на Сайте;</p>
	</li>
	<li>
		<p>Пользоваться Сайтом и его информационным наполнением в коммерческих целях.</p>
	</li>
	
</ul>
<p>Администратор Сайта оставляет за собой право по своему усмотрению изменять (модерировать) или

удалять любую публикуемую Пользователем информацию, нарушающую запреты, установленные в

настоящих Правилах (включая личные сообщения), приостанавливать, ограничивать или прекращать доступ

ко всем или к любому из разделов или сервисов Сайта в любое время по любой причине или без объяснения

причин, с предварительным уведомлением или без такового.</p>
<p>Администратор Сайта закрепляет за собой право удалить и (или) приостановить аккаунт Пользователя,

ограничить или прекратить доступ к любому из сервисов Сайта, если обнаружит, что по его мнению,

Пользователь представляет угрозу для Сайта и (или) его Пользователей.</p>
<p><strong>Ответственность Пользователей Сайта и Администратора Сайта</strong></p>
<p>Пользователь несёт личную ответственность за любой Контент или иную информацию, которые загружает

или иным образом доводит до всеобщего сведения (размещает) на Сайте или с его помощью. Пользователь

не может загружать, передавать или публиковать Контент на Сайте, если он не был создан Пользователем

лично или на размещение которого у него нет разрешения правообладателя.</p>
<p>Пользователь понимает и соглашается с тем, что Администратор Сайта может, но не обязан, просматривать

Сайт на наличие запрещённого Контента и может удалять или перемещать (без предупреждения) любой

Контент или Пользователей по своему личному усмотрению, по любой причине или без причины, включая

без всяких ограничений перемещение или удаление Контента, который, по мнению Администратора Сайта,

нарушает настоящие Правила или который может быть незаконным или может нарушать права, причинить

вред или угрожать безопасности других Пользователей или третьих лиц.</p>
<p>Размещая свой Контент на Сайте, Пользователь управомочивает Администратора Сайта делать копии своего

Контента с целью упорядочения и облегчения публикации и хранения пользовательского Контента на

Сайте.</p>
<p>Администратор Сайта не несёт ответственности за любой ущерб компьютеру Пользователя или иного лица,

мобильным устройствам, любому другому оборудованию или программному обеспечению, вызванный или

связанный со скачиванием материалов с Сайта или по ссылкам, размещённым на Сайте.</p>
<p><strong>Ограничение ответственности</strong></p>
<p>Ссылки, расположенные на Сайте, соединяющие Пользователя с сайтами третьих лиц, используются

исключительно для целей удобства пользования Сайтом. Администратор Сайта не несёт ответственности за

материалы, размещённые на сайтах третьих лиц.</p>
<p>Администратор Сайта может вносить любые изменения в Контент Сайта в любое время без

предварительного уведомления.</p>
<p><strong>Другие положения</strong></p>
<p>Администратор Сайта сохраняет за собой право по своему усмотрению:</p>
<ul>
	<li>
		<p>пересматривать настоящие Правила, путём обновления версии данной страницы Сайта;</p>
	</li>
	<li>
		<p>проводить мониторинг любых сообщений оставленных Пользователями на Сайте и удалять их.</p>
	</li>
</ul>
<p>Если любое положение настоящих Правил будет признано незаконным, недействительным или не имеющим

юридической силы, либо станет неприменимым в силу каких-либо иных причин, это не повлияет на

действительность и применимость остальных положений.</p>
<p>Настоящие Правила составляют соглашение между Пользователем и Администратором Сайта относительно

порядка использования Сайта и его сервисов.</p>
<p>Сайт или его сервисы могут быть в то или иное время частично или полностью недоступны по причине

проведения профилактических или иных работ или по любым другим причинам технического характера.

Администратор Сайта имеет право проводить необходимые профилактические или иные работы в то или

иное время по своему усмотрению с предварительным уведомлением Пользователей или без такового.</p>
<p><strong>Правила хранения частной информации</strong></p>
<p><i>Правила обработки персональных данных</i></p>
<p>Администратор Сайта ставит своей целью обеспечение постоянного соблюдения конфиденциальности в

отношении всех лиц, чьи персональные данные обрабатываются Администратором Сайта, и проявление

должной осторожности в отношении их персональных данных. Администратор Сайта принимает

технические и организационно-правовые меры в целях обеспечения защиты персональных данных

Пользователя от неправомерного или случайного доступа к ним, уничтожения, изменения, блокирования,

копирования, распространения, а также от иных неправомерных действий.</p>
<p>Администратор Сайта в соответствии с положениями настоящих Правил является оператором персональных

данных, отвечающим за обработку персональных данных, рассматриваемых в настоящих положениях.

Администратор Сайта не проверяет достоверность получаемой информации о пользователях.</p>
<p>Персональные данные, включая такие данные, как: имя, фамилия, отчество, e-mail, номер телефона (факса),

а также предоставляемые по желанию Пользователя сведения о месте работы/должности, адресе

проживания, обрабатываются с помощью компьютера. Персональные данные Пользователей хранятся

исключительно на электронных носителях. Передача персональных данных Пользователем Администратору

Сайта через форму обратной связи на Сайте или иным образом означает согласие Пользователя на передачу,

хранение, обработку и использование его персональных данных.</p>
<p>Персональные данные, предоставляемые Пользователем, будут обрабатываться в полном соответствии с

настоящими Правилами с целью (а) управления запросами от Пользователей, (б) предоставления

информации и услуг в связи с такими запросами, а также для целей (в) маркетинга и последующей работы с

клиентами и (г) развития продаж/продукции. Представляя персональные данные, Пользователь соглашается

соблюдать условия, указанные в настоящих Правилах хранения частной информации и Условиях

использования.</p>
<p>Любое посещение Сайта может привести к регистрации IP-адреса, использованного для такого доступа. IP-

адреса используются Администратором Сайта для измерения пользования Сайтом. (IP-адрес – это

уникальный номер, позволяющий идентифицировать установление пользователем соединения с

Интернетом, он автоматически предоставляется веб-браузером при каждом просмотре Сайта

Пользователем.) Такое измерение может также включать в себя сбор информации о том, как Пользователь

использует Сайт, что также может включать в себя мониторинг использования Сайта с целью содействия

Администратору Сайта в совершенствовании Сайта, персонализации работы Пользователя путем настройки

просматриваемого контента, в оптимизации пользовательского опыта и определении частоты пользования.

Кроме того, Администратор Сайта вправе проводить статистический анализ собранных данных.</p>
<p><i>Раскрытие персональной информации третьим лицам</i></p>
<p>Администратор Сайта не передает персональные данные Пользователей 3-им лицам, за исключением

случаев, прямо предусмотренных настоящими Правилами. Предоставление персональных данных

Пользователей по запросу органов государственной власти/местного самоуправления осуществляется в

порядке, предусмотренном законодательством РФ.</p>
<p><i>Доступ к персональным данным и их исправление</i></p>
<p>Пользователь вправе на основании запроса получать от Администратора Сайта информацию, касающуюся

обработки его персональных данных. Соответствующий запрос направляется Администратору Сайта в

письменной форме по следующему адресу: 109443, г. Москва, ул. Зеленодольская, вл. 42.</p>
<p>Запрос, направляемый Пользователем, должен содержать следующую информацию:</p>
<p>Если обращается физическое лицо:</p>
<ul>
	<li>
		<p>номер основного документа, удостоверяющего личность Пользователя;</p>
	</li>
	<li>
		<p>сведения о дате выдачи указанного документа и выдавшем его органе;</p>
	</li>
	<li>
		<p>текст запроса в свободной форме (в запросе необходимо обязательно указать дату регистрации на Сайте);</p>
	</li>
	<li>
		<p>подпись пользователя.</p>
	</li>
</ul>
<p>Если обращается юридическое лицо:</p>
<ul>
	<li>
		<p>запрос в свободной форме на фирменном бланке (в запросе необходимо обязательно указать дату

регистрации на Сайте);</p>
	</li>
	<li>
		<p>запрос должен быть подписан уполномоченным лицом, с приложением документов, подтверждающих

полномочия лица.</p>
	</li>
</ul>
<p>Администратор Сайта обязуется рассмотреть и направить ответ на поступивший запрос Пользователя в

течение 30 календарных дней с момента поступления обращения (запроса) Пользователя.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>