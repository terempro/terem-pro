<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "правила строительства терем");
$APPLICATION->SetPageProperty("title", "Полезный раздел | Правила строительства и заказа домов");
$APPLICATION->SetTitle("Полезный раздел | Правила строительства и заказа домов");
?>

	<section class="content text-page white" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="my-text-title text-center text-uppercase desgin-h1">
									<h1>
										ИНФОРМАЦИЯ ПО ОРГАНИЗАЦИИ<br>СТРОИТЕЛЬСТВА В КОМПАНИИ «ЯР-ГРАД»
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
                                    Клиентов компании «ЯР-ГРАД» очень часто интересуют различные нюансы, касающиеся организации строительного процесса. Мы собрали для Вас всю необходимую информацию, начиная от момента заключения договора, заканчивая сдачей готового объекта, гарантией и работой службы рекламации.
								</p>
								<br>
								<br>
							</div>
						</div>
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
											<div class="my-table">
												<div>
													<p>Формирование цены. Оплата</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq1">
											<div class="( my-collapse-content + design-main )">
												<p>Прежде всего, мы работаем без предоплаты по поэтапной системе расчета.* Это означает, что Вы вносите оплату только по факту выполнения определенной части заказа.</p>
												<p>Первый платеж в размере 40% вносится только после возведения фундамента.</p>
												<p>Второй платеж в размере 40% - только после строительства дома под кровлю.</p>
												<p>Оставшиеся 20% - только после окончательной сдачи дома.<span style="color:rgba(0,0,0,0.4);">**</span> </p>
												<p>На нашем сайте (в разделе «Каталог домов»), в СМИ и других печатных материалах мы обозначаем стоимость дома в комплектации «Лето», в которую входят:</p>
												<ul>
													<li>
														<p>Фундамент;</p>
													</li>
													<li>
														<p>Весь материал для сборки дома;</p>
													</li>
													<li>
														<p>Окна, двери, кровля, материал для внутренней отделки;</p>
													</li>
													<li>
														<p>Доставка;</p>
													</li>
													<li>
														<p>Работы по установке и сборке дома.</p>
													</li>
												</ul>
												<p>Все остальные работы (устройство инженерных систем, дополнительное утепление, пристройка террас и веранд и т.д.) являются дополнительными и осуществляются за отдельную плату.</p>

												<p style="font-size:12px;color:rgba(0,0,0,0.4);">*&nbsp;&nbsp;&nbsp;Условия действуют для жителей Ярославля и Ярославской области.</p>
												<p style="font-size:12px;color:rgba(0,0,0,0.4);">**&nbsp;Форма оплаты 20-60-20 действует при условии соблюдения срока начала строительства, предлагаемого Компанией.
												<br>&nbsp;&nbsp;&nbsp;&nbsp;В остальных случаях действует форма оплаты 40-40-20. Подробности узнавайте в отделе продаж.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->

						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
											<div class="my-table">
												<div>
													<p>Доставка и сборка дома</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq2">
											<div class="( my-collapse-content + design-main )">
												<p>Компания «ЯР-ГРАД» имеет свое современное и хорошо оборудованное производство, где формируется полный комплект будущего дома. После чего комплект доставляется на участок заказчика. Строительство домов осуществляется в пределах Ярославской области. Мы предлагаем возможность бесплатной доставки домов, электрики и инженерных коммуникаций, садовых домов и бань по всей Ярославской области. Подробнее об условиях доставки можно узнать в нашем офисе или по телефону +7 (485) 260-72-29</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="false" aria-controls="faq4">
											<div class="my-table">
												<div>
													<p>Заключение договора</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq4">
											<div class="( my-collapse-content + design-main )">
												<p>Заключение договора начинается с обозначения клиентом характеристик своего участка. Вам будет необходимо указать:</p>
												<ul>
													<li>
														<p>Район застройки;</p>
													</li>
													<li>
														<p>Место строительства;</p>
													</li>
													<li>
														<p>Площадь участка;</p>
													</li>
													<li>
														<p>Состояние подъезда к участку;</p>
													</li>
													<li>
														<p>Тип грунта;</p>
													</li>
													<li>
														<p>Необходимость оформления пропуска для проезда автотранспорта;</p>
													</li>
													<li>
														<p>Рельеф местности, наличие других строений, деревьев, пеньков;</p>
													</li>
													<li>
														<p>Наличие водоснабжения и электроэнергии на участке;</p>
													</li>
													<li>
														<p>Наличие жилого помещения для размещения бригады.</p>
													</li>
												</ul>
												<p>Отметим, что монтаж фундамента может осуществляться по отдельному договору. </p>
												<p>При заключении договора также в обязательном порядке необходимо предъявить паспорт и документ, удостоверяющий право собственности на землю.</p>
												<p>Если у Вас нет своего земельного участка, в нашей компании Вам могут помочь с выбором и приобретением земли. </p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq5" aria-expanded="false" aria-controls="faq5">
											<div class="my-table">
												<div>
													<p>Работа строительной бригады</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq5">
											<div class="( my-collapse-content + design-main )">
												<p>Средний срок строительства каркасного дома может достигать 45 дней. Все это время рабочие проживают на участке заказчика, который должен разместить их в пригодное для жизни помещение. Если такой возможности нет, то рабочие привезут с собой бытовку.</p>
												<p>Клиент не оплачивает проживание бригады!</p>
												<p>Дата начала работ обязательно обговаривается заранее.</p>
												<p>Рабочий день строителей может быть ненормированным из-за погодных условий, но, как показывает практика, наши рабочие укладываются в обозначенный срок строительства. </p>
												<p>За 3-4 дня до назначенной даты начала строительства заказчику необходимо созвониться с диспетчером и договориться о месте и времени встречи с бригадой. </p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq6" aria-expanded="false" aria-controls="faq6">
											<div class="my-table">
												<div>
													<p>Клиентская служба</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq6">
											<div class="( my-collapse-content + design-main )">
												<p>Обратиться в клиентскую службу можно, написав обращение в соответствующую форму в разделе «Контакты» на нашем сайте, а также позвонив по телефону +7 (485) 260-72-29</p>
												<p>При обращении в клиентскую службу при себе необходимо иметь паспорт и/или оригинал договора.</p>
												<p>Клиентская служба работает ежедневно с 09.00 до 18.00.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq7" aria-expanded="false" aria-controls="faq7">
											<div class="my-table">
												<div>
													<p>Служба рекламации</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq7">
											<div class="( my-collapse-content + design-main )">
												<p>Служба рекламации занимается гарантийным обслуживанием и решением спорных вопросов после окончания строительства (подписания акта приемки). Обратиться в службу рекламации можно, написав обращение в соответствующую форму в разделе «Контакты» на нашем сайте, а также позвонив по телефону +7 (485) 260-72-29</p>
												<p>При обращении в службу рекламации при себе необходимо иметь:</p>
												<ul>
													<li>
														<p>Паспорт;</p>
													</li>
													<li>
														<p>Оригинал договора;</p>
													</li>
													<li>
														<p>Акт приемки КС-2;</p>
													</li>
													<li>
														<p>Квитанцию об оплате.</p>
													</li>
													<li>
														<p>Фотографии дефектов (по возможности). </p>
													</li>
												</ul>
												<p>Служба рекламации принимает заявления ежедневно с 09.00 до 19.00.</p>
												<p>Обращаем Ваше внимание, что заявление принимается только от заказчика проекта либо от иного лица, уполномоченного заказчиком (при наличии нотариально заверенной доверенности).</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq8" aria-expanded="false" aria-controls="faq8">
											<div class="my-table">
												<div>
													<p>Гарантия качества</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq8">
											<div class="( my-collapse-content + design-main )">
												<p>На всех этапах строительства дома заказчик имеет право осуществлять соответствующий контроль. После окончания строительства клиент осматривает готовое строение и подписывает акт приемки.</p>
												<p>В случае обнаружения недостатков, заказчик может обратиться с заявлением в службу рекламации компании «ЯР-ГРАД». При подтверждении наличия дефектов, компания устраняет их бесплатно.</p>
												<p>На дома из бруса гарантия составляет 25 лет.</p>
												<p>На каркасные дома гарантия составляет до 15 лет.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->

					</div>
				</div>
			</div>

		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>