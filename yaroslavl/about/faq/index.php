<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "faq, часто задоваемые вопросы");
$APPLICATION->SetPageProperty("title", "Часто задаваемые вопросы");
$APPLICATION->SetTitle("Хотелось бы остановить выбор на компании ТеремЪ - Задайте вопрос.");
?>

    <section class="content text-page white" role="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="white padding-side clearfix">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="text-center text-uppercase">
                                    <h1>
                                        Часто задаваемые вопросы
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <p>
                                    На этой странице Вы можете найти ответы на часто задаваемые нашими клиентами вопросы. Уточнить информацию можно у наших менеджеров на выставочной площадке и по телефону.
                                </p>
                                <br>
                                <br>
                            </div>
                        </div>
                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>Вы занимаетесь строительством по всей России или только в Ярославле?</p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq1">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Компания «ЯР-ГРАД» строит до 600 км от г. Ярославля</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->

                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>	Занимаетесь ли вы достройкой и реконструкцией существующего строения? И как можно получить расчёт?</p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq2">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Достройкой и реконструкцией занимаемся. Расчет можно получить отправив запрос на нашу электронную почту info.yar@terem-pro.ru</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->

                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>Можно ли использовать материнский капитал при строительстве дома в компании «ЯР-ГРАД»?</p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq3">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Да. Материнский капитал можно использовать при строительстве дома в компании «ЯР-ГРАД».</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->

                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>	Можно ли заказать дом по индивидуальному проекту?</p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq4">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Дом по индивидуальному проекту можно заказать в компании «ЯР-ГРАД».</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->

                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq5" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>	У меня есть фундамент. Можно ли построить на нём дом по собственному проекту?</p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq5">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Конечно, можно  построить дом на вашем фундаменте по собственному проекту.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->
                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq6" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>Возможен ли самостоятельный вывоз комплекта дома, материалов, и/или самостоятельная сборка? </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq6">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Нет, это невозможно. Мы строительная компания, которая имеет свое производство и строительные бригады. Дома мы строим сами, с использованием современных строительных технологий и качественных материалов. Мы даем гарантию на построенный дом, собранный по собственной технологии и специально обученными специалистами.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->


                        <!--one item 7-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq7" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>Компания «ЯР-ГРАД»  занимается инженерными коммуникациями? Входят ли в стоимость дома эти работы?</p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq7">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Компания «ЯР-ГРАД» занимается инженерными коммуникациями. Можно заключить отдельные договоры на электромонтажные работы, установку отопления, водоснабжения, канализации и т.д.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 7 END-->

                        <!--one item 8-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq8" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>Какие в компании условия оплаты? Есть ли предоплата?</p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq8">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>
                                                    Оплата осуществляется по факту выполнения этапов  работ в любом отделении Сбербанка. Подробную информацию можно получить в офисе компании либо по контактному телефону +7 (485) 260-72-29
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 8 END-->


                        <!--one item 9-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq9" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>
                                                        Есть ли возможность купить дом в кредит?
                                                    </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq9">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>
                                                    Дом в кредит купить можно. Подробную информацию можно получить в офисе компании либо по контактному телефону +7 (485) 260-72-29.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 9 END-->

                        <!--one item 10-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq10" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>
                                                        Выезжают ли ваши строители на участок для того, чтобы определить условия застройки и пожелания клиента на месте?
                                                    </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq10">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>
                                                    Да, при необходимости выезд специалиста возможен, но только при условии заключения клиентом договора на строительство. Эта услуга платная.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 10 END-->

                        <!--one item 11-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq11" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>
                                                        Можно ли в домах компании «Теремъ» проживать круглогодично?
                                                    </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq11">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>
                                                    Да, в домах для постоянного проживания тысячи наших заказчиков живут круглогодично. Эти дома предназначены для строительства на дачных участках и на участках для индивидуального жилищного строительства. Комплектация домов предусматривает его круглогодичное использование с использованием стационарных отопительных агрегатов (АОГВ, калориферов, печей и пр.).
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 11 END-->

                        <!--one item 12-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq12" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>
                                                        Какие установлены сроки строительства домов?
                                                    </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq12">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>
                                                    Разные дома имеют различные сроки строительства. От нескольких дней до 4 месяцев
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 12 END-->

                        <!--one item 13-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq13" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>
                                                        Что такое каркасная технология строительства домов?
                                                    </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq13">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>
                                                    Стены каркасного дома выполняются из доски 35х150 мм. Доска устанавливается с шагом 500 (400 мм для домов для постоянного проживания) мм. Между стойками укладывается утеплитель 150 мм (200 мм для домов для постоянного проживания). С двух сторон стены обшиваются вагонкой (дома для постоянного проживания с наружной стороны обшиваются сайдингом), с наружной стороны - по изоспану А, с внутренней стороны - по изоспану Б. Весь конструктив выполняется из сухого материала.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 12 END-->

                        <!--one item 14-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq14" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>
                                                        Что входит в базовую комплектацию дома?
                                                    </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq14">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>В базовую комплектацию дома (бани) входит:</p>
                                                <ul>
                                                    <li>
                                                        <p>Фундамент;</p>
                                                    </li>
                                                    <li>
                                                        <p>Весь материал для сборки дома;</p>
                                                    </li>
                                                    <li>
                                                        <p>Окна, двери, кровля, материал для внутренней отделки;</p>
                                                    </li>
                                                    <li>
                                                        <p>Доставка;</p>
                                                    </li>
                                                    <li>
                                                        <p>Работы по установке и сборке дома.</p>
                                                    </li>
                                                </ul>
                                                <p>Узнать о каждом конкретном доме более подробно можно в разделе «Каталог домов».</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 14 END-->

                        <!--one item 15-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq15" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>
                                                        Какую гарантию компания «ЯР-ГРАД» даёт на дома?
                                                    </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq15">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Гарантия до 25 лет.</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 15 END-->

                        <!--one item 16-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq16" aria-expanded="false" aria-controls="faq1">
                                            <div class="my-table third-col">
                                                <div>
                                                    <p class="icon">?</p>
                                                </div>
                                                <div>
                                                    <p>
                                                        Где находится выставочная площадка компании «ЯР-ГРАД»? Как к вам добраться?
                                                    </p>
                                                </div>
                                                <div>
                                                    <i class="glyphicon glyphicon-menu-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="collapse my-collapse" id="faq16">
                                            <div class="( my-collapse-content + design-main )">
                                                <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                                <p>Офис компании «ЯР-ГРАД» расположен по адресу : г.Ярославль, Московское шоссе д. 97, ТЦ Фрунзенский. Добраться до нас можно как на авто, так и на общественном транспорте.</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item 15 END-->

                    </div>
                </div>
            </div>

        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
