<!DOCTYPE html>
<html lang="ru_RU"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title></title>
	<link href='assets/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="/include/scripts/jquery.min.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="sticky-page">
			<section class="content full-height bg-prev">
				<div class="container full-height">
					<div class="row full-height">
						<div class="col-xs-12 col-md-12 col-sm-12 full-height my-flex flex-center-logo">
							<div class="logo-prev">
								<img src="assets/img/logo-prev.png" alt="">
							</div>
						</div>
						<div class="col-xs-12 col-md-12 col-sm-12 full-height my-flex flex-center main-page hidden">
							<div class="full-width state1" data-item="state_1">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>

								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Мы стремимся к улучшению качества обслуживания наших клиентов, поэтому нам очень важно ваше мнение о работе нашей компании. Пожалуйста, выберите «заполнить анкету» или кнопку «оставить отзыв».</p>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<button type="button" class="btn btn-default btn-grey btn-big active text-uppercase state_1">заполнить анкету</button>
												<button type="button" class="btn btn-default btn-grey btn-big text-uppercase disable state_24">ОСТАВИТЬ ОТЗЫВ</button>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state24 hidden" data-item="state_24">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>

								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Пожалуйста, поделитесь впечатлениями о качестве обслуживания в нашей компании. Ваше мнение очень важно для нас.</p>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12 text-right">
											<a href="#" class="comment-next state_26">Далее ></a>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<textarea name="comment" id="comment" cols="7" rows="7"></textarea>
										</div>
									</div>
									
								</div>
							</div>


							<div class="full-width state2 hidden" data-item="state_2">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>

								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Для нас важно постоянно совершенствовать качество обслуживания наших клиентов. Если Вы ответите на все вопросы данной анкеты, то внесёте существенный вклад в дальнейшее развитие нашего сервиса. Нам очень важно ваше мнение о нашей работе.</p>
											<p>Впереди всего 11 шагов, это не займет много времени.</p>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<button type="button" class="btn btn-default btn-grey btn-big active text-uppercase state_2">ПЕРЕЙТИ К ОТВЕТАМ</button>
												<button type="button" class="btn btn-default btn-grey btn-big text-uppercase state_goBack">НАЗАД</button>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state3 hidden" data-item="state_3">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 1/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default no-border num">2</button></li>
												<li><button type="button" class="btn btn-default no-border num">3</button></li>
												<li><button type="button" class="btn btn-default no-border num">4</button></li>
												<li><button type="button" class="btn btn-default no-border num">5</button></li>
												<li><button type="button" class="btn btn-default no-border num">6</button></li>
												<li><button type="button" class="btn btn-default no-border num">7</button></li>
												<li><button type="button" class="btn btn-default no-border num">8</button></li>
												<li><button type="button" class="btn btn-default no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Укажите первичный источник информации о нашей компании</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width TV">Телевидение</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width INET">Интернет</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width RADIO">Радио</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width PARTNERS">Партнеры</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width PRESS">Пресса</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width CATALOG">Каталог домов</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width MORE5">Другое</button>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>

								<div class="content-block-three more-5-inner hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Укажите, по возможности</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-9 col-sm-9">
														<input type="text" class="form-control INPUT1" />
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small full-width BTNNEXT1">Далее <i class="glyphicon glyphicon-menu-right"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="content-block-three TV-inner hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>На каком канале</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width TVCHANEL1">НТВ</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width TVCHANEL2">Euronews</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width TVCHANEL3">Россия 1</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width TVCHANEL4">Россия 2</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width TVCHANEL5">ТВЦ</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width TVCHANEL6">ТВ 3</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width TVCHANEL7">Звезда</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width TVCHANEL9">Первый канал</button>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>

								<div class="content-block-three INET-inner hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>На каком ресурсе:</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-12 col-sm-12">
														<div class="my-flex not-grid-items">
															<div><button type="button" class="btn btn-default small no-border full-width INETChenel1">Google</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width INETChenel2">Yandex</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width INETChenel3">Mail.ru</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width INETChenel4">Rambler</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width INETChenel5">Форум</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width INETChenel6">Соц. Сети</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width INETChenel7">YouTube</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width INETChenel8">Баннер</button></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="content-block-three RADIO-inner hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>На какой станции:</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-12 col-sm-12">
														<div class="my-flex not-grid-items">
															<div><button type="button" class="btn btn-default small no-border full-width RADIOChanel1">Ретро FM</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width RADIOChanel2">Радио 7</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width RADIOChanel3">Спорт FM</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width two-string RADIOChanel4">Дорожное<br>радио</button></div>
															<div><button type="button" class="btn btn-default small no-border full-width RADIOChanel5">Авторадио</button></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="content-block-three PARTNERS-inner hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Пожалуйста уточните:</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PARTNERSChenel1">Своя дача, участки</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PARTNERSChenel2">ТРЦ Афимолл</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PARTNERSChenel3">Другое</button>
													</div>

												</div>
												<div class="more-6-inner hidden">
													<div class="row">
														<div class="col-xs-12 col-md-12 col-sm-12">
															<p>Укажите, по возможности</p>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 col-md-12 col-sm-12">
															<div class="btns">
																<div class="row">
																	<div class="col-xs-12 col-md-9 col-sm-9">
																		<input type="text" class="form-control INPUT6" />
																	</div>
																	<div class="col-xs-12 col-md-3 col-sm-3">
																		<button type="button" class="btn btn-default small full-width BTNNEXT6">Далее <i class="glyphicon glyphicon-menu-right"></i></button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>

								<div class="content-block-three PRESS-inner hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>В каком издании:</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel1">Метро</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel2">Антенна</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel3">Теленеделя</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel4">Аргументы и Факты</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel5">Красивые дома</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel6">Деревянные дома</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel7">Идеи Вашего Дома</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel8">Новые Известия</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width PRESSChenel9">7 Дней</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<!-- <div class="content-block-three CATALOG-inner hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Пожалуйста, уточните где конкретно:</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width CATALOGChenel1">Каталог на АЗС</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width two-string CATALOGChenel2">Каталог<br>в «Азбука Вкуса»</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width two-string CATALOGChenel3">Каталог<br>в ТЦ «Мега»</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width two-string CATALOGChenel4">Каталог<br>на строительном рынке</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width two-string CATALOGChenel5">Каталог<br>на выставке меда</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width two-string CATALOGChenel6">В журнале<br>«Любимая дача»</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width two-string CATALOGChenel7">В журнале<br>«Красивые дома»</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width two-string CATALOGChenel8">В журнале<br>«Деревянные дома»</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width CATALOGChenel9">В подъезде дома</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width CATALOGChenel10">В бизнес-центре</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width CATALOGChenel11">В пробке</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div> -->

								<div class="content-block-three OTHER-inner hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Пожалуйста уточните:</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width OTHERChenel1 two-string">Газета в почтовом<br>ящике</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width OTHERChenel2 two-string">Листовка в журнале<br>«Любимая дача»</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width OTHERChenel3">Щит на улице</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width OTHERChenel4">Рекомендации</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width OTHERChenel5 two-string">Проходил / проезжал<br>мимо</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small no-border full-width OTHERChenel6">Другое</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="content-block-four OTHER-inner-2 hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Укажите, по возможности</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-9 col-sm-9">
														<input type="text" class="form-control INPUT2" />
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small full-width BTNNEXT2">Далее <i class="glyphicon glyphicon-menu-right"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

							<div class="full-width state11 hidden" data-item="state_11">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 2/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default no-border num">3</button></li>
												<li><button type="button" class="btn btn-default no-border num">4</button></li>
												<li><button type="button" class="btn btn-default no-border num">5</button></li>
												<li><button type="button" class="btn btn-default no-border num">6</button></li>
												<li><button type="button" class="btn btn-default no-border num">7</button></li>
												<li><button type="button" class="btn btn-default no-border num">8</button></li>
												<li><button type="button" class="btn btn-default no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>На каком транспорте вы добирались к нам на выставку</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width TRAN1">Личный</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width TRAN2">Общественный</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width TRAN3">Пешком</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state12 hidden" data-item="state_12">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 3/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default no-border num">4</button></li>
												<li><button type="button" class="btn btn-default no-border num">5</button></li>
												<li><button type="button" class="btn btn-default no-border num">6</button></li>
												<li><button type="button" class="btn btn-default no-border num">7</button></li>
												<li><button type="button" class="btn btn-default no-border num">8</button></li>
												<li><button type="button" class="btn btn-default no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>На основании какого фактора Вы приняли решение приехать к нам</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width one-size tostate13">Известность<br>компании</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default line3pl full-width one-size tostate13">Положительная<br>репутация компании</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default line3pl full-width one-size tostate13">Приемлимая<br>стоимость строительства</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width one-size tostate13">Рекомендации<br>знакомых</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-6 col-sm-6">
														<button type="button" class="btn btn-default full-width one-size tostate13">Возможность оценить ассортимент<br>на выставочной площадке</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width line3pl one-size tostate13">Реклама на ТВ,<br>в журнале, на улице</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width one-size tostate13">Ассортимент<br>проектов</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width line3pl one-size tostate13">Узнали<br>о проведении акции</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width one-size MORE2">Другое</button>
													</div>
												</div>
												<div class="row hidden more2">
													<div class="col-xs-12 col-md-9 col-sm-9">
														<input type="text" class="form-control INPUT3" />
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small full-width BTNNEXT3">Далее <i class="glyphicon glyphicon-menu-right"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state13 hidden" data-item="state_13">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 4/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default no-border num">5</button></li>
												<li><button type="button" class="btn btn-default no-border num">6</button></li>
												<li><button type="button" class="btn btn-default no-border num">7</button></li>
												<li><button type="button" class="btn btn-default no-border num">8</button></li>
												<li><button type="button" class="btn btn-default no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Какой из факторов наиболее важен при выборе дома (укажите один)</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Внешний вид</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Цена</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Гарантия</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Планировка</button>
													</div>
												</div>

												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Технологии</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Срок строительства</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state14 hidden" data-item="state_14">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 5/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default active no-border num">5</button></li>
												<li><button type="button" class="btn btn-default no-border num">6</button></li>
												<li><button type="button" class="btn btn-default no-border num">7</button></li>
												<li><button type="button" class="btn btn-default no-border num">8</button></li>
												<li><button type="button" class="btn btn-default no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Стоимость проекта по договору</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default two-string middle full-width">Соответствует<br>ожиданиям</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default two-string middle full-width">Выше<br>ожидаемой</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default two-string middle full-width">Ниже<br>ожидаемой</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default two-string middle full-width">Значительно ниже<br>ожидаемой</button>
													</div>

												</div>


											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state15 hidden" data-item="state_15">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 6/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default active no-border num">5</button></li>
												<li><button type="button" class="btn btn-default active no-border num">6</button></li>
												<li><button type="button" class="btn btn-default no-border num">7</button></li>
												<li><button type="button" class="btn btn-default no-border num">8</button></li>
												<li><button type="button" class="btn btn-default no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Просим Вас оценить обслуживание нашими специалистами от 1 – плохо, до 5 – отлично<br>Сколько звёзд Вы бы нам поставили?</p>
											<br>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="row">
												<div class="col-xs-12 col-md-3 col-sm-3">
													<p>CALL-ЦЕНТР</p>
													<div class="rating">
														<input type="radio" name="star1" id="star1" value="1" checked="checked" />
														<label for="star1" value="1"><div class="star"></div></label>
														<input type="radio" name="star1" id="star2" value="2" />
														<label for="star2" value="2"><div class="star"></div></label>
														<input type="radio" name="star1" id="star3" value="3" />
														<label for="star3" value="3"><div class="star"></div></label>
														<input type="radio" name="star1" id="star4" value="4" />
														<label for="star4" value="4"><div class="star"></div></label>
														<input type="radio" name="star1" id="star5" value="5" />
														<label for="star5" value="5"><div class="star"></div></label>
													</div>
												</div>
												<div class="col-xs-12 col-md-3 col-sm-3">
													<p>МЕНЕДЖЕР</p>
													<div class="rating">
														<input type="radio" name="star2" id="star6" value="1" checked="checked" />
														<label for="star6"><div class="star"></div></label>
														<input type="radio" name="star2" id="star7" value="2" />
														<label for="star7"><div class="star"></div></label>
														<input type="radio" name="star2" id="star8" value="3" />
														<label for="star8"><div class="star"></div></label>
														<input type="radio" name="star2" id="star9" value="4" />
														<label for="star9"><div class="star"></div></label>
														<input type="radio" name="star2" id="star10" value="5" />
														<label for="star10"><div class="star"></div></label>
													</div>
												</div>
												<div class="col-xs-12 col-md-3 col-sm-3">
													<p>ТЕХ. ОТДЕЛ</p>
													<div class="rating">
														<input type="radio" name="star3" id="star11" value="1" checked="checked" />
														<label for="star11"><div class="star"></div></label>
														<input type="radio" name="star3" id="star12" value="2" />
														<label for="star12"><div class="star"></div></label>
														<input type="radio" name="star3" id="star13" value="3" />
														<label for="star13"><div class="star"></div></label>
														<input type="radio" name="star3" id="star14" value="4" />
														<label for="star14"><div class="star"></div></label>
														<input type="radio" name="star3" id="star15" value="5" />
														<label for="star15"><div class="star"></div></label>
													</div>
												</div>
												<div class="col-xs-12 col-md-3 col-sm-3">
													<p>ДОГОВОРНЫЙ ОТДЕЛ</p>
													<div class="rating">
														<input type="radio" name="star4" id="star16" value="1" checked="checked" />
														<label for="star16"><div class="star"></div></label>
														<input type="radio" name="star4" id="star17" value="2" />
														<label for="star17"><div class="star"></div></label>
														<input type="radio" name="star4" id="star18" value="3" />
														<label for="star18"><div class="star"></div></label>
														<input type="radio" name="star4" id="star19" value="4" />
														<label for="star19"><div class="star"></div></label>
														<input type="radio" name="star4" id="star20" value="5" />
														<label for="star20"><div class="star"></div></label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-9 col-sm-9"></div>
										<div class="col-xs-12 col-md-3 col-sm-3">
											<button type="button" class="btn btn-default small full-width NEXTSTATE4">Далее <i class="glyphicon glyphicon-menu-right"></i></button>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state16 hidden" data-item="state_16">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 7/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default active no-border num">5</button></li>
												<li><button type="button" class="btn btn-default active no-border num">6</button></li>
												<li><button type="button" class="btn btn-default active no-border num">7</button></li>
												<li><button type="button" class="btn btn-default no-border num">8</button></li>
												<li><button type="button" class="btn btn-default no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Цель строительства</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-6 col-sm-6">
														<button type="button" class="btn btn-default full-width">Для проживания в летний период</button>
													</div>
													<div class="col-xs-12 col-md-6 col-sm-6">
														<button type="button" class="btn btn-default full-width">Для постоянного проживания</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state17 hidden" data-item="state_17">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 8/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default active no-border num">5</button></li>
												<li><button type="button" class="btn btn-default active no-border num">6</button></li>
												<li><button type="button" class="btn btn-default active no-border num">7</button></li>
												<li><button type="button" class="btn btn-default active no-border num">8</button></li>
												<li><button type="button" class="btn btn-default no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Для кого Вы строите дом</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Для себя</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Для детей</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Для родителей</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Продажа</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state18 hidden" data-item="state_18">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 9/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default active no-border num">5</button></li>
												<li><button type="button" class="btn btn-default active no-border num">6</button></li>
												<li><button type="button" class="btn btn-default active no-border num">7</button></li>
												<li><button type="button" class="btn btn-default active no-border num">8</button></li>
												<li><button type="button" class="btn btn-default active no-border num">9</button></li>
												<li><button type="button" class="btn btn-default no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Чьё мнение о покупке было решающим</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Супруг/супруга</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Дети</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Самостоятельно</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Другое</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state19 hidden" data-item="state_19">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 10/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default active no-border num">5</button></li>
												<li><button type="button" class="btn btn-default active no-border num">6</button></li>
												<li><button type="button" class="btn btn-default active no-border num">7</button></li>
												<li><button type="button" class="btn btn-default active no-border num">8</button></li>
												<li><button type="button" class="btn btn-default active no-border num">9</button></li>
												<li><button type="button" class="btn btn-default active no-border num">10</button></li>
												<li><button type="button" class="btn btn-default no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Как будет производиться оплата</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Собственные средства</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Частично в кредит</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width">Кредит</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state20 hidden" data-item="state_20">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 11/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default active no-border num">5</button></li>
												<li><button type="button" class="btn btn-default active no-border num">6</button></li>
												<li><button type="button" class="btn btn-default active no-border num">7</button></li>
												<li><button type="button" class="btn btn-default active no-border num">8</button></li>
												<li><button type="button" class="btn btn-default active no-border num">9</button></li>
												<li><button type="button" class="btn btn-default active no-border num">10</button></li>
												<li><button type="button" class="btn btn-default active no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Род занятости</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Руководитель</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Студент</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Бизнес</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Специалист</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Госслужащий</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Военнослужащий</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Домохозяйка</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Пенсионер</button>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20">Менеджер</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width tostate20" >Не работаю</button>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default full-width MORE4">Другое</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="content-block-three more4 hidden">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Укажите, по возможности</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-9 col-sm-9">
														<input type="text" class="form-control INPUT4" />
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<button type="button" class="btn btn-default small full-width BTNNEXT4">Далее <i class="glyphicon glyphicon-menu-right"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state21 hidden" data-item="state_21">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 11/11
											</h4>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="list-numbers">
												<li><button type="button" class="btn btn-default active no-border num">1</button></li>
												<li><button type="button" class="btn btn-default active no-border num">2</button></li>
												<li><button type="button" class="btn btn-default active no-border num">3</button></li>
												<li><button type="button" class="btn btn-default active no-border num">4</button></li>
												<li><button type="button" class="btn btn-default active no-border num">5</button></li>
												<li><button type="button" class="btn btn-default active no-border num">6</button></li>
												<li><button type="button" class="btn btn-default active no-border num">7</button></li>
												<li><button type="button" class="btn btn-default active no-border num">8</button></li>
												<li><button type="button" class="btn btn-default active no-border num">9</button></li>
												<li><button type="button" class="btn btn-default active no-border num">10</button></li>
												<li><button type="button" class="btn btn-default active no-border num">11</button></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Спасибо, что уделили нам внимание и предоставили достоверную информацию о том, как началась история нашего сотрудничества. Благодаря Вашим ответам, качество работы компании «Теремъ» станет еще лучше.</p>
											<p>Пожалуйста, передайте планшет вашему менеджеру.</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<br>
														<button type="button" class="btn btn-default full-width">ОК</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="full-width state22 hidden" data-item="state_22">
								<div class="header">
									<div class="row my-flex">
										<div class="col-xs-12 col-md-6 col-sm-6">
											<img src="assets/img/logo.png" alt="">
										</div>
										<div class="col-xs-12 col-md-6 col-sm-6 text-right close-container">
											<button type="button" class="btn btn-default close">ЗАКРЫТЬ <span aria-hidden="true">×</span></button>
										</div>
									</div>
								</div>
								<div class="content-block-one">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<h4 class="text-uppercase">
												Анкета покупателя<br>вопрос 11/11
											</h4>
										</div>
									</div>
								</div>
								<div class="content-block-two">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="date">
												<p><strong>Дата</strong>:<?php echo date('d-m-Y');?></p>
												<p><strong>Анкета</strong> № <?php echo rand(100, 999);?>-<?php echo rand(10, 99);?></p>
											</div>
										</div>
									</div>

								</div>
								<div class="content-block-three">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>Внесите <strong>№ договора</strong></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<ul class="number-dog">
												<li>
													<select class="form-control number-dog-i" name="" id="sel1">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
													</select>
												</li>
												<li>
													<select class="form-control number-dog-i" name="" id="sel2">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
													</select>
												</li>
												<li>
													<select class="form-control number-dog-i" name="" id="sel3">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
													</select>
												</li>
												<li>
													<select class="form-control number-dog-i" name="" id="sel4">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
													</select>
												</li>
												<li>
													<select class="form-control number-dog-i" name="" id="sel5">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
													</select>
												</li>
												<li>
													<select class="form-control number-dog-i" name="" id="sel6">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
													</select>
												</li>
												<li>
													<select class="form-control number-dog-i" name="" id="sel7">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
													</select>
												</li>
												<li>
													<select class="form-control number-dog-i" name="" id="sel8">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
													</select>
												</li>
											</ul>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="btns">
												<div class="row">
													<div class="col-xs-12 col-md-4 col-sm-4">
														<br>
														<button type="button" class="btn btn-default full-width saveinfo" >СОХРАНИТЬ ДАННЫЕ</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
		</div>
		<link href='assets/css/style.min.css' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="assets/js/form.js"></script>
		<script type="text/javascript" src="assets/js/jquery.maskedinput.js"></script>
		<script type="text/javascript" src="assets/js/validator.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>
	</body>
	<div class="modal fade" id="done" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p class="text-center"><strong>ОБРАЩАЕМ ВАШЕ ВНИМАНИЕ.<br>ИНФОРМАЦИЯ НЕ БУДЕТ СОХРАНЕНА</p></strong>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-6">
							<button type="button" class="btn btn-default full-width send" id="send">ДА</button>
						</div>
						<div class="col-xs-12 col-md-6 col-sm-6">
							<button type="button" class="btn btn-default full-width two-string middle " data-dismiss="modal">ПРОДОЛЖИТЬ<br>ЗАПОЛНЕНИЕ</button>
						</div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	</html>