<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты компании Терем в Владимире");
$APPLICATION->SetPageProperty("keywords", "терем контакты, терем телефон, терем про почта");
$APPLICATION->SetTitle("Контакты компании Терем в Владимире");
// $APPLICATION->AddHeadScript("/bitrix/templates/.default/assets/js/map.js");
$code=$APPLICATION->CaptchaGetCode();

$request_failed = isset($_SESSION["CLAIM_FAILED"]);
$double_request = isset($_SESSION["CLAIM_SENT"]) && ($_SESSION["CLAIM_SENT"] == true);

?><script type="text/javascript" src="/bitrix/templates/.default/assets/js/map.js"></script>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-sm-8">
                            <div class="text-uppercase">
                                <h3 class="contact-title">
                                    ОФИЦИАЛЬНЫЙ ПАРТНЕР ВО ВЛАДИМИРЕ
                                </h3>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="text-right">
                                <h3 class="title-contanct-3">
                                    <span class="red"><a href="mailto:info.vladimir@terem-pro.ru">info.vladimir@terem-pro.ru</a></span>
                                </h3>
                                <h2 class="title-contanct-2">
                                    <span class="red"><a href="#">+7 (4922) 49-42-24</a></span><!--id="telMain3"-->
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <video class="VideoContact" style="position: relative;display: block;  margin: 0px -50px 0px; width: calc(100% + 100px);" controls="" itemprop="url">
                                <source src="/upload/contact.mp4" type="video/mp4">
                                <source src="/upload/contact.webm" type="video/webm">
                                <source src="/upload/contact.flv" type="video/flv">
                            Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="catalog-item-gallery white + my-margin">
                    <div class="white my-padding-50 clearfix">
                        <div class="contacts-toggle-btns">
                            <ul>
                                <li class="active">
                                    <a href="#1a" data-toggle="tab" class="btn btn-danger contact-btn text-uppercase">Владимир</a>
                                </li>
                            </ul>
                        </div>
                        <div class="row tab-content clearfix">
                            <div class="row-eq-height tab-pane active" id="1a">
                                <div class="col-xs-12 col-md-4 flex-column">
                                    <div>
                                        <p><strong>Страна:</strong> Российская Федерация</p>
                                    </div>
                                    <div>
                                        <p><strong>Город:</strong> Владимир</p>
                                    </div>
                                    <div>
                                        <p><strong>Полное наименование компании:</strong> ИП Белков А. А.</p>
                                    </div>
                                    <div>
                                        <p><strong>Адрес:</strong> г. Владимир, ул. Куйбышева, д. 22Б (Здание ТК М7 корпус В)</p>
                                    </div>
                                    <div>
                                        <p><strong>Телефон:</strong> +7 (4922) 49-42-24</p>
                                    </div>
                                    <div>
                                        <p><strong>Режим работы:</strong> ПН – ПТ 09:00 – 18:00 СБ – ВС 10:00 – 16:00</p>
                                    </div>
                                    <div>
                                        <p><strong>Корпоративная почта:</strong> info.vladimir@terem-pro.ru</p>
                                    </div>
                                    <div>
                                        <p><strong>Проезд общественным транспортом:</strong> Остановка: завод Магнетон. Автобусы: 14, 31, 7с, 13с, 32, 4с, 54.</p>
                                    </div>
                                    <div>
                                        <p><strong>Проезд на автомобиле:</strong> по трассе М7, поворот у АЗС к ТК «Самохвал», здание ТК М7 корпус В, вход со стороны трассы.</p>
                                    </div>
                                    <div>
                                        <button type="button" data-target="#call" data-toggle="modal" class="btn btn-danger contact-btn text-uppercase"><span class="hidden-sm visible-xs visible-md visible-lg">заявка на бесплатную консультацию</span><span class="visible-sm hidden-xs">консультация</span></button>
                                    </div>
                                    <div class="my-margin">
                                      <p>
                                        Остерегайтесь подделок! Только официальные партнеры компании «Теремъ» обеспечивают надежное строительство и предоставляют гарантию на все виды работ. Актуальный список дилеров опубликован на главном сайте компании <a href="https://www.terem-pro.ru">www.terem-pro.ru</a>, уточнить информацию можно по телефону.
                                      </p>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-8">
                                    <div id="map11" class="ymap"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row welcome-text" style="margin-top: 50px;">
                            <p class="text-center" style="font-size: 1em;">
                                <img src="/bitrix/templates/.default/assets/img/warning-sign.png" alt="Внимание!" class="warning-sign">
                                <strong>
                                    Строительная компания «Теремъ» приглашает партнеров из России и стран СНГ к сотрудничеству: <a href="mailto:partner@terem-pro.ru" class="my-blue-link">partner@terem-pro.ru</a>
                                </strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content text-page white" role="content" id="map-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="catalog-item-gallery my-padding-50 white mb-5">
                    <div class="tab-content">
                        <div id="zoom-map" style="overflow:hidden;position:relative;">
                            <img src="myMap.jpg" class="full-width" alt="">
                            <div class="btn-zooms">
                                <button class="btn btn-zoom-in"><i class="glyphicon glyphicon-plus"></i></button>
                                <button class="btn btn-zoom-out"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <ul class="myMap-items">
                                    <li>
                                        <span class="color-1"></span><a href="#">ДОГОВОРНЫЙ ОТДЕЛ, ОФИС</a>
                                    </li>
                                    <li>
                                        <span class="color-2"></span><a href="#">ТЕХНОЛОГИЧЕСКИЙ ОТДЕЛ</a>
                                    </li>
                                    <li>
                                        <span class="color-3"></span><a href="#">ДОСТРОЙКА И РЕКОНСТРУКЦИЯ</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <ul class="myMap-items">
                                    <li>
                                        <span class="color-4"></span><a href="#">КЛИЕНТСКАЯ СЛУЖБА</a>
                                    </li>
                                    <li>
                                        <span class="color-5"></span><a href="#">ОТДЕЛ РЕКЛАМАЦИИ</a>
                                    </li>
                                    <li>
                                        <span class="color-6"></span><a href="#">ИНДИВИДУАЛЬНОЕ СТРОИТЕЛЬСТВО</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <ul class="myMap-items">
                                    <li>
                                        <span class="color-7"></span><a href="#">ИНЖЕНЕРНЫЕ КОММУНИКАЦИИ</a>
                                    </li>
                                    <li>
                                        <span class="color-8"></span><a href="#">ЗЕМЕЛЬНЫЕ УЧАСТКИ</a>
                                    </li>
                                    <li>
                                        <span class="color-9"></span><a href="#">КРЕДИТНО-СТРАХОВОЙ ОТДЕЛ</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <noindex rel="nofollow">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <ul class="number-list">
                                        <li>
                                            1.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/terem-lk/">Терем ЛК, 6х9 м</a>
                                        </li>
                                        <li>
                                            2.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/bogatyr-2/">Богатырь 2, 7х8 м</a>
                                        </li>
                                        <li>
                                            3.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/terem-1/">Терем К, 6х9 м</a>
                                        </li>
                                        <li>
                                            4.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/varyag-3/">Варяг 3, 8х10 м</a>
                                        </li>
                                        <li>
                                            4а.<a href="/catalog/malye-stroeniya-besedki/besedka-barbekyu/">Беседка, 6х4 м</a>
                                        </li>
                                        <li>
                                            4б.<a href="/catalog/malye-stroeniya-besedki/besedkaа/">Беседка, 3х3 м</a>
                                        </li>
                                        <li>
                                            5.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/viking-d-11/">Викинг 11, 8х9.5 м</a>
                                        </li>
                                        <li>
                                            6.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/kantsler-2/">Канцлер 2, 8х9 м</a>
                                        </li>
                                        <li>
                                            7.<a href="/catalog/besedki-bani-i-sadovye-doma/banya4х4/">Баня, 4х4 м</a>
                                        </li>
                                        <li>
                                            8.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/lider-8k/">Лидер 8, 6х8 м</a>
                                        </li>
                                        <li>
                                            9.<a href="#">Административное здание</a>
                                        </li>
                                        <li>
                                            10.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/svyatogor-k/">Святогор, 7х10 м</a>
                                        </li>
                                        <li>
                                            11.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/viking-d-2/">Викинг 2, 7х9.5 м</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <ul class="number-list">
                                        <li>
                                            12.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/bogatyr-lk-1k/">Богатырь 1, 7х7 м</a>
                                        </li>
                                        <li>
                                            13.<a href="/catalog/sadovye-doma/malysh5k6х5/">Малыш 5, 6х5 м</a>
                                        </li>
                                        <li>
                                            14.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/dobrynya-3/">Добрыня 3, 8.5х10 м</a>
                                        </li>
                                        <li>
                                            15.<a href="/catalog/sadovye-doma/malysh3k5х4/">Малыш 3, 5х4 м</a>
                                        </li>
                                        <li>
                                            16.<a href="/catalog/klassicheskie-dachnye-doma/lider-6/">Лидер 6, 6х6 м</a>
                                        </li>
                                        <li>
                                            17.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/viking-d-4/">Викинг 4, 8.5х9.5 м</a>
                                        </li>
                                        <li>
                                            18.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/imperator-2/">Император 2, 8.5х9 м</a>
                                        </li>
                                        <li>
                                           18а.<a href="/catalog/malye-stroeniya-besedki/besedka+a/">Беседка 4х4 м</a>
                                       </li>
                                        <li>
                                            19.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-150-do-400-kv-m/general-k-200/">Генерал, 14х20.5 м</a>
                                        </li>
                                        <li>
                                            20.<a href="/catalog/originalnye-dachnye-doma-vityaz-odnoetazhnye-i-so-vtorym-svetom/vityaz-4/">Лада 2, 6х9 м</a>
                                        </li>
                                        <li>
                                            21.<a href="#">Административное здание</a>
                                        </li>
                                        <li>
                                            22.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/varyag-d-1/">Варяг 1, 8х8 м</a>
                                        </li>

                                    </ul>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <ul class="number-list">
                                        <!-- <li>
                                            <span>23а.</span><a href="/catalog/malye-stroeniya-besedki/besedka4/">Беседка 3.8х4.8 м</a>
                                        </li> -->
                                        <li>
                                            23.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/bogatyr-lk-3k/">Богатырь 3 ЛК, 7х9 м</a>
                                        </li>
                                        <li>
                                            24.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/premer-d-1/">Премьер 1, 7.5х7.5 м</a>
                                        </li>
                                        <li>
                                            25.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/kantsler-d-1/">Канцлер 1, 8х7.5 м</a>
                                        </li>
                                        <li>
                                            26.<a href="/catalog/besedki-bani-i-sadovye-doma/banya-ladaК6х8/">Баня “Лада”, 6х8 м</a>
                                        </li>
                                        <li>
                                            27.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/kantsler-d-3/">Канцлер 3, 10х10 м</a>
                                        </li>
                                        <li>
                                            27а.<a href="/catalog/malye-stroeniya-besedki/besedka-barbekyo/">Беседка 4х4 м</a>
                                        </li>
                                        <li>
                                            28.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/gertsog/">Герцог, 8.5х9.5 м</a>
                                        </li>
                                        <li>
                                            29.<a href="/catalog/besedki-bani-i-sadovye-doma/banya3х4/">Баня, 3х4 м</a>
                                        </li>
                                        <li>
                                            30.<a href="/catalog/besedki-bani-i-sadovye-doma/banya6х5СМ/">Баня СМ, 6х5 м</a>
                                        </li>
                                        <li>
                                            31.<a href="/catalog/originalnye-dachnye-doma-vityaz-odnoetazhnye-i-so-vtorym-svetom/vityaz-2/">Витязь 2, 6х8 м</a>
                                        </li>
                                        <li>
                                            32.<a href="/catalog/besedki-bani-i-sadovye-doma/banya6х6/">Баня, 6х6 м</a>
                                        </li>
                                        <li>
                                            33.<a href="/catalog/besedki-bani-i-sadovye-doma/banya5х5/">Баня, 5х5 м</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </noindex>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");