<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "faq, часто задоваемые вопросы");
$APPLICATION->SetPageProperty("title", "Часто задаваемые вопросы");
$APPLICATION->SetTitle("Хотелось бы остановить выбор на компании ТеремЪ - Задайте вопрос.");
?>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h1>
                                    Часто задаваемые вопросы
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                                На этой странице Вы можете найти ответы на часто задаваемые нашими клиентами вопросы. Уточнить информацию можно у наших менеджеров на выставочной площадке и по телефону.
                            </p>
                            <br>
                            <br>
                        </div>
                    </div>
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Входит ли в стоимость земельный участок?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq2">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Стоимость земельного участка не входит в стоимость дома. Если у вас пока ещё нет земли, то вы можете приобрести её в нашем проекте «Земля». Мы предлагаем участки по всей Владимирской области. Если предполагается строительство на уже существующем участке, то при заключении договора потребуется документ, подтверждающий право собственности на участок и общегражданский паспорт.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq3">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Занимаетесь ли вы достройкой и реконструкцией существующего строения? И как можно получить расчет?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq3">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да, занимаемся. Консультацию и расчет вы можете получить у менеджеров в нашем офисе по адресу: г. Владимир, ул. Куйбышева, д. 22Б.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="false" aria-controls="faq4">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли использовать материнский капитал при строительстве дома в компании «Теремъ»?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq4">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Добрый день. Вы можете заказать и построить у нас дом за свои средства или с использованием кредитных средств. Далее Вы получаете свидетельство на дом. После этого обращаетесь в Пенсионный фонд для реализации материнского капитала.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq5" aria-expanded="false" aria-controls="faq5">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли заказать дом по индивидуальному проекту?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq5">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Добрый день. Квалифицированные архитекторы разработают проект с учетом всех Ваших пожеланий. Есть возможность привязки проекта к существующему фундаменту. Дополнительную информацию Вы можете получить в Отделе индивидуального строительства.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq51" aria-expanded="false" aria-controls="faq51">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>У меня есть фундамент, можно ли построить на нем дом по собственному проекту?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq51">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Добрый день. Квалифицированные архитекторы разработают проект с учетом всех Ваших пожеланий. Есть возможность привязки проекта к существующему фундаменту. Дополнительную информацию Вы можете получить в Отделе индивидуального строительства.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq6" aria-expanded="false" aria-controls="faq6">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Возможен ли самостоятельный вывоз комплекта дома, материалов, и/или самостоятельная сборка?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq6">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Нет, это невозможно. Мы строительная компания, которая имеет свое производство и строительные бригады. Дома мы строим сами, с использованием современных строительных технологий и качественных материалов. Мы даем гарантию на построенный дом, собранный по собственной технологии и специально обученными специалистами.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq7" aria-expanded="false" aria-controls="faq7">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Компания «Теремъ» занимается инженерными коммуникациями? Входят ли в стоимость дома эти работы?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq7">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>К каждому из проектов домов компании в Отделе инженерных коммуникаций есть план по электропроводке, установке отопительной системы и других инженерных коммуникаций. В нашем офисе (г. Владимир, ул. Куйбышева, д. 22Б) можно заключить отдельные договоры на электромонтажные работы, установку отопления, водоснабжения, канализации и т.д.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq10" aria-expanded="false" aria-controls="faq10">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Выезжают ли ваши специалисты на участок для того, чтобы определить условия застройки и пожелания клиента на месте?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq10">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да, при необходимости выезд специалиста возможен, но только при условии заключения клиентом договора на строительство. Подробности можно узнать в нашем офисе по адресу г. Владимир, ул. Куйбышева, д. 22Б или по тел.: +7 (4922) 49-42-24.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq11" aria-expanded="false" aria-controls="faq11">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли в домах компании «Теремъ» проживать круглогодично?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq11">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да, в домах для постоянного проживания тысячи наших заказчиков живут круглогодично. Эти дома предназначены для строительства на дачных участках и на участках для индивидуального жилищного строительства. Комплектация домов предусматривает его круглогодичное использование с использованием стационарных отопительных агрегатов (АОГВ, калориферов, печей и пр.).</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq12" aria-expanded="false" aria-controls="faq12">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Какие установлены сроки строительства домов?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq12">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Разные дома имеют различные сроки строительства: от 4 дней до 4 месяцев. Подробности вы можете узнать на нашем сайте в разделе «Каталог домов» и по телефону: +7 (4922) 49-42-24.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq13" aria-expanded="false" aria-controls="faq13">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Что такое каркасная технология строительства домов?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq13">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Стены каркасного дома выполняются из доски 35х150 мм. Доска устанавливается с шагом 500 (400 мм для домов для постоянного проживания) мм. Между стойками укладывается утеплитель 150 мм (200 мм для домов для постоянного проживания). С двух сторон стены обшиваются вагонкой (дома для постоянного проживания с наружной стороны обшиваются сайдингом), с наружной стороны - по изоспану А, с внутренней стороны - по изоспану Б. Весь конструктив выполняется из сухого материала.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq14" aria-expanded="false" aria-controls="faq14">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Что входит в базовую комплектацию дома?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq14">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>В базовую комплектацию дома (бани) входит:</p>
                                            <ul>
                                                <li>
                                                    <p>Фундамент;</p>
                                                </li>
                                                <li>
                                                    <p>Весь материал для сборки дома;</p>
                                                </li>
                                                <li>
                                                    <p>Окна, двери, кровля, материал для внутренней отделки;</p>
                                                </li>
                                                <li>
                                                    <p>Доставка;</p>
                                                </li>
                                                <li>
                                                    <p>Работы по установке и сборке дома.</p>
                                                </li>
                                            </ul>
                                            <p>Узнать о каждом конкретном доме более подробно можно в разделе «Каталог домов».</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq15" aria-expanded="false" aria-controls="faq15">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Какую гарантию компания «Теремъ» дает на дома?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq15">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Мы даем гарантию до 25 лет.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                </div>
            </div>
        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
