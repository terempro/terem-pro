<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пакетные предложения");
?>
<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="img-wrapp-offset">
									<img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-13.jpg" class="full-width" alt="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="text-center text-uppercase">
									<h1>
										Пакетные предложения
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>Пакет LIGHT – это отличное предложение для тех, кто заботится о близких, старается дать своей семье все самое необходимое для полноценной жизни и отдыха. Уютный дом, облагороженный земельный участок, забор, защищающий вашу территорию, и многое другое.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>Расширенный вариант данного пакета – это предложение PREMIUM. Если ли вы любите домашний уют наравне с желанием иметь больше свободного пространства, то этот пакет для вас. Шикарный дом, просторная баня, инженерные системы и многое другое. Выгодное предложение для тех, кто стремится к большему.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<section class="content" role="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="flex-reversed">
						<div class="row">
							<div class="col-xs-12 col-md-6 col-sm-12">
								<div class="my-promotion desgin-3 + my-margin">
									<div>
										<a href="#"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-14.jpg"></a>
									</div>
									<div>
										<h4 class="text-uppercase">
											«Усадьба викинг Light»
										</h4>
										<h3 class="text-uppercase">
											<span>3 300 000</span>
										</h3>
									
									</div>
									<a class="all-item-link" href="#"></a>
								</div>
							</div>
							<div class="col-xs-12 col-md-6 col-sm-12">
								<div class="my-promotion desgin-3 + my-margin">
									<div>
										<a href="#"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-15.jpg"></a>
									</div>
									<div>
										<h4 class="text-uppercase">
											«Усадьба канцлер Premium»
										</h4>
										<h3 class="text-uppercase">
											<span>5 700 000</span>
										</h3>
									</div>
									<a class="all-item-link" href="#"></a>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
	</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>