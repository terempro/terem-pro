<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "деревянные дома, строительство деревянных домов, терем, деревянные дома под ключ, деревянные дома москва, стоимость деревянного дома, terem");
$APPLICATION->SetPageProperty("title", "Все об отоплении загородного дома");
$APPLICATION->SetTitle("Все об отоплении загородного дома");
?>

	<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="my-text-title text-center text-uppercase desgin-h1">
									<h1>
										Отопление
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>Компания «Торговый дом ЦСК» предлагает всем желающим услуги по установке систем отопления разных конфигураций в вашем загородном доме. Наши специалисты имеют большой опыт работы с инженерными системами малоэтажных строений. Мы учитываем всевозможные нюансы, работаем только с качественным оборудованием, обязательно даем гарантию и всю необходимую документацию.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h3 class="text-uppercase">Газовое отопление</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-9 col-sm-9">
								<p>Компания «Торговый дом ЦСК» предлагает услуги по установке современного газового котла с закрытой камерой сгорания с принудительным удалением продуктов сгорания. Удаление происходит при помощи вентилятора.</p>
								<img src="/bitrix/templates/.default/assets/img/services/item-9.jpg" class="full-width" alt="">
								<p>Разводка магистральных трубопроводов и стояков системы отопления осуществляется из полипропиленовых армированных труб диаметров 25 мм. Подводка к радиаторам отопления – из полипропиленовых труб диаметров 20 мм. Разводка магистрального трубопровода запроектирована открытым способом по стене вдоль напольного плинтуса. В качестве отопительных приборов запроектированы алюминиевые радиаторы водяного отопления.</p>
								<p><strong>На устанавливаемое компанией «Торговый дом ЦСК» газовое оборудование, выдается проектная документация и гарантия на все виды работ на 1 год. Обращаем ваше внимание, что без проектной документации газовая служба не подключит ваш дом к газовой магистрали.</strong></p>
							</div>
							<div class="col-xs-12 col-md-3 col-sm-3">
								<img src="/bitrix/templates/.default/assets/img/services/item-10.jpg" class="full-width" alt="">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h3 class="text-uppercase">Конвектор</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-9 col-sm-9">
								<p><strong>Конвекторное отопление является самым экономичным среди всех электрических отопительных систем.</strong> Используя конвектор, вы создаете микроклимат в каждой комнате, отапливая только те помещения, которые Вам нужны. В случае поломки (что случается очень редко) без отопления останется лишь одна комната. Оборудование для конвекторного отопления занимает мало места и не требует отдельного помещения.</p>
								<p>Специалисты компании «Теремъ» могут установить в Вашем доме конвекторы. Конвекторы данной фирмы работают абсолютно беззвучно и не сжигают кислород. Мы предлагаем европейское качество и экономичность в эксплуатации!</p>
							</div>
							<div class="col-xs-12 col-md-3 col-sm-3">
								<img src="/bitrix/templates/.default/assets/img/services/item-12.jpg" class="full-width" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>