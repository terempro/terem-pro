<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает достройку и реконструкцию дачных домов и коттеджей");
$APPLICATION->SetPageProperty("keywords", "достройка реконструкция терем, достройка реконструкция, достройка и реконструкиця дачных домов, достройка коттеджей");
$APPLICATION->SetPageProperty("title", "Достройка и реконструкция дачных домов и коттеджей");
$APPLICATION->SetTitle("Достройка и реконструкция дачных домов и коттеджей");
?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix" style="padding: 0 35px 35px;">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <!-- main block of this case -->
                            <div class="dostroy-what-we-do">

                                <div class="item-1"><img src="/bitrix/templates/.default/assets/img/services/dostroika/item-28.jpg" class="full-width" alt=""></div>
                                <div class="item-2">
                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-27.jpg" class="full-width" alt="">
                                    <div class="inner">
                                        <a href="#" class="inner-1" data-item="inner-1">Замена кровли</a>
                                        <a href="#" class="inner-2" data-item="inner-2">Замена дверей</a>
                                        <a href="#" class="inner-3" data-item="inner-3">Пристройка террасы</a>
                                        <a href="#" class="inner-4" data-item="inner-4">Отделка цоколя сайдингом под кирпич</a>
                                        <a href="#" class="inner-5" data-item="inner-5">Наружная отделка сайдингом</a>
                                        <a href="#" class="inner-6" data-item="inner-6">Замена окон</a>
                                        <a href="#" class="inner-7" data-item="inner-7">Дополнительное утепление</a>
                                    </div>
                                    <div class="hiddenEl item-11">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <h3 class="text-uppercase">замена кровли</h3>
                                                <p>Услуга по замене кровли и/или кровельного покрытия с устройством вентиляции подкровельного пространства.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-29.jpg" class="full-width" alt="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p>Подробнее во вкладке <a href="#test" id="some">описание услуг</a></p>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary close-btn"></a>
                                    </div>
                                    <div class="hiddenEl item-21">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <h3 class="text-uppercase">замена дверей</h3>
                                                <p>Замена межкомнатных и входных дверей.</p><br><br><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-30.jpg" class="full-width" alt="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p>Подробнее во вкладке <a href="#">описание услуг</a></p>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary close-btn"></a>
                                    </div>
                                    <div class="hiddenEl item-31">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <h3 class="text-uppercase">пристройка террасы</h3>
                                                <p>Пристройка террас и веранд по типовым или индивидуальным проектам.</p><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-31.jpg" class="full-width" alt="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p>Подробнее во вкладке <a href="#">описание услуг</a></p>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary close-btn"></a>
                                    </div>
                                    <div class="hiddenEl item-41">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <h3 class="text-uppercase">Отделка цоколя  «под кирпич»</h3>
                                                <p>Листовой материал на основе современных полимерных изделий.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-32.jpg" class="full-width" alt="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p>Подробнее во вкладке <a href="#">описание услуг</a></p>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary close-btn"></a>
                                    </div>
                                    <div class="hiddenEl item-51">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <h3 class="text-uppercase">наружная Отделка сайдингом</h3>
                                                <p>Надежное утепление фасада и защита от воздействия внешней среды. </p><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-33.jpg" class="full-width" alt="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p>Подробнее во вкладке <a href="#">описание услуг</a></p>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary close-btn"></a>
                                    </div>
                                    <div class="hiddenEl item-61">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <h3 class="text-uppercase">Замена окон</h3>
                                                <p>Демонтаж старых окон и установка новых, подбор типа и вида окон по желанию клиента.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-34.jpg" class="full-width" alt="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p>Подробнее во вкладке <a href="#">описание услуг</a></p>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary close-btn"></a>
                                    </div>
                                    <div class="hiddenEl item-71">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <h3 class="text-uppercase">Дополнительное утепление</h3>
                                                <p>Дополнительное утепление загородного дома проводится с целью сохранения тепла в доме.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-35.jpg" class="full-width" alt="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p>Подробнее во вкладке <a href="#">описание услуг</a></p>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary close-btn"></a>
                                    </div>

                                </div>
                                <div class="item-3">
                                    <a href="#" class="trigger-btn btn btn-primary" data-item="show-case"><img src="/bitrix/templates/.default/assets/img/arrow-line.png" alt=""></a>
                                </div>
                            </div><!-- main block of this case END-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h1>
                                    достройка и реконструкция
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>Компания «Теремъ» предоставляет широкий спектр услуг по достройке и реконструкции загородных домов. Многолетний опыт специалистов нашей компании лег в основу проведения всех строительных работ.</p>
                            <p>Каждый заказ по достройке и реконструкции &mdash; индивидуален. Разработка проекта, подбор материалов и расчет стоимости производятся с учетом личных пожеланий клиентов. <a href="https://www.youtube.com/watch?v=6LV-4BG83Qs&feature=youtu.be" target="_blank">По ссылке</a> можно посмотреть отзыв одного из заказчиков, которому достраивали сруб из оцилиндрованного бревна.</p>
                        </div>
                    </div>
                    <br/>

                    <!-- <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <img src="squ2.jpg" class="full-width" style="margin-top: 20px;max-width:322px;" alt="">
                        </div>
                        <div class="col-xs-12 col-md-8 col-sm-8 col-lg-6">
                            <div class="new-dostroika">
                                <h4>АНТИКРИЗИС:<span> СКИДКИ ДО 40% НА ДОСТРОЙКУ И РЕКОНСТРУКЦИИ</span></h4>
                                <p class="margin">Держим цены под контролем - на все услуги по достройке и реконструкции предоставляется скидка до 40%. Обновить свой дом теперь можно на максимально выгодных условиях!</p>
                                <p class="margin"><strong>Расчет скидки:</strong></p>
                                <ul>
                                    <li>
                                        <p>при сумме заказа до 500 тысяч рублей размер – до -20%;</p>
                                    </li>
                                    <li>
                                        <p>при сумме заказа от 500 тысяч до 1 000 000 рублей – до -30%;</p>
                                    </li>
                                    <li>
                                        <p>При сумме заказа от 1 000 000 рублей – до -40%.</p>
                                    </li>

                                </ul>

                                <p class="margin" style="padding-right: 30px;">Подробности уточняйте у менеджеров компании «Теремъ», которые готовы ответить на все вопросы по услугам с 10:00 до 20:00 ежедневно.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                            <div class="new-dostroika">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#call"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                            </div>
                        </div>
                    </div> -->
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <img src="/upload/iblock/3c1/3c176eec697875eb66f8e9c7c765056f.jpg" class="full-width" style="margin-top: 20px;max-width:322px;" alt="">
                        </div>
                        <div class="col-xs-12 col-md-8 col-sm-8 col-lg-6">
                            <div class="new-dostroika">
                                <h4>ОБНОВИТЕ СВОЙ ДОМ ВЫГОДНО<br><span>ТОЛЬКО СЕЙЧАС СКИДКИ ДО 40%</span></h4>
                                <p class="margin">
                                    Только сейчас скидки до 40% на широкий спектр услуг по достройке и реконструкции загородных домов.
                                </p>
                                <p class="margin">
                                        Услуга распространяется на все виды и категории работ:
                                </p>
                                <br>
                                <ul>
                                    <li>
                                        <p>Пристройка веранд, террас, крылец</p>
                                    </li>
                                    <li>
                                        <p>Надстройка мансард</p>
                                    </li>
                                    <li>
                                        <p>Отделка цоколя фундамента</p>
                                    </li>
                                    <li>
                                        <p>Наружная отделка </p>
                                    </li>
                                    <li>
                                        <p>Внутренняя отделка пола, стен и потолка</p>
                                    </li>
                                    <li>
                                        <p>Замена кровли</p>
                                    </li>
                                    <li><p>Установка водосточной системы </p></li>
                                    <li><p>Замена окон, дверей, межэтажных лестниц </p></li>
                                    <li><p>Обустройство парной </p></li>
                                    <li><p>Демонтажные работы </p></li>
                                </ul>
                                <p class="margin">
                                    Бесплатный выезд специалиста: стоимость выезда компенсируется при заключение договора.
                                </p>
                                <br>
                                <p class="margin">
                                    Подробности уточняйте у менеджеров компании «Теремъ», которые готовы ответить на все вопросы по услугам с 10:00 до 20:00 у менеджеров компании «ТеремЪ» по телефону или на выставочной площадке.
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                            <!--							<div class="new-dostroika">
                                                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#call"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                                                                                    </div>-->
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <img src="squ3.jpg" class="full-width" style="margin-top: 20px;max-width:322px;" alt="">
                        </div>
                        <div class="col-xs-12 col-md-8 col-sm-8 col-lg-6">
                            <div class="new-dostroika">
                                <h4><span>ДОСТРОЙКА И РЕКОНСТРУКЦИЯ</span> СТАЛИ ЕЩЕ УДОБНЕЕ!</h4>
                                <p class="margin"><strong>Достройка и реконструкция стали еще удобнее и выгоднее!</strong><br/>Теперь Вы можете взять в банке «ОТП банк» кредит суммой до 500 000 рублей без оформления предварительного договора. Процедура займет всего 15 минут, от Вас потребуется только паспорт.</p>
                                <p class="margin"><strong>Услуга распространяется на все виды и категории работ:</strong></p><br/>
                                <ul>
                                    <li>
                                        <p>Демонтаж деревянных строений;</p>
                                    </li>
                                    <li>
                                        <p>Устройство фундамента для пристройки;</p>
                                    </li>
                                    <li>
                                        <p>Индивидуальная реконструкция строений;</p>
                                    </li>
                                    <li>
                                        <p>Достройка существующих строений;</p>
                                    </li>
                                    <li>
                                        <p>Внешняя и внутренняя отделка;</p>
                                    </li>
                                    <li>
                                        <p>Бани и беседки по индивидуальному проекту.</p>
                                    </li>
                                </ul>
                                <p class="margin">Обновите свой дом на максимально выгодных условиях, воспользовавшись дополнительной скидкой от компании "Теремъ" на все услуги по достройки и реконструкции. Подробнее о расчете скидки в разделе <a href="http://www.terem-pro.ru/promotion/">акции.</a> </p>
                                <br/><p class="margin" style="padding-right: 30px;">Подробности уточняйте у менеджеров компании «Теремъ», которые готовы ответить на все вопросы по услугам с 10:00 до 20:00 у менеджеров компании «ТеремЪ» по телефону или на выставочной площадке.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                            <!--							<div class="new-dostroika">
                                                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#call"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                                                                                    </div>-->
                        </div>
                    </div>
                    <br/>

                    <!-- <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <img src="saiding.jpg" class="full-width" style="margin-top: 20px;max-width:322px;" alt="">
                        </div>
                        <div class="col-xs-12 col-md-8 col-sm-8 col-lg-6">
                            <div class="new-dostroika">
                                <h4><span>САЙДИНГ</span><br>СПЕЦИАЛЬНОЕ ПРЕДЛОЖЕНИЕ – ВСЕГО ЗА <span>1 300 руб. за м2</span></h4>
                                <p class="margin"></p>
                                <p class="margin"><strong>Услуга включает в себя:</strong></p>
                                <ul>
                                    <li>
                                        <p>Выравнивающая обрешетка 20х100;</p>
                                    </li>
                                    <li>
                                        <p>Ветровлагозащитная мембрана;</p>
                                    </li>
                                    <li>
                                        <p>Стеновые панели сайдинга;</p>
                                    </li>
                                    <li>
                                        <p>Комплектующие;</p>
                                    </li>
                                    <li>
                                        <p>Отделка карнизов;</p>
                                    </li>
                                    <li>
                                        <p>Отливы подоконные;</p>
                                    </li>
                                    <li>
                                        <p>Отливы фундаментные.</p>
                                    </li>
                                </ul>
                                <p class="margin">Предложение для домов построенных компанией Теремъ.</p>

                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                            <div class="new-dostroika">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#call"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                            </div>
                        </div>
                        <br/>
                    </div>
                    <br/> -->

                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <img src="squ.jpg" class="full-width" style="margin-top: 20px;max-width:322px;" alt="">
                        </div>
                        <div class="col-xs-12 col-md-8 col-sm-8 col-lg-6">
                            <div class="new-dostroika">
                                <h4>ЗАМЕНА КРОВЛИ ВСЕГО ОТ 1150 Р/КВ.М<br><span>ВОДОСТОЧНАЯ СИСТЕМА В ПОДАРОК!</span></h4>
                                <p class="margin">
                                    Специальное предложение от компании «Теремъ»! Обновите или отремонтируйте кровлю – от 1150 р/ кв.м. Каждому клиенту водосточная система в подарок!
                                </p>
                                <p class="margin"><strong>Услуга включает в себя:</strong></p>
                                <ul>
                                    <li>
                                        <p>Автономное проживание бригады;</p>
                                    </li>
                                    <li>
                                        <p>Срок работы от 3х дней в зависимости от масштабов;</p>
                                    </li>
                                    <li>
                                        <p>Все комплектующие кровли включены в стоимость.</p>
                                    </li>
                                </ul>
                                <p class="margin" style="padding-right: 30px;">Подробности уточняйте у менеджеров компании «Теремъ» с 10:00 до 20:00 ежедневно.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                            <div class="new-dostroika">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#call"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                            </div>
                        </div>
                        <br/>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <img src="veranda.jpg" class="full-width" style="margin-top: 20px;max-width:322px;" alt="">
                        </div>
                        <div class="col-xs-12 col-md-8 col-sm-8 col-lg-6">
                            <div class="new-dostroika">
                                <h4>ВЕРАНДА «ПОД КЛЮЧ» ВСЕГО ЗА 114 000 РУБЛЕЙ.<br><span>ПЛАСТИКОВЫЕ ОКНА В ПОДАРОК!</span></h4>
                                <p>
                                  Увеличьте полезную площадь своего дома!
                                </p>
                                <p>
                                  Пристройка веранды 2х5 под ключ всего за 114&nbsp;000 рублей. Пластиковые окна в подарок. Веранду можно использовать, как дополнительную комнату, кухню или бойлерную.
                                </p>
                                <p>
                                  Подробности акции уточняйте у менеджеров компании «Теремъ» по телефону или на территории выставочного комплекса. Ежедневно с 10:00 до 20:00 ждем вас по адресу: г. Москва, ул. Зеленодольская, вл.42.
                                </p>
                            </div>
                        </div>
                        <!-- <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                            <div class="new-dostroika">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#call"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                            </div>
                        </div> -->
                        <br/>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <img src="terrace.jpg" class="full-width" style="margin-top: 20px;max-width:322px;" alt="">
                        </div>
                        <div class="col-xs-12 col-md-8 col-sm-8 col-lg-6">
                            <div class="new-dostroika">
                                <h4>ПРИСТРОЙКА ТЕРРАСЫ «ПОД КЛЮЧ» ВСЕГО ЗА&nbsp;81&nbsp;000 РУБЛЕЙ.</h4>
                                <p>
	                                 Специальное предложение компании «Теремъ» &mdash; пристройка террасы&nbsp;2&times;5 «под&nbsp;ключ» всего за&nbsp;81&nbsp;000 рублей.
                                 </p>
                                <p>
                                	Обновите свой дом на&nbsp;максимально выгодных условиях вместе с&nbsp;отделом Достройки и&nbsp;Реконструкции.
                                </p>
                                <p>
                                	 Подробности акции уточняйте у&nbsp;менеджеров компании «Теремъ» по&nbsp;телефону или&nbsp;на&nbsp;территории выставочного комплекса. Ежедневно с&nbsp;10:00 до&nbsp;20:00 ждем вас по&nbsp;адресу: г.&nbsp;Москва, ул.&nbsp;Зеленодольская, вл.&nbsp;42.
                                </p>
                            </div>
                        </div>
                        <!-- <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                            <div class="new-dostroika">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#call"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                            </div>
                        </div> -->
                        <br/>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <img src="siding.jpg" class="full-width" style="margin-top: 20px;max-width:322px;" alt="">
                        </div>
                        <div class="col-xs-12 col-md-8 col-sm-8 col-lg-6">
                            <div class="new-dostroika">
                                <h4><span>ОТДЕЛКА САЙДИНГОМ</span><br>ОТ 940 Р/ КВ.М + УТЕПЛЕНИЕ ДОМА СО СКИДКОЙ 50%</h4>
                                <p>Выгодное предложение от отдела Достройки и Реконструкции! Отделка дома сайдингом стала еще доступнее – теперь от 940 р/кв.м. Дополнительно утеплите свой дом со скидкой 50%!
                                <ul class="listWithRedArrows" style="font-size: inherit">
                                  <li> Оперативная работа от 4х дней;
                                  <li> Автономная работа бригады;
                                  <li> Гарантия на выполненные работы.
                                </ul>
                                <p>Подробности проведения акции уточняйте у менеджеров компании «Теремъ» по телефону или на выставке в Кузьминках. Ждем вас ежедневно с 10:00 до 20:00!
                            </div>
                        </div>
                        <!-- <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                            <div class="new-dostroika">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#call"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                            </div>
                        </div> -->
                        <br/>
                    </div>
                    <br/>
                </div>
            </div>
        </div>

    </div>
</section>
<!--tabs froms,contacts,reviews and etc.-->
<section class="content white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white + my-margin">
                    <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-center && bg-grey && padding-navs-20 && padding-tabs-30-30 ))">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">ОНЛАЙН ЗАЯВКА</a>
                            </li>
                            <li role="presentation">
                                <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">ОПИСАНИЕ УСЛУГ</a>
                            </li>
                            <li role="presentation">
                                <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">С ЧЕГО НАЧАТЬ?</a>
                            </li>
                            <li role="presentation">
                                <a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">РЕАЛИЗОВАННЫЕ ПРОЕКТЫ</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!--one item tab-->
                            <div role="tabpanel" class="tab-pane active" id="tab1">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <p>
                                            В предложенной ниже форме Вы можете выбрать интересующую Вас услугу. Ниже появится окно с расширенными характеристиками, где необходимо отметить галочками нужные опции. Обращаем Ваше внимание, что выбирать можно столько услуг, сколько Вам требуется. Все, что Вы выбрали, будет отражено в строке «Выбрано» в самом конце данной формы.
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <!--big form-->
                                        <form class="form-send comunication" name="send" data-toggle="validator" action="/include/request.php" method="post" role="form" data-form="send" data-roistat="Достройка">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <select class="form-control myselect" data-item="select" id="">
                                                            <option value="0">Демонтаж деревянных строений (без вывоза мусора)</option>
                                                            <option value="1">Устройство фундамента для пристройки</option>
                                                            <option value="2">Индивидуальная реконструкция строений</option>
                                                            <option value="3">Достройка существующих строений</option>
                                                            <option value="4">Отделка</option>
                                                            <option value="5">Все услуги</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--select who equial to 0 [data-item="form-0"]-->
                                            <div class="form-group currently active" data-item="form-0">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="my-form-title-red text-center text-uppercase">
                                                            <h3>демонтаж деревянных строений (без вывоза мусора)</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group currently active" data-item="form-0">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox1" name="chk[]" value="демонтаж пристройки">
                                                            <label for="checkbox1">
                                                                демонтаж пристройки
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox2" name="chk[]" value="демонтаж перегородки">
                                                            <label for="checkbox2">
                                                                демонтаж перегородки
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox3" name="chk[]" value="демонтаж обшивки (внутренней или наружной)">
                                                            <label for="checkbox3">
                                                                демонтаж обшивки (внутренней или наружной)
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox4" name="chk[]" value="демонтаж мансарды">
                                                            <label for="checkbox4">
                                                                демонтаж мансарды
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox5" name="chk[]" value="демонтаж кровельного покрытия">
                                                            <label for="checkbox5">
                                                                демонтаж кровельного покрытия
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--select who equial to 0 [data-item="form-0"] END-->
                                            <!--select who equial to 1 [data-item="form-1"]-->
                                            <div class="form-group currently" data-item="form-1">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="my-form-title-red text-center text-uppercase">
                                                            <h3>Устройство фундамента для пристройки</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group currently" data-item="form-1">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox6" name="chk[]" value="свайно-винтовой">
                                                            <label for="checkbox6">
                                                                свайно-винтовой
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox7" name="chk[]" value="опоры из блоков">
                                                            <label for="checkbox7">
                                                                опоры из блоков
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox8" name="chk[]" value="монолитно-армированная лента">
                                                            <label for="checkbox8">
                                                                монолитно-армированная лента
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox9" name="chk[]" value="свой фундамент">
                                                            <label for="checkbox9">
                                                                свой фундамент
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox10" name="chk[]" value="бурозаливной с ростверком">
                                                            <label for="checkbox10">
                                                                бурозаливной с ростверком
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--select who equial to 1 [data-item="form-1"] END-->
                                            <!--select who equial to 2 [data-item="form-2"]-->
                                            <div class="form-group currently" data-item="form-2">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="my-form-title-red text-center text-uppercase">
                                                            <h3>индивидуальная реконструкция строений</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group currently" data-item="form-2">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox11" name="chk[]" value="замена окон">
                                                            <label for="checkbox11">
                                                                замена окон
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox12" name="chk[]" value="замена дверей">
                                                            <label for="checkbox12">
                                                                замена дверей
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox13" name="chk[]" value="замена лестницы">
                                                            <label for="checkbox13">
                                                                замена лестницы
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox14" name="chk[]" value="замена кровли">
                                                            <label for="checkbox14">
                                                                замена кровли
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">


                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox15" name="chk[]" value="замена и усиление конструктивной части дома">
                                                            <label for="checkbox15">
                                                                замена и усиление конструктивной части дома
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox16" name="chk[]" value="установка водосточной системы">
                                                            <label for="checkbox16">
                                                                установка водосточной системы
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox17" name="chk[]" value="утепление пола, стен, перекрытий">
                                                            <label for="checkbox17">
                                                                утепление пола, стен, перекрытий
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--select who equial to 2 [data-item="form-2"] END-->
                                            <!--select who equial to 3 [data-item="form-3"]-->
                                            <div class="form-group currently" data-item="form-3">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="my-form-title-red text-center text-uppercase">
                                                            <h3>достройка существующих строений</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group currently" data-item="form-3">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox18" name="chk[]" value="пристройка веранды, террасы или комбинированной пристройки по индивидуальному проекту">
                                                            <label for="checkbox18">
                                                                пристройка веранды, террасы или комбинированной пристройки по индивидуальному проекту
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox19" name="chk[]" value="строительство полноценного второго этажа">
                                                            <label for="checkbox19">
                                                                строительство полноценного второго этажа
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox20" name="chk[]" value="cтроительство мансардного этажа">
                                                            <label for="checkbox20">
                                                                cтроительство мансардного этажа
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox21" name="chk[]" value="возведение эркерной пристройки">
                                                            <label for="checkbox21">
                                                                возведение эркерной пристройки
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div><!--select who equial to 3 [data-item="form-3"] END-->
                                            <!--select who equial to 4 [data-item="form-4"] -->
                                            <div class="form-group currently" data-item="form-4">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="my-form-title-red text-center text-uppercase">
                                                            <h3>отделка</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group currently" data-item="form-4">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox22" name="chk[]" value="внутренняя отделка">
                                                            <label for="checkbox22">
                                                                внутренняя отделка
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox23" name="chk[]" value="внешняя отделка">
                                                            <label for="checkbox23">
                                                                внешняя отделка
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox24" name="chk[]" value="внешняя покраска">
                                                            <label for="checkbox24">
                                                                внешняя покраска
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="checkbox bordered">
                                                            <input type="checkbox" id="checkbox25" name="chk[]" value="деревянная отделка бань">
                                                            <label for="checkbox25">
                                                                деревянная отделка бань
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div><!--select who equial to 4 [data-item="form-4"] END-->

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="selected-items">
                                                            <div class="inner"></div>
                                                            <textarea name="selected-items" data-item="checked-items"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <p class="my-form-subtitle"><strong>Контактные данные</strong></p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <p class="my-label">Ваше имя</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <input type="text" name="NameClient" class="form-control my-input" placeholder="Ваше имя" required>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <p class="my-label">Телефон для связи</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <input type="text"  name="TelephoneClient" class="form-control my-input" placeholder="8 920 000 00 00" required>
                                                                <input type="hidden" name="id_request" value="dostroyka"/>
                                                                <input type="hidden" name="request_type" value="2">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div><div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <p><strong>Внимание!</strong></p>
                                <p>Отправляя форму Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">Политикой обработки данных.</a></p>
                            </div>
                        </div>
                                            <div class="row">

                                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                                                    <button type="submit" name="from" value="dostroika" class="btn btn-danger" onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;">ОТПРАВИТЬ ЗАЯВКУ</button>
                                                    <script>
                                                        $(document).ready(function () {
                                                            var today = new Date();
                                                            $('#order_id').val(today);
                                                        });

                                                    </script>
                                                    <input type="hidden" id="order_id" name="order" data-date="<?=date("D M  d Y H:m:s")?>" value=""/>
                                                </div>
                                            </div>
                                        </form><!--big form END-->
                                    </div>
                                </div>
                            </div><!--one item tab END-->
                            <!--one item tab-->
                            <div role="tabpanel" class="tab-pane" id="tab2">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <p>
                                            В предложенной ниже форме Вы можете выбрать интересующую Вас услугу. Ниже появится окно с расширенными характеристиками, где необходимо отметить галочками нужные опции. Обращаем Ваше внимание, что выбирать можно столько услуг, сколько Вам требуется. Все, что Вы выбрали, будет отражено в строке «Выбрано» в самом конце данной формы.
                                        </p>
                                    </div>
                                </div>
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>Демонтаж деревянных строений</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse1">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>
                                                            Самостоятельный демонтаж деревянных строений может обернуться обрушением, поэтому не стоит рисковать и пытаться своими силами выполнить эту работу. Специалисты компании «Теремъ» при проведении демонтажа обязательно соблюдают правила техники безопасности и четко следуют технологиям разбора деревянных объектов.
                                                        </p>
                                                        <p>Вы можете заказать демонтаж следующих строений:</p>
                                                        <ul id="test">
                                                            <li>
                                                                <p><i>Демонтаж пристройки</i></p>
                                                            </li>
                                                            <li>
                                                                <p><i>Демонтаж мансарды</i></p>
                                                            </li>
                                                            <li>
                                                                <p><i>Демонтаж перегородки</i></p>
                                                            </li>
                                                            <li>
                                                                <p><i>Демонтаж кровельного покрытия</i></p>
                                                            </li>
                                                            <li>
                                                                <p><i>Демонтаж обшивки (внутренней или наружной)</i></p>
                                                            </li>
                                                        </ul>
                                                        <p>Обращаем Ваше внимание, демонтаж проводится без вывоза мусора.</p>
                                                        <p>Любой объект разбирается до основания. Если в дальнейшем запланировано строительство, то после вывоза мусора останется чистый участок, где сразу же можно начинать строительные работы.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>Устройство фундамента для пристройки</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse2">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>
                                                            Если Вы решили сделать пристройку к своему дому, то первое с чем Вы столкнетесь – необходимость выбора фундамента. Компания «Теремъ» предлагает возможность заказать один из следующих видов фундамента:
                                                        </p>
                                                        <p>Вы можете заказать демонтаж следующих строений:</p>
                                                        <ul>
                                                            <li>
                                                                <p><i>Свайно-винтовой</i></p>
                                                            </li>
                                                            <li>
                                                                <p><i>Опоры из блоков</i></p>
                                                            </li>
                                                            <li>
                                                                <p><i>Монолитно-армированная лента</i></p>
                                                            </li>
                                                            <li>
                                                                <p><i>Бурозаливной с ростверком</i></p>
                                                            </li>
                                                        </ul>
                                                        <p>Наши специалисты помогут Вам с выбором, бесплатно проконсультируют и ответят на все интересующие вопросы.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>Индивидуальная реконструкция строений</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse3">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <ul>
                                                            <li>
                                                                <p><i>Замена окон</i></p>
                                                                <p>Услуга по демонтажу старых окон и установка новых, подбор типа и вида окон по желанию клиента. Деревянные – недорогие, экологически чистые окна, требующие определенного ухода. Возможно установка деревянного стеклопакета. Пластиковые окна - долговечны, прочны, легки в уходе. Важный момент – монтаж, которым должны заниматься профессионалы с большим опытом работы.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Замена дверей</i></p>
                                                                <p>Замена межкомнатных и входных дверей. Популярный вариант - лаковые филенчатые c хорошей звукоизоляцией, высокой степенью прочности и надежности. При выборе входных дверей надежным и солидным вариантом будут металлические двери, которые станут защитой Вашего дома, прослужат долгие годы и удивят своей устойчивостью к перепадам температур, коррозии и огню. Также наши специалисты привезут и установят входные и межкомнатные двери по Вашему индивидуальному заказу.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Замена лестницы</i></p>
                                                                <p>Услуга по замене деревянной лестницы. Выполнение услуги начинается с демонтажа старой лестницы. Новую лестницу привозят на участок в разобранном виде, все детали – ступени, перила, балясины пронумерованы. После сборки лестницы начинается этап монтажа. Лестница устанавливается и надежно закрепляется.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Замена кровли</i></p>
                                                                <p>Услуга по замене кровли и/или кровельного покрытия с устройством вентиляции подкровельного пространства. Реконструкция всей крыши, полная или частичная замена покрытия, грамотный выбор кровельного материала ведущих производителей. Строительство мансардных этажей с возможностью выбора типа крыши: скатной (односкатной, двускатной и т.д.), плоской, ломаной.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Замена и усиление конструктивной части дома</i></p>
                                                                <p>Устройство новых перегородок, перепланировка помещения, а также устройство арок, установка несущих балок. Опытные специалисты выполнят все работы по замене и усилению конструктивной части дома в кратчайшие сроки и с использованием высококачественных строительных материалов.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Установка водосточной системы</i></p>
                                                                <p>Дополнительное утепление загородного дома проводится с целью сохранения тепла в доме. Стены можно утеплить с наружной стороны и внутри строения. Чаще всего используется минеральная вата известного бренда KNAUF, в плитах или рулонах.</p>
                                                                <p>К работе над каждым проектом выделяется профессиональная бригада и специалист для консультаций и оперативного решения возникших вопросов.</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>Достройка существующих строений</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse4">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <ul>
                                                            <li>
                                                                <p><i>Пристройка террасы, веранды или комбинированной пристройки по индивидуальному проекту</i></p>
                                                                <p>Пристройка террас и веранд по типовым или индивидуальным проектам с учетом особенностей основного строения. Такой вариант модернизации дома является оптимальным решением для увеличения площади строения и поможет придать дому неповторимость, архитектурную завершенность.При разработке проекта наши специалисты учитывают тип и глубину залегания фундамента, состояние стен и кровли. Декоративная отделка производится в соответствии с обликом основного здания.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Строительство полноценного второго этажа</i></p>
                                                                <p>Полноценный второй этаж - это помещение, площадь которого равна площади первого этажа, а потолок выполнен без скосов для кровли. Специалисты компании выполнят строительство второго этажа с учетом всех норм и правил и с использованием современных материалов.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Строительство мансардного этажа</i></p>
                                                                <p>Мансардный этаж имеет ряд неоспоримых преимуществ. Прежде всего, это дополнительная полезная площадь, которую можно оборудовать, к примеру, под спальню, детскую или комнату отдыха. Высота стен мансарды до начала ската составляет не более 1,5 метров.</p>
                                                                <p>Мансардный этаж может быть нескольких конфигураций:</p>
                                                                <ul>
                                                                    <li>
                                                                        <p><i>С двухскатной крышей</i></p>
                                                                    </li>
                                                                    <li>
                                                                        <p><i>С ломанной крышей</i></p>
                                                                    </li>
                                                                    <li>
                                                                        <p><i>Полуторный этаж</i></p>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <p><i>Возведение эркерной пристройки</i></p>
                                                                <p>Услуга позволяет увеличить площадь основного здания. Дополнительное пространство чаще всего пристраивают к кухням, столовым, гостиным и даже спальням. Используются современные материалы и технологии.</p>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>Отделка</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse5">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <ul>
                                                            <li>
                                                                <p><i>Внутренняя отделка</i></p>
                                                                <p>Вы можете заказать внутреннюю отделку строения с использованием следующих материалов: вагонка, фальшбрус, блокхаос, гипсокартон, ОСП, пластиковые панели, доска пола, ламинат.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Внешняя отделка</i></p>
                                                                <p>Услуга выполняется с использованием (по выбору клиента) следующих современных материалов: сайдинг, вагонка, фасадные панели, клинкерная плитка, цокольный сайдинг, декоративные элементы.</p>
                                                            </li>
                                                            <li>
                                                                <p><i>Внешняя покраска</i></p>
                                                                <p>Обработка и защита наружных стен от внешнего воздействия окружающей среды.</p>

                                                            </li>
                                                            <li>
                                                                <p><i>Деревянная отделка бань</i></p>
                                                                <p>Чистовая отделка всех помещений, включая парную.</p>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                            </div><!--one item tab END-->
                            <div role="tabpanel" class="tab-pane" id="tab3">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <h2>4 шага к успешному строительству</h2>
                                        <!-- <ul class="list-with-count"> -->
                                        <ul>
                                            <li>
                                                <p><strong>ШАГ I.</strong> Предварительный звонок</p>
                                            </li>
                                            <li>
                                                <p><strong>ШАГ II.</strong> Визит на выставочную площадку. На первую встречу с сотрудниками отдела желательно привезти информацию о Вашем строении: фотографии, план (от руки или БТИ). Специалист определит возможность проведения работ, сроки и предварительную стоимость</p>
                                            </li>
                                            <li>
                                                <p><strong>ШАГ III.</strong> Выезд инженера-замерщика по итогам первой встречи для детального осмотра и замеров объекта</p>
                                            </li>
                                            <li>
                                                <p><strong>ШАГ IV.</strong> Составления технического задания, проектирование и расчет, заключение договора</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!--one item tab END-->
                            <div role="tabpanel" class="tab-pane" id="tab4">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <p>этом разделе мы собрали для Вас реальные истории наших клиентов. Фотографии помогут Вам ясно представить, как выглядели дома до обращения в компанию «Теремъ» и какими они стали после достройки и/или реконструкции. А в описании Вы найдете информацию о применяемых отделочных материалах и этапах проводимых работ. Раздел будет регулярно пополняться интересными историями и иллюстрациями.</p>
                                    </div>
                                </div>
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>Из небольшого дома в особняк</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse6">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>Отдел достройки и реконструкции компании «Теремъ» занимается даже самым сложными и масштабными случаями. В этот раз специалистам предстояло работать над домом из бревна с двумя пристройками из бруса семьи Голиковых. Дом был целиком обшит сайдингом, поэтому наружная отделка не требовалась. Зато было необходимо провести демонтаж крыши с перекрытием на двух пристройках и кровельного покрытия дома.</p>
                                                        <p>После окончания подготовительных работ квалифицированные строители занялись наращиванием стен пристроек до уровня перекрытий первого этажа и надстройкой второго этажа над существующими пристройками. Но впереди еще было утепление элементов всего строения: перекрытий, стен, кровли. Завершающим штрихом стала замена кровли дома и крыльца, а также внутренняя отделка и установка водосточной системы. Как поясняют специалисты отдела достройки и реконструкции, работа была действительно масштабной, зато теперь срок службы дома увеличился в разы.</p>
                                                        <p>В процессе работы были использованы только высококачественные строительные материалы: металлочерепица «Каскад», сайдинг Encore, водосточная система Nicoll, теплоизоляционный материал Knauf.</p>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-1.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-1.jpg" alt="" class="full-width">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-2.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-2.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-3.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-3.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-4.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-4.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>когда над проектом работают профессионалы</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse7">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>Команде отдела достройки и реконструкции компании «Теремъ» предстояло проделать огромную работу – старенький дом из бруса превратить в современный, теплый и красивый коттедж.</p>
                                                        <p>Работа началась с демонтажа веранды и террасы. После этого началось устройство ленточно-монолитного фундамента, на котором затем и была возведена утепленная каркасная веранда с мансардой и террасой.В самом же доме была произведена замена всех окон, дверей, установлена лестница и водосточная система. Профессиональные строители также сделали наружную отделку всего дома. В отделе достройки и реконструкции пояснили, что для проведения работ использовались качественные и проверенные материалы: металлочерепица «Монтеррей», теплоизоляционный материал Knauf, а наружная отделка была выполнена с помощью блокхауса.</p>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-5.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-5.jpg" alt="" class="full-width">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-6.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-6.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-7.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-7.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-8.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-8.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>достройка и реконструкция кирпичного дома</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse8">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>Компания «Теремъ» занимается достройкой и реконструкцией без преувеличения всех видов загородных домов. К каждому объекту мы можем найти свой подход, в том числе и к кирпичным домам. В этот раз специалисты компании взялись за достройку и реконструкцию дома семьи Иваненко.</p>
                                                        <p>Был проведен демонтаж кровельного покрытия веранды и усиление ее стен. Профессиональные строители увеличили высоту существующего фундамента под пристройку столбами из блоков. После чего была проведена пристройка каркасной веранды с открытой террасой и установка лестницы и ограждения.В отделе достройки и реконструкции компании «Теремъ» пояснили, что снаружи веранда была обшита блокхаусом, также в кратчайшие сроки была произведена внутренняя отделка стен и установка окон, а кровлю строители положили оцинкованным профнастилом. </p>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-9.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-9.jpg" alt="" class="full-width">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-10.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-10.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-11.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-11.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-12.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-12.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>достройка и реконструкция дома и гаража</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse9">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>Перед специалистами отдела достройки и реконструкции встала задача построить новый мансардный этаж и заменить кровлю гаража. Работать предстояло с кирпичным домом современной постройки.</p>
                                                        <p>Работы начались с демонтажа мансарды до плит перекрытия. После чего строители занялись возведением утепленного теплоизоляционным материалом Knauf мансардного этажа и заменой кровли гаража на мягкую кровлю СТ-20 Cedar Brown.</p>
                                                        <p>Для внутренней отделки всего помещения мансарды использовался блокхаус. Мы также установили мансардные окна и обшили фронтоны фасадными панелями, - рассказали в отделе достройки и реконструкции.Также на доме заказчика строители установили снегоразрезатели и водосточную систему Nicoll.</p>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-13.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-13.jpg" alt="" class="full-width">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-14.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-14.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-15.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-15.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-16.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-16.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>вторая жизнь старого дома</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="collapse10">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>Семья Новиковых обратилась в компанию «Теремъ» в надежде дать новую жизнь своему дому с верандой из бревна. За работу взялся отдел достройки и реконструкции.</p>
                                                        <p>
                                                            - Работы нам предстояло много. Необходимо было провести  демонтаж старого кровельного покрытия, затем заменить его новым. Также нам нужно было установить новые окна, двери, лестницу, водосточную систему, - говорит технолог отдела достройки и реконструкции. </p>
                                                        <p>С поставленными заказчиком задачами строители и специалисты отдела справились на «ура» и выполнили все это в кратчайшие сроки.</p>
                                                        <p>Но самой масштабной частью всей реконструкции стала пристройка утепленной веранды с полуторным этажом и открытой террасой. Перед началом работы профессиональные строители провели монтаж ленточного, монолитно-армированного фундамента под пристройку.</p>
                                                        <p>- Важный момент – это материалы, которые мы использовали в процессе работы. Компания «Теремъ» пользуется услугами только проверенных поставщиков и использует исключительно высококачественные материалы, - пояснили в отделе достройки и реконструкции.</p>
                                                        <p>Поэтому для наружной отделки был использован сайдинг Encore, материалом кровли стала металлочерепица «Каскад», в качестве теплоизоляционного материала использовалась продукция компании Knauf, а водосточная система – фирмы «ТехноНиколь». </p>
                                                        <p>Семья Новиковых осталась довольна, а в отделе достройки и реконструкции компании «Теремъ» говорят, что вот так дать новую жизнь можно даже самой старой постройке. Главное – обратиться в проверенную компанию.</p>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-6 col-sm-6">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-17.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-17.jpg" alt="" class="full-width">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-6 col-sm-6">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-18.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-18.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>как гараж помог увеличить площадь дома</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-11">
                                                <div class="collapse my-collapse" id="collapse11">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>Специалисты отдела достройки и реконструкции компании «Теремъ» взялись увеличить площадь кирпичного дома с помощью пространства над пристроенным гаражом.</p>
                                                        <p>
                                                            Прежде всего, был выполнен демонтаж крыши гаража. Затем профессиональные строители надстроили полноценный утепленный этаж и установили там окна. Снаружи был произведен монтаж водосточной системы ТехноНиколь и проведена отделка сайдингом Sayga.</p>
                                                        <p>С поставленными заказчиком задачами строители и специалисты отдела справились на «ура» и выполнили все это в кратчайшие сроки.</p>
                                                        <p>Помимо этого была заменена кровля на самом доме и существующем крыльце.</p>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-19.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-19.jpg" alt="" class="full-width">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-20.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-20.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-21.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-21.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-22.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-22.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>брусовой дом получил вторую жизнь</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-11">
                                                <div class="collapse my-collapse" id="collapse12">
                                                    <div class="( my-collapse-content + design-main + no-padding )">
                                                        <p>В отдел достройки и реконструкции компании «Теремъ» обратился заказчик с просьбой провести ряд важных работ в его загородном доме из бруса. Для наших специалистов нет ничего невозможного, и уже очень скоро профессиональные строители начали демонтаж веранды, крыльца, кровельного покрытия дома.</p>
                                                        <p>
                                                            Сразу после окончания демонтажа начался монтаж ленточного монолитно-армированного фундамента под пристройку. На фундамент установили утепленную веранду с мансардой и крыльцо с односкатной кровлей. С помощью теплоизоляционного материала Knauf строители сделали утепление пола, стен и перекрытий дома. Последним штрихом стала замена оконных блоков и установка на все окна рольставней с карданным приводом. - Для кровли мы использовали металлочерепицу «Каскад», наружная отделка дома и веранды - сайдинг «Mitten», а установленная водосточная система – фирмы ТехноНиколь, - пояснили в отделе достройки и реконструкции.</p>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-23.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-23.jpg" alt="" class="full-width">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-24.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-24.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-25.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-25.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <a class="fancybox" rel="group1" href="/bitrix/templates/.default/assets/img/services/dostroika/item-26.jpg">
                                                                    <img src="/bitrix/templates/.default/assets/img/services/dostroika/item-26.jpg" alt="" class="full-width" title="" oncontextmenu="return false">
                                                                </a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--one item END-->
                            </div><!--one item tab END-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--tabs froms,contacts,reviews and etc. END-->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
