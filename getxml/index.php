<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if (!CModule::IncludeModule("iblock")) die();

const VERIFICATION_KEY = 'c98f28f8aed3447853b6bad8f9af6e75bff71b203444d269dea060582887fef0';
const ADS_IBLOCK = 59;

if (!empty($_POST))
{
    if (!isset($_POST['token']) || $_POST['token'] != VERIFICATION_KEY)
    {
        echo 'Неверный ключ доступа!';
        die;
    }
    
    if (!isset($_POST['from']) || !preg_match('/^\d{2}\.\d{2}\.\d{4}$/', $_POST['from']))
    {
        echo 'Неверный формат начальной даты!';
        die;
    }
    
    if (!isset($_POST['to']) || !preg_match('/^\d{2}\.\d{2}\.\d{4}$/', $_POST['to']))
    {
        echo 'Неверный формат конечной даты!';
        die;
    }
    
    // получаем список заявок
    
    $all_requests = CIBlockElement::GetList(
            ['ID' => 'desc'],
            [
                "IBLOCK_ID" => ADS_IBLOCK,
                "><DATE_CREATE" => [ConvertTimeStamp(strtotime($_POST['from']), "FULL"), ConvertTimeStamp(strtotime($_POST['to'].' 23:59:59'), "FULL")],
                "PROPERTY_GIVEN_TO_1C" => false
            ],
            false,
            false,
            [
                "ID",
                "IBLOCK_SECTION_ID",
                "DATE_CREATE",
                "PROPERTY_FIO",
                "PROPERTY_PHONE",
                "PROPERTY_GIVEN_TO_1C"
            ]
            );
    
    echo '<?xml version="1.0" encoding="UTF-8"?>';
    
    while ($r = $all_requests->GetNext())
    {
        $id = (int)$r["ID"];
        $fio = $r["PROPERTY_FIO_VALUE"];
        $phone = $r["PROPERTY_PHONE_VALUE"];
        $type = (int)CIBLockSection::GetById($r["IBLOCK_SECTION_ID"])->GetNext()["CODE"];
        $date = $r["DATE_CREATE"];
        
        printf('<Заявка ФИО="%s" Телефон="%s" ВидЗаявки="%d" ДатаЗаявки="%s" РекламаИсточник="" РекламаОбъявление="" ID="%d" ДопУслуга=""></Заявка>'."\r\n",
                $fio, $phone, $type, $date, $id
                );
        
        CIBlockElement::SetPropertyValuesEx($id, ADS_IBLOCK, array('GIVEN_TO_1C' => [603]));
    }
}

//<form action="index.php" method="post">
//    <input type="hidden" value="c98f28f8aed3447853b6bad8f9af6e75bff71b203444d269dea060582887fef0" name="token">
//    <input type="text" name="from">
//    <input type="text" name="to">
//    <input type="submit" name="send">
//</form>

?>

