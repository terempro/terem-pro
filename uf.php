<?php
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAdmin())
{
    exit('Попытка вторжения');
}

CModule::IncludeModule('iblock');

$houses = array();

// Получаем все серии
        
$arSort = ['NAME' => 'asc'];
$arFilter = ['IBLOCK_ID' => 13];
$arSelect = ['ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_SERIES'];
$series = CIBlockElement::GetList($arSort, $arFilter, 0, 0, $arSelect);

while ($s = $series->GetNext())
{
    // Имя категории
    $category = CIBlockSection::GetById($s['IBLOCK_SECTION_ID'])->GetNext();
    
    // Получаем каркасные варианты
    $karkas_variants = CIBlockElement::GetProperty(13, $s['ID'], [], ['CODE' => 'KARKAS_VARIANTS_HOUSE']);
    // Получаем брусовые варианты
    $brus_variants = CIBlockElement::GetProperty(13, $s['ID'], [], ['CODE' => 'BRUS_VARIANTS_HOUSE']);
    // Получаем кирпичные варианты
    $kirpich_variants = CIBlockElement::GetProperty(13, $s['ID'], [], ['CODE' => 'KIRPICH_VARIANTS_HOUSE']);
    
    // ID вариантов сохраняем в массив
    
    $variants_array = [];
    
    while ($v = $karkas_variants->GetNext()) 
    {
        $variants_array[] = ['ID' => (int)$v['VALUE'], 'TECH' => 'Каркас'];
    }
    while ($v = $brus_variants->GetNext()) 
    {
        $variants_array[] = ['ID' => (int)$v['VALUE'], 'TECH' => 'Брус'];
    }
    while ($v = $kirpich_variants->GetNext()) 
    {
        $variants_array[] = ['ID' => (int)$v['VALUE'], 'TECH' => 'Кирпич'];
    }
    
    foreach ($variants_array as $v)
    {
        // получаем конкретный вариант
        $arOrder = ['NAME' => 'asc'];
        $arFilter = ['IBLOCK_ID' => 21, 'ID' => $v["ID"]];
        $arSelect = [
            'NAME', 
            'PREVIEW_PICTURE',
            'IBLOCK_SECTION_ID',
            'PROPERTY_SIZE1',
            'PROPERTY_SIZE2',
            'PROPERTY_SQUARE_HOUSE',
            'PROPERTY_SQUARE_ALL_HOUSE',
            'PROPERTY_SQUARE_LIFE_HOUSE',
            'PROPERTY_FLOOR',
            'PROPERTY_TEHNOLOGY_DESCRIPTION',
            'PROPERTY_ID_CALCULATOR',
            'PROPERTY_PRICE_HOUSE.PROPERTY_LINK',
            'PROPERTY_PRICE_HOUSE.PROPERTY_PRICE_1',
            'PROPERTY_PRICE_HOUSE.PROPERTY_PRICE_2',
            'PROPERTY_PRICE_HOUSE.PROPERTY_DISCONT1',
        ];
        
        $house_variant = CIBlockElement::GetList($arOrder, $arFilter, 0, 0, $arSelect)->GetNext();
        
        if ($house_variant['PROPERTY_PRICE_HOUSE_PROPERTY_PRICE_1_VALUE'])
        {
            // Определяем псевдо-варианты
            $variant = 'Лето';
            if ($v["TECH"] == 'Кирпич' 
                || ($v['TECH'] == 'Каркас' && (strpos($house_variant['PROPERTY_TEHNOLOGY_DESCRIPTION_VALUE'], '200') || strpos($house_variant['NAME'], '200')))
                || ($v['TECH'] == 'Брус' && (strpos($house_variant['PROPERTY_TEHNOLOGY_DESCRIPTION_VALUE'], '180') || strpos($house_variant['NAME'], '180'))))
            {
                $variant = 'Для постоянного проживания';
            }
            
            $houses[] = [
                
                // Основная информация
                'name' => trim($house_variant['NAME']),
                'image' => CFile::GetPath($house_variant['PREVIEW_PICTURE']),
                'link' => trim($house_variant['PROPERTY_PRICE_HOUSE_PROPERTY_LINK_VALUE']),
                'series' => trim($s['NAME']),
                'series_id' => (int)$s['ID'],
                'series_type' => trim($s['PROPERTY_SERIES_VALUE']),
                'category' => trim($category['NAME']),
                
                // Площадь, этажность, размеры
                'square' => (int)$house_variant['PROPERTY_SQUARE_HOUSE_VALUE'],
                'living_square' => (int)$house_variant['PROPERTY_SQUARE_LIFE_HOUSE_VALUE'],
                'total_square' => (int)$house_variant['PROPERTY_SQUARE_ALL_HOUSE_VALUE'],
                'floors' => (int)$house_variant['PROPERTY_FLOOR_VALUE'],    
                'width' => (float)$house_variant['PROPERTY_SIZE1_VALUE'], 
                'height' => (float)$house_variant['PROPERTY_SIZE2_VALUE'], 
                
                // Технология и цены
                'technology_type' => $v["TECH"],
                'technology_description' => trim($house_variant['PROPERTY_TEHNOLOGY_DESCRIPTION_VALUE']),
                'variant' => $variant,
                'real_price' => (int)$house_variant['PROPERTY_PRICE_HOUSE_PROPERTY_PRICE_1_VALUE'],
                'big_price' => (int)$house_variant['PROPERTY_PRICE_HOUSE_PROPERTY_PRICE_2_VALUE'],
                
            ];
            
            // Если вариант - лето, добавляем псевдо-вариант для зимы
            
            if ($variant == 'Лето')
            {
            
                $file = $_SERVER["DOCUMENT_ROOT"].'/upload/calcs/' . $house_variant["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' .$house_variant["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
                $f = fopen($file, 'r');
                if (!$f) continue;
                $file_data = fread($f, filesize($file));
                fclose($f);

                $discont1 = $house_variant['PROPERTY_PRICE_HOUSE_PROPERTY_DISCONT1_VALUE'] ?: 30;
                $result = unserialize($file_data);
                if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный')
                {
                    $price_opt = 0;
                    $test = 0;

                    foreach ($result[0]["ITEMS"] as $i)
                    {
                        $p = (int)$i['PRICE'];
                        $test = $test + $p;
                        $price_opt = $price_opt + ($p - (($p * $discont1) / 100));
                    }
                } 

                $houses[] = [

                    // Основная информация
                    'name' => trim($house_variant['NAME']),
                    'image' => CFile::GetPath($house_variant['PREVIEW_PICTURE']),
                    'link' => trim($house_variant['PROPERTY_PRICE_HOUSE_PROPERTY_LINK_VALUE']),
                    'series' => trim($s['NAME']),
                    'series_id' => (int)$s['ID'],
                    'series_type' => trim($s['PROPERTY_SERIES_VALUE']),
                    'category' => trim($category['NAME']),

                    // Площадь, этажность, размеры
                    'square' => (int)$house_variant['PROPERTY_SQUARE_HOUSE_VALUE'],
                    'living_square' => (int)$house_variant['PROPERTY_SQUARE_LIFE_HOUSE_VALUE'],
                    'total_square' => (int)$house_variant['PROPERTY_SQUARE_ALL_HOUSE_VALUE'],
                    'floors' => (int)$house_variant['PROPERTY_FLOOR_VALUE'],    
                    'width' => (float)$house_variant['PROPERTY_SIZE1_VALUE'], 
                    'height' => (float)$house_variant['PROPERTY_SIZE2_VALUE'], 

                    // Технология и цены
                    'technology_type' => $v["TECH"],
                    'technology_description' => trim($house_variant['PROPERTY_TEHNOLOGY_DESCRIPTION_VALUE']),
                    'variant' => 'Зима',
                    'real_price' => ((int)($house_variant['PROPERTY_PRICE_HOUSE_PROPERTY_PRICE_1_VALUE']*1000) + $price_opt)/1000,
                    'big_price' => 0,

                ];
                
            }
        }
    }
}

echo json_encode($houses);