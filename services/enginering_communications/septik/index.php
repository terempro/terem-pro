<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "деревянные дома, строительство деревянных домов, терем, деревянные дома под ключ, деревянные дома москва, стоимость деревянного дома, terem");
$APPLICATION->SetPageProperty("title", "Септик для загородного дома и дачи в Москве");
$APPLICATION->SetTitle("Септик для загородного дома и дачи в Москве");
?><section class="content text-page white + my-margin" role="content">
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12 col-sm-12">
			<div class="white padding-side clearfix">
				<div class="row">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<div class="text-center text-uppercase">
							<h1>
							Cептики </h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<p>
							 Современные технологии позволяют иметь в загородных домах весь тот комфорт, который доступен владельцам городских квартир. И один из обязательных пунктов — автономные канализации. Компания «Теремъ» предлагает современное оборудование для обустройства очистных сооружений. Септики — надежное и экологичное оборудование, на установку которых не влияет тип грунта и уровень залегания грунтовых вод. После очистки канализационных стоков в такой автономной системе получаются два конечных продукта: техническая вода, которая безвредна для почвы и растений, и активный ил, пригодный для удобрения деревьев и кустарников.&nbsp;
						</p>
						<p>
							 Компания «Теремъ» предлагает два типа автономных канализаций с принудительным отводом очищенных стоков.
						</p>
					</div>
				</div>
				 <!--one item-->
				<div class="row">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="( my-collapse-btn + design-border ) collapsed" type="button" data-toggle="collapse" data-target="#septik1" aria-expanded="false" aria-controls="septik1">
									<div class="my-table">
										<div>
											<h3>АВТОНОМНАЯ КАНАЛИЗАЦИЯ С ВНЕШНИМ НАКОПИТЕЛЬНЫМ РЕЗЕРВУАРОМ&nbsp;</h3>
										</div>
										<div>
 <i class="glyphicon glyphicon-menu-down"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="collapse my-collapse" id="septik1">
									<div class="( my-collapse-content + design-border + no-padding )">
										<div class="row">
											<div class="col-xs-12 col-md-12 col-sm-12">
 <img src="/services/enginering_communications/septik/астра-принудительная.png"><br>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				 <!--one item END--> <!--one item-->
				<div class="row">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="( my-collapse-btn + design-border ) collapsed" type="button" data-toggle="collapse" data-target="#septik2" aria-expanded="false" aria-controls="septik2">
									<div class="my-table">
										<div>
											<h3>АВТОНОМНАЯ КАНАЛИЗАЦИЯ СО ВСТРОЕННЫМ НАКОПИТЕЛЬНЫМ РЕЗЕРВУАРОМ&nbsp;</h3>
										</div>
										<div>
 <i class="glyphicon glyphicon-menu-down"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="collapse my-collapse" id="septik2">
									<div class="( my-collapse-content + design-border + no-padding )">
										<div class="row">
											<div class="col-xs-12 col-md-12 col-sm-12">
 <img src="/bitrix/templates/.default/assets/img/services/enginering_communications/septik/item-2.jpg" class="full-width" alt="">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				 <!--one item END-->
				<div class="row">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<p>
 <br>
						</p>
						<h3>ПРЕИМУЩЕСТВА АВТОНОМНОЙ СТАНЦИИ ОЧИСТКИ КАНАЛИЗАЦИОННЫХ СТОКОВ&nbsp;</h3>
						<p>
						</p>
						<ul>
							<li>
							<p>
								 Автономная станция очистки канализационных стоков имеет все необходимые сертификаты и гигиенические заключения.
							</p>
 </li>
							<li>
							<p>
								 Подходит для установки даже на маленьких участках.
							</p>
 </li>
							<li>
							<p>
								 Для эксплуатации установки необходимо только электричество. Не нужно покупать никаких бактерий.
							</p>
 </li>
							<li>
							<p>
								 Система способна обходиться без стока до трех месяцев. Идеально подойдет для дачи.
							</p>
 </li>
							<li>
							<p>
								 В случае отключения электричества и при аварии система продолжает работать как обычный септик.
							</p>
 </li>
							<li>
							<p>
								 Полипропиленовый корпус имеет толщину 20 мм. Это позволяет монтировать систему без бетонирования. Полипропилен не подвержен коррозии и защищен от ультрафиолетового излучения.
							</p>
 </li>
							<li>
							<p>
								 Компрессор не требует смазки и не издает шум.
							</p>
 </li>
							<li>
							<p>
								 Автономную станцию можно разместить в любом месте участка.
							</p>
 </li>
						</ul>
						<p>
						</p>
						<p>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>