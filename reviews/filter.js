function filter() {
    const form = document.querySelector('.reviews--filter');
    const btns = [...document.querySelectorAll('.reviews-filter--dropdown-item')];

    function show(dropdown) {
        dropdown.classList.remove('reviews-filter--dropdown-menu__inactive');
        dropdown.classList.add('reviews-filter--dropdown-menu__active');
    }

    function hide(dropdown) {
        dropdown.classList.remove('reviews-filter--dropdown-menu__active');
        dropdown.classList.add('reviews-filter--dropdown-menu__inactive');
    }

    function toggle(el) {
        el.nextElementSibling.classList.contains('reviews-filter--dropdown-menu__inactive') ? show(el.nextElementSibling) : hide(el.nextElementSibling);
    }

    function activate(btn) {
        btn.classList.add('js-active');
    }

    function deactivate(btn) {
        btn.classList.remove('js-active');
    }

    function hideOthers(el) {
        if (!el.classList.contains('js-active') && el.nextElementSibling !== null) {
            hide(el.nextElementSibling)
        }
    }

    function hideAll() {
        btns.forEach(btn =>{
            if (btn.nextElementSibling != null) {
                hide(btn.nextElementSibling)
            }
        })
    }

    btns.forEach(btn => {
        btn.addEventListener('click', (e) => {
            e.preventDefault();
            btns.forEach(deactivate);
            activate(btn);
            toggle(btn);
            btns.forEach(hideOthers);
        });
    })

    document.addEventListener('click', (e) => {
        if (!(e.target.classList.contains('reviews-filter--dropdown-item')  || e.target.parentElement.classList.contains('reviews-filter--dropdown-item'))) {
            hideAll();
        }
    });
}

filter();
