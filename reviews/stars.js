function handleStars() {
    const starField = document.querySelector('.add-review--stars');
    const stars = [...starField.querySelectorAll('.rating--star')];
    const rates = [...starField.querySelectorAll('[name="add-review--star"]')];

    function showStarOnHover(e) {
        const index = stars.indexOf(e.target);
        for (let i = 0; i <= index; i++) {
            stars[i].classList.remove('rating--star__emptyTemp');
            stars[i].classList.add('rating--star__fullTemp')
        }
        for (let i = index + 1; i < stars.length; i++) {
            stars[i].classList.remove('rating--star__fullTemp');
            stars[i].classList.add('rating--star__emptyTemp');
        }
    }

    function bindShowStarOnHover(el) {
        el.addEventListener('mouseover', showStarOnHover);
    }

    stars.forEach(bindShowStarOnHover);

    function removeHoverStars(star) {
        star.classList.remove('rating--star__fullTemp');
        star.classList.remove('rating--star__emptyTemp');
        if (stars.indexOf(star) > 0) {
            star.classList.add('rating--star__empty');
        }
    }

    starField.addEventListener('mouseout', () => {
        stars.forEach(removeHoverStars);
    });

    function handlePermStars(rate) {
        if (rate.checked) {
            for (let i = 0; i <= rate.value - 1; i++) {
                stars[i].classList.remove('rating--star__empty');
                stars[i].classList.add('rating--star__full')
            }
            for (let i = rate.value; i < stars.length; i++) {
                stars[i].classList.remove('rating--star__full');
                stars[i].classList.add('rating--star__empty');
            }
        }
    }

    rates.forEach(rate => {
        rate.addEventListener('change', () => {
            rates.forEach(handlePermStars)
        });
    })
}

handleStars();
