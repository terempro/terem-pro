function pages() {
    const pagesContainer = document.querySelector('.people--pages');

    function updatePageNumbers() {
        let pages = [...pagesContainer.querySelectorAll('.owl-dot')];
        function updateNumbers() {
            let counter = 1;
            function addPageNumber(page) {
                page.textContent = counter++;
            }
            pages.forEach(addPageNumber)
        }
        updateNumbers();
    }

    updatePageNumbers();
    window.addEventListener('resize', updatePageNumbers);

    const next = document.querySelector('.people--pages-wrapper .next');

    next.addEventListener('click', function() {
        const active = pagesContainer.querySelector('.active');
        if (active.nextElementSibling) {
            active.nextElementSibling.click();
        }
    });
}

document.addEventListener('DOMContentLoaded', pages);
