function activateLoginCheckbox() {
    const checkbox = document.querySelector('#login-cb');

    function check(box) {
        box.innerHTML = '<i class="fa fa-check"></i>';
    }
    
    function uncheck(box) {
        box.innerHTML = '';
    }
    
    function toggleCheckbox() {
        checkbox.checked ? check(checkbox.parentElement.querySelector('.mock-cb')) : uncheck(checkbox.parentElement.querySelector('.mock-cb'));
    }
    
    checkbox.addEventListener('change', toggleCheckbox)    
}

document.addEventListener('DOMContentLoaded', activateLoginCheckbox);