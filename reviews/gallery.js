function gallery() {
    const gallery = document.querySelector('.people--gallery-wrapper');
    const galleryView = document.querySelector('.people--gallery');
    const selectorList = document.querySelector('.reviews--selector');
    const galleryItems = [...document.querySelectorAll('.people--gallery-item')];
    const itemsCount = galleryItems.length;
    const pagesCount = getPageCount();
    const galleryWidth = getGalleryWidth();

    function getPageCount() {
        if (window.innerWidth > 1160) {
            return Math.ceil(itemsCount / 9);
        } else if (window.innerWidth > 760 && window.innerWidth < 1160) {
            return Math.ceil(itemsCount / 6);
        } else {
            return Math.ceil(itemsCount / 3)
        }
    }

    function getGalleryWidth() {
        if (window.innerWidth > 1160) {
            return 1053;
        } else if (window.innerWidth > 760 && window.innerWidth <= 1160) {
            return 702;
        } else if (window.innerWidth > 400 && window.innerWidth <= 760) {
            return 351
        } else {
            return  galleryItems[0].offsetWidth + 12;
        }
    }

    selectorList.innerHTML = '';
    moveGallery(0);

    for (let i = 1; i <= pagesCount; i++) {
        i === 1 ? selectorList.innerHTML += `<li class="active-selector">${i}</li>` : selectorList.innerHTML += `<li>${i}</li>`;
    }

    selectorList.innerHTML += '<li class="reviews--selector-next">След.</li>';

    const pagesSelectors = [...selectorList.querySelectorAll('li')];

    function moveGallery(shift) {
        gallery.style.transition = `1s`
        gallery.style.transform = `translateX(-${shift}px)`
    }

    function bindMoveGallery(selector) {
        if (!selector.classList.contains('reviews--selector-next')) {
            const position = (selector.textContent - 1 ) * galleryWidth;
            selector.addEventListener('click', () => {
                pagesSelectors.forEach(deactivate);
                activate(selector);
                moveGallery(position);
                // currentPosition = position;
            });
        }
    }

    function activate(selector) {
        selector.classList.add('active-selector')
    }

    function deactivate(selector) {
        selector.classList.remove('active-selector')
    }

    pagesSelectors.forEach(bindMoveGallery);

    const next = document.querySelector('.reviews--selector-next');

    function moveGalleryToNext() {
        const nextItem = document.querySelector('.active-selector').nextElementSibling;
        nextItem.click();
    }

    next.addEventListener('click', moveGalleryToNext);
}

gallery();
window.addEventListener('resize', gallery);
