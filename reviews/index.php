<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<section class="content content-back-side" data-parallax="up" data-opacity="true">
    <div class="container-fluid reset-padding">
        <div class="row reset-margin">
            <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
                <div class="back-img" style="background-image: url('/bitrix/templates/.default/assets/img/bg.jpg');">
                </div>
            </div>
        </div>
    </div>
</section>

<main class="main">
    <section class="content reviews">
        <h1 class="main--header">ТЕРЕМЪ &mdash; ОТЗЫВЫ</h1>
        <p>
        Отзывы о компании Терем. На этой странице вы можете написать отзыв, задать вопросы нашим специалистам, а также ознакомиться с&nbsp;ответами на <a href="/about/faq" title="Частые вопросы">часто задаваемые вопросы</a>.
        </p>
        <section class="reviews--stars">
            <h2 class="reviews--header">Звездные клиенты</h2>
            <section class="stars--gallery owl-carousel owl-theme my-item-slider">
                <article class="item stars--item reviews--item">
                    <div class="stars--video-container video-container">
                        <video class="stars--video">
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="stars--caption">
                        <h3 class="caption--person">
                            <span class="person--name">Константин Константинович Константинопольский</span>
                            <span class="person--message">говорит спасибо за постройку такого прекрасного дома</span>
                        </h3>
                        <section class="caption--details">
                            <div>
                                <span class="details--house">Софийский собор</span>
                                <span class="details--review-date">05.11.1605</span>
                            </div>
                            <!-- Как подключить? -->
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <!-- конец вопроса -->
                        </section>
                    </div>
                </article>
                <article class="item stars--item reviews--item">
                    <div class="stars--video-container video-container">
                        <video class="stars--video">
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="stars--caption">
                        <h3 class="caption--person">
                            <span class="person--name">Иосиф Давыдович Кобзон</span>
                            <span class="person--message">говорит спасибо за постройку такого прекрасного дома</span>
                        </h3>
                        <section class="caption--details">
                            <div>
                                <span class="details--house">Дворец Кобзона</span>
                                <span class="details--review-date">05.11.1605</span>
                            </div>
                            <!-- Как подключить? -->
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <!-- конец вопроса -->
                        </section>
                    </div>
                </article>
                <article class="item stars--item reviews--item">
                    <div class="stars--video-container video-container">
                        <video class="stars--video">
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="stars--caption">
                        <h3 class="caption--person">
                            <span class="person--name">Ниф-Ниф</span>
                            <span class="person--message">говорит спасибо за постройку такого прекрасного дома</span>
                        </h3>
                        <section class="caption--details">
                            <div>
                                <span class="details--house">Дом из веточек</span>
                                <span class="details--review-date">05.11.1605</span>
                            </div>
                            <!-- Как подключить? -->
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <!-- конец вопроса -->
                        </section>
                    </div>
                </article>
                <article class="item stars--item reviews--item">
                    <div class="stars--video-container video-container">
                        <video class="stars--video">
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="stars--caption">
                        <h3 class="caption--person">
                            <span class="person--name">Наф-Наф</span>
                            <span class="person--message">говорит спасибо за постройку такого прекрасного дома</span>
                        </h3>
                        <section class="caption--details">
                            <div>
                                <span class="details--house">Дом из соломы</span>
                                <span class="details--review-date">05.11.1605</span>
                            </div>
                            <!-- Как подключить? -->
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <!-- конец вопроса -->
                        </section>
                    </div>
                </article>
                <article class="item stars--item reviews--item">
                    <div class="stars--video-container video-container">
                        <video class="stars--video">
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="stars--caption">
                        <h3 class="caption--person">
                            <span class="person--name">Наф-Наф</span>
                            <span class="person--message">говорит спасибо за постройку такого прекрасного дома</span>
                        </h3>
                        <section class="caption--details">
                            <div>
                                <span class="details--house">Дом из соломы</span>
                                <span class="details--review-date">05.11.1605</span>
                            </div>
                            <!-- Как подключить? -->
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <!-- конец вопроса -->
                        </section>
                    </div>
                </article>
                <article class="item stars--item reviews--item">
                    <div class="stars--video-container video-container">
                        <video class="stars--video">
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="stars--caption">
                        <h3 class="caption--person">
                            <span class="person--name">Наф-Наф</span>
                            <span class="person--message">говорит спасибо за постройку такого прекрасного дома</span>
                        </h3>
                        <section class="caption--details">
                            <div>
                                <span class="details--house">Дом из соломы</span>
                                <span class="details--review-date">05.11.1605</span>
                            </div>
                            <!-- Как подключить? -->
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <!-- конец вопроса -->
                        </section>
                    </div>
                </article>
                <article class="item stars--item reviews--item">
                    <div class="stars--video-container video-container">
                        <video class="stars--video">
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="stars--caption">
                        <h3 class="caption--person">
                            <span class="person--name">Наф-Наф</span>
                            <span class="person--message">говорит спасибо за постройку такого прекрасного дома</span>
                        </h3>
                        <section class="caption--details">
                            <div>
                                <span class="details--house">Дом из соломы</span>
                                <span class="details--review-date">05.11.1605</span>
                            </div>
                            <!-- Как подключить? -->
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <!-- конец вопроса -->
                        </section>
                    </div>
                </article>
            </section>
        </section>
    </section>
    <section class="content reviews--general reviews">
        <h2 class="reviews--header">Фото | Видео отзывы</h2>
        <form class="reviews--filter">
            <div class="reviews-filter--dropdown">
                <button class="reviews-filter--dropdown-item">
                    <span>Категория дома</span>
                </button>
                <ul class="reviews-filter--dropdown-menu reviews-filter--dropdown-menu__inactive">
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из веточек</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из соломы</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из кирпичей</a>
                    </li>
                </ul>
            </div>
            <div class="reviews-filter--dropdown">
                <button class="reviews-filter--dropdown-item">
                    <span>Серия</span>
                </button>
                <ul class="reviews-filter--dropdown-menu reviews-filter--dropdown-menu__inactive">
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Ниф-Ниф</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Наф-Наф</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">И другой &mdash; самый умный &mdash; поросенок</a>
                    </li>
                </ul>
            </div>
            <div class="reviews-filter--dropdown">
                <button class="reviews-filter--dropdown-item"><span>Обсуждаемые</span></button>
                <ul class="reviews-filter--dropdown-menu reviews-filter--dropdown-menu__inactive">
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из веточек</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из соломы</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из кирпичей</a>
                    </li>
                </ul>
            </div>
            <div class="reviews-filter--dropdown">
                <button class="reviews-filter--dropdown-item"><span>Оцениваемые</span></button>
                <ul class="reviews-filter--dropdown-menu reviews-filter--dropdown-menu__inactive">
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из веточек</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из соломы</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из кирпичей</a>
                    </li>
                </ul>
            </div>
            <div class="reviews-filter--dropdown">
                <button class="reviews-filter--dropdown-item"><span>Новые</span></button>
                <ul class="reviews-filter--dropdown-menu reviews-filter--dropdown-menu__inactive">
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из веточек</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из соломы</a>
                    </li>
                    <li>
                        <a class="reviews-filter--dropdown-menu-item">Дом из кирпичей</a>
                    </li>
                </ul>
            </div>
        </form>
        <section class="reviews--people">
            <section class="people--gallery owl-carousel owl-theme">
                <div class="owl-item">
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container">
                            <!-- <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span> -->
                            <img src="1.jpg" alt="">
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Волк не смог сдуть домик из кирпичей</span>
                            </h3>
                            <section class="caption--details">
                                <div class="caption--details-wrapper">
                                    <span class="details--name">Умный поросенок Нуф-Нуф, который построил домик из кирпичей</span>
                                    <span class="details--house">Домик из кирпичей</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Хорошо построили!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Дядюшка Тыква</span>
                                    <span class="details--house">Домик дядюшки Тыквы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Я недоволен!!!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Ниф-Ниф</span>
                                    <span class="details--house">Дом из соломы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                </div>
                <div class="owl-item">
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Волк не смог сдуть домик из кирпичей</span>
                            </h3>
                            <section class="caption--details">
                                <div class="caption--details-wrapper">
                                    <span class="details--name">Умный поросенок Нуф-Нуф, который построил домик из кирпичей</span>
                                    <span class="details--house">Домик из кирпичей</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Хорошо построили!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Дядюшка Тыква</span>
                                    <span class="details--house">Домик дядюшки Тыквы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Я недоволен!!!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Ниф-Ниф</span>
                                    <span class="details--house">Дом из соломы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                </div>
                <div class="owl-item">
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Волк не смог сдуть домик из кирпичей</span>
                            </h3>
                            <section class="caption--details">
                                <div class="caption--details-wrapper">
                                    <span class="details--name">Умный поросенок Нуф-Нуф, который построил домик из кирпичей</span>
                                    <span class="details--house">Домик из кирпичей</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Хорошо построили!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Дядюшка Тыква</span>
                                    <span class="details--house">Домик дядюшки Тыквы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Я недоволен!!!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Ниф-Ниф</span>
                                    <span class="details--house">Дом из соломы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                </div>
                <div class="owl-item">
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Волк не смог сдуть домик из кирпичей</span>
                            </h3>
                            <section class="caption--details">
                                <div class="caption--details-wrapper">
                                    <span class="details--name">Умный поросенок Нуф-Нуф, который построил домик из кирпичей</span>
                                    <span class="details--house">Домик из кирпичей</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Хорошо построили!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Дядюшка Тыква</span>
                                    <span class="details--house">Домик дядюшки Тыквы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Я недоволен!!!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Ниф-Ниф</span>
                                    <span class="details--house">Дом из соломы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                </div>
                <div class="owl-item">
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Волк не смог сдуть домик из кирпичей</span>
                            </h3>
                            <section class="caption--details">
                                <div class="caption--details-wrapper">
                                    <span class="details--name">Умный поросенок Нуф-Нуф, который построил домик из кирпичей</span>
                                    <span class="details--house">Домик из кирпичей</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Хорошо построили!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Дядюшка Тыква</span>
                                    <span class="details--house">Домик дядюшки Тыквы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Я недоволен!!!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Ниф-Ниф</span>
                                    <span class="details--house">Дом из соломы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                </div>
                <div class="owl-item">
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Волк не смог сдуть домик из кирпичей</span>
                            </h3>
                            <section class="caption--details">
                                <div class="caption--details-wrapper">
                                    <span class="details--name">Умный поросенок Нуф-Нуф, который построил домик из кирпичей</span>
                                    <span class="details--house">Домик из кирпичей</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Хорошо построили!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Дядюшка Тыква</span>
                                    <span class="details--house">Домик дядюшки Тыквы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                    <article class="people--gallery-item reviews--item">
                        <div class="people--media-container video-container">
                            <video class="people--media">
                                <source src="/upload/contact.mp4" type="video/mp4">
                            </video>
                            <span class="video-container--play-btn"></span>
                        </div>
                        <div class="people--caption reviews--caption">
                            <div class="rating details--rating">
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__full"></span>
                                <span class="rating--star rating--star__half"></span>
                                <span class="rating--star rating--star__empty"></span>
                            </div>
                            <h3 class="caption--person">
                                <span class="person--message">Я недоволен!!!</span>
                            </h3>
                            <section class="caption--details">
                                <div>
                                    <span class="details--name">Ниф-Ниф</span>
                                    <span class="details--house">Дом из соломы</span>
                                    <span class="details--review-date">05.11.1605</span>
                                </div>
                            </section>
                        </div>
                    </article>
                </div>
            </section>
            <!-- <div class="reviews--selector-wrapper">
                <span class="review--selector-title">Страницы:</span>
                <ul class="reviews--selector"></ul>
            </div> -->
            <div class="people--pages-wrapper">
                <span>Страницы:</span>
                <div class="people--pages"></div>
                <span class="next">След.</span>
            </div>
        </section>
        <section class="reviews--add-review add-review">
            <form class="add-review--form" action="">
                <div class="add-review--form-row add-review--form-row__flex">
                    <div>
                        <fieldset class="add-review--stars">
                            <label class="rating--star add-review--star rating--star__full" for="star1">
                                <input type="radio" name="add-review--star" value="1" id="star1" checked>
                            </label>
                            <label class="rating--star add-review--star rating--star__empty" for="star2">
                                <input type="radio" name="add-review--star" value="2" id="star2">
                            </label>
                            <label class="rating--star add-review--star rating--star__empty" for="star3">
                                <input type="radio" name="add-review--star" value="3" id="star3">
                            </label>
                            <label class="rating--star add-review--star rating--star__empty" for="star4">
                                <input type="radio" name="add-review--star" value="4" id="star4">
                            </label>
                            <label class="rating--star add-review--star rating--star__empty" for="star5">
                                <input type="radio" name="add-review--star" value="5" id="star5">
                            </label>
                        </fieldset>
                        <input class="add-review--item add-review--text-input add-review--text-input__big" type="text" id="add-review--header" placeholder="Заголовок">
                    </div>
                    <div>
                        <label for="house-name">Выберите название объекта</label>
                        <select class="add-review--item add-review--select" id="house-name">
                            <option value="Велоцраптор" selected>Велоцираптор</option>
                            <option value="Годзилла">Годзилла</option>
                            <option value="Бульбазавр">Бульбазавр</option>
                        </select>
                    </div>
                </div>
                <fieldset class="add-review--form-row">
                    <textarea class="add-review--item add-review--text expand" name="add-review" rows="5" cols="" placeholder="Оставить отзыв"></textarea>
                    <label class="add-review--attach-label" for="attach">Загрузить фото-видео</label>
                    <input class="add-review--attach" id="attach" type="file">
                </fieldset>
                <div class="add-review--form-row add-review--form-row__flex add-review--form-row__submit">
                    <button type="submit" name="add-review--submit" class="add-review--submit" disabled>Отправить</button>
                    <p class="add-review--notice">Отправляя письмо, вы соглашаетесь с <a href="https://www.terem-pro.ru/privacy-policy/">политикой обработки данных</a></p>
                </div>
            </form>
        </section>
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#login">Open Modal</button>
    </section>

    
</main>



<!-- Modal -->
<div id="login" class="modal fade login" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Войти в Теремъ</h4>
      </div>
      <form action="" class="login-form">
          <div class="login-form--row">
              <div class="login-avatar">
                <img src="/bitrix/templates/.default/assets/img/reviews/avatar-placeholder.jpg"></img>
              </div>
              <input type="text" placeholder="Имя | Логин">
          </div>
          <input type="password" placeholder="Пароль">
          <input type="email" placeholder="E-mail">
          <label for="login-cb" class="login-checkbox">
            <input type="checkbox" id="login-cb">
            <span class="mock-cb"></span>
            <span>
                Я принимаю условия<br>
                <a href="/privacy-policy/">политики обработки данных</a>
            </span>
          </label>
          <button class="login-btn">Вход</button>
      </form>
    </div>

  </div>
</div>

<script src="carousel.js"></script>
<script src="player.js"></script>
<script src="filter.js"></script>
<script src="stars.js"></script>
<script src="textarea.js"></script>
<script src="checkbox.js" defer></script>
<script src="pages.js" defer></script>
<script src="reply.js" defer></script>
<script src="validate.js" defer></script>

<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>




