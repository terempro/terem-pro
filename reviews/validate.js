function validateReviewForms() {
    const forms = [...document.querySelectorAll('.add-review--form')];

    function validateFields(form) {
        const fields = [...form.querySelectorAll('input[type="text"], textarea')];
        const submit = form.querySelector('[type="submit"]');

        fields.forEach(field => {
            field.dataset.ready = false;

            function validate() {
                let isFormReady = true;

                // validation condition
                field.dataset.ready = field.value.length > 3 ? true : false;
                // validation condition end

                function checkIfReady(field) {
                    if (field.dataset.ready === 'false') {
                        isFormReady = false;
                    }
                }

                fields.forEach(checkIfReady);
                submit.disabled = isFormReady ?  false : true;
            }
            field.addEventListener('input', validate);
        })
    }
    forms.forEach(validateFields);
}

document.addEventListener('DOMContentLoaded', validateReviewForms);
