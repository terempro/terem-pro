<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>

<section class="content content-back-side" data-parallax="up" data-opacity="true">
    <div class="container-fluid reset-padding">
        <div class="row reset-margin">
            <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
                <div class="back-img" style="background-image: url('/bitrix/templates/.default/assets/img/bg.jpg');">
                </div>
            </div>
        </div>
    </div>
</section>

<main class="main">
    <section class="content reviews">
        <h1 class="main--header">ТЕРЕМЪ &mdash; ОТЗЫВЫ</h1>
        <article class="reviews--detailed">
            <section class="review--caption">
                <div class="review--caption-avatar">
                    <img src="/bitrix/templates/.default/assets/img/reviews/avatar.jpg" alt="">
                </div>
                <div class="review--caption-info">
                    <div class="review--rating-date">
                        <div class="rating review--rating">
                            <span class="rating--star rating--star__full"></span>
                            <span class="rating--star rating--star__full"></span>
                            <span class="rating--star rating--star__full"></span>
                            <span class="rating--star rating--star__half"></span>
                            <span class="rating--star rating--star__empty"></span>
                        </div>
                        <div class="review--caption-date review--date">
                            17.07.2017
                        </div>
                    </div>
                    <!-- <div class="review--caption-title">             -->
                    <div class="review--name-house">
                        <span class="review--name">Константин Константинович Константинопольский</span>
                        <span class="review--house">Галактика 2</span>
                    </div>
                    <!-- </div> -->
                </div>
            </section>
            <section class="review--text">
                <h2 class="review--heading">Спасибо за дом!</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </section>
            <section class="review--action">
                <div class="review--action-like">
                    <button class="review--like"><i class="fa fa-thumbs-down"></i></button>
                    <button class="review--dislike"><i class="fa fa-thumbs-up"></i></button>
                    <span class="review--like-counter">0</span>
                </div>
                <div class="review--action-share">
                    <button class="review--share">
                        <i class="fa fa-share-alt"></i>
                        Поделиться
                    </button>
                </div>
            </section>
            <section class="review--stage">
                <div class="owl-carousel owl-theme review-main my-item-slider">
                    <div class="item">
                        <video controls>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="item">
                        <video controls>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="item">
                        <video controls>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="item">
                        <video controls>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="item">
                        <video controls>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="item">
                        <video controls>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                    </div>
                    <!-- <div class="item">
                        <img src="1.jpg" alt="">
                    </div> -->
                </div>
                <div class="owl-carousel owl-theme review-previews my-item-slider">
                    <div class="item">
                        <video>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="item">
                        <video>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="item">
                        <video>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="item">
                        <video>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="item">
                        <video>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <div class="item">
                        <video>
                            <source src="/upload/contact.mp4" type="video/mp4">
                        </video>
                        <span class="video-container--play-btn"></span>
                    </div>
                    <!-- <div class="item">
                        <img src="1.jpg" alt="">
                    </div> -->
                </div>
            </section>
            <section class="review--comments">
                <h3 class="review--comments-heading reviews--header">
                    Комментарии к отзыву
                </h3>
                <section class="review--comment-block">
                    <article class="review--comment" data-id="comment1">
                        <section class="review--comment-avatar-date">
                            <div class="review--comment-avatar">
                                <img src="../bitrix/templates/.default/assets/img/reviews/avatar.jpg" alt="">
                            </div>
                            <div class="review--comment-date review--date">17.07.2017</div>
                        </section>
                        <div class="review--comment-body">
                            <section class="review--comment-heading">
                                <p class="review--comment-name">Константин Константинович Константинопольский</p>
                                <div class="review--action">
                                    <div class="review--action-like">
                                        <button class="review--like"><i class="fa fa-thumbs-down"></i></button>
                                        <button class="review--dislike"><i class="fa fa-thumbs-up"></i></button>
                                        <span class="review--like-counter">0</span>
                                    </div>
                                </div>
                            </section>
                            <section class="review--comment-text">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </section>
                            <section class="review--comment-action">
                                <button class="review--complain" data-id="comment1">Пожаловаться</button>
                                <button class="review--reply" data-id="comment1">Ответить</button>
                            </section>
                        </div>
                    </article>

                    <article class="review--comment review--subcomment" data-id="comment2">
                        <section class="review--comment-avatar-date">
                            <div class="review--comment-avatar">
                                <img src="../bitrix/templates/.default/assets/img/reviews/avatar.jpg" alt="">
                            </div>
                            <div class="review--comment-date review--date">17.07.2017</div>
                        </section>
                        <div class="review--comment-body">
                            <section class="review--comment-heading">
                                <p class="review--comment-name">Константин Константинович Константинопольский</p>
                                <div class="review--action">
                                    <div class="review--action-like">
                                        <button class="review--like"><i class="fa fa-thumbs-down"></i></button>
                                        <button class="review--dislike"><i class="fa fa-thumbs-up"></i></button>
                                        <span class="review--like-counter">0</span>
                                    </div>
                                </div>
                            </section>
                            <section class="review--comment-text">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </section>
                            <section class="review--comment-action">
                                <button class="review--complain" data-id="comment2">Пожаловаться</button>
                                <button class="review--reply" data-id="comment2">Ответить</button>
                            </section>
                        </div>
                    </article>

                    <article class="review--comment" data-id="comment3">
                        <section class="review--comment-avatar-date">
                            <div class="review--comment-avatar">
                                <img src="/bitrix/templates/.default/assets/img/reviews/avatar.jpg" alt="">
                            </div>
                            <div class="review--comment-date review--date">17.07.2017</div>
                        </section>
                        <div class="review--comment-body">
                            <section class="review--comment-heading">
                                <p class="review--comment-name">Константин Константинович Константинопольский</p>
                                <div class="review--action">
                                    <div class="review--action-like">
                                        <button class="review--like"><i class="fa fa-thumbs-down"></i></button>
                                        <button class="review--dislike"><i class="fa fa-thumbs-up"></i></button>
                                        <span class="review--like-counter">0</span>
                                    </div>
                                </div>
                            </section>
                            <section class="review--comment-text">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </section>
                            <section class="review--comment-action">
                                <button class="review--complain" data-id="comment3">Пожаловаться</button>
                                <button class="review--reply" data-id="comment3">Ответить</button>
                            </section>
                        </div>
                    </article>

                    <form class="review--leave-reply review--subcomment">
                        <textarea class="expand" name="" id="" placeholder="Ваш ответ..."></textarea>
                        <button type="submit">Ответить</button>
                    </form>
                </section>
            </section>
            <section class="add-review">
                <form class="add-review--form" action="">
                    <fieldset class="add-review--form-row">
                        <textarea class="add-review--item add-review--text expand" name="add-review" rows="5" cols="" placeholder="Оставить комментарий..."></textarea>
                        <label class="add-review--attach-label" for="attach">Загрузить фото-видео</label>
                        <input class="add-review--attach" id="attach" type="file">
                    </fieldset>
                    <div class="add-review--form-row add-review--form-row__flex add-review--form-row__submit">
                        <button type="submit" name="add-review--submit" class="add-review--submit" disabled>Отправить</button>
                        <p class="add-review--notice">Отправляя письмо, вы соглашаетесь с <a href="https://www.terem-pro.ru/privacy-policy/">политикой обработки данных</a></p>
                    </div>
                </form>
            </section>
        </article>
    </section>
</main>

<script src="carousel.js"></script>
<script src="textarea.js"></script>

<script src="carousel.js"></script>
<script src="player.js"></script>
<script src="filter.js"></script>
<script src="stars.js"></script>
<script src="textarea.js"></script>
<script src="pages.js" defer></script>
<script src="reply.js" defer></script>
<script src="validate.js" defer></script>

<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>


