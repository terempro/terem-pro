function starGallery () {
    $('.reviews--stars .owl-carousel').owlCarousel({
        video: true,
        loop: false,
        margin: 12,
        nav: true,
        navText: ['<i class="my-arrow-left">', '<i class="my-arrow-right">'],
        responsive:{
            0:{
                items:1
            },
            700:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })
}

function peopleGallery () {
    $('.people--gallery').owlCarousel({
        video: true,
        loop: false,
        margin: 12,
        dots: true,
        dotsContainer: '.people--pages',
        nav: false,
        // navText: ['<i class="my-arrow-left">', '<i class="my-arrow-right">'],
        responsive:{
            0:{
                items:1
            },
            760:{
                items:2
            },
            1160:{
                items:3
            }
        }
    })
}

function commentsGallery() {
    const main = $('.review-main');
    const previews = $('.review-previews');

    main.owlCarousel({
        video: true,
        loop: false,
        margin: 0,
        dots: true,
        nav: true,
        navText: ['<i class="my-arrow-left">', '<i class="my-arrow-right">'],
        responsive:{
            0:{
                items:1
            },
            700:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    previews.owlCarousel({
        video: true,
        loop: false,
        margin: 12,
        dots: true,
        nav: true,
        navText: ['<i class="my-arrow-left">', '<i class="my-arrow-right">'],
        responsive:{
            0:{
                items:2
            },
            700:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })

    syncTwoSliders(main, previews);
}

document.addEventListener('DOMContentLoaded', starGallery);
document.addEventListener('DOMContentLoaded', peopleGallery);
document.addEventListener('DOMContentLoaded', commentsGallery);
