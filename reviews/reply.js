function replyToComment() {
    const replyBtns = [...document.querySelectorAll('button.review--reply')];

    replyBtns.forEach(btn => {
        btn.addEventListener('click', function(e) {

            const comment = document.querySelector(`article.review--comment[data-id="${btn.dataset.id}"]`);
            const replyForm = document.createElement('form')
            replyForm.classList.add('review--leave-reply', 'review--subcomment');
            replyForm.dataset.replyto = btn.dataset.id;
            replyForm.innerHTML = `<textarea class="expand" name="" id="" placeholder="Ваш ответ..."></textarea>
                                   <button type="submit">Ответить</button>`;
            comment.parentNode.insertBefore(replyForm, comment.nextElementSibling);

            btn.disabled = true;
            autoExpandTextareas();
        });
    })
}

document.addEventListener('DOMContentLoaded', replyToComment);
