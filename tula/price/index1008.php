<?php

function d($path){
    $t = explode('/',$path);
    unset($t[count($t)-1]);
    return implode('/', $t);
}
//var_dump(d(__FILE__));
//exit;

//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Прайс лист компании Терем.");
$APPLICATION->SetPageProperty("keywords", "прайс лист,цена деревянные дома");
$APPLICATION->SetPageProperty("title", "Прайс лист компании Терем в Туле. Всегда свежие цены и актуальная информация");
$APPLICATION->SetTitle("Прайс лист компании Терем в Туле. Всегда свежие цены и актуальная информация");
?>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                   Прайс-лист компании «Теремъ»
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                                Цены действительны на дату отгрузки фундамента. Цены включают стоимость доставки дома в радиусе до 100 км от нашей производственной базы в г. Бронницы Московской области, а так же сборку и проживание бригады. Цены указаны на комплектацию «Лето», подробно с ней ознакомиться можно в карточке дома. 
                            </p>
 <!--<p>*Цены действительны до 12.04.17</p>-->
                            <p>
                               <br>Все подробности по телефону: +7 (4872) 751-446
                            </p>
                            <br>
                            <br>
                        </div>
                    </div>


                    <?php
                    //Собираем ID и NAME категорий
                    $IBLOCK_ID = 13;

                    $F = Array(
                        'IBLOCK_ID' => $IBLOCK_ID,
                        'ACTIVE' => 'Y'
                    );

                    $S = CIBlockSection::GetList(Array("SORT" => "ASC"), $F, true);


                    $Categoryes = Array();


                    $cnt = 0;


                    while ($Section = $S->Fetch()) {
                        $Categoryes[$cnt]['ID'] = $Section['ID'];
                        $Categoryes[$cnt]['NAME'] = $Section['NAME'];
                        $cnt++;
                    }





                    //Собираем все дома в массив с категориями
                    foreach ($Categoryes as $key => $Ctaegory) {
                        $Select = Array(
                            "ID",
                            "NAME",
                            "PROPERTY_SIZER",
                            "PROPERTY_KARKAS_VARIANTS_HOUSE",
                            "PROPERTY_BRUS_VARIANTS_HOUSE",
                            "PROPERTY_KIRPICH_VARIANTS_HOUSE",
                        );
                        $Filter = Array("IBLOCK_ID" => 13, "SECTION_ID" => $Ctaegory["ID"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
                        $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $Filter, false, Array("nPageSize" => 500), $Select);

                        $counter = 0;

                        while ($ob = $res->Fetch()) {

                            $Categoryes[$key]["HOUSES"][$counter] = $ob;


                            if(count($ob["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"]) > 0){



                                foreach($ob["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"] as $val){

                                    $arSelects = Array(
                                        "ID",
                                        "NAME",
                                        "PROPERTY_TEHNOLOGY_HOUSE",
                                        "PROPERTY_PRICE_HOUSE_TULA",
                                        "PROPERTY_ID_CALCULATOR",
                                        "PROPERTY_LINK",
                                    );

                                    $arFilters = Array(
                                        "IBLOCK_ID" => 21,
                                        "ID" => $val,
                                        "ACTIVE_DATE" => "Y",
                                        "ACTIVE" => "Y"
                                    );

                                    $ress = CIBlockElement::GetList(
                                        Array("SORT" => "ASC"),
                                        $arFilters,
                                        false,
                                        Array(),
                                        $arSelects
                                    );

                                    while ($obs = $ress->Fetch()) {

										if (!$obs["PROPERTY_PRICE_HOUSE_TULA_VALUE"]) continue;
                                        $Sel = Array(
                                            "ID",
                                            "PROPERTY_PRICE_1",
                                            "PROPERTY_DISCONT1",
                                            "PROPERTY_LINK",
                                        );

                                        $Filt = Array(
                                            "IBLOCK_ID" => $PRICE_IBLOCK_ID,
                                            "ID" => $obs["PROPERTY_PRICE_HOUSE_TULA_VALUE"],
                                            "ACTIVE" => "Y"
                                        );

                                        $r = CIBlockElement::GetList(
                                            Array("SORT" => "ASC"),
                                            $Filt,
                                            false,
                                            Array(),
                                            $Sel
                                        );

                                        while ($o = $r->Fetch()){
                                            $price = $o['PROPERTY_PRICE_1_VALUE'];
                                            $Categoryes[$key]["HOUSES"][$counter]['LINK'] = $o['PROPERTY_LINK_VALUE'];

                                            $discont1 = 30;
                                            if($o['PROPERTY_DISCONT1_VALUE'] != NULL && $o['PROPERTY_DISCONT1_VALUE'] > 0){
                                                $discont1 =  $o['PROPERTY_DISCONT1_VALUE'];
                                            }
                                        }





//                                        echo "<pre>";
//                                        var_dump($Categoryes[$key]["HOUSES"][$counter]['LINK']);
//                                        die();




                                        if($obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'] == 'Каркасный 200'){

                                            $Categoryes[$key]["HOUSES"][$counter]['KARKAS']['POST'] = $price;//$obs['PROPERTY_PRICE_HOUSE_VALUE'].'- '.$val.' '.$obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'].' '.$price;

                                        }else{

                                            $Categoryes[$key]["HOUSES"][$counter]['KARKAS']['LETO'] = $price;//$obs['PROPERTY_PRICE_HOUSE_VALUE'].'- '.$val.' '.$obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'].' '.$price;


                                            $file = $_SERVER["DOCUMENT_ROOT"].'/upload/calcs/' . $obs["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' .$obs["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
                                            $f = fopen($file, 'r');
                                            $file_data = fread($f, filesize($file));
                                            fclose($f);


                                            $result = unserialize($file_data);
                                            if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный'){



                                                $price_opt = 0;
                                                $test = 0;


                                                foreach ($result[0]["ITEMS"] as $i) {
                                                    $p = (int) $i['PRICE'];
                                                    $test = $test + $p;
                                                    $price_opt = $price_opt + ($p - (($p * $discont1) / 100));
                                                }



                                                $str = $Categoryes[$key]["HOUSES"][$counter]['KARKAS']['LETO'].'000';
                                                $Categoryes[$key]["HOUSES"][$counter]['KARKAS']['ZIMA'] =  $str + $price_opt;


                                        }

                                        }

                                    }

                                }

                            }

                            if(count($ob["PROPERTY_BRUS_VARIANTS_HOUSE_VALUE"]) > 0){



                                foreach($ob["PROPERTY_BRUS_VARIANTS_HOUSE_VALUE"] as $val){

                                    $arSelects = Array(
                                        "ID",
                                        "NAME",
                                        "PROPERTY_TEHNOLOGY_HOUSE",
                                        "PROPERTY_PRICE_HOUSE_TULA",
                                        "PROPERTY_ID_CALCULATOR",
                                    );

                                    $arFilters = Array(
                                        "IBLOCK_ID" => 21,
                                        "ID" => $val,
                                        "ACTIVE_DATE" => "Y",
                                        "ACTIVE" => "Y"
                                    );

                                    $ress = CIBlockElement::GetList(
                                        Array("SORT" => "ASC"),
                                        $arFilters,
                                        false,
                                        Array(),
                                        $arSelects
                                    );

                                    while ($obs = $ress->Fetch()) {

										if (!$obs["PROPERTY_PRICE_HOUSE_TULA_VALUE"]) continue;
                                        $Sel = Array(
                                            "ID",
                                            "PROPERTY_PRICE_1",
                                            "PROPERTY_DISCONT1",
                                            "PROPERTY_LINK",
                                        );

                                        $Filt = Array(
                                            "IBLOCK_ID" => $PRICE_IBLOCK_ID,
                                            "ID" => $obs["PROPERTY_PRICE_HOUSE_TULA_VALUE"],
                                            "ACTIVE" => "Y"
                                        );

                                        $r = CIBlockElement::GetList(
                                            Array("SORT" => "ASC"),
                                            $Filt,
                                            false,
                                            Array(),
                                            $Sel
                                        );



                                        while ($o = $r->Fetch()){
                                            $price = $o['PROPERTY_PRICE_1_VALUE'];

                                            $discont1 = 30;
                                            if($o['PROPERTY_DISCONT1_VALUE'] != NULL && $o['PROPERTY_DISCONT1_VALUE'] > 0){
                                                $discont1 =  $o['PROPERTY_DISCONT1_VALUE'];
                                            }
                                        }




                                        if($obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'] == 'Брус клееный 180'){

                                            $Categoryes[$key]["HOUSES"][$counter]['BRUS']['POST'] = $price;//$obs['PROPERTY_PRICE_HOUSE_VALUE'].'- '.$val.' '.$obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'].' '.$price;

                                        }else{

                                            $Categoryes[$key]["HOUSES"][$counter]['BRUS']['LETO'] = $price;//$obs['PROPERTY_PRICE_HOUSE_VALUE'].'- '.$val.' '.$obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'].' '.$price;


                                            $file = $_SERVER["DOCUMENT_ROOT"].'/upload/calcs/' . $obs["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' .$obs["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
                                            $f = fopen($file, 'r');
                                            $file_data = fread($f, filesize($file));
                                            fclose($f);


                                            $result = unserialize($file_data);
                                            if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный'){



                                                $price_opt = 0;
                                                $test = 0;


                                                foreach ($result[0]["ITEMS"] as $i) {
                                                    $p = (int) $i['PRICE'];
                                                    $test = $test + $p;
                                                    $price_opt = $price_opt + ($p - (($p * $discont1) / 100));
                                                }



                                                $str = $Categoryes[$key]["HOUSES"][$counter]['BRUS']['LETO'].'000';
                                                $Categoryes[$key]["HOUSES"][$counter]['BRUS']['ZIMA'] =  $str + $price_opt;


                                            }

                                        }

                                    }

                                }

                            }



                            if(count($ob["PROPERTY_KIRPICH_VARIANTS_HOUSE_VALUE"]) > 0){



                                foreach($ob["PROPERTY_KIRPICH_VARIANTS_HOUSE_VALUE"] as $val){

                                    $arSelects = Array(
                                        "ID",
                                        "NAME",
                                        "PROPERTY_TEHNOLOGY_HOUSE",
                                        "PROPERTY_PRICE_HOUSE_TULA",
                                        "PROPERTY_ID_CALCULATOR",
                                    );

                                    $arFilters = Array(
                                        "IBLOCK_ID" => 21,
                                        "ID" => $val,
                                        "ACTIVE_DATE" => "Y",
                                        "ACTIVE" => "Y"
                                    );

                                    $ress = CIBlockElement::GetList(
                                        Array("SORT" => "ASC"),
                                        $arFilters,
                                        false,
                                        Array(),
                                        $arSelects
                                    );

                                    while ($obs = $ress->Fetch()) {

										if (!$obs["PROPERTY_PRICE_HOUSE_TULA_VALUE"]) continue;
                                        $Sel = Array(
                                            "ID",
                                            "PROPERTY_PRICE_1",
                                            "PROPERTY_DISCONT1",
                                            "PROPERTY_LINK",
                                        );

                                        $Filt = Array(
                                            "IBLOCK_ID" => $PRICE_IBLOCK_ID,
                                            "ID" => $obs["PROPERTY_PRICE_HOUSE_TULA_VALUE"],
                                            "ACTIVE" => "Y"
                                        );

                                        $r = CIBlockElement::GetList(
                                            Array("SORT" => "ASC"),
                                            $Filt,
                                            false,
                                            Array(),
                                            $Sel
                                        );



                                        while ($o = $r->Fetch()){
                                            $price = $o['PROPERTY_PRICE_1_VALUE'];

                                            $discont1 = 30;
                                            if($o['PROPERTY_DISCONT1_VALUE'] != NULL && $o['PROPERTY_DISCONT1_VALUE'] > 0){
                                                $discont1 =  $o['PROPERTY_DISCONT1_VALUE'];
                                            }
                                        }


                                        if($obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'] == 'Кирпичный'){

                                            $Categoryes[$key]["HOUSES"][$counter]['KIRP']['POST'] = $price;//$obs['PROPERTY_PRICE_HOUSE_VALUE'].'- '.$val.' '.$obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'].' '.$price;

                                        }else{

                                            $Categoryes[$key]["HOUSES"][$counter]['KIRP']['LETO'] = $price;//$obs['PROPERTY_PRICE_HOUSE_VALUE'].'- '.$val.' '.$obs['PROPERTY_TEHNOLOGY_HOUSE_VALUE'].' '.$price;


                                            $file = $_SERVER["DOCUMENT_ROOT"].'/upload/calcs/' . $obs["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' .$obs["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
                                            $f = fopen($file, 'r');
                                            $file_data = fread($f, filesize($file));
                                            fclose($f);


                                            $result = unserialize($file_data);
                                            if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный'){



                                                $price_opt = 0;
                                                $test = 0;


                                                foreach ($result[0]["ITEMS"] as $i) {
                                                    $p = (int) $i['PRICE'];
                                                    $test = $test + $p;
                                                    $price_opt = $price_opt + ($p - (($p * $discont1) / 100));
                                                }



                                                $str = $Categoryes[$key]["HOUSES"][$counter]['KIRP']['LETO'].'000';
                                                $Categoryes[$key]["HOUSES"][$counter]['KIRP']['ZIMA'] =  $str + $price_opt;


                                            }

                                        }

                                    }

                                }

                            }




                            $counter++;
                        }
                    }


                    //echo "<pre>";
                    //var_dump($Categoryes);






                    
                   
                    ?>


                    <?php $cnt = 1;?>
                    <?php foreach ($Categoryes as $key => $cat): ?>
                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="( my-collapse-btn + design-bordered ) collapsed" type="button" data-toggle="collapse" data-target="#price<?=$cnt?>" aria-expanded="false" aria-controls="price<?=$cnt?>">
                                    <div class="my-table">
                                        <div>
                                            <p><?php echo $cat["NAME"]; ?></p>
                                        </div>
                                        <div>
                                            <i class="glyphicon glyphicon-chevron-up"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="collapse my-collapse" id="price<?=$cnt?>">
                                    <div class="( my-collapse-content + design-bordered-none )">
                                                <table class="table table-striped table-prices">
                                                    <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>
                                                                    <p>Лето</p>
                                                                </th>
                                                                <th>
                                                                    <p>Зима</p>
                                                                </th>
                                                                <th>
                                                                    <p>Постоянное проживание</p>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    <tbody>
                                                    <?php foreach($cat["HOUSES"] as $v): ?>

                                                        <?php if(count($v['KARKAS']) > 0 
                                                                && (number_format($v['KARKAS']["LETO"], 3, ' ', ' ') != 0)
                                                                || number_format($v['KARKAS']["POST"], 3, ' ', ' ') != 0) :?>
                                                            <tr>
                                                                <td>
                                                                    <p>
                                                                        <a href="/tula<?=$v['LINK']?>"><?=$v['NAME']?> Каркас <?=$v['PROPERTY_SIZER_VALUE']?> м</a>
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <?php if($v['KARKAS']["LETO"]):?>
                                                                        <?=number_format($v['KARKAS']["LETO"], 3, ' ', ' ');?>
                                                                        <?php endif;?>
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <?php if($v['KARKAS']["ZIMA"]):?>
                                                                        <?=number_format($v['KARKAS']["ZIMA"], 0, ' ', ' ');?>
                                                                    <?php endif;?>
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <?php if($v['KARKAS']["POST"]):?>
                                                                        <?=number_format($v['KARKAS']["POST"], 3, ' ', ' ');?>
                                                                        <?php endif;?>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        <?php endif;?>





                                                        <?php if(count($v['BRUS']) > 0 
                                                                && (number_format($v['BRUS']["LETO"], 3, ' ', ' ') != 0)
                                                                || number_format($v['BRUS']["POST"], 3, ' ', ' ') != 0) :?>
                                                            <tr>
                                                                <td>
                                                                    <p>
                                                                        <a href="/tula<?=$v['LINK']?>?tech=brus"><?=$v['NAME']?> Брус <?=$v['PROPERTY_SIZER_VALUE']?> м</a>
                                                                    </p>
                                                                </td>

                                                                <td>
                                                                    <p>
                                                                        <?php if($v['BRUS']["LETO"]):?>
                                                                            <?=number_format($v['BRUS']["LETO"], 3, ' ', ' ');?>
                                                                        <?php endif;?>
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <?php if($v['BRUS']["ZIMA"]):?>
                                                                            <?=number_format($v['BRUS']["ZIMA"], 0, ' ', ' ');?>
                                                                        <?php endif;?>
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <?php if($v['BRUS']["POST"]):?>
                                                                            <?=number_format($v['BRUS']["POST"], 3, ' ', ' ');?>
                                                                        <?php endif;?>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        <?php endif;?>


                                                        <?php if(count($v['KIRP']) > 0 
                                                                && (number_format($v['KIRP']["LETO"], 3, ' ', ' ') != 0)
                                                                || number_format($v['KIRP']["POST"], 3, ' ', ' ') != 0) :?>
                                                            <tr>
                                                                <td>
                                                                    <p>
                                                                        <a href="/tula<?=$v['LINK']?>?tech=kirpich"><?=$v['NAME']?> Кирпичный <?=$v['PROPERTY_SIZER_VALUE']?> м</a>
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <?php if($v['KIRP']["LETO"]):?>
                                                                            <?=number_format($v['KIRP']["LETO"], 3, ' ', ' ');?>
                                                                        <?php endif;?>
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <?php if($v['KIRP']["ZIMA"]):?>
                                                                            <?=number_format($v['KIRP']["ZIMA"], 0, ' ', ' ');?>
                                                                        <?php endif;?>
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <?php if($v['KIRP']["POST"]):?>
                                                                            <?=number_format($v['KIRP']["POST"], 3, ' ', ' ');?>
                                                                        <?php endif;?>
                                                                    </p>
                                                                </td>

                                                            </tr>
                                                        <?php endif;?>




                                                    <?php endforeach; ?>
                                                    </tbody>

                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="5">
                                                                <button type="button" data-toggle="collapse" data-target="#price<?=$cnt?>" aria-expanded="false" aria-controls="price<?=$cnt?>">свернуть</button>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->
                        <? $cnt++; ?>
                    <?php endforeach; ?> 






                </div>
            </div>
        </div>


    </div>
</section>



<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>