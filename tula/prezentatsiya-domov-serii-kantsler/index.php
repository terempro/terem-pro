<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Презентация домов серии  &laquo;КАНЦЛЕР&raquo;");
$APPLICATION->SetPageProperty("keywords", "терем презентация, терем регистрация на презентацию, терем про канцлер тула");
$APPLICATION->SetTitle("Презентация домов серии  &laquo;КАНЦЛЕР&raquo;");
?>

<link href="style.css" rel="stylesheet">
<section class="content text-page white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    ПРЕЗЕНТАЦИЯ ДОМОВ СЕРИИ &laquo;КАНЦЛЕР&raquo;
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-12 col-sm-12">
                        <div class="row">
                            <p>Дорогие друзья, 27 октября на территории выставочного комплекса компании «Вятич» – официального партнера компании «Терем» в Туле – состоится презентация домов серии <a href="/catalog/filter/series_type-kanzler">«Канцлер»</a>. В программе мероприятия: интересная и полезная информация о технологиях строительства, выступления спикеров и все о коттеджном домостроении (в рамках домов серии <a href="/catalog/filter/series_type-kanzler">«Канцлер»</a>).<br>
                                Обязательно прохождение регистрации на сайте!<br>
                                По всем дополнительным вопросам обращайтесь по телефону:+7 (4872) 33-72-73 Наш адрес: г. Тула, ул. Галкина, д. 1 д с 9:00 до 20:00 (Автостанция Заречье).
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                            <h2 class="text-uppercase text-quest">Регистрация на презентацию</h2>
                            <form class="form-send" data-form="send" method="post" action="/include/callback.php">
                                <div class="row">
                                    <div class="col-xs-12 col-md-5 col-sm-6">
                                        <div class="col-xs-12">
                                            <input name="name" class="form-control my-input" placeholder="Ваше имя" required="required" type="text">
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-6">
                                        <div class="col-xs-12">
                                            <input name="phone" class="form-control my-input" data-item="phone" placeholder="Ваш телефон" required="required" type="tel">
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-12">
                                        <input type="hidden" name="city" class="form-control my-input" placeholder="Введите ваш город" value="Тула" required="">
                                        <button type="submit" name="btn" class="btn btn-danger btn-quest text-uppercase" style="pointer-events: all; cursor: pointer;">Зарегистрироваться</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <p><small><strong>Внимание!</strong> Все поля обязательны для заполнения. Отправляя форму, Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">Политикой обработки данных.</a></small></p>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>