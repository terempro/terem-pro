<?php

/*
 * Генератор CSV-фида для Google AdWords
 * создаёт CSV файл с актульным описанием и ценами для коттеджей
 */

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

\Bitrix\Main\Loader::includeModule('iblock');

$data = [];

// ID инфоблоков

const SERIES_IBLOCK = 13;
const VARIANTS_IBLOCK = 21;
const PRICE_IBLOCK = 22;

// Общие данные для всех домов

const ITEM_CATEGORY = 'Коттеджи';
const ITEM_DESCRIPTION = 'Дом от компании Теремъ';
const SITE_URL = 'https://www.terem-pro.ru';

// Запрашиваем серии

$series_request = CIBlockElement::GetList(
        
    ['id' => 'asc'],
    ['IBLOCK_ID' => SERIES_IBLOCK, 'SECTION_CODE' => 'kottedzhi'],
    0,
    0,
    ['ID', 'PROPERTY_KARKAS_VARIANTS_HOUSE']
        
);

while ($s = $series_request->GetNext())
{
    // Для каждой серии запрашиваем каркасные варианты
   
    if ($s['PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE'][0])
    {
        $info = [];
        
        $variant = CIBlockElement::GetList(
                
            ['sort' => 'asc'],
            [
                'IBLOCK_ID' => VARIANTS_IBLOCK, 
                'ID' => $s['PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE'][0]
            ],
            0,
            0,
            ['ID', 'NAME', 'DETAIL_PICTURE', 'PROPERTY_PRICE_HOUSE']    
        )->GetNext();
        
        $info['id'] = $variant["ID"];
        $info['name'] = trim($variant["NAME"]);
        $info['picture'] = SITE_URL.CFile::GetPath($variant["DETAIL_PICTURE"]);
        
        // Для каждого варианта получаем цены
        
        $pricelist = CIBlockElement::GetList(
                
            ['sort' => 'asc'],
            [
                'IBLOCK_ID' => PRICE_IBLOCK, 
                'ID' => $variant['PROPERTY_PRICE_HOUSE_VALUE']
            ],
            0,
            0,
            ['PROPERTY_PRICE_1', 'PROPERTY_PRICE_2', 'PROPERTY_LINK']
        
        )->GetNext();
        
        $info['price'] = number_format($pricelist["PROPERTY_PRICE_2_VALUE"]).' RUB';
        $info['sale_price'] = number_format($pricelist["PROPERTY_PRICE_1_VALUE"]).' RUB';
        $info['url'] = SITE_URL.$pricelist["PROPERTY_LINK_VALUE"];
        
        $data[] = $info;     
    }  
}

// Формирование CSV

$h = fopen('adwords.txt', 'w');

fputs($h, "ID\tItem Title\tFinal URL\tImage URL\tItem subtitle\tItem description\tItem category\tPrice\tSale price\tContextual keywords\tItem address\tTracking template\r\n");

foreach ($data as $d)
{
    fprintf($h, "%d\t%s\t%s\t%s\t%s\t%s\t%s\t\"%s\"\t\"%s\"\t%s\t%s\t%s\r\n",
            
        $d['id'],               // ID
        $d['name'],             // Item name
        $d['url'],              // Final URL
        $d['picture'],          // Image URL   
        '',                     // Item subtitle
        ITEM_DESCRIPTION,       // Item description
        ITEM_CATEGORY,          // Item category
        $d['price'],            // Price
        $d['sale_price'],       // Sale price
        '',                     // Contextual keywords,
        '',                     // Item address
        ''                      // Tracking template
    );
}

fclose($h);

header('location: adwords.txt');