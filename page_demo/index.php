<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по сайту");
?>

<section class="content text-page white" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side + my-margin clearfix">
					<div class="my-text-title text-center text-uppercase desgin-h1">
						<h1>Брусовые дома</h1>
					</div>
					<div class="text-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<br>
					</div>

					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<div class="img-with-description">
								<img src="http://placehold.it/850x450" class="full-width">
								<p><i>Lorem ipsum dolor sit amet</i></p>
								
							</div>
						</div>
					</div>
					
					<div class="text-title">
						<h2 class="text-uppercase">Lorem ipsum dolor sit amet</h2>
					</div>
					<div class="text-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<br>
					</div>
					<div class="arrow-title">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<img src="/bitrix/templates/.default/assets/img/expert/sep2.png" alt="arrow-left" class="full-width">
							</div>
							<div class="col-xs-12 col-sm-10 col-md-10 text-center col-sm-offset-1 col-md-offset-1">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12">
								<img src="/bitrix/templates/.default/assets/img/expert/sep1.png" alt="arrow-right" class="full-width">
							</div>
						</div>
						
						
					</div>

					<div class="text-with-img right-img">
						<div class="row">
							<div class="text-title">
								<div class="col-xs-12 col-md-12 col-sm-12">
									<h2 class="text-uppercase">Lorem ipsum dolor sit amet</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="text-description">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate </p>
									
								</div>
							</div>
							<div class="col-xs-12 col-md-4 col-sm-4">
								<div class="img-with-description">
									<img src="http://placehold.it/450x450" class="full-width">
									<p><i>Lorem ipsum dolor sit amet</i></p>
									
								</div>
							</div>
						</div>
						<br>
					</div>
					
					<div class="text-title">
						<h2 class="text-uppercase">Lorem ipsum dolor sit amet</h2>
					</div>
					<div class="text-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<br>
					</div>

					<div class="row">
						<div class="col-xs-12 col-md-3 col-sm-3">
							<div class="img-with-description">
								<img src="http://placehold.it/450x450" class="full-width">
								<p><i>Lorem ipsum dolor sit amet</i></p>
								
							</div>
						</div>
						<div class="col-xs-12 col-md-3 col-sm-3">
							<div class="img-with-description">
								<img src="http://placehold.it/450x450" class="full-width">
								<p><i>Lorem ipsum dolor sit amet</i></p>
								
							</div>
						</div>
						<div class="col-xs-12 col-md-3 col-sm-3">
							<div class="img-with-description">
								<img src="http://placehold.it/450x450" class="full-width">
								<p><i>Lorem ipsum dolor sit amet</i></p>
								
							</div>
						</div>
						<div class="col-xs-12 col-md-3 col-sm-3">
							<div class="img-with-description">
								<img src="http://placehold.it/450x450" class="full-width">
								<p><i>Lorem ipsum dolor sit amet</i></p>
								
							</div>
						</div>
					</div>

					<div class="text-title">
						<h2 class="text-uppercase">Lorem ipsum dolor sit amet</h2>
					</div>
					<div class="text-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<br>
					</div>

					<div class="arrow-title">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<img src="/bitrix/templates/.default/assets/img/expert/sep2.png" alt="arrow-left" class="full-width">
							</div>
							<div class="col-xs-12 col-sm-10 col-md-10 text-center col-sm-offset-1 col-md-offset-1">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12">
								<img src="/bitrix/templates/.default/assets/img/expert/sep1.png" alt="arrow-right" class="full-width">
							</div>
							<br>
						</div>
					</div>

					<div class="text-title">
						<h2 class="text-uppercase">Lorem ipsum dolor sit amet</h2>
					</div>
					<div class="text-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<br>
					</div>

					<div class="row">
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="img-with-description">
								<img src="http://placehold.it/450x450" class="full-width">
								<p><i>Lorem ipsum dolor sit amet</i></p>
								
							</div>
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="img-with-description">
								<img src="http://placehold.it/450x450" class="full-width">
								<p><i>Lorem ipsum dolor sit amet</i></p>
								
							</div>
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="img-with-description">
								<img src="http://placehold.it/450x450" class="full-width">
								<p><i>Lorem ipsum dolor sit amet</i></p>
							
							</div>
						</div>
					</div>

					<!-- <div class="text-with-img left-img">
						<div class="row">
							<div class="col-xs-12 col-md-4 col-sm-4">
								<div class="img-with-description">
									<img src="http://placehold.it/850x450" class="full-width">
									<p><i>Lorem ipsum dolor sit amet</i></p>
									<br>
								</div>
							</div>
							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="text-title">
									<h2 class="text-uppercase">Lorem ipsum dolor sit amet</h2>
								</div>
								<div class="text-description">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
									<br>
								</div>
							</div>
							
						</div>
					</div> -->
					
					
					<div class="text-title">
						<h2 class="text-uppercase">Lorem ipsum dolor sit amet</h2>
					</div>
					<div class="text-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<br>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>