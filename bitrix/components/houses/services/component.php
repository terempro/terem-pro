<?php
$resHouses = CIBlockElement::GetList(Array("SORT" => "ASC"), $arParams['AR_FILTER'],
    false, false, ['ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE',
    'PROPERTY_' . $arParams['PRICES_PROPERTY'], 'PROPERTY_SIZE_HOUSE', 'PROPERTY_SIZE1',
    'PROPERTY_SIZE2', 'PROPERTY_SQUARE_ALL_HOUSE', 'PROPERTY_SQUARE_HOUSE',
    'PROPERTY_SQUARE_LIFE_HOUSE']);
//варианты домов
while($house = $resHouses->Fetch())
{
	if($house['PREVIEW_PICTURE']
		and $picture = CFile::ResizeImageGet($house['PREVIEW_PICTURE'],
			array('width' => 285, 'height' => 285), BX_RESIZE_IMAGE_EXACT))
				$house['PICTURE'] = $picture['src'];
	elseif($house['DETAIL_PICTURE']
		and $picture = CFile::ResizeImageGet($house['DETAIL_PICTURE'],
			array('width' => 285, 'height' => 285), BX_RESIZE_IMAGE_EXACT))
				$house['PICTURE'] = $picture['src'];
	else
		continue;
	//дома без картинок едва ли уместны
	$priceHouse = CIBlockElement::GetList(["PRICE_1" => "ASC", "PRICE_2" => "ASC"],
	    ['IBLOCK_ID' => $arParams['PRICES_IBLOCK'], 'ACTIVE' => 'Y',
	    'ID' => $house['PROPERTY_' . $arParams['PRICES_PROPERTY'] . '_VALUE'],
	    ['LOGIC' => 'OR', ['!PROPERTY_PRICE_1' => false], ['!PROPERTY_PRICE_2' => false]]],
	    false, ['nTopCount' => 1], ['ID', 'IBLOCK_ID', 'PROPERTY_PRICE_1', 'PROPERTY_PRICE_2']);
	//цены
	if ($price = $priceHouse->Fetch())
	{
		if($price['PROPERTY_PRICE_1_VALUE'])
			$house['PRICE'] = number_format($price['PROPERTY_PRICE_1_VALUE'], 0, '', ' ');
		elseif($price['PROPERTY_PRICE_2_VALUE'])
			$house['PRICE'] = number_format($price['PROPERTY_PRICE_2_VALUE'], 0, '', ' ');
		else
			continue;
		//каталог домов
		$catHouse = CIBlockElement::GetList(Array("SORT" => "ASC"), ['IBLOCK_ID' => 13,
		    'ACTIVE' => 'Y', ['LOGIC' => 'OR', ['PROPERTY_VARIANTS_HOUSE' => $house['ID']],
		    ['PROPERTY_KARKAS_VARIANTS_HOUSE' => $house['ID']],
		    ['PROPERTY_BRUS_VARIANTS_HOUSE' => $house['ID']],
		    ['PROPERTY_KIRPICH_VARIANTS_HOUSE' => $house['ID']]]], false, ['nTopCount' => 1]);
		//ссылки нормальные у элементов каталога домов
		if ($addInHouse = $catHouse->GetNext())
			$house['URL'] = $addInHouse['DETAIL_PAGE_URL'];
		//размеры
		if($house['PROPERTY_SIZE_HOUSE_VALUE'])
			$house['SIZE'] = $house['PROPERTY_SIZE_HOUSE_VALUE'];
		elseif($house['PROPERTY_SIZE1_VALUE'] and $house['PROPERTY_SIZE2_VALUE'])
			$house['SIZE'] = $house['PROPERTY_SIZE1_VALUE'] . 'x' . $house['PROPERTY_SIZE2_VALUE'];
		else
			continue;
		//площадь
		if($house['PROPERTY_SQUARE_ALL_HOUSE_VALUE'])
			$house['AREA'] = $house['PROPERTY_SQUARE_ALL_HOUSE_VALUE'];
		elseif($house['PROPERTY_SQUARE_HOUSE_VALUE'])
			$house['AREA'] = $house['PROPERTY_SQUARE_HOUSE_VALUE'];
		elseif($house['PROPERTY_SQUARE_LIFE_HOUSE_VALUE'])
			$house['AREA'] = $house['PROPERTY_SQUARE_LIFE_HOUSE_VALUE'];
		else
			continue;
		//массив вывода
		$arResult['HOUSES'][] = $house;
	}
}
if($arResult['HOUSES'])
	$this->IncludeComponentTemplate();
