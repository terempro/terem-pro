<section class="content" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="flex-reversed">
					<?php
					$end = count($arResult['HOUSES']) - 1;
					//дома
					foreach ($arResult['HOUSES'] as $key => $house)
					{ 
						if($key % 2 == 0)
						{ ?>
							<div class="row">
							<?php
						} ?>
						<div class="col-xs-12 col-md-6 col-sm-12">
							<div class="my-promotion desgin-3 my-margin">
								<div>
									<a href="<?=$house['URL'] ?>">
										<img src="<?=$house['PICTURE'] ?>"></a>
								</div>
								<div>
									<h4 class="text-uppercase"><?=$house['NAME'] ?>
										<br/><span class="material">Кирпич</span>
										<br/><span class="sizes"><?=$house['SIZE'] ?> м
										<br/><?=$house['AREA'] ?> м<sup>2</sup></span></h4>
									<h3>
										<span><?=$house['PRICE'] ?> р.</span></h3>
								</div>
								<a class="all-item-link" href="<?=$house['URL'] ?>"></a>
							</div>
						</div>
						<?php
						if($key % 2 > 0 or $key == $end)
						{ ?>
							</div>
							<?php
						}
					} ?>
				</div>
			</div>
		</div>
	</div>
</section>