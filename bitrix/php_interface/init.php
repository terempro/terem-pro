<?php
/*// ROISTAT BEGIN
function sendToRoistat($WEB_FORM_ID, &$arFields, &$arrVALUES)
{
    if (array_key_exists('debugroistat', $_COOKIE) and $_COOKIE['debugroistat'] === 'secretcookieroistat')
    {
        echo '<pre>';
        var_dump($_REQUEST);
        echo '</pre>';
        die;
    }
}

AddEventHandler('form', 'onBeforeResultAdd', 'sendToRoistat');
// ROISTAT END*/

require_once("Mobile_Detect.php");
require_once("excel/reader.php");
require_once("CAddServicesParcer.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/include/classes/IBlockChangeHandler.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/include/app/core.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/include/app/classes/HouseCatalog.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/include/classes/libmail.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/include/classes/libmail_idna_convert.php");
// require_once($_SERVER["DOCUMENT_ROOT"]."/include/classes/sxgeo/SxGeo.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/include/classes/sxgeo/SxGeo_new.php");

$SxGeo = new SxGeo();

$CITIES_ARRAY = array(
    'MOSCOW' =>  'Москва',
    /*'KAZAN' => 'Казань',*/
    'VOLOGDA' => 'Вологда',
    'TULA' => 'Тула',
    /*'BRYANSK' => 'Брянск',*/
    'SARATOV' => 'Саратов',
    'NOVOSIBIRSK' => 'Новосибирск',
    'IZHEVSK' => 'Ижевск',
    /*'KRASNODAR' => 'Краснодар',*/
    'RYAZAN' => 'Рязань',
    'VLADIMIR' => 'Владимир',
	'YAROSLAVL' => 'Ярославль',
);

$CITY_CODES_ARRAY = array(
    'MOSCOW' =>  22,
    /* 'KAZAN' => 51, */
    'VOLOGDA' => 52,
    'TULA' => 66,
    /* 'BRYANSK' => 77, */
    'SARATOV' => 82,
    'NOVOSIBIRSK' => 86,
    'IZHEVSK' => 91,
    /* 'KRASNODAR' => 96, */
    'RYAZAN' => 100,
    'VLADIMIR' => 106,
	'YAROSLAVL' => 114,
);

if (!function_exists("mysql_escape_string"))
{
    function mysql_escape_string($str)
    {
        $replaces = array(
                "\x00" => "\\x00",
                "\x1a" => "\\x1a",
                "\n" => "\\n",
                "\r" => "\\r",
                "\\" => "\\\\",
                "'" => "\'",
                '"' => '\"'
            );

        return strtr($str, $replaces);
    }
}

function resetString($str){
    $str = strip_tags($str);
    $str = htmlspecialchars($str);
    $str = mysql_escape_string($str);
    return $str;
}

function v($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

function ConsiderCity()
{
    global $CITIES_ARRAY, $CITY_CODES_ARRAY, $CURRENT_CITY_CODE, $PRICE_SUFFIX, $PRICE_IBLOCK_ID, $SxGeo, $CURRENT_CITY;

	// для ссылок из раздела контакты можно передавать параметр	

	if (strip_tags($_GET['city'])) {
		$str = $_GET['city'];
		$str = trim($str);
		$str = stripslashes($str);
		$str = htmlspecialchars($str);	
		$_SESSION['city'] = $str; 	
	}		
	
    if (!in_array(($_SESSION['city']), $CITIES_ARRAY))
        $_SESSION['city'] = 'Москва';

    $path = $_SERVER['REQUEST_URI'];
    //оттуда редиректить не надо
    if (preg_match('/^\/(api|bitrix|include|upload|kitchen)/', $path))
        return;

    $cities_regexp = strtolower(implode('|', array_keys($CITIES_ARRAY)));
    $city_matches = array();
    $session_code = array_search($_SESSION['city'], $CITIES_ARRAY);

    if ($session_code == 'MOSCOW')
        $session_code = '';
    //если в УРЛе нет города из сессии
    if ($session_code and !preg_match('/^\/' . strtolower($session_code) . '/', $path))
    {
        if (preg_match('/(' . $cities_regexp . ')/', $path, $city_matches))
            $path = str_replace('/' . $city_matches[0], '', $path);
        //переходим на город из сессии
        LocalRedirect('/' . strtolower($session_code) . $path);
    }
    elseif (!$session_code and preg_match('/(' . $cities_regexp . ')/', $path, $city_matches))
    {
        $path = str_replace('/' . $city_matches[0], '', $path);
        LocalRedirect($path);
    }
    if (!$_SESSION['city'] or !in_array(($_SESSION['city']), $CITIES_ARRAY))
        $CURRENT_CITY = $SxGeo->City() ? : 'Москва';
    else
        $CURRENT_CITY = $_SESSION['city'];
    // Получаем нужную информацию о городе
    $CURRENT_CITY_CODE = array_search($CURRENT_CITY, $CITIES_ARRAY);
    $PRICE_SUFFIX = $CURRENT_CITY_CODE == 'MOSCOW' ? '' : '_' . $CURRENT_CITY_CODE;
    $PRICE_IBLOCK_ID = $CITY_CODES_ARRAY[$CURRENT_CITY_CODE];
}

function AddTask($task)
{
    $tasks = explode(',', file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/include/tasks/tasklist.txt'));

    if (!$tasks[0])
        $tasks = array();
    
    if (!in_array($task, $tasks))
        $tasks[] = $task;

    file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/include/tasks/tasklist.txt', implode(',', $tasks));
}

// Запрет удаления элементов инфоблока
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("IBlockChangeHandler", "ElementDeletionPreventer"));
// Отслеживание изменений элементов инфоблока
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("IBlockChangeHandler", "ElementUpdateHandler"));
// Отслеживание добавления элементов в инфоблок
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("IBlockChangeHandler", "ElementAdditionHandler"));
// Отслеживание удаления элементов из инфоблока
AddEventHandler("iblock", "OnAfterIBlockElementDelete", Array("IBlockChangeHandler", "ElementDeletionHandler"));
// Назначение ежедневных операций
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("IBlockChangeHandler", "LaunchDailyOperations"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("IBlockChangeHandler", "LaunchDailyOperations"));
AddEventHandler("iblock", "OnAfterIBlockElementDelete", Array("IBlockChangeHandler", "LaunchDailyOperations"));


function isCard()
{

        global $APPLICATION;

        $page = $APPLICATION->GetCurPage();
        $isCard = false;

        $arFilter = Array('IBLOCK_ID'=>13);
        $db_list = CIBlockSection::GetList(Array(), $arFilter, true);

          while($ar_result = $db_list->GetNext())
          {
            $arURL[] = $ar_result['CODE'];
          }

          foreach ($arURL as $key => $value) {

            if(strripos($page, $value)){
                $str = explode('/', $page);

                if(count($str) >= 5){
                    $isCard =  true;
                }

            }

          }


        return $isCard;

}

//Прмяоугольник
function thumb2($path, $side_x = 640, $side_y = 480, $rewrite = FALSE) {

    $new_img_path = substr($path, 0, strlen($path) - 4).'_thumb.jpg';

    if (!$rewrite and file_exists($new_img_path)) {
        return $new_img_path;
    }

    $img_src = imagecreatefromjpeg($path);

    $img_dest = imagecreatetruecolor($side_x, $side_y);
    $color = imagecolorallocate($img_dest, 255, 255, 255);
    imagefill($img_dest, 0, 0, $color);

    $x = imagesx($img_src);
    $y = imagesy($img_src);

    $ratio = max($x / $side_x, $y / $side_y);

    $width = round($x / $ratio);
    $height = round($y / $ratio);

    $left = round(($side_x - $width) / 2);
    $top = round(($side_y - $height) / 2);

    imagecopyresampled($img_dest, $img_src, $left, $top, 0, 0, $width, $height, $x, $y);

    imagejpeg($img_dest, $new_img_path);

    return $new_img_path;


}

//Квадрат
function thumb($path, $side = 250) {

    $info = pathinfo($path);
    $file = basename($path, '.' . $info['extension']);

    $new_img_file = $info['dirname'] . '/' . $file . '_thumb.jpg';

    if (file_exists($new_img_file)) {
        return $new_img_file;
    }

    $info = getimagesize($path);
    $w = $info[0];
    $h = $info[1];
    $mime = $info['mime'];

    if ($w > $h) {
        $h = round($h / floatval($w / $side));
        $w = $side;

        $left = 0;
        $top = round(($side - $h) / 2);
    } elseif ($h > $w) {
        $w = round($w / floatval($h / $side));
        $h = $side;

        $left = round(($side - $w) / 2);
        $top = 0;
    } else {
        $w = $side;
        $h = $side;
        $left = 0;
        $top = 0;
    }

    switch ($mime) {
        case 'image/jpeg':
            $img_original = imagecreatefromjpeg($path);
            break;
        case 'image/png':
            $img_original = imagecreatefrompng($path);
            break;
    }

    $img_dest = imagecreatetruecolor($side, $side);
    $color = imagecolorallocate($img_dest, 255, 255, 255);
    imagefill($img_dest, 0, 0, $color);

    imagecopyresampled($img_dest, $img_original, $left, $top, 0, 0, $w, $h, imagesx($img_original), imagesy($img_original));

    imagejpeg($img_dest, $new_img_file, 100);

    return $new_img_file;

}


//Окончание слова
function declension_words($num,$arWords)
{
    if ($num < 21){
        if ($num == 1)
            $w = $arWords[0];
        elseif ($num > 1 and $num < 5)
            $w = $arWords[1];
        else
            $w = $arWords[2];
        return $w;
    } else {
        $l = (int)substr($num, -1);
        if ($l == 1)
            $w = $arWords[0];
        elseif ($l > 1 and $l < 5)
            $w = $arWords[1];
        else
            $w = $arWords[2];
        return $w;
    }
}


// Getting params

if (isset($_GET['utm_campaign']) and isset($_GET['utm_term']))
{
    $utm_campaign = htmlspecialchars($_GET['utm_campaign']);
    $utm_term = explode('|',htmlspecialchars($_GET['utm_term']))[0];

    setcookie('utm_campaign', $utm_campaign, time()+86400*90, '/');
    setcookie('utm_term', $utm_term, time()+86400*90, '/');
    setcookie('entrance_source', 'Яндекс', time()+86400*90, '/');

    if (isset($_GET['utm_content']))
    {
        $utm_content = explode('|', htmlspecialchars($_GET['utm_content']))[0];
    }

    setcookie('utm_content', $utm_content, time()+86400*90, '/');
}
else
{
    $url = $_SERVER["HTTP_REFERER"];
    
    if (preg_match('/^http(s?):\/\/((www\.)?)google\./i', $url))
    {
        setcookie('entrance_source', 'Google', time()+86400*90, '/');
    }
    elseif (preg_match('/^http(s?):\/\/((www\.)?)yandex\./i', $url))
    {
        setcookie('entrance_source', 'Яндекс', time()+86400*90, '/');
    }
}
session_start();
$APPLICATION->IncludeComponent('terem:city', '.default');
