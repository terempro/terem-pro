<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE)
    throw new \RuntimeException('Bitrix core not included');

        const CATALOG_AS_FILE_PROPERTY_ID = 69;
        const CATALOG_VARIAN_HOUSE = 21;
        const IBLOCK_ADDITIONAL_SERVICE = 24;
        const ADD_SERVICES_PRICE_PROPERTY_ID = 84;
        const ADD_SERVICES_SHOW_PROPERTY_ID = 85;
        const ADD_SERVICES_UNIT_PROPERTY_ID = 86;
        const ADD_SERVICES_SHOW_PROPERTY_VALUE_ID = 90;

if (CModule::IncludeModule("iblock")) {

    class CAddServicesParcer {

        public function ParceFileData($arFields) {


            $arSelect = Array("ID", "PROPERTY_ID_CALCULATOR",);
            $arFilter = Array("IBLOCK_ID" => 21, "ID" => $arFields["ID"]);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arF[] = $ob->GetFields();
            }

            if ($arF[0]["PROPERTY_ID_CALCULATOR_VALUE"] == NULL) {

                $file = CAddServicesParcer::getFilePath($arFields);
                if ($file) {
                    
                    $dirname = date("d-m-Y__H-i-s")."__".bin2hex(random_bytes(4));
                    $dir = mkdir("../../upload/calcs/" . $dirname, 0700);
                    //chmod("../../upload/calcs/" . $dirname, 0775);

                    if ($dir) {
                        $name = "calc_" . $dirname . ".data";


                        $data = CAddServicesParcer::readExelFile($file);

                        CAddServicesParcer::saveData($data, $name, $dirname);

                        CIBlockElement::SetPropertyValueCode($arFields['ID'], 87, $dirname);
                    } else {
                        echo "Не удалось создать директорию! Сообщите администратору.";
                        die();
                    }

                }
            }
        }

        public static function getFilePath($arFields) {
            $arSelect = Array(
                "ID",
                "IBLOCK_ID",
                "PROPERTY_AS_FILE",
                "PROPERTY_ID_CALCULATOR"
            );
            $arFilter = Array(
                "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                "ACTIVE" => "Y",
                "ID" => $arFields['ID']
            );

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelect);
            if ($arItem = $res->GetNext()) {
                $arFile = CFile::GetFileArray($arItem['PROPERTY_AS_FILE_VALUE']);
                if ($arFile['SRC']) {
                    return $_SERVER['DOCUMENT_ROOT'] . $arFile['SRC'];
                } else {
                    return false;
                }
            }
        }

        function readExelFile($filepath) {
            $Excel = new Spreadsheet_Excel_Reader(); // создаем объект
            $Excel->setOutputEncoding('cp1251'); // устанавливаем кодировку
            $Excel->read($filepath); // открываем файл
            $count = $Excel->sheets[0]['numRows']; // узнаем количество строк в 1 листе
            // с помощью цикла выводим все ячейки

            $data = array();
            for ($rowNum = 1; $rowNum <= $count; $rowNum++) {
                $data[] = array(
                    0 => iconv('cp1251', 'utf-8', $Excel->sheets[0]['cells'][$rowNum][1]),
                    1 => iconv('cp1251', 'utf-8', $Excel->sheets[0]['cells'][$rowNum][2]),
                    2 => iconv('cp1251', 'utf-8', $Excel->sheets[0]['cells'][$rowNum][3]),
                    5 => iconv('cp1251', 'utf-8', $Excel->sheets[0]['cells'][$rowNum][6]),
                );
            }

            return $data;
        }

        function saveData($data, $name, $dirname) {
            $category = array();
            $result = array();

            foreach ($data as $v) {
                if ((int) $v[0]) {
                    if (!in_array($v[2], $category)) {
                        $result[] = array(
                            'NAME' => $v[2],
                            'ITEMS' => array(),
                        );
                        $category[] = $v[2];
                    }
                }
            }

            foreach ($result as $k => $v) {
                foreach ($data as $d) {
                    if ($d[2] == $v['NAME']) {
                        $result[$k]['ITEMS'][] = array(
                            'NAME' => $d[1],
                            'PRICE' => $d[5],
                        );
                    }
                }
            }

            $file_data = serialize($result);
            $f = fopen('../../upload/calcs/' . $dirname . "/" . $name, 'w');
            fwrite($f, $file_data);
            fclose($f);
        }

        function loadData($file) {
            $f = fopen($file, 'r');
            $file_data = fread($f, filesize($file));
            fclose($f);

            return unserialize($file_data);
        }

    }

}


