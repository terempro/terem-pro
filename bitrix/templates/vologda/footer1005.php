</div>

<section class="footer">
  <div class="container">
   <div class="row footer-lvl-1">
    <div class="col-xs-12 col-md-5 col-sm-5">
    

      <p class="text-danger"><i>Строительная компания «Теремъ» приветствует вас!</i></p>


      <h4>
        ДОСТУПНОСТЬ ЦЕН НА ДОМА –<br>ОДИН ИЗ ОСНОВНЫХ ПРИНЦИПОВ НАШЕЙ РАБОТЫ
      </h4>
    </div>
  </div>

  <div class="row footer-lvl-2">
    <div class="col-xs-12 col-md-3 col-sm-3">
      <h3>КАТАЛОГ</h3>
      <ul>
		<li><a href="/catalog/">Деревянные дома</a></li>
        <li><a href="/catalog/bani/">Бани</a></li>
        <li><a href="/catalog/dachnye-doma/">Дачные дома</a></li>
        <li><a href="/catalog/malye-stroeniya-besedki/">Малые строения</a></li>
        <li><a href="/catalog/sadovye-doma/">Садовые дома</a></li>
        <li><a href="/catalog/kottedzhi/">Коттеджи</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-md-3 col-sm-3">
      <h3>УСЛУГИ</h3>
      <ul>
        <li><a href="/services/enginering_communications/">Инженерные системы</a></li>
        <li><a href="/services/individual_projects/">Индивидуальные проекты</a></li>
        <li><a href="/services/dostroika/">Достройка и реконструкция</a></li>
        <li><a href="/services/the-estates-catalogue/">Благоустройство участка</a></li>
        <li><a href="/services/addition2/">Пристройка террас и веранд</a></li>
        <li><a href="http://zemli.terem-pro.ru/" target="_blank">Земельные участки</a></li>
        <li><a href="/services/credit-calculator/">Кредитование и страхование</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-md-3 col-sm-3">
      <h3>О КОМПАНИИ</h3>
      <ul>
        <li><a href="/about/news/">Новости</a></li>
        <li><a href="/promotion/">Акции</a></li>
        <li><a href="/about/otzyvy/">Отзывы</a></li>
        <li><a href="/about/video/">Видео</a></li>
        <li><a href="/about/faq/">Ответы на вопросы</a></li>
        <li><a href="/about/production/">Технологии производства</a></li>
        <li><a href="/contacts/">Контакты</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-md-3 col-sm-3">
      <div class="social">
        <p>Присоединяйтесь к нам!</p>
        <ul>
          <li>
            <a target="_blank" href="https://www.facebook.com/terempro"><img src="/bitrix/templates/.default/assets/img/social/Terem_fbook_icon.png" alt="facebook"></a>
            <a rel="nofollow" target="_blank" href="http://instagram.com/pro_terem"><img src="/bitrix/templates/.default/assets/img/social/Terem_insta_icon.png" alt="instagram"></a>
            <a rel="nofollow" target="_blank" href="https://twitter.com/ProTerem"><img src="/bitrix/templates/.default/assets/img/social/Terem_tw_icon.png" alt="twitter"></a>
            <a rel="nofollow" target="_blank" href="http://vk.com/pro_terem"><img src="/bitrix/templates/.default/assets/img/social/Terem_VK_icon.png" alt="vkontakte"></a>
          </li>
          <li>
            <a rel="nofollow" target="_blank" href="http://terem-pro.livejournal.com"><img src="/bitrix/templates/.default/assets/img/social/Terem_LiveJ_icon.png" alt="livejournal"></a>
            <a rel="nofollow" target="_blank" href="http://ok.ru/v2010godu"><img src="/bitrix/templates/.default/assets/img/social/od.png" alt="odnoklasniki"></a>
            <a rel="nofollow" target="_blank" href="http://www.terem-pro.ru/sitemap.xml"><img src="/bitrix/templates/.default/assets/img/social/map.png" alt="map"></a>
            <a rel="nofollow" target="_blank" href="https://www.youtube.com/c/terempro"><img src="/bitrix/templates/.default/assets/img/social/y.png" alt="map"></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row footer-lvl-2">
    <div class="col-xs-12 col-md-12 col-sm-12">
      <p>&copy; Теремъ-про, 2009 - <?=date("Y")?>. Все материалы данного сайта являются объектами авторского права (в том числе дизайн). Запрещается копирование, распространение (в том числе путем копирования на другие сайты и ресурсы в Интернете) или любое иное использование информации и объектов без предварительного согласия правообладателя. Cайт не является публичной офертой.</p>
    </div>
  </div>
</div>
</section>
<a href="#0" class="cd-top">Наверх</a>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
  (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
      try {
        w.yaCounter937330 = new Ya.Metrika({id:937330,
          webvisor:true,
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          ut:"noindex"});
      } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
      d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
  })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/937330?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- POZVONIM -->
<!--script crossorigin="anonymous" async type="text/javascript" src="//api.pozvonim.com/widget/callback/v3/323b31e035c7a1db05113730eb1789dd/connect" id="check-code-pozvonim" charset="UTF-8"></script-->
<!-- EN POZ -->

<!--Google analytics--><script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55697220-1', 'auto');
ga('send', 'pageview');

</script>
<script>
  window.onload = function()
  {
   if(jQuery('.catalog-item-description').length ==1)
   {
     jQuery('.my-form.form-advice').submit(function(){
       ga('send','event','form','submit','получить консультацию');
     });
   }
   else
   {

     jQuery('.my-form.form-advice').submit(function(){
       ga('send','event','form','submit','заказать');
     });
   }
 };
</script>
<!--Google analytics end-->

<!-- google remarkiting-->

<!-- Код тега ремаркетинга Google -->
<!--------------------------------------------------
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 984788836;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/984788836/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<!-- {literal} -->





<!-- end google remarkiting -->
<?php include($_SERVER["DOCUMENT_ROOT"]."/include/modals/modals.php"); ?>
<?php 
$page = $APPLICATION->GetCurPage();
?>

<?php if($page == '/contacts/'):?>
  <script src='//api-maps.yandex.ru/2.1/?lang=ru_RU'></script>
<?php endif;?>

<script src="/bitrix/templates/.default/assets/js/owl.carousel.min.js"></script>
<link href='/bitrix/templates/.default/assets/css/owl.carousel.css' property='stylesheet' rel='stylesheet' type='text/css'>
<link href='/bitrix/templates/.default/assets/css/jquery.fancybox.css' property='stylesheet' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/form.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/validator.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.transit.min.js"></script> 
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.threesixty.min.js"></script> 
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/main.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/new_filter.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.mousewheel.min.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
</body>
</html>