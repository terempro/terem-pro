<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?>



<?if (!empty($arResult)):?>
<ul class="nav navbar-nav hidden-xs">

    <?
    $previousLevel = 0;
    $cnt = 0;
    foreach($arResult as $arItem):?>

    <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
    <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
    <?endif?>

    <?if ($arItem["IS_PARENT"]):?>

    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
    <li>
        <div class="has-inner-menu">
            <a href="<?= $arItem["LINK"] ?>">
                <span><?= $arItem["TEXT"] ?></span>
                <span class="glyphicon glyphicon-menu-down"></span>
                <span class="glyphicon glyphicon-menu-up"></span>
            </a>

            <ul class="dropdown-menu">
                <?else:?>
                <li><?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?= $arItem["LINK"] ?>" class="parent"><?= $arItem["TEXT"] ?></a>
                    <ul>
                        <?endif?>

                        <?else:?>

                        <?if ($arItem["PERMISSION"] > "D"):?>

                        <?php if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li><a href="<?= $arItem["LINK"] ?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?= $arItem["TEXT"] ?></a></li>
                        <?php elseif(in_array($arItem["LINK"], ['/promotion/', '/about/news/'])):?>

                        <li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="/saratov<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
                        <?if($cnt == 2):?>

                        <?php endif;?>
                        <?endif?>
                        <?endif?>

                        <?endif?>

                        <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
                        <?if($cnt == 2):?>
                        <? $cnt = 0; ?>
                        <?else:?>
                        <? $cnt++; ?>
                        <?endif;?>
                        <?endforeach?>

                        <?if ($previousLevel > 1)://close last item tags?>
                        <?= str_repeat("</ul></div></li>", ($previousLevel - 1)); ?>
                        <?endif?>

                        <li>

                            <a href="#" class="phone hidden-xs"><span id="telMain2"> +7 (8452) 33-81-44</span></a>
                            <a href="#" class="modal-call" data-target="#call" data-toggle="modal">обратный звонок</a>


                            <?$APPLICATION->IncludeComponent(
                            "bitrix:search.form",
                            "search_template",
                            Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "PAGE" => "#SITE_DIR#search/index.php",
                            "USE_SUGGEST" => "N"
                            )
                            );?>

                        </li>

                    </ul>
                    <div class="menu-clear-left"></div>
                    <?endif?>
