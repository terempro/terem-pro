<?php

$db = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>$arResult['IBLOCK_ID'], 'ID'=>$arResult['ID']), false, false, array('SHOW_COUNTER'));
if ($i = $db->Fetch()){
    $arResult["SHOW_COUNTER"] = $i['SHOW_COUNTER'];
}

global $CITY_CODES_ARRAY;

//echo "<pre>";
//var_dump($arResult['PROPERTIES']['KIRPICH_VARIANTS_HOUSE']);

//Конфигурирцем массив для выборки данных по вариантам
$arSelectVariant = Array(
    "ID",
    "NAME",
    "CODE",
    "IBLOCK_SECTION_ID",
    "PREVIEW_PICTURE",
    "PREVIEW_TEXT",
    "DETAIL_TEXT",
    "DETAIL_PICTURE",
    "DATE_ACTIVE_FROM",
    "PROPERTY_SIZE_HOUSE",
    "PROPERTY_SQUARE_HOUSE",
    "PROPERTY_SQUARE_ALL_HOUSE",
    "PROPERTY_SQUARE_LIFE_HOUSE",
    "PROPERTY_PLANS_HOUSE_1",
    "PROPERTY_PLANS_HOUSE_2",
    "PROPERTY_PLANS_HOUSE_3",
    "PROPERTY_PLANS_HOUSE_4",
    "PROPERTY_PLANS_HOUSE_5",
    "PROPERTY_PLANS_HOUSE_6",
    "PROPERTY_COMPECTATION_HOUSE",
    "PROPERTY_AS_FILE",
    "PROPERTY_TEHNOLOGY_HOUSE",
    "PROPERTY_D_RACURS",
    "PROPERTY_NITERIER",
    "PROPERTY_EXTERIER",
    "PROPERTY_DAYS_HOUSE",
    "PROPERTY_PRICE_HOUSE_SARATOV",
    "PROPERTY_CODE_1C",
    "PROPERTY_LIKE_HOUSE",
    "PROPERTY_REVIEWS_HOUSE",
    "PROPERTY_SERVICES_OPTIMAL",
    "PROPERTY_PRESENTS_OPTIMAL",
    "PROPERTY_PERSON_PHOTO",
    "PROPERTY_ID_CALCULATOR",
    "PROPERTY_SALE",
    "PROPERTY_PRESENT",
    "PROPERTY_PRESENT2",
    "PROPERTY_VIDEO",
    "PROPERTY_VIDEO_EXTRA",
    "PROPERTY_VIDEO_PREVIEW",
    "PROPERTY_LIDER_LIST",
    "PROPERTY_INGS_LIST",
    "PROPERTY_TEHNOLOGY_DESCRIPTION",
    "PROPERTY_SIZE1",
    "PROPERTY_SIZE2",
    "INGS_LIST",
);


$arSelectPrice = Array(
    "ID",
    "NAME",
    "PROPERTY_PRICE_1",
    "PROPERTY_PRICE_2",
    "PROPERTY_PRICE_3",
    "PROPERTY_OPTIMAL_PRICE",
    "PROPERTY_KEY_PRICE",
    "PROPERTY_KEY_DECRIPTION",
    "PROPERTY_KEY_PRICE2",
    "PROPERTY_KEY_PRICE3",
    "PROPERTY_DISCONT1",
    "PROPERTY_DISCONT2"
);

// ПРОВЕРЯЕМ НАЛИЧИЕ ЦЕННИКА ДЛЯ ДАННОГО ГОРОДА НА ДОМА ИЗ КАРКАСА

if ($arResult['PROPERTIES']['KARKAS_VARIANTS_HOUSE']['VALUE'])
{
    $variants = array();
    
    foreach($arResult['PROPERTIES']['KARKAS_VARIANTS_HOUSE']['VALUE'] as $k => $v)
    {

        $arFilter = Array(
            "IBLOCK_ID" => 21,
            "ID" => $v,
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y",
            
        );

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelectVariant);
        while ($ob = $res->GetNextElement()) 
        {
            
            $variants[$k] = $ob->GetFields();
        }
    }
   

    foreach($variants as $k => $var){

        $arFilter2 = Array(
            "IBLOCK_ID" => $CITY_CODES_ARRAY['SARATOV'],
            "ID" => $var["PROPERTY_PRICE_HOUSE_SARATOV_VALUE"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y",
            
        );

        $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelectPrice);
        while ($ob = $res->GetNextElement())
        {
            $priceArr = $ob->GetFields();
            if (!$priceArr["PROPERTY_PRICE_1_VALUE"] )
            {
                unset($variants[$k]);   
            }
        }
        
    }
    
   sort($variants); 
   $arResult['KARKAS_ID_HOUSE'] = $variants[0]["ID"];
   $karkas_variants = $variants;
     

}


// ПРОВЕРЯЕМ НАЛИЧИЕ ЦЕННИКА ДЛЯ ДАННОГО ГОРОДА НА ДОМА ИЗ БРУСА

if ($arResult['PROPERTIES']['BRUS_VARIANTS_HOUSE']['VALUE'])
{
    $variants = array();
    
    foreach($arResult['PROPERTIES']['BRUS_VARIANTS_HOUSE']['VALUE'] as $k => $v)
    {

        $arFilter = Array(
            "IBLOCK_ID" => 21,
            "ID" => $v,
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelectVariant);
        while ($ob = $res->GetNextElement()) {
            $variants[$k] = $ob->GetFields();
        }
    }

    foreach($variants as $k => $var)
    {

        $arFilter2 = Array(
            "IBLOCK_ID" => $CITY_CODES_ARRAY['SARATOV'] ,
            "ID" => $var["PROPERTY_PRICE_HOUSE_SARATOV_VALUE"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelectPrice);
        while ($ob = $res->GetNextElement())
        {
            $priceArr = $ob->GetFields();
            if (!$priceArr["PROPERTY_PRICE_1_VALUE"])
            {
                unset($variants[$k]);
            }
        }
        
    }
    
    sort($variants); 
    $arResult['BRUS_ID_HOUSE'] = $variants[0]["ID"];
    $brus_variants = $variants;
    
    //if (!$num_prices) unset($arResult['PROPERTIES']['BRUS_VARIANTS_HOUSE']['VALUE']);

}

// ПРОВЕРЯЕМ НАЛИЧИЕ ЦЕННИКА ДЛЯ ДАННОГО ГОРОДА НА ДОМА ИЗ КИРПИЧА

if ($arResult['PROPERTIES']['KIRPICH_VARIANTS_HOUSE']['VALUE'])
{
    $variants = array();
    
    foreach($arResult['PROPERTIES']['KIRPICH_VARIANTS_HOUSE']['VALUE'] as $k => $v)
    {

        $arFilter = Array(
            "IBLOCK_ID" => 21,
            "ID" => $v,
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelectVariant);
        while ($ob = $res->GetNextElement()) {
            $variants[$k] = $ob->GetFields();
        }
    }

    foreach($variants as $k => $var){

        $arFilter2 = Array(
            "IBLOCK_ID" => $CITY_CODES_ARRAY['SARATOV'],
            "ID" => $var["PROPERTY_PRICE_HOUSE_SARATOV_VALUE"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelectPrice);
        while ($ob = $res->GetNextElement())
        {
            $priceArr = $ob->GetFields();
            if (!$priceArr["PROPERTY_PRICE_1_VALUE"])
            {
                unset($variants[$k]);
            }
        }
        
    }
    
    sort($variants); 
    $arResult['KIRPICH_ID_HOUSE'] = $variants[0]["ID"];
    $kirpich_variants = $variants;

}

$tech = isset($_GET['tech']) ? resetString($_GET['tech']) : "";

// ЭТО БУДЕМ ГРУЗИТЬ ДЛЯ КАРКАСА

if($karkas_variants && !in_array($tech, ['brus', 'kirpich']))
{
    
    

    foreach($karkas_variants as $k => $v){
        
        

        $arFilter = Array(
            "IBLOCK_ID" => 21,
            "ID" => $v['ID'],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        //$cnt = 0;
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelectVariant);
        while ($ob = $res->GetNextElement()) {
            $arResult['START_VARIANT'][$k] = $ob->GetFields();
            //$cnt++;
        }
    }
    
   

    foreach($arResult['START_VARIANT'] as $k => $var){

        $arFilter2 = Array(
            "IBLOCK_ID" => $CITY_CODES_ARRAY['SARATOV'],
            "ID" => $var["PROPERTY_PRICE_HOUSE_SARATOV_VALUE"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $cnt2 = 0;
        $res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelectPrice);
        while ($ob2 = $res2->GetNextElement()) {
            $arResult['START_VARIANT'][$cnt2]["PRICES"] = $ob2->GetFields();
            $cnt2++;
        }
    }
    
    
    $matches = explode('&lt;/video&gt;', $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_VALUE"]["TEXT"]);
    foreach ($matches as &$m)
    {
        $m .= '&lt;/video&gt;';
    }
    unset($m, $matches[count($matches)-1]);
    
    foreach ($matches as $m)
    {
        $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_EXTRA_VALUE"][] = $m;
        $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_PREVIEW_VALUE"][] = '';
    }
    
}
    
// ЭТО БУДЕМ ГРУЗИТЬ ДЛЯ БРУСА

elseif($brus_variants && (!$tech || $tech == 'brus'))
{


    foreach($brus_variants as $k => $v){

        $arFilter = Array(
            "IBLOCK_ID" => 21,
            "ID" => $v,
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        //$cnt = 0;
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelectVariant);
        while ($ob = $res->GetNextElement()) {
            $arResult['START_VARIANT'][$k] = $ob->GetFields();
            //$cnt++;
        }
    }



    foreach($arResult['START_VARIANT'] as $k => $var){

        $arFilter2 = Array(
            "IBLOCK_ID" => $CITY_CODES_ARRAY['SARATOV'],
            "ID" => $var["PROPERTY_PRICE_HOUSE_SARATOV_VALUE"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelectPrice);
        while ($ob = $res->GetNextElement()) {
            $arResult['START_VARIANT'][$k]["PRICES"] = $ob->GetFields();
        }
    }
    
    $matches = explode('&lt;/video&gt;', $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_VALUE"]["TEXT"]);
    foreach ($matches as &$m)
    {
        $m .= '&lt;/video&gt;';
    }
    unset($m, $matches[count($matches)-1]);
    
    foreach ($matches as $m)
    {
        $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_EXTRA_VALUE"][] = $m;
        $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_PREVIEW_VALUE"][] = '';
    }

}   
          
elseif($kirpich_variants && (!$tech || $tech == 'kirpich'))
{

    foreach($kirpich_variants as $k => $v){

        $arFilter = Array(
            "IBLOCK_ID" => 21,
            "ID" => $v,
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        //$cnt = 0;
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelectVariant);
        while ($ob = $res->GetNextElement()) {
            $arResult['START_VARIANT'][$k] = $ob->GetFields();
            //$cnt++;
        }
    }

    foreach($arResult['START_VARIANT'] as $k => $var){

        $arFilter2 = Array(
            "IBLOCK_ID" => $CITY_CODES_ARRAY['SARATOV'],
            "ID" => $var["PROPERTY_PRICE_HOUSE_SARATOV_VALUE"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelectPrice);
        while ($ob = $res->GetNextElement()) {
            $arResult['START_VARIANT'][$k]["PRICES"] = $ob->GetFields();
        }
    }
    
    $matches = explode('&lt;/video&gt;', $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_VALUE"]["TEXT"]);
    foreach ($matches as &$m)
    {
        $m .= '&lt;/video&gt;';
    }
    
    unset($m, $matches[count($matches)-1]);

    foreach ($matches as $m)
    {
        $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_EXTRA_VALUE"][] = $m;
        $arResult["START_VARIANT"][0]["PROPERTY_VIDEO_PREVIEW_VALUE"][] = '';
    }
}

