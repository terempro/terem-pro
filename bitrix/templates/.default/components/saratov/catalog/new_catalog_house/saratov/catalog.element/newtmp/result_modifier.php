<?php
//Генерация ссылки серр
//global $USER;
//if($USER->isAdmin()){
    
    $search_box = 'Дом для постоянного проживания';
    if($arResult['PROPERTIES']['PODPIS']['VALUE'] == 'Дом для постоянного проживания'){
         $search_box = 'Дом для сезонного проживания';
    }
    
    $arSelect11 = Array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_PODPIS");
    $arFilter11 = Array("IBLOCK_ID"=>13, "ACTIVE"=>"Y", "NAME" => "%".$arResult['NAME']."%", "PROPERTY_PODPIS" => $search_box);
    $res11 = CIBlockElement::GetList(Array(), $arFilter11, false, Array("nPageSize"=>50), $arSelect11);
    while($ob11 = $res11->GetNextElement())
    {
     $arFields11[] = $ob11->GetFields();
    }
    if(count($arFields11) > 0){
        
        $resultat = CIBlockElement::GetByID($arFields11[0]["ID"]);
        if($obRes = $resultat->GetNextElement())
        {
          $ar_resultat = $obRes->GetProperty("VARIANTS_HOUSE");
        }
        
        if($ar_resultat['VALUE']){
            foreach($ar_resultat['VALUE'] as $k => $v){
                $res_price = CIBlockElement::GetByID($v);
                 if($pRes = $res_price->GetNextElement()){
                     $ar_price[] = $pRes->GetProperty("PRICE_HOUSE");
                 }
            }
        }
        
        
        if($ar_price){
            $c = 0;
            foreach($ar_price as $k => $v){
                $final_price = CIBlockElement::GetByID($v['VALUE']);
                if($fRes = $final_price->GetNextElement()){
                     $ar_fprice[$c]["OPTIMAL_PRICE"] = $fRes->GetProperty("OPTIMAL_PRICE");
                     $ar_fprice[$c]["PRICE_1"] = $fRes->GetProperty("PRICE_1");
                }
                $c++;
            }
        }
        
       foreach($ar_fprice as $v){
           $optimal1[] = $v["OPTIMAL_PRICE"]["VALUE"];
           $prices1[] = $v["PRICE_1"]["VALUE"];
           
       }
      
       $maxopt = min($optimal1);
       $maxprc = min($prices1);
       
      
       
       if((int)$maxopt > (int)$maxprc){
           $itog = (int)$maxopt;
       }else{
           $itog = (int)$maxprc;
       }
       

        $arResult['ALTER_LINK'] = $arFields11[0]["DETAIL_PAGE_URL"];
        $arResult['ALTER_LINK_DESCRIPTION'] = $search_box;
        $arResult['ALTER_PRICE'] = $itog;
    }
    
    //echo "<pre>";
    //var_dump($arFields);
    //var_dump($arResult['PROPERTIES']['PODPIS']['VALUE']);
    
    
    
    
//}
////////////////////////Генерация ссылки серр


//Привязка информация о домах
$arSelect = Array(
    "ID",
    "NAME",
    "CODE",
    "IBLOCK_SECTION_ID",
    "PREVIEW_PICTURE",
    "PREVIEW_TEXT",
    "DETAIL_TEXT",
    "DETAIL_PICTURE",
    "DATE_ACTIVE_FROM",
    "PROPERTY_SIZE_HOUSE",
    "PROPERTY_SQUARE_HOUSE",
    "PROPERTY_SQUARE_ALL_HOUSE",
    "PROPERTY_SQUARE_LIFE_HOUSE",
    "PROPERTY_PLANS_HOUSE_1",
    "PROPERTY_PLANS_HOUSE_2",
    "PROPERTY_COMPECTATION_HOUSE",
    "PROPERTY_AS_FILE",
    "PROPERTY_TEHNOLOGY_HOUSE",
    "PROPERTY_D_RACURS",
    "PROPERTY_NITERIER",
    "PROPERTY_EXTERIER",
    "PROPERTY_DAYS_HOUSE",
    "PROPERTY_PRICE_HOUSE",
    "PROPERTY_CODE_1C",
    "PROPERTY_LIKE_HOUSE",
    "PROPERTY_REVIEWS_HOUSE",
    "PROPERTY_SERVICES_OPTIMAL",
    "PROPERTY_PRESENTS_OPTIMAL",
    "PROPERTY_PERSON_PHOTO",
    "PROPERTY_ID_CALCULATOR",
    "PROPERTY_SALE",
    "PROPERTY_PRESENT",
    "PROPERTY_PRESENT2",
    "PROPERTY_VIDEO",
    "PROPERTY_LIDER_LIST",
    "PROPERTY_INGS_LIST",
    "PROPERTY_TEHNOLOGY_DESCRIPTION",
    "PROPERTY_SIZE1",
    "PROPERTY_SIZE2",
);
$cnt = 0;
foreach ($arResult["PROPERTIES"]["VARIANTS_HOUSE"]["~VALUE"] as $item) {
    $arFilter = Array(
        "IBLOCK_ID" => 21,
        "ID" => $item,
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y"
    );
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arResult['HOUSE_INFORMATION'][$cnt] = $ob->GetFields();
        $cnt++;
    }
}










//Привязка информация ценах
$cnt2 = 0;
$arSelect2 = Array(
    "ID",
    "NAME",
    "PROPERTY_PRICE_1",
    "PROPERTY_PRICE_2",
    "PROPERTY_PRICE_3",
    "PROPERTY_OPTIMAL_PRICE",
    "PROPERTY_KEY_PRICE",
    "PROPERTY_KEY_DECRIPTION",
    "PROPERTY_KEY_PRICE2",
    "PROPERTY_KEY_PRICE3",
    "PROPERTY_DISCONT1",
    "PROPERTY_DISCONT2"
);



foreach ($arResult["HOUSE_INFORMATION"] as $k => $item) {
    $arFilter2 = Array(
        "IBLOCK_ID" => 22,
        "ID" => $item["PROPERTY_PRICE_HOUSE_VALUE"],
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y"
    );
    $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelect2);
    while ($ob = $res->GetNextElement()) {
        $arResult['HOUSE_INFORMATION'][$cnt2]["PRICES"] = $ob->GetFields();
        $cnt2++;
    }

 

    $arImage1 = CFile::ResizeImageGet($item["PROPERTY_PLANS_HOUSE_1_VALUE"], array('width' => 838, 'height' => 546), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
    $arImage2 = CFile::ResizeImageGet($item["PROPERTY_PLANS_HOUSE_2_VALUE"], array('width' => 838, 'height' => 546), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
    $arImage3 = CFile::ResizeImageGet($item["PROPERTY_PLANS_HOUSE_1_VALUE"], array('width' => 285, 'height' => 285), true);
    $arImage4 = CFile::ResizeImageGet($item["PROPERTY_PLANS_HOUSE_2_VALUE"], array('width' => 285, 'height' => 285), true);
    $arResult["HOUSE_INFORMATION"][$k]['PLAN1_838'] = $arImage1['src'];
    $arResult["HOUSE_INFORMATION"][$k]['PLAN2_838'] = $arImage2['src'];
    $arResult["HOUSE_INFORMATION"][$k]['PLAN1_285'] = $arImage3['src'];
    $arResult["HOUSE_INFORMATION"][$k]['PLAN2_285'] = $arImage4['src'];




    foreach ($arResult["HOUSE_INFORMATION"] as $k => $item) {

        //Сбор списка приемуществ
        $VALUES = array();
        $res = CIBlockElement::GetProperty(37, $item["PROPERTY_LIDER_LIST_VALUE"], "sort", "asc", array("CODE" => "TEXT_LIST"));
        while ($ob = $res->GetNext()) {
            $VALUES[] = $ob['VALUE'];
        }
        if (count($VALUES) > 0) {
            $arResult["HOUSE_INFORMATION"][$k]['LIDER_LIST'] = $VALUES;
        } else {
            $arResult["HOUSE_INFORMATION"][$k]['LIDER_LIST'] == NULL;
        }

        //Услуги инженерки
        $VALUES = array();
        $res = CIBlockElement::GetProperty(38, $item["PROPERTY_INGS_LIST_VALUE"], "sort", "asc", array("CODE" => "ING_LIST"));
        while ($ob = $res->GetNext()) {
            $VALUES[] = $ob['VALUE'];
        }
        if (count($VALUES) > 0) {
            $arResult["HOUSE_INFORMATION"][$k]['ING_LIST'] = $VALUES;
        } else {
            $arResult["HOUSE_INFORMATION"][$k]['ING_LIST'] == NULL;
        }
     

        $summ = 0;
        $p = $item["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"] . "000";
        if (count($item["PROPERTY_SERVICES_OPTIMAL_DESCRIPTION"]) > 1) {
            foreach ($item["PROPERTY_SERVICES_OPTIMAL_DESCRIPTION"] as $i) {
                $summ = $summ + $i;
            }
        }


        $arResult["HOUSE_INFORMATION"][$k]["SUMMDOP"] = $summ;
        $arResult["HOUSE_INFORMATION"][$k]["ALLSUMM"] = $p + $summ;

        if ($USER->isAdmin()) {
            //echo "<pre>";
            //var_dump($arResult["HOUSE_INFORMATION"]);
        }
        
        //echo "<pre>"; 
        //echo 1235;
       // echo $_SERVER['DOCUMENT_ROOT'] ;   
        //$file = '/var/www/zolotarev/data/www/terem-pro.ru/upload/calcs/' . $item["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' . $item["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';

        $file = $_SERVER['DOCUMENT_ROOT'].'/upload/calcs/' . $item["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' . $item["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
        //error_reporting(E_ALL);

    
    
        $f = fopen($file, 'r');
        $file_data = fread($f, filesize($file));
        fclose($f);

        $result = unserialize($file_data);

        $price = 0;
        $OptimalSum = 0;





        $arResult["HOUSE_INFORMATION"][$k]["CALC"] = $result;




        if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный') {

            $discont1 = 30;
            if($arResult['HOUSE_INFORMATION'][$k]["PRICES"]["PROPERTY_DISCONT1_VALUE"] != NULL && $arResult['HOUSE_INFORMATION'][$k]["PRICES"]["PROPERTY_DISCONT1_VALUE"] > 0){
                $discont1 = $arResult['HOUSE_INFORMATION'][$k]["PRICES"]["PROPERTY_DISCONT1_VALUE"];
            }

        

            //echo "<pre>";
            //var_dump($result[0]["ITEMS"]);
            //die();
            foreach ($result[0]["ITEMS"] as $i) {




                $price = (int) $i['PRICE'];
                $price = $price - (($price * $discont1) / 100);
                $OptimalSum = $OptimalSum + $price;
            }
        }

        if ($USER->IsAdmin()) {
            //echo "<pre>";
            //var_dump($arResult["HOUSE_INFORMATION"][$k]["CALC"]);
        }





//Первый IF с ----
        if ($USER->IsAdmin()) {
            $myPrice = $arResult["HOUSE_INFORMATION"][$k]["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"];
            //echo "<pre>";
            //var_dump($arResult["HOUSE_INFORMATION"][0]["CALC"]);
            //echo $OptimalSum."<br/>";
        }


        if ($OptimalSum != 0) {

            //Второй IF выводится только 2ое число 
            if ($USER->IsAdmin()) {
                //echo 1;
                //echo "<pre>";
                //var_dump($myPrice);
            }


            if ($item["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"]) {
                $ValPrice = "" . $item["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"] . "000";
            } else {
                $ValPrice = "" . $arResult["HOUSE_INFORMATION"][$k]["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"] . "000";
            }






            $ValPrice = (int) $ValPrice;
            $OptimalSum = $OptimalSum + $ValPrice;

            $arResult["HOUSE_INFORMATION"][$k]["OPTIMALPRICE"] = $OptimalSum;
        }
    }
}