<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="slider-with-trigger">
    <div class="my-item-slider + my-margin" data-item="slider-item">
        <?php $cnt1 = 0; ?>
        <?php foreach ($arResult["ITEMS"] as $arItem): ?>
            <div class="item">
                <div class="iner">

                    <?php if($cnt1 == 0): ?>
                        <div class="row countdown-wrapper">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="countdown"></div>
                            </div>
                        </div>
                    <?php endif; ?>


                    <a href="<?=$arItem["PROPERTIES"]["LINK"]["~VALUE"]?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"]; ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"]; ?>"></a>
                    <?php if($cnt1 != 0): ?>
                    <div class="title-item text-center">
                        <?php if ($arItem["PROPERTIES"]["NAME"]["~VALUE"]): ?>
                            <h4><?= $arItem["PROPERTIES"]["NAME"]["~VALUE"] ?></h4>
                        <?php endif; ?>
                        <?php if ($arItem["PROPERTIES"]["SIZE"]["~VALUE"]): ?>
                            <h5><?= $arItem["PROPERTIES"]["SIZE"]["~VALUE"] ?></h5>
                        <?php endif; ?>
                        <?php if ($arItem["PROPERTIES"]["TEHNOLOGY"]["~VALUE"]): ?>
                            <h5><?= $arItem["PROPERTIES"]["TEHNOLOGY"]["~VALUE"] ?></h5>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php $cnt1++; ?>
        <?php endforeach; ?>
    </div>



    <div data-item="trigger-set">
        <?php $cnt = 0; ?>
        <?php foreach ($arResult["ITEMS"] as $arItem): ?>

            <?php if ($arItem["PROPERTIES"]["STAR"]["VALUE_XML_ID"] == "Yes"): ?>
                <div class="trigger" data-trigger="<?= $cnt; ?>">
                  <?php if($arItem["PROPERTIES"]["TEXT_STAR"]["VALUE"]):?>
                        <?php echo html_entity_decode($arItem["PROPERTIES"]["TEXT_STAR"]["VALUE"]);?>
                  <?php endif;?>
                </div>
            <?php elseif(0): ?>
                <div class="trigger" data-trigger="<?= $cnt; ?>"></div>
            <?php endif; ?>


            <?php $cnt++; ?>
        <?php endforeach; ?>
    </div>



</div>
