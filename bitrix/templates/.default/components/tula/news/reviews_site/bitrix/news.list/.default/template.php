<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
    <?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?= $arResult["NAV_STRING"] ?><br />
    <?endif;?>
    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div class="row" itemscope itemtype="http://schema.org/Review">
			<meta itemprop="datePublished" content="<?php echo $arItem["DISPLAY_ACTIVE_FROM"];?>"/>
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="item-review">
                    <div class="row">
                        <div class="col-xs-12 col-md-9 col-sm-9">
                            <div class="inner">
                                <p itemprop="itemReviewed" itemscope itemtype="http://schema.org/Thing">
                                    <strong itemprop="name">
                                            <?php echo $arItem["PROPERTIES"]["TITLE"]["VALUE"]; ?>
                                           
                                    </strong>
								
                                </p>
							
                                <p itemprop="description">
                                    <?php echo $arItem["PREVIEW_TEXT"]; ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="date">
                                <p itemprop="author" itemscope itemtype="http://schema.org/Person">
								<strong itemprop="name"> <?php echo $arItem["NAME"]; ?>
								</strong>
								</p>
								 <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
								  Рейтинг:
									<span itemprop="ratingValue">10</span> из
									<span itemprop="bestRating">10</span>
								  </span>
                                <p><?php echo $arItem["PROPERTIES"]["CITY"]["VALUE"]; ?></p>
                                <p><?php echo $arItem["ACTIVE_FROM"]; ?></p>
								<span itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
									<meta itemprop="name" content="Washington Times">
								</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      

    <?php endforeach; ?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <br /><?= $arResult["NAV_STRING"] ?>
    <?endif;?>
</div>
