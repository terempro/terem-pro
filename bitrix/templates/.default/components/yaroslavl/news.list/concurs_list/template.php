<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?php if(count($arResult['ITEMS']) > 0):?>



<ul class="contest__items">
    <?php foreach($arResult['ITEMS'] as $arItem):?>
    <li class="item">
        <div class="item__img">
            <a class="fancybox" rel="group<?=$arItem['ID']?>" href="<?=$arItem['DETAIL_PICTURE']['SRC']?>">

                <?php
                //$src = thumb($_SERVER['DOCUMENT_ROOT'].$arItem['PREVIEW_PICTURE']['SRC'], $side = 210);
                //$src_new = str_replace('/var/www/zolotarev/data/www/terem-pro.ru','',$src);
                //echo $src_new;

                $renderImage = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], Array("width" => 280, "height" => 160),BX_RESIZE_IMAGE_PROPORTIONAL);


                ?>

                <img src="<?=$renderImage['src']?>" alt="<?=$arItem['NAME']?>, <?=$arItem['PROPERTIES']['CHILD_AGE']['VALUE']?>">
                <!-- <div class="zoom">
                    <i class="glyphicon glyphicon-zoom-in"></i>
                </div> -->
            </a>
        </div>
        <div class="item__desc">

            <div class="item__desc">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="userName"><strong><?=$arItem['NAME']?></strong></div>
                        <div class="userAge"><?=$arItem['PROPERTIES']['CHILD_AGE']['VALUE']?> <?=declension_words($arItem['PROPERTIES']['CHILD_AGE']['VALUE'],["год","года","лет"])?></div>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" data-image="<?=$arItem['ID']?>" class="heart"></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="count_golos" id="<?=$arItem['ID']?>">
                            <?php if($arItem['PROPERTIES']['LIKES']['VALUE']): ?>
                                <?=$arItem['PROPERTIES']['LIKES']['VALUE']?>
                                <?=declension_words($arItem['PROPERTIES']['LIKES']['VALUE'],["голос","голоса","голосов"])?>
                            <?php else: ?>
                                0 голосов
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="//yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki" data-counter=""></div>
                    </div>

                </div>

            </div>









        </div>
    </li>
    <?php endforeach; ?>
<?php endif; ?>


</ul>

<?php $this->SetViewTarget('pager_concurs'); ?>
<div style='margin: 0 auto; max-width: 500px; text-align: center;'>
  <?=$arResult["NAV_STRING"]?>  
</div>
<?php $this->EndViewTarget();?> 