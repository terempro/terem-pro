<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


       
  

//delayed function must return a string
if(empty($arResult) || count($arResult)==1)
	return '<ul class="bread-crumbs"></ul>';

    
      
$strReturn = '<ul class="bread-crumbs">';

for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> "" && $index<($itemSize-1))
		$strReturn .= '<li><div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="'.$arResult[$index]["LINK"].'" title="'.$title.'"><span itemprop="title">'.$title.'</span></a></div></li>';
	else
		$strReturn .= '<li><div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="#"><span itemprop="title">'.$title.'</span></a></div></li>';
}

$strReturn .= '</ul>';
return $strReturn;






?>