<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="row">
<? $regions=array(
    'vologda' => 'Вологда',
    'novosibirsk' => 'Новосибирск',
    'ryazan' => 'Рязань',
    'tula' => 'Тула',
	/*'bryansk' => 'Брянск',*/
    'saratov' => 'Саратов',
    'izhevsk' => 'Ижевск',
	/*'krasnodar' => 'Краснодар',*/
    'catalog' => 'Москва',
    'vladimir' => 'Владимир',
	'yaroslavl' => 'Ярославль'
);
?>
<? $region_act=explode('/', $_SERVER['REQUEST_URI']);
$region=$region_act[1];?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<?if($arItem['PROPERTY_REGION_VALUE']==$regions[$region]):?>
 <?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
 $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
 <div class="col-xs-12 col-md-3 col-sm-3">
 <a href="<?echo $arItem["PREVIEW_TEXT"]?>" alt="<? echo count($arResult["ITEMS"]);?>" class="my-block-link my-margin">
 <img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?echo $arItem["NAME"]?>">
 </a>
 </div>
<?endif;?>
<?endforeach;?>
</div>