<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="content text-page white" role="content">
    <div class="container" itemscope itemtype="http://schema.org/Article">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix + my-margin">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1 itemprop="articleSection">
                                   <?=$arResult["NAME"]?>
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12" itemprop="articleBody">
                            <?echo $arResult["DETAIL_TEXT"];?>
                        </div>
                    </div>
                    <meta itemprop="references" content="<?=$arResult["NAME"]?>">
                    <meta itemprop="datePublished" content="<?=$arResult["NAME"]?>">
                </div>
            </div>
        </div>

    </div>
</section>
