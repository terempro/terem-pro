<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if($_REQUEST['SECTION']):?>
<section class="content" role="content">
    <div class="container">
        <div class="flex-reversed">
            <div class="row">
                <?php $SECTION = $_GET['SECTION']; ?>
                 <?php
               $NAMES = array(
                   '1' => 'Бани',
                   '2' => 'Дома до 100 м2',
                   '3' => 'Дома до 150 м2',
                   '4' => 'Дома от 150м2',
                   '5' => 'Дома с выпусками из бруса',
                   );
                if($cnt == 0){
                   $APPLICATION->AddChainItem($NAMES[$SECTION], "/services/individual_projects/projects.php?SECTION=".$SECTION); 
                }
                ?>
                
    
                <?php foreach ($arResult['ITEMS'] as $item): ?>
                <?php if($item["PROPERTIES"]["CATEGORY"]["VALUE_XML_ID"] == $SECTION): ?>
                    <div class="col-xs-12 col-md-6 col-sm-12 shuffle">
                     <?php if ($item["PROPERTIES"]["IMG_2"]["~VALUE"]): ?>
                            <div class="my-promotion desgin-4 + my-margin hover-grey">
                    <?php else: ?>
                            <div class="my-promotion desgin-4 + my-margin hover-grey single-item">
                    <?php endif; ?>
                            <div class="flex-row">
                                <div>
                                    <a href="<?php echo $item["DETAIL_PAGE_URL"]; ?>">
                                        <?php if ($item["PREVIEW_PICTURE"]["SRC"]): ?>
                                            <img src="<?php echo $item["PREVIEW_PICTURE"]["SRC"]; ?>">
                                        <?php else: ?>
                                            <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg" alt=""/>
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            <?php echo $item["NAME"]; ?>
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-toggle="modal" data-whatever="<?php echo $item["NAME"]; ?>"><i class="glyphicon glyphicon-earphone"></i></a>
                                        
                                    </div>
                                    <div class="options">
                                        
                                       <!--  <a href="#" class="compare">Получить бесплатную консультацию</a> -->
                                       <div>
                                        <?php if($item["ID"] == '254907' || $item["ID"] == '254906'):?>
                                             <p>
                                                 <b class="text-red">Новинка!</b>
                                            </p> 
                                             <p>
                                                Двухэтажные дома из бруса<br/> с выпусками.
                                            </p><br/>
                                        <?php endif;?>
                                            <p>
                                                <b>Площадь:</b>
                                            </p>
                                        </div>
                                        <p>
                                            <?php echo $item["PROPERTIES"]["S_1"]["NAME"]; ?> - <?php echo $item["PROPERTIES"]["S_1"]["~VALUE"]; ?>
                                        </p>
                                        <p>
                                            <?php echo $item["PROPERTIES"]["S_2"]["NAME"]; ?> - <?php echo $item["PROPERTIES"]["S_2"]["~VALUE"]; ?>
                                        </p>
                                        <p>
                                            <?php echo $item["PROPERTIES"]["S_3"]["NAME"]; ?> - <?php echo $item["PROPERTIES"]["S_3"]["~VALUE"]; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-row">
                                 <?php if ($item["PROPERTIES"]["IMG_1"]["~VALUE"]): ?>
                                <div>
                                    <img src="<?php echo CFile::GetPath($item["PROPERTIES"]["IMG_1"]["~VALUE"]); ?>">
                                </div>
                                <?php else: ?>
                                   <div> 
                                     <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg" alt=""/>
                                    </div>
                                <?php endif; ?>

                                
                                <?php if ($item["PROPERTIES"]["IMG_2"]["~VALUE"]): ?>
                                    <div>
                                        <img src="<?php echo CFile::GetPath($item["PROPERTIES"]["IMG_2"]["~VALUE"]); ?>">
                                    </div>
                                <?php endif; ?>

                            </div>
                            <a class="all-item-link" href="<?php echo $item["DETAIL_PAGE_URL"]; ?>"></a>
                        </div>
                    </div>
                  <?php endif;?>
          
                <?php endforeach; ?>
            </div>  
        </div>
    </div>
</section>
<?php endif;?>
<?php if($_REQUEST['SECTION'] == 5):?>

<section class="content" role="content">
    <div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side + my-margin clearfix" style="background: #fff;">
						<div class="my-text-title text-center text-uppercase desgin-h1">
								<h1>
									ДОМА ИЗ БРУСА С ВЫПУСКАМИ
								</h1>
							</div>
							
							
						<div class="text-title clearfix">
							<h2 class="text-uppercase" style="font-size: 27px; text-transform: none;">Дома с выпуском от компании «Теремъ»</h2>
						</div>
						<div class="text-description clearfix">
							<p>Теперь все брусовые дома компании «Теремъ» можно заказать в варианте с выпуском – когда торцы брусьев выступают наружу на углах. Это архитектурное решение придает коттеджам классический вид, являясь отсылкой к традиционному деревянному домостроению. Подробности о строительстве домов с выпуском уточняйте по телефону или у менеджеров на территории выставочного комплекса в Кузьминках.</p>
						</div>	
						<div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="img-with-description clearfix">
                                <img src="/upload/articles/bg.jpg" class="full-width">                
                                <p><i></i></p>
                            </div>
                        </div>
                    </div>
					<div class="text-title clearfix">
							<h2 class="text-uppercase" style="font-size: 27px; text-transform: none;">Как строят брусовые дома с выпуском</h2>
						</div>
						<div class="text-description clearfix">
						<p>Выпуск бруса – классическая технология, по которой обустраивается угловое соединение. В обычных домах компании «Теремъ» используется соединение «теплый угол»: прямоугольный выступ на одном брусе помешается в паз в другом, образуется замок, который закрепляется скобами. В результате стены образуют прямой угол.</p>
						<p>В доме с выпуском, брус выходит за пределы стыка двух стен. Угловое соединение делается «в чашку»: на брусе, толщиной 200 мм, запиливается паз, в который помещается брус следующего венца. Дом собирается по всему периметру одновременно. Для дополнительной защиты от продувания межвенцовые стыки прокладываются уплотнительной лентой, а угловые стыки – натуральной паклей.</p>
						</div>
						<div class="text-title clearfix">
							<h2 class="text-uppercase" style="font-size: 27px; text-transform: none;">Практическая польза эстетического решения</h2>
						</div>
						<div class="text-description clearfix">
						<p>Выпуск бруса не только визуально преображает дом, но имеет и сугубо практическое значение. До внедрения современных технологий строительства, именно такое угловое соединение позволяло избежать всякой вероятности продувания и построить крепкий надежный дом.</p>
						<p>Как и для всех коттеджей, пиломатериал для строительства домов с выпусками подготавливается в заводских условиях. Производственная база оснащена немецкими станками, которые позволяют добиться абсолютной точности запилов на брусе – дом получается максимально теплым и надежным. Дома из бруса с выпуском – декоративное решение из традиций древнерусских мастеров.</p>
						</div>
						<div class="text-description clearfix">
						<p><strong>Преимущества домов с выпуском:</strong></p>
						<ul style="list-style: none; font-size: 16px;">
						 <li><span style="color: #e0051c;">►</span> Утепленный угол;</li>
						 <li><span style="color: #e0051c;">►</span> Простая сборка дома;</li>
						 <li><span style="color: #e0051c;">►</span> Надежность соединений;</li>
						</ul>
						</div>
						
						<div class="text-description clearfix">
						<p><strong>Дома из бруса с выпусками - декоративное решение из традиций древнерусских мастеров.</strong></p>
						</div>
						

						
				</div>
				
				
				
				
			</div>
		</div>
		

	</div>
</section>	
<?php endif;?>


