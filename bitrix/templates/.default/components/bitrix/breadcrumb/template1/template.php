<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

foreach ($arResult as $key => $item) {
	//echo "<pre>";
	//print_r($item);
	//echo "</pre>";
	if($key == 0){
		$arItem = array();
		include($_SERVER['DOCUMENT_ROOT']."/.top.menu.php");
		foreach ($aMenuLinks as $value) {
			if($item['LINK'] != $value[1]){
				$arItem[] = array("TITLE" => $value[0], "LINK" => $value[1]);
			}
		}
		$item["ITEMS"] = $arItem;
	}elseif($key == 1){
		$arItem = array();
		$page = explode('/', $item['LINK']);
		$url = $_SERVER['DOCUMENT_ROOT'] ."/". $page[1] . "/.left.menu.php";
		if(file_exists($url)){
			include($url);
			foreach ($aMenuLinks as $value) {
				if($item['LINK'] != $value[1]){
					$arItem[] = array("TITLE" => $value[0], "LINK" => $value[1]);
				}
			}
			$item["ITEMS"] = $arItem;
		}
			
	}
	$items[] = $item;
}

$arResult = $items;

	//echo "<pre>";
	//print_r($arResult);
	//echo "</pre>";

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

$strReturn .= '
	<ul class="bread-crumbs" vocab="http://schema.org/" ; typeof="BreadcrumbList">
		<li property="itemListElement" typeof="ListItem">
			<a class="arrow" property="item" typeof="WebPage" href="/">
				<span property="name">ТеремЪ</span>
			</a>
			<meta property="position" content="0">
		</li>';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	$arrow = ($index > 0? '' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		if($arResult[$index]["ITEMS"])
		{
			$strReturn .= '
				<ul class="dropdown" property="itemListElement" typeof="ListItem">
					<a class="dropdown-toggle" property="item" typeof="WebPage" href="'.$arResult[$index]["LINK"].'" title="'.$title.'">
						<span property="name">'.$title.'</span>
					</a>
					<meta property="position" content="'.($index + 1).'">
					<ul class="dropdown-menu">
			';
			foreach ($arResult[$index]["ITEMS"] as  $item) {
				$strReturn .= '
					<li>
						<a href="'.$item["LINK"].'" title="'.$item['TITLE'].'">
							<span>'.$item['TITLE'].'</span>
						</a>
					</li>
				';
			}

			$strReturn .= '</ul></ul>';
		}
		else
		{
			$strReturn .= '
				<li property="itemListElement" typeof="ListItem">
					<a property="item" typeof="WebPage" href="'.$arResult[$index]["LINK"].'" title="'.$title.'" >
						<span property="name">'.$title.'</span>
					</a>
					<meta property="position" content="'.($index + 1).'">
				</li>
			';
		}
	}
	else
	{
		if($arResult[$index]["ITEMS"])
		{
			$strReturn .= '
				<ul class="dropdown bread-crumbs_last" property="itemListElement" typeof="ListItem">
						<a style="pointer-events: none;	text-decoration: underline;" property="item" typeof="WebPage" href="#">
							<span property="name">'.$title.'</span>
						</a>
						<meta property="position" content="'.($index + 1).'">
					<ul class="dropdown-menu"> 
			';
			foreach ($arResult[$index]["ITEMS"] as  $item) {
				$strReturn .= '
				<li>
					<a href="'.$item["LINK"].'" title="'.$item['TITLE'].'">
						<span>'.$item['TITLE'].'</span>
					</a>
				</li>
				';
			}
			$strReturn .= '</ul></ul>';
		}
		else 
		{
			$strReturn .= '
				<li property="itemListElement" typeof="ListItem">
					<a style="pointer-events: none;	text-decoration: underline;" property="item" typeof="WebPage" href="#">
						<span property="name">'.$title.'</span>
					</a>
					<meta property="position" content="'.($index + 1).'">
				</li> 
			';
		}	
	}
}

$strReturn .= '</ul>
<style>
	.dropdown{
		padding-top: 0;
		padding-left: 0;
		max-width: none!important;
	}
	.dropdown:hover > .dropdown-menu { 
    	display: block; 
	}
	.dropdown:hover > .dropdown-toggle:after { 
		border-top: 4px solid transparent!important;
		border-bottom: 4px solid #fff!important;
		margin-top: -3px!important;
		margin-left: 6px;
    	margin-right: 10px;
	}

	.bread-crumbs .dropdown .dropdown-toggle:after { 
		border-left: 4px solid transparent;
		border-top: 4px solid #fff;
		margin-top: 5px;
		margin-left: 6px;
    	margin-right: 10px;
	}

	.dropdown:hover > a:after { 
		border-top: 4px solid transparent!important;
		border-bottom: 4px solid #fff!important;
		margin-top: -3px!important;
		margin-left: 6px;
    	margin-right: 10px;
	}
	.dropdown-menu{
		margin: 0;
		border: none;
		border-radius: 0;
		padding-top: 20px;
		padding-bottom: 14px;
	}
	.content .dropdown-menu a{
		color: #333;
		display: block;
		font-family: Arial;
		text-align: left;
		font-weight: 400;
		letter-spacing: .3px;
		font-size: 12px;
		text-transform: uppercase;
		padding: 2px 18px;
	}

	.content .dropdown-menu a:hover{
		background: 0 0;
		font-weight: 700;
		padding-right: 14px;
	}

	.dropdown-menu a:after{
		content: none!important;
	}
	.bread-crumbs .bread-crumbs_last a:after{
		border-left: 4px solid transparent;
		border-top: 4px solid #fff;
		margin-top: 5px;
		margin-left: 6px;
    	margin-right: 10px;
	}
	.dropdown-menu li:last-child{
		pointer-events: auto!important;
	}
	 .dropdown-menu li:last-child a{
		text-decoration: none!important;
	}

</style>';

return $strReturn;