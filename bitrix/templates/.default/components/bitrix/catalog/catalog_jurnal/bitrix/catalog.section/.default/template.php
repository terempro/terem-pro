<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

?>


<section class="content text-page white" role="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="white my-margin clearfix">
          <div class="journal journal-article-layer">
            <div class="journal__wrapper">

              <div class="journal__content">
                <div class="journal-article-about-header">
					<?php if ($arResult["LONG_PICTURE"]): ?>
					<?php if ($arResult["LINK"]): ?><a href="<?= $arResult["LINK"] ?>"><?php endif; ?>
						<img src="<?= $arResult["LONG_PICTURE"] ?>" alt="Обложка выпуска">
					<?php if ($arResult["LINK"]): ?></a><?php endif; ?>
                  <?php else: ?>
                    <h1 class="journal-article-about-header__title">Журнал выпуск №<?= $arResult["NUMBER"] ?></h1>
                  <?php endif; ?>
                </div>
                <div class="journal-article-about-subject">
                  <h2>В НОМЕРЕ</h2>
                  <ul>
                    <?php foreach ($arResult['SUB_SECTION'] as $k => $v): ?>

                      <?php if (!$v["EDITOR'S"]): ?>
                        <li class="lead">
                          <h3><?= $v['NAME'] ?></h3>
                          <ul>
                            <?php foreach ($v['ITEMS'] as $item): ?>
                              <li class="col-xs-12 col-sm-4">
                                <a href="<?= $item['DETAIL_PAGE_URL'] ?>">
                                  <img src="<?php echo $item['PREVIEW_PICTURE'] ? CFile::ResizeImageGet($item['PREVIEW_PICTURE'], ["width" => 270, "height" => 180], BX_RESIZE_IMAGE_EXACT)['src'] : '/bitrix/templates/.default/assets/img/journal_thumbnail.png' ?>" alt="Название статьи" class="lead__img">
                                  <p><?= $item['NAME'] ?></p>
                                  <p><small><?= $item['PREVIEW_TEXT'] ?></small></p>
                                </a>
                              </li>
                            <?php endforeach; ?>
                          </ul>
                        </li>
                      <?php endif; ?>

                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
              <div class="journal__aside">
                  <div class="from-author">
                    <?php if ($arResult['REDACTOR']['PREVIEW_PICTURE']): ?>
                      <div>
                        <img src="<?= CFile::GetPath($arResult['REDACTOR']['PREVIEW_PICTURE']) ?>" alt="Фото редактора">
                      </div>
                    <?php endif; ?>
                    <p class="from"><i>От редактора</i></p>
                    <div class="from-text">
                      <?= $arResult['REDACTOR']['PREVIEW_TEXT'] ?>
                    </div>
                  </div>
                  <?php if ($arResult["RAND_ARTICLES"]): ?>
                    <div class="journal-previous">
                      <h3 class="journal-previous__title">В прошлом номере</h3>
                      <ul class="journal-previous__list">
                        <?php foreach ($arResult["RAND_ARTICLES"] as $a): ?>
                          <li class="journal-previous__item"><a href="<?= $a["DETAIL_PAGE_URL"] ?>"><?= $a["NAME"] ?></a></li>
                        <?php endforeach; ?>
                      </ul>
                    </div>
                  <?php endif; ?>
              </div>
            </div>
            <div class="journal-nav">
              <div>
                <?php if ($arResult['PREV']): ?>
                  <a href="<?= $arResult['PREV'] ?>">< <span>НАЗАД</span></a>
                <?php endif; ?>
              </div>
              <div>
                <?php if ($arResult['NEXT']): ?>
                  <a href="<?= $arResult['NEXT'] ?>"><span>ВПЕРЕД</span> ></a>
                <?php endif; ?>
              </div>
            </div>

            <!--						<div class="row">-->
            <!--							<div class="col-xs-12 col-md-12 col-sm-12 text-center">-->
            <!--								<div class="journal-subscribe">-->
            <!--									<a href="#" class="btn btn-danger btn-ghost">оформить бесплатную подписку</a>-->
            <!--								</div>-->
            <!--							</div>-->
            <!--						</div>-->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
