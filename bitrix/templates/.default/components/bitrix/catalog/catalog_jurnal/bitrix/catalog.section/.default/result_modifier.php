<?php
$IBLOCK_ID = 41;
$SECTION_ID = $arResult['ID'];


if ($arResult["IBLOCK_SECTION_ID"] != NULL) {
    $redactor = NULL;
    $arResult['PREV'] = NULL;
    $arResult['NEXT'] = NULL;
    $arResult['IMG_LEFT'] = false;
    $arResult["NUMBER"] = CIBlockSection::GetById($arResult["IBLOCK_SECTION_ID"])->GetNext()["SORT"];

    $section = CIBlockSection::GetByID($arResult["ID"]);
    if ($ar_res = $section->GetNext()){
        $SUB_SECTIONS[] = $ar_res;
    }

    foreach ($SUB_SECTIONS as $k => $sub_section) {
        $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "=SECTION_ID" => $sub_section['ID'], "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, Array("nPageSize" => 50), $arSelect);
        while ($ob = $res->GetNext()) {
            $SUB_SECTIONS[$k]['ITEMS'][] = $ob;
        }
    }

    //От редактора
    $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
    $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "=SECTION_ID" => $arResult["IBLOCK_SECTION_ID"], "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, Array("nPageSize" => 1), $arSelect);
    while ($ob = $res->GetNext()) {
        $redactor = $ob;
    }
	$arResult["LINK"] = CIBLockSection::GetList([], ["IBLOCK_ID" => 41, "ID" => $arResult["IBLOCK_SECTION_ID"]], false, ["UF_*"])->GetNext()["UF_LINK"];
    $arResult["LONG_PICTURE"] = CFile::GetPath(CIBLockSection::GetList([], ["IBLOCK_ID" => 41, "ID" => $arResult["IBLOCK_SECTION_ID"]], false, ["UF_*"])->GetNext()["UF_PICTURE_DETAIL"]);

} else {
    $arResult['IMG_LEFT'] = true;
    $arResult["NUMBER"] = $arResult["SORT"];
    $arResult["LONG_PICTURE"] = CFile::GetPath(CIBLockSection::GetList([], ["IBLOCK_ID" => 41, "ID" => $arResult["ID"]], false, ["UF_*"])->GetNext()["UF_PICTURE_DETAIL"]);
	$arResult["LINK"] = CIBLockSection::GetList([], ["IBLOCK_ID" => 41, "ID" => $arResult["ID"]], false, ["UF_*"])->GetNext()["UF_LINK"];


//Выборка подразделов
    $rsParentSection = CIBlockSection::GetByID($SECTION_ID);
    if ($arParentSection = $rsParentSection->GetNext()) {
        $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'], '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']);
        $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
        while ($arSect = $rsSect->GetNext()) {
            $arSect["EDITOR'S"] = CIBLockSection::GetList([], ["IBLOCK_ID" => 41, "ID" => $arSect["ID"]], false, ["UF_EDITOR"])->GetNext()["UF_EDITOR"];
            $SUB_SECTIONS[] = $arSect;
        }
    }

//Выборка элементов
    foreach ($SUB_SECTIONS as $k => $sub_section) {
        $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "=SECTION_ID" => $sub_section['ID'], "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, Array("nPageSize" => 50), $arSelect);
        while ($ob = $res->GetNext()) {
            $SUB_SECTIONS[$k]['ITEMS'][] = $ob;
        }
    }


//От редактора
    $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
    $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "=SECTION_ID" => $SECTION_ID, "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, Array("nPageSize" => 1), $arSelect);
    while ($ob = $res->GetNext()) {
        $redactor = $ob;
    }

//Следующий и предыдущий раздел
    $arFilter2 = Array('IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y', '=DEPTH_LEVEL' => '1');
    $db_list2 = CIBlockSection::GetList(Array("order" => "asc"), $arFilter2, true);

    while ($ar_result = $db_list2->GetNext()) {
        $PARENTS[] = $ar_result['ID'];
    }

    $position = array_search($SECTION_ID, $PARENTS);
    $next = $position + 1;
    $prev = $position - 1;

    if ($next) {
        $res_next = CIBlockSection::GetByID($PARENTS[$next]);
        if ($ar_res = $res_next->GetNext()) {
            $arResult['NEXT'] = $ar_res['SECTION_PAGE_URL'];
        } else {
            $arResult['NEXT'] = NULL;
        }
    }

    if ($prev === 0) {
        $res_prev = CIBlockSection::GetByID($PARENTS[0]);
        if ($ar_res = $res_prev->GetNext()) {
            $arResult['PREV'] = $ar_res['SECTION_PAGE_URL'];
        } else {
            $arResult['PREV'] = NULL;
        }
    } else {
        if ($prev) {
            $res_prev = CIBlockSection::GetByID($PARENTS[$prev]);
            if ($ar_res = $res_prev->GetNext()) {
                $arResult['PREV'] = $ar_res['SECTION_PAGE_URL'];

            } else {
                $arResult['PREV'] = NULL;
            }
        }
    }
}


$arResult['SUB_SECTION'] = $SUB_SECTIONS;
$arResult['REDACTOR'] = $redactor;

$prev_n = CIBlockSection::GetList(["SORT"=>"desc"],["IBLOCK_ID"=>$IBLOCK_ID, "SECTION_ID"=>NULL, "<ID"=>$arResult["ID"]])->GetNext();

if($prev_n)
{
    $some_articles = CIBlockElement::GetList(["rand"=>"asc"],["IBLOCK_ID"=>$IBLOCK_ID, "SECTION_ID"=>$prev_n["ID"], "INCLUDE_SUBSECTIONS"=>"Y", "!NAME"=>'От редактора', "ACTIVE"=>"Y"],0,["nTopCount"=>5],["NAME","DETAIL_PAGE_URL"],0,0,["NAME","DETAIL_PAGE_URL"]);

    while ($a = $some_articles->GetNext())
    {
        $arResult["RAND_ARTICLES"][] = $a;
    }
}
