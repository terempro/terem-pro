
<?php
//if($USER->isAdmin())
//{
	
	$groups = 
	[
		29114,
		29113,
		29112,
		29111,
		29108,
		29107,
		256064,
		29134,
		29133,
		29132,
		29131,
		29130,
		270474,
		29140,
		29139,
		29138,
		29137,
		270789,
		29146,
		29145,
		29144,
		29143,
		29142,
		271562,
		29121,
		29120,
		29119,
		29118,
		29117,
		29116,
		270355,
		29194,
		29199,
		29198,
		29197,
		29196,
		29195,
		274828,
	];
	
	
	$db_old_groups = CIBlockElement::GetElementGroups($arResult['ID'], true);
	$ar_new_groups = Array($NEW_GROUP_ID);
	while($ar_group = $db_old_groups->Fetch())
	{
		 $id_group = $ar_group["ID"];
	}
   
	if (in_array($id_group, $groups))
	{
		$arResult['OLD_TEMPLATE'] = 1;
	}else
	{
		$arResult['OLD_TEMPLATE'] = 0;
	}

	//echo "<pre>";
	//var_dump($id_group);
	//var_dump($arResult['OLD_TEMPLATE']);
//}

?>


<?php $this->SetViewTarget('meta_article'); ?>
<!--  -->
<meta property="og:title" content="<?=$arResult["NAME"]?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://<?=$_SERVER["HTTP_HOST"].$arResult["DETAIL_PAGE_URL"]?>" />
<meta property="og:image" content="https://<?=$_SERVER["HTTP_HOST"].CFile::GetPath($arResult["PREVIEW_PICTURE"]["ID"])?>" />
<meta property="og:description" content="<?=$arResult["PREVIEW_TEXT"]?>" />
<?php $this->EndViewTarget(); ?>

<?php

// сортировку берем из параметров компонента
$arSort = array(
    $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
    $arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
);
// выбрать нужно id элемента, его имя и ссылку. Можно добавить любые другие поля, например PREVIEW_PICTURE или PREVIEW_TEXT
$arSelect = array(
    "ID",
    "NAME",
    "DETAIL_PAGE_URL"
);
// выбираем активные элементы из нужного инфоблока. Раскомментировав строку можно ограничить секцией
$arFilter = array (
    "IBLOCK_ID" => $arResult["IBLOCK_ID"],
    "SECTION_CODE" => $arParams["SECTION_CODE"],
    "ACTIVE" => "Y",
    "CHECK_PERMISSIONS" => "Y",
);
// выбирать будем по 1 соседу с каждой стороны от текущего
$arNavParams = array(
    "nPageSize" => 1,
    "nElementID" => $arResult["ID"],
);
$arItems = Array();
$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
$rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
while($obElement = $rsElement->GetNextElement())
    $arItems[] = $obElement->GetFields();
// возвращается от 1го до 3х элементов в зависимости от наличия соседей, обрабатываем эту ситуацию
if(count($arItems)==3):
    $arResult["TORIGHT"] = Array("NAME"=>$arItems[0]["NAME"], "URL"=>$arItems[0]["DETAIL_PAGE_URL"]);
    $arResult["TOLEFT"] = Array("NAME"=>$arItems[2]["NAME"], "URL"=>$arItems[2]["DETAIL_PAGE_URL"]);
elseif(count($arItems)==2):
    if($arItems[0]["ID"]!=$arResult["ID"])
        $arResult["TORIGHT"] = Array("NAME"=>$arItems[0]["NAME"], "URL"=>$arItems[0]["DETAIL_PAGE_URL"]);
    else
        $arResult["TOLEFT"] = Array("NAME"=>$arItems[1]["NAME"], "URL"=>$arItems[1]["DETAIL_PAGE_URL"]);
endif;



$some_articles = CIBlockElement::GetList(["rand"=>"asc"],["IBLOCK_ID"=>41, "ACTIVE"=>"Y", "SECTION_ID"=>$arResult["SECTION"]["IBLOCK_SECTION_ID"], "INCLUDE_SUBSECTIONS"=>"Y", "!NAME"=>'%редактора%', "!ID"=>$arResult["ID"]],0,["nTopCount"=>3],["NAME","DETAIL_PAGE_URL","PREVIEW_PICTURE", "PREVIEW_TEXT"]);

while ($a = $some_articles->GetNext())
{
    $arResult["RAND_ARTICLES"][] = $a;
}
