<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?php if(!$arResult['OLD_TEMPLATE']):?>

<?php if ($arResult["PROPERTIES"]["ACTIVE"]["~VALUE"] == 'Активировать'): ?>

	


	  <section class="content text-page white" role="content">
		<div class="container">

		  <div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
			  <div class="white my-margin clearfix">
				<div class="journal journal-article-layer">
				  <div class="journal__wrapper">
					<?php if ($arResult["RAND_ARTICLES"]): ?>
					  <div class="journal__content">
						<div class="journal-article-about-content hidden-sm">
						  <div class="journal-recommend">
							<div class="journal-recommend__title">Также в номере</div>
							<ul class="journal-recommend__list">
							  <?php foreach ($arResult["RAND_ARTICLES"] as $a): ?>
								<li class="journal-recommend__item">
								  <a href="<?= $a["DETAIL_PAGE_URL"] ?>">
									<img src="<?= $a["PREVIEW_PICTURE"] ? CFile::GetPath($a["PREVIEW_PICTURE"]) : '/bitrix/templates/.default/assets/img/journal_thumbnail.png' ?>" alt="Название статьи" class="journal-recommend__img">
									<span><?= $a["NAME"] ?></span>
									<small><?= $a["PREVIEW_TEXT"] ?></small>
								  </a>
								</li>
							  <?php endforeach; ?>
							</ul>
						  </div>
						  <a href="/jurnal/<?= $arResult["SECTION"]["PATH"][0]["CODE"] ?>/" class="btn btn-danger contact-btn text-uppercase">Все статьи номера</a>
						</div>
					  </div>
					<?php endif; ?>
					<div class="journal__aside">
					  <?php if ($arResult["PROPERTIES"]["TITLE1"]["VALUE"]): ?>
						<div class="row">
						  <div class="col-xs-12 col-md-12 col-sm-12">
							<div class="journal-article-title">
							  <h1><?= $arResult["PROPERTIES"]["TITLE1"]["~VALUE"] ?></h1>
							</div>
						  </div>
						</div>
					  <?php endif; ?>


					  <?php if ($arResult["PROPERTIES"]["TEXT1"]["VALUE"]["TEXT"]): ?>
						<div class="text-description clearfix">
						  <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT1"]["VALUE"]["TEXT"]) ?></p>
						</div>
					  <?php endif; ?>


					  <?php if ($arResult["PROPERTIES"]["IMAGE1"]["~VALUE"]): ?>
						<div class="row">
						  <div class="col-xs-12 col-md-12 col-sm-12">
							<div class="img-with-description clearfix">
							  <img src="<?= CFile::GetPath($arResult["PROPERTIES"]["IMAGE1"]["~VALUE"]) ?>" class="full-width">

							  <?php
							  $rsFile = CFile::GetByID($arResult["PROPERTIES"]["IMAGE1"]["~VALUE"]);
							  $arFile = $rsFile->Fetch();
							  ?>

							  <p><i><?= $arFile["DESCRIPTION"] ?></i></p>

							</div>
						  </div>
						</div>
					  <?php endif; ?>


					  <?php if ($arResult["PROPERTIES"]["TITLE2"]["~VALUE"]): ?>
						<div class="text-title clearfix">
						  <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE2"]["~VALUE"] ?></h2>
						</div>
					  <?php endif; ?>

					  <?php if ($arResult["PROPERTIES"]["TEXT2"]["VALUE"]["TEXT"]): ?>
						<div class="text-description clearfix">
						  <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT2"]["VALUE"]["TEXT"]) ?></p>
						</div>
					  <?php endif; ?>


					  <?php if ($arResult["PROPERTIES"]["PREV1"]["~VALUE"]): ?>
						<div class="arrow-title clearfix">
						  <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
							  <img src="/bitrix/templates/.default/assets/img/expert/sep2.png" alt="arrow-left" class="full-width">
							</div>

							<div class="col-xs-12 col-sm-10 col-md-10 text-center col-sm-offset-1 col-md-offset-1">
							  <p><?= $arResult["PROPERTIES"]["PREV1"]["~VALUE"] ?></p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12">
							  <img src="/bitrix/templates/.default/assets/img/expert/sep1.png" alt="arrow-right" class="full-width arrow-right-img">
							</div>
						  </div>
						</div>
					  <?php endif; ?>

					  <div class="text-with-img right-img">
						<?php if ($arResult["PROPERTIES"]["TITLE3"]["~VALUE"]): ?>
						  <div class="row">
							<div class="text-title">
							  <div class="col-xs-12 col-md-12 col-sm-12">
								<h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE3"]["~VALUE"] ?></h2>
							  </div>
							</div>
						  </div>
						<?php endif; ?>
						<div class="row">
						  <div class="col-xs-12 col-md-8 col-sm-8">

							<?php if ($arResult["PROPERTIES"]["TEXT3"]["VALUE"]["TEXT"]): ?>
							  <div class="text-description">
								<p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT3"]["VALUE"]["TEXT"]) ?></p>
							  </div>
							<?php endif; ?>

						  </div>

						  <?php if ($arResult["PROPERTIES"]["IMAGE2"]["~VALUE"]): ?>
							<div class="col-xs-12 col-md-4 col-sm-4">
							  <div class="img-with-description">
								<img src="<?= CFile::GetPath($arResult["PROPERTIES"]["IMAGE2"]["~VALUE"]) ?>" class="full-width">
								<?php
								$rsFile = CFile::GetByID($arResult["PROPERTIES"]["IMAGE2"]["~VALUE"]);
								$arFile = $rsFile->Fetch();
								?>
								<p><i><?= $arFile["DESCRIPTION"] ?></i></p>

							  </div>
							</div>
						  <?php endif; ?>
						</div>
					  </div>

					  <?php if ($arResult["PROPERTIES"]["TITLE4"]["~VALUE"]): ?>
						<div class="text-title clearfix">
						  <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE4"]["~VALUE"] ?></h2>
						</div>
					  <?php endif; ?>

					  <?php if ($arResult["PROPERTIES"]["TEXT4"]["VALUE"]["TEXT"]): ?>
						<div class="text-description clearfix">
						  <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT4"]["VALUE"]["TEXT"]) ?></p>
						</div>
					  <?php endif; ?>



					  <?php if (is_array($arResult["PROPERTIES"]["IMAGE3"]["VALUE"])): ?>
						<div class="row">
						  <?php foreach ($arResult["PROPERTIES"]["IMAGE3"]["VALUE"] as $v): ?>
							<?php
							$rsFile = CFile::GetByID($v);
							$arFile = $rsFile->Fetch();
							?>
							<div class="col-xs-12 col-md-3 col-sm-3">
							  <div class="img-with-description">
								<img src="<?= CFile::GetPath($v) ?>" class="full-width">
								<p><i><?= $arFile["DESCRIPTION"] ?></i></p>
							  </div>
							</div>
						  <?php endforeach; ?>
						</div>
					  <?php endif; ?>


					  <?php if ($arResult["PROPERTIES"]["TITLE5"]["~VALUE"]): ?>
						<div class="text-title clearfix">
						  <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE5"]["~VALUE"] ?></h2>
						</div>
					  <?php endif; ?>

					  <?php if ($arResult["PROPERTIES"]["TEXT5"]["VALUE"]["TEXT"]): ?>
						<div class="text-description clearfix">
						  <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT5"]["VALUE"]["TEXT"]) ?></p>
						</div>
					  <?php endif; ?>

					  <?php if ($arResult["PROPERTIES"]["PREV2"]["~VALUE"]): ?>
						<div class="arrow-title clearfix">
						  <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
							  <img src="/bitrix/templates/.default/assets/img/expert/sep2.png" alt="arrow-left" class="full-width">
							</div>


							<div class="col-xs-12 col-sm-10 col-md-10 text-center col-sm-offset-1 col-md-offset-1">
							  <p><?= $arResult["PROPERTIES"]["PREV2"]["~VALUE"] ?></p>
							</div>


							<div class="col-xs-12 col-sm-12 col-md-12">
							  <img src="/bitrix/templates/.default/assets/img/expert/sep1.png" alt="arrow-right" class="full-width arrow-right-img">
							</div>
						  </div>
						</div>
					  <?php endif; ?>
					  <?php if ($arResult["PROPERTIES"]["TITLE6"]["~VALUE"]): ?>
						<div class="text-title clearfix">
						  <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE6"]["~VALUE"] ?></h2>
						</div>
					  <?php endif; ?>

					  <?php if ($arResult["PROPERTIES"]["TEXT6"]["VALUE"]["TEXT"]): ?>
						<div class="text-description clearfix">
						  <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT6"]["VALUE"]["TEXT"]) ?></p>
						</div>
					  <?php endif; ?>



					  <?php if (is_array($arResult["PROPERTIES"]["IMAGE4"]["VALUE"])): ?>

						<div class="row">
						  <?php foreach ($arResult["PROPERTIES"]["IMAGE4"]["VALUE"] as $i): ?>

							<?php
							$rsFile = CFile::GetByID($i);
							$arFile = $rsFile->Fetch();
							?>

							<div class="col-xs-12 col-md-4 col-sm-4">
							  <div class="img-with-description">
								<img src="<?= CFile::GetPath($i) ?>" class="full-width">
								<p><i><?= $arFile["DESCRIPTION"] ?></i></p>

							  </div>
							</div>
						  <?php endforeach; ?>
						</div>

					  <?php endif; ?>


					  <?php if ($arResult["PROPERTIES"]["TITLE7"]["~VALUE"]): ?>
						<div class="text-title clearfix">
						  <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE7"]["~VALUE"] ?></h2>
						</div>
					  <?php endif; ?>

					  <?php if ($arResult["PROPERTIES"]["TEXT7"]["VALUE"]["TEXT"]): ?>
						<div class="text-description clearfix">
						  <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT7"]["VALUE"]["TEXT"]) ?></p>
						</div>
					  <?php endif; ?>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </section>
	<?php else: ?>

	  <section class="content text-page white" role="content">
		<div class="container">

		  <div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
			  <div class="white my-margin clearfix">
				<div class="journal journal-article-layer">
				  <div class="journal__wrapper">

					<div class="journal__content">


						  <div class="journal-article-title">
							<h1><?= $arResult['NAME'] ?></h1>
						  </div>


					  <div class="journal-article-content">
						<?= $arResult['DETAIL_TEXT'] ?>
					  </div>







					</div>
					<?php if ($arResult["RAND_ARTICLES"]): ?>
					  <div class="journal__aside">

						  <div class="journal-recommend">
							<div class="journal-recommend__title">Также в номере</div>
							<ul class="journal-recommend__list">
							  <?php foreach ($arResult["RAND_ARTICLES"] as $a): ?>
								<li class="journal-recommend__item">
								  <a href="<?= $a["DETAIL_PAGE_URL"] ?>">
									<img src="<?= $a["PREVIEW_PICTURE"] ? CFile::GetPath($a["PREVIEW_PICTURE"]) : '/bitrix/templates/.default/assets/img/journal_thumbnail.png' ?>" alt="Название статьи" class="journal-recommend__img">
									<span><?= $a["NAME"] ?></span>
									<small><?= $a["PREVIEW_TEXT"] ?></small>
								  </a>
								</li>
							  <?php endforeach; ?>
							</ul>
						  </div>
						  <a href="/jurnal/<?= $arResult["SECTION"]["PATH"][0]["CODE"] ?>/" class="btn btn-danger contact-btn text-uppercase">Все статьи номера</a>

					  </div>
					<?php endif; ?>
				  </div>
				  <div class="journal-nav">
					<div>
					  <?php if(is_array($arResult["TOLEFT"])):?>
						<a href="<?= $arResult["TOLEFT"]["URL"] ?>">< <span>НАЗАД</span></a>
					  <?php endif; ?>
					</div>
					<div>
					  <?php if(is_array($arResult["TORIGHT"])):?>
						<a href="<?= $arResult["TORIGHT"]["URL"] ?>"><span>ВПЕРЕД</span> ></a>
					  <?php endif; ?>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </section>

	<?php endif; ?>

<?php else:?>

<section class="content text-page white" role="content">
    <div class="container">

      <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
          <div class="white my-margin clearfix">
            <div class="journal journal-article-layer">
              <div class="journal__wrapper">

                <div class="journal__content" style="max-width: 100% !important;">


                      <div class="journal-article-title">
                        <h1><?= $arResult['NAME'] ?></h1>
                      </div>


                  <div class="journal-article-content">
                    <?= $arResult['DETAIL_TEXT'] ?>
                  </div>







                </div>
               
              </div>
              <div class="journal-nav">
                <div>
                  <?php if(is_array($arResult["TOLEFT"])):?>
                    <a href="<?= $arResult["TOLEFT"]["URL"] ?>">< <span>НАЗАД</span></a>
                  <?php endif; ?>
                </div>
                <div>
                  <?php if(is_array($arResult["TORIGHT"])):?>
                    <a href="<?= $arResult["TORIGHT"]["URL"] ?>"><span>ВПЕРЕД</span> ></a>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
