<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
?>
<section class="content text-page" role="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="my-margin clearfix">
          <div class="journal journal-preview-layer">
            <!-- begin NEW BLOCK -->


              <a href="<?=$arResult['SECTIONS'][count($arResult['SECTIONS'])-(1||$USER->IsAuthorized() ? 1 : 2)]["SECTION_PAGE_URL"]?>">
                <div class="journal-preview">
                  <div class="journal-preview-header">
                    <img src="<?=CFile::GetPath(CIBLockSection::GetList([],["IBLOCK_ID"=>41,"ID"=>$arResult['SECTIONS'][count($arResult['SECTIONS'])-(1||$USER->IsAuthorized() ? 1 : 2)]["ID"]],false,["UF_*"])->GetNext()["UF_PICTURE"])?>" alt="">
                  </div>
                </div>
              </a>


            <!-- end NEW BLOCK -->

            <?php $cnt = count($arResult['SECTIONS'])-(1||$USER->IsAuthorized() ? 2 : 3); $epr = 0; ?>
            <?php for($i = $cnt; $i >= 0; --$i):?>
              <?php if($arResult['SECTIONS'][$i]['DEPTH_LEVEL']):?>

                <?php if(!($epr%4)):?>
                  <div class="row">
                  <?php endif; ?>
                      <?php ++$epr; ?>
                  <div class="col-xs-12 col-md-3 col-sm-3">
                    <a href="<?=$arResult['SECTIONS'][$i]["SECTION_PAGE_URL"]?>">
                      <div class="journal-preview">
                        <div class="journal-preview-header">
                          <img src="<?=$arResult['SECTIONS'][$i]["PICTURE"]["SRC"]?>" alt="">
                        </div>
                        <div class="journal-preview-content">
                          <p>Выпуск №<?=($i+1)?></p>
                          <h4>
                            <?=$arResult['SECTIONS'][$i]['NAME']?>
                          </h4>
                        </div>
                      </div>
                    </a>
                  </div>

                <?php if(!($epr%4)):?>
                    </div>
                <?php endif; ?>


                <?php endif; ?>

              <?php endfor; ?>


            </div>
          </div>
        </div>
      </div>

    </div>
  </section>
