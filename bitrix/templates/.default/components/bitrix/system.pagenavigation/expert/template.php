<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

$curPage = $arResult["NAV_RESULT"]->NavPageNomer;
$totalPages = $arResult["NAV_RESULT"]->NavPageCount;
$navNum = $arResult["NAV_RESULT"]->NavNum;
?>
<?php



if ($_GET['PAGEN_1'] != NULL && $_GET['PAGEN_1'] > 0 && $_GET['PAGEN_1'] != 0) {
    $page = str_replace("?SORT=view", " ", $_GET['PAGEN_1']);
    $page = str_replace("?SORT=date", " ", $page);

} else {
    $page = 1;
}

$nex = $page+1;
$prev = $page-1;

?>
<div class="col-xs-12 col-md-6 col-sm-6 text-right">
    <div class="expert-pagination">
        <p class="inline">Страница</p>
        <input type="text" class="form-control my-input inline" value="<?= $page ?>">
        <p class="inline">из <span><?= $arResult["NavPageCount"] ?></span></p>
        <div class="btn-group border" role="group">
            <a href="?PAGEN_1=<?php echo $prev; ?>"><button type="button" class="btn btn-default"><span class="caret left"></span></button></a>
            <a href="?PAGEN_1=<?php echo $nex; ?>"><button type="button" class="btn btn-default"><span class="caret right"></span></button></a>
        </div>
    </div>
</div>
</div>