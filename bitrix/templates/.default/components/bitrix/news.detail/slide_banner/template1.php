<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="headliner-block" data-parallax="down">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
           <?if ($USER->IsAdmin()):?>
                <a href="<?= $arResult["PROPERTIES"]["LINK"]["VALUE"] ?>" class="headliner hidden-xs + my-margin op-0">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <h1>
                                <?= $arResult["NAME"] ?>
                            </h1>
                        </div>
                        <div class="col-xs-12 col-md-10 col-sm-9">
                          <div class="container-timer"><p>Дескрипт слогана</p></div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-sm-3">
                            
                        </div>
                    </div>
                </a>
            <?endif;?>
            <div class="row hidden-xs fixed">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="my-flex slider-navigation">
                        <div class="main-dots">

                        </div>
                        <div class="main-navigation">

                        </div>
                    </div>
                </div>
            </div>
             <?if ($USER->IsAdmin()):?>
                <a href="<?= $arResult["PROPERTIES"]["LINK"]["VALUE"] ?>" class="headliner visible-xs + my-margin op-0">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <h1>
                                <?= $arResult["NAME"] ?>
                            </h1>
                        </div>
                        <div class="col-xs-12 col-md-10 col-sm-9">
                           <div class="container-timer"><p>Дескрипт слогана</p></div>
                        </div>
                    </div>
                </a> 
            <?endif;?>
        </div>
    </div>
</div>