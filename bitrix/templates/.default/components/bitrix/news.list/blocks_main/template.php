<div class="col-xs-12 col-md-6 col-sm-12">
    <div class="flex-reversed">

        <?php
            $slide_count = count($arResult["SLIDERS"]);
            $step_slide = 0;
        ?>

        <?php foreach ($arResult["SLIDERS"] as $item): ?>

            <?php if($slide_count >= 2):?>

                <?php if($step_slide == 0):?>
                        <div class="row" style="padding: 0 5px;">
                        <div class="col-xs-12 col-md-12 col-sm-12 my-item-slider" data-item="slider-info-block"  style="padding: 0">
                <?php endif;?>

                    <div class="my-promotion desgin-2 + my-margin">
                        <div>
                            <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>
                            </a>
                        </div>
                        <div>
                            <div class="news-header">
                                <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                    <?php echo html_entity_decode($item["NAME"]); ?>
                                </a>
                            </div>
                            <?php if ($item["PREVIEW_TEXT"] != ""): ?>
                                <p><?= $item["PREVIEW_TEXT"] ?></p>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php $step_slide++; ?>

                <?php if($slide_count == $step_slide):?>
                        </div>
                    </div>
                <?php endif;?>

            <?php endif; ?>

        <?php endforeach; ?>







        <?php foreach ($arResult["ITEMS"] as $item): ?>
            <?php if ($item["PROPERTIES"]["POSITION"]["VALUE"] == "Справа"): ?>
                
                <?php if ($item["PROPERTIES"]["TYPE"]["VALUE"] == "Обычный"): ?>


                <div class="row">

                    <div class="col-xs-12 col-md-12 col-sm-12 " >

                                    <div class="my-promotion desgin-2 + my-margin">
                                        <div>
                                            <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                                <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>
                                            </a>
                                        </div>
                                        <div>
                                            <div class="news-header">
                                                <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                                    <?php echo html_entity_decode($item["NAME"]); ?>
                                                </a>
                                            </div>
                                            <?php if ($item["PREVIEW_TEXT"] != ""): ?>
                                                <p><?= $item["PREVIEW_TEXT"] ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                    </div>

                </div>
                        

                <?php endif; ?>


            <?php endif; ?>
        <?php endforeach; ?> 








    </div>
</div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="flex-reversed">
            <div class="row">
                <?php foreach ($arResult["ITEMS"] as $item): ?>
                    <?php if ($item["PROPERTIES"]["TYPE"]["VALUE"] == "Центральный"): ?>
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="my-promotion desgin-2 + my-margin">
                                <div>
                                    <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                        <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>
                                    </a>
                                </div>
                                <div>
                                    <div class="news-header">
                                        <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                            <?php echo html_entity_decode($item["NAME"]); ?>
                                        </a>
                                    </div>
                                    <?php if ($item["PREVIEW_TEXT"] != ""): ?>
                                        <p><?= $item["PREVIEW_TEXT"] ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="flex-reversed">
        	<div class="row">
			    <div class="col-xs-12 col-md-12 col-sm-12">
			        <?php foreach ($arResult["ITEMS"] as $item): ?>
			            <?php if ($item["PROPERTIES"]["POSITION"]["VALUE"] == "Слева"): ?>
			                <?php if ($item["PROPERTIES"]["TYPE"]["VALUE"] == "Длинный"): ?>
			                    <div class="row">
			                        <div class="col-xs-12 col-md-12 col-sm-12">
			                            <div class="my-promotion ( desgin-2 && one-column ) + my-margin">
			                                <div>
			                                    <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
			                                        <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>

			                                    </a>
			                                </div>
			                                <div>
                            
			                                    <div class="news-header">
			                                        <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
			                                            <?php echo html_entity_decode($item["NAME"]); ?>
			                                        </a>
			                                    </div>

			                                    <?php if ($item["PREVIEW_TEXT"] != ""): ?>
			                                        <p><?= $item["PREVIEW_TEXT"] ?></p>
			                                    <?php endif; ?>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                <?php endif; ?>
			            <?php endif; ?>
			        <?php endforeach; ?> 
			    </div>
			</div>
            <div class="row">
                <?php foreach ($arResult["ITEMS"] as $item): ?>
                    <?php if ($item["PROPERTIES"]["POSITION"]["VALUE"] == "Слева"): ?>
                        <?php if ($item["PROPERTIES"]["TYPE"]["VALUE"] == "Обычный"): ?>

                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-2 + my-margin">
                                    <div>
                                        <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                            <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>
                                        </a>
                                    </div>
                                    <div>
                                   
                                        
                                    <?php if(strlen($item["NAME"]) == 59):?>
                                        <div class="news-header">
                                            <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                                <?php echo html_entity_decode($item["NAME"]); ?>
                                            </a>
                                        </div>
                                    <?php else:?>
                                        <div class="news-header">
                                            <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                                <?php echo html_entity_decode($item["NAME"]); ?>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                        <?php if ($item["PREVIEW_TEXT"] != ""): ?>
                                            <p><?= $item["PREVIEW_TEXT"] ?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?> 
            </div>

        </div>
    </div>
</div>




</div>