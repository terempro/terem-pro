<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="slider-with-trigger">
    <div class="my-item-slider + my-margin" data-item="slider-item">
        <?php $cnt1 = 0; ?>
        <?php foreach ($arResult["ITEMS"] as $arItem): ?>
            <div class="item">
                <div class="iner" style="position: relative;">

                    <?php if($arItem["PROPERTIES"]["COUNTER_LEFT"]["VALUE"]): ?>
                        <style>
                            .temporary-counter
                            {
                                
                                width: 80%;
                                position: absolute;
                                bottom: 10%;
                                left: 10%;
                                color: #000;
                                z-index: 45;
                                font-weight: 600;
                                font-family: 'Reforma','Arial',sans-serif;
                            }
                            
                            .temporary-counter .left-word
                            {
                                font-size: 44px;
                                text-align: center;
                                text-transform: uppercase;
                                padding-bottom: 10px;
                            }
                            
                            .temporary-counter .counter-board
                            {
                                display: flex;
                                justify-content: center;
                            }
                            
                            .temporary-counter .counter-board > div
                            {
                                display: flex;
                                position: relative;
                            }
                            
                            .temporary-counter .counter-board > div:not(:first-child)
                            {
                                margin-left: 20px;
                            }
                            
                            .temporary-counter .counter-board > div:not(:first-child)::before
                            {
                                display: inline-block;
                                max-width: 10px;
                                position: absolute;
                                left: -15px;
                                top: 50%;
                                margin-top: -16px;
                                content: '\2022\a\2022';
                                font-size: 36px;
                                line-height: 20px;
								color: <?= $arItem["PROPERTIES"]["COUNTER_COLOUR"]["VALUE"] ?>;
                            }
                            
                            .temporary-counter .counter-board > div::after
                            {
                                font: bold 14px Verdana, Arial, sans-serif;
                                display: block;
                                width: 100%;
                                text-align: center;
                                padding-top: 6px;
                                position: absolute;
                                top: 100%;
								color: <?= $arItem["PROPERTIES"]["COUNTER_COLOUR"]["VALUE"] ?>;
                            }
                            
                            .temporary-counter .counter-board > div.counter-hours::after
                            {
                                content: 'часов';
                            }
                            
                            .temporary-counter .counter-board > div.counter-minutes::after
                            {
                                content: 'минут';
                            }
                            
                            .temporary-counter .counter-board > div.counter-seconds::after
                            {
                                content: 'секунд';
                            }
                            
                            .temporary-counter .tablo
                            {
                                width: 58px;
                                height: 79px;
                                line-height: 79px;
                                background-image: url('/bitrix/templates/.default/assets/img/tablo.png');
                                background-size: 100% 100%;
                                color: #fff;
                                font-size: 64px;
                                text-align: center;
                            }
                            
                            .temporary-counter .tablo:first-child
                            {
                                margin-right: 5px;
                            }
                            
                            .temporary-counter .counter-board > div:last-child .tablo
                            {
                                color: #c00;
                            }
                            
                            @media(max-width: 1200px)
                            {
                                .temporary-counter .left-word
                                {
                                    font-size: 36px;
                                }
                                
                                .temporary-counter .tablo
                                {
                                    width: 52px;
                                    height: 70px;
                                    line-height: 70px;
                                    font-size: 56px;
                                }
                            }
                            
                            @media(max-width: 800px)
                            {
                                .temporary-counter .left-word
                                {
                                    font-size: 20px;
                                }
                                
                                .temporary-counter .tablo
                                {
                                    width: 35px;
                                    height: 40px;
                                    line-height: 40px;
                                    font-size: 27px;
                                }
                            }
                        </style>
                        <?php
                        $diff = strtotime(str_replace('T', ' ', $arItem["PROPERTIES"]["COUNTER_LEFT"]["VALUE"])) - time();
                        
                        $h0 = floor(floor($diff/3600)/10);
                        $h1 = floor($diff)%10;
                        $m0 = floor((floor($diff/60)%60)/10);
                        $m1 = (floor($diff/60)%60)%10;
                        $s0 = floor((floor($diff)%60)/10);
                        $s1 = (floor($diff)%600)%10;
                        ?>
                        <div class="temporary-counter">
                            <div style="color:<?= $arItem["PROPERTIES"]["COUNTER_COLOUR"]["VALUE"] ?>" class="left-word"><?= $arItem["PROPERTIES"]["COUNTER_TEXT"]["VALUE"] ?></div>
                            <div class="counter-board">
                                <div class="counter-hours">
                                    <div class="tablo"><?= $h0 ?></div>
                                    <div class="tablo"><?= $h1 ?></div>
                                </div>
                                <div class="counter-minutes">
                                    <div class="tablo"><?= $m0 ?></div>
                                    <div class="tablo"><?= $m1 ?></div>
                                </div>
                                <div class="counter-seconds">
                                    <div class="tablo"><?= $s0 ?></div>
                                    <div class="tablo"><?= $s1 ?></div>
                                </div>
                            </div>
                        </div>
                        <script>
                            var till = +(new Date(`<?= $arItem["PROPERTIES"]["COUNTER_LEFT"]["VALUE"] ?>+03:00`));
                            setInterval(function()
                            {
                                var now = +Date.now();
                                var diff = Math.round((till - now)/1000);
                                if (diff < 0) return;
                                
                                var h0 = Math.floor(Math.floor(diff/3600)/10);
                                var h1 = Math.floor(diff/3600)%10;
                                var m0 = Math.floor((Math.floor(diff/60)%60)/10);
                                var m1 = (Math.floor(diff/60)%60)%10;
                                var s0 = Math.floor((Math.floor(diff)%60)/10);
                                var s1 = (Math.floor(diff)%600)%10;
                                
                                document.querySelector('.counter-hours .tablo:first-child').innerHTML = h0;
                                document.querySelector('.counter-hours .tablo:last-child').innerHTML = h1;
                                document.querySelector('.counter-minutes .tablo:first-child').innerHTML = m0;
                                document.querySelector('.counter-minutes .tablo:last-child').innerHTML = m1;
                                document.querySelector('.counter-seconds .tablo:first-child').innerHTML = s0;
                                document.querySelector('.counter-seconds .tablo:last-child').innerHTML = s1;
                            }, 1000);
                        </script>
                    <?php endif; ?>


                    <a href="<?=$arItem["PROPERTIES"]["LINK"]["~VALUE"]?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"]; ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"]; ?>"></a>
                    <?php if($cnt1 != 0): ?>
                    <div class="title-item text-center">
                        <?php if ($arItem["PROPERTIES"]["NAME"]["~VALUE"]): ?>
                            <h4><?= $arItem["PROPERTIES"]["NAME"]["~VALUE"] ?></h4>
                        <?php endif; ?>
                        <?php if ($arItem["PROPERTIES"]["SIZE"]["~VALUE"]): ?>
                            <h5><?= $arItem["PROPERTIES"]["SIZE"]["~VALUE"] ?></h5>
                        <?php endif; ?>
                        <?php if ($arItem["PROPERTIES"]["TEHNOLOGY"]["~VALUE"]): ?>
                            <h5><?= $arItem["PROPERTIES"]["TEHNOLOGY"]["~VALUE"] ?></h5>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php $cnt1++; ?>
        <?php endforeach; ?>
    </div>



    <div data-item="trigger-set">
        <?php $cnt = 0; ?>
        <?php foreach ($arResult["ITEMS"] as $arItem): ?>

            <?php if ($arItem["PROPERTIES"]["STAR"]["VALUE_XML_ID"] == "Yes"): ?>
                <div class="trigger" data-trigger="<?= $cnt; ?>">
                  <?php if($arItem["PROPERTIES"]["TEXT_STAR"]["VALUE"]):?>
                        <?php echo html_entity_decode($arItem["PROPERTIES"]["TEXT_STAR"]["VALUE"]);?>
                  <?php endif;?>
                </div>
            <?php else: ?>
                <div class="trigger" style="background:transparent;" data-trigger="<?= $cnt; ?>"></div>
            <?php endif; ?>


            <?php $cnt++; ?>
        <?php endforeach; ?>
    </div>



</div>
