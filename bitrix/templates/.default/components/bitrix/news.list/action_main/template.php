<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php foreach($arResult["ITEMS"] as $arItem):?>
<div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="my-promotion desgin-3 + my-margin title-thin">
            <div>
                <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
            </div>
            <div>
                <h4 class="text-uppercase">
                   <?php echo html_entity_decode($arItem["NAME"]); ?>
                </h4>
                <?php echo html_entity_decode($arItem["PREVIEW_TEXT"]);?>
                <button data-yagoal="CONSULT_HOUSE_CARD_CALLED" type="button" class="cta-btn visible-xs" data-toggle="modal" data-target="#formAdviceItem" data-code="" data-price="">Получить предложение</button>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
