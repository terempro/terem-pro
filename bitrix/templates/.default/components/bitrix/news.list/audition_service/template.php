<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<section class="content text-page white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    дополнительные услуги
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--left column-->
                        <div class="col-xs-12 col-md-8 col-sm-8" data-item="global-calc">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <p>
                                        <strong>Отметьте нужные услуги в списке, и цена работ будет рассчитана автоматически.</strong>
                                    </p>
                                    <p> Дополнительные услуги охватывают все ваши возможные требования к будущему дому, и не являются обязательными. Если у вас есть сомнения по какому-либо вопросу, рекомендуем обратиться к нашим консультантам в выставочном центре или по телефону. </p>
                                </div>
                            </div>

                            <?php foreach ($arResult["CALCS"] as $k => $items): ?>
                                <?php
                                /*var_dump($items['NAME']);*/
                                $id_cat = NULL;
                                if(strripos($items['NAME'], 'Оптимальный')){
                                    $id_cat = 'optimal_table';
                                    
                                }else{
                                   if($items['NAME'] != 'Прочие услуги'){
                                    $class_cat = 'category_table';
                                }
                                }


                                
                                ?>
                                <!--one item-->
                                
                                <div class="row <?php if($class_cat):?> <?=$class_cat?> <?php endif;?>" <?php if($id_cat):?> id="<?=$id_cat?>" <?php endif;?> >
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-border ) collapsed" type="button" data-toggle="collapse" data-target="#faq<?= $k ?>" aria-expanded="false" aria-controls="faq<?= $k ?>">
                                                    <div class="my-table">
                                                        <div>
                                                            <p><?= $items['NAME'] ?></p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-menu-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="faq<?= $k ?>">
                                                    <div class="( my-collapse-content + design-border + no-padding )">
                                                        <table class="full-width calculate-table" data-item="calc">
                                                            <?php $cnt = 0; ?>    
                                                            <?php foreach ($items['ITEMS'] as $item): ?>
                                                                <?php 
                                                                if (preg_match('/Дальность/', $item['NAME']))
                                                                {
                                                                    continue;
                                                                }
                                                                $rand = rand(10, 9999);
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox black">
                                                                            <input type="checkbox" value="<?=  $item['PRICE']?:'0' ?>" id="checkbox<?=$cnt+$k+$rand?>" required>
                                                                            <label for="checkbox<?=$cnt+$k+$rand?>">
                                                                                <?=$item['NAME']?>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <p><?=number_format($item['PRICE'], 0, ' ', ' ');?></p>
                                                                    </td>
                                                                </tr>
                                                                <?php $cnt++; ?>        
                                                            <?php endforeach; ?>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--one item END-->
                            <?php endforeach; ?>
                        </div><!--left column END-->
                        <!--right column-->
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="right-side">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="total-price">
                                                    <div><img src="<?=CFile::GetPath($arResult["PICTURE_ID"]);?>" class="full-width" alt=""></div>
                                                    <div><h3><?=$arResult['NAME']?></h3></div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <p>Базовая комплектация</p>
                                                        </div>
                                                        <div class="col-xs-12 col-md-6 col-sm-6 text-right">

                                                            <p id="opt-price-1"><strong><?=number_format($arResult['PRICE'], 3, ' ', ' ');?> руб.</strong></p>
                                                            <?php if($arResult["PRICE_OPTIMAL"]):?><p id="opt-price-2" style="display: none;" id="optimal-price"><strong><?=$arResult["PRICE_OPTIMAL"]?> 000 руб.</strong></p><?php endif;?>
                                                            <p id="discont1" style="display:none"><?=$arResult["DISCONT1"]?></p>
                                                            <p id="discont2" style="display:none"><?=$arResult["DISCONT2"]?></p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <p>Дополнительные услуги</p>
                                                        </div>
                                                        <div class="col-xs-12 col-md-6 col-sm-6 text-right">
                                                            <p><strong><span id="sumdop">0</span> руб.</strong></p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                            <div class="all-sum"><p>= <span id="allsum" data-item-total="<?=$arResult['PRICE']?>000"><?=number_format($arResult['PRICE'], 3, ' ', ' ');?></span> руб.</p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="chosen-spec">
                                                    <h3 class="text-uppercase text-center">выбранные услуги</h3>
                                                    <ul class="list-items" data-item="scroll-bar" id="list_bar_calc">
                                                        
                                                    </ul>
                                                    <hr>
                                                    <button type="button" class="btn btn-default" onclick="window.print();">Распечатать список</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <!--right column END-->
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>