<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="content content-back-side hidden-xs start-show pointer-" data-parallax="up" data-opacity-main="true">
    <div class="container-fluid reset-padding">
        <div class="row reset-margin">
            <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">

<!--                <div class="star-toggler">-->
<!--                    <div class="starlight-trigger">-->
<!--                        <button class="off"></button>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="stars_bg hidden">-->
<!--                    <div class="planet_bg">-->
<!--                        <img src="/bitrix/templates/.default/assets/img/PL1.png">-->
<!--                    </div>-->
<!--                    <div class="stars"></div>-->
<!--                    <div class="twinkling"></div>-->
<!--                </div>-->


                <div class="my-item-slider back-side-slider" data-item="slider-back-side">

                    <?php
                    $cnt = 0;

                    foreach ($arResult["ITEMS"] as $arItem)
                    {
                        if($arItem['NEW_TEXT'])
                            echo $arItem['NEW_TEXT'];
                        else
                        { ?>
                            <div class="item margin_top">
                                <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"] ?>">
                                    <div class="inner">
                                        <div class="main-img" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"] ?>');">
                                           <canvas id="canvas_<?=$cnt?>"  style="width: 100% !important;"></canvas>
                                       </div>
                                       <div class="baner">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="inner with-top-padding content text-right">
                                                            <?php
                                                            if ($arItem["NAME"] != '.')
                                                                echo '<p>' . $arItem["NAME"] . '</p>';
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <?php
                                if($arItem["PROPERTIES"]["COUNTER_LEFT"]["VALUE"])
                                {
                                    $diff = strtotime(str_replace('T', ' ', $arItem["PROPERTIES"]["COUNTER_LEFT"]["VALUE"])) - time();
                                    $h0 = floor(floor($diff/3600)/10);
                                    $h1 = floor($diff)%10;
                                    $m0 = floor((floor($diff/60)%60)/10);
                                    $m1 = (floor($diff/60)%60)%10;
                                    $s0 = floor((floor($diff)%60)/10);
                                    $s1 = (floor($diff)%600)%10;
                                    ?>
                                    <div class="temporary-counter">
                                        <div class="counter-board">
                                            <div class="counter-hours">
                                                <div class="tablo"><?= $h0 ?></div>
                                                <div class="tablo"><?= $h1 ?></div>
                                            </div>
                                            <div class="counter-minutes">
                                                <div class="tablo"><?= $m0 ?></div>
                                                <div class="tablo"><?= $m1 ?></div>
                                            </div>
                                            <div class="counter-seconds">
                                                <div class="tablo"><?= $s0 ?></div>
                                                <div class="tablo"><?= $s1 ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        var till = +(new Date(`<?= $arItem["PROPERTIES"]["COUNTER_LEFT"]["VALUE"] ?>+03:00`));
                                        
                                        setInterval(function()
                                        {
                                            var now = +Date.now();
                                            var diff = Math.round((till - now)/1000);

                                            if (diff < 0)
                                                return;
                                            
                                            var h0 = Math.floor(Math.floor(diff/3600)/10);
                                            var h1 = Math.floor(diff/3600)%10;
                                            var m0 = Math.floor((Math.floor(diff/60)%60)/10);
                                            var m1 = (Math.floor(diff/60)%60)%10;
                                            var s0 = Math.floor((Math.floor(diff)%60)/10);
                                            var s1 = (Math.floor(diff)%600)%10;
                                            
                                            document.querySelector('.counter-hours .tablo:first-child').innerHTML = h0;
                                            document.querySelector('.counter-hours .tablo:last-child').innerHTML = h1;
                                            document.querySelector('.counter-minutes .tablo:first-child').innerHTML = m0;
                                            document.querySelector('.counter-minutes .tablo:last-child').innerHTML = m1;
                                            document.querySelector('.counter-seconds .tablo:first-child').innerHTML = s0;
                                            document.querySelector('.counter-seconds .tablo:last-child').innerHTML = s1;
                                        }, 1000);
                                    </script>
                                    <?php
                                } ?>
                            </div>
                            <?php
                            $cnt++;
                        }
                    } ?>

                </div>
            </div>
        </div>
    </div>
</section>