<style type="text/css">
    .my-item-slider.back-side-slider .temporary-counter {
        width: 80%;
        position: absolute;
        top: 14%;
        left: 10%;
        color: #000;
        z-index: 45;
        font-weight: 600;
        font-family: 'Reforma','Arial',sans-serif;
    }

    .my-item-slider.back-side-slider .temporary-counter .counter-board {
        display: flex;
        justify-content: center;
        position: relative;
    }
    .my-item-slider.back-side-slider .temporary-counter .counter-board:before {
        position: absolute;
        content: 'ОСТАЛОСЬ';
        color: white;
        font-size: 50px;
        top: -90%;
    }
    .my-item-slider.back-side-slider .temporary-counter .counter-board > div {
        display: flex;
        position: relative;
    }
    .my-item-slider.back-side-slider .temporary-counter .counter-board > div:not(:first-child)::before
    {
        display: inline-block;
        max-width: 10px;
        position: absolute;
        left: -15%;
        top: 25%;
        content: '\2022\a\2022';
        font-size: 36px;
        line-height: 20px;
        color: #4A64A9;
    }

    .my-item-slider.back-side-slider .temporary-counter .counter-board > div::after {
        font: bold 14px Verdana, Arial, sans-serif;
        width: 100%;
        text-align: center;
        position: absolute;
        bottom: -20%;
        color: white;
    }
    .temporary-counter .counter-board > div.counter-hours::after { content: 'часов' }
    
    .temporary-counter .counter-board > div.counter-minutes::after { content: 'минут' }
    
    .temporary-counter .counter-board > div.counter-seconds::after { content: 'секунд' }

    .my-item-slider.back-side-slider .temporary-counter .tablo {
        width: 40px;
        height: 80px;
        line-height: 79px;
        background-size: 100% 100%;
        color: #4A64A9;
        font-size: 90px;
        text-align: center;
    }

    .my-item-slider.back-side-slider .temporary-counter .counter-board > div:last-child .tablo {color: #c00}

    .my-item-slider.back-side-slider .temporary-counter .counter-minutes {
        margin-left: 1%;
        margin-right: 1%;
    }

    .margin_top {margin-top: 1%}

    @media(max-width: 1920px) {
        .margin_top {margin-top: 1%}

        .my-item-slider.back-side-slider .temporary-counter { top: 13% }
        .my-item-slider.back-side-slider .temporary-counter .tablo { font-size: 60px }

        .my-item-slider.back-side-slider .temporary-counter .counter-board:before {
            font-size: 30px;
            top: -40%;
        }

        .my-item-slider.back-side-slider .temporary-counter .counter-board > div::after { bottom: -10% }
    }

    @media(max-width: 1440px) {
        .margin_top {margin-top: 2%}

        .my-item-slider.back-side-slider .temporary-counter {top: 14%}
        
        .my-item-slider.back-side-slider .temporary-counter .tablo {
            height: 70px;
            line-height: 70px;
            width: 40px;
            font-size: 55px;
        }
    }

    @media(max-width: 1366px) {
        .margin_top {margin-top: 3%}

        .my-item-slider.back-side-slider .temporary-counter {top: 15.5%}
        
        .my-item-slider.back-side-slider .temporary-counter .tablo {
            width: 35px;
            font-size: 50px;
        }
    }

    @media(max-width: 1280px) {
        .my-item-slider.back-side-slider .temporary-counter {top: 15%}
        
        .my-item-slider.back-side-slider .temporary-counter .tablo { font-size: 50px }
    }

    @media(max-width: 1024px) {
        .margin_top {margin-top: 7%}

        .my-item-slider.back-side-slider .temporary-counter {top: 19%}
        
        .my-item-slider.back-side-slider .temporary-counter .tablo {
            width: 35px;
            font-size: 50px;
        }
    }

    @media(max-width: 960px) {
        .margin_top {margin-top: 10%}

        .my-item-slider.back-side-slider .temporary-counter {top: 26%}
        
        .my-item-slider.back-side-slider .temporary-counter .tablo {
            height: 40px;
            line-height: 40px;
            width: 34px;
            font-size: 40px;
        }

        .my-item-slider.back-side-slider .temporary-counter .counter-board:before { top: -100% }

        .my-item-slider.back-side-slider .temporary-counter .counter-board > div::after { bottom: -40% }

        .my-item-slider.back-side-slider .temporary-counter .counter-board > div:not(:first-child)::before {
            left: -10%;
            top: 20%;
            font-size: 25px;
            line-height: 15px;
    }

    @media(max-width: 854px) {
        .margin_top {margin-top: 10%}

        .my-item-slider.back-side-slider .temporary-counter {top: 27%}
        
        .my-item-slider.back-side-slider .temporary-counter .tablo {
            height: 40px;
            line-height: 40px;
            width: 25px;
            font-size: 35px;
        }

        .my-item-slider.back-side-slider .temporary-counter .counter-board > div::after { font-size: 8px }

        .my-item-slider.back-side-slider .temporary-counter .counter-board > div:not(:first-child)::before {
            font-size: 15px;
            line-height: 10px;
        }

        .my-item-slider.back-side-slider .temporary-counter .counter-board:before {
            font-size: 20px;
            top: -50%;
        }
    }

    @media(max-width: 812px) {
        .margin_top {margin-top: 11%}

        .my-item-slider.back-side-slider .temporary-counter {top: 32%}

        .my-item-slider.back-side-slider .temporary-counter .tablo {
            width: 20px;
            font-size: 30px;
        }

        .my-item-slider.back-side-slider .temporary-counter .counter-board > div:after { bottom: -15% }
    }

    @media(max-width: 800px) {
        .margin_top {margin-top: 8%}

        .my-item-slider.back-side-slider .temporary-counter {top: 18%}
        
        .my-item-slider.back-side-slider .temporary-counter .tablo {
            width: 60px;
            font-size: 80px;
        }

        .my-item-slider.back-side-slider .temporary-counter .counter-board:before {
            font-size: 50px;
            top: -220%;
        }

        .my-item-slider.back-side-slider .temporary-counter .counter-board > div:after {
            bottom: -110%;
            font-size: 20px;
        }
    }

    @media(max-width: 768px) {
        .margin_top {margin-top: 8%}

        .my-item-slider.back-side-slider .temporary-counter {top: 18%}
        
        .my-item-slider.back-side-slider .temporary-counter .tablo {
            width: 65px;
            font-size: 70px;
        }
    }
</style>