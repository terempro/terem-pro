<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="flex-reversed">
                    <?php $cnt = 1; ?>
                    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
                        <?php if ($cnt == 3): ?>
                            </div>
                            <?php $cnt = 1; ?>    
                        <?php endif; ?>       
                        <?php if ($cnt == 1): ?>
                            <div class="row"> 
                        <?php endif; ?>



<div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"></a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                           <?echo $arItem["NAME"]?>
                                        </h4>
                                        <p>
                                           <?echo $arItem["PREVIEW_TEXT"];?>
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"></a>
                                </div>
                            </div>









                        <?php $cnt++; ?>
                    <?php endforeach; ?>  
                </div>
            </div>
        </div>
    </div>
</section>
