<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="content" role="content">
    <div class="container">
        <div class="flex-reversed">
            <div class="row">
                <?php $SECTION = $_GET['SECTION']; ?>
                <?php foreach ($arResult['ITEMS'] as $item): ?>
                <?php if($item["PROPERTIES"]["CATEGORY"]["VALUE_XML_ID"] == $SECTION): ?>
                    <div class="col-xs-12 col-md-6 col-sm-12 shuffle">
                     <?php if ($item["PROPERTIES"]["IMG_2"]["~VALUE"]): ?>
                            <div class="my-promotion desgin-4 + my-margin hover-grey">
                    <?php else: ?>
                            <div class="my-promotion desgin-4 + my-margin hover-grey single-item">
                    <?php endif; ?>
                            <div class="flex-row">
                                <div>
                                    <a href="<?php echo $item["DETAIL_PAGE_URL"]; ?>">
                                        <?php if ($item["PREVIEW_PICTURE"]["SRC"]): ?>
                                            <img src="<?php echo $item["PREVIEW_PICTURE"]["SRC"]; ?>">
                                        <?php else: ?>
                                            <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg" alt=""/>
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            <?php echo $item["NAME"]; ?>
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-toggle="modal" data-whatever="<?php echo $item["NAME"]; ?>"><i class="glyphicon glyphicon-earphone"></i></a>
                                        
                                    </div>
                                    <div class="options">
                                        
                                       <!--  <a href="#" class="compare">Получить бесплатную консультацию</a> -->
                                       <div>
                                        <?php if($item["ID"] == '254907' || $item["ID"] == '254906'):?>
                                             <p>
                                                 <b class="text-red">Новинка!</b>
                                            </p> 
                                             <p>
                                                Двухэтажные дома из бруса<br/> с выпусками.
                                            </p><br/>
                                        <?php endif;?>
                                            <p>
                                                <b>Площадь:</b>
                                            </p>
                                        </div>
                                        <p>
                                            <?php echo $item["PROPERTIES"]["S_1"]["NAME"]; ?> - <?php echo $item["PROPERTIES"]["S_1"]["~VALUE"]; ?>
                                        </p>
                                        <p>
                                            <?php echo $item["PROPERTIES"]["S_2"]["NAME"]; ?> - <?php echo $item["PROPERTIES"]["S_2"]["~VALUE"]; ?>
                                        </p>
                                        <p>
                                            <?php echo $item["PROPERTIES"]["S_3"]["NAME"]; ?> - <?php echo $item["PROPERTIES"]["S_3"]["~VALUE"]; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-row">
                                 <?php if ($item["PROPERTIES"]["IMG_1"]["~VALUE"]): ?>
                                <div>
                                    <img src="<?php echo CFile::GetPath($item["PROPERTIES"]["IMG_1"]["~VALUE"]); ?>">
                                </div>
                                <?php else: ?>
                                   <div> 
                                     <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg" alt=""/>
                                    </div>
                                <?php endif; ?>

                                
                                <?php if ($item["PROPERTIES"]["IMG_2"]["~VALUE"]): ?>
                                    <div>
                                        <img src="<?php echo CFile::GetPath($item["PROPERTIES"]["IMG_2"]["~VALUE"]); ?>">
                                    </div>
                                <?php endif; ?>

                            </div>
                            <a class="all-item-link" href="<?php echo $item["DETAIL_PAGE_URL"]; ?>"></a>
                        </div>
                    </div>
                  <?php endif;?>      
                <?php endforeach; ?>
            </div>  
        </div>
    </div>
</section>