<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php $cnt1 = 1; ?>

<?php

//echo "<pre>";
//var_dump($arResult['HOUSES']);
?>
<?php foreach ($arResult['HOUSES'] as $house): ?>
    <section class="content white catalog-item-<?= $cnt1 ?> content-catalog-item <?php if ($cnt1 == 1): ?>active<?php endif; ?>" role="content" data-item="<?= $cnt1 ?>">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="catalog-item-gallery white + my-margin">
                        <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-left && bg-grey && padding-20 ))">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Фото и планировки</a>
                                </li>
                                <li role="presentation" class="back-to-catalog"><a href="/services/individual_projects/">Вернуться в каталог</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab1">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-9 col-sm-9">
                                            <div data-item="slider-item-catalog">
                                                <div class="owl-item">
                                                    <div class="my-main-img">
                                                        <img src="<?php echo $house["BIG"]["src"]; ?>">
                                                    </div>
                                                </div>
                                                <div class="owl-item">
                                                    <div class="my-main-img">
                                                        <img src="<?php echo $house["PROPERTY_PLAN1_VALUE"]["src"]; ?>">
                                                    </div>
                                                </div>
                                                <div class="owl-item">
                                                    <div class="my-main-img">
                                                        <img src="<?php echo $house["PROPERTY_PLAN2_VALUE"]["src"]; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3 col-sm-3">
                                            <div class="left-side">
                                                <div class="left-side-trigger state_0">
                                                    <div class="my-main-img">
                                                        <a class="fancybox" rel="group" href="<?php echo $house["BIG"]["src"]; ?>"><img src="<?php echo $arResult["PREVIEW_PICTURE"]["SRC"]; ?>">
                                                            <div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div>
                                                        </a>
                                                    </div>
                                                    <div class="my-main-img">
                                                        <a class="fancybox" rel="group" href="<?php echo $house["PROPERTY_PLAN1_VALUE"]["src"]; ?>"><img src="<?php echo $house["PROPERTY_PLAN1_VALUE"]["src"]; ?>">
                                                            <div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div>
                                                        </a>
                                                    </div>
                                                    <div class="my-main-img">
                                                        <a class="fancybox" rel="group" href="<?php echo $house["PROPERTY_PLAN2_VALUE"]["src"]; ?>"><img src="<?php echo $house["PROPERTY_PLAN2_VALUE"]["src"]; ?>">
                                                            <div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div>
                                                        </a>
                                                    </div>
                                                      <div class="my-main-img">
                                                       <a class="fancybox" rel="group" href="<?php echo $house["BIG"]["src"]; ?>"><img src="<?php echo $arResult["PREVIEW_PICTURE"]["SRC"]; ?>">
                                                            <div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div>
                                                       </a>
                                                    </div>
                                                      <div class="my-main-img">
                                                        <a class="fancybox" rel="group" href="<?php echo $house["PROPERTY_PLAN1_VALUE"]["src"]; ?>"><img src="<?php echo $house["PROPERTY_PLAN1_VALUE"]["src"]; ?>">
                                                            <div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-9 col-sm-9">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h3 class="text-uppercase">
                                                         <span><?php echo $arResult["NAME"]; ?></span>
                                                    </h3>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <ul>
                                                        <?php if($house["PROPERTY_S2_H_VALUE"]):?>
                                                        <li>Общая площадь: <strong><?= $house["PROPERTY_S2_H_VALUE"] ?> м.кв.</strong></li>
                                                        <?php endif; ?>
                                                        <?php if($house["PROPERTY_S3_H_VALUE"]):?>
                                                        <li>Жилая площадь: <strong><?= $house["PROPERTY_S3_H_VALUE"] ?> м.кв.</strong></li>
                                                        <?php endif; ?>
                                                    </ul>
                                                </div>
                                                <?php if($house["PROPERTY_S1_H_VALUE"]):?>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <ul>
                                                        <?php if($arResult["PROPERTIES"]["LINER"]["~VALUE"]):?><li>Линейные размеры: <strong><?= $arResult["PROPERTIES"]["LINER"]["~VALUE"] ?> м.кв.</strong></li><?php endif; ?>
                                                        <li>Площадь застройки: <strong><?= $house["PROPERTY_S1_H_VALUE"] ?> м.кв.</strong></li>
                                                    </ul>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $cnt1++; ?>
<?php endforeach; ?>



<section class="content white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="catalog-item-description white + my-margin">
                    <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-2 && appearance-list-left && bg-grey && padding-navs-20 && padding-tabs-30-30 ))">
                        <!-- Nav tabs -->



                        <ul class="nav nav-tabs" role="tablist">
                            <?php $cnt2 = 1; ?>    
                            <?php foreach ($arResult['HOUSES'] as $house): ?>
                                <li role="presentation" <?php if ($cnt2 == 1): ?> class="active" <?php endif; ?>>
                                    <a href="" data-target=".catalog-item-<?= $cnt2 ?>" aria-controls="catalog-item-<?= $cnt2 ?>" role="tab" data-toggle="tab">
                                        <p>
                                           Проект «<?php echo $arResult["NAME"]; ?>»
                                        </p>
                                        <p>
                                           <?= $house ["PROPERTY_TEHNOLOGY_VALUE"] ?> 
                                           
                                        </p>
                                        <p>
                                        от <?php echo number_format($house ["PROPERTY_PRICE2_H_VALUE"], 0, ' ', ' ');?> р.
                                        </p>
                                    </a>
                                </li>
                                <?php $cnt2++; ?>
                            <?php endforeach; ?>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <?php $cnt3 = 1; ?> 
                            <?php foreach ($arResult['HOUSES'] as $house): ?>
                                <div role="tabpanel" class="tab-pane <?php if ($cnt3 == 1): ?>active<?php endif; ?> catalog-item-<?= $cnt3 ?>">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-3 col-sm-3">
                                            <ul>
                                                <li><p>Модель: <strong> <?= $house ["PROPERTY_MODEL_VALUE"] ?></strong></p></li>
                                                <li><p>Технология: <strong> <?= $house ["PROPERTY_TEHNOLOGY_VALUE"] ?></strong></p></li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12 col-md-8 col-sm-8">
                                            <ul>

                                                <li>
                                                    <div class="social">
                                                        <!--<ul>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/Terem_fbook_icon.png"></a></li>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/Terem_insta_icon.png"></a></li>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/Terem_tw_icon.png"></a></li>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/Terem_VK_icon.png"></a></li>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/Terem_LiveJ_icon.png"></a></li>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/map.png"></a></li>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/map.png"></a></li>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/map.png"></a></li>
                                                            <li><a href="#"><img src="/bitrix/templates/.default/assets/img/social/map.png"></a></li>
                                                        </ul>-->
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-9 col-sm-9">
                                            <div class="my-flex catalog-prices">
                                                <?php
                                                    $str_len2 = strlen($house ["PROPERTY_PRICE2_H_VALUE"]);
                                                ?>
                                                <?php if($house ["PROPERTY_PRICE2_H_VALUE"] == $house ["PROPERTY_PRICE1_H_VALUE"]):?>
                                                <div class="active">
                                                    <p>Цена сегодня</p>
                                                    <p><?php echo number_format($house ["PROPERTY_PRICE2_H_VALUE"], 0, ' ', ' ');?> руб.</p>
                                                </div>
                                                <?php else:?>
                                                    <div class="active">
                                                    <p>Цена сегодня</p>
                                                    <p><?php echo number_format($house ["PROPERTY_PRICE2_H_VALUE"], 0, ' ', ' ');?> руб.</p>
                                                </div>
                                                <div>
                                                    <p>Цена </p>
                                                    <p><?php echo number_format($house ["PROPERTY_PRICE1_H_VALUE"], 0, ' ', ' ');?> руб.</p>
                                                </div>
                                                <?php endif;?>    
                                                 
                                               
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3 col-sm-3">
                                            <button style="margin-top: 0;" type="button" href="#" class="btn btn-primary" data-toggle="modal" data-target="#formAdviceItem" data-whatever="<?= $house['NAME'] ?>"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <ul class="help">
                                                <li><p><strong><a style="color: #333 !important;" href="/about/expert-opinion/tekhnologii-stroitelstva/">Не знаете, какую технологию строительства выбрать? Мы поможем разобраться!</a></strong></p></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php $cnt3++; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php $cnt4 = 1; ?>
<?php foreach ($arResult['HOUSES'] as $house): ?>
     <section class="content white catalog-item-<?= $cnt4 ?> content-catalog-tab <?php if ($cnt4 == 1): ?>active<?php endif; ?>" role="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="catalog-item-description-2 white + my-margin">

                        <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-left && bg-grey && padding-navs-20 && padding-tabs-30-30 ))">
                            <!-- Nav tabs -->


                            <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#catalog-item-description<?= $cnt4 ?><?= $house["ID"] ?>" aria-controls="catalog-item-description<?= $cnt4 ?><?= $house["ID"] ?>" role="tab" data-toggle="tab">
                                        Описание 
                                    </a></li>
                                <!--<li role="presentation"><a href="#catalog-item-description<?php echo ($cnt4 + 1); ?><?= $house["ID"] ?>" aria-controls="catalog-item-description<?php echo ($cnt4 + 1); ?><?= $house["ID"] ?>" role="tab" data-toggle="tab">
                                        Описание <?= $cnt4 ?>-->
                                    </a></li>
                                <!--<li role="presentation"><a href="#catalog-item-description<?php echo ($cnt4 + 2); ?><?= $house["ID"] ?>" aria-controls="catalog-item-description<?php echo ($cnt4 + 2); ?><?= $house["ID"] ?>" role="tab" data-toggle="tab">
                                        Отзывы <?= $cnt4 ?>
                                    </a></li>-->
                            </ul>



                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="catalog-item-description<?= $cnt4 ?><?= $house["ID"] ?>">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <p>
                                                <?= $house["PREVIEW_TEXT"] ?>
                                            </p>
                                            
                                            
                                            
                                            <?php if(count($house["PROPERTY_COPLECTATION_VALUE"]) > 1):?>
                                            <div class="more">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#description<?= $cnt4 ?><?= $house["ID"] ?>" aria-expanded="false" aria-controls="description<?= $cnt4 ?><?= $house["ID"] ?>">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-md-9 col-sm-9 text-left">
                                                                    <h4 class="catalog-item-description-2-title">
                                                                        Комплектация 
                                                                    </h4>
                                                                </div>
                                                                <div class="col-xs-12 col-md-3 col-sm-3 text-right">
                                                                    <span class="btn-close">показать</span>
                                                                    <span class="btn-open">скрыть</span>
                                                                </div>
                                                            </div>

                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="collapse my-collapse" id="description<?= $cnt4 ?><?= $house["ID"] ?>">

                                                    <?php foreach($house["PROPERTY_COPLECTATION_VALUE"] as $k => $v):?>


<?php if($v != "."):?>
<div class="row">
                                                        <div class="col-xs-12 col-md-3 col-sm-3">
                                                            <p class="catalog-item-description-2-subtitle"><?=$house["PROPERTY_COPLECTATION_DESCRIPTION"][$k]?></p>
                                                        </div>
                                                        <div class="col-xs-12 col-md-9 col-sm-9">
                                                            <p><?=$v?></p>
                                                        </div>
                                                    </div>
<?php else:?>
<div class="row">
<div class="col-xs-12 col-md-12 col-sm-12">
<p style="text-align: center;" class="catalog-item-description-2-subtitle"><?=$house["PROPERTY_COPLECTATION_DESCRIPTION"][$k]?></p>
</div>
</div>
<?php endif; ?>

                                                    <?php endforeach;?>
                                                    
                                                
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                            
                                            
                                            
                                            
                                            <div class="additions">
                                                <p>
                                                     <?= $house["DETAIL_TEXT"] ?>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                                <!--<div role="tabpanel" class="tab-pane" id="catalog-item-description<?php echo ($cnt4 + 1); ?><?= $house["ID"] ?>">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <hr />
                                            <p>
                                              
                                            </p>
                                        </div>
                                    </div>
                                </div>-->


                                <!--<div role="tabpanel" class="tab-pane" id="catalog-item-description<?php echo ($cnt4 + 2); ?><?= $house["ID"] ?>">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <p>Мы публикуем избранные реальные отзывы владельцов именно этой модели. Почитать все отзывы или написать вопрос вы можете в <a href="#">этом разделе</a></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="item-review">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-9 col-sm-9">
                                                        <div class="inner">
                                                            <p>
                                                                Хочется сказать, что Теремъ хорошая компания и строиться не только можно но и нужно именно здесь.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="date">
                                                            <p><strong>Королева Екатерина</strong></p>
                                                            <p>Москва</p>
                                                            <p>21 Мая 2015</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="item-review">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-9 col-sm-9">
                                                        <div class="inner">
                                                            <p>
                                                                Хочется сказать, что Теремъ хорошая компания и строиться не только можно но и нужно именно здесь.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="date">
                                                            <p><strong>Королева Екатерина</strong></p>
                                                            <p>Москва</p>
                                                            <p>21 Мая 2015</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="item-review">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-9 col-sm-9">
                                                        <div class="inner">
                                                            <p>
                                                                Хочется сказать, что Теремъ хорошая компания и строиться не только можно но и нужно именно здесь.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="date">
                                                            <p><strong>Королева Екатерина</strong></p>
                                                            <p>Москва</p>
                                                            <p>21 Мая 2015</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $cnt4++; ?>
<?php endforeach; ?>

