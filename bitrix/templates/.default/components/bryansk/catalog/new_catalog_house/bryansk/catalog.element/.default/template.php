<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$detect = new Mobile_Detect;
$tech = isset($_GET['tech']) ? resetString($_GET['tech']) : "";

if (strpos($_SERVER['HTTP_REFERER'], 'https://'.$_SERVER["HTTP_HOST"].'/bryansk/catalog/') === 0)
{
    $back_url = $_SERVER['HTTP_REFERER'];
}
else
{
    $back_url = '/bryansk/catalog/';
}

?>
<section class="content content-back-side" data-parallax="up" data-opacity="true">
  <div class="container-fluid reset-padding">
    <div class="row reset-margin">
      <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
        <div class="back-img"
        style="background-image: url('/bitrix/templates/.default/assets/img/bg.jpg');"></div>
      </div>
    </div>
  </div>
</section>
<section class="content white">
  <?php
  //echo "<pre>";
  //var_dump($arResult['START_VARIANT'][0]['PROPERTY_SQUARE_ALL_HOUSE_VALUE']);
  //$arResult['KARKAS_VARIANT']['DETAIL_PUCTURE']
  //$arResult['KARKAS_VARIANT']['PROPERTY_PLANS_HOUSE_1_VALUE']
  //$arResult['KARKAS_VARIANT']['PROPERTY_PLANS_HOUSE_1_VALUE']
  //$arResult['START_VARIANT'][0]["PROPERTY_PRICE_1_VALUE"]);
  //var_dump($arResult['START_VARIANT']);
  $profit = ($arResult['START_VARIANT'][0]['PRICES']["PROPERTY_PRICE_2_VALUE"]-$arResult['START_VARIANT'][0]['PRICES']["PROPERTY_PRICE_1_VALUE"]);
  //echo "Текущая " .(int)$arResult['START_VARIANT'][0]['PRICES']["PROPERTY_PRICE_1_VALUE"]."<br/>";
  //echo "Большая ".(int)$arResult['START_VARIANT'][0]['PRICES']["PROPERTY_PRICE_2_VALUE"]."<br/>";
  //echo "Вариант 2 ".$profit."<br/>";
  ?>
  <section class="house-card">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="catalog-item-gallery white-holder my-margin row-eq-height row-eq-height--rwd1200">
            <div class="col-lg-9 col-md-12 col-xs-12 col--no-gutter">
              <div class="section-wrapper">


                <section class="house-slider-tab slider-previews-ajx active" id="house-slider-tab" >

                  <ul class="slider-wrapper" data-item="slider-item-catalog" data-type="house-slider">

                    <?php if($arResult['START_VARIANT'][0]['DETAIL_PICTURE']):?>
                      <li class="owl-item" id="photo-slide">
                        <img src="<?=CFile::GetPath($arResult['START_VARIANT'][0]['DETAIL_PICTURE'])?>">
                        <?php if($arResult['START_VARIANT'][0]["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                        <div class="caption">
                            <?php if($arResult['START_VARIANT'][0]['DETAIL_PICTURE']["DESCRIPTION"]): ?><?=$arResult['START_VARIANT'][0]['DETAIL_PICTURE']["DESCRIPTION"]?><?php endif; ?>
                           <!--  Варианты пристроек к этому дому в разделе <a href="/bryansk/additional-service/<?=$arResult['START_VARIANT'][0]["CODE"]?>/">калькулятор доп.услуг</a> -->
                        </div>
                        <?php endif; ?>
                      </li>
                    <?php else:?>
                      <li class="owl-item" id="photo-slide">
                        <img src="http://placehold.it/838x556">
                      </li>
                    <?php endif; ?>

                    <?php if(!$detect->isMobile()):?>
                      <?php if($arResult['START_VARIANT'][0]['PROPERTY_D_RACURS_VALUE']):?>
                        <li class="owl-item" id="3D-slide">
                          <div id="i3d" class="i3d-gallary">
                            <?=htmlspecialchars_decode($arResult['START_VARIANT'][0]['PROPERTY_D_RACURS_VALUE']['TEXT'])?>
                          </div>
                        </li>
                      <?php endif; ?>
                    <?php endif; ?>

                    <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_1_VALUE']):?>
                      <li class="owl-item" id="layout_1-slide">
                        <img src="<?=CFile::GetPath($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_1_VALUE'])?>">
                        <?php if($arResult['START_VARIANT'][0]["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                        <div class="caption">
                            <?php if($arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_1_DESCRIPTION"]): ?><?=$arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_1_DESCRIPTION"]?><?php endif; ?>
                            <!-- Варианты пристроек к этому дому в разделе <a href="/bryansk/additional-service/<?=$arResult['START_VARIANT'][0]["CODE"]?>/">калькулятор доп.услуг</a> -->
                        </div>
                        <?php endif; ?>
                      </li>
                    <?php endif; ?>

                    <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_2_VALUE']):?>
                      <li class="owl-item" id="layout_2-slide">
                        <img src="<?=CFile::GetPath($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_2_VALUE'])?>">
                        <?php if($arResult['START_VARIANT'][0]["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                        <div class="caption">
                            <?php if($arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_2_DESCRIPTION"]): ?><?=$arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_2_DESCRIPTION"]?><?php endif; ?>
                            <!-- Варианты пристроек к этому дому в разделе <a href="/bryansk/additional-service/<?=$arResult['START_VARIANT'][0]["CODE"]?>/">калькулятор доп.услуг</a> -->
                        </div>
                        <?php endif; ?>
                      </li>
                    <?php endif; ?>


                    <?php if(!$detect->isMobile()):?>
                      <?php foreach($arResult["START_VARIANT"][0]["PROPERTY_EXTERIER_VALUE"] as $item):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($item, Array("width" => 838, "height" => 556));
                        ?>
                        <li class="owl-item">
                          <img src="<?= $renderImage['src']?>">
                        </li>
                      <?php endforeach; ?>
                      <?php foreach($arResult["START_VARIANT"][0]["PROPERTY_NITERIER_VALUE"] as $item):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($item, Array("width" => 838, "height" => 556));
                        ?>
                        <li class="owl-item">
                          <img src="<?= $renderImage['src']?>">
                        </li>
                      <?php endforeach; ?>
                    <?php endif;?>

                  </ul>
                  <div class="slider-previews">
                    <ul class="slider-previews__list" data-item="slider-catalog-side" data-type="house-slider-side">

                      <?php if($arResult['START_VARIANT'][0]['DETAIL_PICTURE']):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($arResult['START_VARIANT'][0]['DETAIL_PICTURE'], Array("width" => 120, "height" => 80));
                        ?>
                        <li data-link="photo-slide">
                          <img src="<?=$renderImage['src']?>">
                        </li>
                      <?php else:?>
                        <li data-link="photo-slide">
                          <img src="http://placehold.it/120x80">
                        </li>
                      <?php endif; ?>

                      <?php if(!$detect->isMobile()):?>
                        <?php if($arResult['START_VARIANT'][0]['PROPERTY_D_RACURS_VALUE']):?>
                          <li data-link="3D-slide" class="threesixty-slide">
                            <img src="/bitrix/templates/.default/assets/img/catalog-item/360.svg">
                          </li>
                        <?php endif; ?>
                      <?php endif;?>

                      <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_1_VALUE']):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_1_VALUE'], Array("width" => 120, "height" => 80));
                        ?>
                        <li data-link="layout_1-slide" class="layout_1-holder" >
                          <img src="<?=$renderImage['src']?>">
                        </li>
                      <?php endif; ?>


                      <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_2_VALUE']):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_2_VALUE'], Array("width" => 120, "height" => 80));
                        ?>
                        <li data-link="layout_2-slide" class="layout_2-holder" >
                          <img src="<?=$renderImage['src']?>">
                        </li>
                      <?php endif; ?>



                      <?php if(!$detect->isMobile()):?>
                        <?php foreach($arResult["START_VARIANT"][0]["PROPERTY_EXTERIER_VALUE"] as $item):?>
                          <?php
                          $renderImage = CFile::ResizeImageGet($item, Array("width" => 120, "height" => 80));
                          ?>
                          <li class="owl-item">
                            <img src="<?= $renderImage['src']?>">
                          </li>
                        <?php endforeach; ?>
                        <?php foreach($arResult["START_VARIANT"][0]["PROPERTY_NITERIER_VALUE"] as $item):?>
                          <?php
                          $renderImage = CFile::ResizeImageGet($item, Array("width" => 120, "height" => 80));
                          ?>
                          <li class="owl-item">
                            <img src="<?= $renderImage['src']?>">
                          </li>
                        <?php endforeach; ?>
                      <?php endif;?>

                    </ul>
                  </div>
                </section>



                <section class="package-tab" id="package-tab">
                  <?php if($arResult['KARKAS_ID_HOUSE']):?>

                    <div class="package-tab__inner__bottom-b">
                      <div class="bottom-b__right">
                        <a href="" onclick="houseCardPrint('#photo-slide', '.package-tab__inner.active', '#layout_1-slide', '#layout_2-slide'); return false;" class="print-link">Распечатать<span class="glyphicon glyphicon-print"></span></a>
                      </div>
                    </div>

                    <div class="package-tab__inner <?php if(!$tech): ?> active <?php endif; ?>" data-item="package-tab-karkas" id="karkas-complect">

                    </div>
                  <?php endif; ?>
                  <?php if($arResult['BRUS_ID_HOUSE']):?>
                    <div class="package-tab__inner <?php if($tech == 'brus'):?>active <?php endif; ?>" data-item="package-tab-brus" id="brus-complect">

                    </div>
                  <?php endif; ?>
                  <?php if($arResult['KIRPICH_ID_HOUSE']):?>
                    <div class="package-tab__inner <?php if($tech == 'kirpich'):?>active <?php endif; ?>" data-item="package-tab-kirpich" id="kirpich-complect">

                    </div>
                  <?php endif;?>

                  <div class="variant__modal" data-id="modal-package" id="formAdvice">
                    <div class="variant__modal__container">
                      <a href="#close" class="close-cross"></a>

                      <h5 class="variant__parameters__title">Пакет инженерных коммуникаций для этого дома</h5>

                      <div class="variant__parameters__list" id="variant-modal-ajax">

                      </div>
                      <a href="/services/enginering_communications/">Посмотреть подробнее в разделе «Инженерные коммуникаций»</a>
                      <form action="http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/" method="post" data-form="send">
                        <input type="text" name="name" required="required" placeholder="имя">
                        <input type="tel" name="tel" required="required" placeholder="телефон" data-item="phone">
                        <button type="submit" class="cta-btn">
                          Получить цены
                        </button>
                      </form>
                    </div>
                  </div>
                </section>


                  
                 <?php if($arResult["START_VARIANT"][0]["PROPERTY_VIDEO_EXTRA_VALUE"]):?>
                  
                  <section class="video-tab" id="video-tab">
                    <div class="video-wrapper">
                      <div class="video-slider" data-item="slider-item-catalog">
                        <?php 
                        foreach ($arResult["START_VARIANT"][0]["PROPERTY_VIDEO_EXTRA_VALUE"] as $video_path)
                            echo html_entity_decode($video_path); 
                        ?>
                      </div>
                        
                     <?php if (count($arResult["START_VARIANT"][0]["PROPERTY_VIDEO_EXTRA_VALUE"]) > 1): ?>  
                      <div class="video-previews">
                          <ul class="slider-previews__list" data-item="slider-catalog-side">
                                <?php
                                
                                for ($i = 0; $i < count($arResult["START_VARIANT"][0]["PROPERTY_VIDEO_EXTRA_VALUE"]); ++$i)
                                {
                                    if ($arResult['START_VARIANT'][0]['PROPERTY_VIDEO_PREVIEW_VALUE'][$i])
                                    {
                                        $renderImage = CFile::ResizeImageGet($arResult['START_VARIANT'][0]['PROPERTY_VIDEO_PREVIEW_VALUE'][$i], Array("width" => 120, "height" => 80));
                                    }
                                    else
                                    {
                                        $renderImage['src'] = '/upload/video_preview.jpg';
                                    }
                                        
                                    echo '<li class="owl-item">'.
                                         '<img src="'.$renderImage['src'].'">'.
                                         '</li>';
                                }
                                ?>
                            </ul>
                      </div>
                        <?php endif; ?>
                      <div class="video-caption">

                        <div class="video-caption__left">
                          <span class="video-title">
                            <?=$arResult['NAME']?>
                          </span>
                          <span class="view-count"><?=$arResult["SHOW_COUNTER"]?> просмотров</span>
                        </div>

                      </div>
                    </div>
                    <div class="video-description">
                      <div class="description-text">
                        <img src="/bitrix/templates/.default/assets/img/terem-from-youtube.jpg" class="youtube-terem-icon">
                        <p>
                          Присоединяйтесь к числу подписчиков канала «Теремъ», чтобы не пропустить ничего интересного.
                        </p>
                      </div>
                      <noindex>
                        <button class="g-ytsubscribe" data-channelid="UC1jxLRcdCGn9DstlU9UUSRA" data-layout="default" data-count="default" data-onytevent="onYtEvent">
                        </button>
                      </noindex>
                    </div>
                  </section>
                 <?php endif; ?>   
                <section class="feedback-tab" id="feedback-tab">
                  <div id="mc-container"></div>
                  <script type="text/javascript">

                  cackle_widget = window.cackle_widget || [];
                  cackle_widget.push({
                    widget: 'Comment',
                    id: 51841,
                    msg: {
                      placeholder: 'Оставьте свой отзыв…'
                    }
                  });
                  (function() {
                    var mc = document.createElement('script');
                    mc.type = 'text/javascript';
                    mc.async = true;
                    mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
                  })();
                  </script>
                </section>
                <section class="house-slider-tab slider-previews-ajx2" id="house-slider-tab2" >
                  <ul class="slider-wrapper" data-item="slider-item-catalog" data-type="photo-slider">
                    <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_3_VALUE']):?>
                      <li class="owl-item" id="layout_3-slide">
                        <img src="<?=CFile::GetPath($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_3_VALUE'])?>">
                        <?php if($arResult['START_VARIANT'][0]["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                        <div class="caption">
                            <?php if($arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_3_DESCRIPTION"]): ?><?=$arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_3_DESCRIPTION"]?><?php endif; ?>
                            <!-- Варианты пристроек к этому дому в разделе <a href="/bryansk/additional-service/<?=$arResult['START_VARIANT'][0]["CODE"]?>/">калькулятор доп.услуг</a> -->
                        </div>
                        <?php endif; ?>
                      </li>
                    <?php endif; ?>

                    <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_4_VALUE']):?>
                      <li class="owl-item" id="layout_4-slide">
                        <img src="<?=CFile::GetPath($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_4_VALUE'])?>">
                        <?php if($arResult['START_VARIANT'][0]["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                        <div class="caption">
                            <?php if($arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_4_DESCRIPTION"]): ?><?=$arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_4_DESCRIPTION"]?><?php endif; ?>
                            <!-- Варианты пристроек к этому дому в разделе <a href="/bryansk/additional-service/<?=$arResult['START_VARIANT'][0]["CODE"]?>/">калькулятор доп.услуг</a> -->
                        </div>
                        <?php endif; ?>
                      </li>
                    <?php endif; ?>

                    <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_5_VALUE']):?>
                      <li class="owl-item" id="layout_5-slide">
                        <img src="<?=CFile::GetPath($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_5_VALUE'])?>">
                        <?php if($arResult['START_VARIANT'][0]["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                        <div class="caption">
                            <?php if($arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_5_DESCRIPTION"]): ?><?=$arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_5_DESCRIPTION"]?><?php endif; ?>
                            <!-- Варианты пристроек к этому дому в разделе <a href="/bryansk/additional-service/<?=$arResult['START_VARIANT'][0]["CODE"]?>/">калькулятор доп.услуг</a> -->
                        </div>
                        <?php endif; ?>
                      </li>
                    <?php endif; ?>

                    <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_6_VALUE']):?>
                      <li class="owl-item" id="layout_6-slide">
                        <img src="<?=CFile::GetPath($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_6_VALUE'])?>">
                        <?php if($arResult['START_VARIANT'][0]["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                        <div class="caption">
                            <?php if($arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_6_DESCRIPTION"]): ?><?=$arResult['START_VARIANT'][0]["PROPERTY_PLANS_HOUSE_6_DESCRIPTION"]?><?php endif; ?>
                            <!-- Варианты пристроек к этому дому в разделе <a href="/bryansk/additional-service/<?=$arResult['START_VARIANT'][0]["CODE"]?>/">калькулятор доп.услуг</a> -->
                        </div>
                        <?php endif; ?>
                      </li>
                    <?php endif; ?>
                  </ul>
                  <div class="slider-previews">
                    <ul class="slider-previews__list" data-item="slider-catalog-side" data-type="photo-slider-side">
                      <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_3_VALUE']):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_3_VALUE'], Array("width" => 120, "height" => 80));
                        ?>
                        <li data-link="layout_1-slide" class="layout_1-holder" >
                          <img src="<?=$renderImage['src']?>">
                        </li>
                      <?php endif; ?>

                      <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_4_VALUE']):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_4_VALUE'], Array("width" => 120, "height" => 80));
                        ?>
                        <li data-link="layout_2-slide" class="layout_2-holder" >
                          <img src="<?=$renderImage['src']?>">
                        </li>
                      <?php endif; ?>

                      <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_5_VALUE']):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_5_VALUE'], Array("width" => 120, "height" => 80));
                        ?>
                        <li data-link="layout_3-slide" class="layout_1-holder" >
                          <img src="<?=$renderImage['src']?>">
                        </li>
                      <?php endif; ?>

                      <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_6_VALUE']):?>
                        <?php
                        $renderImage = CFile::ResizeImageGet($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_6_VALUE'], Array("width" => 120, "height" => 80));
                        ?>
                        <li data-link="layout_4-slide" class="layout_2-holder" >
                          <img src="<?=$renderImage['src']?>">
                        </li>
                      <?php endif; ?>
                    </ul>
                  </div>
                </section>
              </div>
            </div>
            <div class="col-lg-3 col-md-12 col-xs-12 col--no-gutter">
              <div class="control-panel">
                <div class="control-panel__fixed-top">
                  <div class="house-name">
                    <h2 class="house-title">
                      <?=$arResult['NAME']?>
                    </h2>
                    <span class="house-area">
                      <?=$arResult['PROPERTIES']['SIZER']['VALUE']?> м; от <?=$arResult['START_VARIANT'][0]['PROPERTY_SQUARE_ALL_HOUSE_VALUE']?> кв.м
                    </span>
                  </div>
                  <span class="my-hr"></span>
                  <span class="choose-tech__span">
                        Выберите технологию:
                    </span>
                    <div class="tech-type">
                    <ul>
                      <?php if($arResult['KARKAS_ID_HOUSE']):?>
                        <li class="tech-type__elem <?php if(!$tech): ?> active <?php endif; ?>">
                          <a href="#tab-karkas" data-toggle="tab" data-link="package-tab-karkas" data-type="karkas" data-house="<?=$arResult['KARKAS_ID_HOUSE']?>">
                              каркас
                          </a>
                        </li>
                      <?php endif;?>
                      <?php if($arResult['BRUS_ID_HOUSE']):?>
                        <li class="tech-type__elem <?php if($tech == 'brus'):?> active<?php endif; ?>">
                          <a href="#tab-brus" data-toggle="tab" data-link="package-tab-brus" data-type="brus" data-house="<?=$arResult['BRUS_ID_HOUSE']?>">
                            брус
                          </a>
                        </li>
                      <?php endif;?>
                      <?php if($arResult['KIRPICH_ID_HOUSE']):?>
                        <li class="tech-type__elem <?php if($tech == 'kirpich'):?>active <?php endif; ?>">
                          <a href="#tab-kirpich" data-toggle="tab" data-link="package-tab-kirpich" data-type="kirpich" data-house="<?=$arResult['KIRPICH_ID_HOUSE']?>">
                            кирпич
                          </a>
                        </li>
                      <?php endif;?>
                    </ul>
                    </div>
                    <div class="tech-type__price tab-content clearfix">
                    <?php if($arResult['KARKAS_ID_HOUSE']):?>
                      <div class="tab-pane my-tab-pane <?php if(!$tech): ?> active <?php endif; ?>" id="tab-karkas">
                        <div class="loader"></div>

                      </div>
                    <?php endif;?>
                    <?php if($arResult['BRUS_ID_HOUSE']):?>
                      <div class="tab-pane my-tab-pane <?php if($tech == 'brus'): ?>active<?php endif; ?>" id="tab-brus">
                        <div class="loader"></div>

                      </div>
                    <?php endif;?>
                    <?php if($arResult['KIRPICH_ID_HOUSE']):?>


                      <?php if(!$arResult['BRUS_ID_HOUSE'] && !$arResult['KARKAS_ID_HOUSE']):?>
                        <div class="tab-pane my-tab-pane active" id="tab-kirpich">
                          <div class="loader"></div>

                        </div>
                      <?php else:?>
                        <div class="tab-pane my-tab-pane <?php if($tech == 'kirpich'): ?>active<?php endif; ?>" id="tab-kirpich">
                          <div class="loader"></div>

                        </div>
                      <?php endif;?>


                    <?php endif; ?>
                    </div>

                  
                  <div class="rating"  >

                    <div class="cackle-comment-count" data-cackle-url="http://www.terem-pro.ru<?=$arResult['DETAIL_PAGE_URL']?>"></div>
                    <script type="text/javascript">
                    cackle_widget = window.cackle_widget || [];
                    cackle_widget.push({widget: 'CommentCount', id: 51841, html: '<span class="rating__span">Рейтинг:</span>&nbsp;<span class="rating-stars-b">{{=it.stars}}</span>'});
                    (function() {
                      var mc = document.createElement('script');
                      mc.type = 'text/javascript';
                      mc.async = true;
                      mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
                    })();
                    </script>
                  </div>
                </div>
                <div class="control-panel__fixed-middle">
                  <div class="nav-b">
                    <ul>
                      <li class="active"><a class=" nav-b__link" data-href="house-slider-tab" href="">
                        Фото и планировки
                      </a></li>
                      <?php if($arResult['START_VARIANT'][0]['PROPERTY_PLANS_HOUSE_3_VALUE']):?>
                        <li><a class="nav-b__link" data-href="house-slider-tab2" href="">
                          Варианты планировок
                        </a></li>
                      <?php endif; ?>
                      <li>
                        <a class="nav-b__link complectation" data-href="package-tab" href="" data-house="<?=$arResult['ID']?>" data-karkas="<?=$arResult['KARKAS_ID_HOUSE']?>" data-kirpich="<?=$arResult['KIRPICH_ID_HOUSE']?>" data-brus="<?=$arResult['BRUS_ID_HOUSE']?>" >
                          Комплектация
                        </a></li>

                        <?php if($arResult["START_VARIANT"][0]["PROPERTY_VIDEO_EXTRA_VALUE"]):?>
                          <li><a class="nav-b__link" data-href="video-tab" href="">
                            Видео
                          </a></li>
                        <?php endif; ?>
                        <li><a class="nav-b__link" data-href="feedback-tab" href="">
                          Отзывы
                        </a></li>





                      </ul>
                    </div>
                  </div>
                  <div class="control-panel__fixed-bottom">
                    <div class="cta-b">
                      <button data-yagoal="CONSULT_HOUSE_CARD_CALLED" type="button" class="cta-btn" data-toggle="modal" data-target="#formAdviceItem" data-code="<?=$arResult["START_VARIANT"][0]["PROPERTY_CODE_1C_VALUE"]?>" data-price="" data-house="<?=$arResult['NAME']?>">
                        Заказать консультацию
                      </button>
                    </div>
                    <div class="info-bottom">
                      <div class="chosen-house">
                        <span>
                          Вы смотрите: <span><?=$arResult['NAME']?></span> <span id="chosen-house-tech">каркас</span>
                        </span>
                      </div>
                      <div class="back-to-catalog">
                        <a href="<?= $back_url ?>">
                          Вернуться в каталог
                        </a>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- 			<div class="row welcome-text">
        <div class="col-xs-12">
        <p class="text-danger"><i>Строительная компания «Теремъ» приветствует вас!</i></p>
      </div>
    </div>
    <div class="row">
    <div class="col-xs-12 col-md-3 col-sm-3">
    <a href="/price/" class="my-block-link my-block-link--price my-margin">
    <h3 class="catalog-titile">
    Прайс-лист
  </h3>
</a>
</div>
<div class="col-xs-12 col-md-3 col-sm-3">
<a href="/jurnal/" class="my-block-link my-block-link--journal my-margin">
<h3 class="catalog-titile">
Журнал «Терем»
</h3>
</a>
</div>
<div class="col-xs-12 col-md-3 col-sm-3">
<a href="http://zemli.terem-pro.ru/" class="my-block-link my-block-link--zemli my-margin">
<h3 class="catalog-titile">
Продажа <span>земли</span>
<br>
в поселках
</h3>
</a>
</div>
<div class="col-xs-12 col-md-3 col-sm-3">
<a href="" class="my-block-link my-block-link--contact my-margin" data-toggle="modal" data-target="#formAdviceItem" data-code="00000124051" data-price="1080000" data-house="Канцлер 4">
<h3 class="catalog-titile">
Отдел продаж
</h3>
</a>
</div>
</div> -->

</div>
</section>
</section>
