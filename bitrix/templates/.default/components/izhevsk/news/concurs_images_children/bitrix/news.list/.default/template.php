<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white  + my-margin clearfix my-padding">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title desgin-h1">
                                <h1>
                                    Вся правда о «Тереме»
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>Описание конкурса и т.п. Серия «Витязь» - это одноэтажные, с необычной планировкой дома, где есть всё необходимо для комфортной загородной жизни: гостиная, совмещённая с кухней, 2 комнаты, удобный холл и санузел. Приятный бонус - второй свет, который расширяет возможности внутреннего оформления и декорирования. Эти дачные домики подойдут тем, кто предпочитает экономию на всех этапах от строительства, до эксплуатации, вплоть до ухода и уборки помещений. А также для тех, кто только начал осваивать....</p>
                        </div>
                    </div>
                    <div class="row offset-bottom">
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="expert-pagination Gallary-pagination-list">
                                <p class="inline">Сортировать:</p>
                                <div class="btn-group border" role="group">

                                    <a  href="/councurs/?SORT=LIKE" class="<?php if ($_GET['SORT'] == "LIKE"): ?> active <?php endif; ?> btn btn-default">по количеству лайков</a>

                                    <a href="/councurs/?SORT=AGE" class="<?php if ($_GET['SORT'] == "AGE"): ?> active <?php endif; ?> btn btn-default ">по возрасту участника</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6 text-right">
                            <div class="expert-pagination Gallary-pagination-list">
                                <p class="inline">Поиск по имени:</p>
                                <div class="btn-group border" role="group">
                                    <form action="/councurs/">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="q" placeholder="Поиск">
                                            <span class="input-group-addon"><button type="submit" href="#" class="btn btn-submit ">найти</button></span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- 1000x630 -->
<!-- 278x278 -->
<section class="content" role="content">
    <div class="container">

        <?php $count = 0; ?>
        <?php foreach ($arResult['ITEMS'] as $arItem): ?>
            <?php if ($count == 0): ?>
                <div class="row">
                <?php endif; ?> 

                <div class="col-xs-12 col-md-3 col-sm-3">
                    <div class="Gallary-preview">
                        <?php
                        //PREV
                        $previewImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 278, "height" => 278), BX_RESIZE_IMAGE_EXACT);
                        //DETAIL

                        $img = thumb2($_SERVER['DOCUMENT_ROOT'] . $arItem["PREVIEW_PICTURE"]['SRC'], 1000, 630, true);
                        $SRC_IMG = str_replace('/var/www/zolotarev/data/www/terem-pro.ru', '', $img);

                        ?>
                        <a href="#item<?= $arItem['ID'] ?>" class="fancybox-g" rel="group1">
                            <img src="<?= $previewImage["src"] ?>" class="full-width" alt="">
                        </a>
                        <div class="Gallary-inner" id="item<?= $arItem['ID'] ?>" style="display:none;">
                            <div>
                                <img src="<?= $SRC_IMG ?>" class="full-width" alt="">
                            </div>
                            <div class="Gallary-inner-description">
                                <div>
                                    <p><strong><?= $arItem["PROPERTIES"]["P_FIO"]["VALUE"] ?>, <?= $arItem["PROPERTIES"]["P_AGE"]["VALUE"] ?></strong></p>
                                    <p>Детский дом «<?= $arItem["PROPERTIES"]["P_HOUSE"]["VALUE"] ?>»</p>
                                </div>
                                <div>
                                    <div><a href="javascript: Like('<?= $arItem['ID'] ?>');"><span><i class="glyphicon glyphicon-heart"></i></span><span>Нравится</span></a></div>
                                    <div><div class="liked"><span><img src="/bitrix/templates/.default/assets/css/liked.png" alt=""></span><span id='<?= $arItem['ID'] ?>'><?= $arItem["PROPERTIES"]["P_LIKE"]["VALUE"] ?></span></div></div>
                                </div>
                            </div>
                        </div>
                        <div class="Gallary-preview-description">
                            <p><i class="glyphicon glyphicon-heart"></i><span id='like_<?= $arItem['ID'] ?>'><?= $arItem["PROPERTIES"]["P_LIKE"]["VALUE"] ?></span></p>
                            <p><?= $arItem["PROPERTIES"]["P_FIO"]["VALUE"] ?></p>
                            <p><?= $arItem["PROPERTIES"]["P_AGE"]["VALUE"] ?> лет</p>
                        </div>
                    </div>
                </div>

                <?php $count++; ?>
                <?php if ($count == 3): ?>        
                </div>
                <?php $count = 0; ?>
            <?php endif; ?>
        <?php endforeach; ?>

    </div>
</section>