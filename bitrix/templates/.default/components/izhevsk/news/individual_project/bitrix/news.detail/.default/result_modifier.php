<?php
foreach ($arResult["PROPERTIES"]["HOUSES"]["VALUE"] as $k => $item) {
    

    $arSelect = Array(
        "ID", 
        "NAME",
        "PROPERTY_NAME_H",
        "PROPERTY_S1_H",
        "PROPERTY_S2_H",
        "PROPERTY_S3_H",
        "PROPERTY_PRICE1_H",
        "PROPERTY_PRICE2_H",
        "PROPERTY_IMG_1",
        "PROPERTY_PLAN1",
        "PROPERTY_PLAN2",
        "PROPERTY_MODEL",
        "PROPERTY_TEHNOLOGY",
        "PREVIEW_TEXT",
        "DETAIL_TEXT",
        "PROPERTY_COPLECTATION",
        "PROPERTY_LINER"
        );
    $arFilter = Array("IBLOCK_ID" => 28, "ID" => $item, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
    
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arResult['HOUSES'][] = $ob->GetFields();
    }
}


foreach($arResult['HOUSES'] as $k => $item){
    $arResult['HOUSES'][$k]["PROPERTY_PLAN1_VALUE"] = CFile::ResizeImageGet($item["PROPERTY_PLAN1_VALUE"], array('width' => 838, 'height' => 546), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
    $arResult['HOUSES'][$k]["PROPERTY_PLAN2_VALUE"] = CFile::ResizeImageGet($item["PROPERTY_PLAN2_VALUE"], array('width' => 838, 'height' => 546), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
    $arResult['HOUSES'][$k]['BIG'] = CFile::ResizeImageGet($item["PROPERTY_IMG_1_VALUE"], array('width' => 838, 'height' => 546), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
    $arResult['HOUSES'][$k]['SMALL'] = CFile::ResizeImageGet($item["PROPERTY_IMG_1_VALUE"], array('width' => 275, 'height' => 275), BX_RESIZE_IMAGE_EXACT, true);
}

//echo "<pre>";
//var_dump($arResult);




