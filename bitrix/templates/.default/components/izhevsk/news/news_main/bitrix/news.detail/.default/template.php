<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="content text-page white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    <?= $arResult["NAME"] ?>
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <?php echo $arResult["DETAIL_TEXT"]; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="social-news">
                            <div><p style="margin-right: 5px;font-size:16px;"><strong>поделиться</strong></p>
                                </div>
                                <div>
                                   <script type="text/javascript">(function (w, doc) {
                                                            if (!w.__utlWdgt) {
                                                                w.__utlWdgt = true;
                                                                var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                                                s.type = 'text/javascript';
                                                                s.charset = 'UTF-8';
                                                                s.async = true;
                                                                s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                                                var h = d[g]('body')[0];
                                                                h.appendChild(s);
                                                            }
                                                        })(window, document);
                                                    </script>
                                                    <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="round-rectangle" data-sn-ids="vk.fb.tw.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1463423" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6 text-right">
                            <p><strong>Перейти к остальным</strong> <a href="/about/news/" style="color: #337ab7;">Новостям</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<?php if($arResult['ID'] == '271058'): ?>
    <section class="content" role="">
        <div class="container">
            <div class="flex-reversed">

                <div class="row" id="items">

                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin-1./">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/8a1/8a1622e5b8b548835559d2ab21b7a24d.jpg?148759484890459" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Боярин 1<br>
                                            8.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>10.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1025000" data-whatever="Боярин 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 025  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 150: 
                                                <strong>от 

                                                    1 470  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin-1./"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin-1/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/6dd/6dd1947b593065ef4922831890053632.jpg?148612952790459" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Боярин 1<br>
                                            8.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>10.5
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2100000" data-whatever="Боярин 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от

                                                    2 100  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от

                                                    2 360  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от

                                                    2 680  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от

                                                    3 970  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin-1/"></a>
                            <div class="item-show-more">
                                <div class="hover-block">
                                    <div>
                                        <a href="/catalog/kottedzhi/boyarin-1/">
                                            <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14823204292944" alt=""></i></div>
                                            <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                        </a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin-4/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/b7e/b7e89a90db0ced3c6b7b095c8be1ee14.jpg?148829549399850" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Боярин 4<br>
                                            10x12.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1470000" data-whatever="Боярин 4 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 470  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin-4/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin4/">

                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/1bb/1bb0aef5d7606c8a9be0be5a0120118c.jpg?148829556099850" alt="">

                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Боярин 4<br>
                                            10x12.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3060000" data-whatever="Боярин 4 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>

                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    3 060  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    3 370  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    3 830  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    5 660  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin4/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin-5-k-150/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/674/674fe01dd600faf1e5388bfc8e2828bf.jpg?148767012697195" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Боярин 5<br>
                                            12<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>14 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1680000" data-whatever="Боярин 5 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 680  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin-5-k-150/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin-5/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/803/803f412685b704894f6ae8ac16fd397a.jpg?148648009397195" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Боярин 5<br>
                                            12<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>14
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3450000" data-whatever="Боярин 5 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от

                                                    3 450  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от

                                                    3 840  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от

                                                    4 350  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от

                                                    6 420  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin-5/"></a>
                            <div class="item-show-more">
                                <div class="hover-block">
                                    <div>
                                        <a href="/catalog/kottedzhi/boyarin-5/">
                                            <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14823204292944" alt=""></i></div>
                                            <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                        </a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/monarkh-3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/1e9/1e91b17f570df5860aab1f8fe3ad67c3.jpg?1487149051103622" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Монарх 3<br>
                                            11.3<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>14.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1760000" data-whatever="Монарх 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 760  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/monarkh-3/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/monarkh/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/2c0/2c07a54169d20f9188987c978f96e44a.jpg?148587136623594" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Монарх 3<br>
                                            14.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>11.3
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3600000" data-whatever="Монарх 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от

                                                    3 600  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от

                                                    4 600  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от

                                                    4 600  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от

                                                    6 800  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/monarkh/"></a>
                            <div class="item-show-more">
                                <div class="hover-block">
                                    <div>
                                        <a href="/catalog/kottedzhi/monarkh/">
                                            <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14823204292944" alt=""></i></div>
                                            <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                        </a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/gertsog-1/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/a73/a73fd7ecb7bbf3e1560ba07d733d8795.jpg?148759789096521" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Герцог 1<br>
                                            7.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>8.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1120000" data-whatever="Герцог 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 120  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/gertsog-1/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/gertsog-1-k-200/">

                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/132/1326f6aa859adab5d365e7d9c7d3e91e.jpg?148760083896521" alt="">

                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Герцог 1<br>
                                            7.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>8.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2780000" data-whatever="Герцог 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>

                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    2 780  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    2 440  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    2 780  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    3 960  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/gertsog-1-k-200/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/gertsog-2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/1cf/1cff65abe65dd01138a1a01a6d544d88.jpg?148759906823689" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Герцог 2<br>
                                            8.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>9.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1280000" data-whatever="Герцог 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 280  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/gertsog-2/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/gertsog/">

                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/42b/42bcc986367a288a37e8549b6f8bc921.jpg?148587148023689" alt="">

                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Герцог 2<br>
                                            8.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>9.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="00000147504" data-price="2400000" data-whatever="Герцог 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>

                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    2 400  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    2 800  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    3 170  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    4 530  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/gertsog/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/gertsog-3+./">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/4f8/4f8ff6d3f38d3e36933e8acecda9dd7c.jpg?1487599919100960" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Герцог 3<br>
                                            9.7<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>11.4 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1490000" data-whatever="Герцог 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 490  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/gertsog-3+./"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/gertsog-3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/8a9/8a999b225db4fa970995fe3fb1dfc8bf.jpg?1487235700100960" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Герцог 3<br>
                                            9.7<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>11.4
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2780000" data-whatever="Герцог 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от

                                                    2 780  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от

                                                    3 260  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от

                                                    3 700  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от

                                                    5 280  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/gertsog-3/"></a>
                            <div class="item-show-more">
                                <div class="hover-block">
                                    <div>
                                        <a href="/catalog/kottedzhi/gertsog-3/">
                                            <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14823204292944" alt=""></i></div>
                                            <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                        </a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/galaktika-2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/9ce/9ce5921b48f46c1ea85a147b6ca79bda.jpg?148818628324093" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Галактика 2<br>
                                            12<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>15 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2090000" data-whatever="Галактика 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    2 090  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/galaktika-2/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/galaktika+2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/23b/23b74db314e52130fbab9afeb0e74a56.jpg?148818632024093" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Галактика 2<br>
                                            12<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>15  
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3990000" data-whatever="Галактика 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    3 990  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    4 760  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    5 180  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    7 380  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/galaktika+2/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/pesnya-4/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/088/0889b59066ab5ff7b27d7e10d44bcd15.jpg?148766873092715" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Песня 4<br>
                                            13<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>14 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1410000" data-whatever="Песня 4 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 410  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/pesnya-4/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/pesnya-4./">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/75d/75ddcfd07906dac18c2b3b8a316b171d.jpg?148767237992715" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Песня 4<br>
                                            13<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>14 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2700000" data-whatever="Песня 4 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    2 700  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    3 210  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    6 280  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/pesnya-4./"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/kantsler4/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/f27/f27920dad8af85b127e1bb3be46458df.jpg?148835952225052" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Канцлер 4<br>
                                            10.5x12 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1470000" data-whatever="Канцлер 4 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 470  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/kantsler4/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/kantsler-4/">

                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/cae/caee3d4cfcd92ad7e51dafc4e1bbd97c.jpg?148587215626082" alt="">

                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Канцлер 4<br>
                                            10.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>12 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="00000145918" data-price="2570000" data-whatever="Канцлер 4 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>

                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    2 570  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    2 910  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    3 680  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    5 900  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/kantsler-4/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/general/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/cd0/cd0c233aa3505a33278d9fb7a0d5b1c2.jpg?148836054129009" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Генерал<br>
                                            14x20.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2610000" data-whatever="Генерал " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    2 610  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/general/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/general-k-200/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/dd6/dd63103ee6b778cf58f56d90b4464ec5.jpg?148587224329009" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Генерал<br>
                                            14.0<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>20.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="5140000" data-whatever="Генерал " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    5 140  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    5 890  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    6 330  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    9 200  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/general-k-200/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/polkovnik1/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/805/805dbf9776ad05addfd3e6311bffa75f.jpg?148768656593091" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Полковник 1<br>
                                            10.75<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>16.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1670000" data-whatever="Полковник 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 670  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/polkovnik1/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/polkovnik-1/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/e1e/e1e6fe692521c033e03a39ceea6cc5c3.jpg?148836182488430" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Полковник 1<br>
                                            10.75x16.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3200000" data-whatever="Полковник 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    3 200  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    3 770  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    4 050  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    5 820  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/polkovnik-1/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/polkovnik-3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/7fc/7fcf326631d13e001885a85ab75bc05d.jpg?148836259625461" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Полковник 3<br>
                                            12.5x18 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1980000" data-whatever="Полковник 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 980  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/polkovnik-3/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/polkovnik3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/4ad/4ad3f912b8ae3e19da98cb3713d33baa.jpg?148587152025461" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Полковник 3<br>
                                            18x12.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3800000" data-whatever="Полковник 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    3 800  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    4 470  т.р.


                                                </strong>
                                            </p>
                                            <p>Брус клееный 180: 
                                                <strong>от 

                                                    4 810  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    6 910  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/polkovnik3/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/imperator-1-/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/234/234c098430d98bfb9c384f8903c96438.jpg?148708865924831" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Император 1 <br>
                                            7.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>8 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1050000" data-whatever="Император 1  " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для сезонного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150: 
                                                <strong>от 

                                                    1 050  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/imperator-1-/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/imperator-1/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/7b4/7b47611903f79893f7c922cd75f270c6.jpg?148587176624831" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Император 1<br>
                                            8<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>7.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="00000147505" data-price="1710000" data-whatever="Император 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    1 710  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    3 310  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/imperator-1/"></a>


                        </div>
                    </div>
                </div>
                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/imperator-2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/6fa/6fa9bbe36fac985e13cf5fc20ba9dfa5.jpg?148587170118951" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Император 2<br>
                                            9<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>8.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="00000147506" data-price="2070000" data-whatever="Император 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    2 070  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    4 120  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/imperator-2/"></a>


                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/imperator-3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/0d1/0d1d4d4e7715ab4a847f32dffd6ddbab.jpg?148587121927222" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Император 3<br>
                                            9<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>9.5 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="00000147507" data-price="2240000" data-whatever="Император 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">Дом для постоянного проживания</div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200: 
                                                <strong>от 

                                                    2 240  т.р.


                                                </strong>
                                            </p>
                                            <p>Кирпичный: 
                                                <strong>от 

                                                    4 920  т.р.


                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/imperator-3/"></a>


                        </div>
                    </div>
                </div>

                <div class="row" id="items">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">
                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin-2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/3ef/3ef8550ff286c67b00242c9ee69a939d.jpg?1488286224104150" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                            Боярин 2<br>
                                            9x11 
                                        </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1180000" data-whatever="Боярин 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для сезонного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150:
                                                <strong>от 
                                                1 180  т.р.
                                                </strong>
                                            </p>
                                            <p>Брус клееный 150:
                                                <strong>от 
                                                1 520  т.р.
                                                </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin-2/"></a>
                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/55f/55f782b0a88816e1e31ca89fd9c3db41.jpg?1489565978104150" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Боярин 2<br>
                                                                        9<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>11 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2420000" data-whatever="Боярин 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для постоянного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от 

                                                                                2 420  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от 

                                                                                2 720  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180 МБ:
                                                <strong>от 

                                                                                3 100  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от 

                                                                                4 530  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin2/"></a>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin-3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/33e/33e545a2c8371b21702760b3512c8a16.jpg?1489566064103749" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Боярин 3<br>
                                                                        9<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>12.5 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1330000" data-whatever="Боярин 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для сезонного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150:
                                                <strong>от 

                                                                                1 330  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin-3/"></a>

                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/boyarin3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/0e2/0e2695ecd438e4d6a543f7178867bd59.jpg?1489566350103749" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Боярин 3<br>
                                                                        9<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>12.5 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2700000" data-whatever="Боярин 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для постоянного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от 

                                                                                2 700  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от 

                                                                                3 060  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180 МБ:
                                                <strong>от 

                                                                                3 490  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от 

                                                                                5 100  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/boyarin3/"></a>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/monarkh-1/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/6b6/6b65d4389952f4bc634df55b73df88e7.jpg?148828971694993" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Монарх 1<br>
                                                                        10x11.5 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1480000" data-whatever="Монарх 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для сезонного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150:
                                                <strong>от 

                                                                                1 480  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/monarkh-1/"></a>

                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/monarkh1/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/015/015d2556750ee6cd6dc4b113145d5d55.jpg?148828983494993" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Монарх 1<br>
                                                                        10x11.5 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3070000" data-whatever="Монарх 1 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для постоянного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от 

                                                                                3 070  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от 

                                                                                3 400  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180 МБ:
                                                <strong>от 

                                                                                3 870  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от 

                                                                                5 660  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/monarkh1/"></a>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/monarkh-2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/b49/b49d81c52f54e24cd80a95e5c47b65ba.jpg?1488291299101118" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Монарх 2<br>
                                                                        10x13.5 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1650000" data-whatever="Монарх 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для сезонного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150:
                                                <strong>от 

                                                                                1 650  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/monarkh-2/"></a>

                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/monarkh2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/768/76811066dbe00bc1c8e2e57264a2382f.jpg?1488291379101118" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Монарх 2<br>
                                                                        10x13.5 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3330000" data-whatever="Монарх 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для постоянного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от 

                                                                                3 330  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от 

                                                                                3 730  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180 МБ:
                                                <strong>от 

                                                                                4 250  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от 

                                                                                6 230  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/monarkh2/"></a>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/pesnya-2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/c9d/c9d7a3dd225a94115c2129e34fd05a09.jpg?148956648996685" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Песня 2<br>
                                                                        11<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>12 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1150000" data-whatever="Песня 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для сезонного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150:
                                                <strong>от 

                                                                                1 150  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/pesnya-2/"></a>

                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/pesnya2/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/a43/a43ec91a7cd9270bb6a67ccf0b5e1222.jpg?148956655996685" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Песня 2<br>
                                                                        11<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>12 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2200000" data-whatever="Песня 2 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для постоянного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от 

                                                                                2 200  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от 

                                                                                2 620  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от 

                                                                                5 120  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/pesnya2/"></a>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/pesnya-3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/13d/13d363de434eaed07656d24b67f6c36b.jpg?148956803998090" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Песня 3<br>
                                                                        12<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>13 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1290000" data-whatever="Песня 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для сезонного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150:
                                                <strong>от 

                                                                                1 290  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/pesnya-3/"></a>

                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/pesnya3/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/7ab/7ab8e452d973d52cfae56d26721223bc.jpg?148956811698090" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Песня 3<br>
                                                                        12<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>13 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2470000" data-whatever="Песня 3 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для постоянного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от 

                                                                                2 470  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от 

                                                                                2 920  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от 

                                                                                5 700  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/pesnya3/"></a>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/pesnya-5/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/fb2/fb239cd53ad0f7829d8ba727882ebe32.jpg?148956819490745" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Песня 5<br>
                                                                        15<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>15.3 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="1590000" data-whatever="Песня 5 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для сезонного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150:
                                                <strong>от 

                                                                                1 590  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/pesnya-5/"></a>

                        </div>
                    </div>
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/pesnya5/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/35b/35b8c7ad9c20b63a1af2dd48b3fb3b84.jpg?148956826090745" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Песня 5<br>
                                                                        15<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>15.3 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="3090000" data-whatever="Песня 5 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для постоянного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от 

                                                                                3 090  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от 

                                                                                3 610  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от 

                                                                                6 980  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/pesnya5/"></a>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/galaktika-4/">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/fa4/fa438025b2a2a12ecb172f05d7295f6a.jpg?148966580824093" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Галактика 4<br>
                                                                        13.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>17 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="2490000" data-whatever="Галактика 4 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для сезонного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 150:
                                                <strong>от 

                                                                                2 490  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/galaktika-4/"></a>

                        </div>
                    </div>

                    <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12">
                        <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">

                            <div class="flex-row">
                                <div>
                                    <a href="/catalog/kottedzhi/galaktika-4./">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/481/4815e5544916b6333ea2934df9bf2e5a.jpg?148966551024093" alt="">
                                    </a>
                                </div>
                                <div>
                                    <div class="description">
                                        <h4 class="text-uppercase">
                                                                        Галактика 4<br>
                                                                        13.5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>17 
                                                                    </h4>
                                        <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="4810000" data-whatever="Галактика 4 " data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                    </div>
                                    <div class="options">
                                        <div>
                                            <div class="description-title">
                                                Дом для постоянного проживания
                                            </div>
                                            <p>
                                                <b>Варианты исполнения:</b>
                                            </p>
                                            <p>Каркасный 200:
                                                <strong>от 

                                                                                4 810  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180:
                                                <strong>от 

                                                                                5 710  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Брус клееный 180 МБ:
                                                <strong>от 

                                                                                6 970  т.р.

                                                                            </strong>
                                            </p>
                                            <p>Кирпичный:
                                                <strong>от 

                                                                                8 890  т.р.

                                                                            </strong>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="all-item-link" href="/catalog/kottedzhi/galaktika-4./"></a>

                        </div>
                    </div>


                </div>

            



        



            </div>
        </div></section>
<?php endif; ?>