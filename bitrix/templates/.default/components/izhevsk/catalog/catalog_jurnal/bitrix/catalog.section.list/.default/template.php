<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<section class="content text-page" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="my-margin clearfix">
					<div class="journal journal-preview-layer">


							<?php $cnt = 0; ?>
							<?php foreach($arResult['SECTIONS'] as $section):?>
								<?php if($section['DEPTH_LEVEL']):?>

									<?php if($cnt == 0):?>
										<div class="row">
									<?php endif; ?>

									<div class="col-xs-12 col-md-3 col-sm-3">
										<a href="<?=$section["SECTION_PAGE_URL"]?>">
											<div class="journal-preview">
												<div class="journal-preview-header">
													<img src="<?=$section["PICTURE"]["SRC"]?>" alt="">
												</div>
												<div class="journal-preview-content">
													<p>Выпуск №<?=($cnt+1)?></p>
													<h4>
														<?=$section['NAME']?>
													</h4>
												</div>
											</div>
										</a>
									</div>

											<?php $cnt++; ?>
											<?php if($cnt == count($arResult['SECTIONS'])):?>
												<?php $cnt = 0; ?>
												</div>
											<?php endif; ?>

										<?php endif; ?>

							<?php endforeach; ?>


					</div>
				</div>
			</div>
		</div>

	</div>
</section>