<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); 
?>
<?if (!empty($arResult)):?>
<noindex>
    <ul class="nav navbar-nav visible-xs">
        <?
        $previousLevel = 0;
        foreach($arResult as $arItem):?>

        <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
        <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
        <?endif?>

        <?if ($arItem["IS_PARENT"]):?>

        <?if ($arItem["DEPTH_LEVEL"] == 1):?>
        <li>
            <div class="dropdown visible-xs">
                <a href="#menu<?= $arItem["ITEM_INDEX"] ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span><?= $arItem["TEXT"] ?></span>
                    <span class="glyphicon glyphicon-menu-down"></span>
                    <span class="glyphicon glyphicon-menu-up"></span>
                </a>

                <ul class="dropdown-menu">
                    <?else:?>
                    <li><?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?><a href="<?= $arItem["LINK"] ?>" class="parent"><?= $arItem["TEXT"] ?></a>
                        <ul>
                            <?endif?>

                            <?else:?>

                            <?if ($arItem["PERMISSION"] > "D"):?>

                            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                            <li><a href="<?= $arItem["LINK"] ?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?= $arItem["TEXT"] ?></a></li>
                            <?else:?>
                            <li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
                            <?if($cnt == 2):?>
                                    <br/>
                                <?endif;?>
                            <?endif?>
                            <?endif?>

                            <?endif?>

                            <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
                                <?if($cnt == 2):?>
                                <? $cnt = 0; ?>
                            <?else:?>
                                <? $cnt++; ?>
                            <?endif;?>
                            <?endforeach?>

                            <?if ($previousLevel > 1)://close last item tags?>
                            <?= str_repeat("</ul></div></li>", ($previousLevel - 1)); ?>
                            <?endif?>
                           
                            <li>
                            
                                <a href="#" class="phone hidden-xs"><span id="telMain2"> +7 (4872) 770-155</span></a>
                                <a href="#" class="modal-call" data-target="#call" data-toggle="modal">обратный звонок</a>
                                <form class="navbar-form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Поиск">
                                        <button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
                                    </div>
                                </form>
                            </li>
                  
                        </ul>
                        </noindex>
                        <?endif?>