<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php
//echo "<pre>";
//var_dump($arResult);
?> 
<div class="col-xs-12 col-md-6 col-sm-12">
    <div class="flex-reversed">
        <?php foreach ($arResult["ITEMS"] as $item): ?>
            <?php if ($item["PROPERTIES"]["POSITION"]["VALUE_XML_ID"] == "RIGHT"): ?>
                <?php if ($item["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] == "standart"): ?>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-promotion desgin-2 + my-margin">
                                <div>
                                    <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                        <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                            <?php echo html_entity_decode($item["NAME"]); ?>
                                        </a>
                                    </h4>
                                    <?php if ($item["PREVIEW_TEXT"] != ""): ?>
                                        <p><?= $item["PREVIEW_TEXT"] ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?> 
    </div>
</div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="flex-reversed">
            <div class="row">
                <?php foreach ($arResult["ITEMS"] as $item): ?>
                    <?php if ($item["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] == "center"): ?>
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="my-promotion desgin-2 + my-margin">
                                <div>
                                    <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                        <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                            <?php echo html_entity_decode($item["NAME"]); ?>
                                        </a>
                                    </h4>
                                    <?php if ($item["PREVIEW_TEXT"] != ""): ?>
                                        <p><?= $item["PREVIEW_TEXT"] ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="flex-reversed">
        	<div class="row">
			    <div class="col-xs-12 col-md-12 col-sm-12">
			        <?php foreach ($arResult["ITEMS"] as $item): ?>
			            <?php if ($item["PROPERTIES"]["POSITION"]["VALUE_XML_ID"] == "LEFT"): ?>
			                <?php if ($item["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] == "long"): ?>
			                    <div class="row">
			                        <div class="col-xs-12 col-md-12 col-sm-12">
			                            <div class="my-promotion ( desgin-2 && one-column ) + my-margin">
			                                <div>
			                                    <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
			                                        <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>

			                                    </a>
			                                </div>
			                                <div>
                            
			                                    <h4 class="text-uppercase">
			                                        <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
			                                            <?php echo html_entity_decode($item["NAME"]); ?>
			                                        </a>
			                                    </h4>

			                                    <?php if ($item["PREVIEW_TEXT"] != ""): ?>
			                                        <p><?= $item["PREVIEW_TEXT"] ?></p>
			                                    <?php endif; ?>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                <?php endif; ?>
			            <?php endif; ?>
			        <?php endforeach; ?> 
			    </div>
			</div>
            <div class="row">
                <?php foreach ($arResult["ITEMS"] as $item): ?>
                    <?php if ($item["PROPERTIES"]["POSITION"]["VALUE_XML_ID"] == "LEFT"): ?>
                        <?php if ($item["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] == "standart"): ?>

                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-2 + my-margin">
                                    <div>
                                        <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                            <img src="<?= $item["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $item["PREVIEW_PICTURE"]["ALT"] ?>"/>
                                        </a>
                                    </div>
                                    <div>
                                   
                                        
                                    <?php if(strlen($item["NAME"]) == 59):?>
                                        <h1 class="text-uppercase">
                                            <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                                <?php echo html_entity_decode($item["NAME"]); ?>
                                            </a>
                                        </h1>
                                    <?php else:?>
                                        <h4 class="text-uppercase">
                                            <a href="<?= $item["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                                                <?php echo html_entity_decode($item["NAME"]); ?>
                                            </a>
                                        </h4>
                                    <?php endif; ?>
                                        <?php if ($item["PREVIEW_TEXT"] != ""): ?>
                                            <p><?= $item["PREVIEW_TEXT"] ?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?> 
            </div>

        </div>
    </div>
</div>




</div>