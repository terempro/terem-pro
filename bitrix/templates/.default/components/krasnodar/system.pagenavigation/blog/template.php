<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

if($arResult["NavPageCount"] > 1)
{
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
            <div class="my-pagination my-table my-table-center">
                <nav>
                    <ul class="mypaginations my-flex">
                        <li>Страницы: </li>



                        <?
                        if($arResult["bDescPageNumbering"] === true):
                        $bFirst = true;
                        if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):

                        ?>

                        <?

                        if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
                        $bFirst = false;
                        if($arResult["bSavePage"]):
                        ?>
                        <li><a class="" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">1</a></li>
                        <?
                        else:
                        ?>
                        <li><a class="" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a></li>
                        <?
                        endif;
                        ?>

                        <?
                        if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):
                        ?>


                        <?
                        endif;
                        endif;
                        endif;
                        do
                        {
                        $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

                        if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                        ?>


                        <li class="active"><a href=""><?= $NavRecordGroupPrint ?></a></li>

                        <?
                        elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
                        ?>
                        <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="<?= ($bFirst ? "" : "") ?>"><?= $NavRecordGroupPrint ?></a>
                        <?
                        else:
                        ?>
                        <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"<?
                               ?> class="<?= ($bFirst ? "" : "") ?>"><?= $NavRecordGroupPrint ?></a></li>

                        <?
                        endif;
                        ?>

                        <?

                        $arResult["nStartPage"]--;
                        $bFirst = false;
                        } while($arResult["nStartPage"] >= $arResult["nEndPage"]);

                        if ($arResult["NavPageNomer"] > 1):
                        if ($arResult["nEndPage"] > 1):
                        if ($arResult["nEndPage"] > 2):
                        ?>


                        <?
                        endif;
                        ?>
                        <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><?= $arResult["NavPageCount"] ?></a>

                            <?
                            endif;

                            ?>

                            <?
                            endif; 

                            else:
                            $bFirst = true;

                            if ($arResult["NavPageNomer"] > 1):

                            ?>

                            <?

                            if ($arResult["nStartPage"] > 1):
                            $bFirst = false;
                            if($arResult["bSavePage"]):
                            ?>
                        <li><a class="" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a></li>
                        <?
                        else:
                        ?>
                        <li><a class="" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a></li>
                        <?
                        endif;
                        ?>

                        <?
                        if ($arResult["nStartPage"] > 2):
                        ?>


                        <?
                        endif;
                        endif;
                        endif;

                        do
                        {
                        if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                        ?>
                        <li class="active"><a href="#"><?= $arResult["nStartPage"] ?></a></li> 
                        <?
                        elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
                        ?>
                        <li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="<?= ($bFirst ? "" : "") ?>"><?= $arResult["nStartPage"] ?></a></li>
                        <?
                        else:
                        ?>
                        <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"<?
                               ?> class="<?= ($bFirst ? "" : "") ?>"><?= $arResult["nStartPage"] ?></a></li>
                        <?
                        endif;
                        ?>

                        <?
                        $arResult["nStartPage"]++;
                        $bFirst = false;
                        } while($arResult["nStartPage"] <= $arResult["nEndPage"]);

                        if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                        if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
                        if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
                        ?>
                        <li ><a href="#">...</a></li>

                        <?
                        endif;
                        ?>
                        <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a></li>
                        <span class="blog-vert-separator"></span>
                        <?
                        endif;
                        ?>

                        <?
                        endif;
                        endif;

                        if ($arResult["bShowAll"]):
                        if ($arResult["NavShowAll"]):
                        ?>
                        <li><a class="blog-page-pagen" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=0"><?= GetMessage("nav_paged") ?></a></li>
                        <?
                        else:
                        ?>
                        <li><a class="blog-page-all" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=1"><?= GetMessage("nav_all") ?></a></li>
                        <?
                        endif;
                        endif
                        ?>

                        <?
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>