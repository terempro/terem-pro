<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<section class="content content-back-side" data-parallax="up" data-opacity="true">
    <div class="container-fluid reset-padding">
        <div class="row reset-margin">
            <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
                <div class="back-img"
                     style="background-image: url('/bitrix/templates/.default/assets/img/bg.jpg');"></div>
            </div>
        </div>
    </div>
</section>
<h1>NEW TMP</h1>