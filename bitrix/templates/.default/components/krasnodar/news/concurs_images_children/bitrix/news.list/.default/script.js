function Like(param) {
    var count = parseInt($('#' + param).text());
    if (isNaN(count)) {
        count = 0;
    }
    var newCount = count + 1;


    $.post('/include/like.php', {param: param, newCount: newCount}, function (data) {
        if (data.trim().toString() == 'die') {
            $('#' + param).text(count);
            $('#like_' + param).text(count);
        } else {
            $('#' + param).text(newCount);
            $('#like_' + param).text(newCount);
        }
    });

}