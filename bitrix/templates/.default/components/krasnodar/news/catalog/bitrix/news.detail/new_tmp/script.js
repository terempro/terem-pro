$(document).ready(function () {


    $('.MyConstructor').click(function () {
        var item = $(this).data('item'),
                price = parseInt($(this).data('price')),
                optprice = $(this).data('optprice'),
                CountOptChek = $('.chkOptimal_' + item).length,
                CountDopChek = $('.chkDop_' + item).length,
                IdPrice = 'MyPrice_' + item,
                sale = 0;

        

        $('.chkOptimal_' + item).click(function () {
            var sale = 0;
            Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
        });

        $('.chkDop_' + item).click(function () {
            var sale = 0;
            Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
        });

        //CHEKED
        
        
        
        $('#cek1_' + item).change(function () {
            if ($(this).is(':checked')) {
                $('#cek2_' + item).prop('checked', false);
                $('#cek3_' + item).prop('checked', false);
                var sale = parseInt($(this).val());
                Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
            } else {
                var sale = 0;
                Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
            }
        });

        $('#cek2_' + item).change(function () {
            if ($(this).is(':checked')) {
                $('#cek3_' + item).prop('checked', false);
                $('#cek1_' + item).prop('checked', false);
                var sale = 0;
                Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
            }
        });

        $('#cek3_' + item).change(function () {
            if ($(this).is(':checked')) {
                $('#cek2_' + item).prop('checked', false);
                $('#cek1_' + item).prop('checked', false);
                var sale = 0;
                Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
            }
            
        });
        
        if($('#cek1_' + item).is(':checked')){
            $('#cek2_' + item).prop('checked', false);
            $('#cek3_' + item).prop('checked', false);
            var sale = parseInt($('#cek1_' + item).val());
            Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
        }else{
            Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
        }
    });

    $('.BtnActivate').click(function () {
        var item = $(this).data('item'),
                price = parseInt($(this).data('price')),
                optprice = $(this).data('optprice'),
                CountOptChek = $('.chkOptimal_' + item).length,
                CountDopChek = $('.chkDop_' + item).length,
                IdPrice = 'MyPrice_' + item;

        $('.chkOptimal_' + item).prop('checked', true);
        var sale = 0;
        Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale);
    });




});








function Price(item, price, optprice, CountOptChek, CountDopChek, IdPrice, sale) {

   

    var chekOpt_array = new Array(),
            chekDop_array = new Array(),
            CntChekOpt = 0,
            CntChekDop = 0,
            PriceOpt = 0,
            PriceDop = 0;

    $('.chkOptimal_' + item).each(function () {
        if ($(this).is(':checked')) {
            chekOpt_array.push($(this).val());
            PriceOpt = (PriceOpt + parseInt($(this).val()));
            CntChekOpt++;
        }
    });



    $('.chkDop_' + item).each(function () {
        if ($(this).is(':checked')) {
            chekDop_array.push($(this).val());
            PriceDop = (PriceDop + parseInt($(this).val()));
            CntChekDop++;
        }
    });

    if (CntChekOpt > 0) {
        var PriceOpt2 = PriceOpt - ((PriceOpt * 30) / 100);

        $('#p2_' + item).text(PriceOpt.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $('#p1_' + item).text(PriceOpt2.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));


    } else {
        $('#p2_' + item).text('не выбрано');
        $('#p1_' + item).text('не выбрано');
    }

    if (CntChekDop > 0) {
        var PriceDop2 = PriceDop / 2;

        $('#d2_' + item).text(PriceDop.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $('#d1_' + item).text(PriceDop2.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));


    } else {
        $('#d2_' + item).text('не выбрано');
        $('#d1_' + item).text('не выбрано');
    }





    var NewPrice = PriceOpt + PriceDop + price - sale;
    
    

    if (CntChekOpt == CountOptChek) {


        $('.base-2').addClass('base-2-active');
        $('.base-3').addClass('base-3-active');
        $('.OptCell1_' + item).removeClass('disable');
        $('.OptCell2_' + item).addClass('disable');
        $('.DopCell1_' + item).removeClass('disable');
        $('.DopCell2_' + item).addClass('disable');


        $('#mychek2_' + item).removeClass('disable');

        $('#p2_' + item).addClass('disable');
        $('#p1_' + item).removeClass('disable');
        $('#d2_' + item).addClass('disable');
        $('#d1_' + item).removeClass('disable');
        $('#s2_' + item).addClass('disable');
        $('#s1_' + item).removeClass('disable')
        $('#i2_' + item).addClass('disable');
        $('#i1_' + item).removeClass('disable')

        PriceDop = PriceDop / 2;
        PriceOpt = PriceOpt - ((PriceOpt * 30) / 100);
        var NewPrice = PriceOpt + PriceDop + optprice - sale;
    } else {
        $('.base-2').removeClass('base-2-active');
        $('.base-3').removeClass('base-3-active');
        $('.OptCell1_' + item).addClass('disable');
        $('.OptCell2_' + item).removeClass('disable');
        $('.DopCell1_' + item).addClass('disable');
        $('.DopCell2_' + item).removeClass('disable');


        $('#mychek2_' + item).addClass('disable');

        $('#p2_' + item).removeClass('disable');
        $('#p1_' + item).addClass('disable');
        $('#d2_' + item).removeClass('disable');
        $('#d1_' + item).addClass('disable');
        $('#s2_' + item).removeClass('disable');
        $('#s1_' + item).addClass('disable');
        $('#i2_' + item).removeClass('disable');
        $('#i1_' + item).addClass('disable');


    }

    

    $('#' + IdPrice).text(NewPrice.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " р.");



}