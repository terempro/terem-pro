<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<section class="content text-page white" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white my-margin clearfix">
					<div class="journal journal-article-layer">

						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="journal-article-title">
									<h1><?=$arResult['NAME']?></h1>
								</div>
							</div>
						</div>
						<div class="journal-article-content">
							<?=$arResult['DETAIL_TEXT']?>
						</div>

						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="journal-nav">
									<div>
										<?if(is_array($arResult["TOLEFT"])):?>
											<a href="<?=$arResult["TOLEFT"]["URL"]?>">< <span>НАЗАД</span></a>
										<?php endif;?>
									</div>
									<div>
										<?if(is_array($arResult["TORIGHT"])):?>
											<a href="<?=$arResult["TORIGHT"]["URL"]?>"><span>ВПЕРЕД</span> ></a>
										<?php endif;?>
									</div>
								</div>
							</div>
						</div>
<!--						<div class="row">-->
<!--							<div class="col-xs-12 col-md-12 col-sm-12 text-center">-->
<!--								<div class="journal-subscribe">-->
<!--									<a href="#" class="btn btn-danger btn-ghost">оформить бесплатную подписку</a>-->
<!--								</div>-->
<!--							</div>-->
<!--						</div>-->
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

