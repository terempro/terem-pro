<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$this->setFrameMode(false);
?>


<section class="content filter-wrapper" role="content">
    <div class="container">
        <div class="row catlog-text">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="catalog-description + my-margin + my-padding">
                     <h2 class="filter-block__title">Выберите параметры дома, который нужен Вам</h2>
                        <?php include $_SERVER['DOCUMENT_ROOT'].'/include/filter.php'; ?>
                    <div class="filter-block__extra-info">
                        <p class="filter-block__extra-info--text">
                            Наши специалисты ответят на Ваши вопросы по телефону
                            <br>
                            +7 (495) 419-14-14 или <a href="#" class="modal-call" data-target="#call" data-toggle="modal"><span class="filter-block__extra-info--text--blue-link">перезвонят Вам через 30 секунд</span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="ajax-catalog">
     <?php
    $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list", "", array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
        "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
        "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
        "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
            ), $component, array("HIDE_ICONS" => "Y")
    );
    ?>
</div>


