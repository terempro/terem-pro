<ul class="nav navbar-nav">
    <li><a href="#"><span>Главная</span></a></li>
    <li><a class="active" href="#"><span>Каталог</span></a></li>
    <li class="">
        <div class="has-inner-menu hidden-xs">
            <a href="#">
                <span>Услуги</span>
                <span class="glyphicon glyphicon-menu-down"></span>
                <span class="glyphicon glyphicon-menu-up"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="">Услуги</a></li>
                <li><a href="">Услуги</a></li>
                <li><a href="">Услуги</a></li>
            </ul>
        </div>
        <!--for mobile and table screens 0<768 -->
    <noindex>
        <div class="dropdown visible-xs">
            <a href="#services" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span>Услуги</span>
                <span class="glyphicon glyphicon-menu-down"></span>
                <span class="glyphicon glyphicon-menu-up"></span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="services">
                <li><a href="">Услуги</a></li>
                <li><a href="">Услуги</a></li>
                <li><a href="">Услуги</a></li>
            </ul>
        </div>
    </noindex>
    <!--for mobile and table screens 0<768 END-->
</li>
<li>
    <div class="has-inner-menu hidden-xs">
        <a href="#">
            <span>О компании</span>
            <span class="glyphicon glyphicon-menu-down"></span>
            <span class="glyphicon glyphicon-menu-up"></span>
        </a>
        <ul class="dropdown-menu">
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
        </ul>
    </div>
    <!--for mobile and table screens 0<768 -->
<noindex>
    <div class="dropdown visible-xs">
        <a href="#company" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span>О компании</span>
            <span class="glyphicon glyphicon-menu-down"></span>
            <span class="glyphicon glyphicon-menu-up"></span>
        </a>
        <ul class="dropdown-menu" aria-labelledby="company">
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
            <li><a href="">Услуги</a></li>
        </ul>
    </div>
</noindex>
<!--for mobile and table screens 0<768 END-->
</li>
<li><a href="#"><span>Акции</a></span></li>
<li><a href="#"><span>Прайс-лист</span></a></li>
<li><a href="#"><span>Контакты</span></a></li>
<li>
    <a href="#" class="phone hidden-xs">+7 (495) <span id="telMain">419-14-14</span></a>
    <a href="#" class="modal-call" data-target="#call" data-toggle="modal">обратный звонок</a>
    <form class="navbar-form">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Поиск">
            <button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
        </div>
    </form>
</li>
</ul>