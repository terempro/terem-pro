<?php

//Привязка информация о домах
$arSelect = Array(
    "ID",
    "NAME",
    "CODE",
    "IBLOCK_SECTION_ID",
    "PREVIEW_PICTURE",
    "PREVIEW_TEXT",
    "DETAIL_TEXT",
    "DETAIL_PICTURE",
    "DATE_ACTIVE_FROM",
    "PROPERTY_SIZE_HOUSE",
    "PROPERTY_SQUARE_HOUSE",
    "PROPERTY_SQUARE_ALL_HOUSE",
    "PROPERTY_SQUARE_LIFE_HOUSE",
    "PROPERTY_PLANS_HOUSE_1",
    "PROPERTY_PLANS_HOUSE_2",
    "PROPERTY_COMPECTATION_HOUSE",
    "PROPERTY_AS_FILE",
    "PROPERTY_TEHNOLOGY_HOUSE",
    "PROPERTY_D_RACURS",
    "PROPERTY_NITERIER",
    "PROPERTY_EXTERIER",
    "PROPERTY_DAYS_HOUSE",
    "PROPERTY_PRICE_HOUSE",
    "PROPERTY_CODE_1C",
    "PROPERTY_LIKE_HOUSE",
    "PROPERTY_REVIEWS_HOUSE",
    "PROPERTY_SERVICES_OPTIMAL",
    "PROPERTY_PRESENTS_OPTIMAL",
    "PROPERTY_PERSON_PHOTO",
    "PROPERTY_ID_CALCULATOR",
    "PROPERTY_SALE",
    "PROPERTY_PRESENT",
    "PROPERTY_PRESENT2",
    "PROPERTY_VIDEO"
);
$cnt = 0;
foreach ($arResult["PROPERTIES"]["VARIANTS_HOUSE"]["~VALUE"] as $item) {
    $arFilter = Array(
        "IBLOCK_ID" => 21,
        "ID" => $item,
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y"
    );
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arResult['HOUSE_INFORMATION'][$cnt] = $ob->GetFields();
        $cnt++;
    }
}



//Привязка информация ценах
$cnt2 = 0;
$arSelect2 = Array(
    "ID",
    "NAME",
    "PROPERTY_PRICE_1",
    "PROPERTY_PRICE_2",
    "PROPERTY_PRICE_3",
    "PROPERTY_OPTIMAL_PRICE",
    "PROPERTY_KEY_PRICE",
    "PROPERTY_KEY_DECRIPTION"
);



foreach ($arResult["HOUSE_INFORMATION"] as $k => $item) {
    $arFilter2 = Array(
        "IBLOCK_ID" => 22,
        "ID" => $item["PROPERTY_PRICE_HOUSE_VALUE"],
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y"
    );
    $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelect2);
    while ($ob = $res->GetNextElement()) {
        $arResult['HOUSE_INFORMATION'][$cnt2]["PRICES"] = $ob->GetFields();
        $cnt2++;
    }



    $arImage1 = CFile::ResizeImageGet($item["PROPERTY_PLANS_HOUSE_1_VALUE"], array('width' => 838, 'height' => 546), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
    $arImage2 = CFile::ResizeImageGet($item["PROPERTY_PLANS_HOUSE_2_VALUE"], array('width' => 838, 'height' => 546), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
    $arImage3 = CFile::ResizeImageGet($item["PROPERTY_PLANS_HOUSE_1_VALUE"], array('width' => 285, 'height' => 285), true);
    $arImage4 = CFile::ResizeImageGet($item["PROPERTY_PLANS_HOUSE_2_VALUE"], array('width' => 285, 'height' => 285), true);
    $arResult["HOUSE_INFORMATION"][$k]['PLAN1_838'] = $arImage1['src'];
    $arResult["HOUSE_INFORMATION"][$k]['PLAN2_838'] = $arImage2['src'];
    $arResult["HOUSE_INFORMATION"][$k]['PLAN1_285'] = $arImage3['src'];
    $arResult["HOUSE_INFORMATION"][$k]['PLAN2_285'] = $arImage4['src'];




    foreach ($arResult["HOUSE_INFORMATION"] as $k => $item) {
        $summ = 0;
        $p = $item["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"] . "000";
        if (count($item["PROPERTY_SERVICES_OPTIMAL_DESCRIPTION"]) > 1) {
            foreach ($item["PROPERTY_SERVICES_OPTIMAL_DESCRIPTION"] as $i) {
                $summ = $summ + $i;
            }
        }


        $arResult["HOUSE_INFORMATION"][$k]["SUMMDOP"] = $summ;
        $arResult["HOUSE_INFORMATION"][$k]["ALLSUMM"] = $p + $summ;

        if ($USER->isAdmin()) {
            //echo "<pre>";
            //var_dump($arResult["HOUSE_INFORMATION"]);
        }

        $file = '/var/www/zolotarev/data/www/terem-pro.ru/upload/calcs/' . $item["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' . $item["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';


        error_reporting(E_ALL);
        $f = fopen($file, 'r');
        $file_data = fread($f, filesize($file));
        fclose($f);

        $result = unserialize($file_data);

        $price = 0;
        $OptimalSum = 0;

       



        $arResult["HOUSE_INFORMATION"][$k]["CALC"] = $result;




        if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный') {
        
        
             
        
            //echo "<pre>";
            //var_dump($result[0]["ITEMS"]);
            //die();
            foreach ($result[0]["ITEMS"] as $i) {
            
               
            
            
                $price = (int) $i['PRICE'];
                $price = $price - (($price * 30) / 100);
                $OptimalSum = $OptimalSum + $price;
                
                
                
            }
            
            
           
        }
        
        if ($USER->IsAdmin()){
            //echo "<pre>";
            //var_dump($arResult["HOUSE_INFORMATION"][$k]["CALC"]);
        }





//Первый IF с ----
    if ($USER->IsAdmin()) {
         $myPrice = $arResult["HOUSE_INFORMATION"][$k]["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"];
        //echo "<pre>";
        //var_dump($arResult["HOUSE_INFORMATION"][0]["CALC"]);
        
        //echo $OptimalSum."<br/>";
    }
   
        
        if ($OptimalSum != 0) {
            
         //Второй IF выводится только 2ое число 
           if ($USER->IsAdmin()) {
            //echo 1;
            //echo "<pre>";
            //var_dump($myPrice);
           }
      
        
            if($item["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"]){
                 $ValPrice = "" . $item["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"] . "000";
            }else{
                 $ValPrice = "" . $arResult["HOUSE_INFORMATION"][$k]["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"] . "000";
            }
            
            
        
           
            
            
            $ValPrice = (int) $ValPrice;
            $OptimalSum = $OptimalSum + $ValPrice;

            $arResult["HOUSE_INFORMATION"][$k]["OPTIMALPRICE"] = $OptimalSum;
        }
    }
}






