<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?php
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
$section = '';
$page = 200;

if($dir == '/catalog/besedki-bani-i-sadovye-doma/'){
    $section = 7;
    $page = 200;
}

if($dir == '/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-70-do-100-kv-m/'){
    $section = 29102;
    $page = 200;
} 

if($dir == '/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-100-do-150-kv-m/'){
    $section = 29103;
    $page = 200;
}
if($dir == '/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-150-do-400-kv-m/'){
    $section = 29104;
    $page = 200;
}

if($dir == '/catalog/klassicheskie-dachnye-doma/'){
    $section = 9;
    $page = 200;
}
if($dir == '/catalog/doma-dlya-postoyannogo-prozhivaniya/'){
    $section = 13;
    $page = 200;
}
if($dir == '/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/'){
    $section = 11;
    $page = 200;
}
if($dir == '/catalog/originalnye-dachnye-doma-vityaz-odnoetazhnye-i-so-vtorym-svetom/'){
    $section = 12;
    $page = 200;
}
if($dir == '/catalog/kirpichnie-doma/'){
    $section = 706;
    $page = 200;
}
if($dir == '/catalog/karkasnye-doma/'){
    $section = 28733;
    $page = 26;
}
if($dir == '/catalog/doma-iz-brusa/'){
    $section = 28734;
    $page = 10;
}
if($dir == '/catalog/sadovye-doma/'){
    $section = 28732;
    $page = 200;
}
if($dir == '/catalog/malye-stroeniya-besedki/'){
    $section = 28731;
    $page = 200;
}
if($dir == '/catalog/dvukhetazhnye-doma-iz-brusa/'){
    $section = 29082;
    $page = 200;
}



global $arrFilter;
if($dir == '/catalog/vse-doma/' || $dir == '/catalog/'){
    $arrFilter = array("SECTION_ID" => Array(7,12,13,9,11));
}else{
    $arrFilter = array("SECTION_ID" => Array($section));
}






?>

<?php
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"catalog_list_tmp", 
	array(
		"COMPONENT_TEMPLATE" => "catalog_list_tmp",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "13",
		"NEWS_COUNT" => $page,
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(
			0 => "CODE",
			1 => "XML_ID",
			2 => "NAME",
			3 => "PREVIEW_TEXT",
			4 => "PREVIEW_PICTURE",
			5 => "DETAIL_TEXT",
			6 => "DETAIL_PICTURE",
			7 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "SERIES",
			1 => "LIDER",
			2 => "SIZER",
			3 => "PRICE_FROM",
			4 => "SIZE8",
			5 => "SIZE9",
			6 => "SIZE2",
			7 => "SIZE10",
			8 => "SIZE11",
			9 => "SIZE3",
			10 => "SIZE4",
			11 => "SIZE5",
			12 => "SIZE1",
			13 => "SIZE6",
			14 => "SIZE7",
			15 => "VARIANT1",
			16 => "VARIANT2",
			17 => "VARIANT6",
			18 => "VARIANT7",
			19 => "VARIANT4",
			20 => "VARIANT5",
			21 => "VARIANT3",
			22 => "PRICE2",
			23 => "PRICE3",
			24 => "PRICE4",
			25 => "PRICE5",
			26 => "PRICE1",
			27 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => "blog",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);
?>
