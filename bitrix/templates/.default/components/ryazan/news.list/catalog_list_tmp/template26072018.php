<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$this->setFrameMode(true);
?>
<?php $page = $APPLICATION->GetCurPage(); ?>
<?php foreach ($arResult["ITEMS"] as $arItem): ?>
    <?php
    $SERIES[$arItem['PROPERTIES']['SERIES']["VALUE_SORT"]] = $arItem['PROPERTIES']['SERIES']['VALUE_XML_ID'] . "/" . $arItem['PROPERTIES']['SERIES']['VALUE'];
    ?>
    <?php foreach ($arItem['PROPERTIES']["SIZ_FILTR"]["VALUE_XML_ID"] as $k => $propID): ?>
        <?php $SIZERS[$arItem['PROPERTIES']["SIZ_FILTR"]["VALUE_SORT"][$k]] = $propID . "/" . $arItem['PROPERTIES']["SIZ_FILTR"]["~VALUE"][$k]; ?>
    <?php endforeach; ?>
    <?php foreach ($arItem['PROPERTIES']["TEHNOLOGY_FILTER"]["VALUE_XML_ID"] as $k => $propID): ?>
        <?php $TEHNOLOGY[] = $propID . "/" . $arItem['PROPERTIES']["TEHNOLOGY_FILTER"]["~VALUE"][$k]; ?>
    <?php endforeach; ?>
    <?php foreach ($arItem['PROPERTIES']["PRICE_FILTER"]["VALUE_XML_ID"] as $k => $propID): ?>
        <?php $PRICES[] = $propID . "/" . $arItem['PROPERTIES']["PRICE_FILTER"]["~VALUE"][$k]; ?>
    <?php endforeach; ?>
    <?php foreach ($arItem['PROPERTIES']["SIZE_FILTER"]["VALUE_XML_ID"] as $k => $propID): ?>
        <?php $SIZES[] = $propID . "/" . $arItem['PROPERTIES']["SIZE_FILTER"]["~VALUE"][$k]; ?>
    <?php endforeach; ?>
<?php endforeach; ?>

<?php
ksort($SERIES);
ksort($SIZERS);
if ($USER->IsAdmin()) {
    //echo '<pre>';
    //var_dump($SIZERS);
    //die();
}
?>
<!--noindex-->
<script>
    filter = {
        <?php if (count($SERIES) > 0): ?>
        series: {
            title: 'Серии',
            values: {
                <?php foreach ($SERIES as $s): ?>
                <?= explode('/', $s)[0] ?>: '<?= explode('/', $s)[1] ?>',
            <?php endforeach; ?>
        }
    },
<?php endif; ?>







<?php if (count($SERIES) > 0): ?>
    technology: {
        title: 'Технологии',
        values: {
            <?php foreach ($TEHNOLOGY as $s): ?>
            <?= explode('/', $s)[0] ?>: '<?= explode('/', $s)[1] ?>',
        <?php endforeach; ?>
    }
},
<?php endif; ?>
<?php if (count($SERIES) > 0): ?>
    cost: {
        title: 'Стоимость <span class="title-cost">т.р.</span>',
        values: {
            <?php foreach ($PRICES as $s): ?>
            <?= explode('/', $s)[0] ?>: '<?= explode('/', $s)[1] ?>',
        <?php endforeach; ?>
    }
},
<?php endif; ?>





<?php if (count($SERIES) > 0): ?>
    square: {
        title: 'Площадь <span class="title-square">кв.м</span>',
        values: {
            <?php foreach ($SIZES as $s): ?>
            <?= explode('/', $s)[0] ?>: '<?= explode('/', $s)[1] ?>',
        <?php endforeach; ?>
    }
},
<?php endif; ?>


<?php if (count($SERIES) > 0): ?>
    sizers: {
        title: 'Размер <span class="title-cost">м</span>',
        values: {
            <?php foreach ($SIZERS as $s): ?>
            <?= explode('/', $s)[0] ?>: '<?= explode('/', $s)[1] ?>',
        <?php endforeach; ?>
    }
}
<?php endif; ?>


}
</script>
<!--/noindex-->

<?php if ($page == '/catalog/vse-doma/' || $page == '/catalog/' || $page == '/catalog/doma-dlya-postoyannogo-prozhivaniya/'): ?>
    <!--noindex-->
    <script>
        jQuery.fn.sortDomElements = (function() {
            return function(comparator) {
                return Array.prototype.sort.call(this, comparator).each(function(i) {
                    this.parentNode.appendChild(this);
                });
            };
        })();
        $(document).ready(function () {

            $('#items .item').hide();
            var str = '';
            for (var k in filter) {
                $('.filter').append('<div class="col-xs-12 col-sm-2 col-md-2"> <form> <div class="row"> <div class="col-xs-12 col-md-12 col-sm-12"> <p class="form-title">' + filter[k].title + '</p> </div> </div> <div class="row"><div class="col-xs-12 col-sm-12 col-md-12"> <ul id="' + k + '"></ul> </div> </div> </form> </div>'); for (var k2 in filter[k].values) {$('#' + k).append('<li> <div class="form-group"> <div class="row"> <div class="col-xs-12 col-md-12 col-sm-12"> <div class="new-checkbox" data-item="' + k + '_' + k2 + '" id="' + k + '_' + k2 + '"> <p class="checkbox-filter">' + filter[k].values[k2] + '</p> <span class="icon" data-item="remove"><span class="remove">×</span></span> </div> </div> </div> </div> </li>'); } }

                $('.checkbox-filter').click(function () {
                    $(this).parent().addClass('picked');
                    filterItems();
                });
                /*click remove*/
                $('[data-item="remove"]').click(function(e){
                    $(this).parent('.new-checkbox').removeClass('picked');
                    filterItems();
                });
            });
        var r = 2;
        var pic = 0;
        function filterItems(){

            $('#category').hide();
            $('.catlog-text').hide();
            var selector = '';
            $('.picked-list').html('<p class="text-select">Выбрано:</p>');
            $('.picked').each(function (i, e) {
                selector += '.' + $(this).attr('id');
                var item = $(e).clone();
                $('.picked-list').append(item);
            });
            if (selector.length == 0) {
                selector = '*';
                $('.picked-list .text-select').remove();
            }

            $('#items .item').hide();
                        //$('#category').hide();
                        $('#items .item').filter(selector).show();
                        $('#items .item').filter(selector).attr('sortKey', '1');
                        if (r == 2){
                            $("#items").children().sortDomElements(function(a, b){
                                akey = $(a).attr("sortkey");
                                bkey = $(b).attr("sortkey");
                                if (akey == bkey) return 0;
                                if (akey < bkey) return - 1;
                                if (akey > bkey) return 1;
                            });
                        }

                        if (r == 1 || selector == '*'){
                            console.log(selector);
                            $('#category').show();
                            $('.catlog-text').show();
                            $("#items").children().sortDomElements(function(a, b){
                                akey = $(a).attr("sort");
                                bkey = $(b).attr("sort");
                                if (akey == bkey) return 0;
                                if (akey < bkey) return - 1;
                                if (akey > bkey) return 1;
                            });
                            r = 2;
                        //console.log(r);
                    }



                    $('#items .item').filter(selector).attr('sortKey', '2');
                    var classes = [];
                    $('#items .item:visible').each(function (i, e) {
                        var tmp = ($(e).attr('class')).split(' ');
                        for (var k in tmp) {
                            if ((tmp[k]).substr(0, 6) == 'series' || (tmp[k]).substr(0, 10) == 'technology' || (tmp[k]).substr(0, 4) == 'cost' || (tmp[k]).substr(0, 5) == 'sizer' || (tmp[k]).substr(0, 6) == 'square'){
                                classes.push('#' + tmp[k]);
                            }
                        }
                    });
                    var checkbox_selector = classes.join(', ');
                    $('.filter .new-checkbox').hide();
                    $('.filter .new-checkbox').filter(checkbox_selector).show();
                    $('.picked-list [data-item="remove"]').click(function(e){
                        var ids = $(this).parent('.new-checkbox').attr('id');
                        resetPic(ids);
                    });
                    $('.reset-all').click(function(){
                        $('.new-checkbox').removeClass('picked');
                        r = 1;
                        filterItems();
                        $('.picked-list .text-select').remove();
                        $('#items .item').hide();
                        $('#category').show();
                        $('.catlog-text').show();
                    });
                    if (selector == '*'){
                        $('#items .item').hide();
                    }
                }

                function resetPic(param){
                    var i = param;
                    $('div#' + i).removeClass('picked');
                    filterItems();
                }
            </script>
            <!--/noindex-->
        <?php else: ?>
            <!--noindex-->
            <script>
                jQuery.fn.sortDomElements = (function() {
                    return function(comparator) {
                        return Array.prototype.sort.call(this, comparator).each(function(i) {
                            this.parentNode.appendChild(this);
                        });
                    };
                })();
                $(document).ready(function () {

                    var str = '';
                    for (var k in filter) {
                        $('.filter').append('<div class="col-xs-12 col-sm-2 col-md-2"> <form> <div class="row"> <div class="col-xs-12 col-md-6 col-sm-6"> <p class="form-title">' + filter[k].title + '</p> </div> </div> <div class="row"><div class="col-xs-12 col-sm-12 col-md-12"> <ul id="' + k + '"></ul> </div> </div> </form> </div>'); for (var k2 in filter[k].values) {$('#' + k).append('<li> <div class="form-group"> <div class="row"> <div class="col-xs-12 col-md-12 col-sm-12"> <div class="new-checkbox" data-item="' + k + '_' + k2 + '" id="' + k + '_' + k2 + '"> <p class="checkbox-filter">' + filter[k].values[k2] + '</p> <span class="icon" data-item="remove"><span class="remove">×</span></span> </div> </div> </div> </div> </li>'); } }

                        $('.checkbox-filter').click(function () {
                            $(this).parent().addClass('picked');
                            filterItems();
                        });
                        /*click remove*/
                        $('[data-item="remove"]').click(function(e){
                            $(this).parent('.new-checkbox').removeClass('picked');
                            filterItems();
                        });
                    });
                var r = 2;
                var pic = 0;
                function filterItems(){



                    var selector = '';
                    $('.picked-list').html('<p class="text-select">Выбрано:</p>');
                    $('.picked').each(function (i, e) {
                        selector += '.' + $(this).attr('id');
                        var item = $(e).clone();
                        $('.picked-list').append(item);
                    });
                    if (selector.length == 0) {
                        selector = '*';
                        $('.picked-list .text-select').remove();
                    }

                    $('#items .item').hide();
                    $('#items .item').filter(selector).show();
                    $('#items .item').filter(selector).attr('sortKey', '1');
                    if (r == 2){
                        $("#items").children().sortDomElements(function(a, b){
                            akey = $(a).attr("sortkey");
                            bkey = $(b).attr("sortkey");
                            if (akey == bkey) return 0;
                            if (akey < bkey) return - 1;
                            if (akey > bkey) return 1;
                        });
                        //console.log(r);
                    }

                    if (r == 1 || selector == '*'){
                        $("#items").children().sortDomElements(function(a, b){
                            akey = $(a).attr("sort");
                            bkey = $(b).attr("sort");
                            if (akey == bkey) return 0;
                            if (akey < bkey) return - 1;
                            if (akey > bkey) return 1;
                        });
                        r = 2;
                        //console.log(r);
                    }



                    $('#items .item').filter(selector).attr('sortKey', '2');
                    var classes = [];
                    $('#items .item:visible').each(function (i, e) {
                        var tmp = ($(e).attr('class')).split(' ');
                        for (var k in tmp) {
                            if ((tmp[k]).substr(0, 6) == 'series' || (tmp[k]).substr(0, 10) == 'technology' || (tmp[k]).substr(0, 4) == 'cost' || (tmp[k]).substr(0, 5) == 'sizer' || (tmp[k]).substr(0, 6) == 'square'){
                                classes.push('#' + tmp[k]);
                            }
                        }
                    });
                    var checkbox_selector = classes.join(', ');
                    $('.filter .new-checkbox').hide();
                    $('.filter .new-checkbox').filter(checkbox_selector).show();
                    $('.picked-list [data-item="remove"]').click(function(e){
                        var ids = $(this).parent('.new-checkbox').attr('id');
                        resetPic(ids);
                    });
                    $('.reset-all').click(function(){
                        $('.new-checkbox').removeClass('picked');
                        r = 1;
                        filterItems();
                        $('.picked-list .text-select').remove();
                    });
                }

                function resetPic(param){
                    var i = param;
                    $('div#' + i).removeClass('picked');
                    filterItems();
                }
            </script>
            <!--/noindex-->
        <?php endif; ?>
        <section class="content content-back-side" role="content" data-parallax="up" data-opacity="true">
            <div class="container-fluid reset-padding">
                <div class="row reset-margin">
                    <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
                        <div class="back-img" style="background-image: url('/bitrix/templates/.default/assets/img/bg.jpg');"></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="content" data-item="push-left">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12 col-sm-12">
                        <div class="my-margin-first-block top-bar-push-left">
                            <div class="offset with-two"></div>
                            <div class="attachment">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <ul class="bread-crumbs with-filter">
                                            <?php
                                            $APPLICATION->IncludeComponent(
                                                "bitrix:breadcrumb", "breadcrumbs_catalog", array(
                                                    "COMPONENT_TEMPLATE" => "breadcrumbs",
                                                    "START_FROM" => "0",
                                                    "PATH" => "",
                                                    "SITE_ID" => "-"
                                                    ), false
                                                );
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <!--noindex-->
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="catalog-filter">
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-filter" data-toggle="dropdown" data-target="#" haspopup="true" aria-expanded="false">
                                                        <span><strong>Фильтр</strong></span>
                                                        <span class="glyphicon glyphicon-menu-up"></span>
                                                        <span class="glyphicon glyphicon-menu-down"></span>
                                                    </button>
                                                    <div class="my-flex grey">
                                                        <div class="picked-list my-flex">



                                                        </div>
                                                        <div>
                                                            <a href="#" class="reset-all">Сбросить все фильтры</a>
                                                        </div>
                                                    </div>
                                                    <div class="row"></div>
                                                    <div class="row dropdown-menu filter"  aria-labelledby="myDrop">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/noindex-->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <section class="catalog-after-padding">
            </section>
            <section class="content" role="content">
                <div class="container">
                    <div class="row catlog-text">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="catalog-description + my-margin">
                                <?php
                                global $APPLICATION;
                                $dir = $APPLICATION->GetCurDir();
                                ?>
                                <?php if ($dir == '/catalog/malye-stroeniya-besedki/' && !isset($_GET['PAGEN_1'])): ?>
                                    <h1 class="catalog-titile">Беседки и малые строения</h1>
                                    <p>
                                        Деревянные беседки являются неотъемлемой частью участка или усадьбы, и делают дачный обиход приятнее, проще и функциональней. Поэтому непременно должны быть удобными и гармонично вписываться в общую планировку участка, продолжая архитектуру и дизайн дома. 
                                        Все беседки от самой маленькой,  до беседки-барбекю строятся из клееного бруса и подойдут для участков любого размера.
                                        Беседка-барбекю включает фундамент, декоративные решетки, наружную и внутреннюю отделку в базовой комплектации и может быть дополнена остеклением. 
                                        Гараж строится по каркасной технологии, оснащен окнами, воротами роллетного типа и подходит для круглогодичного использования. 
                                        Все малые строения возводятся за 5-7 дней и практически сразу могут быть использованы по назначению.

                                    </p>
                                <?php endif; ?>                   


                                <?php if ($dir == '/catalog/besedki-bani-i-sadovye-doma/' && !isset($_GET['PAGEN_1'])): ?>
                                    <h1 class="catalog-titile">Бани - проекты и цены</h1>
                                    <p>
                                        Наши прадеды называли баню второй матушкой неслучайно: по своим целебным свойствам русская баня и впрямь уступает только материнской заботе. Понимая, что мало что может сравниться с эффектом от посещения традиционной бани, люди всё чаще заказывают строительство бань под ключ на своих участках. 

                                        <br/>
                                        Современные технологии позволяют построить недорогую деревянную баню под ключ меньше, чем за неделю. Кроме того, натуральные материалы для строительства стали более огнестойкими и долговечными.  
                                    </p>
                                    <h2 class="catalog-titile">Проекты бань</h2>
                                    <p>Наша компания предлагает своим клиентам проекты деревянных бань под ключ из каркаса и клееного бруса по приемлемым ценам. Ниже Вы найдёте бани с метражом (от 12 до 48 кв. м.) и планировками на любой вкус. Кому-то важна лишь добротная парная с душевой, а кому-то захочется не только хорошо попариться, но и отлично провести время, общаясь с близкими, поэтому среди наших проектов есть модели с жилыми зонами. Такие бани можно использовать как гостевые домики. Предлагаем ознакомиться с проектами бань под ключ в нашем каталоге и приятно удивиться нашим ценам.</p>
                                <?php endif; ?>

                                <?php if ($dir == '/catalog/doma-iz-brusa/' && !isset($_GET['PAGEN_1'])): ?>
                                    <h1 class="catalog-titile text-center text-uppercase">Проекты домов из бруса под ключ и особенности брусового строительства</h1>
                                    <p class="catalog-subtitile-up">
                                     Что нам стоит дом из бруса построить? Нарисуем и… будем возводить поэтапно, придерживаясь технологии. Брусовой дом экологичен, эстетичен и практичен. За счет естественной древесной текстуры он не нуждается во внутренней и внешней отделке. Готовые проекты домов из бруса от компании «Теремъ» - это надежное жилье на дачный сезон или на весь год. Создаем комфорт за пределами города в рамках фиксированного бюджета, без дополнительных трат во время строительства. Подробности о будущем своего загородного дома из бруса Вы всегда можете уточнить у менеджеров.
                                 </p>

                                 
                             <?php endif; ?>

                             <?php if ($dir == '/catalog/sadovye-doma/' && !isset($_GET['PAGEN_1'])): ?>
                                <h1 class="catalog-titile text-center text-uppercase">Садовые дома - проекты и цены</h1>
                                <p>
                                    Садовые дома - компактные здания для сезонного проживания. Проекты компании «Теремъ» предполагают комфортные условия для 1-4 человек в зависимости от планировки. Одноэтажный садовый домик подходит в качестве главного дачного здания на небольших участках, как дополнительная гостевая постройка или летняя кухня. Дизайн фасада выполнен в классическом стиле, который универсально смотрится на любой территории. Звоните, чтобы уточнить подробности строительства садовых домиков на Вашем участке.
                                </p>
                            <?php endif; ?>    

                            <?php if ($dir == '/catalog/klassicheskie-dachnye-doma/' && !isset($_GET['PAGEN_1'])): ?>
                                <h1 class="catalog-titile">Классические дачные дома - проекты и цены</h1>
                                <p>
                                    Классические дачные дома от бестселлеров, до новинок включают как компактные, так и большие строения с несколькими спальнями, просторными гостиными, кухнями и санузлами. 
                                    Все проекты выверены до мельчайших деталей и разработаны по уникальным технологиям компании «Теремъ». Все процессы от закладки фундамента, до внутренней отделки выстроены в определенной последовательности, а использование материалов в домокомплектах, изготовленных на собственном производстве, практически исключает отходы, сокращая издержки и оптимизируя итоговую стоимость дома. Работа квалифицированных постоянных строительных бригад строго регламентирована, что сократило время строительства и позволило поднять качество на принципиально новый уровень,  
                                    Строятся дачные дома из клееного бруса или по каркасной технологии. Все проекты включают фундамент, полноценное крыльцо, а также подготовлены для проведения внутренних коммуникаций. Высота потолков в домах с 1-м этажом – 2.5 м, в двухэтажных домах – 2.4 м. В качестве второго этажа в домах серии «Терем», «Богатырь», «Святогор», «Пересвет», «Илья Муромец» выступает мансарда, что делает дом экономически выгодным и очень уютным. 
                                    Увидеть и оценить дачные дома под ключ можно на выставочной площадке, расположенной по адресу: м. Кузьминки, ул. Зеленодольская, вл42 

                                </p>

                                <div class="innovation">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <p class="text-uppercase title"><big><strong>важно:</strong> базовая комплектация классических дачных домов уже включает: </big></p>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-xs-12 col-md-2 col-sm-4">
                                            <div class="item-inner height-2">
                                                <p>монолитно-ленточный фундамент *</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-2 col-sm-4">
                                            <div class="item-inner height-2">
                                                <p>просторное крыльцо</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-2 col-sm-4">
                                            <div class="item-inner height-2">
                                                <p>высота потолка 2.5 <span>м</span></p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-2 col-sm-4">
                                            <div class="item-inner height-2">
                                                <p>широкая лестница на мансарду с перилами</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-2 col-sm-4">
                                            <div class="item-inner height-2">
                                                <p>филенчатые двери</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-2 col-sm-4">
                                            <div class="item-inner height-2">
                                                <p>комфортная планировка</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p style="margin-top:-30px; font-size: 14px;color: #999;background: transparent;">
                                    * Кроме серии «Лидер»
                                </p>
                                <div class="space"></div>

                            <?php endif; ?>
                            <?php if ($dir == '/catalog/doma-dlya-postoyannogo-prozhivaniya/' && !isset($_GET['PAGEN_1'])): ?>
                                <h1 class="catalog-titile ">Дома для постоянного проживания - проекты и цены</h1>
                                <p>

                                    Зимний дом или коттедж, удобный и комфортный, который не нужно достраивать, по доступной цене – это мечта, которую воплотили архитекторы и технологи компании «Теремъ», собрав все лучшее в своих проектах и сделав их дешевле и доступней, благодаря отлаженным строительным технологиям! Коттеджи и дома для постоянного проживания созданы с учётом норм для жилых помещений всесезонной эксплуатации. В строительстве используются современные материалы, изготовленные на собственном производстве из первосортного сырья, объемы закупок которого позволили существенно снизить итоговую стоимость, а качество поднять еще выше. В базовой комплектации есть всё необходимое: фундамент на глубину промерзания, клееный брус от 180 мм или каркас от 200 мм с шагом 400 мм, обшивка сайдингом, внутренняя отделка, встроенная вентиляция, современное проверенное утепление, двойные стеклопакеты и многое другое. Архитектура и дизайн проектов «Теремъ» разработана на основе пожеланий и потребностей покупателей, поэтому каждый может подобрать жильё по вкусу: от домов в русском стиле, до больших коттеджей, выполненных в европейских традициях, с просторными гостиными и кухнями, несколькими спальнями и санузлами, кабинетом, топочной, кладовыми и гардеробными. К домам для постоянного проживания предложен пакет инженерных коммуникаций, который позволит построить дом «под ключ» с единым контролем качества и полной гарантийной ответственность. Дом под ключ - это  богатая базовая комплектация домов для постоянного проживания, отопление, водоснабжение, электричество и канализация. Готовые коттеджи и дома для постоянного проживания под ключ возводятся за 25-60 дней.

                                </p>

                                

                                <div class="space"></div>

                            <?php endif; ?>
                            <?php if ($dir == '/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/' && !isset($_GET['PAGEN_1'])): ?>
                                <h1 class="catalog-titile">Дачные коттеджи - проекты и цены</h1>
                                <p>
                                    Дачные коттеджи предназначены для сезонного проживания, но при желании могут быть доработаны в зимние дома. 
                                    Все проекты разработаны с учётом мирового опыта, тенденций в строительстве и норм СНиП, возводятся в каркасе и в клееном брусе. При этом цена беспрецедентно низкая за счет авторской технологии компании «Теремъ», которая годами оттачивалась в категории дачных домов и была успешно адаптирована в классе коттеджей.  
                                    Строительством коттеджей под ключ занимаются постоянные квалифицированные бригады, чья работа выверена на каждом этапе. Все детали изготовлены из первоклассного сырья на собственном производстве компании, промаркированы и собраны в комплекты в определённой последовательности, что позволяет строить дома не только быстро, но отслеживать качество работ и материалов. 
                                    Проекты коттеджей под ключ продуманы до мелочей: высокие потолки, удобные лестницы, просторные гостиные, совмещенные с кухнями, два и больше санузлов, отдельные спальни и другие  дополнительные функциональные пространства. При желании можно пристроить террасу или гараж.

                                </p>
                            <?php endif; ?> 
                            <?php if ($dir == '/catalog/originalnye-dachnye-doma-vityaz-odnoetazhnye-i-so-vtorym-svetom/' && !isset($_GET['PAGEN_1'])): ?>
                                <h1 class="catalog-titile">Дома серии «Витязь» - проекты и цены</h1>
                                <p>
                                    Серия «Витязь» - это одноэтажные, с необычной планировкой дома, где есть всё необходимо для комфортной загородной жизни: гостиная, совмещённая с кухней, 2 комнаты, удобный холл и санузел. Приятный бонус - второй свет, который расширяет возможности внутреннего оформления и декорирования. 
                                    Эти дачные домики подойдут тем, кто предпочитает экономию на всех этапах от строительства, до эксплуатации, вплоть до ухода и уборки помещений. А также для тех, кто только начал осваивать свой участок или подбирает вариант для второго или гостевого дома.
                                    Дома «Витязь» строятся из клееного бурса или в каркасе, и рассчитаны на круглогодичное проживание при дополнительном утеплении. За счет небольшого веса могут возводиться на любом типе грунта, а малая площадь застройки даёт возможность ставить такие дома даже на небольших участках. 

                                </p>
                            <?php endif; ?> 

                            <?php if ($dir == '/catalog/kirpichnie-doma/' && !isset($_GET['PAGEN_1'])): ?>
                                <h1 class="catalog-titile text-center text-uppercase">Кирпичные дома под ключ: строим комфортные жилые здания</h1>
                                <p class="catalog-subtitile-up">Дома из кирпича компании «Теремъ» - универсальные готовые проекты, которые можно возвести в удобные и приемлемые сроки. Планировка проектов создавалась с учетом реальных потребностей людей, чтобы каждый день в таком жилье был проведен с комфортом. Бюджет фиксируется в начале строительства и не меняется в течение работ. Обратившись в компанию, вы получаете полный комплекс услуг, а кирпичный дом под ключ станет готовым жильем для всей семьи на долгие годы.</p>  
                                
                            <?php endif; ?> 

                            <?php if ($dir == '/catalog/dvukhetazhnye-doma-iz-brusa/' && !isset($_GET['PAGEN_1'])): ?>
                                <h1 class="catalog-titile text-center text-uppercase">Двухэтажные дома из бруса: как строит компания «Теремъ»</h1>
                                <p class="catalog-subtitile-up">
                                    Готовые проекты двухэтажных брусовых домов подходят для дачного отдыха и для жизни весь год, в том числе зимой. Планировка зданий задает удобные условия для проживания владельцев и гостей, в домах достаточно места для всей семьи. Подробности уточняйте у менеджеров компании «Теремъ», звоните ежедневно с 10:00 до 20:00.
                                </p>
                            <?php endif; ?>    

                            <?php if ($dir == '/catalog/karkasnye-doma/' && !isset($_GET['PAGEN_1'])): ?>
                                <h1 class="catalog-titile text-center text-uppercase">Каркасные дома: цены, проекты и особенности строительства</h1>
                                <p>
                                    Каркасные дома под ключ - проекты, которые компания «Теремъ» построит в краткие сроки. В среднем, строительные работы занимают не более двух месяцев, заселяться можно сразу, ведь здание не дает усадку. Деревянные дома по каркасной технологии строительства возводятся без привязки к сезону благодаря скорости работ: материалы готовы к монтажу, а сложные узлы собираются под контролем на производстве. Здания, представленные в каталоге, подходят как для сезонного, так и для постоянного проживания. Звоните, чтобы узнать подробности о каждом проекте каркасных домов.
                                </p>
                                
                                <div class="space"></div>
                            <?php endif; ?>    
                        </div>
                    </div>
                </div>



                <!-- INCLUDE POSTO PROJ -->
                <?php if ($dir == '/catalog/doma-dlya-postoyannogo-prozhivaniya/'): ?>
                    <div class="flex-reversed catlog-text">
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-70-do-100-kv-m/">
                                            <img src="/upload/catalog/Dobrynia_3_button.jpg" alt="Классические дачные дома">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase" style="line-height: 26px;">
                                            <span class="title_cat_new">Дома для постоянного проживания</span>
                                            <br/><span class="title_cat_two">от 70 до 100 кв. м</span>
                                        </h4>
                                        
                                        <p>
                                            Тёплые и уютные дома с многофункциональной планировкой и полноценной базовой комплектацией порадуют своих будущих хозяев доступными ценами, эргономичностью и высоким качеством исполнения. 
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-70-do-100-kv-m/"></a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-100-do-150-kv-m/">
                                            <img src="/upload/catalog/varyag_3_283x283.jpg" alt="Дачные коттеджи">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase" style="line-height: 26px;">
                                           <span class="title_cat_new">Дома для постоянного проживания</span>
                                           <br/><span class="title_cat_two">от 100 до 150 кв. м.</span>
                                       </h4>
                                       <p>
                                           Идеальный вариант для семейной жизни за городом круглый год – дома до 150 кв.м. Отличная базовая комплектация и прекрасно организованное внутреннее пространство.
                                       </p>
                                   </div>
                                   <a class="all-item-link" href="/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-100-do-150-kv-m/"></a>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-promotion ( desgin-2 &amp;&amp; one-column ) + my-margin">
                                <div>
                                    <a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-150-do-400-kv-m/">
                                        <img src="/upload/catalog/400.jpg" alt="Дома для постоянного проживания">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase" style="line-height: 26px;">
                                        <a href="#" class="text-uppercase">
                                           <span class="title_cat_new">Дома для постоянного проживания</span>
                                           <br/><span class="title_cat_two">от 150 до 400 кв.м.</span>
                                       </a>
                                   </h4>
                                   <p>
                                       Большие дома  для лучшего отдыха и комфортной жизни круглый год – это яркий дизайн, просторные террасы, прекрасные балконы, барбекю-зоны и чудесные комнаты для всей семьи и друзей. 
                                   </p>
                               </div>
                               <a class="all-item-link" href="/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-150-do-400-kv-m/"></a>
                           </div>
                       </div>
                   </div>
               </div>
           <?php endif; ?>
           <!-- INCLUDE POSTO PROJ -->

       </div>






   </section>



   <section class="content" role="content">
    <div class="container">
        <?php if ($page == '/catalog/vse-doma/' || $page == '/catalog/'): ?>
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12" id="category">
                    <div class="flex-reversed">
                        <!--OLD catalog list-->
                        <!--small villages -->
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/malye-stroeniya-besedki/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-1.jpg" alt="беседки">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Малые строения, беседки
                                        </h4>
                                        <p>
                                            Недорогие беседки из клееного бруса, беседки-барбекю с внешней и внутренней отделкой, и каркасные гаражи с рольворатами.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/malye-stroeniya-besedki/"></a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/besedki-bani-i-sadovye-doma/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-2.jpg" alt="Бани">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Бани
                                        </h4>
                                        <p>
                                            Традиционные бани из клееного бруса или каркаса с удобными парными, предбанниками, комнатами для отдыха и душевыми. Строятся за 15-25 дней, включая внутреннюю отделку, и не требуют внешней отделки.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/besedki-bani-i-sadovye-doma/"></a>
                                </div>
                            </div>
                        </div><!--end-->
                        <!--vitaz and gardern-->
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/sadovye-doma/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-3.jpg" alt="Садовые дома">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Садовые дома
                                        </h4>
                                        <p>
                                            Компактные и удобные садовые дома для дачного отдыха. Дома строятся по каркасной технологии или из клеёного бруса за 5-7 дней.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/sadovye-doma/"></a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/originalnye-dachnye-doma-vityaz-odnoetazhnye-i-so-vtorym-svetom/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-4.jpg" alt="Оригинальные дачные дома">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Оригинальные дачные дома
                                        </h4>
                                        <p>
                                            Одноэтажные дома - оптимальный вариант при небольшом бюджете, когда предпочтение отдано экономичному в содержании и эксплуатации жилью, для проживания нескольких человек любого возраста.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/originalnye-dachnye-doma-vityaz-odnoetazhnye-i-so-vtorym-svetom/"></a>
                                </div>
                            </div>
                        </div><!--end-->
                        <!--classic and dacha-->
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/klassicheskie-dachnye-doma/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-5.jpg"  alt="Классические дачные дома">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Классические<br/>дачные<br/>дома
                                        </h4>
                                        <p>
                                            Самые популярные дома для дачи на любой вкус и бюджет, с полноценной базовой  комплектацией (от фундамента, до внутренней отделки). Возводятся за 30 дней и готовы к эксплуатации в день сдачи.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/klassicheskie-dachnye-doma/"></a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-6.jpg"   alt="Дачные коттеджи">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Дачные коттеджи
                                        </h4>
                                        <p>
                                            Деревянные коттеджи по цене дачи: удобные планировки,  качественные материалы и современная архитектура.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/"></a>
                                </div>
                            </div>
                        </div><!--end-->
                        <!--post projivanie-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="my-promotion ( desgin-2 && one-column ) + my-margin">
                                    <div>
                                        <a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-7.jpg" alt="Дома для постоянного проживания">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            <a href="#" class="text-uppercase">Дома <br/>для постоянного<br/>проживания</a>

                                        </h4>
                                        <p><span class="title_cat_two" style="padding-top: 0px !important;">от 70 до 400 кв.м.</span><br/></p>
                                        <p>
                                            Дома и коттеджи для круглогодичного проживания в условиях русской зимы, где есть всё необходимое и по самой выгодной цене. 
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/doma-dlya-postoyannogo-prozhivaniya/"></a>
                                </div>
                            </div>
                        </div>
                        
                        

                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/services/dostroika/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-12.jpg"  alt="Услуги по реконструкции">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Услуги<br>по ремонту<br>и реконструкции
                                        </h4>
                                        <p>
                                            Каждый заказ по достройке и реконструкции - индивидуален. Разработка проекта, подбор материалов и расчет стоимости производятся с учетом личных пожеланий клиентов.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/services/dostroika/"></a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/services/the-estates-catalogue/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-13.jpg" alt="Благоустройство участка">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Благоустройство<br>участка
                                        </h4>
                                        <p>
                                            Каждому хочется жить в прекрасном райском уголке 
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/services/the-estates-catalogue/"></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="my-promotion ( desgin-2 && one-column ) + my-margin">
                                    <div>
                                        <a href="/services/individual_projects/">
                                            <img src="/upload/IND.jpg" alt="Дома по индивидуальным проектам">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            <a href="/services/individual_projects/" class="text-uppercase">Архитектурное<br/>бюро</a>
                                        </h4>
                                        <p>
                                            Мы тщательно просчитываем все, начиная от выбора фундамента, материалов, размера, планировки, заканчивая инженерными коммуникациями и благоустройством участка.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/services/individual_projects/"></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <h2 class="title-white text-uppercase">Дома по технологиям:</h2>
                            </div>
                        </div>
                        <!-- ZGOLOVOK-->
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/karkasnye-doma/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-8.jpg" alt="Каркасные дома">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Все каркасные дома
                                        </h4>
                                        <p>
                                            Современные технологии производства и строительства каркасных домов позволяют не уступать домам из кирпича или бетона в надежности, прочности и долговечности. При этом каркасные дома обладают рядом преимуществ.  
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/karkasnye-doma/"></a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/doma-iz-brusa/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-9.jpg" alt="Дома из бруса">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Все Дома из бруса
                                        </h4>
                                        <p>
                                            Дома из бруса – это воплощение удобства, уюта, красоты и надежности. Такие дома представлены в каталоге компании в самом широком ассортименте. Разные размеры, планировки, детали – вы можете выбрать дом, который понравится всей семье.  
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/doma-iz-brusa/"></a>
                                </div>
                            </div>
                        </div><!--end-->
                        <!--services-->
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/dvukhetazhnye-doma-iz-brusa/">
                                            <img src="/upload/Buttons_2BRUS.jpg"  alt="Двухэтажные дома из бруса">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Все двухэтажные <br/>дома из бруса
                                        </h4>
                                        <p>
                                            Дома из бруса отличаются высокой экологичностью и пользуются все большей популярностью у покупателей. Наши архитекторы разработали новые проекты двухэтажных домов из бруса.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/dvukhetazhnye-doma-iz-brusa/"></a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 + my-margin">
                                    <div>
                                        <a href="/catalog/kirpichnie-doma/">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-10.jpg" alt="Кирпичные дома">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase">
                                            Все кирпичные дома
                                        </h4>
                                        <p>
                                            При строительстве кирпичных домов применяется технология, которая никогда не утратит своей актуальности, потому что она проверена веками.
                                        </p>
                                    </div>
                                    <a class="all-item-link" href="/catalog/kirpichnie-doma/"></a>
                                </div>
                            </div>
                        </div>

                        

                        <!--end-->
                        <!--services-->
                        <!--end-->
                        <!--end list-->
                    </div>
                </div>

            </div>


            <?php if ($page == '/catalog/'): ?>
                <div class="row catlog-text">
                    <div class="col-xs-12 col-md-12 col-sm-12">
                        <div class="catalog-description + my-margin bottom-text">
                            <h1 class="catalog-titile">Теремъ: деревянные дома</h1>
                            <p>
                                Строительная компания «Теремъ» - один из лидеров рынка загородного домостроения. Мы занимаемся проектированием, производством и строительством домов из бруса, каркасных домов и кирпичных строений с учетом русских традиций домостроения и применения новейших технологий.
                            </p>
                            <p>«Теремъ» строит быстро и качественно. Благодаря прорыву современных технологий, строительный процесс больше не затягивается на долгие годы. Деревянные дома строятся из современных материалов, сделанных на собственном производстве из экологически чистой, качественной древесины.</p>
                            <p>Мы предлагаем большой выбор проектов деревянных домов. Дом в Тереме - это оптимальная комплектация, продуманная планировка, современный внешний вид строения, а так же фиксированная стоимость, что немаловажно при заказе полноценного проекта под ключ.</p>
                            <p>Индивидуальный подход к каждому клиенту – это наш принцип. Архитекторы компании спроектируют деревянные дома согласно Вашим желаниям и потребностям, внесут изменения в типовой проект, предложат идеальное комплексное решение.</p>

                            <p>«Теремъ» так же готов предоставить полный комплекс услуг для загородной жизни:</p>
                            <ul>
                                <li>
                                    <p>Необходимые инженерные коммуникации для дома: электрика, отопление, септики, водоподъем</p>
                                </li>
                                <li>
                                    <p>Реконструкция, достройка или ремонт существующих строений</p>
                                </li>
                                <li>
                                    <p>Благоустройство участка, ландшафтный дизайн</p>
                                </li>
                                <li>
                                    <p>Ограждение участка – заборы из евроштакетника, профнастила, сетки-рабицы</p>
                                </li>
                                <li>
                                    <p>Пристройка террас и веранд к существующим строениям</p>
                                </li>
                            </ul>
                            <p>Для удобства покупателей мы предоставляем услуги  по оформлению кредитов и страховых полисов.</p>
                            <p>В нашей компании внимание уделяется каждому покупателю. Нам важен хороший результат, для его достижения мы внимательно отслеживаем все этапы строительства, занимаемся гарантийным обслуживанием домов после сдачи объектов.</p>
                            <p>Компания «Теремъ» следует простому принципу: делать все четко и последовательно. Таким образом мы обеспечиваем нашему потребителю качество при доступной цене.</p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>





        <?php endif; ?>



        <?php if ($page == '/catalog/vse-doma/' || $page == '/catalog/'): ?>
            <noindex> 
            <?php endif; ?>


            <div class="flex-reversed">
                <div class="row" id="items">
                    <?php $s = 10; ?>
                    <?php foreach ($arResult["ITEMS"] as $k => $arItem): ?>
                        <?php
                        $series_str = NULL;
                        $technology_str = NULL;
                        $cost_str = NULL;
                        $square_str = NULL;
                        $sizers_str = NULL;
                        ?>
                        <?php $SERIES[] = $arItem['PROPERTIES']['SERIES']['VALUE_XML_ID'] . "/" . $arItem['PROPERTIES']['SERIES']['VALUE']; ?>
                        <?php $series_str = $arItem['PROPERTIES']['SERIES']['VALUE_XML_ID']; ?>

                        <?php foreach ($arItem['PROPERTIES']["TEHNOLOGY_FILTER"]["VALUE_XML_ID"] as $k => $propID): ?>
                            <?php $technology_str = $technology_str . " technology_" . $propID; ?>
                        <?php endforeach; ?>

                        <?php foreach ($arItem['PROPERTIES']["SIZ_FILTR"]["VALUE_XML_ID"] as $k => $propID): ?>
                            <?php $sizers_str = $sizers_str . " sizers_" . $propID; ?>
                        <?php endforeach; ?>

                        <?php foreach ($arItem['PROPERTIES']["PRICE_FILTER"]["VALUE_XML_ID"] as $k => $propID): ?>
                            <?php $cost_str = $cost_str . " cost_" . $propID; ?>
                        <?php endforeach; ?>

                        <?php foreach ($arItem['PROPERTIES']["SIZE_FILTER"]["VALUE_XML_ID"] as $k => $propID): ?>
                            <?php $square_str = $square_str . " square_" . $propID; ?>
                        <?php endforeach; ?>

                        <?php if ($arItem["PROPERTIES"]["LIDER"]["VALUE"] == "Yes"): ?>
                            <div sortKey="2"  class="col-xs-12 col-md-12 col-sm-12 item series_<?= $series_str ?> <?= $technology_str ?> <?= $cost_str ?> <?= $square_str ?> <?= $sizers_str ?>">
                                <div class="my-promotion desgin-5 + my-margin">
                                    <div class="flex-row">
                                        <div>
                                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                                <?php if ($arItem['DETAIL_PICTURE']['SRC']): ?>
                                                    <img src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>" alt="<?= $arItem["NAME"] ?>">
                                                <?php else: ?>
                                                    <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg">
                                                <?php endif; ?>

                                            </a>
                                        </div>

                                        <div>
                                            <div>
                                                <div class="description">
                                                    <h4 class="text-uppercase">
                                                        <?= $arItem["NAME"] ?><br/>
                                                        <?= $arItem["PROPERTIES"]['SIZER']['VALUE'] ?><br/>
                                                        <span> ОТ <?= $arItem["PROPERTIES"]['PRICE_FROM']['VALUE'] ?> Т.Р.</span>
                                                    </h4>
                                                </div>
                                                <div class="options">
                                                    <div>
                                                        <ul>
                                                            <li><p><b>Варианты исполнения</b></p></li>
                                                            <?php foreach ($arItem["OPT"] as $k => $Item): ?>                                           
                                                                <li><p><?= $Item["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] ?>:<?= $Item["PRICE"]["PROPERTY_PRICE_1_VALUE"] ?></p>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                    <div>
                                                        <a href="#" class="advice" data-target="#formAdvice" data-whatever="<?= $arItem["NAME"] ?>" data-toggle="modal">Получить бесплатную консультацию</a>
                                                        <a href="#" class="compare">Получить бесплатную консультацию</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="flex-row">
                                                    <?php if (count($arItem['PROPERTIES']["FILES"]["VALUE"]) <= 0): ?>
                                                        <div>
                                                            <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg">
                                                        </div>
                                                        <div>
                                                            <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg">
                                                        </div>
                                                    <?php else: ?>
                                                        <?php foreach ($arItem['PROPERTIES']["FILES"]["VALUE"] as $k => $img_id): ?>
                                                            <div>
                                                                <img src="<?= CFile::GetPath($img_id); ?>">
                                                            </div>
                                                            <?php if (count($arItem['PROPERTIES']["FILES"]["VALUE"]) == 1): ?>
                                                                <div>
                                                                    <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg">
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="all-item-link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"></a>
                                </div>
                            </div>
                        <?php else: ?>
                            <div sortKey="2" sort=<?= $s ?> class="col-xs-12 col-md-6 col-sm-12 shuffle <?php if ($arItem['ID'] == '109'): ?>item series_s18<?php endif; ?> item series_<?= $series_str ?> <?= $technology_str ?> <?= $cost_str ?> <?= $square_str ?> <?= $sizers_str ?>">


                             <?php if (count($arItem['PROPERTIES']["FILES"]["VALUE"]) === 1 && $arItem['PROPERTIES']["FILES"]["VALUE"][0] != NULL): ?>
                                 <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">
                                 <?php else: ?>
                                    <div class="my-promotion desgin-4 my-margin hover-grey new-single-item">
                                    <?php endif; ?>



                                    <div class="flex-row">
                                        <div>
                                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                                <?php if ($arItem['PREVIEW_PICTURE']['SRC']): ?>
                                                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem["NAME"] ?>">
                                                <?php else: ?>
                                                    <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg">
                                                <?php endif; ?>
                                            </a>
                                        </div>

                                        <div>
                                            <div class="description">
                                                <h4 class="text-uppercase">
                                                    <?= $arItem["NAME"] ?><br/>

                                                    <?
                                                    echo preg_replace('(х)','<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>', $arItem["PROPERTIES"]['SIZER']['VALUE']);

                                                    ?>
                                                    <!--<span> ОТ <?= $arItem["PROPERTIES"]['PRICE_FROM']['VALUE'] ?> Т.Р.</span>-->
                                                </h4>
                                                <?php foreach ($arItem['OPT'] as $k => $Item): ?>
                                                    <?php $CODE_1C = $arItem["OPT"][0]['PROPERTY_CODE_1C_VALUE']; ?>
                                                <?php endforeach; ?>
                                                <a href="#" class="advice" data-target="#formAdvice" data-code="<?= $CODE_1C ?>" data-price="<?= $arItem['OPT'][0]["PRICE"]["PROPERTY_PRICE_1_VALUE"] ?>000" data-whatever="<?= $arItem["NAME"] ?> <?= $arItem["PROPERTIES"]['SIZER']['VALUE'] ?>" data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                            </div>
                                            <div class="options">
                                                <div>
                                                    <div class="description-title"><?= $arItem["PROPERTIES"]['PODPIS']['VALUE'] ?></div>
                                                    

                                                    <p>
                                                        <b>Варианты исполнения:</b>
                                                    </p>
                                                    <?php
                                                        //echo "<pre>";
                                                        //var_dump($arItem['OPT']);
                                                    ?>

                                                    <?php foreach ($arItem['OPT'] as $k => $Item): ?>
                                                        <p><?= $Item["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] ?>: 
                                                        <strong>от 
                                                          <?php if($Item["PRICE"]["PROPERTY_OPTIMAL_PRICE_VALUE"] > 0 && $Item["PRICE"]["PROPERTY_OPTIMAL_PRICE_VALUE"] != NULL):?>  
                                                            <?php echo preg_replace("/000$/", "$1", number_format($Item["PRICE"]["PROPERTY_OPTIMAL_PRICE_VALUE"], 3, ' ', ' ')); ?> т.р.
                                                          <?php else:?>  
                                                            <?php echo preg_replace("/000$/", "$1", number_format($Item["PRICE"]["PROPERTY_PRICE_1_VALUE"], 3, ' ', ' ')); ?> т.р.
                                                          <?php endif;?>          
                                                            
                                                        </strong>
                                                        </p>
                                                    <?php endforeach; ?>
                                                </div>
                                                <!--<a href="#" class="compare">Получить бесплатную консультацию</a>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-row">
                                        <?php if (!$arItem['PROPERTIES']["FILES"]["VALUE"]): ?>
                                            <div>
                                                <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg">
                                            </div>
                                            <div>
                                                <img src="/bitrix/templates/.default/assets/img/yeplo1.jpg">
                                            </div>
                                        <?php endif; ?>
                                        <?php $cnt = 0; ?>
                                        <?php if ($arItem['PROPERTIES']["FILES"]["VALUE"]): ?>
                                            <?php foreach ($arItem['PROPERTIES']["FILES"]["VALUE"] as $k => $img_id): ?>
                                                <div>
                                                    <img src="<?= $img_id['src'] ?>">
                                                </div>
                                                <?php $cnt ++; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <?php if ($cnt == 1): ?>

                                        <?php endif; ?>
                                    </div>
                                    <a class="all-item-link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" ></a>

                                    <div class="item-show-more">
                                        <div class="hover-block">
                                            <div>
                                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                                    <div><i><img src="/bitrix/templates/.default/assets/img/cursor.png" alt=""></i></div>
                                                    <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                                </a>
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div>
                            </div>
                        <?php endif; ?>
                        <?php $s++; ?>
                    <?php endforeach; ?>  
                </div>
            </div>
            <?php if ($page != '/catalog/'): ?>
                <?= $arResult["NAV_STRING"] ?>
            <?php endif; ?>


            <?php if ($dir == '/catalog/kirpichnie-doma/' && !isset($_GET['PAGEN_1'])): ?>
                <section class="content" role="content">
                    <div class="container">
                        <div class="row catlog-text">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="catalog-description + my-margin bottom-text new-bottom">
                                    <h3 class="text-uppercase">Строительство кирпичных домов</h3>
                                    <p class="margin">Точные сроки строительство дома из кирпича определяются масштабом и сложностью проекта. В среднем же, коробку дома реально возвести за 4-5 месяцев, а при возведении кирпичного дома под ключ придется подождать около года. Технология строительства без учета дополнительных инженерных и отделочных работ предполагает 4 ключевых этапа:</p>
                                    <ol>
                                        <li>
                                            <p>устройство фундамента</p>
                                        </li>
                                        <li>
                                            <p>возведение стен и перегородок</p>
                                        </li>
                                        <li>
                                            <p>монтаж кровли</p>
                                        </li>
                                        <li>
                                            <p>установка окон и входной двери.</p>
                                        </li> 
                                        
                                    </ol>
                                    <p>Дополнительно компания «Теремъ» подготовит кирпичный дом под ключ: проведем трубопровод, канализацию, проложим проводку. Специалисты предложат варианты ландшафта и реализуют проект. В компании «Теремъ» каждый клиент может обустроить свою загородную жизнь на 100%.</p>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-6.png" class="full-width" alt="На фото дом из кирпича Герцог, 8.5х9.5 м.">
                                            <p class="img-description"><i>На фото дом из кирпича Герцог, 8.5х9.5 м.</i></p>
                                        </div>
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-7.png" class="full-width" alt="На фото дом из кирпича Премьер-3, 8.4х9.2 м.">
                                            <p class="img-description"><i>На фото дом из кирпича Премьер-3, 8.4х9.2 м.</i></p>
                                        </div>
                                    </div>
                                    
                                    <h3 class="text-uppercase">3 весомых преимущества компании «Теремъ»</h3>
                                    <p>Архитекторы, строители и инженеры компании «Теремъ» обеспечивают полноценное строительство, что называется под ключ. Все из одних рук под единым контролем качества и полной гарантийной ответственностью. </p>
                                    <p>Мы уверены в качестве строительных материалов и структурных элементов, именно поэтому вы получаете гарантию на кирпичный дом до 20 лет.</p>
                                    <p>Отработанная за годы работы логистика позволяет поставлять строительные материалы в срок, без задержек. Инструменты, материалы, источники энергии – у строителей есть все необходимое, как следствие, дом возводится по графику.</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <?php if ($dir == '/catalog/sadovye-doma/' && !isset($_GET['PAGEN_1'])): ?>
                <section class="content" role="content">
                    <div class="container">
                        <div class="row catlog-text">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="catalog-description + my-margin bottom-text new-bottom">
                                    <h3 class="text-uppercase">Особенности конструкции в недорогих садовых домах</h3>
                                    <p>Каждый проект садового дома компании «Теремъ» обеспечивает жильцам комфорт на протяжении дачного сезона. Несмотря на различия планировок в разных ценовых вариантах, в помещении достаточно места для всего необходимого, нет ощущения недостатка пространства. В садовых домиках окна расположены продуманно, чтобы помещение легко проветривалось, не застаивался воздух. Аромат натуральной древесины добавит свежести в особо жаркие дни. </p>
                                    <p class="margin">
                                    	Садовый дом под ключ в компании «Теремъ» это:
                                    </p>
                                    <ul>
                                        <li>
                                            <p>готовый проект жилого здания;</p>
                                        </li>
                                        <li>
                                            <p>функциональное строение на весь дачный сезон;</p>
                                        </li>
                                        <li>
                                            <p>компактная постройка в классическом стиле.</p>
                                        </li>
                                    </ul>
                                    <p>Садовый домик недорого можно построить по каркасной технологии, цена такого проекта наиболее низкая. Проекты в исполнении из клееного бруса обойдутся дороже, но их стоимость также щадит кошелек.</p>
                                    <p>Здание строится быстро: строительство садового дома укладывается в 1 неделю, по завершению можно заселяться. Каркасная технология не предполагает усадки, а при строительстве из клееного бруса усадка минимальна и не превышает 2%.</p>
                                    <p>Внутреннюю площадь садовых домов дополняет маленькое крыльцо, на котором  свободно разместится небольшой столик с парой кресел. В таком месте уютно завтракать и проводить погожие вечера на свежем воздухе.</p>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-31.jpg" class="full-width" alt="На фото каркасный садовый дом Малыш 2, 4х4 м.">
                                            <p class="img-description"><i>На фото каркасный садовый дом Малыш 2, 4х4 м.</i></p>
                                        </div>
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-32.jpg" class="full-width" alt="На фото каркасный садовый дом Малыш 6, 6х6 м.">
                                            <p class="img-description"><i>На фото каркасный садовый дом Малыш 6, 6х6 м.</i></p>
                                        </div>
                                    </div>
                                    
                                    <h3 class="text-uppercase">5 причин построить садовый дом с компанией «Теремъ»</h3>
                                    <p>Главное преимущество садового домика – компактное размещение на участке. Небольшие размеры постройки гармонично сочетаются со всем архитектурным ансамблем территории. Если вы хотите хороший дом – обращайтесь в компанию «Теремъ».</p>
                                    <p class="margin">
                                    	Компанию «Теремъ» выбирают потому, что здесь:
                                    </p>
                                    <ol>
                                        <li>
                                            <p>Оптимальная стоимость каждого дома.</p>
                                        </li>
                                        <li>
                                            <p>Фиксированная цена на весь период строительства, нет дополнительных вложений.</p>
                                        </li>
                                        <li>
                                            <p>Продуманные строительные проекты, которые представлены на выставке.</p>
                                        </li>
                                         <li>
                                            <p>Результат соответствует Вашим ожиданиям по срокам и качеству.</p>
                                        </li>
                                        <li>
                                            <p>Гарантия на каждый проект садовых домов до 10 лет.</p>
                                        </li>
                                    </ol>
                                    <p>Подробности строительства садовых домиков под ключ вы ежедневно можете уточнить у менеджеров компании «Терем».</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>



            <?php if ($dir == '/catalog/karkasnye-doma/' && !isset($_GET['PAGEN_1'])): ?>
                <section class="content" role="content">
                    <div class="container">
                        <div class="row catlog-text">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="catalog-description + my-margin bottom-text new-bottom">
                                    <h3 class="text-uppercase">Технология строительства каркасных домов и ее особенности</h3>
                                    <p class="margin">Каркасный дом весит меньше других строений: дерево само себе легкое, к тому же, 70% толщины стен занимает утеплитель. Мощной основы под такое здание не требуется. Наиболее оптимальный фундамент каркасного дома под ключ – свайный, который подготавливается за 1-2 дня. Сваи выгодны своей универсальностью, они устанавливаются на участках:</p>
                                    <ul>
                                        <li>
                                            <p>со сложной неустойчивой почвой;</p>
                                        </li>
                                        <li>
                                            <p>с неглубоким расположением грунтовых вод;</p>
                                        </li>
                                        <li>
                                            <p>с ломаным ландшафтом – на склонах.</p>
                                        </li>
                                    </ul>
                                    <p class="margin">Преимущества свайного фундамента - минимальные затраты времени и усилий, а несущей способности такой основы с запасом хватает для каждого проекта каркасного дома. Скорость установки свайного фундамента высокая, строить начинаем сразу после возведения свай. В каркасных домах выстраиваем замкнутую обвязку из профильной трубы 80х80 или 80х120. Чтобы пол был теплым, конструкция создается так:</p>
                                    <ul>
                                        <li>
                                            <p>между балками первого этажа закладывается черновой пол ОСП 6 мм;</p>
                                        </li>
                                        <li>
                                            <p>кладется ветровлагозащитная мембрана;</p>
                                        </li>
                                        <li>
                                            <p>помещается утеплитель, который сверху накрывается пароизоляцией;</p>
                                        </li>
                                        <li>
                                            <p>закладываются доски пола.</p>
                                        </li>
                                    </ul>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-20.jpg" class="full-width" alt="На фото дачный каркасный дом Малыш-5 6х5 м">
                                            <p class="img-description"><i>На фото дачный каркасный дом Малыш-5 6х5 м.</i></p>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-21.jpg" class="full-width" alt="Постройка второго этажа каркасного дома">
                                            <p class="img-description"><i>Постройка второго этажа каркасного дома.</i></p>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-22.jpg" class="full-width" alt="Постройка каркасного дома. Возведение кровли">
                                            <p class="img-description"><i>Постройка каркасного дома. Возведение кровли.</i></p>
                                        </div>
                                    </div>
                                    <p>По окончанию работ над полом, строим каркас. Расстояние между вертикальными стойками из пиломатериалов 400 или 600 мм, отмеряется в зависимости от проекта. Жесткость конструкции обеспечивают укосины и перемычки, поэтому здание получается прочным. </p>
                                    <p class="margin">Когда каркас готов, возводим утепленные стены и перекрытия, которые представляют собой особую конструкцию, состоящую из нескольких слоев. Утепление стен производим в 3 этапа:</p>
                                    <ol>
                                        <li>
                                            <p>Пространство между стойками заполняется утеплителем. Швы делаются в разбежку – щели в местах стыка закрываются еще одним слоем, в результате холодный воздух не попадает в помещение. </p>
                                        </li>
                                        <li>
                                            <p>Полотно утеплителя закрывается парозащитной пленкой и мембранами для защиты от влаги, чтобы утепление было долговечным и эффективным. </p>
                                        </li>
                                        <li>
                                            <p>Стены обшиваются плитой ОСБ или обшивкой 12,5 мм, в зависимости от проекта.</p>
                                        </li>
                                    </ol>
                                    <p>Завершает строительство облицовка внешних стен. Здания для сезонного проживания отделываются обшивкой 12,5 мм, для постоянного – сайдингом. Фасад каркасного дома не отличается от других деревянных и даже кирпичных домов, если выбрать соответствующий декор. Оформить внешние стены каркасного здания можно как в рамках готового проекта, так и под индивидуальные предпочтения – термопанелями с клинкерной плиткой, декоративными плитами или фальш-брусом.</p>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="mr-top-arr"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <img src="/bitrix/templates/.default/assets/img/expert/sep2.png" alt="arrow-left" class="full-width">
                                        </div>
                                        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
                                            <p class="attantion-quote">В зависимости от проекта строительство занимает 1-2 месяца. Отличаются каркасные дома и невысокой стоимостью.</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <img src="/bitrix/templates/.default/assets/img/expert/sep1.png" alt="arrow-right" class="full-width">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="mr-bottom-arr"></div>
                                        </div>
                                    </div>
                                    <h3 class="text-uppercase">Преимущества каркасных домов под ключ от компании «Теремъ»</h3>
                                    <p class="margin">Технологии производства и строительства в компании «Теремъ» позволяют заказать каркасный дом высокой надежности, который прослужит долго. В каталоге предлагается более 70 готовых проектов, среди которых всегда найдется подходящий вариант под личные предпочтения каждого клиента. Не нашли подходящего варианта? Строительная компания «Теремъ» поможет построить дом Вашей мечты по индивидуальным критериям. Проекты каркасных домов это:</p>
                                    <ul>
                                        <li>
                                            <p>справедливая цена постройки здания; </p>
                                        </li>
                                        <li>
                                            <p>высокая скорость строительства;</p>
                                        </li>
                                        <li>
                                            <p>теплая и комфортная постройка.</p>
                                        </li>
                                    </ul>
                                    <p>Каркасный дом обладает эффектом термоса – в нем тепло зимой и прохладно летом. Быстрый прогрев помещений, возведенных по каркасной технологии, позволяет существенно экономить на отоплении.</p>
                                    <p>Звоните, менеджеры компании «Теремъ» с радостью ответят на все вопросы о строительстве каркасных домов!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            <?php endif; ?>

            <?php if ($dir == '/catalog/besedki-bani-i-sadovye-doma/' && !isset($_GET['PAGEN_1'])): ?>
                <section class="content" role="content">
                    <div class="container">           
                        <div class="row catlog-text">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="catalog-description + my-margin bottom-text">
                                    <h2 class="catalog-titile">Почему мы выбираем деревянные бани </h2>
                                    <p>Традиционно русские бани строились из цельного бруса, но со временем технологии возведения таких строений претерпели значительную эволюцию. Сейчас на смену  им пришли менее дорогостоящие долговечные материалы.</p>
                                    <p>Основными материалами для возведения бань в нашей компании являются каркас и клееный брус. Почему мы не советуем строить баню из кирпича? Дело в том, что существенным минусом для кирпичной бани является высокая теплопроводность камня, который без дополнительного утепления будет очень быстро терять тепло. Другое дело - баня из дерева. Она экологична быстрее прогревается, а её стены дольше держат тепло зимой и прохладу  летом. Так как дерево, в сравнении с кирпичом, имеет малый вес, то можно сэкономить на фундаменте. Выбирая строительный материал для своей будущей бани, внимательно изучите его  свойства.</p>
                                    <p><strong>Преимущества бани из каркаса</strong></p>
                                    <ul>
                                        <li><p>Первое и основное преимущество каркасной бани в том, что каркас дешевле своих аналогов и при этом достаточно качественный  долговечный материал</p></li>
                                        <li><p>Каркасные бани выполнены из натурального дерева, поэтому они полностью безопасны для здоровья</p></li>
                                        <li><p>Быстрые сроки строительства бани в любое время года. Её можно собрать в 3 раза быстрее, чем кирпичную</p></li>
                                        <li><p>За счёт того что каркас является относительно лёгким, опять же в сравнении с другими древесными строительными материалами, каркасная баня крепко «встанет» на облегчённый неглубокий фундамент – это дополнительная экономия</p></li>
                                        <li><p>За счёт того что каркас является относительно лёгким, опять же в сравнении с другими древесными строительными материалами, каркасная баня крепко «встанет» на облегчённый неглубокий фундамент – это дополнительная экономия</p></li>
                                    </ul>
                                    <p><strong>Преимущества бани из бруса</strong></p>
                                    <ul>
                                        <li><p>Первым преимуществом бани из клееного бруса является её высокие теплоизоляционные качества. За счёт того, что в клееном брусе полностью отсутствуют  щели и неоднородные участки, тепло такая баня будет «держать» лучше любой другой.</p></li>
                                        <li><p>Клееный брус подвергается специальной обработке во время производства, это обеспечивает материалу отличную устойчивость к воздействию влаге и высоких температур</p></li>
                                        <li><p>Не нуждаются в отделке (обшивка вагонкой или дополнительная пароизоляция), стены из клееного бруса не подвергаются деформации</p></li>
                                        <li><p>Бани из клееного бруса выполнены из натурального дерева, поэтому они безопасны для здоровья</p></li>
                                        <li><p>Практически не дают усадку и очень быстро возводятся (15-25 дней)</p></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>  
                </section>
            <?php endif; ?>    

            <?php if ($dir == '/catalog/doma-iz-brusa/' && !isset($_GET['PAGEN_1'])): ?>
                <section class="content" role="content">
                    <div class="container">
                        <div class="row catlog-text">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="catalog-description + my-margin bottom-text new-bottom">
                                    <h3 class="text-uppercase">Вы полюбите дома из бруса за их преимущества</h3>
                                    <p>Компания «Теремъ» строит брусовые дома под ключ, площадь проектов от 13,44 до 211,39 кв.м. Более 70 готовых архитектурных решений можно возвести в брусовом исполнении. Продуманные планировки, высокие потолки и варианты строительства для постоянного или сезонного проживания, - все это есть в брусовых домах компании «Теремъ».</p>

                                    <p class="margin">Преимущества дома из бруса это:</p>
                                    <ul>
                                        <li>
                                            <p>легкий вес конструкции = быстрое возведение фундамента;</p>
                                        </li>
                                        <li>
                                            <p>экологически чистый материал = свежий воздух в помещении;</p>
                                        </li>
                                        <li>
                                            <p>отлаженная технология строительства = краткие сроки работ;</p>
                                        </li>
                                        <li>
                                            <p>естественный древесный рисунок = экономия на отделке.</p>
                                        </li>
                                    </ul>
                                    <p >В строительстве используем клееный брус из древесины хвойных пород. Пиломатериалы для строительства подготавливаются в заводских условиях, сохраняя свойства древесины, но без ее эксплуатационных недостатков. Тонкие доски (ламели) просушивают и склеивают. На строительную площадку материалы доставляются упакованные и промаркированные, строители собирают брусовой дом, как конструктор, имея четкую инструкцию.</p>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-4.png" class="full-width" alt="На фото дачный каркасный дом Малыш-5 6х5 м">
                                            <p class="img-description"><i>На фото дом из клееного бруса Терем, 6х9 м.</i></p>
                                        </div>
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-5.png" class="full-width" alt="Постройка второго этажа каркасного дома">
                                            <p class="img-description"><i>На фото дом из клееного бруса Канцлер-4, 10.5х12 м.</i></p>
                                        </div>
                                        
                                    </div>
                                    
                                    <h3 class="text-uppercase">Дома из бруса под ключ: сделаем все и вручим вам ключи</h3>
                                    <p>Компания «Теремъ» предоставляет клиентам интересные готовые проекты домов из бруса. Базовые комплектации брусовых коттеджей и домов в каталоге подходят для сезонного и круглогодичного проживания.  Здания больших и компактных площадей, с ломаными крышами, дома из бруса с террасой, просторными балконами и мансардами.</p>
                                    <p class="margin">Брусовый дом под ключ это:</p>
                                    <ul>
                                        <li>
                                            <p>готовое жилое здание;</p>
                                        </li>
                                        <li>
                                            <p>долговечная и надежная постройка;</p>
                                        </li>
                                        <li>
                                            <p>комфортные условия для загородного отдыха.</p>
                                        </li>
                                        
                                    </ul>
                                    <p>Дом из бруса – классика красивой и уютной жизни вдали от шумного города. Вместе с компанией «Теремъ» Вы получаете дом своей мечты, который готов принимать жильцов. Звоните, чтобы уточнить подробности – менеджеры компании «Теремъ» готовы ответить на все вопросы ежедневно с 10:00 до 20:00.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <?php if ($dir == '/catalog/dvukhetazhnye-doma-iz-brusa/' && !isset($_GET['PAGEN_1'])): ?>
                <section class="content" role="content">
                    <div class="container">
                        <div class="row catlog-text">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="catalog-description + my-margin bottom-text new-bottom">
                                    <h3 class="text-uppercase">Преимущества двухэтажных домов, построенных из бруса</h3>
                                    <p>Для строительства брусовых домов в 2 этажа используем клееный брус, который превосходит обычную древесину в прочности и долговечности. Пиломатериалы поставляются на строительную площадку уже профилированными – образно говоря, каждый элемент представляет собой часть конструктора, из которого и собирается постройка. Двухэтажное здание из клееного профилированного бруса строится быстро, имеет большую прочность и надежность.</p>

                                    <p class="margin">Двухэтажные брусовые дома это:</p>
                                    <ul>
                                        <li>
                                            <p>функциональность планировки;</p>
                                        </li>
                                        <li>
                                            <p>простор для дизайнерских решений;</p>
                                        </li>
                                        <li>
                                            <p>большая жилая площадь даже на небольшом участке.</p>
                                        </li>
                                    </ul>
                                    <p >Строительство двухэтажного дома из бруса занимает от 15 до 35 дней. Сначала закладывается основание здания – за 1-2 дня подготавливается свайный фундамент, устанавливается ростверк из профильной трубы. Затем монтируется конструкция пола. Линия за линией, равномерно по всему периметру строятся стены и перекрытия первого этажа.  После установки межэтажного перекрытия взводится второй этаж или мансарда. Затем устанавливаются стропила, монтируется кровля.</p>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-1.png" class="full-width" alt="На фото дачный каркасный дом Малыш-5 6х5 м">
                                            <p class="img-description"><i>На фото 2-этажный дом из бруса Монарх 3 11,3х14,5.</i></p>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-2.png" class="full-width" alt="Постройка второго этажа каркасного дома">
                                            <p class="img-description"><i>Постройка 2-этажного дома из бруса Генерал 14х20,5.</i></p>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <img src="/bitrix/templates/.default/assets/img/catalog-list/item-3.png" class="full-width" alt="Постройка каркасного дома. Возведение кровли">
                                            <p class="img-description"><i>На фото 2-этажный дом из бруса Лайнер 3 16х16,2.</i></p>
                                        </div>
                                    </div>
                                    <p>Компания «Теремъ» предоставляет дополнительные услуги: подводим коммуникации, проводим электроинженерные работы. В итоге, строим дом под ключ, обеспечивая все необходимое для комфортной загородной жизни: электричество, отопление, водоподъем, установленная сантехника и канализация.</p>
                                    <h3 class="text-uppercase">5 причин выбрать двухэтажные дома из бруса в компании «Теремъ»</h3>
                                    <ol style="color:#e0051c;">
                                        <li>
                                            <p style="color: #000;">Предлагаем готовые дома и проектируем под персональные запросы. </p>
                                        </li>
                                        <li>
                                            <p style="color: #000;">Оплата фиксируется и не меняется в ходе работ.</p>
                                        </li>
                                        <li>
                                            <p style="color: #000;">Пиломатериалы подготавливаются в заводских условиях, а не на площадке.</p>
                                        </li>
                                        <li>
                                            <p style="color: #000;">Строим под ключ, подводим коммуникации, проводим сопутствующие работы.</p>
                                        </li> 
                                        <li>
                                            <p style="color: #000;">Гарантия качества до 10 лет на каждый дом.</p>
                                        </li>
                                    </ol>
                                    <p>Чтобы уточнить подробности о строительстве понравившегося дома – звоните, менеджеры компании «Теремъ» предоставят полную информацию о каждом проекте!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

        </div>
        <?php if ($page == '/catalog/vse-doma/' || $page == '/catalog/'): ?>
        </noindex> 
    <?php endif; ?>      








</section>
<?php if ($dir == '/catalog/klassicheskie-dachnye-doma/'): ?>

<?php endif; ?>
