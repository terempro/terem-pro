<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<section class="content text-page white" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white my-margin clearfix">
					<div class="journal journal-article-layer">
						<div class="row">
							<?php if($arResult['IMG_LEFT']):?>
							<div class="col-xs-12 col-md-4 col-sm-5">
								<div class="journal-article-about">
									<div class="journal-article-about-header">
										<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="">
									</div>
									<div class="journal-article-about-content hidden-xs">
										<div class="from-author">
											<p class="from"><i><?=$arResult['REDACTOR']['NAME']?></i></p>
											<?=$arResult['REDACTOR']['PREVIEW_TEXT']?>
										</div>

									</div>
								</div>
							</div>
							<?php endif;?>

							<?php if($arResult['IMG_LEFT']):?>
								<div class="col-xs-12 col-md-7 col-sm-7 col-md-offset-1">
							<?php else:?>
								<div class="col-xs-12 col-md-12 col-sm-12 col-md-offset-1">
							<?php endif;?>

								<div class="journal-article-about-subject">
									<?php if($arResult['IMG_LEFT']):?>
									<h2>В НОМЕРЕ:</h2>
									<?php endif;?>
									<ul>
									<?php foreach($arResult['SUB_SECTION'] as $k => $v):?>

										<li class="lead">
											<h3><?=$v['NAME']?></h3>
										</li>


										<?php foreach($v['ITEMS'] as $item):?>
											<li>
												<a href="<?=$item['DETAIL_PAGE_URL']?>">
													<p><?=$item['NAME']?></p>
                                                                                                        <p><?=$item['PREVIEW_TEXT']?></p>
												</a>
											</li>
										<?php endforeach; ?>



									<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="journal-nav">
									<div>
										<?php if($arResult['PREV']):?>
										<a href="<?=$arResult['PREV']?>">< <span>НАЗАД</span></a>
										<?php endif;?>
									</div>
									<div>
										<?php if($arResult['NEXT']):?>
										<a href="<?=$arResult['NEXT']?>"><span>ВПЕРЕД</span> ></a>
										<?php endif;?>
									</div>
								</div>
							</div>
						</div>
<!--						<div class="row">-->
<!--							<div class="col-xs-12 col-md-12 col-sm-12 text-center">-->
<!--								<div class="journal-subscribe">-->
<!--									<a href="#" class="btn btn-danger btn-ghost">оформить бесплатную подписку</a>-->
<!--								</div>-->
<!--							</div>-->
<!--						</div>-->
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
