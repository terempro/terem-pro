<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$this->setFrameMode(false);

?>

<section class="content" role="content">
    <div class="container">





        <div class="flex-reversed">
            <div class="row" id="items">





                <div sortkey="2" sort="10" class="col-xs-12 col-md-6 col-sm-12 shuffle  item series_s2  technology_karkasny150  cost_prcie_100  square_size2  sizers_fil1 sizers_fil3">


                    <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">



                        <div class="flex-row">
                            <div>
                                <a href="/catalog/malye-stroeniya-besedki/garazh/">
                                    <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/23a/23ac3037899f3994f8a36677d4d2cbb1.jpg?144422836741925" alt="Гараж">
                                </a>
                            </div>

                            <div>
                                <div class="description">
                                    <h4 class="text-uppercase">
                                        Гараж<br>

                                                    4<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>6                                                    <!--<span> ОТ 185 Т.Р.</span>-->
                                    </h4>
                                    <a href="#" class="advice" data-target="#formAdvice" data-code="00000137711" data-price="225000" data-whatever="Гараж 4х6" data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                </div>
                                <div class="options">
                                    <div>
                                        <div class="description-title"></div>


                                        <p>
                                            <b>Варианты исполнения:</b>
                                        </p>

                                        <p>Каркасный 150: 
                                            <strong>от 

                                                225  т.р.


                                            </strong>
                                        </p>
                                    </div>
                                    <!--<a href="#" class="compare">Получить бесплатную консультацию</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="flex-row">
                            <div>
                                <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/resize_cache/iblock/283/300_300_1/28319064be20926185509454c000f9f5.jpg?145105554810781">
                            </div>

                        </div>
                        <a class="all-item-link" href="/catalog/malye-stroeniya-besedki/garazh/"></a>

                        <div class="item-show-more">
                            <div class="hover-block">
                                <div>
                                    <a href="/catalog/malye-stroeniya-besedki/garazh/">
                                        <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14688290642944" alt=""></i></div>
                                        <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>





                <div sortkey="2" sort="11" class="col-xs-12 col-md-6 col-sm-12 shuffle  item series_s1  technology_karkasny100  cost_prcie_100  square_size2  sizers_fil1 sizers_fil3">


                    <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">



                        <div class="flex-row">
                            <div>
                                <a href="/catalog/malye-stroeniya-besedki/besedka-barbekyu/">
                                    <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/f7b/f7b2030b302c1350525fa3a58cb3c419.jpg?144422836643847" alt="Беседка - барбекю">
                                </a>
                            </div>

                            <div>
                                <div class="description">
                                    <h4 class="text-uppercase">
                                        Беседка - барбекю<br>

                                                    6<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>4                                                    <!--<span> ОТ 195 Т.Р.</span>-->
                                    </h4>
                                    <a href="#" class="advice" data-target="#formAdvice" data-code="00000137710" data-price="220000" data-whatever="Беседка - барбекю 6х4" data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                </div>
                                <div class="options">
                                    <div>
                                        <div class="description-title"></div>


                                        <p>
                                            <b>Варианты исполнения:</b>
                                        </p>

                                        <p>Каркасный 100: 
                                            <strong>от 

                                                220  т.р.


                                            </strong>
                                        </p>
                                    </div>
                                    <!--<a href="#" class="compare">Получить бесплатную консультацию</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="flex-row">
                            <div>
                                <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/resize_cache/iblock/7df/300_300_1/7dffbb0ff6491f6b4bec91174599d8d8.jpg?146010630112747">
                            </div>

                        </div>
                        <a class="all-item-link" href="/catalog/malye-stroeniya-besedki/besedka-barbekyu/"></a>

                        <div class="item-show-more">
                            <div class="hover-block">
                                <div>
                                    <a href="/catalog/malye-stroeniya-besedki/besedka-barbekyu/">
                                        <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14688290642944" alt=""></i></div>
                                        <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>





                <div sortkey="2" sort="12" class="col-xs-12 col-md-6 col-sm-12 shuffle  item series_s1  technology_karkasny100  cost_prcie_100  square_size1  sizers_fil1">


                    <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">



                        <div class="flex-row">
                            <div>
                                <a href="/catalog/malye-stroeniya-besedki/besedka-barbekyui/">
                                    <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/65b/65b049186e7b5bf3773019f5d1c53963.jpg?146554670225712" alt="Беседка - барбекю">
                                </a>
                            </div>

                            <div>
                                <div class="description">
                                    <h4 class="text-uppercase">
                                        Беседка - барбекю<br>

                                                    5<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>4                                                    <!--<span> ОТ 140 Т.Р.</span>-->
                                    </h4>
                                    <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="185000" data-whatever="Беседка - барбекю 5х4" data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                </div>
                                <div class="options">
                                    <div>
                                        <div class="description-title"></div>


                                        <p>
                                            <b>Варианты исполнения:</b>
                                        </p>

                                        <p>Каркасный 100: 
                                            <strong>от 

                                                170  т.р.


                                            </strong>
                                        </p>
                                    </div>
                                    <!--<a href="#" class="compare">Получить бесплатную консультацию</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="flex-row">
                            <div>
                                <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/resize_cache/iblock/d5a/300_300_1/d5aeecd9c83569c572418af05bd0d9c4.png?146548973510715">
                            </div>

                        </div>
                        <a class="all-item-link" href="/catalog/malye-stroeniya-besedki/besedka-barbekyui/"></a>

                        <div class="item-show-more">
                            <div class="hover-block">
                                <div>
                                    <a href="/catalog/malye-stroeniya-besedki/besedka-barbekyui/">
                                        <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14688290642944" alt=""></i></div>
                                        <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>





                <div sortkey="2" sort="13" class="col-xs-12 col-md-6 col-sm-12 shuffle  item series_s1  technology_karkasny100  cost_prcie_100  square_size1  sizers_fil1">


                    <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">



                        <div class="flex-row">
                            <div>
                                <a href="/catalog/malye-stroeniya-besedki/besedka-barbekyo/">
                                    <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/09d/09df3736a42dbbc057f5b80cb258ad3d.jpg?146554659125712" alt="Беседка - барбекю">
                                </a>
                            </div>

                            <div>
                                <div class="description">
                                    <h4 class="text-uppercase">
                                        Беседка - барбекю<br>

                                                    4<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>4                                                    <!--<span> ОТ 105 Т.Р.</span>-->
                                    </h4>
                                    <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="155000" data-whatever="Беседка - барбекю 4х4" data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                </div>
                                <div class="options">
                                    <div>
                                        <div class="description-title"></div>


                                        <p>
                                            <b>Варианты исполнения:</b>
                                        </p>

                                        <p>Каркасный 100: 
                                            <strong>от 

                                                140  т.р.


                                            </strong>
                                        </p>
                                    </div>
                                    <!--<a href="#" class="compare">Получить бесплатную консультацию</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="flex-row">
                            <div>
                                <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/resize_cache/iblock/c34/300_300_1/c34bb92b64566f121f77e8e014874cb5.png?146548919910629">
                            </div>

                        </div>
                        <a class="all-item-link" href="/catalog/malye-stroeniya-besedki/besedka-barbekyo/"></a>

                        <div class="item-show-more">
                            <div class="hover-block">
                                <div>
                                    <a href="/catalog/malye-stroeniya-besedki/besedka-barbekyo/">
                                        <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14688290642944" alt=""></i></div>
                                        <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>





                <div sortkey="2" sort="14" class="col-xs-12 col-md-6 col-sm-12 shuffle  item series_s1  technology_karkasny100  cost_prcie_100  square_size1  sizers_fil1">


                    <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">



                        <div class="flex-row">
                            <div>
                                <a href="/catalog/malye-stroeniya-besedki/besedka+a/">
                                    <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/a68/a6874cd9717e20e949360cd7a35002ed.jpg?146554648925712" alt="Беседка">
                                </a>
                            </div>

                            <div>
                                <div class="description">
                                    <h4 class="text-uppercase">
                                        Беседка<br>

                                                    4<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>4                                                    <!--<span> ОТ 82 Т.Р.</span>-->
                                    </h4>
                                    <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="100000" data-whatever="Беседка 4х4" data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                </div>
                                <div class="options">
                                    <div>
                                        <div class="description-title"></div>


                                        <p>
                                            <b>Варианты исполнения:</b>
                                        </p>

                                        <p>Каркасный 100: 
                                            <strong>от 

                                                91  т.р.


                                            </strong>
                                        </p>
                                    </div>
                                    <!--<a href="#" class="compare">Получить бесплатную консультацию</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="flex-row">
                            <div>
                                <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/resize_cache/iblock/d79/300_300_1/d797ac9059720e0268b8e9d8374dff5e.png?146548791610629">
                            </div>

                        </div>
                        <a class="all-item-link" href="/catalog/malye-stroeniya-besedki/besedka+a/"></a>

                        <div class="item-show-more">
                            <div class="hover-block">
                                <div>
                                    <a href="/catalog/malye-stroeniya-besedki/besedka+a/">
                                        <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14688290642944" alt=""></i></div>
                                        <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>





                <div sortkey="2" sort="15" class="col-xs-12 col-md-6 col-sm-12 shuffle  item series_s1  technology_brus35 technology_brus70  cost_prcie_100  square_size1  sizers_fil1">


                    <div class="my-promotion desgin-4 my-margin hover-grey single-item new-single-item">



                        <div class="flex-row">
                            <div>
                                <a href="/catalog/malye-stroeniya-besedki/besedka%D0%B0/">
                                    <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/217/2179373535c02d4be6dc73aea75dc3a1.jpg?146554636325712" alt="Беседка">
                                </a>
                            </div>

                            <div>
                                <div class="description">
                                    <h4 class="text-uppercase">
                                        Беседка<br>

                                                    3<span style="text-transform:lowercase;padding: 0 2px;color: #333;">X</span>3                                                    <!--<span> ОТ 64 Т.Р.</span>-->
                                    </h4>
                                    <a href="#" class="advice" data-target="#formAdvice" data-code="" data-price="80000" data-whatever="Беседка 3х3" data-toggle="modal"><i class="glyphicon glyphicon-earphone"></i></a>
                                </div>
                                <div class="options">
                                    <div>
                                        <div class="description-title"></div>


                                        <p>
                                            <b>Варианты исполнения:</b>
                                        </p>

                                        <p>Каркасный 100: 
                                            <strong>от 

                                                71  т.р.


                                            </strong>
                                        </p>
                                    </div>
                                    <!--<a href="#" class="compare">Получить бесплатную консультацию</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="flex-row">
                            <div>
                                <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/upload/resize_cache/iblock/3ff/300_300_1/3ff5bc7089cc7333dde21e4c10682ba9.png?146548708510240">
                            </div>

                        </div>
                        <a class="all-item-link" href="/catalog/malye-stroeniya-besedki/besedka%D0%B0/"></a>

                        <div class="item-show-more">
                            <div class="hover-block">
                                <div>
                                    <a href="/catalog/malye-stroeniya-besedki/besedka%D0%B0/">
                                        <div><i><img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/cursor.png?14688290642944" alt=""></i></div>
                                        <div><p>узнать подробности<br>комплектации этого дома</p></div>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>


    </div>
</section>