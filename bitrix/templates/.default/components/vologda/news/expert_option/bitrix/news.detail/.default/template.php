<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>



<?php if ($arResult["PROPERTIES"]["ACTIVE"]["~VALUE"] == 'Активировать'): ?>
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
            <div class="white padding-side + my-margin clearfix">

                <?php if ($arResult["PROPERTIES"]["TITLE1"]["VALUE"]): ?>
                    <div class="my-text-title text-center text-uppercase desgin-h1">
                        <h1><?= $arResult["PROPERTIES"]["TITLE1"]["~VALUE"] ?></h1>
                    </div>
                <?php endif; ?>


                <?php if ($arResult["PROPERTIES"]["TEXT1"]["VALUE"]["TEXT"]): ?>
                    <div class="text-description clearfix">
                        <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT1"]["VALUE"]["TEXT"]) ?></p>
                    </div>
                <?php endif; ?>


                <?php if ($arResult["PROPERTIES"]["IMAGE1"]["~VALUE"]): ?>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="img-with-description clearfix">
                                <img src="<?= CFile::GetPath($arResult["PROPERTIES"]["IMAGE1"]["~VALUE"]) ?>" class="full-width">
                        
                                <?php
                                    $rsFile = CFile::GetByID($arResult["PROPERTIES"]["IMAGE1"]["~VALUE"]);
                                    $arFile = $rsFile->Fetch();
                                    ?>
                         
                                <p><i><?=$arFile["DESCRIPTION"]?></i></p>

                            </div>
                        </div>
                    </div>
                <?php endif; ?>


                <?php if ($arResult["PROPERTIES"]["TITLE2"]["~VALUE"]): ?>
                    <div class="text-title clearfix">
                        <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE2"]["~VALUE"] ?></h2>
                    </div>
                <?php endif; ?>

                <?php if ($arResult["PROPERTIES"]["TEXT2"]["VALUE"]["TEXT"]): ?>
                    <div class="text-description clearfix">
                        <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT2"]["VALUE"]["TEXT"]) ?></p>
                    </div>
                <?php endif; ?>


                <?php if ($arResult["PROPERTIES"]["PREV1"]["~VALUE"]): ?>
                    <div class="arrow-title clearfix">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <img src="/bitrix/templates/.default/assets/img/expert/sep2.png" alt="arrow-left" class="full-width">
                            </div>

                            <div class="col-xs-12 col-sm-10 col-md-10 text-center col-sm-offset-1 col-md-offset-1">
                                <p><?= $arResult["PROPERTIES"]["PREV1"]["~VALUE"] ?></p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <img src="/bitrix/templates/.default/assets/img/expert/sep1.png" alt="arrow-right" class="full-width arrow-right-img">
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="text-with-img right-img">
                    <?php if ($arResult["PROPERTIES"]["TITLE3"]["~VALUE"]): ?>
                        <div class="row">
                            <div class="text-title">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE3"]["~VALUE"] ?></h2>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-sm-8">

                            <?php if ($arResult["PROPERTIES"]["TEXT3"]["VALUE"]["TEXT"]): ?>
                                <div class="text-description">
                                    <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT3"]["VALUE"]["TEXT"]) ?></p>
                                </div>
                            <?php endif; ?>

                        </div>

                        <?php if ($arResult["PROPERTIES"]["IMAGE2"]["~VALUE"]): ?>
                            <div class="col-xs-12 col-md-4 col-sm-4">
                                <div class="img-with-description">
                                    <img src="<?= CFile::GetPath($arResult["PROPERTIES"]["IMAGE2"]["~VALUE"]) ?>" class="full-width">
                                    <?php
                                    $rsFile = CFile::GetByID($arResult["PROPERTIES"]["IMAGE2"]["~VALUE"]);
                                    $arFile = $rsFile->Fetch();
                                    ?>
                                    <p><i><?=$arFile["DESCRIPTION"]?></i></p>

                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if ($arResult["PROPERTIES"]["TITLE4"]["~VALUE"]): ?>
                    <div class="text-title clearfix">
                        <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE4"]["~VALUE"] ?></h2>
                    </div>
                <?php endif; ?>

                <?php if ($arResult["PROPERTIES"]["TEXT4"]["VALUE"]["TEXT"]): ?>
                    <div class="text-description clearfix">
                        <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT4"]["VALUE"]["TEXT"]) ?></p>
                    </div>
                <?php endif; ?>



                <?php if (is_array($arResult["PROPERTIES"]["IMAGE3"]["VALUE"])): ?>
                    <div class="row">
                        <?php foreach ($arResult["PROPERTIES"]["IMAGE3"]["VALUE"] as $v): ?>
                         <?php
                        $rsFile = CFile::GetByID($v);
                        $arFile = $rsFile->Fetch();
                       
                        ?>
                            <div class="col-xs-12 col-md-3 col-sm-3">
                                <div class="img-with-description">
                                    <img src="<?= CFile::GetPath($v) ?>" class="full-width">
                                    <p><i><?=$arFile["DESCRIPTION"]?></i></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>


                <?php if ($arResult["PROPERTIES"]["TITLE5"]["~VALUE"]): ?>
                    <div class="text-title clearfix">
                        <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE5"]["~VALUE"] ?></h2>
                    </div>
                <?php endif; ?>

                <?php if ($arResult["PROPERTIES"]["TEXT5"]["VALUE"]["TEXT"]): ?>
                    <div class="text-description clearfix">
                        <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT5"]["VALUE"]["TEXT"]) ?></p>
                    </div>
                <?php endif; ?>

            <?php if ($arResult["PROPERTIES"]["PREV2"]["~VALUE"]): ?>
                <div class="arrow-title clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <img src="/bitrix/templates/.default/assets/img/expert/sep2.png" alt="arrow-left" class="full-width">
                        </div>


                            <div class="col-xs-12 col-sm-10 col-md-10 text-center col-sm-offset-1 col-md-offset-1">
                                <p><?= $arResult["PROPERTIES"]["PREV2"]["~VALUE"] ?></p>
                            </div>


                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <img src="/bitrix/templates/.default/assets/img/expert/sep1.png" alt="arrow-right" class="full-width arrow-right-img">
                        </div>
                    </div>
                </div>
            <?php endif; ?>
                <?php if ($arResult["PROPERTIES"]["TITLE6"]["~VALUE"]): ?>
                    <div class="text-title clearfix">
                        <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE6"]["~VALUE"] ?></h2>
                    </div>
                <?php endif; ?>

                <?php if ($arResult["PROPERTIES"]["TEXT6"]["VALUE"]["TEXT"]): ?>
                    <div class="text-description clearfix">
                        <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT6"]["VALUE"]["TEXT"]) ?></p>
                    </div>
                <?php endif; ?>



                <?php if (is_array($arResult["PROPERTIES"]["IMAGE4"]["VALUE"])): ?>

                    <div class="row">
                        <?php foreach ($arResult["PROPERTIES"]["IMAGE4"]["VALUE"] as $i): ?>
                        
                        <?php
                        $rsFile = CFile::GetByID($i);
                        $arFile = $rsFile->Fetch();
              
                        ?>
                        
                            <div class="col-xs-12 col-md-4 col-sm-4">
                                <div class="img-with-description">
                                    <img src="<?= CFile::GetPath($i) ?>" class="full-width">
                                    <p><i><?=$arFile["DESCRIPTION"]?></i></p>

                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                <?php endif; ?>


                <?php if ($arResult["PROPERTIES"]["TITLE7"]["~VALUE"]): ?>
                    <div class="text-title clearfix">
                        <h2 class="text-uppercase"><?= $arResult["PROPERTIES"]["TITLE7"]["~VALUE"] ?></h2>
                    </div>
                <?php endif; ?>

                <?php if ($arResult["PROPERTIES"]["TEXT7"]["VALUE"]["TEXT"]): ?>
                    <div class="text-description clearfix">
                        <p><?= html_entity_decode($arResult["PROPERTIES"]["TEXT7"]["VALUE"]["TEXT"]) ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php else: ?>
    <div class="my-text-title text-center text-uppercase desgin-h1">
        <h1><?= $arResult["NAME"] ?></h1>  
    </div>

    <?php echo $arResult["DETAIL_TEXT"]; ?>


    <?php if ($arResult["PROPERTIES"]["LINK"]["~VALUE"]): ?>
        <p>
            <a href="<?= $arResult["PROPERTIES"]["LINK"]["~VALUE"] ?>">
                <?= $arResult["PROPERTIES"]["LINK"]["DESCRIPTION"] ?>
            </a>
        </p>
    <?php endif; ?>
    <?php if ($arResult["PROPERTIES"]["PHOJ"]["VALUE"]): ?>
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <h2 class="catalog-titile reset-padding">Похожие статьи:</h2>
                <br>
            </div>
        </div>
        <div class="row my-flex">
            <?php
            $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "CODE");
            $arFilter = Array("IBLOCK_ID" => 30, "ID" => $arResult["PROPERTIES"]["PHOJ"]["VALUE"]);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arFields[] = $ob->GetFields();
            }
            ?>




            <?php foreach ($arFields as $Item): ?>
                <div class="col-xs-12 col-md-3 col-sm-3 my-flex">
                    <div class="my-service-item hover my-flex full-width min-height-2 + my-margin">
                        <div>
                            <a href="/about/expert-opinion/<?php echo $Item["CODE"]; ?>/" target="_blank">
                                <img src="<?php echo CFile::GetPath($Item["PREVIEW_PICTURE"]); ?>" alt="<?= $Item['NAME'] ?>">
                            </a>
                        </div>
                        <div>
                            <div class="inner text-uppercase">
                                <h2><a href="/about/expert-opinion/<?php echo $Item["CODE"]; ?>/" target="_blank"><?= $Item['NAME'] ?></a></h2>

                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>



        </div>
    <?php endif; ?>
<?php endif; ?>