<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
global $DB;
?>
<?php if (CModule::IncludeModule("iblock")): ?>
    <?php
    $result = [];
    $categories = [];
    $series = [];
    $floors = [];
    $tehnologies = [];
    $areas = [];
    $min_area = 0;
    $max_area = 0;
    $prices = [];
    $min_price = 0;
    $max_price = 0;

    if ($_POST) {
        
    } else {
        $result_sql = $DB->Query("SELECT * FROM b_filter_house WHERE active = 1");

        while ($row = $result_sql->Fetch()) {
            $result[] = $row;
        }

        foreach ($result as $v) {
            if (!in_array($v['category_name'], $categories)) {
                $categories[$v['id_category']] = $v['category_name'];
            }
            if (!in_array(trim($v['series_name']), $series)) {

                $series[$v['id_series']] = trim($v['series_name']);
            }
            if (!in_array($v['floors'], $floors)) {
                $floors[] = $v['floors'];
            }
            if (!in_array($v['technology'], $tehnologies)) {
                $tehnologies[] = $v['technology'];
            }
            if (!in_array($v['total_area'], $areas)) {
                $areas[] = round($v['total_area']);
            }
            if (!in_array($v['price_value'], $prices)) {
                $prices[] = round($v['price_value']);
            }
        }

        $min_area = min($areas);
        $max_area = max($areas);



        $min_price = min($prices);
        $max_price = max($prices);
    }
    ?>
    <script>
        var result = {
            data: '',
            domdata: '',
            timer: '',
            max_area_old: '',
            min_area_old: '',
            min_price_old: '',
            max_price_old: '',
            category_check: '',
            series_check: '',
            floor_check: '',
            tech_check: ''
        }
        $(document).ready(function () {

            result.max_area_old = parseInt($('#house-area-up-to').val());
            result.min_area_old = parseInt($('#house-area-from').val());

            result.min_price_old = parseInt($('#house-price-from').val());
            result.max_price_old = parseInt($('#house-price-up-to').val());

            //action click filter start
            $('.house__filter').on('click', 'li', function (e) {
                var target = e.target || window.target;
                var targetLi = $(target).parent();
                var val = $(target).html();
                var filterBtn = $(this).parent().prev();
                var refreshBtn = $('.refresh-button');
                targetLi.addClass('active');
                targetLi.siblings().removeClass('active');
                filterBtn.html(val + '<span class="my-cross"></span>').prev().val(val);
                filterBtn.css('paddingRight', '30px');
                       //клинули поставили крестик
                       
                
                if ($(this).hasClass('series-filter-li')) {
                    var idx = $(this).data('key');
                    result.series_check = $(this).data('id');
                    $('#realmodel').val(idx);
                }
                
                if($(this).hasClass('category-li-filter')){
                    result.category_check = $(this).data('id');
                }
                
                if($(this).hasClass('floor-dilter-li')){
                    result.floor_check = $(this).data('id');
                }
                
                if($(this).hasClass('filter-tech-li')){
                    result.tech_check = $(this).data('id');
                }
                    
              

                filter(result.data);
            });
            
            //если крестик то консоль лог дата id выбранного
            
            
            
            
            var rangePrice = {
                textFrom: 'от',
                textTo: 'до',
                text: ''
            }

            var initialBtnName = '';
            $('.dropdown-toggle').on(function (e) {
                initialBtnName = '';
                var target = e.target || window.target;
                initialBtnName = $(target).text();
            })

            $('.range-box > input').click(function (e) {
                e.stopPropagation();
            })

            $('.range-box').on('blur', 'input', function (e) {
                var target = e.target || window.target;
                var idTarget = $(target).attr('id').split('-');
                var val = $(target).val();
                var val_data = $(target).data('val');
                var filterBtn = $(this).parent().parent().prev();
                var valTarget = parseInt($(target).val());
                var name = $(this).attr('name');
                if (idTarget[2] == 'from') {
                    rangePrice.textFrom = textCombinate('from', valTarget);
                } else {
                    rangePrice.textTo = textCombinate('up', valTarget);
                }

                var textInputPrice = '';
                if (rangePrice.textFrom != 'от') {
                    textInputPrice += rangePrice.textFrom;
                }
                if (rangePrice.textTo != 'до') {
                    textInputPrice += rangePrice.textTo;
                }
                if (!isNaN(rangePrice.from) && rangePrice.from) {
                    rangePrice.textAll += ' ' + rangePrice.textTo
                            + ' ' + rangePrice.textFrom;
                }

                //if (e.keyCode != 8 && e.keyCode != 46) {

                if (parseInt(val) > 0) {
                    console.log('vals', val);
                    console.log('OLD', result.max_price_old);
                    console.log('NEW', val);
                    val = parseInt(val);

                    if (name == 'house-areaUpto' && val != 0 && val > result.max_area_old && !isNaN(val)) {
                        console.log('!!!&&&');
                        $('#house-area-up-to').data('val', val);
                        filter(result.data);
                    }
                    if (name == 'house-areaFrom' && val != 0 && val < result.max_area_old && !isNaN(val)) {
                        console.log('!!!');
                        $('#house-area-from').data('val', val);
                        filter(result.data);
                    }
                    if (name == 'house-priceUpto' && val != 0 && val > result.max_price_old) {
                        console.log('1111111111111', val_data);
                        $('#house-price-up-to').data('val', val);
                        filter(result.data);
                    }
                    if (name == 'house-priceFrom' && val != 0 && val < result.max_price_old) {
                        $('#house-price-from').data('val', val);
                        filter(result.data);
                    }
                } else {
                    if (name == 'house-areaUpto') {
                        console.log('!!!&&&');
                        $('#house-area-up-to').data('val', 500);
                        result.max_area_old = 500;
                        filter(result.data);
                    }
                    if (name == 'house-areaFrom') {
                        console.log('!!!');
                        $('#house-area-from').data('val', 0);
                        result.min_area_old = 0;
                        filter(result.data);
                    }
                    if (name == 'house-priceUpto') {
                        $('#house-price-up-to').data('val', 10000000);
                        filter(result.data);
                    }
                    if (name == 'house-priceFrom') {
                        $('#house-price-from').data('val', 0);
                        filter(result.data);
                    }
                }
                // }



                filterBtn.html(textInputPrice);
                filter(result.data);
                if (!filterBtn.html()) {
                    filterBtn.html(initialBtnName);
                }

            });
            function textCombinate(idx, vals) {
                var result = false;
                if (idx == 'from') {
                    var Text = 'от';
                }
                if (idx == 'up') {
                    var Text = ' до';
                }
                if (!isNaN(vals) && vals && vals != 0) {
                    result = Text;
                    result += ' ' + vals;
                } else {
                    result = '';
                }
                return result;
            }
            ;
            //action click filter end



            if (result.data.length <= 0) {
                //$.post('/include/filter_search.php', {}, function (data) {result.data = data;}).done(function () {filter(result.data); }).fail(function () {alert("error");});
                $.ajax('/include/filter_search.php', {
                    method: 'POST',
                    async: false,
                    success: function (data) {
                        result.data = data;
                    }
                });

                console.log('JSON_DATA3', result);
            }

            filter(result.data);
        });
        function filter() {

            console.log('filter_start');
            var categories = [];
            var series = [];
            var floors = [];
            var tehnologies = [];
            var areas = [];
            var min_area = parseInt($('#house-area-from').val());
            var max_area = parseInt($('#house-area-up-to').val());
            var prices = [];
            var min_price = parseInt($('#house-price-from').val());
            var max_price = parseInt($('#house-price-up-to').val());
            var first_load = true;
            var listCat = $('.categories-filter > li');
            var listSeries = $('.series-filter > li');
            var listFloors = $('.floors-filter > li');
            var listTech = $('.tehnologies-filter > li');

            categories = getArray(listCat);
            series = getArray(listSeries);
            floors = getArray(listFloors);
            tehnologies = getArray(listTech);


            if (isNaN(min_area)) {
                min_area = 0;
            }
            if (isNaN(max_area)) {
                max_area = 500;
            }
            console.log('START_MAX', max_area);

            console.log('result', result);

            if (result.domdata.length != 0) {
                var jsonData = result.domdata;
                first_load = false;
            } else {
                var jsonData = result.data;
            }



            if (categories.length <= 0 && series.length <= 0 && floors.length <= 0 && tehnologies.length <= 0) {
                console.log('1919191');
                var arr = result.data;
            } else {
                var arr = jsonData.filter(function (obj) {

                    if (categories.length > 0) {
                        if (find(categories, obj.id_category) != -1) {
                            return  obj;
                        }
                    }
                    if (series.length > 0) {
                        if (find(series, obj.series_name) != -1) {
                            return  obj;
                        }
                    }
                    if (floors.length > 0) {
                        if (find(floors, obj.floors) != -1) {
                            return  obj;
                        }
                    }
                    if (tehnologies.length > 0) {
                        if (find(tehnologies, obj.technology) != -1) {
                            return  obj;
                        }
                    }



                });
            }





            console.log('resfilter', arr);




            if (arr.length > 0) {

                var filterArr = arr.filter(function (obj) {
                    if (obj.total_area >= min_area && obj.total_area <= max_area) {
                        if (obj.price_value >= min_price && obj.price_value <= max_price) {
                            return  obj;
                        }
                    }

                });
                
              

                console.log('MIN_PRICE', min_price);
                console.log('MAX_PRICE', max_price);


                result.domdata = filterArr;
                renderFilter(result.domdata);
            }
        }

        function renderFilter(finalArr) {
            console.log('FINAL_ARR__', finalArr);
            var listCat = $('.categories-filter');
            var listSeries = $('.series-filter');
            var listFloors = $('.floors-filter');
            var listTech = $('.tehnologies-filter');
            var categories = [];
            var categories_html = '';
            var series = [];
            var series_html = '';
            var floors = [];
            var floors_html = '';
            var tech = [];
            var tech_html = '';
            var areas = [];
            var prices = [];
            finalArr.filter(function (obj) {
                if (find(categories, obj.id_category) == -1) {
                    categories.push(obj.id_category);
                    
                    
                    
                    if(obj.id_category == result.category_check){
                       categories_html = categories_html + '<li  class="category-li-filter active" data-id="' + obj.id_category + '"><a href="#" data-name="' + obj.category_name + '" >' + obj.category_name + '</a></li>'; 
                    }else{
                      categories_html = categories_html + '<li  class="category-li-filter" data-id="' + obj.id_category + '"><a href="#" data-name="' + obj.category_name + '" >' + obj.category_name + '</a></li>';  
                    }
                    
                    
                }
                if (find(series, obj.series_name) === -1) {
                    series.push(obj.series_name);
                    
                     if(obj.series_name == result.series_check){
                         series_html = series_html + '<li data-id="' + obj.series_name + '" data-id="' + obj.series_name + '" data-key="' + obj.id_series + '" class="series-filter-li active" ><a href="#" data-name="' + obj.series_name + '" >' + obj.series_name + '</a></li>';
                     }else{
                         series_html = series_html + '<li data-id="' + obj.series_name + '" data-id="' + obj.series_name + '" data-key="' + obj.id_series + '" class="series-filter-li" ><a href="#" data-name="' + obj.series_name + '" >' + obj.series_name + '</a></li>';
                     }
                    
                    

                }
                if (find(floors, obj.floors) === -1) {
                    floors.push(obj.floors);
                    
                     if(obj.series_name == result.floor_check){
                         floors_html = floors_html + '<li data-id="' + obj.floors + '" class="floor-dilter-li active"><a href="#" data-name="' + obj.floors + '" >' + obj.floors + '</a></li>';
                     }else{
                         floors_html = floors_html + '<li data-id="' + obj.floors + '" class="floor-dilter-li"><a href="#" data-name="' + obj.floors + '" >' + obj.floors + '</a></li>';
                     }
                    
                    
                }
                if (find(tech, obj.technology) === -1) {
                    
                    tech.push(obj.technology);
                    
                    if(obj.technology == result.tech_check){
                        tech_html = tech_html + '<li class="filter-tech-li active" data-id="' + obj.technology + '"><a href="#" data-name="' + obj.technology + '" >' + obj.technology + '</a></li>';
                    }else{
                       tech_html = tech_html + '<li class="filter-tech-li" data-id="' + obj.technology + '"><a href="#" data-name="' + obj.technology + '" >' + obj.technology + '</a></li>'; 
                    }
                    
                    
                    
                    
                    
                }
                if (find(areas, parseInt(obj.total_area)) === -1) {
                    areas.push(parseInt(obj.total_area));
                }
                if (find(prices, parseInt(obj.price_value)) === -1) {
                    prices.push(parseInt(obj.price_value));
                }
            });


            var min_area = Math.min(...areas);
                    var max_area = Math.max(...areas);
                    //console.log('AREAS', areas);


                    //min area set
                    //$('#house-area-from').val(parseInt(min_area));
                    $('#house-area-from').attr('placeholder', 'от ' + min_area);
            result.min_area_old = parseInt(min_area);

            //max area set
            //$('#house-area-up-to').val(parseInt(max_area));
            $('#house-area-up-to').attr('placeholder', 'до ' + max_area);
            result.max_area_old = parseInt(max_area);

            var min_price = Math.min(...prices);
                    var max_price = Math.max(...prices);
                    //min price set
                    //$('#house-price-from').val(min_price);
                    $('#house-price-from').attr('placeholder', 'от ' + min_price);
            result.min_price_old = parseInt(min_price);

            //max price set
            //$('#house-price-up-to').val(max_price);
            $('#house-price-up-to').attr('placeholder', 'до ' + max_price);
            result.max_price_old = parseInt(max_price);



            // console.log('MIN_AREA__', min_area);
            // console.log('MAX_AREA__', max_area);
            // console.log('ARR_SERIES', series);


            if (categories.length > 0) {
                listCat.html(categories_html);
            }
            if (series.length > 0) {
                listSeries.html(series_html);
            }
            if (floors.length > 0) {
                listFloors.html(floors_html);
            }
            if (tech.length > 0) {
                listTech.html(tech_html);
            }

        }



        function getArray(arr) {
            var result = [];
            $(arr).each(function (indx, element) {
                if ($(element).hasClass("active")) {
                    result.push($(element).attr('data-id'));
                }
            });
            return result;
        }

        function find(array, value) {

            for (var i = 0; i < array.length; i++) {
                if (array[i] == value)
                    return i;
            }

            return -1;
        }
    </script>
    <div class="house__filter">
        <form action="" method="post" id="filter" class="filter-form"> <div class="dropdown">
                <input type="hidden" name="realmodel" value="" id="realmodel"/>
                <input name="category" value="" class="hidden" type="text">
                <button class="btn btn-filter btn-lg dropdown-toggle" type="button" id="categoryDropdown" data-placeholder="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Бани<span class="my-caret"></span></button>
                <ul class="dropdown-menu dropdown-menu--terem categories-filter" aria-labelledby="dropdownRoad">
                    <span class="arrow-top"></span>
    <?php foreach ($categories as $k => $v): ?>
                        <li data-id="<?= $k ?>" class="category-li-filter"><a href="#" data-name="<?= $v ?>"><?= $v ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div><div class="dropdown">
                <input name="model" value="" class="hidden" type="text">
                <button class="btn btn-filter btn-lg dropdown-toggle" type="button" id="modelDropdown" data-placeholder="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Серия
                    <span class="my-caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu--terem series-filter" aria-labelledby="dropdownRoad">
                    <span class="arrow-top"></span>
    <?php foreach ($series as $k => $v): ?>
                        <li data-id="<?= $v ?>" data-value="<?= $v ?>" data-key="<?= $k ?>" class="series-filter-li"><a href="#" data-name="<?= $v ?>" ><?= $v ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div><div class="dropdown">
                <input name="house-floors" value="" class="hidden" type="text">
                <button class="btn btn-filter btn-lg dropdown-toggle" type="button" id="house-floorsDropdown" data-placeholder="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Этажность
                    <span class="my-caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu--terem floors-filter" aria-labelledby="dropdownRoad">
                    <span class="arrow-top"></span>
                    <?php foreach ($floors as $k => $v): ?>
                        <li data-id="<?= $v ?>" class="floor-dilter-li" data-real="<?= $k ?>"><a href="#" data-name="<?= $v ?>" data-id="<?= $k ?>"><?= $v ?> <?php echo declension_words($v, ['этаж', 'этажа', 'этажей']); ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div><div class="dropdown">
                <input name="house-tech" value="" class="hidden" type="text">
                <button class="btn btn-filter btn-lg dropdown-toggle" type="button" id="house-techDropdown" data-placeholder="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Технология
                    <span class="my-caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu--terem tehnologies-filter" aria-labelledby="dropdownRoad">
                    <span class="arrow-top"></span>
                <?php foreach ($tehnologies as $k => $v): ?>
                        <li  data-id="<?= $v ?>" class="filter-tech-li"><a href="#" data-name="<?= $v ?>"><?= $v ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="dropdown">
                <button class="btn btn-filter btn-lg dropdown-toggle" type="button" id="house-areaDropdown" data-placeholder="Удаленность от МКАД" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Площадь
                    <span class="my-caret"></span>
                </button>
                <div class="dropdown-menu dropdown-menu--terem" aria-labelledby="house-areaDropdown">
                    <span class="arrow-top"></span>
                    <div class="range-box">
                        <input name="house-areaFrom" data-name="house-area" id="house-area-from" placeholder="от <?= $min_area ?>" data-val="<?= $min_area ?>" value="<?= $min_area ?>" step="1" type="number">
                        <span> - </span>
                        <input name="house-areaUpto" data-name="house-area" id="house-area-up-to" placeholder="до <?= $max_area ?>" data-val="<?= $max_area ?>" value="<?= $max_area ?>" step="1" type="number">
                        <span>кв.м</span>
                    </div>
                </div>
            </div><div class="dropdown">
                <button class="btn btn-filter btn-lg dropdown-toggle" type="button" id="house-priceDropdown" data-placeholder="Удаленность от МКАД" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Цена
                    <span class="my-caret"></span>
                </button>
                <div class="dropdown-menu dropdown-menu--terem" aria-labelledby="house-priceDropdown">
                    <span class="arrow-top"></span>
                    <div class="range-box">
                        <input name="house-priceFrom" data-name="house-price" id="house-price-from" data-val="<?= $min_price ?>" value="<?= $min_price ?>" placeholder="от <?= $min_price ?> 000" type="number">
                        <span> - </span>
                        <input name="house-priceUpto" data-name="house-price" id="house-price-up-to" data-val="<?= $max_price ?>" value="<?= $max_price ?>" placeholder="до <?= $max_price ?> 000" type="number">
                        <span>р.</span>
                    </div>
                </div>
            </div>
            <div class="filter-button">
                <button class="refresh-button" type="reset" style="display: inline-block;">Сбросить</button>
                <button class="submit-button" type="submit">Найти</button>
            </div>
        </form>
    </div>
<?php endif; ?>