<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>




<section class="content content-back-side" data-parallax="up" data-opacity="true">
    <div class="container-fluid reset-padding">
        <div class="row reset-margin">
            <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
                <div class="back-img" style="background-image: url('//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/bg.jpg?1457699869929638');"></div>
            </div>
        </div>
    </div>
</section>
<?php
//if ($USER->IsAdmin()){
//    echo "<pre>";
//    var_dump($arResult);
//    die();
//}
?>
<?php $block1_cnt = 1; ?>
<?php $block2_cnt = 3; ?>
<?php $block3_cnt = 5; ?>
<?php $block4_cnt = 7; ?>
<?php $block5_cnt = 9; ?>
<?php $block6_cnt = 11; ?>
<?php $googletag = 0; ?>
<!--Верхний блок -->


<div itemscope itemtype="http://schema.org/Product">
    <?php foreach ($arResult["HOUSE_INFORMATION"] as $arItem): ?>

        <?php if ($googletag == 0): ?>
            <?php $googletag++; ?>
            <!-- GOOGLE TAG -->
            <script type="text/javascript">
                var google_tag_params = {
                    dynx_itemid: <?php echo $arResult["ID"]; ?>,
                    dynx_pagetype: "offerdetail",
                    dynx_totalvalue: <?php echo number_format($arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"], 3, '', ''); ?>
                };
            </script>
            <!-- GOOGLE TAG -->
        <?php endif; ?>


        <section class="content white catalog-item-<?= $block1_cnt ?> content-catalog-item <?php if ($block1_cnt == 1): ?>active<?php endif; ?>" data-item="<?= $block1_cnt ?>">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12 col-sm-12">
                        <div class="catalog-item-gallery white + my-margin">
                            <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-left && bg-grey && padding-20 ))">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">

                                    <?php if ($arItem["DETAIL_PICTURE"] != NULL): ?>
                                        <li role="presentation" class="active"><a href="#tab<?= $block1_cnt; ?>" aria-controls="tab<?= $block1_cnt; ?>" role="tab" data-toggle="tab">Фото и планировки</a></li>
                                    <?php endif; ?>  

                                    <?php if ($arItem["PROPERTY_D_RACURS_VALUE"]["TEXT"]): ?>    
                                        <li role="presentation"><a href="#tab<?= $block2_cnt; ?>" aria-controls="tab<?= $block2_cnt; ?>" role="tab" data-toggle="tab">3D ракурс</a></li>
                                    <?php endif; ?>    



                                    <?php if (count($arItem["PROPERTY_NITERIER_VALUE"]) > 1): ?>   
                                        <li role="presentation"><a href="#tab<?= $block3_cnt; ?>" aria-controls="tab<?= $block3_cnt; ?>" role="tab" data-toggle="tab">Интерьеры</a></li>
                                    <?php endif; ?>  
                                    <?php if (count($arItem["PROPERTY_EXTERIER_VALUE"]) > 1): ?>
                                        <li role="presentation"><a href="#tab<?= $block4_cnt; ?>" aria-controls="tab<?= $block4_cnt; ?>" role="tab" data-toggle="tab">Экстерьеры</a></li>
                                    <?php endif; ?> 

                                    <?php if (count($arItem["PROPERTY_PERSON_PHOTO_VALUE"]) > 1): ?>    
                                        <li role="presentation"><a href="#tab<?= $block5_cnt; ?>" aria-controls="tab<?= $block5_cnt; ?>" role="tab" data-toggle="tab">Построенные дома</a></li>
                                    <?php endif; ?>    

                                    <?php if ($arItem['PROPERTY_VIDEO_VALUE']["TEXT"]): ?>  
                                        <li role="presentation"><a href="#tab<?= $block5_cnt; ?>" aria-controls="tab<?= $block6_cnt; ?>" role="tab" data-toggle="tab">Видео</a></li>
                                    <?php endif; ?>    

                                    <li role="presentation" class="back-to-catalog"><a href="<?php echo $arResult["LIST_PAGE_URL"]; ?>">Вернуться в каталог</a></li>
                                </ul><!-- Nav tabs END-->
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?php if ($arItem["DETAIL_PICTURE"] != NULL): ?>
                                        <div role="tabpanel" class="tab-pane active" id="tab<?= $block1_cnt; ?>">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-9 col-sm-9">
                                                    <div data-item="slider-item-catalog">
                                                        <div class="owl-item">
                                                            <div class="my-main-img">
                                                                <?php if ($arItem['IBLOCK_SECTION_ID'] == '679'): ?>
                                                                    <div class="addition">
                                                                        <img src="/bitrix/templates/.default/assets/img/Roof_gift_2.png" alt="">
                                                                    </div>
                                                                <?php endif; ?>
                                                                <img src="<?php echo CFile::GetPath($arItem["DETAIL_PICTURE"]); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="owl-item">
                                                            <div class="my-main-img">
                                                                <?php if ($arItem["PLAN1_838"]): ?>
                                                                    <img src="<?= $arItem["PLAN1_838"] ?>">
                                                                <?php else: ?>
                                                                    <img src="/bitrix/templates/.default/assets/img/yeplo2.jpg">
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <div class="owl-item">
                                                            <div class="my-main-img">
                                                                <?php if ($arItem["PLAN2_838"]): ?>
                                                                    <img src="<?= $arItem["PLAN2_838"] ?>">
                                                                <?php else: ?>
                                                                    <img src="/bitrix/templates/.default/assets/img/yeplo2.jpg">
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-3 col-sm-3">
                                                    <div class="left-side">
                                                        <div class="left-side-trigger state_0">
                                                            <div class="my-main-img">
                                                                <?php if ($arItem["DETAIL_PICTURE"]): ?>
                                                                    <a <?php if ($block1_cnt == 1): ?>itemprop="image"<?php endif; ?> class="fancybox" rel="group<?= $arItem["ID"] ?>" href="<?php echo CFile::GetPath($arItem["DETAIL_PICTURE"]); ?>"><img src="<?php echo CFile::GetPath($arItem["PREVIEW_PICTURE"]); ?>"><div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div></a>
                                                                <?php else: ?>
                                                                    <img <?php if ($block1_cnt == 1): ?>itemprop="image"<?php endif; ?> src="/bitrix/templates/.default/assets/img/yeplo2.jpg">
                                                                <?php endif; ?>
                                                            </div>
                                                            <div class="my-main-img">
                                                                <?php if ($arItem["PROPERTY_PLANS_HOUSE_1_VALUE"]): ?>
                                                                    <a class="fancybox" rel="group<?= $arItem["ID"] ?>" href="<?php echo CFile::GetPath($arItem["PROPERTY_PLANS_HOUSE_1_VALUE"]); ?>"><img src="<?= $arItem["PLAN1_285"] ?>"><div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div></a>
                                                                <?php else: ?>
                                                                    <img src="/bitrix/templates/.default/assets/img/yeplo2.jpg">
                                                                <?php endif; ?>
                                                            </div>
                                                            <div class="my-main-img">
                                                                <?php if ($arItem["PROPERTY_PLANS_HOUSE_2_VALUE"]): ?>
                                                                    <a class="fancybox" rel="group<?= $arItem["ID"] ?>" href="<?php echo CFile::GetPath($arItem["PROPERTY_PLANS_HOUSE_2_VALUE"]); ?>"><img src="<?= $arItem["PLAN2_285"] ?>"><div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div></a>
                                                                <?php else: ?>
                                                                    <img src="/bitrix/templates/.default/assets/img/yeplo2.jpg">
                                                                <?php endif; ?>

                                                            </div>
                                                            <div class="my-main-img">
                                                                <?php if ($arItem["DETAIL_PICTURE"]): ?>
                                                                    <a class="fancybox" rel="group<?= $arItem["ID"] ?>" href="<?php echo CFile::GetPath($arItem["DETAIL_PICTURE"]); ?>"><img src="<?php echo CFile::GetPath($arItem["PREVIEW_PICTURE"]); ?>"><div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div></a>
                                                                <?php else: ?>
                                                                    <img src="/bitrix/templates/.default/assets/img/yeplo2.jpg">
                                                                <?php endif; ?>
                                                            </div>
                                                            <div class="my-main-img">
                                                                <?php if ($arItem["PROPERTY_PLANS_HOUSE_1_VALUE"]): ?>
                                                                    <a class="fancybox" rel="group<?= $arItem["ID"] ?>" href="<?php echo CFile::GetPath($arItem["PROPERTY_PLANS_HOUSE_1_VALUE"]); ?>"><img src="<?= $arItem["PLAN1_285"] ?>"><div class="zoom"><i class="glyphicon glyphicon-zoom-in"></i></div></a>
                                                                <?php else: ?>
                                                                    <img src="/bitrix/templates/.default/assets/img/yeplo2.jpg">
                                                                <?php endif; ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-md-9 col-sm-9">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                                            <h1>
                                                                ДОМ <span class="text-uppercase"><?= $arResult["NAME"] ?></span>
                                                            </h1>
                                                        </div>
                                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                                            <ul>
                                                                <li>Линейные размеры: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></li>
                                                                <li>Площадь застройки: <strong><?= $arItem["PROPERTY_SQUARE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                                <li>Срок строительства: <strong><?= $arItem["PROPERTY_DAYS_HOUSE_VALUE"] ?></strong></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                                            <ul>
                                                                <li>Общая площадь: <strong><?= $arItem["PROPERTY_SQUARE_ALL_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                                <li>Жилая площадь: <strong><?= $arItem["PROPERTY_SQUARE_LIFE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-3 col-sm-3">
                                                    <script type="text/javascript">(function (w, doc) {
                                                        if (!w.__utlWdgt) {
                                                            w.__utlWdgt = true;
                                                            var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                                            s.type = 'text/javascript';
                                                            s.charset = 'UTF-8';
                                                            s.async = true;
                                                            s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                                            var h = d[g]('body')[0];
                                                            h.appendChild(s);
                                                        }
                                                    })(window, document);
                                                </script>
                                                <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="round-rectangle" data-sn-ids="vk.fb.tw.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1463423" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>

                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if ($arItem["PROPERTY_D_RACURS_VALUE"]["TEXT"]): ?> 
                                    <div role="tabpanel" class="tab-pane" id="tab<?= $block2_cnt; ?>">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <?php echo html_entity_decode($arItem["PROPERTY_D_RACURS_VALUE"]["TEXT"]); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-9 col-sm-9">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <h1>
                                                            ДОМ <span class="text-uppercase"><?= $arResult["NAME"] ?></span>
                                                        </h1>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <ul>
                                                            <li>Линейные размеры: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></li>
                                                            <li>Площадь застройки: <strong><?= $arItem["PROPERTY_SQUARE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                            <li>Срок строительства: <strong><?= $arItem["PROPERTY_DAYS_HOUSE_VALUE"] ?></strong></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <ul>
                                                            <li>Общая площадь: <strong><?= $arItem["PROPERTY_SQUARE_ALL_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                            <li>Жилая площадь: <strong><?= $arItem["PROPERTY_SQUARE_LIFE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                <script type="text/javascript">(function (w, doc) {
                                                    if (!w.__utlWdgt) {
                                                        w.__utlWdgt = true;
                                                        var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                                        s.type = 'text/javascript';
                                                        s.charset = 'UTF-8';
                                                        s.async = true;
                                                        s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                                        var h = d[g]('body')[0];
                                                        h.appendChild(s);
                                                    }
                                                })(window, document);
                                            </script>
                                            <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="round-rectangle" data-sn-ids="vk.fb.tw.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1463423" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>

                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?> 

                            <?php if (count($arItem["PROPERTY_NITERIER_VALUE"]) > 1): ?>   
                                <div role="tabpanel" class="tab-pane" id="tab<?= $block3_cnt; ?>">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div data-item="slider-item">
                                                <?php foreach ($arItem["PROPERTY_NITERIER_VALUE"] as $arImg): ?>
                                                    <div class="owl-item">
                                                        <div class="my-main-img">
                                                            <img src="<?php echo CFile::GetPath($arImg); ?>">
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-9 col-sm-9">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h1>
                                                        ДОМ <span class="text-uppercase"><?= $arResult["NAME"] ?></span>
                                                    </h1>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <ul>
                                                        <li>Линейные размеры: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></li>
                                                        <li>Площадь застройки: <strong><?= $arItem["PROPERTY_SQUARE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                        <li>Срок строительства: <strong><?= $arItem["PROPERTY_DAYS_HOUSE_VALUE"] ?></strong></li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <ul>
                                                        <li>Общая площадь: <strong><?= $arItem["PROPERTY_SQUARE_ALL_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                        <li>Жилая площадь: <strong><?= $arItem["PROPERTY_SQUARE_LIFE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3 col-sm-3">

                                            <script type="text/javascript">(function (w, doc) {
                                                if (!w.__utlWdgt) {
                                                    w.__utlWdgt = true;
                                                    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                                    s.type = 'text/javascript';
                                                    s.charset = 'UTF-8';
                                                    s.async = true;
                                                    s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                                    var h = d[g]('body')[0];
                                                    h.appendChild(s);
                                                }
                                            })(window, document);
                                        </script>
                                        <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="round-rectangle" data-sn-ids="vk.fb.tw.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1463423" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>

                                    </div>
                                </div>
                            </div>
                        <?php endif; ?> 

                        <?php if (count($arItem["PROPERTY_EXTERIER_VALUE"]) > 1): ?>   
                            <div role="tabpanel" class="tab-pane" id="tab<?= $block4_cnt; ?>">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div data-item="slider-item">
                                            <?php foreach ($arItem["PROPERTY_EXTERIER_VALUE"] as $arImg): ?>
                                                <div class="owl-item">
                                                    <div class="my-main-img">
                                                        <img src="<?php echo CFile::GetPath($arImg); ?>">
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-9 col-sm-9">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <h1>
                                                    ДОМ <span class="text-uppercase"><?= $arResult["NAME"] ?></span>
                                                </h1>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <ul>
                                                    <li>Линейные размеры: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></li>
                                                    <li>Площадь застройки: <strong><?= $arItem["PROPERTY_SQUARE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                    <li>Срок строительства: <strong><?= $arItem["PROPERTY_DAYS_HOUSE_VALUE"] ?></strong></li>
                                                </ul>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <ul>
                                                    <li>Общая площадь: <strong><?= $arItem["PROPERTY_SQUARE_ALL_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                    <li>Жилая площадь: <strong><?= $arItem["PROPERTY_SQUARE_LIFE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-3">

                                        <script type="text/javascript">(function (w, doc) {
                                            if (!w.__utlWdgt) {
                                                w.__utlWdgt = true;
                                                var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                                s.type = 'text/javascript';
                                                s.charset = 'UTF-8';
                                                s.async = true;
                                                s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                                var h = d[g]('body')[0];
                                                h.appendChild(s);
                                            }
                                        })(window, document);
                                    </script>
                                    <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="round-rectangle" data-sn-ids="vk.fb.tw.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1463423" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>

                                </div>
                            </div>
                        </div>
                    <?php endif; ?> 
                    <?php if (count($arItem["PROPERTY_PERSON_PHOTO_VALUE"]) > 1): ?>
                        <div role="tabpanel" class="tab-pane" id="tab<?= $block5_cnt; ?>">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div data-item="slider-item">
                                        <?php foreach ($arItem["PROPERTY_PERSON_PHOTO_VALUE"] as $arImg): ?>
                                            <div class="owl-item">
                                                <div class="my-main-img">
                                                    <img src="<?php echo CFile::GetPath($arImg); ?>">
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-9 col-sm-9">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <h1>
                                                ДОМ <span class="text-uppercase"><?= $arResult["NAME"] ?></span>
                                            </h1>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <ul>
                                                <li>Линейные размеры: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></li>
                                                <li>Площадь застройки: <strong><?= $arItem["PROPERTY_SQUARE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                <li>Срок строительства: <strong><?= $arItem["PROPERTY_DAYS_HOUSE_VALUE"] ?></strong></li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-sm-4">
                                            <ul>
                                                <li>Общая площадь: <strong><?= $arItem["PROPERTY_SQUARE_ALL_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                                <li>Жилая площадь: <strong><?= $arItem["PROPERTY_SQUARE_LIFE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-3 col-sm-3">

                                    <script type="text/javascript">(function (w, doc) {
                                        if (!w.__utlWdgt) {
                                            w.__utlWdgt = true;
                                            var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                            s.type = 'text/javascript';
                                            s.charset = 'UTF-8';
                                            s.async = true;
                                            s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                            var h = d[g]('body')[0];
                                            h.appendChild(s);
                                        }
                                    })(window, document);
                                </script>
                                <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="round-rectangle" data-sn-ids="vk.fb.tw.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1463423" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>

                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($arItem['PROPERTY_VIDEO_VALUE']["TEXT"]): ?>
                    <div role="tabpanel" class="tab-pane" id="tab<?= $block5_cnt; ?>">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div style="margin-left:0px; margin-right:0px;" class="insetVideo">
                                    <?php
                                    echo html_entity_decode($arItem['PROPERTY_VIDEO_VALUE']["TEXT"]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-9 col-sm-9">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                        <h1>
                                            ДОМ <span class="text-uppercase"><?= $arResult["NAME"] ?></span>
                                        </h1>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                        <ul>
                                            <li>Линейные размеры: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></li>
                                            <li>Площадь застройки: <strong><?= $arItem["PROPERTY_SQUARE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                            <li>Срок строительства: <strong><?= $arItem["PROPERTY_DAYS_HOUSE_VALUE"] ?></strong></li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                        <ul>
                                            <li>Общая площадь: <strong><?= $arItem["PROPERTY_SQUARE_ALL_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                            <li>Жилая площадь: <strong><?= $arItem["PROPERTY_SQUARE_LIFE_HOUSE_VALUE"] ?> м.кв.</strong></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3 col-sm-3">

                                <script type="text/javascript">(function (w, doc) {
                                    if (!w.__utlWdgt) {
                                        w.__utlWdgt = true;
                                        var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                        s.type = 'text/javascript';
                                        s.charset = 'UTF-8';
                                        s.async = true;
                                        s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                        var h = d[g]('body')[0];
                                        h.appendChild(s);
                                    }
                                })(window, document);
                            </script>
                            <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="round-rectangle" data-sn-ids="vk.fb.tw.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1463423" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>

                        </div>
                    </div>
                </div>
            <?php endif; ?>

        </div><!-- Tab panes END-->
    </div>
</div>
</div>
</div>
</div>
</section>
<?php $block1_cnt++; ?>
<?php $block2_cnt++; ?>
<?php $block3_cnt++; ?>
<?php $block4_cnt ++; ?>
<?php $block5_cnt ++; ?>
<?php endforeach; ?>





<!--Центральня часть общая -->
<section class="content white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="catalog-item-description white + my-margin">
                    <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-2 && appearance-list-left && bg-grey && padding-navs-20 && padding-tabs-30-30 ))">
                        <!-- Nav tabs -->
                        <?php $block2_cnt = 1; ?>
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach ($arResult["HOUSE_INFORMATION"] as $arItem): ?>
                                <li role="presentation" <?php if ($block2_cnt == 1): ?>class="active"<?php endif; ?>>
                                    <a href="" data-target=".catalog-item-<?= $block2_cnt ?>" aria-controls="catalog-item-<?= $block2_cnt ?>" role="tab" data-toggle="tab">
                                        <p <?php if ($block2_cnt == 1): ?>itemprop="name"<?php endif; ?>>
                                            <?= $arItem['NAME'] ?>, <?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> <span class="text-lowercase">м</span>
                                        </p>
                                        <p><?= $arItem["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] ?></p>
                                        <p>
                                            <?php echo number_format($arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"], 3, ' ', ' '); ?> р.

                                        </p>
                                    </a>
                                </li>
                                <?php $block2_cnt++; ?> 
                            <?php endforeach; ?>    
                        </ul> 
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <?php $block3_cnt = 1; ?>
                            <?php foreach ($arResult["HOUSE_INFORMATION"] as $arItem): ?>
                                <div role="tabpanel" class="tab-pane <?php if ($block3_cnt == 1): ?>active<?php endif; ?> catalog-item-<?= $block3_cnt ?>">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4 col-sm-4 col-md-offset-8 col-sm-offset-8">
                                            <?php if (is_array($arItem["PROPERTY_LIKE_HOUSE_VALUE"])): ?>
                                                <div class="attantion text-right">
                                                    <a href="<?= $arItem["PROPERTY_LIKE_HOUSE_VALUE"]["LINK_HOUSE"] ?>">
                                                        <div>
                                                            <div class="model-img"><img src="<?php echo CFile::GetPath($arItem["PROPERTY_LIKE_HOUSE_VALUE"]["PREVIEW_PICTURE"]); ?>" alt=""></div>
                                                        </div>
                                                        <div>
                                                            <p>
                                                                Обратите внимание на модель: <strong><?= $arItem["PROPERTY_LIKE_HOUSE_VALUE"]["NAME"] ?></strong>	
                                                            </p>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-8 col-sm-8">
                                                    <div class="row catalog-prices">
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <div class="row" <?php if ($block3_cnt == 1): ?>itemprop="offers" itemscope itemtype="http://schema.org/Offer"<?php endif; ?>>
                                                                <?php if ($arItem["PRICES"]["PROPERTY_PRICE_3_VALUE"][0]): ?>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="active">
                                                                            <p>Цена базовой комплектации при заказе оптимальной:</p>
                                                                            <p><?php echo number_format($arItem["PRICES"]["PROPERTY_OPTIMAL_PRICE_VALUE"], 3, ' ', ' '); ?> р.</p>
                                                                            <?php if ($block3_cnt == 1): ?><span itemprop="price" style="display: none;"><?php echo number_format($arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"], 3, ' ', ' '); ?></span><?php endif; ?>
                                                                            <?php if ($block3_cnt == 1): ?><span itemprop="priceCurrency" style="display: none;">RUB</span><?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="">
                                                                            <p style="margin-top: 14px;">Цена базовой комплектации:</p>
                                                                            <p><?php echo number_format($arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"], 3, ' ', ' '); ?> р.</p>
                                                                        </div>
                                                                    </div>



                                                                <?php else: ?>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="active">
                                                                            <p>Цена сегодня:</p>
                                                                            <p><?php echo number_format($arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"], 3, ' ', ' '); ?> р.</p>
                                                                            <?php if ($block3_cnt == 1): ?><span itemprop="price" style="display: none;"><?php echo number_format($arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"], 3, ' ', ' '); ?></span><?php endif; ?>
                                                                            <?php if ($block3_cnt == 1): ?><span itemprop="priceCurrency" style="display: none;">RUB</span><?php endif; ?>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="strikeprice">
                                                                            <p>Цена:</p>
                                                                            <p><?php echo number_format($arItem["PRICES"]["PROPERTY_PRICE_2_VALUE"], 3, ' ', ' '); ?> р.</p>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>


                                                        <?php if ($arItem["PRICES"]["PROPERTY_PRICE_3_VALUE"][0]): ?>
                                                            <div class="col-xs-12 col-md-6 col-sm-6">
                                                                <div class="strikeprice">
                                                                    <p style="margin-top: 27px;">Цена:</p>
                                                                    <p><?php echo number_format($arItem["PRICES"]["PROPERTY_PRICE_2_VALUE"], 3, ' ', ' '); ?> р.</p>
                                                                </div>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="col-xs-12 col-md-6 col-sm-6"></div>
                                                        <?php endif; ?>

                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">  
                                                    <button type="button" href="#" class="btn btn-primary" data-toggle="modal" data-target="#formAdviceItem" data-code="<?= $arItem["PROPERTY_CODE_1C_VALUE"] ?>" data-price="<?= $arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"] ?>000" data-whatever="Дом <?= $arItem["NAME"] ?>"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br/>консультацию</span></button>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <hr class="line-catalog">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <ul>
                                                        <li><p class="fsize-14">Модель: <strong><?= $arItem['NAME'] ?> </strong></p></li>
                                                        <li><p class="fsize-14">Размер: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></p></li>
                                                        <li><p class="fsize-14">Технология: <strong><?= $arItem["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] ?></strong></p></li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <?php if (count($arItem["PROPERTY_SERVICES_OPTIMAL_VALUE"]) > 1): ?> 
                                                        <div class="my-flex attantion-v2">
                                                            <div>
                                                                <h3>ВНИМАНИЕ!</h3>
                                                                <p><b style="font-weight:400 !important;">Оптимальная комплектация для этого дома по невероятной цене.</b> <a href="#TO" class="smooth">Читать подробнее</a></p> 
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="new-text">
                                                        <p class="fsize-14"><strong>СТАТЬИ и РЕКОМЕНДАЦИИ:</strong></p>
                                                        <p class="fsize-14"><noindex><a style="color: #333 !important;" href="/about/expert-opinion/tekhnologii-stroitelstva/">Какую технологию выбрать? Рекомендации специалистов.</a></noindex></p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <?php $block3_cnt++; ?> 
                                <?php $c ++; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?if ($USER->IsAdmin()):?>


<?endif;?>

<!--Нижняя часть  -->
<?php $block4_cnt = 1; ?> 
<?php foreach ($arResult["HOUSE_INFORMATION"] as $arItem): ?>
    <section class="content white catalog-item-<?= $block4_cnt ?> content-catalog-tab <?php if ($block4_cnt == 1): ?>active<?php endif; ?>" id="TO">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="catalog-item-description-2 white + my-margin">
                        <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-left && bg-grey && padding-navs-20 && padding-tabs-30-30 ))">
                            <!-- Nav tabs -->

                            <ul class="nav nav-tabs" role="tablist" >

                                <?php if ($arItem["PREVIEW_TEXT"]): ?>
                                    <?php if (count($arItem["PROPERTY_SERVICES_OPTIMAL_VALUE"]) <= 0): ?> 

                                        <?php if($arItem["PRICES"]["PROPERTY_KEY_PRICE_VALUE"]):?>
                                            <li role="presentation" >
                                            <?php else:?> 
                                                <li role="presentation" class="active">
                                                <?php endif;?>    
                                                
                                            <?php else: ?>
                                                <li role="presentation">
                                                <?php endif; ?>     
                                                <a href="#catalog-item-description<?= $block4_cnt ?><?= $arItem["ID"] ?>" aria-controls="catalog-item-description<?= $block4_cnt ?><?= $arItem["ID"] ?>" role="tab" data-toggle="tab">
                                                    Комплектация «Базовая»
                                                </a>
                                            </li>
                                        <?php endif; ?>


                                        <?php if (count($arItem["PROPERTY_SERVICES_OPTIMAL_VALUE"]) > 1): ?> 
                                            <li role="presentation" class="active">
                                                <a href="#catalog-item-description<?php echo ($block4_cnt + 2); ?><?= $arItem["ID"] ?>" aria-controls="catalog-item-description<?php echo ($block4_cnt + 2); ?><?= $arItem["ID"] ?>" role="tab" data-toggle="tab">
                                                    Комплектация «Оптимальная»
                                                </a>
                                            </li>
                                        <?php endif; ?>


                                        <?php if ($arItem["reviews"]): ?>    
                                            <li role="presentation">
                                                <a href="#catalog-item-description<?php echo ($block4_cnt + 1); ?><?= $arItem["ID"] ?>" aria-controls="catalog-item-description<?php echo ($block4_cnt + 1); ?><?= $arItem["ID"] ?>" role="tab" data-toggle="tab">
                                                    Отзывы
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                        <?if ($arItem["PRICES"]["PROPERTY_KEY_PRICE_VALUE"]):?>
                                        <li role="presentation" class="active">
                                            <a href="#catalog-item-description<?php echo ($block4_cnt + 3); ?><?= $arItem["ID"] ?>" aria-controls="catalog-item-description<?php echo ($block4_cnt + 3); ?><?= $arItem["ID"] ?>" role="tab" data-toggle="tab">
                                                Комплектация «Под ключ»
                                            </a>
                                        </li>
                                        <?endif;?>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <?php if ($arItem["PREVIEW_TEXT"]): ?>

                                            <?php if (count($arItem["PROPERTY_SERVICES_OPTIMAL_VALUE"]) <= 0): ?> 

                                                <?php if($arItem["PRICES"]["PROPERTY_KEY_PRICE_VALUE"]):?>
                                                    <div role="tabpanel" class="tab-pane" id="catalog-item-description<?= $block4_cnt ?><?= $arItem["ID"] ?>"> 
                                                    <?php else:?>
                                                        <div role="tabpanel" class="tab-pane active" id="catalog-item-description<?= $block4_cnt ?><?= $arItem["ID"] ?>">
                                                        <?php endif;?>


                                                    <?php else: ?>
                                                        <div role="tabpanel" class="tab-pane" id="catalog-item-description<?= $block4_cnt ?><?= $arItem["ID"] ?>">   
                                                        <?php endif; ?>

                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <p <?php if ($block4_cnt == 1): ?>itemprop="description"<?php endif; ?>> <?= html_entity_decode($arItem["PREVIEW_TEXT"]) ?></p>
                                                                <div class="more">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#description<?= $block4_cnt ?><?= $arItem["ID"] ?>" aria-expanded="false" aria-controls="description<?= $block4_cnt ?><?= $arItem["ID"] ?>">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12 col-md-2 col-sm-3 text-left">
                                                                                        <h4 class="catalog-item-description-2-title">
                                                                                            Комплектация
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-md-10 col-sm-9 text-left">
                                                                                        <span class="btn-close" style="text-decoration: underline;">показать</span>
                                                                                        <span class="btn-open" style="text-decoration: underline;">скрыть</span>
                                                                                    </div>
                                                                                </div>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <?if ($USER->IsAdmin()):?>
<!--                                                            <div class="my-collapse">
                                                                <div class="group">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                                            <p class="catalog-item-description-2-title">Брусчатый сруб</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row with-line">
                                                                        <div class="col-xs-12 col-md-2 col-sm-2">
                                                                            <p class="catalog-item-description-2-subtitle">Брусчатый сруб</p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-10 col-sm-10">
                                                                            <p>Стены из клееного бруса 100х180 мм Соединение бруса происходит по продольному шипу вдоль бруса круглыми металлическими нагелями (гвоздями) длиной 120 мм по засверленным отверстиям. Между брусьями укладывается утеплитель. Перегородки из клееного бруса 100х80 мм. Обшивка стен сан. узла − ОСП-6 по п/м 35х35 мм. Стены и перегородки устанавливаются на балки пола.</p>
                                                                        </div>
                                                                    </div>

                                                                </div>




                                                            </div>-->
                                                            <?endif;?>
                                                            <div class="collapse my-collapse" id="description<?= $block4_cnt ?><?= $arItem["ID"] ?>">
                                                               <?php foreach ($arItem["PROPERTY_COMPECTATION_HOUSE_VALUE"] as $k => $text): ?>

                                                                <?php if($text["TEXT"] != '.'):?>
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-md-2 col-sm-2">
                                                                            <p class="catalog-item-description-2-subtitle"><?= $arItem["PROPERTY_COMPECTATION_HOUSE_DESCRIPTION"][$k] ?></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-10 col-sm-10">
                                                                            <p><?= $text["TEXT"] ?></p>
                                                                        </div>
                                                                    </div>
                                                                <?php endif;?>


                                                            <?php endforeach; ?>
                                                            
                                                        </div>
                                                    </div>
                                                    <?if ($USER->IsAdmin()):?><div class="new-text"><p class="fsize-14"><strong>СТАТЬИ и РЕКОМЕНДАЦИИ:</strong> <noindex><a style="color: #333 !important;" href="/about/expert-opinion/tekhnologii-stroitelstva/">Какую технологию выбрать?</a></noindex> <noindex><a style="color: #333 !important;" href="/catalog/doma-dlya-postoyannogo-prozhivaniya/">Дома для постоянного проживания</a></noindex></p></div><?php endif; ?>

                                                    <div class="additions">
                                                        <p>*Условие «Без предоплаты» – предполагает строительство дома без предоплаты, оплата 20 % стоимости дома только после постройки фундамента.</p>
                                                        <p>**<a href="http://www.terem-pro.ru/" title="Строительство домов" target="_blank" >Строительство дома</a> допускается на грунтах всех категорий, кроме торфянников, просадочных и водонасыщенных грунтов. Каталог носит исключительно информационный характер и не является публичной офертой. Цены указаны на дома в базовых комплектациях. Изображения не отражают полную информацию о применяемых отделочных материалах и конструкциях домов. Компания оставляет за собой право вносить в проекты необходимые конструктивные изменения, направленные на совершенствование технологии производства и улучшение эксплуатационных качеств своих изделий.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if ($arItem["reviews"]): ?> 
                                        <div role="tabpanel" class="tab-pane" id="catalog-item-description<?php echo ($block4_cnt + 1); ?><?= $arItem["ID"] ?>">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <p>Мы публикуем избранные реальные отзывы владельцов именно этой модели. Почитать все отзывы или написать вопрос вы можете в <a href="#">этом разделе</a></p>
                                                </div>
                                            </div>

                                            <?php foreach ($arItem["reviews"] as $fields): ?>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="item-review">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-md-9 col-sm-9">
                                                                    <div class="inner">
                                                                        <p>
                                                                            <?= $fields["PREVIEW_TEXT"] ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-3 col-sm-3">
                                                                    <div class="date">
                                                                        <p><strong><?= $fields["NAME"] ?></strong></p>
                                                                        <p><?= $fields["PROPERTY_CITY_VALUE"] ?></p>
                                                                        <p><?= $fields["TIMESTAMP_X"] ?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>

                                        </div>
                                    <?php endif; ?>




                                    <?php if (count($arItem["PROPERTY_SERVICES_OPTIMAL_VALUE"]) > 1): ?> 
                                        <div role="tabpanel" class="tab-pane active" id="catalog-item-description<?php echo ($block4_cnt + 2); ?><?= $arItem["ID"] ?>">
                                        <?php else: ?>
                                            <div role="tabpanel" class="tab-pane" id="catalog-item-description<?php echo ($block4_cnt + 2); ?><?= $arItem["ID"] ?>">  
                                            <?php endif; ?>    
                                            <div class="row">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <h3 class="title-card-item">ОБРАТИТЕ ВНИМАНИЕ НА <span>ОПТИМАЛЬНОЕ РЕШЕНИЕ</span> ДЛЯ ЭТОГО ДОМА</h3><br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-4 col-sm-6">
                                                        <div class="holder-catalog-item-review">
                                                            <ul style="margin-bottom: 0px;">
                                                                <li><p class="fsize-14" style="margin-bottom: 0px;">Модель: <strong><?= $arItem['NAME'] ?> </strong></p></li>
                                                                <li><p class="fsize-14" style="margin-bottom: 0px;">Размер: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></p></li>
                                                                <li><p class="fsize-14" style="margin-bottom: 0px;">Технология: <strong><?= $arItem["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] ?></strong></p></li>
                                                                <li><p class="fsize-14" style="margin-bottom: 0px;">Тип: <strong>Комплектация «Оптимальная»</strong></p></li>
                                                            </ul>
                                                            <p style="font-size: 24px;font-weight: 700;color: red;margin-bottom: 5px;margin-top: 7px;">
                                                                <!--OPTIMAL_PRICE-->
                                                                <?php

                                                                if($arItem["ID"] == '7271'){


                                                                  echo "1 346 036 р. + подарок";

                                                              }else{
                                                                  echo number_format($arItem["OPTIMALPRICE"], 0, ' ', ' ')." р. + подарок";
                                                              }
                                                              ?>
                                                              <?php  ?> 
                                                          </p>
                                                          <p style="margin-bottom: 0px;" class="fsize-14">Вы получаете готовый теплый дом по минимальной цене на рынке</p>
                                                          <div class="catalog-item-description text-left">
                                                            <button type="button" href="#" class="btn btn-primary" data-toggle="modal" data-target="#formAdviceItem" data-price="<?= $arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"] ?>000" data-whatever="Дом <?= $arItem["NAME"] ?>" style="
                                                                /* float: left; */
                                                                width: auto;
                                                                padding-bottom: 0;
                                                                padding-top: 0;
                                                                "><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8 col-sm-6">
                                                    <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ПРЕИМУЩЕСТВА КОМПЛЕКТАЦИИ:</h3>
                                                    <ol style="margin-left: 25px;">
                                                        <?php if (count($arItem["PROPERTY_SERVICES_OPTIMAL_VALUE"]) > 1): ?> 

                                                            <?php foreach ($arItem["PROPERTY_SERVICES_OPTIMAL_VALUE"] as $v): ?>
                                                                <li>
                                                                    <p style="margin-bottom: 0px;"><?= $v ?></p>
                                                                </li>
                                                            <?php endforeach; ?>

                                                        <?php endif; ?>
                                                        <?php if (count($arItem["PROPERTY_PRESENTS_OPTIMAL_VALUE"]) > 0): ?>
                                                            <li>
                                                                <div>
                                                                    <div><p style="margin-bottom: 0px;font-size:14px;line-height: 1.5;"><strong>ПОДАРОК </strong> услуга или скидка на выбор:</p></div>
                                                                    <div>
                                                                        <div class="gift my-flex">
                                                                            <?php
                                                                            $c = 1;
                                                                            $c2 = count($arItem["PROPERTY_PRESENTS_OPTIMAL_VALUE"]);
                                                                            ?>   
                                                                            <?php foreach ($arItem["PROPERTY_PRESENTS_OPTIMAL_VALUE"] as $val): ?>

                                                                                <?php if ($c != $c2): ?>
                                                                                    <div>
                                                                                        <div class="inner-item">
                                                                                            <h3 class="text-uppercase"><?= $val ?></h3>
                                                                                        </div>

                                                                                    </div>
                                                                                    <div style="line-height: 45px;">
                                                                                        или
                                                                                    </div>
                                                                                <?php else: ?>
                                                                                    <div>
                                                                                        <div class="inner-item">
                                                                                            <h3 class="text-uppercase"><?= $val ?></h3>
                                                                                        </div>

                                                                                    </div>

                                                                                <?php endif; ?>
                                                                                <? $c++;?>
                                                                            <?php endforeach; ?>    
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        <?php else: ?>
                                                            <li>
                                                                <div>
                                                                    <div><p style="margin-bottom: 0px;font-size:14px;line-height: 1.5;"><strong>ПОДАРОК </strong> услуга или скидка на выбор:</p></div>
                                                                    <div>
                                                                        <div class="gift my-flex">
                                                                            <div>
                                                                                <div class="inner-item">
                                                                                    <h3 class="text-uppercase">Металлочерепица</h3>
                                                                                </div>
                                                                            </div>
                                                                            <div style="line-height: 45px;">
                                                                                или
                                                                            </div>
                                                                            <div>
                                                                                <div class="inner-item">
                                                                                    <h3 class="text-uppercase">Скидка</h3>
                                                                                </div>
                                                                            </div>  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>														
                                                        <?php endif; ?>
                                                    </ol>

                                                    <?php //if ($USER->IsAdmin()):?>
                                                    <br>
                                                    <div style="padding-left: 25px;">

                                                        <?php
                                                        $idSpecialHouse = array(
                                                            '0' => '6289',
                                                            '1' => '104414',
                                                            '2' => '104415',
                                                            '3' => '104416',
                                                            '4' => '104417',
                                                            '5' => '104418',
                                                            '6' => '104419',
                                                            '7' => '104420',
                                                            '8' => '7385',
                                                            '9' => '7386',
                                                            '10' => '7387',
                                                            '11' => '7388',
                                                            '12' => '7389',
                                                            '13' => '7390',
                                                            '14' => '7391',
                                                            '15' => '7392',
                                                            '16' => '254480',
                                                            '17' => '254484',
                                                            '18' => '254481',
                                                            '19' => '254485',
                                                            '20' => '254482',
                                                            '21' => '254486',
                                                            '22' => '254483',
                                                            '23' => '254487',
                                                            '24' => '7413',
                                                            '25' => '7414',
                                                            '26' => '7428',
                                                            '27' => '7429',
                                                            '28' => '7415',
                                                            '29' => '7416',
                                                            '30' => '7222',
                                                            '31' => '7227',
                                                            '32' => '7223',
                                                            '33' => '7228',
                                                            '34' => ' 7224',
                                                            '35' => '7229',
                                                            '36' => '7226',
                                                            '37' => '7231',
                                                            '38' => '7411',
                                                            '39' => '7412',
                                                            '40' => '254448',
                                                            '41' => '254454',
                                                            '42' => '254449',
                                                            '43' => '254455',
                                                            '44' => '254450',
                                                            '45' => '254456',
                                                            '46' => '254451',
                                                            '47' => '254457',
                                                            '48' => '254453',
                                                            '49' => '254458',
                                                            '50' => '254520',
                                                            '51' => '254521',
                                                            );

$cntSH = 0;
$cntUPDATE = 0;
foreach ($idSpecialHouse as $vid) {
    if ($cntSH == 0) {
        if ($arItem['ID'] == $vid) {
            echo "<p style='margin-bottom: 0px;'><strong>На все последующие дополнительные услуги скидка до 50%</strong></p>";
            $cntSH = 1;
            $cntUPDATE = 1;
        }
    }
}
if ($cntUPDATE == 0) {
    echo "<p style='margin-bottom: 0px;'><strong>На все последующие дополнительные услуги категории № 1 скидка до 50%</strong></p>";
}
?>


<p style='margin-bottom: 10px;' class="text-uppercase">К примеру:</p>
<?php if (is_array($arItem['CALC']) && count($arItem['CALC']) > 1): ?>
    <ul style="list-style-type: disc; padding-left: 17px;">
        <?php foreach ($arItem['CALC'] as $v): ?>
            <?php foreach ($v["ITEMS"] as $i): ?>
                <?php if ($i["NAME"] == 'Устройство водосточной системы VERAT до уровня цоколя (цвет коричневый)'): ?>
                    <li>
                        <p style="margin-bottom: 0;">Устройство водосточной системы VERAT до уровня цоколя (цвет коричневый)</p><p style="margin-bottom: 10px;"><strong><?= number_format($i["PRICE"], 0, ' ', ' ') ?> - 50% = <?php $nprice = ($i["PRICE"] / 2);
                        echo number_format($nprice, 0, ' ', ' ');
                        ?> рублей</strong></p>
                    </li>
                <?php endif; ?> 
                <?php if ($i["NAME"] == 'Замена входной двери тамбура ДФ филенчатой на дверь ДМ металлическую эконом-класса'): ?>
                    <li>
                        <p style="margin-bottom: 0;">Замена входной двери тамбура ДФ филенчатой на дверь ДМ металлическую эконом-класса</p><p style="margin-bottom: 10px;"><strong><?= number_format($i["PRICE"], 0, ' ', ' ') ?> - 50% = <?php $nprice = ($i["PRICE"] / 2);
                        echo number_format($nprice, 0, ' ', ' ');
                        ?> рублей</strong></p>
                    </li>
                <?php endif; ?> 
                <?php if ($i["NAME"] == 'Отделка дома сайдингом "VOX" (бежевый)'): ?>
                    <li>
                        <p style="margin-bottom: 0;">Отделка дома сайдингом "VOX" (бежевый)</p><p style="margin-bottom: 10px;"><strong><?= number_format($i["PRICE"], 0, ' ', ' ') ?> - 50% = <?php $nprice = ($i["PRICE"] / 2);
                        echo number_format($nprice, 0, ' ', ' ');
                        ?> рублей</strong></p>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>    
</div>
<?php //adminif endif;  ?>
</div>
</div>
<hr>
<div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="additions">
            <p>*Цена – действительна до 27.07.2016 г. при заказе «оптимальной» комплектации.</p>
            <p>**Условие «Без предоплаты» – предполагает строительство дома без предоплаты, оплата 20 % стоимости дома только после постройки фундамента.</p>
            <p>***<a href="http://www.terem-pro.ru/" title="Строительство домов" target="_blank" >Строительство дома</a> допускается на грунтах всех категорий, кроме торфянников, просадочных и водонасыщенных грунтов. Каталог носит исключительно информационный характер и не является публичной офертой. Цены указаны на дома в базовых комплектациях. Изображения не отражают полную информацию о применяемых отделочных материалах и конструкциях домов. Компания оставляет за собой право вносить в проекты необходимые конструктивные изменения, направленные на совершенствование технологии производства и улучшение эксплуатационных качеств своих изделий.</p>
        </div>
    </div>
</div>
</div>

</div>
<?if ($arItem["PRICES"]["PROPERTY_KEY_PRICE_VALUE"]):?>
<div role="tabpanel" class="tab-pane active" id="catalog-item-description<?php echo ($block4_cnt + 3); ?><?= $arItem["ID"] ?>">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
            <p>Свет горит, вода течет, тепло и уютно. Всё из одних рук с единым контролем качества и полной гарантийной ответственностью.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-4 col-sm-6">
            <div class="holder-catalog-item-review">
                <ul style="margin-bottom: 0px;">
                    <li><p class="fsize-14" style="margin-bottom: 0px;">Модель: <strong><?= $arItem['NAME'] ?> <?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></p></li>
                    <li><p class="fsize-14" style="margin-bottom: 0px;">Размер: <strong><?= $arItem["PROPERTY_SIZE_HOUSE_VALUE"] ?> м</strong></p></li>
                    <li><p class="fsize-14" style="margin-bottom: 0px;">Технология: <strong><?= $arItem["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] ?></strong></p></li>
                    <li><p class="fsize-14" style="margin-bottom: 0px;">Тип: <strong>Для постоянного проживания</strong></p></li>
                    <li><p class="fsize-14" style="margin-bottom: 0px;">Комплектация: <strong style="color: red;"> «Под ключ»</strong></p></li>
                </ul>
                <p style="font-size: 30px;font-weight: 700;color: red;margin-bottom: -15px;margin-top: 7px;" class="my-new-price">
                    <!--OPTIMAL_PRICE-->
                    <?php echo number_format($arItem["PRICES"]["PROPERTY_KEY_PRICE_VALUE"], 0, ' ', ' '); ?> р.
                </p>

                <div class="catalog-item-description text-left">
                    <button type="button" href="#" class="btn btn-primary" data-toggle="modal" data-target="#formAdviceItem" data-price="<?= $arItem["PRICES"]["PROPERTY_PRICE_1_VALUE"] ?>000" data-whatever="Дом <?= $arItem["NAME"] ?>" style="
                        /* float: left; */
                        width: auto;
                        padding-bottom: 0;
                        padding-top: 0;
                        "><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span>
                    </button>
                </div>
                <div style="margin: 80px -20px -20px -20px;">
                    <img src="/bitrix/templates/.default/assets/img/house-card-item.png" class="full-width" alt="">
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-md-8 col-sm-6">
            <div class="calog-item-desc-blocks text-uppercase">
                <div><p>Канализация</p></div>
                <div><p>отопление</p></div>
                <div><p>свет</p></div>
                <div><p>вода</p></div>
            </div>
            <h3 class="title-card-item text-uppercase" style="line-height: 1.3;margin-left:23px;margin-top: 8px;">дом «под ключ»<br>оптимальное решение для жизни <span class="text-lowercase" style="color: red;">в</span> доме</h3><br>


            <?php if($arItem["PRICES"]["PROPERTY_KEY_DECRIPTION_VALUE"] == 'Генерал Брусовой 180'):?>
                <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ПРЕИМУЩЕСТВА КОМПЛЕКТАЦИИ:</h3>
                <ol style="margin-left: 23px;">
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Фундамент ниже глубины промерзания, с ростверком</p>
                    </li>
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Пластиковые окна с двойным стеклопакетом</p>
                    </li>

                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Брус 100х180 мм</p>
                    </li>
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Утепление пола 180 ммс энергофлексом 2 мм</p>
                    </li>
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Утепление стен 2 этажа 200 мм</p>
                    </li>
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Сборный каркас второго этажа с шагом стоек 500 мм</p>
                    </li>
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Перегородки первого этажа брус 100х80 мм</p>
                    </li>
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Отделка сан.узлов пластиковыми панелями</p>
                    </li>
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Звукоизоляция перегородок на втором этаже</p>
                    </li>
                    <li>
                        <p style="margin-bottom: 0px;line-height:1.4;">Система вентиляции кухни и сан.узлов</p>
                    </li>
                    <li>
                       <p style="margin-bottom: 0px;line-height:1.4;"><strong>в ПОДАРОК:</strong> цементно-песчанная черепица BRAAS </p>
                   </li>
               </ol>
               <br>

               <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ИНЖЕНЕРНЫЕ СИСТЕМЫ:</h3>
               <ol style="margin-left: 23px;">
                <li>
                    <p style="margin-bottom: 0px;line-height:1.4;">Устройство однофазной электропроводки до 15 кВт</p>
                </li>
                <li>
                    <p style="margin-bottom: 0px;line-height:1.4;">Водяное отопление</p>
                </li>
                <li>
                    <p style="margin-bottom: 0px;line-height:1.4;">Газификация</p>
                </li>
                <li>
                    <p style="margin-bottom: 0px;line-height:1.4;">Система водоснабжения и вентилируемой канализации</p>
                </li>
                <li>
                    <p style="margin-bottom: 0px;line-height:1.4;">Скважина и водоподъем</p>
                </li>
                <li>
                    <p style="margin-bottom: 0px;line-height:1.4;">Установка станции биологической очистки</p>
                </li>
                <li>
                   <p style="margin-bottom: 0px;line-height:1.4;"><strong>в ПОДАРОК:</strong> монтаж инженерных систем скрытым способом</p>
               </li>
           </ol>

       <?php endif;?>

       <?php if($arItem["PRICES"]["PROPERTY_KEY_DECRIPTION_VALUE"] == 'Генерал Каркасный 200'):?>

        <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ПРЕИМУЩЕСТВА КОМПЛЕКТАЦИИ:</h3>
        <ol style="margin-left: 23px;">
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Фундамент ниже глубины промерзания, с ростверком</p>
            </li>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Пластиковые окна с двойным стеклопакетом</p>
            </li>

            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Панорамные окна</p>
            </li>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Утепление пола 180 ммс энергофлексом 2 мм</p>
            </li>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Утепление стен 1 и 2 этажа 200 мм</p>
            </li>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Сборный каркас с шагом стоек 500 мм, обшит снаружи осп 9 мм</p>
            </li>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Наружная отделка сайдингом</p>
            </li>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Отделка сан.узлов пластиковыми панелями</p>
            </li>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Звукоизоляция перегородок на первом и втором этажах</p>
            </li>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Система вентиляции кухни и сан.узлов</p>
            </li>
            <li>
               <p style="margin-bottom: 0px;line-height:1.4;"><strong>в ПОДАРОК:</strong> цементно-песчанная черепица BRAAS </p>
           </li>
       </ol>
       <br>

       <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ИНЖЕНЕРНЫЕ СИСТЕМЫ:</h3>
       <ol style="margin-left: 23px;">
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Устройство однофазной электропроводки до 15 кВт</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Водяное отопление</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Газификация</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Система водоснабжения и вентилируемой канализации</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Скважина и водоподъем</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Установка станции биологической очистки</p>
        </li>
        <li>
           <p style="margin-bottom: 0px;line-height:1.4;"><strong>в ПОДАРОК:</strong> монтаж инженерных систем скрытым способом</p>
       </li>
   </ol>
<?php endif;?>


<?php if($arItem["PRICES"]["PROPERTY_KEY_DECRIPTION_VALUE"] == 'Каркасный'):?>
    <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ПРЕИМУЩЕСТВА КОМПЛЕКТАЦИИ:</h3>
    <ol style="margin-left: 23px;">
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Фундамент ниже глубины промерзания, с ростверком</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Крепкий конструктив пола – нагрузка до 250 кг/кв.м</p>
        </li>
        <!-- <?php if($arResult['PROPERTIES']['SERIES']['VALUE'] == 'Викинг' || $arResult['PROPERTIES']['SERIES']['VALUE'] == 'Канцлер' || $arResult['PROPERTIES']['SERIES']['VALUE'] == 'Император'):?>
            <li>
                <p style="margin-bottom: 0px;line-height:1.4;">Панорамные окна</p>
            </li>
        <?php endif; ?> -->
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Обработка огнебиозащитой лаг пола и нижней обвязки</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Утепление пола 180 мм с энергофлексом 2 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Сборный каркас с шагом стоек 400 мм, обшит снаружи ОСП 9 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Утепление стен 1 и 2 этажа 200 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Звукоизоляция перегородок на первом и втором этажах</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Пластиковые окна с двойным стеклопакетом</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Гидроизоляция всех сан.узлов</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Система вентиляции кухни и сан.узлов</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Наружная отделка сайдингом</p>
        </li>
        <li>
        <p style="margin-bottom: 0px;line-height:1.4;"><strong>в ПОДАРОК:</strong> мягкая кровля и отделка сан.узлов</p>
       </li>
   </ol>
   <br>

   <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ИНЖЕНЕРНЫЕ СИСТЕМЫ:</h3>
   <ol style="margin-left: 23px;">
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Устройство однофазной электропроводки до 3-8 кВт</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Конвекторное отопление</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Система водоснабжения и вентилируемой канализации</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Скважина и водоподъем</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Установка станции биологической очистки</p>
    </li>
</ol>
<?php endif;?>


<?php if($arItem["PRICES"]["PROPERTY_KEY_DECRIPTION_VALUE"] == 'Каркасный Индивидуалка'):?>
    <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ПРЕИМУЩЕСТВА КОМПЛЕКТАЦИИ:</h3>
    <ol style="margin-left: 23px;">
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Фундамент ниже глубины промерзания, с ростверком</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Пластиковые окна с двойным стеклопакетом</p>
        </li>

        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Панорамные окна</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Утепление пола 180 ммс энергофлексом 2 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Утепление стен 1 и 2 этажа 200 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Сборный каркас с шагом стоек 500 мм, обшит снаружи осп 9 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Наружная отделка сайдингом</p>
        </li>

        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Звукоизоляция перегородок на первом и втором этажах</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Система вентиляции кухни и сан.узлов</p>
        </li>
        <li>
           <p style="margin-bottom: 0px;line-height:1.4;"><strong>в ПОДАРОК:</strong> мягкая кровля и отделка сан.узлов</p>
       </li>
   </ol>
   <br>

   <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ИНЖЕНЕРНЫЕ СИСТЕМЫ:</h3>
   <ol style="margin-left: 23px;">
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Устройство однофазной электропроводки до 15 кВт</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Водяное отопление</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Газификация</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Система водоснабжения и вентилируемой канализации</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Скважина и водоподъем</p>
    </li>
    <li>
        <p style="margin-bottom: 0px;line-height:1.4;">Установка станции биологической очистки</p>
    </li>
    <li>
       <p style="margin-bottom: 0px;line-height:1.4;"><strong>в ПОДАРОК:</strong> монтаж инженерных систем скрытым способом</p>
   </li>
</ol>
<?php endif;?>




<?php if($arItem["PRICES"]["PROPERTY_KEY_DECRIPTION_VALUE"] == 'Брусовой'):?>
    <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ПРЕИМУЩЕСТВА КОМПЛЕКТАЦИИ:</h3>
    <ol style="margin-left: 23px;">
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Фундамент ниже глубины промерзания, с ростверком</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Обработка огнебиозащитой лаг пола и нижней обвязки</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Брус 100х180 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Утепление пола 180 мм в четыре слоя с энергофлексом 2 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Перегородки первого этажа брус 100х80 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Сборный каркас второго этажа с шагом стоек 400 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Утепление стен второго этажа 200 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Звукоизоляция перегородок на втором этаже</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Пластиковые окна с двойным стеклопакетом</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Гидроизоляция сан.узлов</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Система вентиляции кухни и сан.узлов</p>
        </li>
        <li>
            <p style='margin-bottom: 0px;line-height:1.4;'><strong>в ПОДАРОК:</strong> мягкая кровля и отделка сан.узлов</p>
        </li>
    </ol>
    <br>

    <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ИНЖЕНЕРНЫЕ СИСТЕМЫ:</h3>
    <ol style="margin-left: 23px;">
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Устройство однофазной электропроводки до 3-8 кВт</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Конвекторное отопление</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Система водоснабжения и вентилируемой канализации</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Скважина и водоподъем</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Установка станции биологической очистки</p>
        </li>
    </ol>
<?php endif;?>


<?php if($arItem["PRICES"]["PROPERTY_KEY_DECRIPTION_VALUE"] == 'МБ'):?>
    <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ПРЕИМУЩЕСТВА КОМПЛЕКТАЦИИ:</h3>
    <ol style="margin-left: 23px;">
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Фундамент ниже глубины промерзания, с ростверком</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Обработка огнебиозащитой лаг пола и нижней обвязки</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Брус в два этажа 100х180 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Утепление пола 180 мм в четыре слоя с энергофлексом 2 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Перегородки двух этажей брус 100х80 мм</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Пластиковые окна с двойным стеклопакетом</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Гидроизоляция сан.узлов</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Система вентиляции кухни и сан.узлов</p>
        </li>
        <li>
            <p style='margin-bottom: 0px;line-height:1.4;'><strong>в ПОДАРОК:</strong> мягкая кровля и отделка сан.узлов</p>
        </li>
    </ol>
    <br>

    <h3 style="font-family: Arial;font-size: 16px;margin-top: 10px;margin-bottom: 30px;margin-left:23px;">ИНЖЕНЕРНЫЕ СИСТЕМЫ:</h3>
    <ol style="margin-left: 23px;">
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Устройство однофазной электропроводки до 3-8 кВт</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Конвекторное отопление</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Система водоснабжения и вентилируемой канализации</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Скважина и водоподъем</p>
        </li>
        <li>
            <p style="margin-bottom: 0px;line-height:1.4;">Установка станции биологической очистки</p>
        </li>
    </ol>
<?php endif;?>



</div>
</div>
<br>
<hr>
<div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="additions">
            <p>*Цена – действительна до 27.07.2016 г. при заказе «оптимальной» комплектации.</p>
            <p>**Условие «Без предоплаты» – предполагает строительство дома без предоплаты, оплата 20 % стоимости дома только после постройки фундамента.</p>
            <p>***<a href="http://www.terem-pro.ru/" title="Строительство домов" target="_blank">Строительство дома</a> допускается на грунтах всех категорий, кроме торфянников, просадочных и водонасыщенных грунтов. Каталог носит исключительно информационный характер и не является публичной офертой. Цены указаны на дома в базовых комплектациях. Изображения не отражают полную информацию о применяемых отделочных материалах и конструкциях домов. Компания оставляет за собой право вносить в проекты необходимые конструктивные изменения, направленные на совершенствование технологии производства и улучшение эксплуатационных качеств своих изделий.</p>
        </div>
    </div>
</div>
</div>
<?endif;?>
</div>

</div>
</div>


</div>
</div>
</section>
<?php $block4_cnt++; ?> 
<?php endforeach; ?>
</div>

<!-- banners malysh-6-bk-6-0kh6-6-->
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3 col-sm-3">
                <?php $block5_cnt = 1; ?> 
                <?php foreach ($arResult["HOUSE_INFORMATION"] as $arItem): ?>

                    <a href="/additional-service/<?php echo $arItem["CODE"]; ?>/" class="additional-services-item calc my-margin catalog-item-<?= $block5_cnt ?> content-catalog-tab <?php if ($block5_cnt == 1): ?>active<?php endif; ?>" target="_blank">
                        <h4 class="text-uppercase">
                            <span>Калькулятор</span><br>
                            дополнительных услуг 
                        </h4>
                        <p>
                        </p>
                    </a>

                    <?php $block5_cnt++; ?> 
                <?php endforeach; ?>    
            </div>
            <div class="col-xs-12 col-md-3 col-sm-3">
                <a href="/services/enginering_communications/" target="_blank" class="additional-services-item systems my-margin">
                    <h4 class="text-uppercase">
                        <span>Инженерные<br>системы</span>
                    </h4>
                    <p>

                    </p>
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-sm-3">
                <a href="/services/the-estates-catalogue/" class="additional-services-item gods my-margin" target="_blank">
                    <h4 class="text-uppercase">
                        <span>Благоустройство</span><br>и усадьбы
                    </h4>
                    <p>
                        Комплексное благоустройство или отдельные виды работ на Вашем участке
                    </p>
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-sm-3">
                <a href="tel:+7 (495) 721-14-31" class="additional-services-item phons my-margin">
                    <h4 class="text-uppercase">
                        отдел продаж:<br><span id="telMain3">+7 (495) 721-18-00</span>
                    </h4>
                    <p>
                        Работаем без выходных с 10:00<br>до 20:00    
                    </p>
                </a>
            </div>
        </div>
    </div>
</section>

