<?php

global $PRICE_IBLOCK_ID;

$CODE = $_REQUEST['ELEMENT_CODE'];


$arSelect = Array("ID", "NAME", "PROPERTY_ID_CALCULATOR", "PREVIEW_PICTURE", "PROPERTY_PRICE_HOUSE_KAZAN");
$arFilter = Array("IBLOCK_ID" => 21, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "CODE" => $CODE);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);
while ($ob = $res->GetNextElement()) {
    $arFields[] = $ob->GetFields();
}


$cnt2 = 0;
$arSelect2 = Array(
    "ID",
    "NAME",
    "PROPERTY_PRICE_1",
    "PROPERTY_PRICE_2",
    "PROPERTY_PRICE_3",
    "PROPERTY_OPTIMAL_PRICE",
    "PROPERTY_DISCONT1",
    "PROPERTY_DISCONT2"
);
foreach ($arFields as $k => $item) {
    $arFilter2 = Array(
        "IBLOCK_ID" => $PRICE_IBLOCK_ID,
        "ID" => $item["PROPERTY_PRICE_HOUSE_KAZAN_VALUE"],
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y"
    );
    $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelect2);
    while ($ob = $res->GetNextElement()) {
        $arFields[$k]["PRICES"] = $ob->GetFields();
    }
}

$file = $_SERVER["DOCUMENT_ROOT"].'/upload/calcs/' . $arFields[0]["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' . $arFields[0]["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
//error_reporting(E_ALL);
$f = fopen($file, 'r');
$file_data = fread($f, filesize($file));
fclose($f);
$result = unserialize($file_data);

//if($USER->IsAdmin()){
//    //echo "<pre>";
//    //var_dump();
//}


$PR = $arFields[0]['PRICES']["PROPERTY_PRICE_1_VALUE"];
if($arFields[0]['PRICES']["PROPERTY_PRICE_3_VALUE"][0]){
    $PR = $arFields[0]['PRICES']["PROPERTY_PRICE_3_VALUE"][0];
}

if($arFields[0]['PRICES']["PROPERTY_OPTIMAL_PRICE_VALUE"]){
    $OPTIMAL_PRICE = $arFields[0]['PRICES']["PROPERTY_OPTIMAL_PRICE_VALUE"];
    $PR = $arFields[0]['PRICES']["PROPERTY_OPTIMAL_PRICE_VALUE"];
}

$discont1 = 30;
$discont2 = 50;

if($arFields[0]['PRICES']['PROPERTY_DISCONT1_VALUE']){
    $discont1 = $arFields[0]['PRICES']['PROPERTY_DISCONT1_VALUE'];
}
if($arFields[0]['PRICES']['PROPERTY_DISCONT2_VALUE'] !== null){
    $discont2 = $arFields[0]['PRICES']['PROPERTY_DISCONT2_VALUE'];
}



if ($arFields[0]["PROPERTY_ID_CALCULATOR_VALUE"]) {
    $IBLOCK_ID = 24;
    $SECTION_CODE = $arFields[0]["PROPERTY_ID_CALCULATOR_VALUE"];
    $Categoryes = Array();
    $arFilter = Array('IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y', "SECTION_ID" => $SECTION_CODE);
    $Sections = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, true);
    $Categoryes['NAME'] = $arFields[0]['NAME'];
    $Categoryes['PICTURE_ID'] = $arFields[0]['PREVIEW_PICTURE'];
    $Categoryes['PRICE'] = $PR;
    $Categoryes['PRICE_OPTIMAL'] = $OPTIMAL_PRICE;
    $Categoryes['DISCONT1'] = $discont1;
    $Categoryes['DISCONT2'] = $discont2;
    $arResult = $Categoryes;
    $arResult["CALCS"] = $result;
}
?> 



