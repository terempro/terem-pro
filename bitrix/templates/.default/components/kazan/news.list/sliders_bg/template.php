<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>



<section class="content content-back-side hidden-xs start-show pointer-" data-parallax="up" data-opacity-main="true">
    <div class="container-fluid reset-padding">
        <div class="row reset-margin">
            <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">

<!--                <div class="star-toggler">-->
<!--                    <div class="starlight-trigger">-->
<!--                        <button class="off"></button>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="stars_bg hidden">-->
<!--                    <div class="planet_bg">-->
<!--                        <img src="/bitrix/templates/.default/assets/img/PL1.png">-->
<!--                    </div>-->
<!--                    <div class="stars"></div>-->
<!--                    <div class="twinkling"></div>-->
<!--                </div>-->

                <div class="my-item-slider back-side-slider" data-item="slider-back-side">

                    <?php $cnt = 0; ?>

                    <?php foreach ($arResult["ITEMS"] as $arItem): ?>


                     <div class="item">
                         <a href="<?= $arItem["PROPERTIES"]["LINK"]["~VALUE"]?>">
                            <div class="inner">
                                <div class="main-img" style="background-image: url('<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>');">
                                   <canvas id="canvas_<?=$cnt?>"  style="width: 100% !important;"></canvas>
                               </div>
                               

                            
                            <div class="baner">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="inner with-top-padding content text-right">
                                                <p><?= $arItem["NAME"]?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a> 
                </div>


                    <?php $cnt++; ?>
                <?php endforeach; ?>





                </div>
            </div>
        </div>
    </div>
</section>




