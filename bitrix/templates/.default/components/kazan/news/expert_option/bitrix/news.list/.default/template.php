<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<?= $arResult["NAV_STRING"] ?>
<?endif;?>

<?php $cnt = 0; ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>


<?php if ($cnt == 0): ?>
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
            <div class="flex-reversed">
                <div class="row">    
                <?php endif; ?>            
                <div class="col-xs-12 col-md-6 col-sm-12">
                    <div class="my-promotion desgin-3 + my-margin + bordered">
                        <div>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]['src']; ?>" alt=""></a>
                        </div>
                        <div>
                            <h4 class="text-uppercase red"><?php echo $arItem['NAME']; ?></h4>
                            <p><?php echo substr($arItem['PREVIEW_TEXT'], 0, 150); ?>...</p>
                        </div>
                        <a class="all-item-link" href="<?= $arItem["DETAIL_PAGE_URL"]; ?>"></a>
                    </div>
                </div>

                <?php $cnt++; ?>


                <?php if ($cnt == 2): ?> 
                    <?php $cnt = 0; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?> 



<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<?= $arResult["NAV_STRING"] ?>
<?endif;?>

