<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$APPLICATION->AddChainItem("Мнение эекспертов: поиск по тегам");
?>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix">
                    <div class="my-text-title text-center text-uppercase desgin-h1">
                        <h1>Поиск по тегу: <?php echo $_GET['tags']?HTMLToTxt($_GET['tags']):''; ?></h1>  
                        <noindex>
                            <a href="/about/expert-opinion/">Назад</a>
                        </noindex>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="flex-reversed expert">
                                <?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
                                <?elseif($arResult["ERROR_CODE"]!=0):?>
                                <?elseif(count($arResult["SEARCH"])>0):?>
                                <?foreach($arResult["SEARCH"] as $arItem):?>
                                <?php
                                    $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE");
                                    $arFilter = Array("IBLOCK_ID" => 30, "ID" => $arItem["ITEM_ID"]);
                                    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);
                                    while ($ob = $res->GetNextElement()) {
                                        $arFields = $ob->GetFields();
                                    }
                                ?>
                                <div class="row">
                                    <div class="flex-row">
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                            <div class="expert-item">
                                                <h3><a href="#"><?php echo $arItem['TITLE']; ?></a></h3>
                                                <p><?php echo substr($arItem['BODY'], 0, 150); ?>...</p>
                                                <a href="<?php echo $arItem['URL']; ?>">Подробнее</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                            <img src="<?php echo CFile::GetPath($arFields["PREVIEW_PICTURE"]); ?>" class="full-width expert-margin" alt="">
                                        </div>
                                    </div>
                                </div>
                                <?endforeach;?>
                                <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>

                                <?else:?>
                                <?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                    <noindex>
                        <a href="/about/expert-opinion/">Назад</a>
                    </noindex>
                </div>
            </div>
        </div>
    </div>
</section>
