<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "терем заборы, терем ограждения, заборы, ограждения");
$APPLICATION->SetPageProperty("title", "Ограждения от компании Теремъ, заборы из сетки-рабицы, профнастила и кирпича");
$APPLICATION->SetTitle("Ограждения от компании Теремъ, заборы из сетки-рабицы, профнастила и кирпича");
$APPLICATION->AddHeadScript("/bitrix/templates/.default/assets/js/fense.js");
?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    Ограждения
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>Неприкосновенность частной жизни и личная безопасность – вот то, к чему стремится каждый человек в наше время. В этом может помочь качественное и надежное ограждение собственного участка.</p>
                            <p>Специально для своих клиентов компания «Теремъ» подготовила выгодное ценовое предложение по установке качественного и надежного ограждения участка.</p>
                            <!--<p>НОВИНКА! 3D забор – всего от 1 100 руб. /п.м. Идеально подходит для ограждения участка со стороны соседей, не создавая ощущение замкнутого пространства, но обеспечивая надежную защиту дому. Образец данного ограждения представлен на площадке, и Вы сможете лично убедиться в его преимуществах!</p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12" id="category">
                <div class="flex-reversed">
                    <!--OLD catalog list-->
                    <!--small villages -->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/fenseu/item-1.jpg" alt="Евроштакетник">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="">
                                        ЕВРОШТАКЕТНИК<br>от 1 470 РУБ. за п/м
                                    </h4>
                                    <p>
                                        Евроштакетник представляет собой металлические полосы шириной около 12 см и длиной либо 2, либо 1,8 метра. При его производстве используются оцинкованные стальные листы покрытые слоем полимера, защищающим от воздействий внешней среды. </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/fenseu/item-2.jpg" alt="ПРОФНАСТИЛ">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="">
                                        ПРОФНАСТИЛ<br>от 820 РУБ. за п/м
                                    </h4>
                                    <p>
                                        Профнастил - это современный строительный материал, который представляет из себя стальной лист, оцинкованный, предварительно подвергнутый профилированию, для придания ему большей жесткости, а так же покрытый полимерной эмалью. 
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                    </div><!--end-->
                    <!--small villages -->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/fenseu/item-3.jpg" alt="3D забор">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                         3D ЗАБОР<br>от 1 280 РУБ. за п/м
                                    </h4>
                                    <p>
                                        3D забор отличает технология изготовления – на плоскости панели формируется пространственный изгиб, который играет роль дополнительной жесткости, присутствие изгибов придает ограждению особое преимущество – прочность.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/fenseu/item-4.jpg" alt="СЕТКА-РАБИЦА">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        СЕТКА-РАБИЦА<br>от 310 РУБ. за п/м
                                    </h4>
                                    <p>
                                        Проволочная сетка-рабица - популярный и недорогой способ ограждения территорий.Одно из главных достоинств забора из сетки-рабицы - его прозрачность, он не затеняет участок.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                    </div><!--end-->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12 my-margin">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-red">
                                <h2>
                                    ПРЕИМУЩЕСТВА ЗАКАЗА в «ТЕРЕМЕ»
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Бесплатная доставка 250 км от базы
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Работа без предоплаты на стандартный профиль С8
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Бесплатная консультация и составление тех. задания
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Быстрый и легкий монтаж и демонтаж  старого забора
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p class="text-padding-bottom">Вам не обязательно приезжать к нам в офис для того, чтобы заказать ограждение, достаточно просто позвонить по телефону:<br>+7 (495) 215-29-55 или воспользоваться формой заказа: </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
if (CModule::IncludeModule("iblock")) {
    $IBLOCK_ID = 34;
    $euro = 29085; //Евроштакетник
    $prof = 29084; //Профнастил


    $arSelect = Array(
        "ID",
        "NAME",
        "PROPERTY_DLINA",
        "PROPERTY_VISOTA",
        "PROPERTY_COLOR",
        "PROPERTY_SHAG",
        "PROPERTY_KRASKA",
        "PROPERTY_W1",
        "PROPERTY_W2",
        "PROPERTY_PRICE"
    );

    //Выбор профнастила
    $arFilter = Array("IBLOCK_ID" => IntVal($IBLOCK_ID), "SECTION_ID" => IntVal($prof), "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 200), $arSelect);
    while ($ob = $res->GetNextElement()) {
        $Prof[] = $ob->GetFields();
    }
    foreach ($Prof as $item) {
        $PROF_COLOR[] = $item["PROPERTY_COLOR_VALUE"];
        $PROF_VISOTA[] = $item["PROPERTY_DLINA_VALUE"];
        $PROF_KRASKA[] = $item["PROPERTY_KRASKA_VALUE"];
        $PROF_TOLSH[] = $item["PROPERTY_W1_VALUE"];
    }
    $PROF_COLOR_UNIQ = array_unique($PROF_COLOR);
    $PROF_VISOTA_UNIQ = array_unique($PROF_VISOTA);
    $PROF_KRASKA_UNIQ = array_unique($PROF_KRASKA);
    $PROF_TOLSH_UNIQ = array_unique($PROF_TOLSH);
//конец профнастила

//Выбор евроштакетиника
    $arFilter = Array("IBLOCK_ID" => IntVal($IBLOCK_ID), "SECTION_ID" => IntVal($euro), "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 200), $arSelect);
    while ($ob = $res->GetNextElement()) {
        $Euro[] = $ob->GetFields();
    }
    
    foreach($Euro as $eitem){
        $EURO_SHAG[] = $eitem["PROPERTY_SHAG_VALUE"];
        $EURO_VISOTA[] = $eitem["PROPERTY_DLINA_VALUE"];
        $EURO_COLOR[] = $eitem["PROPERTY_COLOR_VALUE"];
        $EURO_KRASKA[] = $eitem["PROPERTY_KRASKA_VALUE"];
    }
    
    $EURO_SHAG_UNIQ = array_unique($EURO_SHAG);
    $EURO_VISOTA_UNIQ = array_unique($EURO_VISOTA);
    $EURO_COLOR_UNIQ = array_unique($EURO_COLOR);
    $EURO_KRASKA_UNIQ = array_unique($EURO_KRASKA);
//конец евроштакетиника
 
//Выбор 3D
    $arFilter = Array("IBLOCK_ID" => IntVal($IBLOCK_ID), "SECTION_ID" => 29087, "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 200), $arSelect);
    while ($ob = $res->GetNextElement()) {
        $d3[] = $ob->GetFields();
    }
    

    
    foreach($d3 as $ditem){
        $D3_VISOTA[] = $ditem["PROPERTY_DLINA_VALUE"];
        $D3_COLOR[] = $ditem["PROPERTY_COLOR_VALUE"];
    }
    
    $D3_VISOTA_UNIQ = array_unique($D3_VISOTA);
    $D3_COLOR_UNIQ = array_unique($D3_COLOR);
    
    
//конец 3D
    
//Выбор Rab
    $arFilter = Array("IBLOCK_ID" => IntVal($IBLOCK_ID), "SECTION_ID" => 29086, "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 200), $arSelect);
    while ($ob = $res->GetNextElement()) {
        $rab[] = $ob->GetFields();
    }
    
   
    
    foreach($rab as $ritem){
        $RAB_VISOTA[] = $ritem["PROPERTY_DLINA_VALUE"];
    }
    
    $RAB_VISOTA_UNIQ = array_unique($RAB_VISOTA);
//конец Rab    
}
?>
<section class="content text-page white " role="content">
    <div class="container my-margin">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-left && bg-grey && bg-tab-content-white && padding-40 ))">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" aria-expanded="true">
                                Профнастил
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" aria-expanded="false">
                                Евроштакетник
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" aria-expanded="false">
                                3D-забор
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab" aria-expanded="false">
                                Сетка-рабица
                            </a>
                        </li>

                    </ul>
                    <!-- Tab panes -->

                    <div class="tab-content">
                        <div id="test"></div>
                        <div role="tabpanel" class="tab-pane active" id="tab1">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <form class="form-fense new" data-toggle="validator" action="" method="post" role="form" >
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Толщина, мм</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="ProfTolsh" id="ProfTolsh" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach ($PROF_TOLSH_UNIQ as $v): ?>
                                                                                <option value="<?= $v ?>" selected=""><?= $v ?></option>
                                                                            <?php endforeach; ?>    
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Высота, мм</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="ProfVisota" id="ProfVisota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach ($PROF_VISOTA_UNIQ as $v): ?>
                                                                                <option value="<?= $v ?>"><?= $v ?> </option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Цвет</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="ProfColor" id="ProfColor" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach ($PROF_COLOR_UNIQ as $v): ?>
                                                                                <option value="<?= $v ?>" selected=""><?= $v ?></option>
                                                                            <?php endforeach; ?>    
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Окрашивание</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="ProfKraska" id="ProfKraska" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach ($PROF_KRASKA_UNIQ as $v): ?>
                                                                                <?php if (strlen($v) > 0): ?>
                                                                                    <option value="<?= $v ?>"><?= $v ?></option>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Ворота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="ProfVorota" id="ProfVorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="none" selected="">без ворот</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">3 шт.</option>
                                                                            <option value="4">4 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Калитка</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="ProfKalitka" id="ProfKalitka" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="none" selected="">Без калитки</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">3 шт.</option>
                                                                            <option value="4">4 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Расстояние до участка, км*</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="number" name="PromKM" id="PromKM" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Общая длина забора, м</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="number" name="ProfZKM" id="ProfZKM" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>ФИО</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="ProfName" id="ProfName" class="form-control" placeholder="Игор"  autocomplete="off">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Телефон</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="tel" name="ProfPhone" id="ProfPhone" class="form-control" placeholder="+7 (999) 000 00 00" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Почта</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="email" name="ProfMail" id="ProfMail" class="form-control" placeholder="Email" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <div class="fense-all-calc">
                                                    <div>
                                                        <div>Итого:</div>
                                                        <div id="Profprice">0 руб.</div>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <div><button data-item="fense-calc" type="button" class="btn btn-default text-red text-uppercase" id="ProfRschet">расчитать</button></div>
                                                            <div><button data-item="fense-send" type="submit" class="btn btn-default text-red text-uppercase" id="ProfRschetPost">Отправить заявку</button></div>
                                                        </div>
                                                        <div>
                                                             <p id="ProfError" class="form-error text-red visible-xs visible-md visible-sm visible-lg"></p>
                                                            <p>Заявка не накладывает на Вас никаких обязательств и является только запросом на получение информации</p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p class="form-attantion">*Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. Стоимость доставки – ?? руб./км. Если Ваш участок находится менее 100 км от производственной базы, доставка бесплатно.</p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab2">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <form class="form-fense new" data-toggle="validator" action="" method="post" role="form" >
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Шаг между секциями</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="EroShag" id="EroShag" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach($EURO_SHAG_UNIQ as $v):?>
                                                                                <option value="<?=$v?>" selected=""><?=$v?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" name="id_request" value="zabor1"/>
                                                                    <input type="hidden" name="ipadress" class="" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Высота, мм</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="EroVisota" id="EroVisota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach($EURO_VISOTA_UNIQ as $v):?>
                                                                                <option value="<?=$v?>" selected=""><?=$v?></option>
                                                                            <?php endforeach; ?>    
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Цвет</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="EroColor" id="EroColor" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach($EURO_COLOR_UNIQ as $v):?>
                                                                                <option value="<?=$v?>" selected=""><?=$v?></option>
                                                                            <?php endforeach; ?>    
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Окрашивание</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="EroKraska" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach($EURO_KRASKA_UNIQ as $v):?>
                                                                                <option value="<?=$v?>" selected=""><?=$v?></option>
                                                                            <?php endforeach; ?>     
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Ворота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="EroVorota" id="EroVorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="none" selected="">без ворот</option>
                                                                            <option value="1">1 шт</option>
                                                                            <option value="2">2 шт</option>
                                                                            <option value="3">3 шт</option>
                                                                            <option value="4">4 шт</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Калитка</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="EroKalitka" id="EroKalitka" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="none" selected="">Без калитки</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">3 шт.</option>
                                                                            <option value="4">4 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Расстояние до участка, км*</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    
                                                                        <input type="number" name="EroRast" id="EroRast" class="form-control" placeholder="0"  autocomplete="off">
                                                                   
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Общая длина забора, м</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="number" name="EroDlz" id="EroDlz" class="form-control" placeholder="0"  autocomplete="off">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>ФИО</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="EroName" id="EroName" class="form-control" placeholder="Игор"  autocomplete="off">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Телефон</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="tel" name="EroPhone" id="EroPhone" class="form-control" placeholder="+7 (999) 000 00 00" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Почта</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="email" name="EroMail" id="EroMail" class="form-control" placeholder="Email" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <div class="fense-all-calc">
                                                    <div>
                                                        <div>Итого:</div>
                                                        <div id="EroPrice">0 руб.</div>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <div><button data-item="fense-calc" type="button" id="EroRaschet" class="btn btn-default text-red text-uppercase">расчитать</button></div>
                                                            <div><button data-item="fense-send" type="submit" class="btn btn-default text-red text-uppercase" id="EroPost">Отправить заявку</button></div>
                                                        </div>
                                                        <div>
                                                            <p id="EroError" class="form-error text-red visible-xs visible-md visible-sm visible-lg"></p>
                                                            <p>Заявка не накладывает на Вас никаких обязательств и является только запросом на получение информации</p>
                                                        </div>
                                                    </div>
                                                </div>
                                           
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p class="form-attantion">*Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. Стоимость доставки – ?? руб./км. Если Ваш участок находится менее 100 км от производственной базы, доставка бесплатно.</p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab3">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <form class="form-fense new" data-toggle="validator" action="" method="post" role="form" >
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-2 col-sm-2">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Высота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="D3Visota" id="D3Visota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach($D3_VISOTA_UNIQ as $v):?>
                                                                                <option value="<?=$v?>"><?=$v?></option>
                                                                            <?php endforeach;?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-2 col-sm-2">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Цвет</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="D3Color" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach($D3_COLOR_UNIQ as $v):?>
                                                                                <option value="<?=$v?>"><?=$v?></option>
                                                                            <?php endforeach;?>    
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-2 col-sm-2">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Ворота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="D3Vorota" id="D3Vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="none" selected="">Без ворот</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">3 шт.</option>
                                                                            <option value="4">4 шт.</option>tion>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-2 col-sm-2">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Калитка</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="D3Kalitka" id="D3Kalitka" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="none" selected="">Без ворот</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">3 шт.</option>
                                                                            <option value="4">4 шт.</option>tion>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Расстояние до участка, км*</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="number" name="D3Rast" id="D3Rast" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Общая длина забора, м</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="number" name="D3Dlin" id="D3Dlin" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>ФИО</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="D3Name" id="D3Name" class="form-control" placeholder="Игор" autocomplete="off" id="phone">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Телефон</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="tel" name="D3Phone" id="D3Phone" class="form-control" placeholder="+7 (999) 000 00 00" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Почта</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="email" name="D3Mail" id="D3Mail" class="form-control" placeholder="Email" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <p class="form-attantion">*Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. Стоимость доставки – ?? руб./км. Если Ваш участок находится менее 100 км от производственной базы, доставка бесплатно.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <div class="fense-all-calc">
                                                    <div>
                                                        <div>Итого:</div>
                                                        <div id="D3Price">0  руб.</div>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <div><button id="D3Raschet" data-item="fense-calc" type="button" class="btn btn-default text-red text-uppercase">расчитать</button></div>
                                                            <div><button data-item="fense-send" type="submit" class="btn btn-default text-red text-uppercase" id="D3Post">Отправить заявку</button></div>
                                                        </div>
                                                        <div>
                                                            <p id="D3Error" class="form-error text-red visible-xs visible-md visible-sm visible-lg"></p>
                                                            <p>Заявка не накладывает на Вас никаких обязательств и является только запросом на получение информации</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab4">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <form class="form-fense new" data-toggle="validator" action="" method="post" role="form" >
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Высота, мм</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="RabVisota" id="RabVisota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <?php foreach($RAB_VISOTA_UNIQ as $v):?>
                                                                            <option value="<?=$v?>" selected=""><?=$v?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Ворота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="RabVorota" id="RabVorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="none" selected="">Без ворот</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">3 шт.</option>
                                                                            <option value="4">4 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Калитока</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="RabKalitaka" id="RabKalitaka" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="none" selected="">Без калитки</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">3 шт.</option>
                                                                            <option value="4">4 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Расстояние до участка, км*</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="number" name="RabRast" id="RabRast" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Общая длина забора, м</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="RabDlin" id="RabDlin" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>ФИО</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="RabName" id="RabName" class="form-control" placeholder="Игор"  autocomplete="off">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Телефон</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="tel" name="RabPhone" id="RabPhone" class="form-control" placeholder="+7 (999) 000 00 00" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Почта</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="email" name="RabMail" id="RabMail" class="form-control" placeholder="Email" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <p class="form-attantion">*Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. Стоимость доставки – ?? руб./км. Если Ваш участок находится менее 100 км от производственной базы, доставка бесплатно.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <div class="fense-all-calc">
                                                    <div>
                                                        <div>Итого:</div>
                                                        <div id="RabPrice">0 руб.</div>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <div><button data-item="fense-calc" type="button" id="RabRaschet" class="btn btn-default text-red text-uppercase">расчитать</button></div>
                                                            <div><button data-item="fense-send" type="submit" class="btn btn-default text-red text-uppercase" id="RabPost">Отправить заявку</button></div>
                                                        </div>
                                                        <div>
                                                             <p id="RabError" class="form-error text-red visible-xs visible-md visible-sm visible-lg"></p>
                                                            <p>Заявка не накладывает на Вас никаких обязательств и является только запросом на получение информации</p>
                                                        </div>
                                                    </div>
                                           
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>