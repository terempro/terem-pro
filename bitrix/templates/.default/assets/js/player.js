function player() {
    const videoContainers = [...document.querySelectorAll('.video--container')];
    let videoContainersInactive = [...document.querySelectorAll('.video--container[data-status="inactive"]')];

    function refreshContainers() {
        videoContainersInactive = [...document.querySelectorAll('.video--container[data-status="inactive"]')];
    }

    function play(video) {
        video.play();
        video.nextElementSibling.style.display = 'none';
    }

    function pause(video) {
        video.pause();
        video.nextElementSibling.style.display = 'block';
    }

    function toggle(video) {
        video.paused ? play(video) : pause(video)
    }

    function pauseAll() {
        videoContainers.forEach(container => {
            pause(container.querySelector('video'));
            deactivate(container)
        })
    }

    function pauseAllInactive() {
        videoContainersInactive.forEach(container => {
            pause(container.querySelector('video'))
        })
    }

    function activate(container) {
        container.dataset.status = "active";
    }

    function deactivate(container) {
        container.dataset.status = "inactive";
    }

    videoContainers.forEach(container => {
        const video = container.querySelector('video');
        function handleVideo() {
            videoContainers.forEach(deactivate);
            activate(container);
            refreshContainers();
            pauseAllInactive();
            toggle(video);
        }
        container.addEventListener('click', handleVideo);

        function pauseOn(mutations) {
            mutations.forEach(mutation => {
                if (!target.classList.contains('active')) {
                    pause(video);
                }
            })
        }

        const target = container.parentElement.parentElement;
        const observer = new MutationObserver(pauseOn);
        observer.observe(target, {
            attributes: true
        });
    })
}

document.addEventListener('DOMContentLoaded', player);
