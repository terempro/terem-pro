/*myMap END*/
(function(){
        var container = document.getElementById('zoom-map');
        var map = container.children[0];
        map.style.position = "relative";
        var mapHeight = map.offsetHeight;
        var mapWidth = map.offsetWidth;
        // console.log(mapHeight,mapWidth);

        container.style.height = mapHeight - 60 + 'px';
        container.style.cursor = 'move';
        container.style.width = mapWidth - 60 + 'px';
        map.onmousedown = function(e) {
          var mapCoords = getCoords(map);
          var shiftX = e.pageX - mapCoords.left;
          var shiftY = e.pageY - mapCoords.top;
          var containerCoords = getCoords(container);
          document.onmousemove = function(e) {
            var newLeft = e.pageX - shiftX - containerCoords.left;
            var newTop = e.pageY - shiftY - containerCoords.top;
            var rightEdge = map.offsetWidth - container.offsetWidth;
            var bottomEdge = map.offsetHeight - container.offsetHeight;

            if (newLeft > 0) {
              newLeft = 0;
            }
            if(newTop > 0){
                newTop = 0;
            }

            if (newLeft < -rightEdge) {
              newLeft = -rightEdge;

            }
            if(newTop < -bottomEdge){
                newTop = -bottomEdge;
            }
            map.style.left = newLeft + 'px';
            map.style.top = newTop + 'px';
          }

          document.onmouseup = function() {
            document.onmousemove = document.onmouseup = null;
          };

           return false;
        };
        var zoomInBtn = document.getElementsByClassName('btn-zoom-in');
        var zoomOutBtn = document.getElementsByClassName('btn-zoom-out');
        map.oncontextmenu = function () {
            map.style.left =  '0px';
            map.style.top =  '0px';
            map.style.height = '100%';
            map.style.width = '100%';
            return false;
        }

        map.ondragstart = function() {
          return false;
        };
        map.addEventListener('dblclick', function(e){
            map.style.height = 'auto';
            map.style.width = 'auto';
        });
        $(zoomInBtn).on('click', function(){
            map.style.height = 'auto';
            map.style.width = 'auto';
        });
        $(zoomOutBtn).on('click', function(){
            map.style.left =  '0px';
            map.style.top =  '0px';
            map.style.height = '100%';
            map.style.width = '100%';
        });

        function getCoords(elem) {
          var box = elem.getBoundingClientRect();

          return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset,
            right: box.right - pageXOffset,
            bottom: box.bottom - pageYOffset
          };

        }

})();


jQuery(document).ready(function ($) {

    /*map*/
    $('#map-section').hide();

    $('#map-section-toggler').on('click', function() {
        $('#map-section').toggle();
    });

    var myMap1;
    var myMap2;
    var myMap3;
    var myMap4;
    var myMap5;
    var myMap6;
    var myMap7;
    var myMap8;
    var myMap9;
    var myMap10;
    var myMap11;
    var link = location.href;

    if(link.match(/kazan/))
    {
        // console.log('KAZAN');
        ymaps.ready(init2);
    }
    else if (link.match(/vologda/))
    {
        // console.log('VOLOGDA');
        ymaps.ready(init3);
    }
    else if (link.match(/tula/))
    {
        // console.log('TULA');
        ymaps.ready(init4);
    }
    else if (link.match(/bryansk/))
    {
        // console.log('TULA');
        ymaps.ready(init5);
    }
    else if (link.match(/saratov/))
    {
        // console.log('TULA');
        ymaps.ready(init6);
    }
    else if (link.match(/novosibirsk/))
    {
        // console.log('TULA');
        ymaps.ready(init7);
    }
    else if (link.match(/izhevsk/))
    {
        // console.log('TULA');
        ymaps.ready(init8);
    }
    else if (link.match(/krasnodar/))
    {
        // console.log('TULA');
        ymaps.ready(init9);
    }
    else if (link.match(/ryazan/))
    {
        // console.log('TULA');
        ymaps.ready(init10);
    }
    else if (link.match(/vladimir/))
    {
        // console.log('TULA');
        ymaps.ready(init11);
    }
    else if (link.match(/yaroslavl/))
    {
        // console.log('yaroslavl');
        ymaps.ready(init12);
    }
    else
    {
        // console.log('MOSCOW');
        ymaps.ready(init1);
    }

    $('a[href="#2a"]').one('click', function () {
        init2();
        myMap2.container.fitToViewport();
    });

    function init1() {
        myMap1 = new ymaps.Map('map', {
            center: [55.70200328, 37.76498550],
            zoom: 16,
            controls: []
        }),
                myMap1.behaviors
                .disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([55.70200328, 37.76498550], {
            hintContent: [
                ''
            ].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap1.geoObjects.add(myPlacemark);
    }
    function init2() {
        myMap2 = new ymaps.Map('map2', {
            center: [55.778183, 49.190954],
            zoom: 16,
            controls: []
        }),
                myMap2.behaviors
                .disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([55.778183, 49.190954], {
            hintContent: [
                ''
            ].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap2.geoObjects.add(myPlacemark);
    }
    function init3()
    {
        myMap3 = new ymaps.Map('map3', {
            center: [59.198953, 39.818833],
            zoom: 16,
            controls: []
        }),
                myMap3.behaviors
                .disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([59.198953, 39.818833], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap3.geoObjects.add(myPlacemark);
    }
    
    function init4()
    {
        myMap4 = new ymaps.Map('map4', {
            center: [54.203477, 37.621940],
            zoom: 16,
            controls: []
        }),
                myMap4.behaviors
                .disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([54.203477, 37.621940], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap4.geoObjects.add(myPlacemark);
    }

    function init5()
    {
        myMap5 = new ymaps.Map('map5', {
            center: [53.219942, 34.427538],
            zoom: 16,
            controls: []
        }),
                myMap5.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([53.219942, 34.427538], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap5.geoObjects.add(myPlacemark);
    }

    function init6()
    {
        myMap6 = new ymaps.Map('map6', {
            center: [51.465535, 46.135937],
            zoom: 16,
            controls: []
        }),
                myMap6.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([51.465535, 46.135937], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap6.geoObjects.add(myPlacemark);
    }

    function init7()
    {
        myMap7 = new ymaps.Map('map7', {
            center: [55.0372456, 82.9312426],
            zoom: 16,
            controls: []
        }),
                myMap7.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([55.0372456, 82.9312426], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap7.geoObjects.add(myPlacemark);
    }

    function init8()
    {
        myMap8 = new ymaps.Map('map8', {
            center: [56.910639, 53.232710],
            zoom: 16,
            controls: []
        }),
                myMap8.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([56.910639, 53.232710], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap8.geoObjects.add(myPlacemark);
    }

    function init9()
    {
        myMap9 = new ymaps.Map('map9', {
            center: [45.251963, 38.998868],
            zoom: 16,
            controls: []
        }),
                myMap9.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([45.251963, 38.998868], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap9.geoObjects.add(myPlacemark);
    }
    function init10()
    {
        myMap10 = new ymaps.Map('map10', {
            center: [54.628836, 39.730664],
            zoom: 16,
            controls: []
        }),
                myMap10.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([54.628836, 39.730664], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap10.geoObjects.add(myPlacemark);
    }
    function init11()
    {
        myMap11 = new ymaps.Map('map11', {
            center: [56.170225, 40.396708],
            zoom: 16,
            controls: []
        }),
                myMap11.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([56.170225, 40.396708], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap11.geoObjects.add(myPlacemark);
    }
    function init12()
    {
        myMap11 = new ymaps.Map('map11', {
            center: [57.600157, 39.875788],
            zoom: 16,
            controls: []
        }),
            myMap11.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom'])
        myPlacemark = new ymaps.Placemark([57.600157, 39.875788], {
            hintContent: [''].join('')
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/templates/.default/assets/img/map-pin.png',
            iconImageSize: [47, 54],
            iconImageOffset: [-24, -54]
        });
        myMap11.geoObjects.add(myPlacemark);
    }

    /*map END*/
});
