

$(document).ready(function () {
    var toForm = $('.form-fense.new');
    var preloader = '<div class="preloading"><img style="position:absolute; top: 50%; left:50%; margin-left:-16px; margin-top:-16px;" src="/bitrix/templates/.default/assets/js/preloader.gif" alt=Загрузка..." /></div>';
    //Расчет профнастила
    $('#ProfRschet').click(function () {
        var error = 1;
        var error2 = 1;
        var type = 'prof';
        var ProfTolsh = $('#ProfTolsh').val();
        var ProfVisota = $('#ProfVisota').val();
        var ProfColor = $('#ProfColor').val();
        var ProfVorota = $('#ProfVorota').val();
        var ProfKalitka = $('#ProfKalitka').val();
        var PromKM = document.getElementById('PromKM').value;
        var ProfZKM = document.getElementById('ProfZKM').value;


        var disk = $("#ProfKraska").attr('disabled');
        if (disk != 'disabled') {
            var ProfKraska = $('#ProfKraska').val();
        } else {
            var ProfKraska = 'disabled';
        }



        if (PromKM.toString().length > 0) {
            PromKM = parseInt(PromKM);
            error2 = 0;
            $('#ProfError').text('');
        } else {
            $('#ProfError').text('Ошибка! «Расстояние до участка»');
            error2 = 1;

        }

        if (error2 == 0) {
            if (ProfZKM > ' ') {
                ProfZKM = parseInt(ProfZKM);
                error = 0;
                $('#ProfError').text('');
            } else {
                $('#ProfError').text('Ошибка! «Общая длина забора»');
                error = 1;
            }
        }








        if (error == 0) {

            $(preloader).appendTo(toForm);
            toForm.addClass('disable');
            $.ajax({url: '/include/calcfense.php',
                type: 'POST',
                data: {
                    type: type,
                    ProfTolsh: ProfTolsh,
                    ProfVisota: ProfVisota,
                    ProfColor: ProfColor,
                    ProfVorota: ProfVorota,
                    ProfKalitka: ProfKalitka,
                    ProfKraska: ProfKraska,
                    PromKM: PromKM,
                    ProfZKM: ProfZKM
                },
                success: function (result) {
                    $('#response').remove();
                    if (result.toString().trim() != 'ERROR') {
                        $('#Profprice').text(result.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " руб.");
                        $('#ProfError').text('');
                    } else {
                        $('#Profprice').text('0 руб.');
                        $('#ProfError').text('К сожалению по выбранным параметрам расчет не возможен. Измените параметры.')
                    }
                    toForm.removeClass('disable');
                    $('.preloading').fadeOut(500, function () {
                        $(this).remove();
                    });
                }
            });

        }

    });


    $('#EroRaschet').click(function () {
        var error = 1;
        var error2 = 1;
        var type = 'ero';
        var EroDlz = document.getElementById('EroDlz').value;
        var EroRast = document.getElementById('EroRast').value;
        var EroVorota = $('#EroVorota').val();
        var EroKraska = $('#EroKraska').val();
        var EroColor = $('#EroColor').val();
        var EroVisota = $('#EroVisota').val();
        var EroShag = $('#EroShag').val();
        var EroKalitka = $('#EroKalitka').val();


        if (EroDlz > ' ') {
            EroDlz = parseInt(EroDlz);
            error2 = 0;
            $('#EroError').text('');
        } else {
            $('#EroError').text('Ошибка! «Общая длина забора»');
            error2 = 1;
        }

        if (error2 == 0) {
            if (EroRast > ' ') {
                EroRast = parseInt(EroRast);
                error = 0;
                $('#EroError').text('');
            } else {
                $('#EroError').text('Ошибка! «Расстояние до участка»');
                error = 1;
            }
        }






        if (error == 0) {
            $(preloader).appendTo(toForm);
            toForm.addClass('disable');
            $.ajax({url: '/include/calcfense.php',
                type: 'POST',
                data: {
                    type: type,
                    EroDlz: EroDlz,
                    EroRast: EroRast,
                    EroVorota: EroVorota,
                    EroKraska: EroKraska,
                    EroColor: EroColor,
                    EroVisota: EroVisota,
                    EroShag: EroShag,
                    EroKalitka: EroKalitka
                },
                success: function (result) {
                    $('#response').remove();
                    if (result.toString().trim() != 'ERROR') {
                        $('#EroPrice').text(result.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " руб.");
                        $('#EroError').text('');
                    } else {
                        $('#EroPrice').text('0 руб.');
                        $('#EroError').text('К сожалению по выбранным параметрам расчет не возможен. Измените параметры.')
                    }
                    toForm.removeClass('disable');
                    $('.preloading').fadeOut(500, function () {
                        $(this).remove();
                    });
                }
            });
        }

    });

    $('#D3Raschet').click(function () {
        var type = 'd3';
        var error = 1;
        var error2 = 1;
        var D3Dlin = document.getElementById('D3Dlin').value;
        var D3Rast = document.getElementById('D3Rast').value;
        var D3Kalitka = $('#D3Kalitka').val();
        var D3Vorota = $('#D3Vorota').val();
        var D3Color = $('#D3Color').val();
        var D3Visota = $('#D3Visota').val();

        if (D3Dlin > ' ') {
            D3Dlin = parseInt(D3Dlin);
            error2 = 0;
            $('#D3Error').text('');
        } else {
            $('#D3Error').text('Ошибка! «Общая длина забора»');
            error2 = 1;
        }

        if (error2 == 0) {
            if (D3Rast > ' ') {
                D3Rast = parseInt(D3Rast);
                error = 0;
                $('#D3Error').text('');
            } else {
                $('#D3Error').text('Ошибка! «Расстояние до участка»');
                error = 1;
            }
        }







        if (error == 0) {
            $(preloader).appendTo(toForm);
            toForm.addClass('disable');
            $.ajax({url: '/include/calcfense.php',
                type: 'POST',
                data: {
                    type: type,
                    D3Dlin: D3Dlin,
                    D3Rast: D3Rast,
                    D3Kalitka: D3Kalitka,
                    D3Vorota: D3Vorota,
                    D3Color: D3Color,
                    D3Visota: D3Visota
                },
                success: function (result) {
                    $('#response').remove();
                    if (result.toString().trim() != 'ERROR') {
                        $('#D3Price').text(result.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " руб.");
                        $('#D3Error').text('');
                    } else {
                        $('#D3Price').text('0 руб.');
                        $('#D3Error').text('К сожалению по выбранным параметрам расчет не возможен. Измените параметры.')
                    }
                    toForm.removeClass('disable');
                    $('.preloading').fadeOut(500, function () {
                        $(this).remove();
                    });
                }
            });
        }

    });

    $('#RabRaschet').click(function () {
        var type = 'rab';
        var error = 1;
        var error2 = 1;
        var RabVisota = $('#RabVisota').val();
        var RabVorota = $('#RabVorota').val();
        var RabKalitaka = $('#RabKalitaka').val();
        var RabRast = document.getElementById('RabRast').value;
        var RabDlin = document.getElementById('RabDlin').value;




        if (RabDlin > ' ') {
            RabDlin = parseInt(RabDlin);
            error2 = 0;
            $('#RabError').text('');
        } else {
            $('#RabError').text('Ошибка! «Общая длина забора»');
            error2 = 1;
        }

        if (error2 == 0) {
            if (RabRast > ' ') {
                RabRast = parseInt(RabRast);
                error = 0;
                $('#RabError').text('');
            } else {
                $('#RabError').text('Ошибка! «Расстояние до участка»');
                error = 1;
            }
        }








        if (error == 0) {
            $(preloader).appendTo(toForm);
            toForm.addClass('disable');
            $.ajax({url: '/include/calcfense.php',
                type: 'POST',
                data: {
                    type: type,
                    RabVisota: RabVisota,
                    RabVorota: RabVorota,
                    RabKalitaka: RabKalitaka,
                    RabRast: RabRast,
                    RabDlin: RabDlin
                },
                success: function (result) {
                    $('#response').remove();
                    if (result.toString().trim() != 'ERROR') {
                        $('#RabPrice').text(result.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " руб.");
                        $('#RabError').text('');
                    } else {
                        $('#RabPrice').text('0 руб.');
                        $('#RabError').text('К сожалению по выбранным параметрам расчет не возможен. Измените параметры.')
                    }
                    toForm.removeClass('disable');
                    $('.preloading').fadeOut(500, function () {
                        $(this).remove();
                    });
                }
            });
        }
    });




    $("#ProfColor").change(function () {
        var result = $(this).val();
        if (result == 'оцинкованный') {
            $("#ProfKraska").attr("disabled", "disabled");
        } else {
            $("#ProfKraska").removeAttr("disabled");
        }
    });

    $("#ProfRschetPost").click(function () {
        var zabor = "Забор из профнастила";
        var ProfTolsh = $('#ProfTolsh').val();
        var ProfVisota = $('#ProfVisota').val();
        var ProfColor = $('#ProfColor').val();
        var ProfVorota = $('#ProfVorota').val();
        var ProfKalitka = $('#ProfKalitka').val();
        var ProfKraska = $('#ProfKraska').val();
        var PromKM = document.getElementById('PromKM').value;
        var ProfZKM = document.getElementById('ProfZKM').value;
        var user = $('#ProfName').val();
        var phone = $('#ProfPhone').val();
        var mail = $('#ProfMail').val();
        var request_type = $('#ProfRequestType').val();
        $.post('/include/request_handler.php', {
                zabor: zabor,
                id_request: "zabor1",
                vorota: ProfVorota,
                kalitka: ProfKalitka,
                thickness: ProfTolsh,
                height: ProfVisota,
                color: ProfColor,
                kraska: ProfKraska,
                km: PromKM,
                dl: ProfZKM,
                user: user,
                phone: phone,
                mail: mail,
                request_type: request_type
            }, function (data) {
                console.log(data);
            });

        if (user.toString().length > 0 && mail.toString().length > 0 && phone.toString().length > 0) {
            
            
            
            $.post('http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/', {
                zabor: zabor,
                id_request: "zabor1",
                vorota: ProfVorota + " Калитка- " + ProfKalitka + " Цвет- " + ProfColor + " Высота- " + ProfVisota + " Растояние до участка-" + PromKM + " Длина забора- " + ProfZKM,
                km: PromKM,
                dl: ProfZKM,
                user: user,
                phone: phone,
                mail: mail
            }, function () {
            });
            $('#ProfName').val('');
            $('#ProfPhone').val('');
            $('#ProfMail').val('');
            $('#ProfError').text('');
            $('#thx').modal('show');
        } else {
            $('#ProfError').text('Заполните пожалуйста все поля!');
        }






        return false;
    });


    $('#EroPost').click(function () {
        var zabor = "Забор из евроштакетиника";
        var EroDlz = document.getElementById('EroDlz').value;
        var EroRast = document.getElementById('EroRast').value;
        var EroVorota = $('#EroVorota').val();
        var EroKraska = $('#EroKraska').val();
        var EroColor = $('#EroColor').val();
        var EroVisota = $('#EroVisota').val();
        var EroShag = $('#EroShag').val();
        var EroKalitka = $('#EroKalitka').val();
        var user = $('#EroName').val();
        var phone = $('#EroPhone').val();
        var mail = $('#EroMail').val();
        var request_type = $('#EroRequestType').val();
        $.post('/include/request_handler.php', {
                zabor: zabor,
                id_request: "zabor1",
                vorota: EroVorota,
                kalitka: EroKalitka,
                step: EroShag,
                height: EroVisota,
                color: EroColor,
                kraska: EroKraska,
                km: EroRast,
                dl: EroDlz,
                user: user,
                phone: phone,
                mail: mail,
                request_type: request_type
            }, function (data) {
                console.log(data);
            });


        if (user.toString().length > 0 && mail.toString().length > 0 && phone.toString().length > 0) {
            $.post('http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/', {
                zabor: zabor,
                id_request: "zabor1",
                vorota: EroVorota + " Калитка- " + EroKalitka + " Цвет- " + EroColor + " Высота- " + EroVorota + " Растояние до участка-" + EroRast + " Длина забора- " + EroDlz,
                km: EroRast,
                dl: EroDlz,
                user: user,
                phone: phone,
                mail: mail
            }, function () {
            });

            $('#EroName').val('');
            $('#EroPhone').val('');
            $('#EroMail').val('');

            $('#EroError').text('');
            $('#thx').modal('show');
        } else {
            $('#EroError').text('Заполните пожалуйста все поля!');
        }


        return false;

    });

    $('#D3Post').click(function () {
        var zabor = "Забор из 3D";
        var D3Dlin = document.getElementById('D3Dlin').value;
        var D3Rast = document.getElementById('D3Rast').value;
        var D3Kalitka = $('#D3Kalitka').val();
        var D3Vorota = $('#D3Vorota').val();
        var D3Color = $('#D3Color').val();
        var D3Visota = $('#D3Visota').val();
        var user = $('#D3Name').val();
        var phone = $('#D3Phone').val();
        var mail = $('#D3Mail').val();
        var request_type = $('#3DRequestType').val();
        $.post('/include/request_handler.php', {
                zabor: zabor,
                id_request: "zabor1",
                vorota: D3Vorota,
                kalitka: D3Kalitka,
                height: D3Visota,
                color: D3Color,
                km: D3Rast,
                dl: D3Dlin,
                user: user,
                phone: phone,
                mail: mail,
                request_type: request_type
            }, function (data) {
                console.log(data);
            });


        if (user.toString().length > 0 && mail.toString().length > 0 && phone.toString().length > 0) {
            $.post('http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/', {
                zabor: zabor,
                id_request: "zabor1",
                vorota: D3Vorota + " Калитка- " + D3Kalitka + " Цвет- " + D3Color + " Высота- " + D3Visota + " Растояние до участка-" + D3Rast + " Длина забора- " + D3Dlin,
                km: D3Rast,
                dl: D3Dlin,
                user: user,
                phone: phone,
                mail: mail
            }, function () {
            });


            $('#D3Name').val('');
            $('#D3Phone').val('');
            $('#D3Mail').val('');
            $('#D3Error').text('');
            $('#thx').modal('show');
        } else {
            $('#D3Error').text('Заполните пожалуйста все поля!');
        }


        

        return false;
    });


    $('#RabPost').click(function (e) {
        var zabor = "Забор из рабицы";
        var RabVisota = $('#RabVisota').val();
        var RabVorota = $('#RabVorota').val();
        var RabKalitaka = $('#RabKalitaka').val();
        var RabRast = document.getElementById('RabRast').value;
        var RabDlin = document.getElementById('RabDlin').value;
        var user = $('#RabName').val();
        var phone = $('#RabPhone').val();
        var mail = $('#RabMail').val();
        var Color = '';
        var request_type = $('#RabRequestType').val();
        $.post('/include/request_handler.php', {
                zabor: zabor,
                id_request: "zabor1",
                vorota: RabVorota,
                kalitka: RabKalitaka,
                height: RabVisota,
                km: RabRast,
                dl: RabDlin,
                user: user,
                phone: phone,
                mail: mail,
                request_type: request_type
            }, function (data) {
                console.log(data);
            });
        

        if (user.toString().length > 0 && mail.toString().length > 0 && phone.toString().length > 0) {
            $.post('http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/', {
                zabor: zabor,
                id_request: "zabor1",
                vorota: RabVorota + " Калитка- " + RabKalitaka + " Цвет- " + Color + " Высота- " + RabVisota + " Растояние до участка-" + RabRast + " Длина забора- " + RabDlin,
                km: RabRast,
                dl: RabDlin,
                user: user,
                phone: phone,
                mail: mail
            }, function () {
            });

            $('#RabName').val('');
            $('#RabPhone').val('');
            $('#RabMail').val('');
            $('#RabError').text('');
            $('#thx').modal('show');
        } else {
            $('#RabError').text('Заполните пожалуйста все поля!');
        }



        

        return false;
    });



});

