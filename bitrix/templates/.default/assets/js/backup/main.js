'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var logo = 'down';

var Helper = {
    ajax: 'true',
    tech: 'karkas',
    price_brus: 'false',
    price_karkas: 'false',
    price_kirpich: 'false',
    comp_brus: 'false',
    comp_karkas: 'false',
    comp_kirpich: 'false',
    dislike: 'false',
    like: 'false',
    write_params: 'false'

};

jQuery(document).ready(function ($) {

    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,

    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    offset_opacity = 1200,

    //duration of the top scrolling animation (in ms)
    scroll_top_duration = 700,

    //grab the "back to top" link
    $back_to_top = $('.cd-top');
    $('a.smooth').click(function () {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top }, 1000);
        return false;
    });
    //hide or show the "back to top" link
    $(window).scroll(function () {
        $(this).scrollTop() > offset ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
        // var scroll_r = $(this).scrollTop();
        // var parallaxFix = $('.inner.with-top-padding.content');
        // parallaxFix.css('top', (- scroll_r * 0.95));
    });
    //smooth scroll to top
    $back_to_top.on('click', function (event) {
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0
        }, scroll_top_duration);
    });

    var text = $('.space-text').html();

    //Скрытие секций карточек кроме первой при начальной загрузке
    // $('.house-slider-tab').show();

    $('.starlight-trigger').find('button').on('click', function (e) {
        $(this).toggleClass('on off');
        // $('.start-show').toggleClass('pointer-disable');
        // $('.start-show').find('.starlight').toggleClass('hidden');
        // $('.slider-navigation').toggleClass('op-0');
        $('.stars_bg').toggleClass('hidden');
        $('.back-side-slider').toggleClass('hidden');
        var text = $('.space-text').html();
        //console.log($('.slider-navigation').hasClass('op-0'));
        if ($('.slider-navigation').hasClass('op-0')) {
            $('.slider-navigation').css('pointer-events', 'none');
        } else {
            $('.slider-navigation').css('pointer-events', 'auto');
        }
        if (text === '<h4>C днём<br>космонавтики!</h4>') {
            //console.log(text);
            $('.space-text').html('<h4>ДОМ, который<br>нужен мне</h4>');
        } else {
            //console.log(text);
            $('.space-text').html('<h4>C днём<br>космонавтики!</h4>');
        }
    });

    // $('.starlight-trigger').toggle(function() {
    //     console.log('on');
    //     $('.space-text').html('<h4>C днём<br>космонавтики!</h4>');
    // }, function() {
    //      console.log('off');
    //     $('.space-text').html('<h4>ДОМ, который<br>нужен мне</h4>');
    // });

    //*****Калькулятор карточки дома***//

});

function send() {
    var form = $('[data-form="send"]');
    form.ajaxForm(function () {
        $('.variant__modal').modal('hide');
        $('#formAdvice').modal('hide');
        $('#call').modal('hide');
        $('#formAdviceItem').modal('hide');
        $('#thx').modal('show');
        $(form).clearForm();
    });
}
$(document).ready(function () {
    $('#send_series').click(function () {
        var p = parseInt($(this).val());
        yaCounter937330.reachGoal('SEND_REQUEST', { price: p });

        //return true;
    });
    $('#send_item').click(function () {
        var p = parseInt($(this).val());
        yaCounter937330.reachGoal('ORDER_ITEM', { price: p });
        //return true;
    });

    $('.countdown').countdown('2017/04/13').on('update.countdown', function (event) {
        var $this = $(this).html(event.strftime(''
        // + '<span>%-d</span> day%!d '
        + '<span>%H</span>' + ' : ' + '<span>%M</span>' + ' : ' + '<span>%S</span>'));
    });

    /*Countdown main.page*/

    // var endDate = new Date(2017, 3, 12);
    // $('.countdown').countdown({
    //     date: endDate,
    //     render: function(data,secondsTitle) {
    //         function loopSec(){
    //             var secondsTitle = 'секунд';
    //             switch(data.sec) {
    //                 case 1:
    //                 case 21:
    //                 case 31:
    //                 case 41:
    //                 case 51: 
    //                 secondsTitle = 'секунда';
    //                 break;
    //                 case 2:
    //                 case 22:
    //                 case 23:
    //                 case 24:
    //                 case 32:
    //                 case 33:
    //                 case 34:
    //                 case 42:
    //                 case 43:
    //                 case 44:
    //                 case 52:
    //                 case 53:
    //                 case 54:
    //                 secondsTitle = 'секунды';
    //                 break;
    //                 default:
    //                 secondsTitle = 'секунд';
    //             }
    //             return secondsTitle;
    //         };
    //         function loopMin(){
    //             var secondsTitle = 'минут';
    //             switch(data.min) {
    //                 case 1:
    //                 case 21:
    //                 case 31:
    //                 case 41:
    //                 case 51: 
    //                 secondsTitle = 'минута';
    //                 break;
    //                 case 2:
    //                 case 22:
    //                 case 23:
    //                 case 24:
    //                 case 32:
    //                 case 33:
    //                 case 34:
    //                 case 42:
    //                 case 43:
    //                 case 44:
    //                 case 52:
    //                 case 53:
    //                 case 54:
    //                 secondsTitle = 'минуты';
    //                 break;
    //                 default:
    //                 secondsTitle = 'минут';
    //             }
    //             return secondsTitle;
    //         };
    //         function loopHour(){
    //             var secondsTitle = 'часов';
    //             switch(data.hours + data.days * 24) {
    //                 case 1:
    //                 case 21:
    //                 case 31:
    //                 case 41:
    //                 case 51: 
    //                 secondsTitle = 'час';
    //                 break;
    //                 case 49: 
    //                 secondsTitle = 'часов';
    //                 break;
    //                 case 2:
    //                 case 22:
    //                 case 23:
    //                 case 24:
    //                 case 32:
    //                 case 33:
    //                 case 34:
    //                 case 42:
    //                 case 43:
    //                 case 44:
    //                 case 52:
    //                 case 53:
    //                 case 54:
    //                 secondsTitle = 'часа';
    //                 break;
    //                 default:
    //                 secondsTitle = 'часов';
    //             }
    //             return secondsTitle;
    //         };
    //         loopHour();
    //         loopMin();
    //         loopSec();
    //         $(this.el).html("<div><span class='time-count'>" + this.leadingZeros(data.hours + data.days * 24) + "</span><span class='time-desc'>" + loopHour() + "</span></div><div><span class='time-count'>" + this.leadingZeros(data.min, 2) + "</span><span class='time-desc'>" + loopMin() + "</span></div><div><span class='time-count'>" + this.leadingZeros(data.sec, 2) + "</span><span class='time-desc'>"+ loopSec() +"</span></div>");
    //     }
    // });
    /*Countdown END*/
    var form = $('[data-form="send"]');
    var formPhoto = $('[data-form="send-photo"]');
    $(formPhoto).validator();
    var optionsPhoto = {
        target: '#test1', // target element(s) to be updated with server response 
        beforeSubmit: showRequest, // pre-submit callback 
        success: showResponse, // post-submit callback 
        dataType: 'json'
    };

    $(formPhoto).ajaxForm(optionsPhoto);

    function showRequest(formData, jqForm, options) {
        var queryString = $.param(formData);
        return true;
    }
    function processJson(data, $form) {
        // 'data' is the json object returned from the server 
        if (data.error) {
            $('#test1').html('Вы загрузили более  либо менее 8-ми фото!');
        } else {
            $('#nubmerPhoto').html('Заявка отправлена. Вашей заявке присвоен № ' + data.id);
            $('#test1').html(' ');
        }
    }
    function showResponse(responseText, statusText, xhr, $form) {

        if (responseText.error) {
            $('#test1').html('Вы загрузили более  либо менее 8-ми фото!');
        } else {
            $('#nubmerPhoto').html('Заявка отправлена. Вашей заявке присвоен № ' + responseText.id + '.');
            $('#test1').html(' ');
            $($form).find('.btn-ghost').text('Отправлено');
            $($form).find('.btn-ghost').addClass('disabled');
            $($form).find('.btn-ghost').attr('disabled', 'true');
        }
    }
    $(formPhoto).on('submit', function (e) {
        if ($(this).hasClass('disabled')) {
            e.preventDefault();
        } else {}
    });

    form.ajaxForm(function () {
        $(form).clearForm();
        $('.variant__modal').modal('hide');
        $('#formAdvice').modal('hide');
        $('#call').modal('hide');
        $('#formAdviceItem').modal('hide');
        $('#thx').modal('show');

        var inputs = document.querySelectorAll('.inputfile');
        if ($(form).hasClass('loadPhoto')) {
            $('#thx').on('show.bs.modal', function () {
                //console.log(this);
                $(this).find('p big').text('Спасибо! Ваши файлы загруженны.');
            });
        };
    });

    $(form).on('submit', function (e) {
        if ($(this).hasClass('disabled')) {
            // handle the invalid form...
            e.preventDefault();
        } else {
            // everything looks good!
            $('.variant__modal').modal('hide');
            $('#formAdvice').modal('hide');
            $('#call').modal('hide');
            $('#formAdviceItem').modal('hide');
            $('#thx').modal('show');
            send();
        }
    });

    $('.package-tab').on('click', '[data-link="show-variant-modal"]', function (e) {
        // var type = Helper.tech;
        var houseID = $(this).data('item');

        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                houseID: houseID,
                type: 'variant-modal'
            },
            success: function success(data) {
                //console.log(data);
                $('#variant-modal-ajax').html(data);
            }
        });

        $('[data-id="modal-package"]').show();
        return false;
    });

    $('.package-tab').on('click', '.close-cross', function (e) {
        // var type = Helper.tech;
        $('[data-id="modal-package"]').hide();
        return false;
    });

    $('[data-item="birth-date"]').mask('99.99.9999');
    $('[data-item="phone"]').mask("+7 (999) 999-99-99");
    $('[data-item="shown-item"]').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var recipient = button.data('house');
        var price = button.data('price');
        var code = button.data('code');
        var modal = $(this);
        modal.find('.modal-body .recipient-name').val(recipient);
        modal.find('.modal-body button[type=submit]').val(price);
        modal.find('.modal-body #codeval').val(code);
    });

    /*fancybox*/
    $(".fancybox").fancybox();
    /*fancybox END*/
    $(".header input").focusin(function () {
        $(this).addClass('focusIn');
        $('.header button[type="submit"]').addClass('focusIn');
    });
    $(".header input").focusout(function () {
        $(this).removeClass('focusIn');
        $('.header button[type="submit"]').removeClass('focusIn');
    });

    $('[data-item="chose-img"] img').on('click', function (e) {
        var $that = $(this).data('src');
        //console.log($that);
        var $myTarget = $('[data-item="target"]');
        var $swap = $myTarget.attr('src', $that);
    });

    Object.defineProperty(Array.prototype, "removeItem", {
        enumerable: false,

        value: function value(itemToRemove) {
            var removeCounter = 0;
            for (var index = 0; index < this.length; index++) {
                if (this[index] === itemToRemove) {
                    this.splice(index, 1);
                    removeCounter++;
                    index--;
                }
            }
            return removeCounter;
        }
    });
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
    var Calc = {
        data: 0,
        price: 0,
        total: 0,
        allsum: '#allsum',
        sum1: '#opt-price-1',
        sum2: '#opt-price-2',
        sum3: 0,
        sumdop: '#sumdop',
        skuOpt: '#discont1',
        skuOther: '#discont2',
        marge: 0,
        list: '#list_bar_calc',
        text: [],
        priceOpt: [],
        priceOptTable: [],
        calculator: '[data-item="global-calc"]',

        Init: function init(options) {
            this.Event(options);
            this.getPrice(options);
            this.getAllsum();
        },

        getOptimal: function getOptimal(event) {
            var el = event.target;
            var table = $(el).parents('#optimal_table');
            var skuOpt = parseInt($(Calc.skuOpt).text());
            var skuOther = parseInt($(Calc.skuOther).text());
            var skuOptValue = skuOpt / 100;
            var skuOtherValue = skuOther / 100;

            var optimalTable = $(this.calculator).find('#optimal_table');

            var optimalInputs = $(optimalTable).find('input');
            var category = $(this.calculator).find('.category_table');
            var categoryInputs = $(category).find('input');

            var categoryTr = $(category).find('tr');
            var categoryP = $(categoryTr).find('p');

            var OptimalTr = $(optimalTable).find('tr');
            var OptimalP = $(OptimalTr).find('p');

            if (table.is('#optimal_table')) {

                if (optimalInputs.length == optimalInputs.filter(":checked").length) {

                    $('#optimal-price').show();
                    $('#optimal-price').prev().hide();

                    $(optimalInputs).each(function (k, v) {
                        var p = parseInt($($(OptimalP)[k]).text());

                        $($(OptimalP)[k]).text(numberWithCommas(Math.ceil(Calc.priceOptTable[k] * (1 - skuOptValue))));
                        $(optimalInputs[k]).val(function (index, value) {
                            return Math.ceil(value * (1 - skuOptValue));
                        });
                    });

                    $(categoryInputs).each(function (k, v) {
                        var p = parseInt($($(categoryP)[k]).text());

                        $($(categoryP)[k]).text(numberWithCommas(Math.ceil(Calc.priceOpt[k] * (1 - skuOtherValue))));
                        $(categoryInputs[k]).val(function (index, value) {
                            return Math.ceil(value * (1 - skuOtherValue));
                        });
                    });

                    var sum1 = parseInt($(Calc.sum1).text().replace(' ', ''));
                    var sum2 = parseInt($(Calc.sum2).text().replace(' ', ''));

                    Calc.marge = sum1 - sum2;
                    Calc.price = Calc.price - Calc.marge;

                    //console.log(Calc.price);

                } else {
                    $('#optimal-price').hide();
                    $('#optimal-price').prev().show();

                    $(optimalInputs).each(function (k, v) {
                        var p = parseInt($($(OptimalP)[k]).text());

                        $($(OptimalP)[k]).text(numberWithCommas(Calc.priceOptTable[k]));
                        $(optimalInputs[k]).val(function (index, value) {
                            return value = Calc.priceOptTable[k];
                        });
                    });

                    $(categoryInputs).each(function (k, v) {
                        var p = parseInt($($(categoryP)[k]).text());

                        $($(categoryP)[k]).text(numberWithCommas(Calc.priceOpt[k]));
                        $(categoryInputs[k]).val(function (index, value) {
                            return value = Calc.priceOpt[k];
                        });
                    });
                    //console.log(Calc.price = Calc.sum3);
                }
            } else {}
        },

        getPrice: function getprice(options) {
            var item = $(options.total);

            Calc.price = parseInt(item.attr('data-item-total'));

            var category = $(this.calculator).find('.category_table');
            var optimalTable = $(this.calculator).find('#optimal_table');

            var categoryInputs = $(category).find('input');
            var optimalInputs = $(optimalTable).find('input');

            Calc.sum3 = parseInt($(Calc.sum1).text().replace(' ', ''));

            $(categoryInputs).each(function (key, value) {
                Calc.priceOpt.push($(value).val());
            });

            $(optimalInputs).each(function (key, value) {
                Calc.priceOptTable.push($(value).val());
            });
        },

        calcPrice: function calcprice(sum1) {
            return Calc.price + Calc.data;
        },

        getAllsum: function getAllsum() {
            var input = $(Calc.calculator).find('input:checked');
            var sum = Calc.data;
            for (var i = 0; input.length > i; i++) {
                sum += parseInt($(input[i]).val());
            }
            Calc.total = sum + Calc.price;

            $(Calc.allsum).text(numberWithCommas(Calc.total));
            $(Calc.sumdop).text(numberWithCommas(sum));
        },
        isChecked: function isChecked(event) {
            var el = event.target,
                check_id,
                chek_text,
                checked,
                chek_val,
                optimal,
                optimalTab,
                optimalText;
            var cur = event.currentTarget;

            if (el.tagName !== 'INPUT') return;

            chek_text = $(el).siblings('label').text();
            check_id = $(el).attr('id');
            chek_val = parseInt($(el).val());

            if ($(el).prop('checked')) {
                Calc.text.push('<li data-val="' + chek_text + '"><p>' + chek_text + '</p></li>');
            } else {
                Calc.text.removeItem('<li data-val="' + chek_text + '"><p>' + chek_text + '</p></li>');
            }
            Calc.getOptimal(event);
            Calc.Update();
        },

        Update: function Update() {
            $(Calc.list).html(Calc.text);
            this.getAllsum();
        },

        Event: function event(options) {
            var calculator = options.calculator;
            $(calculator).on('click', this.isChecked);
        }
    };

    Calc.Init({
        calculator: '[data-item="global-calc"]',
        sum: '#sumdop',
        total: '#allsum',
        list: '#list_bar_calc'
    });

    /*at 112 line we use function change on select*/
    $('[data-item="select"]').change(function () {
        //call select function which we made toggle active
        select_changed();
    });

    function select_changed() {
        //get all items for toggle
        $('.currently').each(function () {
            $(this).removeClass('active');
        });
        //get all options
        $('[data-item="select"]').each(function () {
            //get val of selected option
            var $selected = $(this).val();
            //set active to target in data-item="form-1" to form-5
            $('[data-item="form-' + $selected + '"]').addClass('active');
            //condition which made if val equival to 5 we show all
            if ($selected == 5) {
                $('.currently').addClass('active');
            }
            //rolling back
            return true;
        });
    }
    ;
    $('[data-item="show-case"]').click(function () {
        $(this).toggleClass('slide-left');
        $('.dostroy-what-we-do .item-2').toggleClass('slide-left');
        $('.dostroy-what-we-do .inner').removeClass('slide-right');
        $('.dostroy-what-we-do .item-2 .hiddenEl').removeClass('slide-right');
        $('.dostroy-what-we-do .item-2 img').removeClass('slide-right');
    });
    $('[data-item="inner-1"]').click(function () {
        $('.dostroy-what-we-do .inner').addClass('slide-right');
        if ($('.dostroy-what-we-do .item-2 .hiddenEl').hasClass('slide-right')) {} else {
            $('.dostroy-what-we-do .item-2 .hiddenEl.item-11').addClass('slide-right');
            $('.dostroy-what-we-do .item-2 img').addClass('slide-right');
        }
    });

    $('[data-item="inner-2"]').click(function () {
        $('.dostroy-what-we-do .inner').addClass('slide-right');
        if ($('.dostroy-what-we-do .item-2 .hiddenEl').hasClass('slide-right')) {} else {
            $('.dostroy-what-we-do .item-2 .hiddenEl.item-21').addClass('slide-right');
            $('.dostroy-what-we-do .item-2 img').addClass('slide-right');
        }
    });
    $('[data-item="inner-3"]').click(function () {
        $('.dostroy-what-we-do .inner').addClass('slide-right');
        if ($('.dostroy-what-we-do .item-2 .hiddenEl').hasClass('slide-right')) {} else {
            $('.dostroy-what-we-do .item-2 .hiddenEl.item-31').addClass('slide-right');
            $('.dostroy-what-we-do .item-2 img').addClass('slide-right');
        }
    });
    $('[data-item="inner-4"]').click(function () {
        $('.dostroy-what-we-do .inner').addClass('slide-right');
        if ($('.dostroy-what-we-do .item-2 .hiddenEl').hasClass('slide-right')) {} else {
            $('.dostroy-what-we-do .item-2 .hiddenEl.item-41').addClass('slide-right');
            $('.dostroy-what-we-do .item-2 img').addClass('slide-right');
        }
    });
    $('[data-item="inner-5"]').click(function () {
        $('.dostroy-what-we-do .inner').addClass('slide-right');
        if ($('.dostroy-what-we-do .item-2 .hiddenEl').hasClass('slide-right')) {} else {
            $('.dostroy-what-we-do .item-2 .hiddenEl.item-51').addClass('slide-right');
            $('.dostroy-what-we-do .item-2 img').addClass('slide-right');
        }
    });
    $('[data-item="inner-6"]').click(function () {
        $('.dostroy-what-we-do .inner').addClass('slide-right');
        if ($('.dostroy-what-we-do .item-2 .hiddenEl').hasClass('slide-right')) {} else {
            $('.dostroy-what-we-do .item-2 .hiddenEl.item-61').addClass('slide-right');
            $('.dostroy-what-we-do .item-2 img').addClass('slide-right');
        }
    });
    $('[data-item="inner-7"]').click(function () {
        $('.dostroy-what-we-do .inner').addClass('slide-right');
        if ($('.dostroy-what-we-do .item-2 .hiddenEl').hasClass('slide-right')) {} else {
            $('.dostroy-what-we-do .item-2 .hiddenEl.item-71').addClass('slide-right');
            $('.dostroy-what-we-do .item-2 img').addClass('slide-right');
        }
    });

    $('.dostroy-what-we-do .close-btn').click(function () {
        $('.dostroy-what-we-do .inner').removeClass('slide-right');
        $('.dostroy-what-we-do .item-2 .hiddenEl').removeClass('slide-right');
        $('.dostroy-what-we-do .item-2 img').removeClass('slide-right');
    });
    var obj;
    $('.header .has-inner-menu > a').hover(function () {
        var status = $(this).attr('class');
        if (status != 'test') {
            var maxWidth = 0;
            var width = 0;
            var obj = $(this);
            var countli = $(this).parent().find('.dropdown-menu li').length;
            $(this).parent().find('.dropdown-menu li').each(function (i) {
                //add width
                width = this.offsetWidth;
                if (maxWidth < width) {
                    maxWidth = width;
                } else {
                    if (countli - 1 == i && maxWidth != 0 && i != 0) {
                        var max = 0;
                        var max = maxWidth;
                        add_width(max, obj);
                    }
                }
            });
        }
    });
});

function add_width(max, obj) {
    obj.addClass('test');
    obj.parent().find('.dropdown-menu').css('width', max);
}

$(document).ready(function () {
    $('.vocabulary-menu a').click(function (e) {
        e.preventDefault();
        var that = $(this).attr('data-choice');

        $('.vocabulary-menu a').removeClass('active');
        $('.vocabulary-list .output').removeClass('active');

        $(this).addClass('active');
        $("#" + that).addClass('active');
    });
    /*comunications*/
    var $targetForm = $('.form-credit');
    $targetForm.find('input[type=text], input[type=tel], input[type=email]').attr('disabled', true);
    $('input[type="checkbox"]').click(function () {
        var $c1 = $('#checkbox1');
        var $c2 = $('#checkbox2');

        if ($c1.is(':checked')) {
            $targetForm.find('input[type=text], input[type=tel], input[type=email]').attr('disabled', false);
            $targetForm.find('[data-item="cost-house"]').attr('disabled', true);
        }

        if ($c2.is(':checked')) {
            $targetForm.find('input[type=text], input[type=tel], input[type=email]').attr('disabled', false);
            $targetForm.find('[data-item="cost-house"]').attr('disabled', false);
            $targetForm.find('[data-item="cost-credit"], [data-item="period-credit"]').attr('disabled', true);
        }

        if ($c1.is(':checked') && $c2.is(':checked')) {
            $targetForm.find('input[type=text], input[type=tel], input[type=email]').attr('disabled', false);
        }

        if ($c1.is(':checked') == false && $c2.is(':checked') == false) {
            $targetForm.find('input[type=text], input[type=tel], input[type=email]').attr('disabled', true);
        }

        $('[data-item="checked-items"]').val('');

        var chek = document.getElementsByName('chk[]');

        var check_array = new Array();

        $('input:checkbox').each(function () {
            if ($(this).is(':checked')) {
                check_array.push($(this).val());
            }
        });

        for (i = 0; i < check_array.length; i++) {
            var check_all = $('[data-item="checked-items"]').val();
            if (i > 0) {
                $('[data-item="checked-items"]').val(check_all + ', ' + check_array[i]);
            } else {
                $('[data-item="checked-items"]').val(check_all + '' + check_array[i]);
            }
        }

        //console.log(check_array);
    });
    /*comunications END*/
});

$(document).ready(function () {
    $('.my-tab-list a').click(function (e) {
        e.preventDefault();
        var tab_id = $(this).attr('data-tab');
        $('.my-tab-list a').removeClass('active');
        $('[data-item="my-tab-btns"] > div').removeClass('active');
        $(this).addClass('active');
        $("#" + tab_id).addClass('active');
    });
    $('.my-tab-list a').click(function (e) {
        e.preventDefault();
        var tab_id = $(this).attr('data-tab');
        $('.my-tab-list a').removeClass('active');
        $('[data-item="my-tab-btns"] > div').removeClass('active');
        $(this).addClass('active');
        $("#" + tab_id).addClass('active');
    });
});

$(document).ready(function () {
    var that = $('.content.with-top-padding');
    var banner = $('body .my-item-slider .inner .baner .inner');
    var win = $(window).height() - (550 - 110 - 5);
    //var bannerOffset = $(window).height() * .1;
    var owlBack = $('[data-item="slider-back-side"]');
    var owlMain = $('[data-item="slider-item"]');
    var owlInfoBlock = $('[data-item="slider-info-block"]');
    var owlCatalog = $('[data-item="slider-item-catalog"]');
    var owlCatalogSide = $('[data-item="slider-catalog-side"]');
    that.css('margin-top', win);

    // Синхронизация верхнего и нижнего слайsдера в секции Фото
    var owlPhotoSlider = $('[data-type="photo-slider"]');
    var owlPhotoSliderSide = $('[data-type="photo-slider-side"]');
    syncTwoSliders(owlPhotoSlider, owlPhotoSliderSide);

    sliderCatalogInit();

    owlInfoBlock.owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        navText: ["<i class='my-arrow-left'></i>", "<i class='my-arrow-right'></i>"]
    });

    owlBack.owlCarousel(_defineProperty({
        loop: true,
        margin: 0,
        nav: true,
        dots: true,
        items: 1,
        navContainer: '.main-navigation',
        dotsContainer: '.main-dots',
        autoplayHoverPause: true,
        autoplay: false,
        navSpeed: 500,
        dotsSpeed: 500
    }, 'dots', true));

    owlMain.on('initialized.owl.carousel', function (e) {
        var currentSlide = e.page.index;
        $('.trigger').removeClass('active');
        $('.trigger').each(function (carrentTarget) {
            if (currentSlide == carrentTarget) {
                $(this).addClass('active');
            }
        });
    });
    owlMain.owlCarousel(_defineProperty({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        items: 1,
        autoplay: false,
        autoHeight: true,
        navText: ["<i class='my-arrow-left'></i>", "<i class='my-arrow-right'></i>"]
    }, 'dots', true));

    if ($(owlCatalogSide).find('.owl-item').length <= 5) {
        var _owlCatalogSide$owlCa;

        owlCatalogSide.owlCarousel((_owlCatalogSide$owlCa = {
            loop: false,
            margin: 20,
            nav: false,
            dots: false,
            items: 5,
            autoplay: false,
            navText: ["<i class='my-arrow-left'></i>", "<i class='my-arrow-right'></i>"]
        }, _defineProperty(_owlCatalogSide$owlCa, 'dots', true), _defineProperty(_owlCatalogSide$owlCa, 'responsiveClass', true), _defineProperty(_owlCatalogSide$owlCa, 'responsive', {
            0: {
                items: 3,
                nav: true
            },
            767: {
                items: 3,
                nav: false
            },
            1000: {
                items: 5,
                nav: true,
                loop: false
            }
        }), _defineProperty(_owlCatalogSide$owlCa, 'onInitialized', firstLoad), _owlCatalogSide$owlCa));
    } else {
        var _owlCatalogSide$owlCa2;

        owlCatalogSide.owlCarousel((_owlCatalogSide$owlCa2 = {
            loop: false,
            margin: 20,
            nav: true,
            dots: false,
            items: 5,
            autoplay: false,
            navText: ["<i class='my-arrow-left'></i>", "<i class='my-arrow-right'></i>"]
        }, _defineProperty(_owlCatalogSide$owlCa2, 'dots', true), _defineProperty(_owlCatalogSide$owlCa2, 'responsiveClass', true), _defineProperty(_owlCatalogSide$owlCa2, 'responsive', {
            0: {
                items: 3,
                nav: true
            },
            767: {
                items: 3,
                nav: false
            },
            1000: {
                items: 5,
                nav: true,
                loop: false
            }
        }), _defineProperty(_owlCatalogSide$owlCa2, 'onInitialized', firstLoad), _owlCatalogSide$owlCa2));
    };

    owlMain.on('changed.owl.carousel', function (that) {
        var currentSlide = that.page.index;
        $('.trigger').removeClass('active');
        $('.trigger').each(function (carrentTarget) {
            if (currentSlide == carrentTarget) {
                $(this).addClass('active');
            }
        });
    });

    onStart();

    $('.fancybox-g').fancybox({
        'showCloseButton': false,
        showNavArrows: true,
        'padding': 0
    });

    // // Синхронизация верхнего и нижнего слайдера в секции Фото
    // var owlPhotoSlider = $('[data-type="photo-slider"]');

    // function syncTwoSliders(obj) {

    // owlPhotoSlider.on('click', '.my-arrow-left', function(e){
    //     owlPhotoSlider.on('changed.owl.carousel', function (e) {
    //         var currentSlide = e.page.index;
    //         var allOwlItems = owlCatalogSide.find('div.owl-item');

    //         owlCatalogSide.trigger('prev.owl.carousel');
    //         owlCatalogSide.trigger('refresh.owl.carousel');
    //         allOwlItems.eq(currentSlide)
    //             .addClass('current')
    //             .siblings()
    //             .removeClass('current'); 

    //     });
    // });
    // owlPhotoSlider.on('click', '.my-arrow-right', function(){
    //     owlPhotoSlider.on('changed.owl.carousel', function (e) {
    //         var currentSlide = e.page.index;
    //         var allOwlItems = owlCatalogSide.find('div.owl-item');

    //         owlCatalogSide.trigger('next.owl.carousel');
    //         owlCatalogSide.trigger('refresh.owl.carousel');
    //         allOwlItems.eq(currentSlide)
    //             .addClass('current')
    //             .siblings()
    //             .removeClass('current'); 

    //     });
    // });


    // owlCatalogSide.on('click', '.owl-item', function(e){
    //     $(this)
    //         .addClass('current')
    //         .siblings()
    //         .removeClass('current')

    //     var allOwlItems = owlCatalogSide.find('div.owl-item');
    //     var currentIDX = allOwlItems.index($('.current'));

    //     owlCatalogSide.trigger('to.owl.carousel', [currentIDX, 1, true]);
    //     owlPhotoSlider.trigger('to.owl.carousel', currentIDX);
    // });
    // }


    function firstLoad() {
        owlCatalogSide.find('div.owl-item').eq(0).addClass('current');
    }

    // Переинициализация слайдеров при изменении размера окна
    owlCatalogSide.on('resized.owl.carousel').trigger('refresh.owl.carousel');
    owlCatalog.on('resized.owl.carousel').trigger('refresh.owl.carousel');

    // size in card 
    /*function size(){
        var getSize = document.querySelector('[data-item="prop-size"]');
        if(getSize === null) return;
        if(getSize.tagName !== "P") return;
        var getSize = $('[data-item="prop-size"]'),
            getSizeText = getSize.text();
            getSizeText.replace(/\s/g,'');
        console.log(getSizeText);
    }
    size();*/

    $('.like-btns__item').on('click', function () {

        var cnt = parseInt($(this).text());
        var type = 'lik';
        var item = $(this).data('item');
        cnt++;

        if ($(this).hasClass('dislike-count') && Helper.dislike == 'false') {
            type = 'dis';
            $('.dislike-count').text(cnt);
            Helper.dislike = 'true';
            Like(cnt, type, item);
        }

        if ($(this).hasClass('like-count') && Helper.like == 'false') {
            $('.like-count').text(cnt);
            Helper.like = 'true;';
            Like(cnt, type, item);
        }
    });

    if ($('.house-card').length > 0) {
        var link = $('.tech-type__elem.active a').data('link');
        if (!link) {
            $('.tech-type__elem').eq(0).trigger('click');
        }
    }

    houseCardNav();
    houseLoadInfo();
    slider3dInit();
    // sliderNavBtnsReinit();
});
function Like(cnt, type, item) {
    $.ajax('/include/catalog_card.php', {
        method: 'GET',
        async: true,
        data: {
            cnt: cnt,
            type_like: type,
            item: item,
            type: 'like'
        },
        success: function success(data) {
            Helper.ajax = 'true';
            //console.log(data);
        },
        beforeSend: function beforeSend() {
            Helper.ajax = 'false';
        }
    });
}
function slider3dInit() {
    var threeSixty = $('.threesixty');
    threeSixty.threeSixty({
        dragDirection: 'horizontal', // horizontal OR vertical
        useKeys: true,
        draggable: true
    });

    var autoPlaySlider3D = setInterval(function () {
        threeSixty.nextFrame();
    }, 100);

    threeSixty.on('mousedown touchstart', function () {
        clearInterval(autoPlaySlider3D);
    });
}
function complectClick(type) {
    var id_item = $('.complectation').data('house');
    //console.log(type);


    if (type == 'karkas' && Helper.comp_karkas == 'false') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                id_item: id_item,
                type_tech: type,
                type: 'complectation'
            },
            success: function success(data) {
                $('#karkas-complect').html(data);
                Helper.ajax = 'true';
                Helper.comp_karkas = 'true';
                //console.log(data);
            },
            beforeSend: function beforeSend() {
                Helper.ajax = 'false';
            }
        });
    }
    if (type == 'brus' && Helper.comp_brus == 'false') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                id_item: id_item,
                type_tech: type,
                type: 'complectation'
            },
            success: function success(data) {
                $('#brus-complect').html(data);
                Helper.ajax = 'true';
                Helper.comp_brus = 'true';
                //console.log(data);
            },
            beforeSend: function beforeSend() {
                Helper.ajax = 'false';
            }
        });
    }
    if (type == 'kirpich' && Helper.comp_kirpich == 'false') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                id_item: id_item,
                type_tech: type,
                type: 'complectation'
            },
            success: function success(data) {
                $('#kirpich-complect').html(data);
                Helper.ajax = 'true';
                Helper.comp_kirpich = 'true';
            },
            beforeSend: function beforeSend() {
                Helper.ajax = 'false';
            }
        });
    }
}
function sliderCatalogInit() {
    var _owlCatalog$owlCarous;

    var owlCatalog = $('[data-item="slider-item-catalog"]');
    var owlCatalogSide = $('[data-item="slider-catalog-side"]');

    owlCatalog.owlCarousel((_owlCatalog$owlCarous = {
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        items: 1,
        autoplay: false,
        autoHeight: false,
        autoHeightClass: 'owl-height',
        navText: ["<i class='my-arrow-left'></i>", "<i class='my-arrow-right'></i>"]
    }, _defineProperty(_owlCatalog$owlCarous, 'dots', true), _defineProperty(_owlCatalog$owlCarous, 'touchDrag', false), _defineProperty(_owlCatalog$owlCarous, 'mouseDrag', false), _owlCatalog$owlCarous)).trigger('refresh.owl.carousel').on('initialize.owl.carousel changed.owl.carousel refreshed.owl.carousel', function (event) {
        if (!event.namespace) return;
        var carousel = event.relatedTarget,
            element = event.target,
            current = carousel.current();
        $('.owl-next', element).toggleClass('disabled', current === carousel.maximum());
        $('.owl-prev', element).toggleClass('disabled', current === carousel.minimum());
    });

    if ($(owlCatalogSide).find('.owl-item').length <= 5) {
        var _owlCatalogSide$owlCa3;

        owlCatalogSide.owlCarousel((_owlCatalogSide$owlCa3 = {
            loop: false,
            margin: 20,
            nav: false,
            dots: false,
            items: 5,
            autoplay: false,
            slideBy: 1,
            navText: ["<i class='my-arrow-left'></i>", "<i class='my-arrow-right'></i>"]
        }, _defineProperty(_owlCatalogSide$owlCa3, 'dots', true), _defineProperty(_owlCatalogSide$owlCa3, 'responsiveClass', true), _defineProperty(_owlCatalogSide$owlCa3, 'responsive', {
            0: {
                items: 3,
                nav: true
            },
            767: {
                items: 3,
                nav: false
            },
            1000: {
                items: 5,
                nav: true,
                loop: false
            }
        }), _defineProperty(_owlCatalogSide$owlCa3, 'onInitialized', firstLoad), _owlCatalogSide$owlCa3));
    } else {
        var _owlCatalogSide$owlCa4;

        owlCatalogSide.owlCarousel((_owlCatalogSide$owlCa4 = {
            loop: false,
            margin: 20,
            nav: true,
            dots: false,
            items: 5,
            autoplay: false,
            slideBy: 1,
            navText: ["<i class='my-arrow-left'></i>", "<i class='my-arrow-right'></i>"]
        }, _defineProperty(_owlCatalogSide$owlCa4, 'dots', true), _defineProperty(_owlCatalogSide$owlCa4, 'responsiveClass', true), _defineProperty(_owlCatalogSide$owlCa4, 'responsive', {
            0: {
                items: 3,
                nav: true
            },
            767: {
                items: 3,
                nav: false
            },
            1000: {
                items: 5,
                nav: true,
                loop: false
            }
        }), _defineProperty(_owlCatalogSide$owlCa4, 'onInitialized', firstLoad), _owlCatalogSide$owlCa4));
    };

    //console.log('11111');


    // Синхронизация верхнего и нижнего слайsдера в секции Планировки
    // var owlHouseSlider = $('[data-type="house-slider"]');
    //    var owlHouseSliderSide = $('[data-type="house-slider-side"]');
    syncTwoSliders(owlCatalog, owlCatalogSide);

    function firstLoad() {
        owlCatalogSide.find('div.owl-item').eq(0).addClass('current');
    }
}

function houseLoadInfo() {
    if ($('.house-card').length > 0) {

        var link = $('.tech-type__elem.active a').data('link');
        var text = $('.tech-type__elem.active a').text();
        var type = $('.tech-type__elem.active a').data('type');
        var id_house = $('.tech-type__elem.active a').data('house');
        Helper.tech = type;

        $('[data-item="' + link + '"]').addClass('active').siblings().removeClass('active');
        $('#chosen-house-tech').text(text);
        complectClick(type);
        updatePriceHouse(id_house, link, type);
        // sliderNavBtnsReinit();
    }
}

function syncTwoSliders(mainSlider, sideSlider) {
    mainSlider.on('click', '.my-arrow-left', function (e) {

        mainSlider.on('translated.owl.carousel', function (e) {
            var currentSlide = e.page.index;
            var allOwlItems = sideSlider.find('div.owl-item');

            sideSlider.trigger('prev.owl.carousel').trigger('refresh.owl.carousel');
            allOwlItems.eq(currentSlide).addClass('current').siblings().removeClass('current');
        });
    });
    mainSlider.on('click', '.my-arrow-right', function () {

        mainSlider.on('translated.owl.carousel', function (e) {
            var currentSlide = e.page.index;
            var allOwlItems = sideSlider.find('div.owl-item');

            sideSlider.trigger('next.owl.carousel').trigger('refresh.owl.carousel');
            allOwlItems.eq(currentSlide).addClass('current').siblings().removeClass('current');
        });
    });

    sideSlider.on('click', '.owl-item', function (e) {
        var currentSlide = this;
        var allOwlItems = sideSlider.find('div.owl-item');
        var currentIDX = allOwlItems.index(currentSlide);

        $(currentSlide).addClass('current').siblings().removeClass('current');

        if (!$(currentSlide).hasClass('active')) {
            sideSlider.trigger('to.owl.carousel', currentIDX);
        }

        mainSlider.trigger('to.owl.carousel', currentIDX);
    });
}

function loadComplectation(house_id, type) {

    //console.log('грузим!',house_id,type);
    if (type == 'summer-house' && Helper.ajax == 'true') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                element_id: house_id,
                type: 'load_complect'
            },
            success: function success(data) {
                $('.load-ajax-params').html(data);
                Helper.ajax = 'true';
                // updateScroll();
                //console.log(data);
            },
            beforeSend: function beforeSend() {
                Helper.ajax = 'false';
            }
        });
    }
    if (type == 'winter-house' && Helper.ajax == 'true') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                element_id: house_id,
                type: 'load_complect'
            },
            success: function success(data) {
                //$('.load-ajax-params').html(data);
                $('.load-ajax-params').html('<p><strong>Описание комплектации в разработке.</strong> Приносим свои извинения.</p>');
                Helper.house_zima = 'true';
                Helper.ajax = 'true';
                //updateScroll();
                //console.log(data);
            },
            beforeSend: function beforeSend() {
                Helper.ajax = 'false';
            }
        });
    }
    if (type == 'family-house' && Helper.ajax == 'true') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                element_id: house_id,
                type: 'load_complect'
            },
            success: function success(data) {
                $('.load-ajax-params').html(data);
                Helper.ajax = 'true';
                //console.log(data);
                //updateScroll();
            },
            beforeSend: function beforeSend() {
                Helper.ajax = 'false';
            }
        });
    }
}
// function updateScroll(){

//     console.log($('.load-ajax-params').length);

//     $(document).on('mouseleave','.load-ajax-params',function(){
//         $('.load-ajax-params').unbind('mouseenter');
//         $('body').unbind('mousewheel');
//         $('.load-ajax-params').scrollTop(0);
//         console.log('leve');
//     });

//     $(document).on('mouseenter','.load-ajax-params',function(){

//         scrollNext =  false;

//         var el = $(this);

//         //console.log($.data($(el).get(0), 'events').mouseenter);

//             $('body').bind('mousewheel',function(e){


//                 el.scrollTop(el.scrollTop() + (e.deltaY * -35));

//                 //console.log('ALL ',el.scrollTop() + (e.deltaY * -35));
//                 console.log('TOP ',el.scrollTop());
//                 //console.log('Y ',e.deltaY);
//                 e.preventDefault();
//             });
//     });

// }

function updateImageHouse(id_house) {
    var idx = id_house;

    $.ajax('/include/catalog_card.php', {
        method: 'GET',
        async: true,
        data: {
            element_id: idx,
            type: 'img'

        },
        success: function success(data) {

            $('.slider-previews-ajx').html(data);
            sliderCatalogInit();
            slider3dInit();
            houseCardNav();
            Helper.ajax = 'true';
            //console.log(data);
        },
        beforeSend: function beforeSend() {
            Helper.ajax = 'false';
        }
    });
}

function updatePriceHouse(id_house, link, type) {

    defaultView();

    Helper.write_params = 'false';
    var idx = id_house;
    var tab_price = link.replace("package-", "");

    if (type == 'karkas' && Helper.price_karkas == 'false') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                element_id: idx,
                type: 'price'

            },
            success: function success(data) {
                $('#' + tab_price).html(data);
                Helper.ajax = 'true';
                Helper.price_karkas = 'true';
                //console.log('karkas!!!');
            },
            beforeSend: function beforeSend() {

                Helper.ajax = 'false';
            }
        });
    }
    if (type == 'brus' && Helper.price_brus == 'false') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                element_id: idx,
                type: 'price'

            },
            success: function success(data) {
                $('#' + tab_price).html(data);
                Helper.ajax = 'true';
                Helper.price_brus = 'true';
                //console.log(type);
            },
            beforeSend: function beforeSend() {

                Helper.ajax = 'false';
            }
        });
    }
    if (type == 'kirpich' && Helper.price_kirpich == 'false') {
        $.ajax('/include/catalog_card.php', {
            method: 'GET',
            async: true,
            data: {
                element_id: idx,
                type: 'price'

            },
            success: function success(data) {
                $('#' + tab_price).html(data);
                Helper.ajax = 'true';
                Helper.price_kirpich = 'true';
                //console.log(data,$('#'+tab_price));
            },
            beforeSend: function beforeSend() {

                Helper.ajax = 'false';
            }
        });
    }
}

function defaultView() {
    Helper.write_params = 'false';

    var extraParamsBlock = $('.extra-params');
    var allShowCompactDetailsLink = $('[data-link="show-compact-details"]');
    var allVariants = $('.package-tab__inner').find('.variant');
    var allAboutTitle = $(allVariants).find('.variant__parameters__title');
    var allAboutList = $(allVariants).find('.variant__parameters > ul');
    var allHrSeporator = $(allVariants).find('hr');

    $(extraParamsBlock).hide();
    $(allAboutTitle).show();
    $(allAboutList).show();
    $(allShowCompactDetailsLink).remove();
    $(allHrSeporator).show().css('border-top', '1px solid #eee');
}

function loaderPrice(type, status) {
    //console.log(type,status);
    if (status) {
        var loader = '<div class="loader"></div>';
        $('#tab-' + type).append(loader);
    }
}

// //Показ/скрытие навигационных кнопок слайдера в зависимости от текущей позиции слайдов
// function sliderNavBtnsReinit() {

// }

function houseCardNav() {
    //Переключение технологий карточки дома
    $('.tech-type__elem a').on('click', function (e) {

        var link = $(this).data('link');
        var type = $(this).data('type');
        Helper.tech = type;
        var text = $(this).text();
        var id_house = $(this).data('house');
        $('[data-item="' + link + '"]').addClass('active').siblings().removeClass('active');
        $('#chosen-house-tech').text(text);

        if (Helper.ajax == 'true') {
            updatePriceHouse(id_house, link, type);
            updateImageHouse(id_house);
            complectClick(type);
        }
    });

    //Переключение секций карточки дома
    $('.nav-b__link').on('click', function (e) {
        var link = $(this).data('href');
        var currentSection = $('#' + link);
        var allSections = $('.section-wrapper').children();
        var currentIDX = $(allSections).index(currentSection);
        var sectionHeight = $('.section-wrapper').height();

        e.preventDefault();

        //Классы на навигационных кнопках в сайдбаре
        $(this).parent().addClass('active').siblings().removeClass('active');

        allSections.css('top', -sectionHeight * currentIDX).removeClass('active');
        currentSection.addClass('active');
    });

    //Reinit section height and extra params block width on windows resize
    $(window).resize(function () {
        var currentSection = $('.section-wrapper > section.active');
        var sectionHeight = $('.section-wrapper').height();
        var allSections = $('.section-wrapper').children();
        var currentIDX = $(allSections).index(currentSection);

        var extraParamsBlock = $('.extra-params');
        var sectionWidth = $('.package-tab__inner.active').width() - 20;

        allSections.css('top', -sectionHeight * currentIDX);

        $(extraParamsBlock).css('width', sectionWidth + 'px');
    });

    // //Переключение слайдов по превьюшкам в слайдере карточки дома
    // $('.slider-previews').on('click', 'li', function(e){

    //  if ($(this).parent().hasClass('slider-previews__list--flex')) {
    //      var nodeList = Array.prototype.slice.call($(this).siblings().andSelf());
    //      var owlSlider = $('[data-type="house-slider"]');
    //      var orderNumber = nodeList.indexOf($(this)[0]);

    //      owlSlider.trigger('to.owl.carousel', orderNumber);

    //  } else if ($(this).parent().hasClass('owl-item')) {
    //      var nodeList = Array.prototype.slice.call($(this).closest('.owl-stage').find('.owl-item'));
    //      var owlSlider = $('[data-type="photo-slider"]');
    //      var orderNumber = nodeList.indexOf($(this).parent()[0]);

    //      owlSlider.trigger('to.owl.carousel', orderNumber);

    //  }
    // });

    //Ссылка якорь у "00 отзывов" на секцию с отзывами
    $('.rating').on('click', '.rating__feedback__link', function (e) {
        e.preventDefault();
        var link = $(this).data('href');
        var currentSection = $('#' + link);
        var allSections = $('.section-wrapper').children();
        var currentIDX = $(allSections).index(currentSection);
        var sectionHeight = $('.section-wrapper').height();

        //Классы на навигационных кнопках в сайдбаре
        $('.nav-b > ul > li').last().addClass('active').siblings().removeClass('active');

        allSections.css('top', -sectionHeight * currentIDX).removeClass('active');
        currentSection.addClass('active');
    });

    //Показать больше инфы по клику на "Посмотреть полное описание" в секции "Комплектация"
    $('.package-tab').on('click', '[data-link="show-extra-params"]', function (e) {
        e.preventDefault();

        var link = $(this);
        var type_tech = Helper.tech;
        var allExtraParamsBlock = $('.extra-params');
        var currentExtraParamsBlock = $('#' + type_tech + '-complect .extra-params');
        var sectionWidth = $('.package-tab__inner.active').width() - 20;
        var currentHrSeporator = $(this).closest('.variant').find('.variant__footer > hr');
        var allVariants = $(this).closest('.package-tab__inner').find('.variant');
        var allAboutList = $(allVariants).find('.variant__parameters > ul');
        var allAboutTitle = $(allVariants).find('.variant__parameters__title');
        var allHrSeporator = $(allVariants).find('hr');
        var house_id = link.data('house');
        var type = link.data('type');

        loadComplectation(house_id, type);

        $(allHrSeporator).hide();
        if ($(currentHrSeporator).index(allHrSeporator) == 0) {

            $(currentHrSeporator).show().css('margin-bottom', 0).css('border-top', '2px solid #337ab7').parent('.variant__footer').css('margin-bottom', 0);
            $(allExtraParamsBlock).hide();
            $(currentExtraParamsBlock).css('margin-top', 0).css('width', sectionWidth + 'px');
        } else {
            $(currentHrSeporator).show().css('margin-bottom', 0).css('border-top', '2px solid #337ab7').parent('.variant__footer').css('margin-bottom', 0);
            $(allExtraParamsBlock).hide();
            $(currentExtraParamsBlock).css('margin-top', 22).css('width', sectionWidth + 'px');
        }

        if ($(currentExtraParamsBlock).css('display') == 'none') {
            //console.log(currentExtraParamsBlock);
            $(allAboutList).hide();

            if (Helper.write_params == 'false') {
                $('<a href="" data-link="show-compact-details">Коротко о комплектации</a>').insertBefore(allAboutTitle);
                Helper.write_params = 'true';
            }

            $(allAboutTitle).hide();
            // .parent('.variant__parameters')
            // .css('margin-bottom', 0);
            $(currentExtraParamsBlock).toggle();
        }
    });

    //Вернуть все обратно, когда нажимаем ссылку "Коротко о комплектации"
    $('.package-tab').on('click', '[data-link="show-compact-details"]', function (e) {
        e.preventDefault();
        Helper.write_params = 'false';

        var link = $(this);
        var extraParamsBlock = $('.extra-params');
        var allShowCompactDetailsLink = $('[data-link="show-compact-details"]');
        var allVariants = $(this).closest('.package-tab__inner').find('.variant');
        var allAboutTitle = $(allVariants).find('.variant__parameters__title');
        var allAboutList = $(allVariants).find('.variant__parameters > ul');
        var allHrSeporator = $(allVariants).find('hr');

        $(extraParamsBlock).hide();
        $(allAboutTitle).show();
        $(allAboutList).show();
        $(allShowCompactDetailsLink).remove();
        $(allHrSeporator).show().css('border-top', '1px solid #eee');
    });
}

$(window).scroll(function () {
    var scroll_r = $(this).scrollTop();
    var parallaxUp = $('[data-parallax="up"]');
    var parallaxFix = $('inner.with-top-padding.content');
    var parallaxDown = $('[data-parallax="down"]');
    var opacity = $('[data-opacity="true"]');
    var opacityMain = $('[data-opacity-main="true"]');
    var transform = scroll_r * .05;
    var options = ($(window).height() - scroll_r - $(window).height()) / 1000 + 0.5 < 0.2 ? 0.2 : ($(window).height() - scroll_r - $(window).height()) / 1000 + 0.5;
    //var options2 = (((($(window).height() - scroll_r) - $(window).height()) / 1000) + 0.5) < 0.2 ? 0.2 : (((($(window).height() - scroll_r) - $(window).height()) / 1000) + 0.5);
    var optionsMain = ($(window).height() - scroll_r - $(window).height()) / 1000 + 1 < 0.2 ? 0.2 : ($(window).height() - scroll_r - $(window).height()) / 1000 + 1;

    opacityMain.css('opacity', optionsMain);
    opacity.css('opacity', options);
    parallaxDown.css('top', scroll_r * .08);

    menuTop();
    logos();
    bgUP();
    bgDown();
});

function opacityBg() {
    var parallaxUp = $('[data-parallax="up"]');
    var scroll_r = $(this).scrollTop();
    if ($(this).scrollTop() < 800) {
        parallaxUp.css('top', scroll_r * .035);
    } else if ($(this).scrollTop() <= 800) {
        parallaxUp.css('top', scroll_r * 0);
    }
}

function bgUP() {
    var parallaxUp = $('[data-parallax="up"]');
    var scroll_r = $(this).scrollTop();
    if ($(this).scrollTop() < 800) {
        parallaxUp.css('top', scroll_r * .035);
    } else if ($(this).scrollTop() <= 800) {
        parallaxUp.css('top', scroll_r * 0);
    }
}

function bgDown() {
    var parallaxDown = $('[data-parallax="down"]');
    var scroll_r = $(this).scrollTop();
    if ($(this).scrollTop() < 800) {
        parallaxDown.css('top', scroll_r * .08);
    } else if ($(this).scrollTop() <= 800) {
        parallaxDown.css('top', scroll_r * .08);
    }
}

function menuTop() {
    var pushLeft = $('[data-item="push-left"]');
    if ($(window).scrollTop() > 10 && $(window).width() > 768) {
        $('[data-item="header"]').addClass('fix-header');
        $('[data-item="header"] .navbar-header .logo a').addClass('active');
        pushLeft.addClass('top-bar-left-offset');
        logo = 'down';
        logos();
    } else if ($(window).scrollTop() <= 10 && $(window).width() > 768) {
        $('[data-item="header"]').removeClass('fix-header');
        $('[data-item="header"] .navbar-header .logo a').removeClass('active');
        pushLeft.removeClass('top-bar-left-offset');
        logo = 'up';
        logos();
    }
}

function onStart() {
    var scroll_r = $(window).scrollTop();
    var parallaxUp = $('[data-parallax="up"]');
    var parallaxDown = $('[data-parallax="down"]');
    var opacity = $('[data-opacity="true"]');
    var opacityMain = $('[data-opacity-main="true"]');
    var transform = scroll_r * .05;
    var winHeight = $(window).height();
    //var options = (((( winHeight - scroll_r) - winHeight) / 1000) + 1) < 0.3 ? 0.3 : ((((winHeight - scroll_r) - winHeight) / 1000) + 1);
    var options = (winHeight - scroll_r - winHeight) / 1000 + 0.5 < 0.3 ? 0.3 : (winHeight - scroll_r - winHeight) / 1000 + 0.5;
    var optionsMain = (winHeight - scroll_r - winHeight) / 1000 + 1 < 0.3 ? 0.3 : (winHeight - scroll_r - winHeight) / 1000 + 1;
    opacity.css('opacity', options);
    opacityMain.css('opacity', optionsMain);
    parallaxDown.css('top', scroll_r * .08);
    menuTop();
    bgUP();
    bgDown();
    logos();
}

function logos() {
    if (logo == 'down') {
        $('.header .has-inner-menu .dropdown-menu').each(function (i) {
            var s = $(this).attr('class');
            var r = $(this).css('width');
            $(this).css('width', r);

            if (s == 'dropdown-menu up-logo' || s == 'dropdown-menu') {
                var w = $(this).css('width').substr(0, $(this).css('width').length - 2);
                $(this).css('width', parseInt(w) + 10);
                $(this).addClass('down-logo');
                $(this).removeClass('up-logo');
            }
        });
    } else {
        $('.header .has-inner-menu .dropdown-menu').each(function (i) {
            var s = $(this).attr('class');
            var r = $(this).css('width');
            $(this).css('width', r);

            if (s == 'dropdown-menu down-logo') {
                var w = $(this).css('width').substr(0, $(this).css('width').length - 2);
                $(this).css('width', parseInt(w) - 10);
                $(this).addClass('up-logo');
                $(this).removeClass('down-logo');
            }
        });
    }
}

function houseCardPrint() {
    var prtContent;
    var WinPrint = window.open('', '', 'left=50,top=50,width=800,height=640,toolbar=0,scrollbars=1,status=0');

    WinPrint.document.write('<div id="print" class="contentpane">');

    for (var _len = arguments.length, elemID = Array(_len), _key = 0; _key < _len; _key++) {
        elemID[_key] = arguments[_key];
    }

    for (var i = 0; i < elemID.length; i++) {
        prtContent = $(elemID[i]);
        if (elemID[i] == '#layout_1-slide' || elemID[i] == '#layout_2-slide') {
            WinPrint.document.write('<div class="col-xs-6">');
            WinPrint.document.write(prtContent.html());
            WinPrint.document.write('</div>');
            console.log('123', WinPrint.document);
        } else {
            WinPrint.document.write(prtContent.html());
        }
    }
    WinPrint.document.write('</div>');
    WinPrint.document.close();

    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
}