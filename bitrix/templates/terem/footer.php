</div>

<section class="footer">
  <div class="container">
   <div class="row footer-lvl-1">
    <div class="col-xs-12 col-md-5 col-sm-5">


      <p class="text-danger"><i>Строительная компания «Теремъ» приветствует вас!</i></p>


      <p class="footer-lvl-1-head">
        доступные цены и<br />высокое качество&nbsp;–<br />основные принципы<br />нашей работы
		<!-- ДОСТУПНОСТЬ ЦЕН НА ДОМА –<br>ОДИН ИЗ ОСНОВНЫХ ПРИНЦИПОВ НАШЕЙ РАБОТЫ -->
      </p>
    </div>
  </div>

  <div class="row footer-lvl-2">
    <div class="col-xs-12 col-md-3 col-sm-3">
      <div class="footer-lvl-2-menu-head">КАТАЛОГ</div>
      <ul>
		<li><a href="/catalog/">Деревянные дома</a></li>
        <li><a href="/catalog/bani/">Бани</a></li>
        <li><a href="/catalog/dachnye-doma/">Дачные дома</a></li>
        <li><a href="/catalog/malye-stroeniya-besedki/">Малые строения</a></li>
        <li><a href="/catalog/sadovye-doma/">Садовые дома</a></li>
        <li><a href="/catalog/kottedzhi/">Коттеджи</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-md-3 col-sm-3">
      <div class="footer-lvl-2-menu-head">УСЛУГИ</div>
      <ul>
        <li><a href="/services/enginering_communications/">Инженерные системы</a></li>
        <li><a href="/services/individual_projects/">Индивидуальные проекты</a></li>
        <li><a href="/services/dostroika/">Достройка и реконструкция</a></li>
        <li><a href="/services/the-estates-catalogue/">Благоустройство участка</a></li>
        <li><a href="/services/addition2/">Пристройка террас и веранд</a></li>
        <li><a href="/about/news/zemelnye-uchastki-v-kompanii-terem/">Земельные участки</a></li>
        <li><a href="/services/credit-calculator/">Кредитование и страхование</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-md-3 col-sm-3">
      <div class="footer-lvl-2-menu-head">О КОМПАНИИ</div>
      <ul>
        <li><a href="/about/news/">Новости</a></li>
        <li><a href="/promotion/">Акции</a></li>
        <li><a href="/about/otzyvy/">Отзывы</a></li>
        <li><a href="/about/video/">Видео</a></li>
        <li><a href="/about/faq/">Ответы на вопросы</a></li>
        <li><a href="/about/production/">Технологии производства</a></li>
        <li><a href="/contacts/">Контакты</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-md-3 col-sm-3">
        <div class="sr-contacts" itemscope itemtype="https://schema.org/Organization">
           <span class="hidden" itemprop="name">ООО "Теремъ-pro"</span>
            <div class="sr-contacts__address" itemprop="address">
                <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include_areas/footer/address.php",
                    array(),
                    array("NAME" => "Адрес")); ?>
               </div>
            <div class="sr-contacts__phone" itemprop="telephone">
                <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include_areas/footer/phone.php",
                    array(),
                    array("NAME" => "Телефон")); ?>
            </div>
            <div class="sr-contacts__mail" itemprop="email">
                <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include_areas/footer/mail.php",
                    array(),
                    array("NAME" => "Email")); ?>
            </div>
        </div>
      <div class="social">
        <ul>
          <li>
            <a target="_blank" href="https://www.facebook.com/terempro"><img src="/bitrix/templates/.default/assets/img/social/Terem_fbook_icon.png" alt="facebook"></a>
            <a rel="nofollow" target="_blank" href="http://instagram.com/pro_terem"><img src="/bitrix/templates/.default/assets/img/social/Terem_insta_icon.png" alt="instagram"></a>
            <a rel="nofollow" target="_blank" href="https://twitter.com/ProTerem"><img src="/bitrix/templates/.default/assets/img/social/Terem_tw_icon.png" alt="twitter"></a>
            <a rel="nofollow" target="_blank" href="http://vk.com/pro_terem"><img src="/bitrix/templates/.default/assets/img/social/Terem_VK_icon.png" alt="vkontakte"></a>
          </li>
          <li>
            <a rel="nofollow" target="_blank" href="http://terem-pro.livejournal.com"><img src="/bitrix/templates/.default/assets/img/social/Terem_LiveJ_icon.png" alt="livejournal"></a>
            <a rel="nofollow" target="_blank" href="http://ok.ru/v2010godu"><img src="/bitrix/templates/.default/assets/img/social/od.png" alt="odnoklasniki"></a>
            <a rel="nofollow" target="_blank" href="http://www.terem-pro.ru/sitemap.xml"><img src="/bitrix/templates/.default/assets/img/social/map.png" alt="map"></a>
            <a rel="nofollow" target="_blank" href="https://www.youtube.com/c/terempro"><img src="/bitrix/templates/.default/assets/img/social/y.png" alt="map"></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row footer-lvl-2">
    <div class="col-xs-12 col-md-12 col-sm-12">
      <p><noindex>&copy; Теремъ-про, 2009 - <?=date("Y")?>. Все материалы данного сайта являются объектами авторского права (в том числе дизайн). Запрещается копирование, распространение (в том числе путем копирования на другие сайты и ресурсы в Интернете) или любое иное использование информации и объектов без предварительного согласия правообладателя. Cайт не является публичной офертой.</noindex></p>
    </div>
  </div>
</div>
</section>
<a href="#0" class="cd-top">Наверх</a>



<!-- Yandex.Metrika counter -->
<script type="text/javascript">
  (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
      try {
        w.yaCounter937330 = new Ya.Metrika({id:937330,
          webvisor:true,
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          triggerEvent:true,
          ut:"noindex"});
      } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
      d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
  })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/937330?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<!--Google analytics-->
<?php 
// записываем clientID в google userID
$cid = isset($_COOKIE['_ga']) ? substr(htmlspecialchars($_COOKIE['_ga']), 6) : '';
?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

/* ga('create', 'UA-55697220-1', 'auto'); */
ga('create', 'UA-55697220-1', 'auto', { 'userId': '<?php echo $cid; ?>' });
ga('send', 'pageview');

</script>


<script>
  window.onload = function()
  {
   if(jQuery('.catalog-item-description').length ==1)
   {
     jQuery('.my-form.form-advice').submit(function(){
       ga('send','event','form','submit','получить консультацию');
     });
   }
   else
   {

     jQuery('.my-form.form-advice').submit(function(){
       ga('send','event','form','submit','заказать');
     });
   }
   
    $('[data-ga]').on('submit', function(e)
    {  
        ga('send', 'event', 'form','submit', e.target.dataset.ga);
    });
	
	if (document.querySelector('[data-target="#formGetCode"]'))
	{
		document.querySelector('[data-target="#formGetCode"]').addEventListener('submit', function()
		{  
			ga('send', 'event', 'form','called', 'Вызов формы на отправку заявки с карточки дома');
		});
	}
    
	if (document.querySelector('[data-target="#formAdviceItem"]'))
	{
		document.querySelector('[data-target="#formAdviceItem"]').addEventListener('submit', function()
		{  
			ga('send', 'event', 'form','called', 'Вызов формы на отправку заявки с карточки дома');
		});
	}
      
      
      // Скрипт для показа информации о подарках на карточке дома


        /*$("#giftbutton").hover(function() {
            $(".popover").toggle();
        });*/
        var popover = $(".popover");
        $("#giftbutton").on({
            mouseover:function(){
                popover.show(150);
            },
            mouseout:function(){
                popover.hide(150);
            }
        });
 };
</script>
<!--Google analytics end-->

<!-- calltouch code -->
<script type="text/javascript">
(function (w, d, nv, ls, yac){
    var lwait = function (w, on, trf, dly, ma, orf, osf) { var pfx = "ct_await_", sfx = "_completed";  if(!w[pfx + on + sfx]) { var ci = clearInterval, si = setInterval, st = setTimeout , cmld = function () { if (!w[pfx + on + sfx]) {  w[pfx + on + sfx] = true; if ((w[pfx + on] && (w[pfx + on].timer))) { ci(w[pfx + on].timer);  w[pfx + on] = null;   }  orf(w[on]);  } };if (!w[on] || !osf) { if (trf(w[on])) { cmld();  } else { if (!w[pfx + on]) { w[pfx + on] = {  timer: si(function () { if (trf(w[on]) || ma < ++w[pfx + on].attempt) { cmld(); } }, dly), attempt: 0 }; } } }   else { if (trf(w[on])) { cmld();  } else { osf(cmld); st(function () { lwait(w, on, trf, dly, ma, orf); }, 0); } }} else {orf(w[on]);}};
    var ct = function (w, d, e, c, n) { var a = 'all', b = 'tou', src = b + 'c' + 'h';  src = 'm' + 'o' + 'd.c' + a + src; var jsHost = "https://" + src, s = d.createElement(e); var jsf = function (w, d, s, h, c, n, yc) { if (yc !== null) { lwait(w, 'yaCounter'+yc, function(obj) { return (obj && obj.getClientID ? true : false); }, 50, 100, function(yaCounter) { s.async = 1; s.src = jsHost + "." + "r" + "u/d_client.js?param;" + (yaCounter  && yaCounter.getClientID ? "ya_client_id" + yaCounter.getClientID() + ";" : "") + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":171110}") + ";"; p = d.getElementsByTagName(e)[0]; p.parentNode.insertBefore(s, p); }, function (f) { if(w.jQuery) {  w.jQuery(d).on('yacounter' + yc + 'inited', f ); }}); } else { s.async = 1; s.src = jsHost + "." + "r" + "u/d_client.js?param;" + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":171110}") + ";"; p = d.getElementsByTagName(e)[0]; p.parentNode.insertBefore(s, p); } }; if (!w.jQuery) { var jq = d.createElement(e); jq.src = jsHost + "." + "r" + 'u/js/jquery-1.7.min.js'; jq.onload = function () { lwait(w, 'jQuery', function(obj) { return (obj ? true : false); }, 30, 100, function () { jsf(w, d, s, jsHost, c, n, yac); }); }; p = d.getElementsByTagName(e)[0]; p.parentNode.insertBefore(jq, p); } else { jsf(w, d, s, jsHost, c, n, yac); } };
    var gaid = function (w, d, o, ct, n) { if (!!o) { lwait(w, o, function (obj) {  return (obj && obj.getAll ? true : false); }, 200, (nv.userAgent.match(/Opera|OPR\//) ? 10 : 20), function (gaCounter) { var clId = null; try {  var cnt = gaCounter && gaCounter.getAll ? gaCounter.getAll() : null; clId = cnt && cnt.length > 0 && !!cnt[0] && cnt[0].get ? cnt[0].get('clientId') : null; } catch (e) { console.warn("Unable to get clientId, Error: " + e.message); } ct(w, d, 'script', clId, n); }, function (f) { w[o](function () {  f(w[o]); })});} else{ ct(w, d, 'script', null, n); }};
    var cid  = function () { try { var m1 = d.cookie.match('(?:^|;)\\s*_ga=([^;]*)');if (!(m1 && m1.length > 1)) return null; var m2 = decodeURIComponent(m1[1]).match(/(\d+\.\d+)$/); if (!(m2 && m2.length > 1)) return null; return m2[1]} catch (err) {}}();
    if (cid === null && !!w.GoogleAnalyticsObject){
        if (w.GoogleAnalyticsObject=='ga_ckpr') w.ct_ga='ga'; else w.ct_ga = w.GoogleAnalyticsObject;
        if (typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1){new Promise(function (resolve) {var db, on = function () {  resolve(true)  }, off = function () {  resolve(false)}, tryls = function tryls() { try { ls && ls.length ? off() : (ls.x = 1, ls.removeItem("x"), off());} catch (e) { nv.cookieEnabled ? on() : off(); }};w.webkitRequestFileSystem ? webkitRequestFileSystem(0, 0, off, on) : "MozAppearance" in d.documentElement.style ? (db = indexedDB.open("test"), db.onerror = on, db.onsuccess = off) : /constructor/i.test(w.HTMLElement) ? tryls() : !w.indexedDB && (w.PointerEvent || w.MSPointerEvent) ? on() : off();}).then(function (pm){
            if (pm){gaid(w, d, w.ct_ga, ct, 2);}else{gaid(w, d, w.ct_ga, ct, 3);}})}else{gaid(w, d, w.ct_ga, ct, 4);}
    }else{ct(w, d, 'script', cid, 1);}})
(window, document, navigator, localStorage, "937330");
</script>
<!-- /calltouch code -->



<!-- POZVONIM -->
<!--script crossorigin="anonymous" async type="text/javascript" src="//api.pozvonim.com/widget/callback/v3/323b31e035c7a1db05113730eb1789dd/connect" id="check-code-pozvonim" charset="UTF-8"></script-->
<!-- EN POZ -->


<!-- google remarkiting-->

<!-- Код тега ремаркетинга Google -->
<!--------------------------------------------------
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 984788836;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/984788836/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<!-- {literal} -->





<!-- end google remarkiting -->

<?php include($_SERVER["DOCUMENT_ROOT"]."/include/modals/modals.php"); ?>


<?php
$page = $APPLICATION->GetCurPage();
?>

<?php if($page == '/contacts/'):?>
  <script src='//api-maps.yandex.ru/2.1/?lang=ru_RU'></script>
<?php endif;?>



<?php include $_SERVER['DOCUMENT_ROOT'].'/include/scripts/city.php'; ?>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/select2.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/form.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/validator.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.transit.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.threesixty.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/main.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/new_filter.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/.default/assets/js/jquery.mousewheel.min.js"></script>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-223969-1G1se';</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1949162515147008');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1949162515147008&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->



<script src="https://apis.google.com/js/platform.js" async defer></script>
</body>
</html>
