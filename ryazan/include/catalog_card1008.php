<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
global $APPLICATION;

$detect = new Mobile_Detect;
?>

<?php if($_GET['type'] == 'load_complect'):?>

    <?php
    //echo $_GET['element_id'];

    $arSVariant = Array(
        "ID",
        "NAME",
        "PROPERTY_COMPECTATION_HOUSE",
        "PROPERTY_OPTIMAL_COMPLECT",
    );

    $arFVariant = Array(
        "IBLOCK_ID" => 21,
        "ID" =>$_GET['element_id'],
    );

    $res = CIBlockElement::GetList(Array(), $arFVariant, false, Array("nPageSize" => 1), $arSVariant);
    while ($ob = $res->GetNextElement()) {
        $arCHouse = $ob->GetFields();
    }

    //echo "<pre>";
    //var_dump($arCHouse);


    ?>




    <?php if(count($arCHouse["PROPERTY_COMPECTATION_HOUSE_VALUE"]) > 0 && $_GET['variant'] != 'winter'):?>
	  <ul>
      <?php foreach($arCHouse["PROPERTY_COMPECTATION_HOUSE_VALUE"] as $k => $v): ?>

          <?php if($v["TEXT"] == '' || $v["TEXT"] == '.'):?>
          <li class="variant__parameters__title">
              <?=$arCHouse["PROPERTY_COMPECTATION_HOUSE_DESCRIPTION"][$k]?>
          </li>
          <?php else:?>
          <li>
              <strong><?=$arCHouse["PROPERTY_COMPECTATION_HOUSE_DESCRIPTION"][$k]?></strong>
              <br/>
              <?= $v["TEXT"] ?>
          </li>
          <?php endif;?>
      <?php endforeach; ?>
	  </ul>

    <?php else:?>
			<?php if(count($arCHouse["PROPERTY_OPTIMAL_COMPLECT_VALUE"]) > 0 && $_GET['variant'] == 'winter'):?>
			 <ul>
				<?php foreach($arCHouse["PROPERTY_OPTIMAL_COMPLECT_VALUE"] as $k => $v): ?>

				  <?php if($v["TEXT"] == '' || $v["TEXT"] == '.'):?>
				  <li class="variant__parameters__title">
					  <?=$arCHouse["PROPERTY_OPTIMAL_COMPLECT_DESCRIPTION"][$k]?>
				  </li>
				  <?php else:?>
				  <li>
					  <strong><?=$arCHouse["PROPERTY_OPTIMAL_COMPLECT_DESCRIPTION"][$k]?></strong>
					  <br/>
					  <?= $v["TEXT"] ?>
				  </li>
				  <?php endif;?>
			  <?php endforeach; ?>

			   </ul>
			<?php else:?>
				<p><strong>Описание комплектации в разработке.</strong> Приносим свои извинения.</p>
			<?php endif;?>

    <?php endif;?>




<?php endif; ?>

<?php if($_GET['type'] == 'complectation'): ?>
    <?php

        $technology = $_GET['type_tech'];
        $item = (int)$_GET['id_item'];

        if($technology == 'karkas'){
            $cnt = 0;
            $arSelect = Array("ID", "PROPERTY_KARKAS_VARIANTS_HOUSE");
            $arFilter = Array(
                "IBLOCK_ID" => 13, 
                'ID' => $item, 
                "ACTIVE"=>"Y"
                );

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();

            }

            if(count($arFields["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"]) > 0){
                foreach($arFields["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"] as $variant){

                    $arSelectVariant = Array(
                        "ID",
                        "NAME",
                        "CODE",
                        "IBLOCK_SECTION_ID",
                        "PROPERTY_AS_FILE",
                        "PROPERTY_ID_CALCULATOR",
                        "PROPERTY_PRICE_HOUSE_RYAZAN",
                        "PROPERTY_SQUARE_ALL_HOUSE",
                        "PROPERTY_COMPECTATION_HOUSE",
                        "PROPERTY_LETO",
                        "PROPERTY_ZIMA",
                        "PROPERTY_FAMALY",
                        "PROPERTY_TEHNOLOGY_HOUSE",
                        "PROPERTY_INGS_LIST"
                    );

                    $arFilterVariant = Array(
                        "IBLOCK_ID" => 21,
                        "ID" => $variant,
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        
                    );

                    $res = CIBlockElement::GetList(Array(), $arFilterVariant, false, Array("nPageSize" => 1), $arSelectVariant);
                    while ($ob = $res->GetNextElement()) {
                        $arComplectHouse[$cnt] = $ob->GetFields();
                        $cnt++;
                    }

                }
                
               

               foreach($arComplectHouse as $k => $v){

                   $arFilterPrice = Array(
                       "IBLOCK_ID" => $CITY_CODES_ARRAY["RYAZAN"],
                       "ID" => $v["PROPERTY_PRICE_HOUSE_RYAZAN_VALUE"],
                       "ACTIVE_DATE" => "Y",
                       "ACTIVE" => "Y",
                       "!PROPERTY_PRICE_1" => false,
                   );

                   $arSelectPrice = Array(
                       "ID",
                       "NAME",
                       "PROPERTY_PRICE_1",
                       "PROPERTY_PRICE_2",
                       "PROPERTY_PRICE_3",
                       "PROPERTY_OPTIMAL_PRICE",
                       "PROPERTY_DISCONT1",
                   );

                   $res = CIBlockElement::GetList(Array(), $arFilterPrice, false, Array("nPageSize" => 1), $arSelectPrice);
                   while ($ob = $res->GetNextElement()) {
                       $price = $ob->GetFields();
                       $arComplectHouse[$k]['PRICE_DEFAULT'] = $price["PROPERTY_PRICE_1_VALUE"];
                       $arComplectHouse[$k]['PRICE_BIG'] = $price["PROPERTY_PRICE_1_VALUE"];
                       $arComplectHouse[$k]['DISCONT1'] = $price["PROPERTY_DISCONT1_VALUE"];


                       $file = $_SERVER['DOCUMENT_ROOT'].'/upload/calcs/' . $arComplectHouse[$k]["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' .$arComplectHouse[$k]["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
                       $f = fopen($file, 'r');
                       $file_data = fread($f, filesize($file));
                       fclose($f);

                       $result = unserialize($file_data);
                       if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный'){

                           $discont1 = 30;

                           if($arComplectHouse[$k]['DISCONT1'] != NULL && $arComplectHouse[$k]['DISCONT1'] > 0){
                               $discont1 = $arComplectHouse[$k]['DISCONT1'];
                           }

                           $price = 0;

                           foreach ($result[0]["ITEMS"] as $i) {
                               $p = (int) $i['PRICE'];
                               $price = $price + ($p - (($p * $discont1) / 100));
                           }

                           $str = $arComplectHouse[$k]['PRICE_DEFAULT'].'000';
                           $arComplectHouse[$k]['OPTIMAL_PRICE'] = $str + $price;
                           //$arComplectHouse[$k]['DOP'] = $price;


                       }

                   }




               }


            }



        }

        if($technology == 'brus'){
            $cnt = 0;
            $arSelect = Array("ID", "PROPERTY_BRUS_VARIANTS_HOUSE");
            $arFilter = Array("IBLOCK_ID" => 13, 'ID' => $item, "ACTIVE"=>"Y");

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();

            }

            if(count($arFields["PROPERTY_BRUS_VARIANTS_HOUSE_VALUE"]) > 0){
                foreach($arFields["PROPERTY_BRUS_VARIANTS_HOUSE_VALUE"] as $variant){

                    $arSelectVariant = Array(
                        "ID",
                        "NAME",
                        "CODE",
                        "IBLOCK_SECTION_ID",
                        "PROPERTY_AS_FILE",
                        "PROPERTY_ID_CALCULATOR",
                        "PROPERTY_PRICE_HOUSE_RYAZAN",
                        "PROPERTY_SQUARE_ALL_HOUSE",
                        "PROPERTY_COMPECTATION_HOUSE",
                        "PROPERTY_LETO",
                        "PROPERTY_ZIMA",
                        "PROPERTY_FAMALY",
                        "PROPERTY_TEHNOLOGY_HOUSE",
                        "PROPERTY_INGS_LIST"
                    );

                    $arFilterVariant = Array(
                        "IBLOCK_ID" => 21,
                        "ID" => $variant,
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y"
                    );

                    $res = CIBlockElement::GetList(Array(), $arFilterVariant, false, Array("nPageSize" => 1), $arSelectVariant);
                    while ($ob = $res->GetNextElement()) {
                        $arComplectHouse[$cnt] = $ob->GetFields();
                        $cnt++;
                    }

                }

                foreach($arComplectHouse as $k => $v){

                    $arFilterPrice = Array(
                        "IBLOCK_ID" => $CITY_CODES_ARRAY['RYAZAN'] ,
                        "ID" => $v["PROPERTY_PRICE_HOUSE_RYAZAN_VALUE"],
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y"
                    );

                    $arSelectPrice = Array(
                        "ID",
                        "NAME",
                        "PROPERTY_PRICE_1",
                        "PROPERTY_PRICE_2",
                        "PROPERTY_PRICE_3",
                        "PROPERTY_OPTIMAL_PRICE",
                        "PROPERTY_DISCONT1",
                    );

                    $res = CIBlockElement::GetList(Array(), $arFilterPrice, false, Array("nPageSize" => 1), $arSelectPrice);
                    while ($ob = $res->GetNextElement()) {
                        $price = $ob->GetFields();
                        $arComplectHouse[$k]['PRICE_DEFAULT'] = $price["PROPERTY_PRICE_1_VALUE"];
                        $arComplectHouse[$k]['PRICE_BIG'] = $price["PROPERTY_PRICE_1_VALUE"];
                        $arComplectHouse[$k]['DISCONT1'] = $price["PROPERTY_DISCONT1_VALUE"];


                        $file = $_SERVER['DOCUMENT_ROOT'].'/upload/calcs/' . $arComplectHouse[$k]["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' .$arComplectHouse[$k]["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
                        $f = fopen($file, 'r');
                        $file_data = fread($f, filesize($file));
                        fclose($f);

                        $result = unserialize($file_data);
                        if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный'){

                            $discont1 = 30;

                            if($arComplectHouse[$k]['DISCONT1'] != NULL && $arComplectHouse[$k]['DISCONT1'] > 0){
                                $discont1 = $arComplectHouse[$k]['DISCONT1'];
                            }

                            $price = 0;

                            foreach ($result[0]["ITEMS"] as $i) {
                                $p = (int) $i['PRICE'];
                                $price = $price + ($p - (($p * $discont1) / 100));
                            }

                            $str = $arComplectHouse[$k]['PRICE_DEFAULT'].'000';
                            $arComplectHouse[$k]['OPTIMAL_PRICE'] = $str + $price;



                        }

                    }




                }


            }
        }

        if($technology == 'kirpich'){
            $cnt = 0;
            $arSelect = Array("ID", "PROPERTY_KIRPICH_VARIANTS_HOUSE");
            $arFilter = Array("IBLOCK_ID" => 13, 'ID' => $item, "ACTIVE"=>"Y");

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();

            }



            if(count($arFields["PROPERTY_KIRPICH_VARIANTS_HOUSE_VALUE"]) > 0){
                foreach($arFields["PROPERTY_KIRPICH_VARIANTS_HOUSE_VALUE"] as $variant){

                    $arSelectVariant = Array(
                        "ID",
                        "NAME",
                        "CODE",
                        "IBLOCK_SECTION_ID",
                        "PROPERTY_AS_FILE",
                        "PROPERTY_ID_CALCULATOR",
                        "PROPERTY_PRICE_HOUSE_RYAZAN",
                        "PROPERTY_SQUARE_ALL_HOUSE",
                        "PROPERTY_COMPECTATION_HOUSE",
                        "PROPERTY_LETO",
                        "PROPERTY_ZIMA",
                        "PROPERTY_FAMALY",
                        "PROPERTY_TEHNOLOGY_HOUSE",
                        "PROPERTY_INGS_LIST",
                    );

                    $arFilterVariant = Array(
                        "IBLOCK_ID" => 21,
                        "ID" => $variant,
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y"
                    );

                    $res = CIBlockElement::GetList(Array(), $arFilterVariant, false, Array("nPageSize" => 1), $arSelectVariant);
                    while ($ob = $res->GetNextElement()) {
                        $arComplectHouse[$cnt] = $ob->GetFields();
                        $cnt++;
                    }

                }



                foreach($arComplectHouse as $k => $v){

                    $arFilterPrice = Array(
                        "IBLOCK_ID" => $CITY_CODES_ARRAY["RYAZAN"],
                        "ID" => $v["PROPERTY_PRICE_HOUSE_RYAZAN_VALUE"],
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "!PROPERTY_PRICE_1" => false,
                    );

                    $arSelectPrice = Array(
                        "ID",
                        "NAME",
                        "PROPERTY_PRICE_1",
                        "PROPERTY_PRICE_2",
                        "PROPERTY_PRICE_3",
                        "PROPERTY_OPTIMAL_PRICE",
                        "PROPERTY_DISCONT1",
                    );

                    $res = CIBlockElement::GetList(Array(), $arFilterPrice, false, Array("nPageSize" => 1), $arSelectPrice);
                    while ($ob = $res->GetNextElement()) {
                        $price = $ob->GetFields();
                        $arComplectHouse[$k]['PRICE_DEFAULT'] = $price["PROPERTY_PRICE_1_VALUE"];
                        $arComplectHouse[$k]['PRICE_BIG'] = $price["PROPERTY_PRICE_1_VALUE"];
                        $arComplectHouse[$k]['DISCONT1'] = $price["PROPERTY_DISCONT1_VALUE"];


                        $file = $_SERVER['DOCUMENT_ROOT'].'/upload/calcs/' . $arComplectHouse[$k]["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' .$arComplectHouse[$k]["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
                        $f = fopen($file, 'r');
                        $file_data = fread($f, filesize($file));
                        fclose($f);

                        $result = unserialize($file_data);



                        if ($result[0]["NAME"] == 'Пакет Оптимальный' || $result[0]["NAME"] == 'Пакет  Оптимальный'){

                            $discont1 = 30;

                            if($arComplectHouse[$k]['DISCONT1'] != NULL && $arComplectHouse[$k]['DISCONT1'] > 0){
                                $discont1 = $arComplectHouse[$k]['DISCONT1'];
                            }

                            $price = 0;

                            foreach ($result[0]["ITEMS"] as $i) {
                                $p = (int) $i['PRICE'];
                                $price = $price + ($p - (($p * $discont1) / 100));
                            }

                            $str = $arComplectHouse[$k]['PRICE_DEFAULT'].'000';
                            $arComplectHouse[$k]['OPTIMAL_PRICE'] = $str + $price;




                        }

                    }




                }


            }
        }


        //echo "<pre>";
        //var_dump($arComplectHouse);
    ?>
    <?php if(count($arComplectHouse) > 0):?>
    <div class="row">

        <?php if($arComplectHouse[0]["PRICE_DEFAULT"]):?>
        <div class="col-md-4">
            <div class="variant">
                <header class="variant__header">
                    <h2 class="title--price">
                        <?=number_format($arComplectHouse[0]["PRICE_DEFAULT"], 3, ' ', ' ');?> р.
                    </h2>
                    <h3 class="title--subtitle">
                        <?php if(count($arComplectHouse) == 1 && $technology == 'kirpich'):?>

                        <?php else: ?>
                            <?php if($arComplectHouse[0]['PROPERTY_TEHNOLOGY_HOUSE_VALUE'] == 'Брус клееный 180' || $arComplectHouse[0]['PROPERTY_TEHNOLOGY_HOUSE_VALUE'] == 'Каркасный 200'):?>
                                Для постоянного проживания
                            <?php else:?>
                                Лето
                            <?php endif; ?>
                        <?php endif; ?>
                    </h3>
                    <h4 class="title--small title--grey">
                        <?=$arComplectHouse[0]['PROPERTY_SQUARE_ALL_HOUSE_VALUE']?> кв.м в этой комплектации
                    </h4>
                </header>

                <div class="variant__parameters">

                <?php if ($arComplectHouse[0]["PROPERTY_INGS_LIST_VALUE"]): ?>
                <a href="#modal-package" data-item="<?=$arComplectHouse[0]['ID']?>" data-link="show-variant-modal" class="show-variant-modal">Пакет инженерных коммуникаций</a>
                <?php endif; ?>
                <a href="" data-link="show-extra-params" data-type="summer-house"  data-house="<?=$arComplectHouse[0]['ID']?>" class="show-extra-params">Посмотреть полное описание</a>

                <?php if($arComplectHouse[0]['PROPERTY_TEHNOLOGY_HOUSE_VALUE'] == 'Брус клееный 180' || $arComplectHouse[0]['PROPERTY_TEHNOLOGY_HOUSE_VALUE'] == 'Каркасный 200'):?>
                    <?php if(count($arComplectHouse[0]['PROPERTY_FAMALY_VALUE']) > 0):?>
                        <h5 class="variant__parameters__title">Коротко о комплектации</h5>
                        <ul>
                            <?php foreach($arComplectHouse[0]['PROPERTY_FAMALY_VALUE'] as $v):?>
                                <li>
                                    <?=$v?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php else:?>
                    <?php if(count($arComplectHouse[0]['PROPERTY_LETO_VALUE']) > 0):?>
                        <h5 class="variant__parameters__title">Коротко о комплектации</h5>
                        <ul>
                            <?php foreach($arComplectHouse[0]['PROPERTY_LETO_VALUE'] as $v):?>
                                <li>
                                    <?=$v?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endif;?>
                </div>
                <?php if(!$detect->isMobile()):?>
                <footer class="variant__footer">
                    <div class="variant__btns-group">
                        <?php if(!$detect->isMobile()):?>
                       <!--  <button disabled="disabled">
                            Сравнить
                        </button> -->
                        <?php endif; ?>
                        <button type="button"  onclick="location.href='/ryazan/additional-service/<?=$arComplectHouse[0]['CODE']?>/';" >
                            Калькулятор доп.услуг
                        </button>
                    </div>
                    <hr>
                </footer>
                <div class="extra-params load-ajax-params" id="">
                        <div class="loader"></div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>

		<?php
		//echo "<pre>";
		//var_dump($arComplectHouse[0]['OPTIMAL_PRICE']);

		?>

        <?php if($arComplectHouse[0]['OPTIMAL_PRICE']):?>
        <div class="col-md-4">
            <div class="variant">
                <header class="variant__header">
                    <h2 class="title--price">
                        <?=number_format($arComplectHouse[0]['OPTIMAL_PRICE'], 0, ' ', ' ');?> р.
                    </h2>
                    <h3 class="title--subtitle">
                        Зима
                    </h3>
                    <h4 class="title--small title--grey">
                        <?=$arComplectHouse[0]['PROPERTY_SQUARE_ALL_HOUSE_VALUE']?> кв.м в этой комплектации
                    </h4>
                </header>

                <div class="variant__parameters">
                    <?php if ($arComplectHouse[0]["PROPERTY_INGS_LIST_VALUE"]): ?>
                    <a href="#modal-package" data-item="<?=$arComplectHouse[0]['ID']?>" data-link="show-variant-modal" class="show-variant-modal">Пакет инженерных коммуникаций</a>
                    <?php endif; ?>
                    <a href="" data-link="show-extra-params" data-type="winter-house" data-house="<?=$arComplectHouse[0]['ID']?>" class="show-extra-params">Посмотреть полное описание</a>

                    <?php if(count($arComplectHouse[0]['PROPERTY_ZIMA_VALUE']) > 0):?>
                        <h5 class="variant__parameters__title">Коротко о комплектации</h5>
                        <ul>
                            <?php foreach($arComplectHouse[0]['PROPERTY_ZIMA_VALUE'] as $v):?>
                                <li>
                                    <?=$v?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <?php if(!$detect->isMobile()):?>
                <footer class="variant__footer">
                    <div class="variant__btns-group">
                        <?php if(!$detect->isMobile()):?>
                       <!--  <button disabled="disabled">
                            Сравнить
                        </button> -->
                        <?php endif; ?>
                        <button type="button"  onclick="location.href='/ryazan/additional-service/<?=$arComplectHouse[0]['CODE']?>/?optimal';" >
                            Калькулятор доп.услуг
                        </button>
                    </div>
                    <hr>
                </footer>

                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if($arComplectHouse[1]["PRICE_DEFAULT"]):?>
        <div class="col-md-4">
            <div class="variant">
                <header class="variant__header">
                    <h2 class="title--price">
                        <?=number_format($arComplectHouse[1]["PRICE_DEFAULT"], 3, ' ', ' ');?> р.
                    </h2>
                    <h3 class="title--subtitle">
                        Для постоянного проживания
                    </h3>
                    <h4 class="title--small title--grey">
                        <?=$arComplectHouse[1]['PROPERTY_SQUARE_ALL_HOUSE_VALUE']?> кв.м в этой комплектации
                    </h4>
                </header>

                <div class="variant__parameters">
                    <?php if ($arComplectHouse[1]["PROPERTY_INGS_LIST_VALUE"]): ?>
                    <a href="#modal-package" data-item="<?=$arComplectHouse[1]['ID']?>" data-link="show-variant-modal" class="show-variant-modal">Пакет инженерных коммуникаций</a>
                    <?php endif; ?>
                    <a href="" data-link="show-extra-params" data-type="family-house" data-house="<?=$arComplectHouse[1]['ID']?>" class="show-extra-params">Посмотреть полное описание</a>

                    <?php if(count($arComplectHouse[1]['PROPERTY_FAMALY_VALUE']) > 0):?>
                        <h5 class="variant__parameters__title">Коротко о комплектации</h5>
                        <ul>
                            <?php foreach($arComplectHouse[1]['PROPERTY_FAMALY_VALUE'] as $v):?>
                                <li>
                                    <?=$v?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <?php if(!$detect->isMobile()):?>
                <footer class="variant__footer">
                    <div class="variant__btns-group">
                        <?php if(!$detect->isMobile()):?>
                        <!-- <button disabled="disabled">
                            Сравнить
                        </button> -->
                        <?php endif; ?>
                        <button type="button"  onclick="location.href='/ryazan/additional-service/<?=$arComplectHouse[1]['CODE']?>/';" >
                            Калькулятор доп.услуг
                        </button>
                    </div>

                    <hr>
                </footer>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>

    </div>


    <?php endif; ?>
<?php endif; ?>

<?php if($_GET['type'] == 'variant-modal'): ?>

    <?php

             $arSelectVariant = Array(
                        "ID",
                        "NAME",
                        "CODE",
                        "IBLOCK_SECTION_ID",
                        "PROPERTY_INGS_LIST",
                    );

                    $arFilterVariant = Array(
                        "IBLOCK_ID" => 21,
                        "ID" => (int)$_GET['houseID'],
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y"
                    );

            $res = CIBlockElement::GetList(Array(), $arFilterVariant, false, Array("nPageSize" => 1), $arSelectVariant);
                    while ($ob = $res->GetNextElement()) {
                        $House = $ob->GetFields();
                    }

    ?>
    <?php if($House["PROPERTY_INGS_LIST_VALUE"]):?>
    <?php

                    $Select = Array(
                        "ID",

                        "PROPERTY_ING_LIST",
                    );

                    $Filter = Array(
                        "IBLOCK_ID" => 38,
                        "ID" => (int)$House["PROPERTY_INGS_LIST_VALUE"],
                    );

            $res = CIBlockElement::GetList(Array(), $Filter, false, Array("nPageSize" => 1), $Select);
                    while ($ob = $res->GetNextElement()) {
                        $List[] = $ob->GetFields();
                    }
?>
    <ul>
    <?php foreach($List[0]["PROPERTY_ING_LIST_VALUE"] as $text):?>
    <li><?=$text?></li>
    <?php endforeach; ?>
    </ul>


    <?php else:?>

            <div class="variant__warning">
                <p><strong> Описание комплектации в разработке.</strong><br>Приносим свои извинения.</p>
            </div>



    <?php endif; ?>
<?php endif; ?>

<?php if($_GET['type'] == 'price'): ?>

    <?php
    if(CModule::IncludeModule("iblock")){

        $element_id = (int)$_GET['element_id'];

        $arSelect = Array(
            "ID",
            "PROPERTY_PRICE_HOUSE_RYAZAN",
        );

        $arFilter = Array(
            "IBLOCK_ID" => 21,
            "ID" => $element_id,
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $res = CIBlockElement::GetList(
            Array(),
            $arFilter,
            false,
            Array("nPageSize"=>500),
            $arSelect
        );

        while($ob = $res->GetNextElement())
        {
            $elementArr = $ob->GetFields();
        }


        $arSelectPrice = Array(
            "ID",
            "NAME",
            "PROPERTY_PRICE_1",
            "PROPERTY_PRICE_2",
            "PROPERTY_PRICE_3",
            "PROPERTY_OPTIMAL_PRICE",
            "PROPERTY_KEY_PRICE",
            "PROPERTY_KEY_DECRIPTION",
            "PROPERTY_KEY_PRICE2",
            "PROPERTY_KEY_PRICE3",
            "PROPERTY_DISCONT1",
            "PROPERTY_DISCONT2"
        );

        $arFilter2 = Array(
            "IBLOCK_ID" => $CITY_CODES_ARRAY["RYAZAN"],
            "ID" => $elementArr["PROPERTY_PRICE_HOUSE_RYAZAN_VALUE"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );

        $res = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize" => 1), $arSelectPrice);
        while ($ob = $res->GetNextElement()) {
            $PRICES = $ob->GetFields();
        }




        $profit = ($PRICES["PROPERTY_PRICE_2_VALUE"]-$PRICES["PROPERTY_PRICE_1_VALUE"]);
    }



    ?>

    <span class="current-price"><?=number_format($PRICES["PROPERTY_PRICE_1_VALUE"], 3, ' ', ' ');?> р.</span>
    <?php if ($PRICES["PROPERTY_PRICE_1_VALUE"] != $PRICES["PROPERTY_PRICE_2_VALUE"]): ?>
    <span class="future-price"><?=number_format($PRICES["PROPERTY_PRICE_2_VALUE"], 3, ' ', ' ');?> р.</span>
    <span class="spread-price">выгода сегодня <?=number_format($profit, 3, ' ', ' ');?> р.</span>
    <?php endif; ?>
<?php endif; ?>


<?php if($_GET['type'] == 'img'): ?>

<?php

    if(CModule::IncludeModule("iblock")){
        $element_id = (int)$_GET['element_id'];

        $arSelect = Array(
            "ID",
            "CODE",
            "NAME",
            "DETAIL_PICTURE",
            "PROPERTY_PLANS_HOUSE_1",
            "PROPERTY_PLANS_HOUSE_2",
            "PROPERTY_D_RACURS",
            "PROPERTY_NITERIER",
            "PROPERTY_EXTERIER",
            "PROPERTY_TEHNOLOGY_HOUSE"

        );

        $arFilter = Array(
            "IBLOCK_ID"=>21,
            "ID" => $element_id,
            "ACTIVE"=>"Y"
        );

        $res = CIBlockElement::GetList(
            Array(),
            $arFilter,
            false,
            Array("nPageSize"=>500),
            $arSelect
        );

        while($ob = $res->GetNextElement())
        {
            $elementArr = $ob->GetFields();
        }


    }

    //echo "<pre>";
    //var_dump($elementArr);


?>
    <ul class="slider-wrapper" data-item="slider-item-catalog">

         <?php if($elementArr['DETAIL_PICTURE']):?>
            <li class="owl-item" id="photo-slide">
                <img src="<?=CFile::GetPath($elementArr['DETAIL_PICTURE'])?>">
                <?php if($elementArr["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                <div class="caption">
                    <?php if(CFile::GetFileArray($elementArr['DETAIL_PICTURE'])["DESCRIPTION"]): ?><?=CFile::GetFileArray($elementArr['DETAIL_PICTURE'])["DESCRIPTION"]?><?php endif; ?>
                    Варианты пристроек к этому дому в разделе <a href="/ryazan/additional-service/<?=$elementArr['CODE']?>/">калькулятор доп.услуг</a>
                </div>
                <?php endif; ?>

            </li>
        <?php else:?>
            <li class="owl-item" id="photo-slide">
                <img src="http://placehold.it/838x556">
            </li>
        <?php endif; ?>



    <?php if(!$detect->isMobile()):?>
        <?php if($elementArr['PROPERTY_D_RACURS_VALUE']):?>
            <li class="owl-item" id="3D-slide">
                <div id="i3d" class="i3d-gallary">
                    <?=htmlspecialchars_decode($elementArr['PROPERTY_D_RACURS_VALUE']['TEXT'])?>
                </div>
            </li>
        <?php endif; ?>
        <?php endif;?>

        <?php if($elementArr['PROPERTY_PLANS_HOUSE_1_VALUE']):?>
            <li class="owl-item" id="layout_1-slide">
                <img src="<?=CFile::GetPath($elementArr['PROPERTY_PLANS_HOUSE_1_VALUE'])?>">
                <?php if($elementArr["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                <div class="caption">
                    <?php if($elementArr["PROPERTY_PLANS_HOUSE_1_DESCRIPTION"]): ?><?=$elementArr["PROPERTY_PLANS_HOUSE_1_DESCRIPTION"]?><?php endif; ?>
                    Варианты пристроек к этому дому в разделе <a href="/ryazan/additional-service/<?=$elementArr['CODE']?>/">калькулятор доп.услуг</a></a>
                </div>
                <?php endif; ?>

            </li>
        <?php endif; ?>

        <?php if($elementArr['PROPERTY_PLANS_HOUSE_2_VALUE']):?>
            <li class="owl-item" id="layout_2-slide">
                <img src="<?=CFile::GetPath($elementArr['PROPERTY_PLANS_HOUSE_2_VALUE'])?>">
                <?php if($elementArr["PROPERTY_TEHNOLOGY_HOUSE_VALUE"] != 'Кирпичный'): ?>
                <div class="caption">
                    <?php if($elementArr["PROPERTY_PLANS_HOUSE_2_DESCRIPTION"]): ?><?=$elementArr["PROPERTY_PLANS_HOUSE_2_DESCRIPTION"]?><?php endif; ?>
                    Варианты пристроек к этому дому в разделе <a href="/ryazan/additional-service/<?=$elementArr['CODE']?>/">калькулятор доп.услуг</a></a>
                </div>
                <?php endif; ?>
            </li>
        <?php endif; ?>




    <?php if(!$detect->isMobile()):?>
        <?php foreach($elementArr["PROPERTY_EXTERIER_VALUE"] as $item):?>
            <?php
            $renderImage = CFile::ResizeImageGet($item, Array("width" => 838, "height" => 556));
            ?>
            <li class="owl-item">
                <img src="<?= $renderImage['src']?>">
            </li>
        <?php endforeach; ?>
        <?php foreach($elementArr["PROPERTY_NITERIER_VALUE"] as $item):?>
            <?php
            $renderImage = CFile::ResizeImageGet($item, Array("width" => 838, "height" => 556));
            ?>
            <li class="owl-item">
                <img src="<?= $renderImage['src']?>">
            </li>
        <?php endforeach; ?>
        <?php endif; ?>

    </ul>
    <div class="slider-previews">
        <ul class="slider-previews__list" data-item="slider-catalog-side">
            <?php if($elementArr['DETAIL_PICTURE']):?>
                <?php
                $renderImage = CFile::ResizeImageGet($elementArr['DETAIL_PICTURE'], Array("width" => 120, "height" => 80));
                ?>
                <li data-link="photo-slide">
                    <img src="<?=$renderImage['src']?>">
                </li>
            <?php else:?>
                <li data-link="photo-slide">
                    <img src="http://placehold.it/120x80">
                </li>
            <?php endif; ?>
    <?php if(!$detect->isMobile()):?>
            <?php if($elementArr['PROPERTY_D_RACURS_VALUE']):?>
                <li data-link="3D-slide" class="threesixty-slide">
                    <img src="/bitrix/templates/.default/assets/img/catalog-item/360.svg">
                </li>
            <?php endif; ?>
        <?php endif; ?>

            <?php if($elementArr['PROPERTY_PLANS_HOUSE_1_VALUE']):?>
                <?php
                $renderImage = CFile::ResizeImageGet($elementArr['PROPERTY_PLANS_HOUSE_1_VALUE'], Array("width" => 120, "height" => 80));
                ?>
                <li data-link="layout_1-slide" class="layout_1-holder" >
                    <img src="<?=$renderImage['src']?>">
                </li>
            <?php endif; ?>

            <?php if($elementArr['PROPERTY_PLANS_HOUSE_2_VALUE']):?>
                <?php
                $renderImage = CFile::ResizeImageGet($elementArr['PROPERTY_PLANS_HOUSE_2_VALUE'], Array("width" => 120, "height" => 80));
                ?>
                <li data-link="layout_2-slide" class="layout_2-holder" >
                    <img src="<?=$renderImage['src']?>">
                </li>
            <?php endif; ?>


    <?php if(!$detect->isMobile()):?>
            <?php foreach($elementArr["PROPERTY_EXTERIER_VALUE"] as $item):?>
                <?php
                $renderImage = CFile::ResizeImageGet($item, Array("width" => 120, "height" => 80));
                ?>
                <li class="owl-item">
                    <img src="<?= $renderImage['src']?>">
                </li>
            <?php endforeach; ?>
            <?php foreach($elementArr["PROPERTY_NITERIER_VALUE"] as $item):?>
                <?php
                $renderImage = CFile::ResizeImageGet($item, Array("width" => 120, "height" => 80));
                ?>
                <li class="owl-item">
                    <img src="<?= $renderImage['src']?>">
                </li>
            <?php endforeach; ?>
        <?php endif; ?>

        </ul>
    </div>




<?php endif; ?>





<?php if($_GET['type'] == 'img2'): ?>

<?php

    if(CModule::IncludeModule("iblock")){
        $element_id = (int)$_GET['element_id'];

        $arSelect = Array(
            "ID",
            "CODE",
            "PROPERTY_TEHNOLOGY_HOUSE",
            "PROPERTY_PLANS_HOUSE_3",
            "PROPERTY_PLANS_HOUSE_4",
            "PROPERTY_PLANS_HOUSE_5",
            "PROPERTY_PLANS_HOUSE_6",

        );

        $arFilter = Array(
            "IBLOCK_ID"=>21,
            "ID" => $element_id,
            "ACTIVE"=>"Y"
        );

        $res = CIBlockElement::GetList(
            Array(),
            $arFilter,
            false,
            Array("nPageSize"=>500),
            $arSelect
        );

        while($ob = $res->GetNextElement())
        {
            $elementArr2 = $ob->GetFields();
        }


    }

    //echo "<pre>";
    //var_dump($elementArr);


?>

    <?php if($elementArr2['PROPERTY_PLANS_HOUSE_3_VALUE'] != NULL):?>
     <ul class="slider-wrapper" data-item="slider-item-catalog" data-type="house-slider">



        <?php if($elementArr2['PROPERTY_PLANS_HOUSE_3_VALUE']):?>
            <li class="owl-item" id="layout_3-slide">
                <img src="<?=CFile::GetPath($elementArr2['PROPERTY_PLANS_HOUSE_3_VALUE'])?>">
                <div class="caption">
                    <?php if($elementArr2["PROPERTY_PLANS_HOUSE_3_DESCRIPTION"]): ?><?=$elementArr2["PROPERTY_PLANS_HOUSE_3_DESCRIPTION"]?><?php endif; ?>
                    Варианты пристроек к этому дому в разделе <a href="/ryazan/additional-service/<?=$elementArr2['CODE']?>/">калькулятор доп.услуг</a>
                </div>

            </li>
        <?php endif; ?>

        <?php if($elementArr2['PROPERTY_PLANS_HOUSE_4_VALUE']):?>
            <li class="owl-item" id="layout_4-slide">
                <img src="<?=CFile::GetPath($elementArr2['PROPERTY_PLANS_HOUSE_4_VALUE'])?>">
                <div class="caption">
                    <?php if($elementArr2["PROPERTY_PLANS_HOUSE_4_DESCRIPTION"]): ?><?=$elementArr2["PROPERTY_PLANS_HOUSE_4_DESCRIPTION"]?><?php endif; ?>
                    Варианты пристроек к этому дому в разделе <a href="/ryazan/additional-service/<?=$elementArr2['CODE']?>/">калькулятор доп.услуг</a>
                </div>
            </li>
        <?php endif; ?>

        <?php if($elementArr2['PROPERTY_PLANS_HOUSE_5_VALUE']):?>
            <li class="owl-item" id="layout_5-slide">
                <img src="<?=CFile::GetPath($elementArr2['PROPERTY_PLANS_HOUSE_5_VALUE'])?>">
                <div class="caption">
                    <?php if($elementArr2["PROPERTY_PLANS_HOUSE_5_DESCRIPTION"]): ?><?=$elementArr2["PROPERTY_PLANS_HOUSE_5_DESCRIPTION"]?><?php endif; ?>
                    Варианты пристроек к этому дому в разделе <a href="/ryazan/additional-service/<?=$elementArr2['CODE']?>/">калькулятор доп.услуг</a>
                </div>
            </li>
        <?php endif; ?>

        <?php if($elementArr2['PROPERTY_PLANS_HOUSE_6_VALUE']):?>
            <li class="owl-item" id="layout_6-slide">
                <img src="<?=CFile::GetPath($elementArr2['PROPERTY_PLANS_HOUSE_6_VALUE'])?>">
                <div class="caption">
                    <?php if($elementArr2["PROPERTY_PLANS_HOUSE_6_DESCRIPTION"]): ?><?=$elementArr2["PROPERTY_PLANS_HOUSE_6_DESCRIPTION"]?><?php endif; ?>
                    Варианты пристроек к этому дому в разделе <a href="/ryazan/additional-service/<?=$elementArr2['CODE']?>/">калькулятор доп.услуг</a>
                </div>
            </li>
        <?php endif; ?>



    </ul>
    <div class="slider-previews">
        <ul class="slider-previews__list" data-item="slider-catalog-side">



            <?php if($elementArr2['PROPERTY_PLANS_HOUSE_3_VALUE']):?>
                <?php
                $renderImage = CFile::ResizeImageGet($elementArr2['PROPERTY_PLANS_HOUSE_3_VALUE'], Array("width" => 120, "height" => 80));
                ?>
                <li data-link="layout_3-slide" class="layout_1-holder" >
                    <img src="<?=$renderImage['src']?>">
                </li>
            <?php endif; ?>

            <?php if($elementArr2['PROPERTY_PLANS_HOUSE_4_VALUE']):?>
                <?php
                $renderImage = CFile::ResizeImageGet($elementArr2['PROPERTY_PLANS_HOUSE_4_VALUE'], Array("width" => 120, "height" => 80));
                ?>
                <li data-link="layout_4-slide" class="layout_2-holder" >
                    <img src="<?=$renderImage['src']?>">
                </li>
            <?php endif; ?>

            <?php if($elementArr2['PROPERTY_PLANS_HOUSE_5_VALUE']):?>
                <?php
                $renderImage = CFile::ResizeImageGet($elementArr2['PROPERTY_PLANS_HOUSE_5_VALUE'], Array("width" => 120, "height" => 80));
                ?>
                <li data-link="layout_5-slide" class="layout_1-holder" >
                    <img src="<?=$renderImage['src']?>">
                </li>
            <?php endif; ?>

            <?php if($elementArr2['PROPERTY_PLANS_HOUSE_6_VALUE']):?>
                <?php
                $renderImage = CFile::ResizeImageGet($elementArr2['PROPERTY_PLANS_HOUSE_6_VALUE'], Array("width" => 120, "height" => 80));
                ?>
                <li data-link="layout_6-slide" class="layout_2-holder" >
                    <img src="<?=$renderImage['src']?>">
                </li>
            <?php endif; ?>



        </ul>
    </div>

    <?php else:?>

   <?php echo ""; ?>

<?php endif; ?>


<?php endif; ?>
