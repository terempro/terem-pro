<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "партеры компании терем, партнеры терем");
$APPLICATION->SetPageProperty("title", "Партнеры строительной компании \"ТеремЪ\" Источник: http://terem-pro.ru/about/partnery/");
$APPLICATION->SetTitle("Партнеры строительной компании \"ТеремЪ\"");
?>
<!--<nav id="leftOffcanvasMenu" class="navmenu-fixed-left offcanvas my-offcanvas left-menu" role="navigation">
	<section class="content" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="my-navigation-forums my-padding-basic">
						<div class="my-collapse" id="my-navigation-forums-left">
							<div class="row">
								<div class="col-xs-12">
									<ul>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
									</ul>
								</div>
								<div class="col-xs-12">
									<ul>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
									</ul>
								</div>
								<div class="col-xs-12">
									<ul>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
										<li><a href="#">Производство</a></li>
									</ul>
								</div>
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</nav>-->
<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="my-text-title text-center text-uppercase desgin-h1">
									<h1>
										ПАРТНЕРЫ
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Компания «Теремъ» в своей профессиональной деятельности активно сотрудничает с разными компаниями, имеет много друзей и партнеров. Фирмы, с которыми мы работаем, заслужили наше доверие своей качественной и надежной работой.
								</p>
							
								
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-3 col-sm-3">
								<div class="img-container margin-top">
									<img src="/bitrix/templates/.default/assets/img/partners/Logo_Knauf.jpg">
								</div>
							</div>
							<div class="col-xs-12 col-md-9 col-sm-9">
								<div class="text-uppercase red">
									<h3>
										международная компания «кнауф»
									</h3>
								</div>
								<p>
									KNAUF Insulation – подразделение международной производственной группы KNAUF – один из лидеров в области производства утеплителей и признанный эксперт в их системном использовании. Дивизионы компании располагаются более чем в 35 странах. 
								</p>
								<p>
									Компания KNAUF Insulation первой в мире внедрила инновационную и уникальную технологию производства натуральных минераловатных утеплителей ECOSE®. Благодаря этой технологии впервые стало возможным использование в производстве минераловатных утеплителей связующего на основе натуральных компонентов без фенол-формальдегидныхи акриловых смол. 
								</p>
								<p> В России компания KNAUF Insulation владеет 2 крупными заводами по производству минераловатных утеплителей. Как и вся продукция компаний группы Knauf, продукция компании Knauf Insulation производится в соответствии с высокими немецкими стандартами качества. </p>
								
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-12 col-md-3 col-sm-3">
								<div class="img-container margin-top">
									<img src="/bitrix/templates/.default/assets/img/partners/megazem.jpg">
								</div>
							</div>
							<div class="col-xs-12 col-md-9 col-sm-9">
								<div class="text-uppercase red">
									<h3>
										Компания МегаЗем
									</h3>
								</div>
								<p>
								Компания МегаЗем предлагает одни из самых выгодных условий по продаже дачных участков. Компания основана в 2011 году и уже пять лет успешно развивается в своей отрасли. 
									
								</p>
								<p>
								Предложения компании отличаются прозрачной стоимостью без дополнительных взносов, инфраструктурой и коммуникациями для постоянного проживания. МегаЗем гарантирует качество сделки и юридическую безопасность.  
								</p>
                                                                <p>
                                                                   В 2016 году компания МегаЗем начала успешное сотрудничество с группой компаний «Теремъ» и ее новым, но уже известным проектом "Теремъ-Земля".
                                                                </p>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>