<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "faq, часто задоваемые вопросы");
$APPLICATION->SetPageProperty("title", "Часто задаваемые вопросы");
$APPLICATION->SetTitle("Хотелось бы остановить выбор на компании ТеремЪ - Задайте вопрос.");
?>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h1>
                                    Часто задаваемые вопросы
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                                На этой странице Вы можете найти ответы на часто задаваемые нашими клиентами вопросы. Уточнить информацию можно у наших менеджеров на выставочной площадке и по телефону.
                            </p>
                            <br>
                            <br>
                        </div>
                    </div>
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Вы занимаетесь строительством по всей России или только в Вологде?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq1">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                            <p>Мы строим дома в радиусе 700 км от города Сокол Вологодской области. Отгрузка выполняется с производственной базы, расположенной в городе Сокол Вологодской области.
											</p><p>
											Бесплатная доставка производится в радиусе 100 км от города Сокол Вологодской области бесплатно, далее рассчитывается индивидуально
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->

                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Можно ли использовать материнский капитал при строительстве дома в компании «Вологодский Терем»?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq2">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Вы можете заказать и построить у нас дом за свои средства или с использованием кредитных средств. Далее вы получаете свидетельство на дом. После этого обращаетесь в пенсионный фонд для реализации материнского капитала.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq3">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли заказать дом по индивидуальному проекту?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq3">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Квалифицированные архитекторы компании разработают проект с учётом всех ваших пожеланий. Есть возможность привязки проекта к существующему фундаменту.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="false" aria-controls="faq4">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Есть фундамент. Можно ли построить на нём дом по собственному проекту?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq4">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Квалифицированные архитекторы разработают проект с учётом всех ваших пожеланий. Есть возможность привязки проекта к существующему фундаменту.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq5" aria-expanded="false" aria-controls="faq5">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Возможен ли самостоятельный вывоз комплекта дома, материалов и/или самостоятельная сборка?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq5">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Нет, это невозможно. Мы строительная компания, которая имеет своё производство и строительные бригады. Дома мы строим сами, с использованием современных строительных технологий и качественных материалов. Мы даём гарантию на построенный дом, собранный по собственной технологии и специально обученными специалистами.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq51" aria-expanded="false" aria-controls="faq51">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Компания «Вологодский Терем» занимается инженерными коммуникациями? Входят ли в стоимость дома эти работы?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq51">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>К каждому из проектов домов компании есть план по электропроводке, установке отопительной системы и других инженерных коммуникаций.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq6" aria-expanded="false" aria-controls="faq6">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Какие в компании условия оплаты? Есть ли предоплата?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq6">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Для жителей Вологды и Вологодской области действует стандартная схема оплаты ‒ 20/60/20:</p>
											<ul>											
												<li><p>20% при заключении договора;</p></li>
												<li><p>60% после возведения фундамента;</p></li>
												<li><p>20% после окончания строительства дома, после подписания акта приёма-передачи объекта.</p></li>
											</ul>
											<p>
											Для клиентов c пропиской за пределами Вологды и Вологодской области действует иная система оплаты.
											</p>
											<p>
											Подробную информацию можно получить в выставочном доме по адресу: Вологда, Окружное шоссе, 18 (у ТЦ «Аксон») или по тел.: +7 (8172) 57-82-45.
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq7" aria-expanded="false" aria-controls="faq7">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Есть ли возможность купить дом в кредит?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq7">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Возможно. Более подробную информацию можно получить в выставочном доме по адресу: Вологда, Окружное шоссе, 18 (у ТЦ «Аксон») или по тел.: +7 (8172) 57-82-45.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq8" aria-expanded="false" aria-controls="faq8">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Выезжают ли ваши строители на участок для того, чтобы определить условия застройки и пожелания клиента на месте?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq8">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Выезжают, при заключении договора.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq9" aria-expanded="false" aria-controls="faq9">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Можно ли в домах компании «Теремъ» проживать круглогодично?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq9">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да, в домах для постоянного проживания тысячи наших заказчиков живут круглогодично. Эти дома предназначены для строительства на дачных участках и на участках для индивидуального жилищного строительства. Комплектация домов предусматривает его круглогодичное использование с использованием стационарных отопительных агрегатов (АОГВ, калориферов, печей и пр.).</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq10" aria-expanded="false" aria-controls="faq10">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Какие установлены сроки строительства домов?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq10">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Минимальный срок – несколько дней, а максимальный – почти 4 месяца. Разные дома имеют различные сроки строительства. Подробнее можно узнать на нашем сайте в разделе «Каталог домов» и по тел.: +7 (8172) 57-82-45.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq11" aria-expanded="false" aria-controls="faq11">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Что такое каркасная технология строительства домов?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq11">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Стены каркасного дома выполняются из доски 35х150 мм. Она устанавливается с шагом 500 мм (400 мм ‒ для домов для постоянного проживания). Между стойками укладывается утеплитель 150 мм (200 мм ‒ для домов для постоянного проживания). С двух сторон стены обшиваются вагонкой (дома для постоянного проживания с наружной стороны обшиваются сайдингом), с наружной стороны – по изоспану А, с внутренней стороны – по изоспану Б. Весь конструктив выполняется из сухого материала.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq12" aria-expanded="false" aria-controls="faq12">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Что входит в базовую комплектацию дома?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq12">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>: В базовую комплектацию дома (бани) входят:</p>
											<ul>
												<li><p>фундамент;</p></li>
												<li><p>весь материал для сборки дома;</p></li>
												<li><p>окна, двери, кровля, материал для внутренней отделки;</p></li>
												<li><p>доставка;</p></li>
												<li><p>работы по установке и сборке дома.</p></li>
											</ul>
											<p>
											Узнать о каждом конкретном доме более подробно можно в разделе «Каталог домов».
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq13" aria-expanded="false" aria-controls="faq13">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Какую гарантию компания «Вологодский Терем» даёт на дома?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq13">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Мы даём гарантию до 25 лет.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq14" aria-expanded="false" aria-controls="faq14">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Где находится выставочная площадка компании «Вологодский Терем»? Как к вам добраться?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq14">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Выставочный дом находится в Вологде, Окружное шоссе, 18 (у ТЦ «Аксон»).</p>
											<p>Автобусами 14, 20, 42, 48 до остановки «ТЦ «Аксон». </p>
											<p>На автомобиле: движение из центра по Ленинградской улице до Окружного шоссе и прямо на парковку к ТЦ «Аксон».
											</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
 
                </div>
            </div>
        </div>

    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
