<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "терем отзывы");
$APPLICATION->SetPageProperty("description", "Терем отзывы реальных клиентов о строительной компании и проектах");
$APPLICATION->SetPageProperty("keywords", "терем отзывы");
$APPLICATION->SetPageProperty("title", "Терем отзывы о строительной компании.");
$APPLICATION->SetTitle("Терем отзывы о строительной компании.");
?>	
<?php
if ($_GET) {
    if ($_GET['PAGEN_1'] != 1 && $_GET['PAGEN_1'] != NULL) {
        $TITLE = "Терем отзывы о строительной компании. Страница - " . $_GET['PAGEN_1'];
        $APPLICATION->SetTitle($TITLE);
        $APPLICATION->SetPageProperty("title", $TITLE);
        $str = "Страница " . (int)$_GET['PAGEN_1'];
    }
}
?>

<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">

                            <div class="my-text-title text-center text-uppercase desgin-h1">

                                <h1>
                                    Теремъ - отзывы 
                                    <?php $str?:0 ?>
                                </h1>



                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php if (!$str): ?>
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <p>
                                    Отзывы о компании Терем. На этой странице вы можете написать отзыв, задать вопросы нашим специалистам, а также ознакомиться с ответами на <a rel="nofollow" href="/about/faq/">часто задаваемые вопросы</a>.
                                </p>
                                <!-- <p>
                                    Мы открыли раздел <a rel="nofollow" href="#">«Негативные отзывы - Теремъ решает вопросы»</a>. Здесь вы можете почитать не только благодарности, а рассмотреть ситуации, которые порой возникают во время строительства или гарайтиного срока. Мы понимаем, что невозможно предотвратить все негативные ситуации - поэтому мы берем на себя отвественность на любом этапе и решаем проблемы клиентов.
                                </p> -->
                                <br>

                            </div>
                        <?php endif; ?>  
                    </div>

                    <?php
                    $APPLICATION->IncludeComponent(
	"bitrix:news", 
	"reviews_site", 
	array(
		"COMPONENT_TEMPLATE" => "reviews_site",
		"IBLOCK_TYPE" => "reviews",
		"IBLOCK_ID" => "31",
		"NEWS_COUNT" => "50",
		"USE_SEARCH" => "N",
		"USE_RSS" => "N",
		"USE_RATING" => "N",
		"USE_CATEGORIES" => "N",
		"USE_REVIEW" => "N",
		"USE_FILTER" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "DESC",
		"CHECK_DATES" => "Y",
		"SEF_MODE" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_TITLE" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"USE_PERMISSIONS" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"LIST_ACTIVE_DATE_FORMAT" => "Y-m-d",
		"LIST_FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "CITY",
			1 => "TITLE",
			2 => "PREVIEW_FULL_TEXT",
			3 => "",
		),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"DISPLAY_NAME" => "Y",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"PAGER_TEMPLATE" => "modern",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"SEF_FOLDER" => "/test/",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "#ELEMENT_CODE#/",
		)
	),
	false
);
                    ?>






                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <form class="form-review" data-toggle="validator" action="/include/hendlerOtzyv.php" method="post" role="form" data-form="send">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <p class="form-title text-center">Оставить отзыв или задать вопрос</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Имя</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="user" class="form-control" placeholder="Введите ваше имя" required/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>E-mail</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="email" name="email" class="form-control" placeholder="Введите ваш E-mail" required/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Компания</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="company" class="form-control" placeholder="Введите ваше имя" required/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Страна</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="city" class="form-control" placeholder="Страна" required/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Город</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="gorod" class="form-control" placeholder="Город" required/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Заголовок</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="title" class="form-control" placeholder="Заголовок" required/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Текст</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <textarea class="form-control" name="text" placeholder="Введите вашe сообщение" rows="5" required></textarea>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 col-sm-6 col-md-offset-6 col-sm-offset-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-7 col-md-7">
                                                    <p><small>Отправляя письмо, вы соглашаетесь</small> <a href="/privacy-policy/">Политикой обработки данных</a></p>
                                                </div>
                                                <div class="col-xs-12 col-sm-5 col-md-5">
                                                    <div class="pull-right">
                                                        <button type="submit" class="btn btn-default" value="block-1" name="from">Отправить</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>