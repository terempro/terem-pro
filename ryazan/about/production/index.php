<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "технологии производства, технологии строительства");
$APPLICATION->SetPageProperty("title", "Технологии производства и строительства домов");
$APPLICATION->SetTitle("Технологии производства и строительства домов");
?>
<section class="content prodaction text-page white" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h1 class="text-center">
									Технологии производства в компании «Теремъ»
								</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h3 class="text-center text-uppercase">
									Компания «Вологодский Терем» – официальный партнёр компании «Теремъ»  СЛЕДУЕТ ПРОСТОМУ ПРИНЦИПУ: ДЕЛАТЬ ВСЕ ЧЕТКО И ПОСЛЕДОВАТЕЛЬНО. ТАКИМ ОБРАЗОМ МЫ ОБЕСПЕЧИВАЕМ НАШЕМУ ПОТРЕБИТЕЛЮ КАЧЕСТВО ПРИ ДОСТУПНОЙ ЦЕНЕ.
								</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid reset-padding">
			<div class="row reset-margin">
				<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
					<div class="clearfix">
						<div class="row reset-margin">
							<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
								<div class="prodaction-img-container right-padding">
									<img src="/bitrix/templates/.default/assets/img/prodaction/pk1.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h2 class="text-center">
									МАТЕРИАЛЫ
								</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6 col-sm-6">
								<p>
									У производства досок есть свои особенности и нюансы. Для строительства каркасных домов нужны доски из сухой древесины I сорта. Такую древесину нужно добывать зимой, когда сокодвижение в дереве замедлено, плотность его выше, а само оно более стойко к гниению. К тому же в нем гораздо меньше влаги, чем летом - сушить проще, а значит, деформаций и нарушений геометрий на порядок меньше.
								</p>
								<p>
									Большая часть себестоимости доски - это дизельное топливо для тралов, которые вывозят лес. Но зимой у деревообрабатывающих предприятий нет выручки, чтобы закупить топливо и добыть древесину. Тут на помощь приходят крупные покупатели, такие как компания &laquo;Теремъ&raquo;. Мы даем деньги на покупку топлива и возможность нашим поставщикам спокойно делать свою работу. За это мы получаем уникальную возможность закупать материалы по очень низкой цене. 
								</p>
							</div>
							<div class="col-xs-12 col-md-6 col-sm-6">
								<p>
									К тому же, если уж совсем вдаваться в тонкости производства досок, не все части дерева подходят для этого. Подходит только малая часть, а остальное приходится либо сжигать, либо выбрасывать.
								</p>
								<p>
									Но если у предприятия стабильный и большой спрос на доски, то оно может также наладить и сбыт той части дерева, которая для досок не подходит. Так расходуется меньше материала, больше идет на продажу, и общая стоимость снижается.
								</p>
								<p>
									Таким же образом происходит и со столярным материалом. Из-за тонкостей его производства, большие заказы производить получается легче, быстрее и дешевле, что влияет на цену.
								</p>
								<p><strong>У большинства наших поставщиков мы ключевые клиенты. Поэтому можем требовать, и требуем качество поставляемых нам материалов.</strong></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container-fluid reset-padding">
			<div class="row reset-margin">
				<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
					<div class="clearfix">
						<div class="row reset-margin">
							<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
								<div class="prodaction-img-container right-padding">
									<img src="/bitrix/templates/.default/assets/img/prodaction/pk2.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h2 class="text-center text-uppercase">
									Унифицированное производство
								</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Основная проблема в производстве домов - большая номенклатура производимых деталей. Чем больше количество деталей у дома, тем сложнее его собрать. Поэтому мы ограничиваем и унифицируем их. Это дает возможность максимально упростить производство - технологический процесс отработан, не нужно лишний раз перенастраивать оборудование, повышается скорость производства, уменьшается количество отходов и брак.
								</p>
								<h4>Такая система очень полезна и строителям. У них есть подробная карта сборки, а детали все промаркированы. Это уменьшает вероятность ошибок, строительство становится дешевле, а скорость повышается в среднем на 35%.</h4>
								<p>Таким образом, низкие цены &mdash; это не маркетинговый ход, а эффективность организации производства.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container-fluid reset-padding">
			<div class="row reset-margin">
				<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
					<div class="clearfix">
						<div class="row reset-margin">
							<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
								<div class="prodaction-img-container right-padding">
									<img src="/bitrix/templates/.default/assets/img/prodaction/pk4.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h2 class="text-center text-uppercase">
									Строители
								</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Современные технологии, используемые нашей компанией, позволяют строить дома круглый год. Это значит, что у наших строителей всегда есть работа. Помимо этого, наши бригады являются постоянными. Поэтому мы с удовольствием проводим для них обучающие курсы, повышаем квалификацию и уровень мастерства. Благодаря четкой организации производства, строителям проще и удобнее работать с технической документацией и материалами. И как итог, наши бригады состоят только из профессионалов своего дела, которые качественно и быстро реализуют любой по уровню сложности проект!
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid reset-padding">
			<div class="row reset-margin">
				<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
					<div class="clearfix">
						<div class="row reset-margin">
							<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
								<div class="prodaction-img-container right-padding">
									<img src="/bitrix/templates/.default/assets/img/prodaction/pk5.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h2 class="text-center text-uppercase">
									Фундамент
								</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Планируя строительство дома очень важно грамотно подойти к выбору фундамента будущего строения, ведь он является важнейшим элементом всей постройки. Грубая ошибка в проекте или на этапе строительства может обернуться серьезными проблемами в дальнейшем. Чтобы обезопасить себя, свою семью и сам дом, необходимо обратиться в проверенную компанию, где работают опытные проектировщики.
								</p>
								<p>В компании &laquo;Вологодский Терем&raquo; – официальном партнёре компании &laquo;Теремъ&raquo; высококвалифицированные специалисты приведут аргументированные доводы за и против одного из двух видов фундамента &ndash; ленточного, свайного.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h4 class="text-center">
									Мы поможем вам сделать правильный выбор!
								</h4>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h5>
									ЛЕНТОЧНЫЙ
								</h5>
								<p>
									Такой фундамент представляет собой ленту железобетона, укладываемую под несущими стенами здания. У производства такого фундамента есть особенности: если не знать нагрузку грунта, его подвижку и массу дома, то даже если сделать мощный фундамент, нагрузки окажется недостаточно, и он станет выпирать.
								</p>
								<p>
									 Также важно знать соотношение материалов в железобетоне. Все нагрузки можно поделить на сжатие и растяжение. И если бетон очень прочен на сжатие, то главная его проблема &mdash; неэластичность. Здесь ему помогает железная арматура, которая является эластичной, но не слишком прочной на сжатие. Если рассчитать соотношение неправильно, то это просто удорожит материал, а это нецелесообразно. При правильно рассчитанном соотношении фундамент получается прочным и эластичным и обладает большей выносливостью и долговечностью.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h5>
									СВАЙНЫЙ
								</h5>
								<p>
									Прекрасная альтернатива ленточному фундаменту - свайный. Он не только дешевле и проще в установке, но и обладает весомым преимуществом - его можно закладывать даже зимой. Технология довольно проста. Из расчета нагрузки берется определенное количество свай и вкручивается в землю на глубину промерзания. Поэтому даже при движении грунта от перепада температур дом будет прочно стоять на месте и не двигаться.
								</p>
								<p>
									Внутри свай засыпается цементно-песочная смесь, которая не дает скапливаться конденсату и ржаветь железу. Такой фундамент очень долговечен и позволяет легко контролировать уклон грунта.
								</p>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<hr />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid reset-padding">
			<div class="row reset-margin">
				<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
					<div class="clearfix">
						<div class="row reset-margin">
							<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
								<div class="prodaction-img-container right-padding">
									<img src="/bitrix/templates/.default/assets/img/prodaction/pk9999.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h2 class="text-center text-uppercase">
									СТЕНЫ
								</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h4 class="text-center">
								Дом из бруса или каркасный?<br>Мы поможем разобраться!
								</h4>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h5>КАРКАСНЫЕ</h5>
								<p>
									Каркасные дома являются одним из самых популярных видов жилища во всем мире! Каркас состоит из стоек, внутри которых прокладывается в три слоя утеплитель (150 мм), оборачивается специальной технологической мембраной, которая не впускает влагу, а снаружи конструкция закрывается вагонкой. Такое сочетание дает высокую прочность и теплопроводимость. Например, утеплитель такой толщины по теплопроводимости соизмерим с 30-50 см (в 2-3 раза толще) деревянного бруса или 1,5 м кирпичной кладки (в 10 раз толще). Он является экологически чистым и делается по стандартам из расщепленного на волокна камня.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="prodaction-img-container">
									<img src="/bitrix/templates/.default/assets/img/prodaction/walls1.jpg">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Вся же конструкция очень прочна за счет обшивки снаружи вагонкой. Она закрывает утеплитель, связывает стойки, и, таким образом, создается тавровое соединение - аналогичное, только из других материалов, используется при строительстве мостов. А все потому, что такое соединение - самый прочный механизм для крепления конструкции. Оно выдерживает гигантские нагрузки!
								</p>
								<p>
									Каркасные стены можно оставлять на зиму и не беспокоиться о перепадах температур. При отоплении они теряют гораздо меньше тепла, чем брусовые. Поэтому дом можно быстрее прогреть, не боясь, что с конструкцией может что-то случиться. 
								</p>
							
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h5>Брусовые </h5>
								<p>
									- <strong>Строганный брус</strong> — это массив дерева с минимальной обработкой. Это экологически чистый, натуральный материал. У него довольно сильная усадка: при правильном строительстве и эксплуатации 3-5%, при неправильном доходит до 10%. Учитывая это, необходимо делать технические зазоры над окнами и дверьми, иначе их просто раздавит. Дома из такого бруса лучше всего строить зимой, потому что в это время он гораздо меньше деформируется, чем летом.
								</p>
								<p>
									- <strong>Клееный брус</strong> — для строительства подходит больше. Этот брус делается из высушенных до определенной влажности и проклеенных досок, которые потом спрессовывают друг с другом. Усадка и деформация при такой технологии в разы меньше, а материал получается гораздо более долговечный.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="prodaction-img-container">
									<img src="/bitrix/templates/.default/assets/img/prodaction/walls2.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<div class="container-fluid reset-padding">
			<div class="row reset-margin">
				<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
					<div class="clearfix">
						<div class="row reset-margin">
							<div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
								<div class="prodaction-img-container right-padding">
									<img src="/bitrix/templates/.default/assets/img/prodaction/Krovlya_top.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h5>МЕТАЛЛОЧЕРЕПИЦА</h5>
								<p>
									В основном мы используем металлочерепицу с собственным профилем с полимерным напылением. Напыление - специальная краска, обладающая большой прочностью и износостойкостью. Она не выгорает на солнце, у нее прекрасная ветро- и влагозащита. У такой кровли привлекательно низкая цена.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="prodaction-img-container">
									<img src="/bitrix/templates/.default/assets/img/prodaction/crovlya1.jpg">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<h5>МЯГКАЯ КРОВЛЯ</h5>
								<p>
									При желании можно поставить и мягкую кровлю. У нее лучше условия эксплуатации - она состоит из маленьких битумных лепестков, которые на солнце плавятся и превращаются в монолитную заливную поверхность, которая исключает любые протекания и не требует какого-либо последующего ухода.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="prodaction-img-container">
									<img src="/bitrix/templates/.default/assets/img/prodaction/crovlya2.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>