<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пристройки террас и веранд");
?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h1>
                                    Пристройка террас и веранд
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                               Уютная терраса или грамотно спроектированная веранда поможет придать вашему дому неповторимость и архитектурную завершённость. Пристройка к дому также поможет увеличить площадь основного строения.
							</p>
							<p>
							   Такой вариант модернизации дома является оптимальным решением для преображения и благоустройства.
							</p>
							<p>	
								Компания «Вологодский Терем» – официальный партнёр компании «Теремъ» – занимается проектированием и строительством любых видов пристроек. Сначала наши специалисты тщательно изучают конструкцию
								основного дома, начиная с фундамента и заканчивая крышей. Так формируется грамотный вариант пристройки, который подходит именно вашему дому. Каждый дом уникален, и мы, понимая это, предлагаем подходящее решение, которое обязательно учитывает все пожелания клиентов.
							</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <!--<ul class="my-list design-red">-->
                            <ul>
                                <li>
                                    <p>В качестве пристройки может выступать терраса, летняя веранда или помещение для расположения санузла, баня или сауна со всеми удобствами. </p>
                                </li>
                                <li>
                                    <p>Пристройки из дерева подходят ко всем типам домов.</p>
                                </li>
                                <li>
                                    <p>Опытные специалисты помогут Вам спроектировать и построить объект любой сложности.</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="my-service-item my-table full-width + my-margin">
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell">
                                        <a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/additional2/item-1.jpg"></a>
                                    </div>
                                </div>
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell my-appearance-top">
                                        <div class="inner text-uppercase">
                                            <h2><a href="javascript:void(0);">Террасы</a></h2>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="my-table-row">
                                    <div class="my-table-cell my-appearance-bottom">
                                        <div class="inner">
                                            <p>
                                                Открытая площадка, примыкающая к дому. Отлично подойдет для отдыха
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="my-service-item my-table full-width + my-margin">
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell">
                                        <a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/additional2/item-2.jpg"></a>
                                    </div>
                                </div>
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell my-appearance-top">
                                        <div class="inner text-uppercase">
                                            <h2><a href="javascript:void(0);">Веранды</a></h2>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="my-table-row">
                                    <div class="my-table-cell my-appearance-bottom">
                                        <div class="inner">
                                            <p>
                                                Закрытая пристройка к дому, идеально подходит для российского климата
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="my-service-item my-table full-width + my-margin">
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell">
                                        <a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/additional2/item-3.jpg"></a>
                                    </div>
                                </div>
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell my-appearance-top">
                                        <div class="inner text-uppercase">
                                            <h2><a href="javascript:void(0);">Террасы-веранды</a></h2>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="my-table-row">
                                    <div class="my-table-cell my-appearance-bottom">
                                        <div class="inner">
                                            <p>
                                                Пристройка, имеющая открытую (терраса) и закрытую (веранда) части
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                        <div class="my-table">
                                            <div>
                                                <p>Компания «Теремъ» предлагает широкий выбор современный материалов и технологий:</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq1">
                                        <div class="( my-collapse-content + design-main )">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Внутренняя отделка</h4>
                                                    <ul class="my-list design-additional">
                                                        <li>
                                                            <p>Вагонка</p>
                                                        </li>
                                                        <li>
                                                            <p>Гипсокартон</p>
                                                        </li>
                                                        <li>
                                                            <p>Ламинат</p>
                                                        </li>
                                                        <li>
                                                            <p>Настил деревянного<br>пола</p>
                                                        </li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Кровля</h4>
                                                    <ul class="my-list design-additional">
                                                        <li>
                                                            <p>Мягкая кровля</p>
                                                        </li>
                                                        <li>
                                                            <p>Металлочерепица</p>
                                                            <ul>
                                                                <li>
                                                                    <p>«Монтеррей»</p>
                                                                </li>
                                                                <li>
                                                                    <p>«Каскад»</p>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <p>Оцинкованный профнастил</p>
                                                        </li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Установка окон</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Пластиковые</p></li>
                                                        <li><p>Ламинированные</p></li>
                                                        <li><p>Деревянные</p></li>
                                                        <li><p>Деревянные стеклопакеты</p></li>
                                                        <li><p>Рольставни</p></li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Стены</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Каркас 150 мм</p></li>
                                                        <li><p>Каркас 200 мм</p></li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Утепление</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Минеральная вата KNAUF</p></li>
                                                        <li><p>Утеплитель Rockwool</p></li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Установка дверей</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Межкомнатные</p></li>
                                                        <li><p>Входные</p></li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Наружная отделка</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Сайдинг</p></li>
                                                        <li><p>Вагонка</p></li>
                                                        <li><p>Фасадные панели</p></li>
                                                        <li><p>Цокольный сайдинг</p></li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Фундамент</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Свайный</p></li>
                                                        <li><p>Ленточный</p></li>
                                                        <li><p>Столбчатый</p></li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Наличники и плинтуса</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Деревянные</p></li>
                                                        <li><p>Пластиковые</p></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>