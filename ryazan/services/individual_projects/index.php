<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Проекты индивиудальных жилых домов: планировки, стоимость и характеристики");
$APPLICATION->SetPageProperty("keywords", "индивидуальное строительство проекты домов,индивиудальные проекты");
$APPLICATION->SetPageProperty("title", "Индивидуальное строительство - проекты домов и коттеджей");
$APPLICATION->SetTitle("Индивидуальное строительство - проекты домов и коттеджей");
?>
<?php $page = $APPLICATION->GetCurPage(); ?>
<?php if($page == '/services/individual_projects/'):?>

<section class="content white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix + my-margin">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>Архитектурное бюро</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row idividual-text">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p> <big>Вы можете создать собственный проект дома из бруса, каркаса или кирпича. Проектирование будет осуществляться с учетом всех Ваших пожеланий. Для внутренней и наружной отделки мы используем самые разные материалы. </big></p>
                            <ul>
                                <li>
                                    <p>Деревянная обшивка (вагонка, имитация бруса, блокхаус)</p>
                                </li>
                                <li>
                                    <p>Виниловая обшивка (различные виды сайдинга, фасадные панели VinyTherm)</p>
                                </li>
                                <li>
                                    <p>Обшивка гипсокартоном (производится внутри помещений по желанию заказчика)</p>
                                </li>
                                <li>
                                    <p>Наружная обшивка термопанелями с поверхностью из природного камня или кирпича различных цветов</p>
                                </li>
                                <li>
                                    <p>Кровельные материалы (металлочерепица, мягкая кровля различных цветов)</p>
                                </li>
                            </ul>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <img src="/bitrix/templates/.default/assets/img/expert/sep2.png?14501059151998" alt="arrow-left" class="full-width">
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 text-center col-sm-offset-1 col-md-offset-1">
                                    <p style="font-size:21px; color:#be2a23;line-height: 1.2;margin-bottom:0;">Наши архитекторы воплотят в жизнь любые Ваши идеи по проектированию дома и обустройству участка. Мы создадим для Вас настоящий дом мечты!</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <img src="/bitrix/templates/.default/assets/img/expert/sep1.png?14501059152023" alt="arrow-right" class="full-width">
                                </div>
                            </div>
                            <br>
                            <p><big>Мы тщательно просчитываем все, начиная от выбора фундамента, материалов, размера, планировки, заканчивая инженерными коммуникациями и благоустройством участка.</big></p>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                    <button data-target="#call" data-toggle="modal" class="btn btn-default btn-call-from-buro"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную консультацию</span></button>
                                </div>
                                <div class="col-xs-12 col-md-6 col-sm-6 text-right">
                                    <button onclick="location.href = '/offer-project/';" type="button" class="btn btn-default btn-call-from-buro"><i class="glyphicon glyphicon-upload"></i><span>Выслать фото для расчета</span></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="content" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12" id="category">
                <div class="flex-reversed">
                    <!--OLD catalog list-->
                    <!--small villages -->
                    <div class="row">
                        <!-- div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="/services/individual_projects/projects.php?SECTION=1">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-1.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        БАНИ
                                    </h4>
                                    <p>
                                        Баня по индивидуальному проекту? Не мечта, а реальность. Бильярдная или тренажёрный зал, комнаты отдыха и барбекю-зона на просторной террасе – всё это и многое другое можно включить в проект Вашей уникальной бани. Обратите внимание на готовые проекты, представленные в этом разделе.
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/individual_projects/projects.php?SECTION=1"></a>
                            </div>
                        </div -->						
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="/services/individual_projects/projects.php?SECTION=5">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-2.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ДОМА ИЗ БРУСА С ВЫПУСКАМИ

                                    </h4>
                                    <p>
                                        Наши архитекторы создают интересные проекты домов из бруса с выпусками. Такие дома смотрятся очень колоритно и презентабельно за счёт интересной технологии возведения, при которой концы бруса определённой длины выступают за плоскость стен.
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/individual_projects/projects.php?SECTION=5"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="/offer-project/">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-6.jpg" alt="Оригинальные дачные дома">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ПОСТРОИМ ДОМ<br> <span>ПО ВАШЕМУ ПРОЕКТУ</span>

                                    </h4>
                                    <p>
                                        Если вы точно знаете, как должен выглядеть ваш дом -  мы знаем, как его построить по приемлемой цене. Для этого достаточно выслать нам картинку планировки, которая вам нравится, и мы скажем сколько стоит мечта.

                                    </p>
                                </div>
                                <a class="all-item-link" href="/offer-project/"></a>
                            </div>
                        </div>						
                    </div>




                    <!--end-->
                    <!--vitaz and gardern-->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="/services/individual_projects/projects.php?SECTION=2">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-3.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ПРОЕКТИРОВАНИЕ ДОМОВ <span>ДО 100 КВ.М.</span>

                                    </h4>
                                    <p>
                                        Проекты домов площадью до 100 кв. м порадуют инновационным подходом к планированию жилого пространства и оригинальным взглядом на архитектуру современного загородного особняка. Ознакомьтесь с нашими уникальными моделями домов по индивидуальным проектам.
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/individual_projects/projects.php?SECTION=2"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="/services/individual_projects/projects.php?SECTION=3">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-4.jpg" alt="Оригинальные дачные дома">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ПРОЕКТИРОВАНИЕ ДОМОВ <span>ДО 150 КВ.М.</span>

                                    </h4>
                                    <p>
                                        Большой и красивый дом по индивидуальному проекту – залог по-настоящему комфортной жизни за городом. Дом до 150 кв м. по индивидуальному проекту станет самым любимым и желанным местом отдыха для всей семьи. Сделайте выбор в пользу уюта и неповторимого качества
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/individual_projects/projects.php?SECTION=3"></a>
                            </div>
                        </div>
                    </div><!--end-->
                    <!--vitaz and gardern-->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="/services/individual_projects/projects.php?SECTION=4">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-7.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ЭКСКЛЮЗИВНЫЕ ПРОЕКТЫ ПОД КЛЮЧ <span><br>ОТ 150 КВ.М.</span>
                                    </h4>
                                    <p>
                                        Огромный дом по индивидуальному проекту – идеальный выбор для отдыха и семейной жизни за городом круглый год. Наши проекты порадуют даже самых искушённых приверженцев красивой жизни.
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/individual_projects/projects.php?SECTION=4"></a>
                            </div>
                        </div>
                    </div><!--end-->
                    <!--post projivanie-->
                    <!-- <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-promotion ( desgin-2 &amp;&amp; one-column ) + my-margin">
                                <div>
                                    <a href="/services/individual_projects/projects.php?SECTION=4">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-5.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ЭКСКЛЮЗИВНЫЕ ПРОЕКТЫ ПОД КЛЮЧ <span><br>ОТ 150 КВ.М.</span>

                                    </h4>
                                    <p>
                                         Огромный дом по индивидуальному проекту – идеальный выбор для отдыха и семейной жизни за городом круглый год. Наши проекты порадуют даже самых искушённых приверженцев красивой жизни.
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/individual_projects/projects.php?SECTION=4"></a>
                            </div>
                        </div>
                    </div> --><!--end-->

                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"individual_project",
	array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "ID",
			2 => "CODE",
			3 => "XML_ID",
			4 => "NAME",
			5 => "TAGS",
			6 => "SORT",
			7 => "PREVIEW_TEXT",
			8 => "PREVIEW_PICTURE",
			9 => "DETAIL_TEXT",
			10 => "DETAIL_PICTURE",
			11 => "DATE_ACTIVE_FROM",
			12 => "ACTIVE_FROM",
			13 => "DATE_ACTIVE_TO",
			14 => "ACTIVE_TO",
			15 => "SHOW_COUNTER",
			16 => "SHOW_COUNTER_START",
			17 => "IBLOCK_TYPE_ID",
			18 => "IBLOCK_ID",
			19 => "IBLOCK_CODE",
			20 => "IBLOCK_NAME",
			21 => "IBLOCK_EXTERNAL_ID",
			22 => "DATE_CREATE",
			23 => "CREATED_BY",
			24 => "CREATED_USER_NAME",
			25 => "TIMESTAMP_X",
			26 => "MODIFIED_BY",
			27 => "USER_NAME",
			28 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "S_3",
			2 => "CATEGORY",
			3 => "S_2",
			4 => "S_1",
			5 => "HOUSES",
			6 => "SUMM",
			7 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "27",
		"IBLOCK_TYPE" => "individualhouses",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "S_3",
			1 => "CATEGORY",
			2 => "S_2",
			3 => "S_1",
			4 => "HOUSES",
			5 => "SUMM",
			6 => "",
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_FOLDER" => "/services/individual_projects/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "individual_project",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "#ELEMENT_CODE#/",
		)
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
