<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "реконструкция домов, достройка домов, пристройка террас");
$APPLICATION->SetPageProperty("title", "Услуги по возведению и реконструкции малоэтажных загородных домов");
$APPLICATION->SetTitle("Услуги по возведению и реконструкции малоэтажных загородных домов ");
?>
<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="text-center text-uppercase">
									<h1>
										СТРОИТЕЛЬСТВО И РЕКОНСТРУКЦИЯ ЗАГОРОДНЫХ ДОМОВ
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Наши специалисты работают на всех этапах строительства, начиная от разработки проекта и монтажа фундамента, заканчивая наружной и внутренней отделкой.
								</p>
								<p>
									«Теремъ» заботится о том, чтобы процесс строительства прошел как можно более комфортно для наших клиентов. Мы используем современные технологии, которые позволяют максимально сократить срок строительства, сохранив высокое качество и индивидуальный подход к каждому проекту.
								</p>
								<br>
								<br>
							</div>
						</div>
						
						<div class="row my-flex">
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div class="my-table-row">
										<div>
											<a href="/ryazan/offer-project/"><img src="/bitrix/templates/.default/assets/img/services/item-1.jpg"></a>
										</div>
									</div>
									<div >
										
											<div class="inner text-uppercase">
												<h2><a href="/ryazan/services/dostroika/">Архитектурное бюро</a></h2>
											</div>
											<div class="inner">
												
												<p>
													Мы сделаем Ваш дом таким, каким хотите его видеть Вы
												</p>
											</div>
									</div>
									
								</div>
							</div>
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/ryazan/services/enginering_communications/"><img src="/bitrix/templates/.default/assets/img/services/item-2.jpg"></a>
									
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/ryazan/services/enginering_communications/">Инженерные системы</a></h2>
											</div>
										
											<div class="inner">
											
												<p>
													Наши специалисты сделают Ваш дом современным и уютным
												</p>
											</div>
									</div>
									
								</div>
							</div>
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/ryazan/services/addition2/"><img src="/bitrix/templates/.default/assets/img/services/item-6.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/ryazan/services/addition2/">Пристройки</a></h2>
											</div>
										<div class="inner">
											
												<p>
													Поможет увеличить площадь и придать дому архитектурную завершенность
												</p>
											</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="row my-flex">
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/ryazan/services/the-estates-catalogue/"><img src="/bitrix/templates/.default/assets/img/services/item-4.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/ryazan/services/the-estates-catalogue/">Благоустройство<br>участка</a></h2>
											</div>
										<div class="inner">
											
												<p>
													Решение для тех, кто хочет жить в доме своей мечты
												</p>
											</div>
									</div>
									
								</div>
							</div>
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/ryazan/services/enginering_communications/fensecalc/"><img src="/upload/zabori.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/ryazan/services/enginering_communications/fensecalc/">Заборы</a></h2>
											</div>
											<div class="inner">
												<p>
													Неприкосновенность частной жизни и личная безопасность – вот то, к чему стремится каждый человек в наше время.
												</p>
											</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									
										<div>
											<a href="/ryazan/services/credit-calculator/"><img src="/bitrix/templates/.default/assets/img/services/item-7.jpg"></a>
										</div>
									
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/ryazan/services/credit-calculator/">Услуги кредитования и страхования</a></h2>
											</div>
											<div class="inner">
											
												<p>
													Взять кредит или застраховать дом можно на выставке «Теремъ»
												</p>
											</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<br>
								<br>
								<p>
									Работа над каждым домом начинается с проекта. Здесь очень важно обратиться в компанию, которая имеет большой опыт работы как в строительстве, так и в монтаже инженерных систем. Наши специалисты имеют высокий уровень квалификации, их грамотный и рациональный подход поможет все тщательно спланировать и просчитать. Они помогут Вам лучше осознать свои желания и в конечном итоге переехать жить в дом своей мечты!
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>