<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;


class ParserController extends Controller
{
    public function index(){

        $forum_page = "https://www.forumhouse.ru/threads/342814/";

        $html = file_get_contents($forum_page);

        $crawler = new Crawler($html);

        $last_page = $crawler->filter('div.PageNav')->attr('data-last');
        $last_page_url = "{$forum_page}page-{$last_page}";

        $html = file_get_contents($last_page_url);
        $crawler = new Crawler($html);


        $last_msg_id = $crawler->filter('div.publicControls > a')->last()->text();

        $last_msg_id = (int)str_replace("#","",$last_msg_id);

        $txt_counter_msg = (int)file_get_contents(public_path('last_msg_forumhouse.txt'));

        //dd($txt_counter_msg);

        if($txt_counter_msg <  $last_msg_id){
            file_put_contents(public_path('last_msg_forumhouse.txt'), '');
            file_put_contents(public_path('last_msg_forumhouse.txt'), $last_msg_id);
        }
        else{
            echo "OK!";
        }
    }
}
