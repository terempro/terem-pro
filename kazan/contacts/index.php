<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты компании Терем в Казани");
$APPLICATION->SetPageProperty("keywords", "терем контакты, терем телефон, терем про почта");
$APPLICATION->SetTitle("Контакты компании Терем в Казани");
// $APPLICATION->AddHeadScript("/bitrix/templates/.default/assets/js/map.js");
$code=$APPLICATION->CaptchaGetCode();

$request_failed = isset($_SESSION["CLAIM_FAILED"]);
$double_request = isset($_SESSION["CLAIM_SENT"]) && ($_SESSION["CLAIM_SENT"] == true);

?><script type="text/javascript" src="/bitrix/templates/.default/assets/js/map.js"></script>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-sm-8">
                            <div class="text-uppercase">
                                <h3 class="contact-title">
                                    ОФИЦИАЛЬНЫЙ ПАРТНЕР В КАЗАНИ
                                </h3>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="text-right">
                                <h3 class="title-contanct-3">
                                    <span class="red"><a href="mailto:info.kazan@terem-pro.ru">info.kazan@terem-pro.ru</a></span>
                                </h3>
                                <h2 class="title-contanct-2">
                                    <span class="red"><a href="#">+7 (843) 20-70-348</a></span><!--id="telMain3"-->
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <video class="VideoContact" style="position: relative;display: block;  margin: 0px -50px 0px; width: calc(100% + 100px);" controls="" itemprop="url">
                                <source src="/upload/contact.mp4" type="video/mp4">
                                <source src="/upload/contact.webm" type="video/webm">
                                <source src="/upload/contact.flv" type="video/flv">
                            Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="catalog-item-gallery white + my-margin">
                    <div class="white my-padding-50 clearfix">
                        <div class="contacts-toggle-btns">
                            <ul>
                                <li class="active">
                                    <a href="#1a" data-toggle="tab" class="btn btn-danger contact-btn text-uppercase">
                                        Казань
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="row tab-content clearfix">
                            <div class="row-eq-height tab-pane active" id="1a">
                                <div class="col-xs-12 col-md-4 flex-column">
                                    <div>
                                        <p><strong>Телефон:</strong> +7 (843) 20-70-348. Приём звонков с 8.00 до 20.00 ПН-ВС</p>
                                    </div>
                                    <div>
                                        <p><strong>Адрес:</strong> РТ, г. Казань, ул. Аделя Кутуя, д. 116, каб. №25 (остановка "Завод ЖБИ-3")</p>
                                    </div>

                                    <div>
                                        <button type="button" data-target="#call" data-toggle="modal" class="btn btn-danger contact-btn text-uppercase"><span class="hidden-sm visible-xs visible-md visible-lg">заявка на бесплатную консультацию</span><span class="visible-sm hidden-xs">консультация</span></button>
                                    </div>
                                    <div class="my-margin">
                                      <p>
                                        Остерегайтесь подделок! Только официальные партнеры компании «Теремъ» обеспечивают надежное строительство и предоставляют гарантию на все виды работ. Актуальный список партнёров опубликован на главном сайте компании <a href="https://www.terem-pro.ru">www.terem-pro.ru</a>, уточнить информацию можно по телефону.
                                      </p>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-8">
                                    <div id="map2" class="ymap"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row welcome-text" style="margin-top: 50px;">
                            <p class="text-center" style="font-size: 1em;">
                                <img src="/bitrix/templates/.default/assets/img/warning-sign.png" alt="Внимание!" class="warning-sign">
                                <strong>
                                    Строительная компания «Теремъ» приглашает партнеров из России и стран СНГ к сотрудничеству: <a href="mailto:partner@terem-pro.ru" class="my-blue-link">partner@terem-pro.ru</a>
                                </strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content text-page white" role="content" id="map-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="catalog-item-gallery my-padding-50 white mb-5">
                    <div class="tab-content">
                        <div id="zoom-map" style="overflow:hidden;position:relative;">
                            <img src="myMap.jpg" class="full-width" alt="">
                            <div class="btn-zooms">
                                <button class="btn btn-zoom-in"><i class="glyphicon glyphicon-plus"></i></button>
                                <button class="btn btn-zoom-out"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <ul class="myMap-items">
                                    <li>
                                        <span class="color-1"></span><a href="#">ДОГОВОРНЫЙ ОТДЕЛ, ОФИС</a>
                                    </li>
                                    <li>
                                        <span class="color-2"></span><a href="#">ТЕХНОЛОГИЧЕСКИЙ ОТДЕЛ</a>
                                    </li>
                                    <li>
                                        <span class="color-3"></span><a href="#">ДОСТРОЙКА И РЕКОНСТРУКЦИЯ</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <ul class="myMap-items">
                                    <li>
                                        <span class="color-4"></span><a href="#">КЛИЕНТСКАЯ СЛУЖБА</a>
                                    </li>
                                    <li>
                                        <span class="color-5"></span><a href="#">ОТДЕЛ РЕКЛАМАЦИИ</a>
                                    </li>
                                    <li>
                                        <span class="color-6"></span><a href="#">ИНДИВИДУАЛЬНОЕ СТРОИТЕЛЬСТВО</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <ul class="myMap-items">
                                    <li>
                                        <span class="color-7"></span><a href="#">ИНЖЕНЕРНЫЕ КОММУНИКАЦИИ</a>
                                    </li>
                                    <li>
                                        <span class="color-8"></span><a href="#">ЗЕМЕЛЬНЫЕ УЧАСТКИ</a>
                                    </li>
                                    <li>
                                        <span class="color-9"></span><a href="#">КРЕДИТНО-СТРАХОВОЙ ОТДЕЛ</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <noindex rel="nofollow">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <ul class="number-list">
                                        <li>
                                            1.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/terem-lk/">Терем ЛК, 6х9 м</a>
                                        </li>
                                        <li>
                                            2.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/bogatyr-2/">Богатырь 2, 7х8 м</a>
                                        </li>
                                        <li>
                                            3.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/terem-1/">Терем К, 6х9 м</a>
                                        </li>
                                        <li>
                                            4.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/varyag-3/">Варяг 3, 8х10 м</a>
                                        </li>
                                        <li>
                                            4а.<a href="/catalog/malye-stroeniya-besedki/besedka-barbekyu/">Беседка, 6х4 м</a>
                                        </li>
                                        <li>
                                            4б.<a href="/catalog/malye-stroeniya-besedki/besedkaа/">Беседка, 3х3 м</a>
                                        </li>
                                        <li>
                                            5.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/viking-d-11/">Викинг 11, 8х9.5 м</a>
                                        </li>
                                        <li>
                                            6.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/kantsler-2/">Канцлер 2, 8х9 м</a>
                                        </li>
                                        <li>
                                            7.<a href="/catalog/besedki-bani-i-sadovye-doma/banya4х4/">Баня, 4х4 м</a>
                                        </li>
                                        <li>
                                            8.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/lider-8k/">Лидер 8, 6х8 м</a>
                                        </li>
                                        <li>
                                            9.<a href="#">Административное здание</a>
                                        </li>
                                        <li>
                                            10.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/svyatogor-k/">Святогор, 7х10 м</a>
                                        </li>
                                        <li>
                                            11.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/viking-d-2/">Викинг 2, 7х9.5 м</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <ul class="number-list">
                                        <li>
                                            12.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/bogatyr-lk-1k/">Богатырь 1, 7х7 м</a>
                                        </li>
                                        <li>
                                            13.<a href="/catalog/sadovye-doma/malysh5k6х5/">Малыш 5, 6х5 м</a>
                                        </li>
                                        <li>
                                            14.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/dobrynya-3/">Добрыня 3, 8.5х10 м</a>
                                        </li>
                                        <li>
                                            15.<a href="/catalog/sadovye-doma/malysh3k5х4/">Малыш 3, 5х4 м</a>
                                        </li>
                                        <li>
                                            16.<a href="/catalog/klassicheskie-dachnye-doma/lider-6/">Лидер 6, 6х6 м</a>
                                        </li>
                                        <li>
                                            17.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/viking-d-4/">Викинг 4, 8.5х9.5 м</a>
                                        </li>
                                        <li>
                                            18.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/imperator-2/">Император 2, 8.5х9 м</a>
                                        </li>
                                        <li>
                                           18а.<a href="/catalog/malye-stroeniya-besedki/besedka+a/">Беседка 4х4 м</a>
                                       </li>
                                        <li>
                                            19.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/doma-ot-150-do-400-kv-m/general-k-200/">Генерал, 14х20.5 м</a>
                                        </li>
                                        <li>
                                            20.<a href="/catalog/originalnye-dachnye-doma-vityaz-odnoetazhnye-i-so-vtorym-svetom/vityaz-4/">Лада 2, 6х9 м</a>
                                        </li>
                                        <li>
                                            21.<a href="#">Административное здание</a>
                                        </li>
                                        <li>
                                            22.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/varyag-d-1/">Варяг 1, 8х8 м</a>
                                        </li>

                                    </ul>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <ul class="number-list">
                                        <!-- <li>
                                            <span>23а.</span><a href="/catalog/malye-stroeniya-besedki/besedka4/">Беседка 3.8х4.8 м</a>
                                        </li> -->
                                        <li>
                                            23.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/bogatyr-lk-3k/">Богатырь 3 ЛК, 7х9 м</a>
                                        </li>
                                        <li>
                                            24.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/premer-d-1/">Премьер 1, 7.5х7.5 м</a>
                                        </li>
                                        <li>
                                            25.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/kantsler-d-1/">Канцлер 1, 8х7.5 м</a>
                                        </li>
                                        <li>
                                            26.<a href="/catalog/besedki-bani-i-sadovye-doma/banya-ladaК6х8/">Баня “Лада”, 6х8 м</a>
                                        </li>
                                        <li>
                                            27.<a href="/catalog/kottedzhi-dlya-sezonnogo-prozhivaniya/kantsler-d-3/">Канцлер 3, 10х10 м</a>
                                        </li>
                                        <li>
                                            27а.<a href="/catalog/malye-stroeniya-besedki/besedka-barbekyo/">Беседка 4х4 м</a>
                                        </li>
                                        <li>
                                            28.<a href="/catalog/doma-dlya-postoyannogo-prozhivaniya/gertsog/">Герцог, 8.5х9.5 м</a>
                                        </li>
                                        <li>
                                            29.<a href="/catalog/besedki-bani-i-sadovye-doma/banya3х4/">Баня, 3х4 м</a>
                                        </li>
                                        <li>
                                            30.<a href="/catalog/besedki-bani-i-sadovye-doma/banya6х5СМ/">Баня СМ, 6х5 м</a>
                                        </li>
                                        <li>
                                            31.<a href="/catalog/originalnye-dachnye-doma-vityaz-odnoetazhnye-i-so-vtorym-svetom/vityaz-2/">Витязь 2, 6х8 м</a>
                                        </li>
                                        <li>
                                            32.<a href="/catalog/besedki-bani-i-sadovye-doma/banya6х6/">Баня, 6х6 м</a>
                                        </li>
                                        <li>
                                            33.<a href="/catalog/besedki-bani-i-sadovye-doma/banya5х5/">Баня, 5х5 м</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </noindex>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content white catalog-item-1 content-catalog-item active overflow-v contact-forms" role="content" data-item="1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="catalog-item-gallery white my-padding-50">
                    <div class="tab-panel ( my-tabpanel &amp;&amp; ( design-1 &amp;&amp; appearance-list-center &amp;&amp; bg-grey &amp;&amp; padding-20 ))">
                        <!-- Tab panes -->
                        <div class="contacts-item">
                            <div class="" id="contact3">
                                <h1 class="my-title my-title--extra-gutter">Обратная связь</h1>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading1">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main )" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Получить бесплатную консультацию</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Мы можем позвонить вам абсолютно бесплатно! Пожалуйста, укажите свое имя, номер телефона и предпочтительное время звонка. Наши специалисты ответят на любые возникшие вопросы!</p>
                                                            <form id="crmRequest" data-formtype="contact-form" class="form-send" action="/kazan/include/HendlerContact.php" data-toggle="validator" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="tel" name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <select class="form-control my-input" name="time" required="">
                                                                                        <option value="" disabled="" selected="" title="Удобное время звонка">Удобное время звонка</option>
                                                                                        <option value="10-00">10-00</option>
                                                                                        <option value="11-00">11-00</option>
                                                                                        <option value="12-00">12-00</option>
                                                                                        <option value="13-00">13-00</option>
                                                                                        <option value="14-00">14-00</option>
                                                                                        <option value="15-00">15-00</option>
                                                                                        <option value="16-00">16-00</option>
                                                                                        <option value="17-00">17-00</option>
                                                                                        <option value="18-00">18-00</option>
                                                                                        <option value="19-00">19-00</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="email" name="mail" class="form-control my-input" placeholder="Ваш email" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                        <? if($double_request): ?>
                                                                                Спасибо! Ваше обращение зарегистрировано.<br>
                                                                                Мы обязательно свяжемся с Вами в ближайшее время!
                                                                        <? endif; ?>
                                                                        <? if($request_failed): ?>
                                                                                Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                                Доступ к сервису временно заблокирован!
                                                                        <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на телефон):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" type="text" name="confirmation-code" class="form-control" autofocus>
                                                                        </div>
                                                                        <input type="hidden" value="f0" name="type">
                                                                        <input type="hidden" name="city" value="<?= $CURRENT_CITY ?>">
                                                                        <button type="submit" name="btn" value="f0" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading2">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Задать вопрос директору</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Здесь вы можете задать интересующий вас вопрос генеральному директору компании «Теремъ»</p>
                                                            <form data-formtype="contact-form" action="/kazan/include/HendlerContact.php" data-toggle="validator" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="tel" name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input type="text" name="captcha_word" value="" placeholder="Введите символы с картинки" />
                                                                                        <input type="hidden" name="captcha_sid" value="<?=$code;?>" />
                                                                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$code;?>" alt="CAPTCHA" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" required="" placeholder="Ваш вопрос..."></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                        <? if($double_request): ?>
                                                                                Спасибо! Ваше обращение зарегистрировано.<br>
                                                                                Мы обязательно свяжемся с Вами в ближайшее время!
                                                                        <? endif; ?>
                                                                        <? if($request_failed): ?>
                                                                                Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                                Доступ к сервису временно заблокирован!
                                                                        <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на телефон):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" type="text" name="confirmation-code" class="form-control" autofocus>
                                                                        </div>
                                                                        <input type="hidden" value="f1" name="type"><br>
                                                                        <button type="submit" name="btn" value="f1" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading3">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Пожаловаться на бригаду</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Если у Вас возникли объективные претензии к работе строительной бригады, Вы можете изложить из для скорейшего устранения причины. Все обращения будут рассмотрены в обязательном порядке. Просим обязательно указать номер договора для оперативной обработки данных.</p>
                                                           <form data-formtype="contact-form" action="/kazan/include/HendlerContact.php" data-toggle="validator" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="tel" name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input type="text" name="captcha_word" value="" placeholder="Введите символы с картинки" />
                                                                                        <input type="hidden" name="captcha_sid" value="<?=$code;?>" />
                                                                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$code;?>" alt="CAPTCHA" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="text" name="number" class="form-control my-input" placeholder="Номер бригады" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input min" placeholder="Суть претензии..." required=""></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!-- <div id="captcha2" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                        <? if($double_request): ?>
                                                                                Спасибо! Ваше обращение зарегистрировано.<br>
                                                                                Мы обязательно свяжемся с Вами в ближайшее время!
                                                                        <? endif; ?>
                                                                        <? if($request_failed): ?>
                                                                                Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                                Доступ к сервису временно заблокирован!
                                                                        <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на телефон):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" type="text" name="confirmation-code" class="form-control" autofocus>
                                                                        </div>
                                                                        <input type="hidden" value="f2" name="type"><br>
                                                                        <button type="submit" name="btn2" value="f2" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading4">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Отблагодарить бригаду</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Здесь вы можете отблагодарить бригаду, которая строила Вам дом, за добросовестный труд и высокое качество работы.</p>
                                                            <form data-formtype="contact-form" action="/kazan/include/HendlerContact.php" data-toggle="validator" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="email" name="mail" class="form-control my-input" placeholder="Ваш email" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input type="text" name="captcha_word" value="" placeholder="Введите символы с картинки" />
                                                                                        <input type="hidden" name="captcha_sid" value="<?=$code;?>" />
                                                                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$code;?>" alt="CAPTCHA" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" placeholder="Текст..." required=""></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!-- <div id="captcha3" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                        <? if($double_request): ?>
                                                                                Спасибо! Ваше обращение зарегистрировано.<br>
                                                                                Мы обязательно свяжемся с Вами в ближайшее время!
                                                                        <? endif; ?>
                                                                        <? if($request_failed): ?>
                                                                                Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                                Доступ к сервису временно заблокирован!
                                                                        <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на почту):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" type="text" name="confirmation-code" class="form-control" autofocus>
                                                                        </div>
                                                                        <input type="hidden" value="f3" name="type"><br>
                                                                        <button type="submit" name="btn3" value="f3" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading5">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Обратиться в клиентскую службу</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>По всем вопросам, возникшим во время строительства дома в нашей компании, Вы можете обратиться в клиентскую службу через представленную ниже форму.</p>
                                                            <form data-formtype="contact-form" action="/kazan/include/HendlerContact.php" data-toggle="validator" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="email" name="mail" class="form-control my-input" placeholder="Ваш email" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input type="text" name="captcha_word" value="" placeholder="Введите символы с картинки" />
                                                                                        <input type="hidden" name="captcha_sid" value="<?=$code;?>" />
                                                                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$code;?>" alt="CAPTCHA" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" placeholder="Вопрос..." required=""></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!--  <div class="g-recaptcha" id="captcha4" data-sitekey="6LeyiQwUAAAAACJ9_CrHgc9lJpXySHFwEdjjJkgy" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                        <? if($double_request): ?>
                                                                                Спасибо! Ваше обращение зарегистрировано.<br>
                                                                                Мы обязательно свяжемся с Вами в ближайшее время!
                                                                        <? endif; ?>
                                                                        <? if($request_failed): ?>
                                                                                Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                                Доступ к сервису временно заблокирован!
                                                                        <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на почту):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" type="text" name="confirmation-code" class="form-control" autofocus>
                                                                        </div>
                                                                        <input type="hidden" value="f4" name="type"><br>
                                                                        <button type="submit" name="btn4" value="f4" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading6">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Обратиться в рекламный отдел</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Обращения по вопросам сотрудничества и рекламы.</p>
                                                            <form data-formtype="contact-form" action="/kazan/include/HendlerContact.php" data-toggle="validator" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="email" name="mail" class="form-control my-input" placeholder="Ваш email" required="">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input type="text" name="captcha_word" value="" placeholder="Введите символы с картинки" />
                                                                                        <input type="hidden" name="captcha_sid" value="<?=$code;?>" />
                                                                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$code;?>" alt="CAPTCHA" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" placeholder="Текст..." required=""></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!-- <div class="g-recaptcha" id="captcha5" data-sitekey="6LeyiQwUAAAAACJ9_CrHgc9lJpXySHFwEdjjJkgy" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                        <? if($double_request): ?>
                                                                                Спасибо! Ваше обращение зарегистрировано.<br>
                                                                                Мы обязательно свяжемся с Вами в ближайшее время!
                                                                        <? endif; ?>
                                                                        <? if($request_failed): ?>
                                                                                Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                                Доступ к сервису временно заблокирован!
                                                                        <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на почту):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" type="text" name="confirmation-code" class="form-control" autofocus>
                                                                        </div>
                                                                        <input type="hidden" value="f5" name="type"><br>
                                                                        <button type="submit" name="btn5" value="f5" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>