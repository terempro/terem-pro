<?php
const REQUESTS_IBLOCK = 59;
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once 'dompdf/lib/html5lib/Parser.php';
require_once 'dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'dompdf/lib/php-svg-lib/src/autoload.php';
require_once 'dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();

use Dompdf\Dompdf;
use Dompdf\Options;

/*// ROISTAT BEGIN
$roistatData['roistat'] = isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null;
$roistatData['form'] = 'Получить сертификат на сигнализацию';
$roistatData['name'] = isset($_POST['name']) ? $_POST['name'] : null;
$roistatData['phone'] = isset($_POST['phone']) ? $_POST['phone'] : null;

if (!empty($roistatData['phone'])) 
{
    $connection = curl_init();
    curl_setopt($connection, CURLOPT_URL, 'http://xn----itba2alfdel.su');
    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($connection, CURLOPT_POST, true);
    curl_setopt($connection, CURLOPT_POSTFIELDS, $roistatData);
    $res = curl_exec($connection);
    curl_close($connection);
}
// ROISTAT END*/

$name = htmlspecialchars(trim($_POST['name']));
$phone = htmlspecialchars(trim($_POST['phone']));

if (!isset($_SESSION['qr_verification']) || !isset($_POST['qr_verification']) || $_SESSION['qr_verification'] != $_POST['qr_verification'])
{
    echo json_encode(['error' => 'Ошибка верификации! Обратитесь к администратору сервера.']);
    die;
}

$phone = preg_replace('/\D/', '', $phone);

if (!$name || strlen($name) < 3 || preg_match('/[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя\s]/', $name))
{
    echo json_encode(['error' => 'Введите корректное имя!']);
    die;
}

if (!$phone || strlen($phone) != 11 || !in_array($phone[0], [7,8]))
{
    echo json_encode(['error' => 'Введите корректный номер телефона!']);
    die;
}

// Инициализируем данные

$data = array();

$data["date"] = date('d.m.Y H:i:s');
$data["request_type"] = 13;
$data['comment'] = '';
$data['credit_type'] = '';
$data['code_bs'] = '';
$data['entrance_source'] = isset($_COOKIE['entrance_source']) ? htmlspecialchars($_COOKIE['entrance_source']) : '';
$data['utm_campaign'] = isset($_COOKIE['utm2campaign']) ? htmlspecialchars($_COOKIE['utm2campaign']) : '';
$data['utm_content'] = isset($_COOKIE['utm2content']) ? htmlspecialchars($_COOKIE['utm2content']) : '';
$data['utm_term'] = isset($_COOKIE['utm2term']) ? htmlspecialchars($_COOKIE['utm2term']) : '';
$data['utm_source'] = isset($_COOKIE['utm2source']) ? htmlspecialchars($_COOKIE['utm2source']) : '';
$data['utm_term'] = isset($_COOKIE['utm2medium']) ? htmlspecialchars($_COOKIE['utm2medium']) : '';

$data['fio'] = $name;
$data['phone'] = '8 ('.substr($phone, 1, 3).') '.substr($phone, 4, 3).'-'.substr($phone, 7, 2).'-'.substr($phone, 9, 2);
$data['email'] = '';

$data['ip'] = $_SERVER['REMOTE_ADDR'];
$data['ua'] = $_SERVER['HTTP_USER_AGENT'];

$section_id = CIBlockSection::GetList([], ["IBLOCK_ID" => REQUESTS_IBLOCK, "CODE" => $data['request_type']],
        0, ["ID"])->GetNext()["ID"];

// Смотрим, не было ли уже заявки с этого устройства

$arFilter = [
    'IBLOCK_ID' => REQUESTS_IBLOCK,
    'IBLOCK_SECTION_ID' => $section_id,
    array(
        'LOGIC' => 'OR',
        ['PROPERTY_PHONE' => $data['phone']],
        ['PROPERTY_IP' => $data['ip'], 'PROPERTY_UA' => $data['ua']]
    ),
];

$request = CIBlockElement::GetList([], $arFilter, 0,['ntopCount' => 1], ['ID', 'TIMESTAMP_X'])->GetNext();

if ($request)
{
    echo json_encode(['error' => 'Заявка уже была оставлена с данного устройства '.$request['TIMESTAMP_X'].'<br>Если Вы ранее не оставляли заявок, просим Вас связаться с нами через <a href="mailto:info@terem-pro.ru">info@terem-pro.ru</a>']);
    die;
}

// Иначе выдаём код

/*$codes = [];
$h = fopen('codes.txt', 'r');
if (!$h)
{
    echo json_encode(['error' => 'Неизвестная ошибка обработки запроса']);
    die;
}
while (($s = fgets($h)) && trim($s)) $codes[] = trim($s);
fclose($h);

$currrent_key = 0;
$current_code = '';

// Ищем первый код без статуса [G] - первый ещё невыданный код

foreach ($codes as $key => $code)
{
    if (!strpos($code, '[G]'))
    {
        $current_key = $key;
        $current_code = $code;
        break;
    }
}

if (!$current_code)
{
    echo json_encode(['error' => 'К сожалению, все коды уже закончились']);
    die;
}

$codes[$current_key] = $current_code.' [G]';

$h = fopen('codes.txt', 'w');
if (!$h)
{
    echo json_encode(['error' => 'Неизвестная ошибка обработки запроса']);
    die;
}

foreach ($codes as $code)
{
    fputs($h, $code."\r\n");
}

fclose($h);

// Генерируем PDF

$html = <<<PDF
<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Ваш уникальный код для получения подарка</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" lang="ru">
        <style>
            *
            {
                padding: 0;
                margin: 0;
                border: 0;
            }
            
            body
            {
                padding: 22mm 22mm 16mm 22mm;
            }
            
            header img, footer img
            {
                width: 166mm;
                height: auto;
            }
        
            main
            {
                padding: 22mm 0 28mm 0;
                text-align: center;
                font: bold 33mm 'Helvetica';
                color: #B10000;
            }
            
            
        </style>
    </head>
    <body>
        <header><img src="images/top.png"></header>
        <main>{$current_code}</main>
        <footer><img src="images/bottom.png"></footer>
    </body>
</html>
PDF;

$options = new Options();
$options->set('defaultFont', 'Helvetica');
$dompdf = new Dompdf($options);
$dompdf->loadHtml($html);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$result = $dompdf->output();
$fid = bin2hex(random_bytes(12));
file_put_contents('pdf/'.$fid.'.pdf', $result);

$data["attrs"] = array(
    ['VALUE' => 'Код', 'DESCRIPTION' => $current_code],
    ['VALUE' => 'Ссылка на PDF', 'DESCRIPTION' => '/eb430691fe30d16070b5a144c3d3303c/pdf/'.$fid.'.pdf'],   
);*/

// Saving data to Iblock


$props = array();

$props["FIO"] = $data['fio'];
$props["PHONE"] = $data['phone'];
$props["EMAIL"] = $data['email'];
$props["COMMENT"] = $data['comment'];
// $props["ATTRS"] = $data['attrs'];
$props["UTM_CAMPAIGN"] = $data['utm_campaign'];
$props["UTM_TERM"] = $data['utm_term'];
$props["UTM_CONTENT"] = $data['utm_content'];
$props["ENTRANCE_SOURCE"] = $data['entrance_source'];
$props["IP"] = $data['ip'];
$props["UA"] = $data['ua'];

$fields = array(
    
    "IBLOCK_ID" => REQUESTS_IBLOCK,
    "IBLOCK_SECTION_ID" => $section_id,
    "NAME" => $data['fio'].', '.$data['phone'].' ('.$data['date'].')',
    "ACTIVE" => "Y",
    "PROPERTY_VALUES" => $props
    
);

$e = new CIBlockElement;
// Creating XML

if ($id = $e->Add($fields))
{
	/*
    $xmlstr =   '<?xml version="1.0" encoding="UTF-8"?>'.
                '<Заявка '.
                'ФИО="'.$data['fio'].'" '.
                'Телефон="'.$data['phone'].'" '.
                'ВидЗаявки="1" '.
                'ДатаЗаявки="'.$data['date'].'" '.
                'Реклама="'.$data['entrance_source'].'" '.
                'РекламаКампания="'.$data['utm_campaign'].'" '.
                'РекламаОбъявление="'.$data['utm_content'].'" '.
                'РекламаКлючевоеСлово="'.$data['utm_term'].'" '.
                'КодБУ="'.$data['code_bs'].'" '.
                'Кредит="'.$data['credit_type'].'" '.
                'СписокДУ="" '.
                'ID="'.$id.'">'.
                '</Заявка>';
	*/

	$xmlstr = '<?xml version="1.0" encoding="UTF-8"?>'.
				'<Заявка ID="'.$id.'" СписокДУ="" Кредит="'.$data['credit_type'].'" КодБУ="'.$data['code_bs'].'" РекламаКлючевоеСлово="" РекламаОбъявление="" РекламаКампания="ИНТЕРНЕТ (FB Заявки)" Реклама="'.$data['entrance_source'].'" ДатаЗаявки="'.$data['date'].'" ВидЗаявки="1" Телефон="'.$data['phone'].'" ФИО="'.$data['fio'].'" email="'.$email.'" utm_source="'.$data['utm_source'].'" utm_medium="'.$data['utm_medium'].'" utm_campaign="'.$data['utm_campaign'].'" utm_content="'.$data['utm_content'].'" utm_term="'.$data['utm_term'].'" КачествоЗаявки="1" />'.'</Заявка>';
    
    $file_path = $_SERVER["DOCUMENT_ROOT"].'/xml_requests/'.$id.'.xml';

    if(file_put_contents($file_path, $xmlstr) and $handle = ftp_connect('u208395.ftp.masterhost.ru')
        and ftp_login($handle, 'u208395_ftp', 'n6ssella4lingr')
        and ftp_put($handle, 'requests/'.$id.'.xml', $file_path, FTP_ASCII) and ftp_close($handle))
            unlink($file_path);

    echo json_encode(['code' => $id]);
}
else
    echo json_encode(['error' => $e->LAST_ERROR]);
