<?php

if (!isset($_GET['key']) || $_GET['key'] != 'ef44bb5d')
{
    die;
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Заявка на получение кода");
$_SESSION['qr_verification'] = bin2hex(random_bytes(12));

?>
<script>

</script>

<section class="content content-back-side" role="content" data-parallax="up" data-opacity="true">
    <div class="container-fluid reset-padding">
        <div class="row reset-margin">
            <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
                <div class="back-img" style="background-image: url('/bitrix/templates/.default/assets/img/bg.jpg');"></div>
            </div>
        </div>
    </div>
</section>
<section class="content text-page white claim-code" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12 my-margin">
                <div class="white padding-side fence-padding clearfix">
                    <div class="row">
                        <div class="my-text-title text-uppercase desgin-h1 message">
                            <h2>
                                Получить сертификат на сигнализацию?
                            </h2>
                            <p>Для отправки заявки заполните, пожалуйста, все поля.</p>
                            <p>Внимание! Отправляя форму, вы соглашаетесь с <a href="/privacy-policy/">политикой обработки данных</a></p>
                        </div>
                        <form class="form-send form-fense my-margin new" action="coderequest.php" method="post">


                                <label>Введите ваше имя
                                    <input type="text" name="name" class="form-control my-input" placeholder="Иванов Иван Иванович" required="">

                                </label>



                                <label>Введите телефон
                                    <input type="tel" placeholder="+7 495 123-45-67" name="phone" class="form-control my-input" required="">
                                </label>




                                <button type="submit" class="btn btn-danger text-uppercase">Отправить заявку</button>


                                <input type="hidden" value="<?=$_SESSION['qr_verification']?>" name="qr_verification">
                        </form>
                        <div>
                            <div class="help-block with-errors"></div>
                            <!-- <div class="help-block with-errors"></div>
                            <div class="help-block with-errors"></div> -->
                            <p class="response"></p>
                        </div>

                        <script>
                            document.addEventListener('DOMContentLoaded', () => {
                                $('[type="tel"]').mask("+7 999 999-99-99", {
                                    placeholder: "_ "
                                });
                            })

                            var form = $('form');

                            form.on('submit', function (e) {
                                e.preventDefault();
                                var btn = $(form).find('button');
                                btn.attr('disabled', true);
                                var link = $(this).attr('action');
                                var data = new FormData(this);
                                $.ajax({
                                    url: link,
                                    type: $(this).attr("method"),
                                    data: new FormData(this),
                                    processData: false,
                                    contentType: false,
                                    success: function (result, status) {
                                        var data = JSON.parse(result);
                                        if ("error" in data)
                                        {
                                            btn.attr('disabled', false);
                                            $('p.response').html(data.error);
                                        }
                                        else if("code" in data && "file" in data)
                                        {
                                            form.find('input').attr('disabled', true);
                                            $('p.response').html(
                                                    `Спасибо, ваша заявка успешно зарегистрирована!<br>
                                                    Ваш уникальный код: ${data.code}<br>
                                                    Через 2 секунды Вы будете перенаправлены для скачивания PDF-файла с вашим персональным кодом<br>
                                                    <a href="${data.file}">Скачать самостоятельно</a>`
                                                    );
                                            
                                            yaCounter937330.reachGoal('QR_ALARM_SENT');

                                            setTimeout(function(){ location.href = data.file; }, 2250);
                                        }
                                    },
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
