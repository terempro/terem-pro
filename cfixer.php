<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$pl = CIBlockElement::GetList([], ['IBLOCK_ID' => 22], 0, 0, ['ID', 'PROPERTY_OPTIMAL_PRICE', 'PROPERTY_PRICE_1']);

while ($p = $pl->GetNext())
{
    if ($p['PROPERTY_OPTIMAL_PRICE_VALUE'] && $p['PROPERTY_OPTIMAL_PRICE_VALUE'] != $p['PROPERTY_PRICE_1_VALUE'])
    {
        v(1);
    }
}

