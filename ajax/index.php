<?php
session_start();
$_SESSION['qr_verification'] = bin2hex(random_bytes(12));
?>
<!DOCTYPE html>
<html lang="ru">

    <head>

        <meta charset="utf-8">
        <title>Теремъ &mdash; акция cигнализация в подарок! Купи дом и получи сигнализацию в подарок!</title>
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="og:image" content="path/to/image.jpg">
        <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="/bitrix/css/main/font-awesome.min.css">

        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#000">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#000">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#000">

        <style>
            body {
                opacity: 0;
                overflow-x: hidden;
            }

            html {
                background-color: #fff;
            }
        </style>

    </head>

    <body>
        <header class="header hero">
            <div class="container">
                <div class="header--wrapper">
                    <a href="#hero" class="logo hero smooth"></a>
                    <nav class="nav">
                        <!-- <a href="#gallery" data-name="gallery" class="nav__link smooth">Галерея</a> -->
                        <a href="#projects" data-name="projects" class="nav__link smooth">Проекты</a>
                        <a href="#feedback" data-name="feedback" class="nav__link smooth">Отзывы</a>
                        <a href="#materials" data-name="materials" class="nav__link smooth">Материалы</a>
                        <a href="#choose" data-name="choose" class="nav__link smooth">Подбор проекта</a>
                        <a href="#partner" data-name="partner" class="nav__link smooth">Партнер</a>
                        <a href="#contacts" data-name="contacts" class="nav__link smooth">Контакты</a>
                    </nav>
                    <i class="sandwich-button fa fa-bars"></i>
                </div>
            </div>
        </header>
        <div class="section hero" id="hero">
            <div class="container">
                <div class="section--wrapper">

                    <div class="myrow heading-row">
                            <h1 class="main-title">ПРИ ПОКУПКЕ ДОМА </br>сигнализация <span>в&nbsp;подарок*</span></h1>

                            <img src="img/ajax/item5.png" alt="" class="header_item"/>
                    </div>

                    <div>
                        <div class="row counter-row myrow">
                            <div class="col">
                                <p class="my-text">до окончания акции осталось:</p>
                                <div class="counter">
                                    <div class="counter__item days">
                                        <div class="counter__digits">00</div>
                                        <div class="counter__time">дней</div>
                                    </div>
                                    <div class="counter__item hours">
                                        <div class="counter__digits">00</div>
                                        <div class="counter__time">часов</div>
                                    </div>
                                    <div class="counter__item minutes">
                                        <div class="counter__digits">00</div>
                                        <div class="counter__time">минут</div>
                                    </div>
                                    <div class="counter__item seconds">
                                        <div class="counter__digits">00</div>
                                        <div class="counter__time">секунд</div>
                                    </div>
                                </div>

                            </div>
                            <div class="col">
                                <a class="btn btn--big smooth" href="#kitchen">Получить<br>сигнализацию <i class="fa fa-caret-right arrow"></i></a>
                            </div>

                        </div>
                        <div class="row smaller-text-row myrow">
                              <div class="smaller-text">
                                    * При приобретении дома, выдаётся сертификат, номиналом 10 000 (десять тысяч) рублей, дающий право на получение скидки на покупку беспроводной сигнализации Ajax в ООО «АЛЬФАТОРГ».
                                 </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>

        <div class="section kitchen" id="kitchen">
            <div class="container">
                <div class="kitchen--wrapper">
                    <div class="kitchen--left">
                        <div class="group">
                            <div class="title title--grey">
                                При покупке дома<br><span>сигнализация в&nbsp;подарок</span>
                            </div>
                            <p style="margin-top: 0">
                                Акция действует<br>с&nbsp;01.11.2017 года по&nbsp;01.12.2017 года включительно
                            </p>
                        </div>

                        <div class="group">
                            <p>
                                До конца акции осталось:
                            </p>
                            <div class="counter">
                                <div class="counter__item days">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">дней</div>
                                </div>
                                <div class="counter__item hours">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">часов</div>
                                </div>
                                <div class="counter__item minutes">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">минут</div>
                                </div>
                                <div class="counter__item seconds">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">секунд</div>
                                </div>
                            </div>
                        </div>

                        <div class="group">
                            <h2 class="moto">Тёплые дома<br>для всей семьи</h2>
                        </div>

                        <div class="group">
                            <a href="#modal-rules" data-link="modal">Правила акции</a>
                        </div>
                    </div>
                    <div class="kitchen--right">
                        <div class="group">
                            <div class="title">
                                <span>Получить сигнализацию?</span>
                            </div>
                        </div>
                        <div class="group">
                            <p class="form__title">
                                Заполните форму
                            </p>
                            <p class="form__annotation">
                                Для получения персонального предложения и консультации
                            </p>
                        </div>
                        <div class="group">
                            <form name="alarm-form" action="/eb430691fe30d16070b5a144c3d3303c/coderequest.php" data-yagoal="REQUEST_ALARM_SENT" method="post">
                                <div class="form__group">
                                    <label for="kitchen-name" class="form__label">
                                        Как к вам удобнее обратиться
                                        <input name="name" required type="text" id="kitchen-name" class="form__input" placeholder="Ваше имя">
                                    </label>
                                    <label for="kitchen-tel" class="form__label">
                                        Ваш контактный номер
                                        <input name="phone" required type="tel" id="kitchen-tel" class="form__input" data-item="phone" placeholder="+7 495 123-45-67">
                                    </label>
                                </div>
                                <div class="form__group">
                                    <input required type="checkbox" class="checkbox-input" id="kitchen-agree" hidden>
                                    <label for="kitchen-agree" class="form__label form__label--checkbox">
                                        <span>Я принимаю условия <a href="/privacy-policy">политики обработки данных</a></span>
                                    </label>
                                </div>
                                <div class="form__group">
                                    <input type="hidden" value="<?=$_SESSION['qr_verification']?>" name="qr_verification">
                                    <button type="submit" class="btn btn--form">Получить сертификат</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="section gallery" id="gallery">
                <div class="container">
                        <div class="gallery--wrapper">
                                <div class="gallery__header">
                                        <div class="title title--white">Фото построенных домов</div>
                                </div>
                                <div class="gallery__slider">
                                        <div class="slider">
                                                <div class="slider1">
                                                        <div id="slider1">
                                                                <div class="slider__item">
                                                                        <a href="img/build/7.jpg" class="fancybox"><img src="img/build/7.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/8.jpg" class="fancybox"><img src="img/build/8.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/9.jpg" class="fancybox"><img src="img/build/9.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/10.jpg" class="fancybox"><img src="img/build/10.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/11.jpg" class="fancybox"><img src="img/build/11.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/12.jpg" class="fancybox"><img src="img/build/12.jpg" /></a>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="slider2">
                                                        <div id="slider2" style="margin-top: 5px">
                                                                <div class="slider__item">
                                                                        <a href="img/build/1.jpg" class="fancybox"><img src="img/build/1.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/2.jpg" class="fancybox"><img src="img/build/2.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/3.jpg" class="fancybox"><img src="img/build/3.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/4.jpg" class="fancybox"><img src="img/build/4.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/5.jpg" class="fancybox"><img src="img/build/5.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/6.jpg" class="fancybox"><img src="img/build/6.jpg" /></a>
                                                                </div>

                                                        </div>
                                                </div>
                                        </div>
                                </div>
                                <div class="gallery__footer">
                                        <div class="group">
                                                <h2 class="moto">Тёплые дома<br>для всей семьи</h2>
                                        </div>
                                        <div class="group">
                                                <div class="counter">
                                                        <div class="counter__item days">
                                                                <div class="counter__digits">00</div>
                                                                <div class="counter__time">дней</div>
                                                        </div>
                                                        <div class="counter__item hours">
                                                                <div class="counter__digits">00</div>
                                                                <div class="counter__time">часов</div>
                                                        </div>
                                                        <div class="counter__item minutes">
                                                                <div class="counter__digits">00</div>
                                                                <div class="counter__time">минут</div>
                                                        </div>
                                                        <div class="counter__item seconds">
                                                                <div class="counter__digits">00</div>
                                                                <div class="counter__time">секунд</div>
                                                        </div>
                                                </div>
                                                <a class="btn btn--big smooth" href="#kitchen">Получить кухню<br>в подарок</a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div> -->
        <div class="section choose" id="projects">
            <div class="container">
                <div class="choose--wrapper">
                    <div class="choose__header">
                        <span class="title title--grey">
                            Выберите <span>свой дом</span>
                        </span>
                        <!-- <span class="bold-grey">от 110 до 330 кв.м</span> -->
                    </div>
                    <section class="choose-house--houses">
                        <!-- <div class="choose-house--row"> -->
                            <div class="house--wrapper house--wrapper__important">
                                <article class="choose-house--house house house__important" data-house="Монарх 1">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Монарх 1</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <span class="house--material">Кирпич</span>
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">10 × 11.5 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 209.64  кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 1 585 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о доме
                                                <i class="fa fa-caret-right arrow"></i>
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/houses/preview/290-mon.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                        <!-- </div> -->

                        <!-- <div class="choose-house--row"> -->
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Лидер 7">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Лидер 7</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">6 × 7 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 62.99  кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 310 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о доме
                                                <i class="fa fa-caret-right arrow"></i>
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/houses/preview/290-lider.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Богатырь 1">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Богатырь 1</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">7 × 7 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 73.24  кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 440 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о доме
                                                <i class="fa fa-caret-right arrow"></i>
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/houses/preview/290-bog.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                        <!-- </div> -->

                        <!-- <div class="choose-house--row"> -->
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Баня 6 × 6 м">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Баня 6 × 6 м</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">6 × 6 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 32.40 кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 285 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о доме
                                                <i class="fa fa-caret-right arrow"></i>
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/houses/preview/290-ban.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Варяг 1">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Варяг 1</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">8 × 8 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 110.53  кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 715 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о доме
                                                <i class="fa fa-caret-right arrow"></i>
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/houses/preview/290-var.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                        <!-- </div> -->



                        <!-- <div class="choose-house--row"> -->
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Канцлер 4">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Канцлер 4</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <span class="house--material">Кирпич</span>
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">10.5 × 12 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 218.73  кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 1 650 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о доме
                                                <i class="fa fa-caret-right arrow"></i>
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/houses/preview/kanzler4.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Добрыня 3">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Добрыня 3</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <span class="house--material">Кирпич</span>
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">8.5 × 10 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 75.51  кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 590 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о доме
                                                <i class="fa fa-caret-right arrow"></i>
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/houses/preview/290-dobr.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                        <!-- </div> -->
                    </section>
                    <!-- <div class="slider--wrapper">
                        <div class="slider__extra">
                            <a href="#modal-call" data-link="modal" class="btn btn--label">Оставить<br>заявку</a>
                            <div class="counter">
                                <div class="counter__item days">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">дней</div>
                                </div>
                                <div class="counter__item hours">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">часов</div>
                                </div>
                                <div class="counter__item minutes">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">минут</div>
                                </div>
                                <div class="counter__item seconds">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">секунд</div>
                                </div>
                            </div>
                        </div>
                        <div class="single-item">

                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="section feedback" id="feedback">
            <div class="container">
                <div class="feedback--wrapper">
                    <div class="feedback__header">
                        <div class="title title--white">Отзывы наших клиентов</div>
                    </div>
                    <div class="slider">
                        <div id="video-slider" class="slider--wrapper">
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/J6yMcicOfno?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Кобзон<br>Иосиф<br>Давыдович
                                    </div>
                                    <div class="slider__house">
                                        Герцог 1 | Индивидуальный проект
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/AOeEuOAR0_g?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Михаил<br>Четвертаков
                                    </div>
                                    <div class="slider__house">
                                        Полковник 3 | 18х12,5 м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/N6gCDS-bV5s?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Игорь<br>Сорокин
                                    </div>
                                    <div class="slider__house">
                                        Герцог 2 | 8,5х9,5 м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/m-_63RZUlV0?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Лариса<br>Белоус
                                    </div>
                                    <div class="slider__house">
                                        Канцлер 4 | 10,5 х12 м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/W7y79xQCOIQ?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Татьяна<br>Григорова<br>и Никита<br>Астафьев
                                    </div>
                                    <div class="slider__house">
                                        Герцог 1 | 8,5х9,5 м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/DNwJjQXneHI?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Алексей<br>Филипов
                                    </div>
                                    <div class="slider__house">
                                        Канцлер 2 | 8х9 м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/4ww7kbTiyXY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Елена<br>Копцева
                                    </div>
                                    <div class="slider__house">
                                        Канцлер 3 | 10х10 м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/u3t9k6MMPmo?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Татьяна<br>Моргунова
                                    </div>
                                    <div class="slider__house">
                                        Канцлер 1 | 8х7,5 м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/v6YD6TFjz5U?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Ольга<br>Михальчева
                                    </div>
                                    <div class="slider__house">
                                        Варяг 4 | 9х9 м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/OFX03Fv9288?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Борис<br>Федоров
                                    </div>
                                    <div class="slider__house">
                                        Канцлер 1 | 8х7,5 м
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feedback__footer">
                        <div class="group">
                            <h2 class="moto">Тёплые дома<br>для всей семьи</h2>
                        </div>
                        <div class="group">
                            <div class="counter">
                                <div class="counter__item days">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">дней</div>
                                </div>
                                <div class="counter__item hours">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">часов</div>
                                </div>
                                <div class="counter__item minutes">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">минут</div>
                                </div>
                                <div class="counter__item seconds">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">секунд</div>
                                </div>
                            </div>
                            <a class="btn btn--big smooth" href="#kitchen">Получить сигнализацию<br>в подарок</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section materials" id="materials">
            <div class="container">
                <div class="materials--wrapper">
                    <div class="materials__header">
                        <div class="title title--grey">Из чего мы строим?</div>
                        <div class="group">
                            <div class="counter">
                                <div class="counter__item days">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">дней</div>
                                </div>
                                <div class="counter__item hours">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">часов</div>
                                </div>
                                <div class="counter__item minutes">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">минут</div>
                                </div>
                                <div class="counter__item seconds">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">секунд</div>
                                </div>
                            </div>
                            <a class="btn btn--big smooth" href="#kitchen">Получить сигнализацию<br>в подарок</a>
                        </div>
                    </div>
                    <div class="materials__body">
                        <div class="materials__raw">
                            <div class="item">
                                <div class="item__icon">
                                    <img src="img/wood1.png" alt="Северная сосна">
                                </div>
                                <div class="item__text">Северная<br>сосна</div>
                            </div>
                            <div class="item">
                                <div class="item__icon">
                                    <img src="img/wood2.png" alt="Северная ель">
                                </div>
                                <div class="item__text">Северная<br>ель</div>
                            </div>
                        </div>
                        <div class="materials__tech">
                            <div class="item">
                                <div class="item__img">
                                    <img src="img/1.png" alt="Брус">
                                </div>
                                <div class="item__name">Брус</div>
                                <div class="item__btn">
                                    <a href="/about/expert-opinion/tekhnologii-stroitelstva/#lumber" class="btn btn--grey">Подробнее<br>о технологии</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="item__img">
                                    <img src="img/2.png" alt="Каркас">
                                </div>
                                <div class="item__name">Каркас</div>
                                <div class="item__btn">
                                    <a href="/about/expert-opinion/tekhnologii-stroitelstva/#frame" class="btn btn--grey">Подробнее<br>о технологии</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="item__img">
                                    <img src="img/3.png" alt="Кирпич">
                                </div>
                                <div class="item__name">Кирпич</div>
                                <div class="item__btn">
                                    <a href="/about/expert-opinion/tekhnologii-stroitelstva/#brick" class="btn btn--grey">Подробнее<br>о технологии</a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="section help" id="choose">
            <div class="container">
                <div class="help--wrapper">
                    <div class="title title--grey">
                        Не нашли подходящий дом?
                    </div>
                    <form action="/include/request_handler.php" class="help__form form" data-yagoal="REQUEST_ALARM_PROJECT_PICKING_UP_SENT">
                        <div class="form--left">
                            <div class="form__title">
                                Менеджер подберет вам проект
                            </div>
                            <div class="form__collection">
                                <div class="form__collection--left">
                                    <div class="form__checkbox">
                                        <input name="type[]" value="Клееный брус" type="checkbox" id="brus" hidden="">
                                        <label for="brus" class="form__label">клееный брус</label>
                                    </div>
                                    <div class="form__checkbox">
                                        <input name="type[]" value="Каркас" type="checkbox" id="karkas" hidden="">
                                        <label for="karkas" class="form__label">каркас</label>
                                    </div>
                                    <div class="form__checkbox">
                                        <input name="type[]" value="Кирпич" type="checkbox" id="kirpich" hidden="">
                                        <label for="kirpich" class="form__label">кирпич</label>
                                    </div>
                                </div>

                                <div class="form__collecton--right">
                                    <div class="form__checkbox">
                                        <input name="floors[]" type="checkbox" value="Один этаж" id="one_floor" hidden="">
                                        <label for="one_floor" class="form__label">одноэтажный</label>
                                    </div>
                                    <div class="form__checkbox">
                                        <input name="floors[]" value="Два этажа" type="checkbox" id="two_floors" hidden="">
                                        <label for="two_floors" class="form__label">два этажа</label>
                                    </div>
                                    <div class="form__area">
                                        <input name="square" type="text" placeholder="Площадь" id="area" class="form__area--input">
                                        <label for="area" class="form__area--label">кв.м</label>
                                    </div>
                                </div>
                            </div>
                            <div class="group">
                                <h2 class="moto">Тёплые дома<br>для всей семьи</h2>
                            </div>
                        </div>
                        <div class="form--right">
                            <div class="form__group">
                                <label for="ki tchen-name" class="form__label">
                                    Как к вам удобнее обратиться
                                    <input required name="name" type="text" id="kitchen-name" class="form__input" placeholder="Ваше имя">
                                </label>
                                <label for="kitchen-tel" class="form__label">
                                    Ваш контактный номер
                                    <input required name="phone" type="tel" id="kitchen-tel" class="form__input" data-item="phone" placeholder="+7 495 123-45-67">
                                </label>
                            </div>
                            <div class="form__group" style="margin-top: 40px">
                                <input required type="checkbox" class="checkbox-input" id="help-agree" hidden="">
                                <label for="help-agree" class="form__label form__label--checkbox">
                                    <span>Я принимаю условия <a href="/privacy-policy">Политики обработки данных</a></span>
                                </label>
                            </div>
                            <div class="form__group">
                                <input type="hidden" name="request_type" value="15">
                                <button type="submit" class="btn btn--form">Получить консультацию</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="section partner" id="partner">
            <div class="container">
                <div class="partner--wrapper">
                    <div class="partner--left">
                        <div class="partner__logo">
                            <img src="img/alarm/logo.png" alt="Ajax">
                        </div>
                        <div class="partner__text">
                            <h2 class="partner__title">Компания &laquo;Альфаторг&raquo;</h2>
                            <p>
                                Официальный дистрибьтор систем безопаности Ajax в&nbsp;России.
                            </p>
                            <p>
                                Системы AJAX &mdash; это GSM-сигнализации нового поколения, которые помогают предотвращать не&nbsp;только проникновение воров, но&nbsp;и&nbsp;возгорание, и&nbsp;затопление помещений. Управлять сигнализацией можно с&nbsp;мобильного устройства или компьютера, что позволяет всегда быть в&nbsp;курсе происходящего в&nbsp;доме. Интуитивно понятная система настроек помогает установить сигнализацию самостоятельно всего за&nbsp;15&nbsp;минут, а&nbsp;благодаря экономичному потреблению энергии датчики могут работать до&nbsp;7&nbsp;лет на&nbsp;одной батарейке.
                            </p>
                            <p>
                                Эффективность систем безопасности AJAX подтверждают 100&nbsp;000 семьей и&nbsp;компаний по&nbsp;всему миру.
                            </p>
                            <!-- <p>
                                Своими гарнитурами мы создаем интерьер для спокойного домашнего уюта и веселых дружеских встреч.
                            </p>
                            <p>
                                «Магия кухни» — сделаем вашу кухню комфортной и стильной!
                            </p> -->
                        </div>
                        <div class="partner__contacts">
                            <p>
                                8-800-500-65-54<br><a href="http://alfatorg-sb.ru/">www.alfatorg-sb.ru</a>
                            </p>
                        </div>
                    </div>
                    <div class="partner--right">
                        <div class="row">
                            <img src="img/alarm/1.jpg" alt="">
                            <img src="img/alarm/2.jpg" alt="">
                        </div>
                        <div class="row">
                            <img src="img/alarm/3.jpg" alt="">
                        </div>
                        <div class="row">
                            <div class="col">
                                <img src="img/alarm/4.jpg" alt="">
                                <img src="img/alarm/5.jpg" alt="">
                            </div>
                            <div class="col">
                                <img src="img/alarm/6.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="section contacts" id="contacts">
            <div class="container">
                <div class="contacts--wrapper">
                    <div class="contacts__map">
                        <div class="contacts__panel">
                            <div class="title title--grey">Контакты</div>
                            <div class="group address">
                                <strong>Адрес:</strong>
                                <span>г. Москва,</span>
                                <span>ул. Зеленодольская, вл.42</span>
                            </div>
                            <div class="group phone">
                                <strong>Телефон:</strong>
                                <span>8 (495) 419-07-78</span>
                            </div>
                            <div class="group email">
                                <strong>E-mail:</strong>
                                <a href="mailto:info@terem-pro.ru">info@terem-pro.ru</a>
                            </div>
                            <div class="group site">
                                <strong>Сайт:</strong>
                                <a href="https://terem-pro.ru">www.terem-pro.ru</a>
                            </div>
                            <div class="socials">
                                <div>
                                    <a href="https://www.facebook.com/terempro" class="fb"></a>
                                    <a href="https://twitter.com/ProTerem" class="tw"></a>
                                    <a href="http://vk.com/pro_terem" class="vk"></a>
                                </div>
                                <div>
                                    <a href="http://ok.ru/v2010godu" class="ok"></a>
                                    <a href="http://instagram.com/pro_terem" class="in"></a>
                                    <a href="http://terem-pro.livejournal.com/" class="lj"></a>
                                </div>
                            </div>
                        </div>
                        <div class="contacts__ymap">
                            <div id="map" style="width: 100%; height: 600px"></div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="section questions">
                <div class="container">
                    <div class="questions--header">
                        <h2 class="questions--heading">
                            Остались вопросы?
                        </h2>
                        <section class="questions--message">
                            <p>Оставьте номер</p>
                            <p>Наши специалисты свяжутся с вами</p>
                        </section>
                    </div>
                    <form action="/include/request_handler.php" class="questions--form form" data-yagoal="REQUEST_ALARM_QUESTIONS_LEFT_SENT">
                        <label for="questions--name">
                            Как к вам удобнее обратиться?
                            <input name="name" type="text" id="questions--name" placeholder="Ваше имя">
                        </label>
                        <label for="questions--phone">
                            Ваш контактный номер
                            <input name="phone" type="tel" id="questions--phone" data-item="phone" placeholder="+7 495 123-45-67">
                        </label>
                        <input type="hidden" name="request_type" value="14">
                        <button type="submit">Перезвонить мне</button>
                    </form>
                </div>
            </section>

            <footer class="contacts__footer">
                <div class="container">
                    <p>
                        © Теремъ-про, 2009 - 2017. Все материалы данного сайта являются объектами авторского права (в том числе дизайн). Запрещается
                        копирование, распространение (в том числе путем копирования на другие сайты и ресурсы в Интернете) или любое иное использование
                        информации и объектов без предварительного согласия правообладателя. Cайт не является публичной офертой.
                    </p>
                </div>
            </footer>
        </section>



        <link rel="stylesheet" href="css/main.min.css">
        <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
        <script src="js/scripts.min.js"></script>


    </body>


</html>

<!-- Modals -->

<div id="modal-code" class="modal code-modal" role="dialog">
    <div class="code-modal--container">
        <p class="attention">Внимание!</p>
        <p class="message">Ваш персональный код <br>для получения сертификата на сигнализацию</p>
        <span class="code">######</span>
        <p class="small-text">Сохраните этот код и предъявите менеджеру при покупке дома или бани</p>
        <p class="small-text">Представитель компании свяжется с вами в ближайшее время</p>
        <a class="save">Сохранить</a>
        <p class="error-msg">
            Вы уже получили код
        </p>
        <button class="close">×</button>
    </div>

</div>

<!-- Modal thx -->
<div id="modal-thx" class="modal modal-thx" role="dialog">
    <div class="modal__dialog">
        <button type="button" class="close"></button>
        <div class="modal__header">
            <h3 class="modal__title">
                <span></span>
                <span class="title title--grey">Спасибо!</span>
            </h3>
            <div class="modal__subtitle">
            </div>
        </div>
        <div class="modal__body">
            <span>Ваша заявка принята!<br>Наши менеджеры скоро с вами свяжутся.</span>
        </div>
    </div>
</div>



<!-- Modal rules -->
<div id="modal-rules" class="modal modal-rules" role="dialog">
    <div class="modal__dialog">
        <button type="button" class="close"></button>
        <div class="modal__header">
            <h3 class="modal__title">
                <span></span>
                <span class="title title--grey">Правила акции</span>
            </h3>
            <div class="modal__subtitle">
            </div>
        </div>
        <div class="modal__body">
            <ol>
                <li>Общие положения
                    <ol>
                        <li>Организатором акции &laquo;Сигнализации для дома в&nbsp;подарок&raquo; является юридическое лицо, созданное в&nbsp;соответствии с&nbsp;законодательством Российской Федерации. Наименование: Общество с&nbsp;ограниченной ответственностью &laquo;Теремъ-про&raquo;. Юридический адрес: 140170, Московская область, г. Бронницы, ул. Центральная, д.2., ОГРН&nbsp;&mdash; 1105040000174, ИНН&mdash; 5002096561.</li>
                        <li>Партнером акции &laquo;Сигнализации для дома в&nbsp;подарок&raquo; является юридическое лицо, созданное в&nbsp;соответствии с&nbsp;законодательством Российской Федерации. Наименование: Общество с&nbsp;ограниченной ответственностью ООО &laquo;АЛЬФАТОРГ&raquo;. Юридический адрес: 119048, г. Москва, ул. Ефремова, 19к4, пом.&nbsp;2, ОГРН&nbsp;&mdash; 1167746891905, ИНН- 7704372706.</li>
                    </ol>
                </li>
                <li>Сроки проведения
                    <ol>
                        <li>Срок публикации в&nbsp;средствах информации: с&nbsp;01.11.2017 года по&nbsp;01.12.2017 года включительно.</li>
                        <li>Сроки проведения акции и&nbsp;срок выдачи сертификатов: с&nbsp;01.11.2017 года по&nbsp;01.12.2017 года включительно.</li>
                        <li>Сертификат может быть предъявлен партнеру с&nbsp;момента его получения и&nbsp;до&nbsp;30.04.2018&nbsp;года.</li>
                        <li>Срок снятия с&nbsp;публикации: 01.12.2017&nbsp;года.</li>
                    </ol>
                </li>
                <li>
                    Условия проведения акции
                    <ol>
                        <li>При заключении клиентом с&nbsp;организатором договора на&nbsp;строительство бани, дома в&nbsp;период с&nbsp;01.11.2017г.&nbsp;до&nbsp;01.12.2017г., клиент получает сертификат номиналом 10&nbsp;000&nbsp;рублей.</li>
                        <li>Сертификат нельзя обналичить, продать или обменять.</li>
                        <li>Сертификат не&nbsp;подлежит возврату и&nbsp;обмену на&nbsp;денежные средства.</li>
                        <li>Сертификат можно использовать только в&nbsp;рамках одного договора на&nbsp;приобретение товара партнера.</li>
                        <li>Организатор акции не&nbsp;несет ответственность за&nbsp;качество товара партнера, сроки предоставления партнером услуг.</li>
                        <li>Комплектация товара, срок поставки и&nbsp;монтажа оговариваются с&nbsp;партнером отдельно.</li>
                        <li>Доставка в&nbsp;удалённые регионы производится согласно тарифам грузоперевозчика со&nbsp;100% предоплатой и&nbsp;с&nbsp;обязательным страхованием груза за&nbsp;счёт клиента.</li>
                        <li>Настройка и&nbsp;установка товара не&nbsp;входит в&nbsp;акционное предложение и&nbsp;могут быть заказаны отдельно.</li>
                        <li>По&nbsp;истечении указанного срока сертификат становится недействительным и&nbsp;скидка не&nbsp;предоставляется.</li>
                        <li>В&nbsp;случае потери, порчи, кражи сертификата он&nbsp;не&nbsp;восстанавливается, дубликат сертификата не&nbsp;выдается.</li>
                        <li>Товар партнера, участвующий в&nbsp;акции: охранная система AJAX (далее&nbsp;&mdash; товар):
                            <ol>
                                <li>Базовая станция Ajax Hub (GSM+Ethernet)</li>
                                <li>Беспроводная клавиатура Ajax KeyPad</li>
                                <li>Брелок AjaxSpaceControl</li>
                                <li>Датчик акустический разбития стекла Ajax GlassProtect</li>
                                <li>Датчик движения Ajax MotionProtect</li>
                                <li>Датчик движения с&nbsp;микроволновым детектором AjaxMotionProtectPlus</li>
                                <li>Датчик дыма и&nbsp;температуры Ajax FireProtect</li>
                                <li>Датчик комбинированный движения и&nbsp;разбития стекла Ajax CombiProtect</li>
                                <li>Датчик универсальный открытия двери и&nbsp;окна Ajax DoorProtect</li>
                                <li>Датчик утечки воды Ajax LeaksProtect</li>
                                <li>Контроллер для розеток Ajax WallSwitch</li>
                                <li>Сирена домашняя Ajax HomeSiren</li>
                                <li>Сирена уличная Ajax StreetSiren</li>
                            </ol>
                        </li>
                    </ol>
                </li>
                <li>
                    Территория проведения акции
                    <ol>
                        <li>Акция проводится на территории РФ.</li>
                    </ol>
                </li>
                <li>
                    Порядок и способ информирования участников акции
                    <ol>
                        <li>Информирование участников акции и&nbsp;потенциальных участников акции, о&nbsp;ее&nbsp;условиях, сроках, досрочном прекращении ее&nbsp;проведения будет происходить посредством размещения информации по&nbsp;адресу <a href="https://www.terem-pro.ru/ajax/">https://www.terem-pro.ru/ajax/</a></li>
                        <li>Размещения настоящих правил акции в&nbsp;глобальной сети интернет по&nbsp;адресу <a href="https://www.terem-pro.ru/ajax/">https://www.terem-pro.ru/ajax/</a> на&nbsp;весь период срока проведения акции.</li>
                    </ol>
                </li>
                <li>
                    Призовой фонд акции
                    <ol>
                        <li>Призовой фонд акции формируется за счет средств партнера акции.</li>
                        <li>Призовой фонд включает в&nbsp;себя 500 (пятьсот) сертификатов номиналом 10&nbsp;000 (десять тысяч) рублей каждый.</li>
                    </ol>
                </li>
                <li>
                    Участники акции
                    <ol>
                        <li>
                            Участником акции могут стать лица, соответствующие следующим условиям:
                            <ul>
                                <li>К&nbsp;участию в&nbsp;акции допускаются только проживающие на&nbsp;территории проведения акции (п.&nbsp;2.2. правил) лица в&nbsp;возрасте от&nbsp;18&nbsp;лет.</li>
                                <li>К&nbsp;участию в&nbsp;акции не&nbsp;допускаются: лица, указавшие некорректную, неточную, недостоверную информацию.</li>
                                <li>Факт участия в&nbsp;акции подразумевает ознакомление и&nbsp;полное согласие клиента с&nbsp;настоящими правилами. Клиент соглашается на&nbsp;обработку его/ее персональных данных организатором и/или уполномоченными им&nbsp;лицами в&nbsp;целях акции. Персональные данные предоставляются на&nbsp;добровольной основе, однако несогласие на&nbsp;обработку таких данных делает невозможным участие в&nbsp;акции.</li>
                            </ul>
                        </li>
                    </ol>
                </li>
                <li>
                    Права и обязанности клиента
                    <ol>
                        <li>Клиент имеет право:
                            <ul>
                                <li>Ознакомиться правилами акции.</li>
                                <li>Принимать участие в&nbsp;акции в&nbsp;порядке, определенном настоящими правилами, получать информацию об&nbsp;изменениях в&nbsp;правилах.</li>
                                <li>Требовать от&nbsp;организатора акции получения информации об&nbsp;акции в&nbsp;соответствии с&nbsp;правилами акции.</li>
                                <li>На&nbsp;получение сертификата при заключении клиентом с&nbsp;организатором договора на&nbsp;строительство бани, дома в&nbsp;период с&nbsp;01.11.2017&nbsp;г.&nbsp;до&nbsp;01.12.2017&nbsp;г.</li>
                                <li>Выполнять все действия, связанные с&nbsp;участием в&nbsp;акции и&nbsp;получением сертификата в&nbsp;соответствии с&nbsp;настоящими правилами акции.</li>
                            </ul>
                        </li>
                    </ol>
                </li>
                <li>
                    Права и обязанности организатора
                    <ol>
                        <li>Организатор имеет право:
                            <ul>
                                <li>Отказать в&nbsp;участии в&nbsp;акции клиенту, не&nbsp;соблюдающим настоящие правила.</li>
                                <li>Отказать в&nbsp;выдаче сертификата клиенту, не&nbsp;получившему сертификат в&nbsp;срок, указанный организатором, по&nbsp;вине клиента.</li>
                                <li>Передать невостребованные сертификаты партнеру.</li>
                                <li>Вносить изменения в&nbsp;правила акции с&nbsp;обязательной публикацией изменений по&nbsp;интернет-адресу <a href="https://www.terem-pro.ru/">https://www.terem-pro.ru/</a>.</li>
                                <li>Организатор оставляет за&nbsp;собой право не&nbsp;вступать в&nbsp;письменные переговоры либо иные контакты с&nbsp;клиентами акции кроме как в&nbsp;случаях, указанных в&nbsp;настоящих правилах или на&nbsp;основании требований действующего законодательства Российской Федерации.</li>
                                <li>Организатор также имеет и&nbsp;иные права, предусмотренные настоящими правилами. </li>
                                <li>Организатор оставляет за&nbsp;собой право отказать клиенту в&nbsp;выдаче сертификата на&nbsp;свое усмотрение. Клиент имеет право обратиться к&nbsp;организатору за&nbsp;пояснениями причин отказа</li>
                            </ul>
                        </li>
                        <li>Организатор обязуется:
                            <ul>
                                <li>Провести Акцию в&nbsp;порядке, определенном настоящими правилами.</li>
                                <li>Выдать сертификаты клиенту в&nbsp;соответствии с&nbsp;условиями настоящих правил.</li>
                            </ul>
                        </li>
                    </ol>
                </li>
                <li>
                    Порядок и сроки получения сертификата
                    <ol>
                        <li>Клиент в&nbsp;соответствии с&nbsp;п.&nbsp;3. правил акции получает от&nbsp;организатора акции сертификат в&nbsp;офисе компании, которая располагается по&nbsp;адресу: г. Москва, ул. Зеленодольская вл. 42, выставочная площадка &laquo;Теремъ&raquo;.</li>
                        <li>Клиент обязан заполнить и&nbsp;подписать &laquo;акт приёма-передачи подарочного сертификата&raquo; (в&nbsp;двух экземплярах), согласие на&nbsp;обработку персональных данных.</li>
                        <li>Сертификат не&nbsp;выдается при несоблюдении клиентом настоящих правил.</li>
                    </ol>
                </li>
                <li>
                    Дополнительные условия
                    <ol>
                        <li>Организатор не&nbsp;несет ответственности&nbsp;за:
                            <ul>
                                <li>несоблюдение, несвоевременное выполнение клиентом настоящих правил;</li>
                                <li>получение от&nbsp;клиента неполных, некорректных сведений, необходимых для участия в&nbsp;акции и&nbsp;получения сертификата.</li>
                            </ul>
                        </li>
                        <li>Персональные данные, полученные от&nbsp;клиентов в&nbsp;соответствии с&nbsp;данными правилами, хранятся в&nbsp;базе данных, администрированием которой занимается организатор акции. Персональные данные используются и&nbsp;хранятся в&nbsp;целях проведения акции.</li>
                        <li>Данные правила являются единственными официальными правилами участия в&nbsp;акции. В&nbsp;случае возникновения ситуаций, допускающих неоднозначное толкование этих правил, и/или вопросов, не&nbsp;урегулированных этими правилами, окончательное решение о&nbsp;таком толковании и/или разъяснения принимается непосредственно и&nbsp;исключительно организатором акции.</li>
                        <li>Термины, употребляемые в&nbsp;настоящих правилах, относятся исключительно к&nbsp;настоящей акции.</li>
                        <li>Все спорные вопросы, касающиеся настоящей акции, регулируются на&nbsp;основе действующего законодательства&nbsp;РФ.</li>
                    </ol>
                </li>
            </ol>
        </div>
    </div>
</div>

<!-- Modal house -->
<div id="modal-house" class="modal" role="dialog">

</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter937330 = new Ya.Metrika({
                    id: 937330,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    ut: "noindex"
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
<div><img src="//mc.yandex.ru/watch/937330?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->
