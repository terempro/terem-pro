<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Забор из евроштакетника под ключ - проекты и цены");
$APPLICATION->SetPageProperty("keywords", "забор из евроштакетника");
$APPLICATION->SetTitle("Забор из евроштакетника");
/*$APPLICATION->AddHeadScript("/bitrix/templates/.default/assets/js/fense.js");*/
?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                   Забор из евроштакетника
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                           <img src="img/item-1.jpg" class="full-width" alt="">

                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <p>Евроштакетник представляет собой металлические полосы шириной около 12 см и длиной либо 2, либо 1,8 метра. При его производстве используются оцинкованные стальные листы, покрытые слоем полимера, обеспечивающего защиту от воздействий внешней среды. Преимущество данного материала в том, что он не создает подобие глухой стены. Заграждение может сохранять частичный обзор и не препятствовать проникновению солнечных лучей на огороженный участок.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <h3 style="color: #a3372b;" class="text-uppercase">почему евроштакетник выгодно купить у нас</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <p>За максимально короткие сроки специалисты нашей компании качественно выполнят работы по установке заграждения из евроштакетника на вашем участке. Многолетний опыт и серьёзный подход к делу позволяют нам с уверенностью утвержать: мы работаем с подобными задачами на самом высоком уровне. Именно поэтому вам не придётся тратить свои силы и средства на последующие ремонты.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <h3 style="color: #a3372b;" class="text-uppercase">как купить евроштакетник у нас</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <p>Для того, чтобы сделать заказ, вы можете обратиться к нам на сайт в раздел «ОГРАЖДЕНИЯ», позвонив по телефону + 7 (495) 721-18-00 или нажав на кнопку «ОТПРАВИТЬ ЗАЯВКУ», которую Вы найдете чуть ниже.</p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                        <div class="my-table">
                                            <div>
                                                <p>Область применения евроштакетника</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq1" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs )">
                                        <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                            <ul>
                                                                <li>
                                                                    <p>Заборы из металлического штакетника устанавливаются на дачных территориях, в частных домах за городом, на любых охраняемых территориях.</p>
                                                                </li>
                                                                <li>
                                                                    <p>Базы отдыха или лагеря для детей также часто ограждены современными и красивыми металлическими заборами, которые лучшим образом совмещают в себе качество и эстетичность.</p>
                                                                </li>
                                                                <li>
                                                                    <p>Огораживание участков земли, спортивных площадок, для территориального выделения школ и детских садов.</p>
                                                                </li>
                                                                <li>
                                                                    <p>Отлично подходит для промышленных и коммерческих зон, с целью ограждения территорий.</p>
                                                                </li>
                                                            </ul>
                                                            <p>Конечно, это лишь небольшая часть возможного применения штакетника, так как все ограничивается фантазией пользователя.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
                                        <div class="my-table">
                                            <div>
                                                <p>Установка ограждения из евроштакетника</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq2" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs )">
                                         <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                            <img src="img/item-2.jpg" class="full-width" alt="">
                                                            <br><br>
                                                                <p>Решив установить забор, вы можете сами определить, будут штакетины с одной стороны забора или с двух. Стоит учитывать тот факт, что если устанавливать штакетины в шахматном порядке, то можно смело рассчитывать на то, что все что за забором, будет скрыто от посторонних глаз. При однорядном размещении штакетин рекомендуется оставлять между ними от 20 до 60 мм. А при установке в шахматном порядке расстояние между штакетин может быть от 40 до 80мм.</p>
                                                                <p>Так, как каждый клиент хочет, чтобы его забор и цветовое решение было уникальным, мы предлагаем вам возможность выбрать один из представленных в каталоге цветов по таблице RAL. Все это можно обсудить с консультантом.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq3">
                                        <div class="my-table">
                                            <div>
                                                <p>Фото наших заборов из евроштакетника</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq3" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs)">
                                         <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <img src="img/item-3.jpg" class="full-width" alt="">
                                                            <p style="font-size: 14.5px;"><i><small>Евроштакетник с шагом в 50 мм.</small></i></p>
                                                        </div>
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <img src="img/item-4.jpg" class="full-width" alt="">
                                                            <p style="font-size: 14.5px;"><i><small>Евроштакетник собранный в шахматном порядке на фундаменте</small></i></p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                                                            <a href="#" class="btn btn-default text-uppercase fensue-btn-works">наши работы</a>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <br><br><br>
                    <div class="row">
                        <div class="col-xs-12 col-md-2 col-sm-3">
                            <a href="/services/enginering_communications/fensecalc/" class="btn btn-default text-uppercase fensue-btn-works no-padding full-width redBtn">Отправить заявку</a>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3 col-md-offset-1 col-sm-offset-1">
                            <button type="button" href="#" class="btn btn-primary fensue-call" data-toggle="modal" data-target="#formAdviceItem" data-price="" data-whatever=""><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>