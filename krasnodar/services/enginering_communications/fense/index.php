<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "терем заборы, терем ограждения, заборы, ограждения");
$APPLICATION->SetPageProperty("title", "Ограждения от компании Теремъ, заборы из сетки-рабицы, профнастила и кирпича");
$APPLICATION->SetTitle("Ограждения от компании Теремъ, заборы из сетки-рабицы, профнастила и кирпича");
?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    Ограждения
                                </h1>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>Неприкосновенность частной жизни и личная безопасность – вот то, к чему стремится каждый человек в наше время. В этом может помочь качественное и надежное ограждение собственного участка.</p>
                            <p>Специально для своих клиентов компания «Теремъ» подготовила выгодное ценовое предложение по установке качественного и надежного ограждения участка.</p>
                            <!--<p>НОВИНКА! 3D забор – всего от 1 100 руб. /п.м. Идеально подходит для ограждения участка со стороны соседей, не создавая ощущение замкнутого пространства, но обеспечивая надежную защиту дому. Образец данного ограждения представлен на площадке, и Вы сможете лично убедиться в его преимуществах!</p>-->
                        </div>
                    </div>
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#col1" aria-expanded="false" aria-controls="col1">
                                        <div class="my-table">
                                            <div>
                                                <p><strong>Устройство и порядок монтажа забора из профнастила</strong></p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="col1">
                                        <div class="( my-collapse-content + design-main + no-padding )">
                                            <p>На предварительно подготовленном участке бурится отверстие диаметром 150 мм и глубиной 500 мм, затем в почву вбивается столб на глубину от 1000 до 1200 мм (глубина зависит от высоты ограждения). Столбы устанавливаются на расстоянии около 2500 мм друг от друга. Профильные трубы привариваются электросваркой к столбам (строго горизонтально) в несколько рядов (в зависимости от высоты ограждения). На получившийся каркас с помощью кровельных саморезов крепится профнастил. Цвет саморезов соответствует цвету забора (Дополнительная услуга).</p>
                                            <img src="/bitrix/templates/.default/assets/img/services/item-21.jpg" class="full-width" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12  + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#col2" aria-expanded="false" aria-controls="col2">
                                        <div class="my-table">
                                            <div>
                                                <p><strong> Устройство и порядок монтажа забора из сетки-рабицы </strong></p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="col2">
                                        <div class="( my-collapse-content + design-main + no-padding )">
                                            <p>На предварительно подготовленном участке бурится отверстие диаметром 150 мм и глубиной 500 мм, затем в почву вбивается столб на глубину от 1000 до 1200 мм (глубина зависит от высоты ограждения). Столбы устанавливаются на расстоянии около 2500 мм друг от друга. Арматура диаметром 6 мм приваривается электросваркой к столбам (строго горизонтально) в два ряда. На получившийся каркас крепится сетка-рабица.</p>
                                            <img src="/bitrix/templates/.default/assets/img/services/item-22.jpg" class="full-width" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12  + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#col3" aria-expanded="false" aria-controls="col3">
                                        <div class="my-table">
                                            <div>
                                                <p><strong> Устройство монолитной и кирпичной ленты со столбом из кирпича </strong></p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="col3">
                                        <div class="( my-collapse-content + design-main + no-padding )">
                                            <img src="/bitrix/templates/.default/assets/img/services/item-23.jpg" class="full-width" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--one item END-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h2>
                                    Бесплатная консультация
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <form class="form-fense" data-toggle="validator" action="https://crm.terem-pro.ru/index.php/welcome/postGiveHouse/" method="post" role="form" data-form="send">
                                <div class="row">
                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label for="">Тип забора</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <select name="zabor" id="zabor" class="my-input form-control" style="-webkit-appearance: none;">
                                                        <option value="Забор из профнастила" selected="">Забор из профнастила</option>
                                                        <option value="Забор из рабицы">Забор из рабицы</option>
                                                        <option value="Забор евроштакетник">Забор евроштакетник</option>
                                                        <option value="Забор «3D»">Забор «3D»</option>
                                                    </select>
                                                    <input type="hidden" name="id_request" value="zabor1"/>
                                                    <input type="hidden" name="ipadress" class="" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>"/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label for="">Количество ворот</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                        <option value="Без ворот" selected="">Без ворот</option>
                                                        <option value="1">1 шт.</option>
                                                        <option value="2">2 шт.</option>
                                                        <option value="3">3 шт.</option>
                                                        <option value="4">4 шт.</option>
                                                        <option value="5">5 шт.</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label for=""> Расстояние до участка</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="number" min="0" max="10000" class="my-input form-control" name="km" id="km" placeholder="6000 км" autocomplete="off">
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label for="">Общая длина забора</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="number" min="0" max="10000" class="my-input form-control" name="dl" id="dl" placeholder="6000 метров" autocomplete="off">
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>ФИО</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="user" class="form-control" placeholder="Иван Иванов" required/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Телефон</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="+7 (999) 999 99 99" name="phone" autocomplete="off" id="phone">
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Почта</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="email" name="mail" class="form-control" placeholder="Email" required/>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label class="op-0">Конпка</label>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                    <button type="submit" class="btn btn-danger" value="block-1" name="from">Отправить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <p><small>Заявка не накладывает никаких обязательств и является только запросом на получение информации</small></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<?if ($USER->IsAdmin()):?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    Ограждения
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>Неприкосновенность частной жизни и личная безопасность – вот то, к чему стремится каждый человек в наше время. В этом может помочь качественное и надежное ограждение собственного участка.</p>
                            <p>Специально для своих клиентов компания «Теремъ» подготовила выгодное ценовое предложение по установке качественного и надежного ограждения участка.</p>
                            <!--<p>НОВИНКА! 3D забор – всего от 1 100 руб. /п.м. Идеально подходит для ограждения участка со стороны соседей, не создавая ощущение замкнутого пространства, но обеспечивая надежную защиту дому. Образец данного ограждения представлен на площадке, и Вы сможете лично убедиться в его преимуществах!</p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12" id="category">
                <div class="flex-reversed">
                    <!--OLD catalog list-->
                    <!--small villages -->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/catalog-list/item-1.jpg?1446542515111836" alt="беседки">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        Малые строения, беседки
                                    </h4>
                                    <p>
                                       Недорогие беседки из клееного бруса, беседки-барбекю с внешней и внутренней отделкой, и каркасные гаражи с рольворатами.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/catalog-list/item-2.jpg?1446542515113187" alt="Бани">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        Бани
                                    </h4>
                                    <p>
                                       Традиционные бани из клееного бруса или каркаса с удобными парными, предбанниками, комнатами для отдыха и душевыми. Строятся за 15-25 дней, включая внутреннюю отделку, и не требуют внешней отделки.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                    </div><!--end-->
                     <!--small villages -->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/catalog-list/item-1.jpg?1446542515111836" alt="беседки">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        Малые строения, беседки
                                    </h4>
                                    <p>
                                       Недорогие беседки из клееного бруса, беседки-барбекю с внешней и внутренней отделкой, и каркасные гаражи с рольворатами.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/catalog-list/item-2.jpg?1446542515113187" alt="Бани">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        Бани
                                    </h4>
                                    <p>
                                       Традиционные бани из клееного бруса или каркаса с удобными парными, предбанниками, комнатами для отдыха и душевыми. Строятся за 15-25 дней, включая внутреннюю отделку, и не требуют внешней отделки.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                    </div><!--end-->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12 my-margin">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-red">
                                <h2>
                                    ПРЕИМУЩЕСТВА ЗАКАЗА в «ТЕРЕМЕ»
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Бесплатная доставка 250 км от базы
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Работа без предоплаты на стандартный профиль С8
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Бесплатная консультация и составление тех. задания
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Быстрый и легкий монтаж и демонтаж  старого забора
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p class="text-padding-bottom">Вам не обязательно приезжать к нам в офис для того, чтобы заказать ограждение, достаточно просто позвонить по телефону:<br>+7 (495) 215-29-55 или воспользоваться формой заказа: </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content text-page white " role="content">
    <div class="container my-margin">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-left && bg-grey && bg-tab-content-white && padding-40 && strech-line ))">
                            <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" aria-expanded="true">
                               Профнастил
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" aria-expanded="false">
                                Евроштакетник
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" aria-expanded="false">
                                3D-забор
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab" aria-expanded="false">
                                Сетка-рабица
                            </a>
                        </li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab1">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <form class="form-fense new" data-toggle="validator" action="http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/" method="post" role="form" data-form="send">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Толщина, мм</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="zabor" id="zabor" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Забор из профнастила" selected="">0,4</option>
                                                                            <option value="Забор из рабицы">0,4</option>
                                                                            <option value="Забор евроштакетник">0,4</option>
                                                                            <option value="Забор «3D»">0,4</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" name="id_request" value="zabor1"/>
                                                                    <input type="hidden" name="ipadress" class="" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Высота, мм</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">1800</option>
                                                                            <option value="1">1800.</option>
                                                                            <option value="2">1800</option>
                                                                            <option value="3">1800</option>
                                                                            <option value="4">1800</option>
                                                                            <option value="5">1800</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Цвет</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">красный</option>
                                                                            <option value="1">красный</option>
                                                                            <option value="2">красный</option>
                                                                            <option value="3">красный</option>
                                                                            <option value="4">красный</option>
                                                                            <option value="5">красный</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Окрашивание</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">С одной стороны</option>
                                                                            <option value="1">С одной стороны</option>
                                                                            <option value="2">С одной стороны</option>
                                                                            <option value="3">С одной стороны</option>
                                                                            <option value="4">С одной стороны</option>
                                                                            <option value="5">С одной стороны</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Ворота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">без ворот</option>
                                                                            <option value="1">без ворот</option>
                                                                            <option value="2">без ворот</option>
                                                                            <option value="3">без ворот</option>
                                                                            <option value="4">без ворот</option>
                                                                            <option value="5">без ворот</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Ширина ворот</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select disabled">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected=""></option>
                                                                            <option value="1">без ворот</option>
                                                                            <option value="2">без ворот</option>
                                                                            <option value="3">без ворот</option>
                                                                            <option value="4">без ворот</option>
                                                                            <option value="5">без ворот</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Калитка</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">1 шт.</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">1 шт.</option>
                                                                            <option value="3">1 шт.</option>
                                                                            <option value="4">1 шт.</option>
                                                                            <option value="5">1 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Расстояние до участка, км*</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">150</option>
                                                                            <option value="1">150</option>
                                                                            <option value="2">150</option>
                                                                            <option value="3">150</option>
                                                                            <option value="4">150</option>
                                                                            <option value="5">150</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Общая длина забора, м</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>ФИО</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="" class="form-control" placeholder="Игор"  autocomplete="off">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Телефон</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="tel" name="" class="form-control" placeholder="+7 (999) 000 00 00" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Почта</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="email" name="mail" class="form-control" placeholder="Email" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <div class="fense-all-calc">
                                                    <div>
                                                        <div>Итого:</div>
                                                        <div>0 000 000 руб.</div>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <div><button data-item="fense-calc" type="button" class="btn btn-default text-red text-uppercase">расчитать</button></div>
                                                            <div><button data-item="fense-send" type="submit" class="btn btn-default text-red text-uppercase">Отправить заявку</button></div>
                                                        </div>
                                                        <div>
                                                            <p class="form-error text-red">Заполните все поля!</p>
                                                            <p>Заявка не накладывает на Вас никаких обязательств и является только запросом на получение информации</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p class="form-attantion">*Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. Стоимость доставки – ?? руб./км. Если Ваш участок находится менее 100 км от производственной базы, доставка бесплатно.</p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab2">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <form class="form-fense new" data-toggle="validator" action="http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/" method="post" role="form" data-form="send">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Шаг между секциями</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="zabor" id="zabor" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Забор из профнастила" selected="">2 см</option>
                                                                            <option value="Забор из рабицы">2 см</option>
                                                                            <option value="Забор евроштакетник">2 см</option>
                                                                            <option value="Забор «3D»">2 см</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" name="id_request" value="zabor1"/>
                                                                    <input type="hidden" name="ipadress" class="" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Высота, мм</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">1800</option>
                                                                            <option value="1">1800</option>
                                                                            <option value="2">1800</option>
                                                                            <option value="3">1800</option>
                                                                            <option value="4">1800</option>
                                                                            <option value="5">1800</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Цвет</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">красный</option>
                                                                            <option value="1">красный</option>
                                                                            <option value="2">красный</option>
                                                                            <option value="3">красный</option>
                                                                            <option value="4">красный</option>
                                                                            <option value="5">красный</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Окрашивание</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">С одной стороны</option>
                                                                            <option value="1">С одной стороны</option>
                                                                            <option value="2">С одной стороны</option>
                                                                            <option value="3">С одной стороны</option>
                                                                            <option value="4">С одной стороны</option>
                                                                            <option value="5">С одной стороны</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Ворота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">без ворот</option>
                                                                            <option value="1">без ворот</option>
                                                                            <option value="2">без ворот</option>
                                                                            <option value="3">без ворот</option>
                                                                            <option value="4">без ворот</option>
                                                                            <option value="5">без ворот</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Калитка</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">1 шт.</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">1 шт.</option>
                                                                            <option value="3">1 шт.</option>
                                                                            <option value="4">1 шт.</option>
                                                                            <option value="5">1 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Расстояние до участка, км*</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">150</option>
                                                                            <option value="1">150</option>
                                                                            <option value="2">150</option>
                                                                            <option value="3">150</option>
                                                                            <option value="4">150</option>
                                                                            <option value="5">150</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Общая длина забора, м</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">200000</option>
                                                                            <option value="1">200000</option>
                                                                            <option value="2">200000</option>
                                                                            <option value="3">200000</option>
                                                                            <option value="4">200000</option>
                                                                            <option value="5">200000</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    
                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>ФИО</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="" class="form-control" placeholder="Игор"  autocomplete="off">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Телефон</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="tel" name="" class="form-control" placeholder="+7 (999) 000 00 00" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Почта</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="email" name="mail" class="form-control" placeholder="Email" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <div class="fense-all-calc">
                                                    <div>
                                                        <div>Итого:</div>
                                                        <div>0 000 000 руб.</div>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <div><button data-item="fense-calc" type="button" class="btn btn-default text-red text-uppercase">расчитать</button></div>
                                                            <div><button data-item="fense-send" type="submit" class="btn btn-default text-red text-uppercase">Отправить заявку</button></div>
                                                        </div>
                                                        <div>
                                                            <p class="form-error text-red">Заполните все поля!</p>
                                                            <p>Заявка не накладывает на Вас никаких обязательств и является только запросом на получение информации</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <p class="form-attantion">*Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. Стоимость доставки – ?? руб./км. Если Ваш участок находится менее 100 км от производственной базы, доставка бесплатно.</p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab3">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <form class="form-fense new" data-toggle="validator" action="http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/" method="post" role="form" data-form="send">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-2 col-sm-2">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Высота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="zabor" id="zabor" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Забор из профнастила" selected="">2500</option>
                                                                            <option value="Забор из рабицы">2500</option>
                                                                            <option value="Забор евроштакетник">2500</option>
                                                                            <option value="Забор «3D»">2500</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" name="id_request" value="zabor1"/>
                                                                    <input type="hidden" name="ipadress" class="" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Цвет</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">коричневый</option>
                                                                            <option value="1">коричневый</option>
                                                                            <option value="2">коричневый</option>
                                                                            <option value="3">коричневый</option>
                                                                            <option value="4">коричневый</option>
                                                                            <option value="5">коричневый</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-2 col-sm-2">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Ворота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">3 шт.</option>
                                                                            <option value="1">3 шт.</option>
                                                                            <option value="2">3 шт.</option>
                                                                            <option value="3">3 шт.</option>
                                                                            <option value="4">3 шт.</option>
                                                                            <option value="5">3 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-2 col-sm-2">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Калитка</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">2 шт.</option>
                                                                            <option value="1">2 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">2 шт.</option>
                                                                            <option value="4">2 шт.</option>
                                                                            <option value="5">2 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Расстояние до участка, км*</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">150</option>
                                                                            <option value="1">150</option>
                                                                            <option value="2">150</option>
                                                                            <option value="3">150</option>
                                                                            <option value="4">150</option>
                                                                            <option value="5">150</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Общая длина забора, м</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>ФИО</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="" class="form-control" placeholder="Игор" autocomplete="off" id="phone">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Телефон</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="tel" name="" class="form-control" placeholder="+7 (999) 000 00 00" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Почта</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="email" name="mail" class="form-control" placeholder="Email" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <p class="form-attantion">*Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. Стоимость доставки – ?? руб./км. Если Ваш участок находится менее 100 км от производственной базы, доставка бесплатно.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <div class="fense-all-calc">
                                                    <div>
                                                        <div>Итого:</div>
                                                        <div>0 000 000 руб.</div>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <div><button data-item="fense-calc" type="button" class="btn btn-default text-red text-uppercase">расчитать</button></div>
                                                            <div><button data-item="fense-send" type="submit" class="btn btn-default text-red text-uppercase">Отправить заявку</button></div>
                                                        </div>
                                                        <div>
                                                            <p class="form-error text-red">Заполните все поля!</p>
                                                            <p>Заявка не накладывает на Вас никаких обязательств и является только запросом на получение информации</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab4">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <form class="form-fense new" data-toggle="validator" action="http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/" method="post" role="form" data-form="send">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8 col-sm-8">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Высота, мм</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="zabor" id="zabor" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Забор из профнастила" selected="">1500</option>
                                                                            <option value="Забор из рабицы">1500</option>
                                                                            <option value="Забор евроштакетник">1500</option>
                                                                            <option value="Забор «3D»">1500</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" name="id_request" value="zabor1"/>
                                                                    <input type="hidden" name="ipadress" class="" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>"/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Ворота</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">2 шт.</option>
                                                                            <option value="1">2 шт.</option>
                                                                            <option value="2">2 шт.</option>
                                                                            <option value="3">2 шт.</option>
                                                                            <option value="4">2 шт.</option>
                                                                            <option value="5">2 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Калитока</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">1 шт.</option>
                                                                            <option value="1">1 шт.</option>
                                                                            <option value="2">1 шт.</option>
                                                                            <option value="3">1 шт.</option>
                                                                            <option value="4">1 шт.</option>
                                                                            <option value="5">1 шт.</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label for="">Расстояние до участка, км*</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="fensue-select">
                                                                        <select name="vorota" id="vorota" class="my-input form-control" style="-webkit-appearance: none;">
                                                                            <option value="Без ворот" selected="">70</option>
                                                                            <option value="1">70</option>
                                                                            <option value="2">70</option>
                                                                            <option value="3">70</option>
                                                                            <option value="4">70</option>
                                                                            <option value="5">70</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Общая длина забора, м</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="" class="form-control" placeholder="200" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>ФИО</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="text" name="" class="form-control" placeholder="Игор"  autocomplete="off">
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Телефон</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="tel" name="" class="form-control" placeholder="+7 (999) 000 00 00" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <label>Почта</label>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <input type="email" name="mail" class="form-control" placeholder="Email" required/>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <p class="form-attantion">*Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. Стоимость доставки – ?? руб./км. Если Ваш участок находится менее 100 км от производственной базы, доставка бесплатно.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                <div class="fense-all-calc">
                                                    <div>
                                                        <div>Итого:</div>
                                                        <div>0 000 000 руб.</div>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <div><button data-item="fense-calc" type="button" class="btn btn-default text-red text-uppercase">расчитать</button></div>
                                                            <div><button data-item="fense-send" type="submit" class="btn btn-default text-red text-uppercase">Отправить заявку</button></div>
                                                        </div>
                                                        <div>
                                                            <p class="form-error text-red">Заполните все поля!</p>
                                                            <p>Заявка не накладывает на Вас никаких обязательств и является только запросом на получение информации</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>