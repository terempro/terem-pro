<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "укладка печи, печи терем, устройство печи");
$APPLICATION->SetPageProperty("title", "Компания ТеремЪ - продажа и установка кирпичных печей");
$APPLICATION->SetTitle("Компания ТеремЪ - продажа и установка кирпичных печей");
?>
<section class="content text-page white" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side + my-margin clearfix">
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<div class="text-center text-uppercase">
								<h1>
									Устройство кирпичной печи в загородных домах
								</h1>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p>
								В настоящее время все больше людей стремятся оборудовать свои загородные коттеджи кирпичной печью. Это и качественная система отопления, и яркий элемент интерьера, способный создать особую атмосферу в доме. 
							</p>
							<br>
							<p>
								Компания «Теремъ» специально по просьбам клиентов вводит новую услугу – устройство кирпичной печи «под ключ». Наши специалисты оборудуют Ваш дом согласно всем действующим нормам и правилам. Мы работаем с загородными домами всех видов и размеров.
							</p>
							<br>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-4 col-sm-4">
							<img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/item-1.jpg" class="full-width" alt="">
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							<img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/item-2.jpg" class="full-width" alt="">
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							<img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/item-3.jpg" class="full-width" alt="">
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p><strong>Преимущества современной кирпичной печи:</strong></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-6">
							<div class="item">
								<p class="text-uppercase with-dots txt-width-left-padding">Автономность</p>
								<p class="txt-width-left-padding">Печь никак не связана с работой коммунальных служб и качеством подачи электричества. Это гарантирует бесперебойную работу, а значит, в Вашем доме всегда будет тепло и уютно.</p>
			       			</div>
						</div>
						<div class="col-xs-12 col-md-6 col-sm-6">
							<div class="item">
						        <p class="text-uppercase with-dots txt-width-left-padding">Доступность и удобство </p>
						        <p class="txt-width-left-padding">Современное печное отопление является финансово доступным и неприхотливым в процессе эксплуатации. К тому же печь долго остывает и ее можно использовать в помещениях любой площади.</p>
			       			</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-6">
							<div class="item">
						        <p class="text-uppercase with-dots txt-width-left-padding">Регулировка уровня тепла</p>
						        <p class="txt-width-left-padding">С помощью печи Вы сможете поддерживать в своем доме желаемый уровень тепла и даже влажности. Это создаст комфортную среду для Вас, а также поможет продлить срок службы дома.</p>
			       			</div>
						</div>
						<div class="col-xs-12 col-md-6 col-sm-6">
							<div class="item">
						        <p class="text-uppercase with-dots txt-width-left-padding">Естественная вентиляция</p>
						        <p class="txt-width-left-padding">Конфигурация печи спроектирована таким образом, что она выполняет функцию естественной вентиляции помещений.</p>
			       			</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p>Очень важно определиться с местом размещения печи. Лучше, если ее установка будет задумана еще до начала строительства дома. Но если Вы решили обзавестись печью уже в готовом доме, наши специалисты справятся и с этой задачей.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<div class="title-1">
								<span><img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/icon-1.jpg"></span>
								 <span>При установке печи очень важно соблюдать правила пожарной безопасности:</span> 
						 	</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p class="trigger-pechi"><img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/trigger.png">Труба дымохода должна располагаться между стропилами, также не должна проходить через конек или ендову.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p class="trigger-pechi"><img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/trigger.png">Для печи необходимо использовать огнеупорный кирпич.</p>
						</div>
					</div>
					<br>
					<!--one item-->
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
							<div class="row">
								<div class="col-xs-12 col-md-12 col-sm-12">
									<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq3">
										<div class="my-table">
											<div>
												<p><strong>Схема прохода печи и печного дымохода через пол, перекрытие, крышу</strong></p>
											</div>
											<div>
												<i class="glyphicon glyphicon-menu-down"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-md-12 col-sm-12">
									<div class="collapse my-collapse" id="faq3">
										<div class="( my-collapse-content + design-main )">
											<img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/item-inner.png" class="full-width" alt="">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--one item END-->
					<br>
					<br>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p>Компания «Теремъ» при установке кирпичных печей применяет традиционные и современные технологии строительства.</p>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p><strong>Устройство кирпичной печи в компании «Теремъ» – это:</strong></p>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-xs-12 col-md-3 col-sm-3">
							<div class="item">
								<img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/icon-2.png">
								<br><br>
								<p>Многолетний опыт работы в сфере загородного строительства</p>
							</div>
						</div>
						<div class="col-xs-12 col-md-3 col-sm-3">
							<div class="item">
								<img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/icon-2.png">
								<br><br>
								<p>Соблюдение всех норм и требований пожарной безопасности</p>
							</div>
						</div>
						<div class="col-xs-12 col-md-3 col-sm-3">
							<div class="item">
								<img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/icon-2.png">
								<br><br>
								<p>Использование надежного печного оборудования</p>
							</div>
						</div>
						<div class="col-xs-12 col-md-3 col-sm-3">
							<div class="item">
								<img src="/bitrix/templates/.default/assets/img/about/kirpichnie_pechi/icon-2.png">
								<br><br>
								<p>Гарантия качества выполненных работ</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>