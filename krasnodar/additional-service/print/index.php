<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    if (!$_POST['house_name']){
	LocalRedirect('/404.php');
    }
    
    $printArr = array();
    foreach($_POST as $key=>$el){
	$printArr[$key] = urldecode($el);
    }
    
    $total = $printArr['house_base_price'];
?>
<table>
    <tr>
	<td>
	    <img src="/bitrix/templates/terem/img/logo.png" title="" alt="">
	</td>
	<td>
	    тел. +7 (495) 721-18-00
	</td>
    </tr>
    <tr>
	<td>
	    <h1>Дополнительные услуги</h1>
	</td>
    </tr>
    <tr>
	<td style="padding-bottom: 20px;">
	    <strong>Название дома: <?=$printArr['house_name']?> </strong>
	</td>
	<td style="padding-bottom: 20px;">
	   <strong> Базовая цена: <?=$total?> руб. </strong>
	</td>
    </tr>
    <tr>
	 <br><br><br>
    </tr>
    
	<? $services = json_decode($printArr['services']);
	   $additional_summ = 0; 
	    if (!empty($services)):
		foreach($services as $s):?>
		<tr>
		    <td style="max-width: 500px;"><?=$s->service?></td>
		    <td><?=$s->price?> руб.</td>
		</tr>
		<?
		$additional_summ = $additional_summ + $s->price;
		endforeach;
		$total = $total + $additional_summ;
		?>
		
		<tr>
		    <td style="padding-top: 20px;">Стоимость дополнительных услуг:</td>
		    <td style="padding-top: 20px;"><?=$additional_summ;?> руб.</td>
		</tr>
	    <? endif; ?>
	    
    <tr>
	<td><strong>Общая стоимость:</strong></td>
	<td><strong><?=$total?> руб.</strong></td>
    </tr>
	    
    
</table>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>