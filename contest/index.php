<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("«Баня в подарок» – выигрывайте вместе с Теремъ!");
?>
<script type="text/javascript" scr="/bitrix/templates/.default/assets/js/loadphoto.js"></script>
<section class="content white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix Warranty">

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <h1 class="text-uppercase text-center">
                                <span>«Баня в подарок»</span> – выигрывайте вместе с Теремъ!
                            </h1>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <img src="1234.jpg" alt=""/ class="full-width">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <form id="form-contest" action="/include/contest.php?data" method="post" class="form-send" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label for="">Ваше ФИО</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="fio" class="form-control my-input" placeholder="Ваше ФИО" required="">
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label for="">Номер договора</label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <input type="text" name="contract" class="form-control my-input" placeholder="ХХХХХХХХ" required="">
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label for="">&nbsp;&nbsp;</label>
                                            </div>
                                            <div class="col-xs-12">
                                                <button id="contest-button" type="submit" class="btn btn-danger text-uppercase contest-form-button" style="pointer-events: all; cursor: pointer; width:100%;" <?php if (isset($_SESSION['CONTEST_SENT']) || isset($_SESSION['CONTEST_FAILED'])): ?> disabled <?php endif; ?>>Отправить</button>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-9 col-sm-9">
                                                    <div class="row">
                                                        <div class="col-xs-12" id="phone-label">
                                                            <label class="contest-phone-label" for=""></label>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="flex-container">
                                                                <div class="code-sender" id="code-sender">
                                                                    <input type="tel" name="phone" data-item="phone" class="form-control my-input" placeholder="+7 XXX XXX XX XX">
                                                                    <button type="button" class="btn btn-danger text-uppercase contest-form-button" id="contest-get-code">Получить код</button>
                                                                </div>
                                                                <div class="tel-confirmation" id="tel-confirmation">
                                                                    <div class="code-block">
                                                                        <input type="text" name="code" class="form-control my-input" placeholder="XXXX">
                                                                        <button type="button" id="contest-send-code" class="btn btn-danger text-uppercase contest-form-button">Подтвердить</button>
                                                                    </div>
                                                                </div>
                                                                <div class="system-msg" id="contest-msg">
                                                                    <?php if (isset($_SESSION['CONTEST_SENT'])): ?>
                                                                        Вы уже отправили заявку на участие в конкурсе. Спасибо!
                                                                    <?php elseif (isset($_SESSION['CONTEST_FAILED'])): ?>
                                                                        Сожалеем, но Вы использовали 3 попытки ввода кода. Попробуйте отправить заявку позже.
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <p style="padding-top: 3px;">* Отправляя заявку, Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">политикой конфиденциальности и правилами пользования сайтом</a> и даете <a href="/privacy-policy/" target="_blank">согласие  на использование загруженных изображений</a>.</p>
                                        </div>
                                        <div class="col-xs-12 col-md-6 text-right">
                                            <span id="response-message" class="respond-message"></span>&nbsp;&nbsp;

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content white " role="content" data-step="2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix Warranty">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <h2 class="text-uppercase" style="margin: 0">Условия участия</h2>
                            <p>
                                Стань обладателем бани с мансардой – одной из 5, которые дарит компания «Теремъ». Участвуй в акции с 29 июня по 30 июля и получи каркасную баню 6х5 в комплектации «Лето», общей площадью 42 кв.м!
                            </p>
                            <p>
                                Для участия необходимо:
                            </p>
                            <ol style="font-size: 16px">
                                <li>Купить участок в строительной компании «Теремъ» и до 30 июля его полностью оплатить;</li>
                                <li>Зарегистрировать номер договора в форме выше;</li>
                                <li>Сохранить sms-сообщение с кодом подтверждения.</li>
                            </ol>
                            <p>
                                Компания «Теремъ» вручит подарки 5 августа в 16:00 на территории выставочного комплекса в Кузьминках. Чтобы получить баню нужно присутствовать лично!
                            </p>
                            <p class="text-center">
                                Получи баню мечты от компании «Теремъ»!
                            </p>
                            <a href="ПРАВИЛА проведения и условия участия в акции БАНЯ В ПОДАРОК.docx">Скачать правила участия в конкурсе</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
