<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
?>
<!DOCTYPE html>
<html lang="ru">

    <head>

        <meta charset="utf-8">
        <title>Теремъ &mdash; сертификат на&nbsp;мебель в&nbsp;подарок!</title>
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="og:image" content="path/to/image.jpg">
        <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="/bitrix/css/main/font-awesome.min.css">

        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#000">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#000">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#000">

        <style>
            body {
                opacity: 0;
                overflow-x: hidden;
            }

            html {
                background-color: #fff;
            }
        </style>

        <script src="js/scripts.min.js" defer></script>
    </head>

    <body>
        <header class="header hero">
            <div class="container">
                <div class="header--wrapper">
                    <a href="#hero" class="logo hero smooth"></a>
                    <nav class="nav">
                        <!-- <a href="#gallery" data-name="gallery" class="nav__link smooth">Галерея</a> -->
                        <a href="#projects" data-name="projects" class="nav__link smooth">Проекты</a>
                        <a href="#feedback" data-name="feedback" class="nav__link smooth">Отзывы</a>
                        <a href="#materials" data-name="materials" class="nav__link smooth">Материалы</a>
                        <a href="#choose" data-name="choose" class="nav__link smooth">Подбор проекта</a>
                        <a href="#partner" data-name="partner" class="nav__link smooth">Партнер</a>
                        <a href="#contacts" data-name="contacts" class="nav__link smooth">Контакты</a>
                    </nav>
                    <i class="sandwich-button fa fa-bars"></i>
                </div>
            </div>
        </header>
        <div class="section hero" id="hero">
            <div class="container">
                <div class="section--wrapper">

                    <div class="myrow heading-row">
                            <h1 class="main-title"><span>При покупке бани&nbsp;&mdash; сертификат на&nbsp;мебель в&nbsp;подарок</span></h1>
                            <p class="main-subtitle"><span>Для покупателей дома и&nbsp;бани увеличиваем сумму подарка</span></p>
                            <!-- <img src="img/ajax/item5.png" alt="" class="header_item"/> -->
                    </div>

                    <div>
                        <div class="row counter-row myrow">
                            <div class="col">
                                <p class="my-text">до окончания акции осталось:</p>
                                <div class="counter">
                                    <div class="counter__item days">
                                        <div class="counter__digits">00</div>
                                        <div class="counter__time">дней</div>
                                    </div>
                                    <div class="counter__item hours">
                                        <div class="counter__digits">00</div>
                                        <div class="counter__time">часов</div>
                                    </div>
                                    <div class="counter__item minutes">
                                        <div class="counter__digits">00</div>
                                        <div class="counter__time">минут</div>
                                    </div>
                                    <div class="counter__item seconds">
                                        <div class="counter__digits">00</div>
                                        <div class="counter__time">секунд</div>
                                    </div>
                                </div>

                            </div>
                            <div class="col">
                                <a class="btn btn--big smooth" href="#kitchen">Получить сертификат
                                    <!-- <i class="fa fa-caret-right arrow"></i> -->
                                </a>
                            </div>

                        </div>
                        <div class="row smaller-text-row myrow">
                            <div class="smaller-text">
                                * При приобретении бани или дома и&nbsp;бани одновременно выдается сертификат номиналом 30&nbsp;000 (тридцать тысяч) или 50&nbsp;000 (пятьдесят тысяч) рублей соответственно, дающий право на&nbsp;получение скидки на&nbsp;покупку комплекта мебели KODA от&nbsp;компании OLIMAR
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>

        <div class="section kitchen" id="kitchen">
            <div class="container">
                <div class="kitchen--wrapper">
                    <div class="kitchen--left">
                        <div class="group">
                            <div class="title title--grey">
                                <span>При покупке бани<br>сертификат на мебель в подарок</span>
                            </div>
                            <p class="kitchen--subtitle">
                                Для покупателей дома и&nbsp;бани увеличиваем сумму подарка
                            </p>
                            <p style="margin-top: 0">
                                Акция действует<br>с&nbsp;15.01.2018 года по&nbsp;31.01.2018 года включительно
                            </p>
                        </div>

                        <div class="group">
                            <p>
                                До конца акции осталось:
                            </p>
                            <div class="counter">
                                <div class="counter__item days">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">дней</div>
                                </div>
                                <div class="counter__item hours">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">часов</div>
                                </div>
                                <div class="counter__item minutes">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">минут</div>
                                </div>
                                <div class="counter__item seconds">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">секунд</div>
                                </div>
                            </div>
                        </div>

                        <div class="group">
                            <h2 class="moto">Удобные бани <br>для&nbsp;вас</h2>
                        </div>

                        <div class="group">
                            <a href="#modal-rules" data-link="modal">Правила акции</a>
                        </div>
                    </div>
                    <div class="kitchen--right">
                        <div class="group">
                            <div class="title">
                                <span>Получить сертификат?</span>
                            </div>
                        </div>
                        <div class="group">
                            <p class="form__title">
                                Заполните форму
                            </p>
                            <p class="form__annotation">
                                Для получения персонального предложения и консультации
                            </p>
                        </div>
                        <div class="group">
                            <form action="/include/request_handler.php" class="help__form form" data-yagoal="REQUEST_OLIMAR_CONSULTING_SENT">
                                <div class="form__group">
                                    <label for="kitchen-name" class="form__label">
                                        Как к вам удобнее обратиться
                                        <input name="name" required type="text" id="kitchen-name" class="form__input" placeholder="Ваше имя">
                                    </label>
                                    <label for="kitchen-tel" class="form__label">
                                        Ваш контактный номер
                                        <input name="phone" required type="tel" id="kitchen-tel" class="form__input" data-item="phone" placeholder="+7 495 123-45-67">
                                    </label>
                                </div>
                                <div class="form__group">
                                    <input required type="checkbox" class="checkbox-input" id="kitchen-agree" hidden>
                                    <label for="kitchen-agree" class="form__label form__label--checkbox">
                                        <span>Я принимаю условия <a href="/privacy-policy">политики обработки данных</a></span>
                                    </label>
                                </div>
                                <div class="form__group">
                                    <input type="hidden" name="request_type" value="17">
                                    <button type="submit" class="btn btn--form">Получить сертификат</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="section gallery" id="gallery">
                <div class="container">
                        <div class="gallery--wrapper">
                                <div class="gallery__header">
                                        <div class="title title--white">Фото построенных домов</div>
                                </div>
                                <div class="gallery__slider">
                                        <div class="slider">
                                                <div class="slider1">
                                                        <div id="slider1">
                                                                <div class="slider__item">
                                                                        <a href="img/build/7.jpg" class="fancybox"><img src="img/build/7.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/8.jpg" class="fancybox"><img src="img/build/8.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/9.jpg" class="fancybox"><img src="img/build/9.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/10.jpg" class="fancybox"><img src="img/build/10.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/11.jpg" class="fancybox"><img src="img/build/11.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/12.jpg" class="fancybox"><img src="img/build/12.jpg" /></a>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="slider2">
                                                        <div id="slider2" style="margin-top: 5px">
                                                                <div class="slider__item">
                                                                        <a href="img/build/1.jpg" class="fancybox"><img src="img/build/1.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/2.jpg" class="fancybox"><img src="img/build/2.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/3.jpg" class="fancybox"><img src="img/build/3.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/4.jpg" class="fancybox"><img src="img/build/4.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/5.jpg" class="fancybox"><img src="img/build/5.jpg" /></a>
                                                                </div>
                                                                <div class="slider__item">
                                                                        <a href="img/build/6.jpg" class="fancybox"><img src="img/build/6.jpg" /></a>
                                                                </div>

                                                        </div>
                                                </div>
                                        </div>
                                </div>
                                <div class="gallery__footer">
                                        <div class="group">
                                                <h2 class="moto">Тёплые дома<br>для всей семьи</h2>
                                        </div>
                                        <div class="group">
                                                <div class="counter">
                                                        <div class="counter__item days">
                                                                <div class="counter__digits">00</div>
                                                                <div class="counter__time">дней</div>
                                                        </div>
                                                        <div class="counter__item hours">
                                                                <div class="counter__digits">00</div>
                                                                <div class="counter__time">часов</div>
                                                        </div>
                                                        <div class="counter__item minutes">
                                                                <div class="counter__digits">00</div>
                                                                <div class="counter__time">минут</div>
                                                        </div>
                                                        <div class="counter__item seconds">
                                                                <div class="counter__digits">00</div>
                                                                <div class="counter__time">секунд</div>
                                                        </div>
                                                </div>
                                                <a class="btn btn--big smooth" href="#kitchen">Получить кухню<br>в подарок</a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div> -->
        <div class="section choose" id="projects">
            <div class="container">
                <div class="choose--wrapper">
                    <div class="choose__header">
                        <span class="title title--grey">
                            Выберите свою баню
                        </span>
                        <!-- <span class="bold-grey">от 110 до 330 кв.м</span> -->
                    </div>
                    <section class="choose-house--houses">
                        <!-- <div class="choose-house--row"> -->
                            <div class="house--wrapper house--wrapper__important">
                                <article class="choose-house--house house house__important" data-house="Баня 6 × 6 м">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Баня 6&times;6</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">6 × 6 м</span>
                                            </p>
                                            <!--<p class="house--details-item">Площадь:
                                                <span class="house--area">от 50 кв.м</span>
                                            </p>-->
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 285 000 руб. до  24.12</span>
                                            </p>
											<p class="house--details-item" style="color:#dd0000;">Выгода:
                                                <span class="house--price">155 000 руб. до  24.12</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о бане
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img house--img__important" href="#modal-house" data-link="modal">
                                        <img src="img/houses/preview/banya66.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                        <!-- </div> -->

                        <!-- <div class="choose-house--row"> -->
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Баня 3 × 4 м">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Баня 3&times;4</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">3 × 4 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 9 кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 160 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о бане
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/house/Banya3x4/1.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Баня 4 × 4 м">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Баня 4&times;4</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">4 × 4 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 13 кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 170 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о бане
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/house/Banya4x4/1.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                        <!-- </div> -->

                        <!-- <div class="choose-house--row"> -->
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Баня 5 × 4 м">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Баня 5&times;4</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">5 × 4 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 16 кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 185 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о бане
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/house/Banya5x4/1.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Баня 5 × 5 м">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Баня 5&times;5</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">5 × 5 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 22 кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 240 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о бане
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/house/Banya5x5/1.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                        <!-- </div> -->

                        <!-- <div class="choose-house--row"> -->
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Баня 6 × 5 м">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Баня 6&times;5</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">6 × 5 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 27 кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 225 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о бане
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/house/Banya6x5/1.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                            <div class="house--wrapper">
                                <article class="choose-house--house house" data-house="Баня Лада">
                                    <section class="house--caption">
                                        <h3 class="house--heading">
                                            <a href="#modal-house" data-link="modal">Баня &laquo;Лада&raquo;</a>
                                        </h3>
                                        <section class="house--details">
                                            <div class="house--materials">
                                                <span class="house--material">Каркас</span>
                                                <span class="house--material">Брус</span>
                                                <!-- <span class="house--material">Кирпич</span> -->
                                            </div>
                                            <p class="house--details-item">Размер:
                                                <span class="house--size">6 × 8 м</span>
                                            </p>
                                            <p class="house--details-item">Площадь:
                                                <span class="house--area">от 57 кв.м</span>
                                            </p>
                                            <p class="house--details-item">Стоимость:
                                                <span class="house--price">от 610 000 руб.</span>
                                            </p>
                                            <a class="house--more" href="#modal-house" data-link="modal">
                                                Подробнее о бане
                                            </a>
                                        </section>
                                    </section>
                                    <a class="house--img" href="#modal-house" data-link="modal">
                                        <img src="img/house/BanyaLada/1.jpg" alt="">
                                    </a>
                                </article>
                            </div>
                        <!-- </div> -->
                    </section>
                    <!-- <div class="slider--wrapper">
                        <div class="slider__extra">
                            <a href="#modal-call" data-link="modal" class="btn btn--label">Оставить<br>заявку</a>
                            <div class="counter">
                                <div class="counter__item days">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">дней</div>
                                </div>
                                <div class="counter__item hours">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">часов</div>
                                </div>
                                <div class="counter__item minutes">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">минут</div>
                                </div>
                                <div class="counter__item seconds">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">секунд</div>
                                </div>
                            </div>
                        </div>
                        <div class="single-item">

                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <section class="section house-and-banya" id="house-and-banya">
            <div class="container">
                <div>
                    <section class="house-and-banya--heading">
                        <h2 class="title">
                            Дом и баня &mdash; подарок больше
                        </h2>
                        <a href="/catalog/" target="_blank">Посмотреть<br>все проекты</a>
                    </section>
                    <section class="house-and-banya--content">
                        <div class="images">
                            <div class="image">
                                <img src="img/house_banya/house.jpg" alt="">
                            </div>
                            <div class="image">
                                <img src="img/house_banya/banya.jpg" alt="">
                            </div>
                        </div>
                        <div class="caption">
                            <p class="motto">Удобные бани <br>для&nbsp;вас</p>
                            <button href="#kitchen" class="btn btn--form smooth">Получить сертификат на мебель</button>
                        </div>
                    </section>
                </div>
            </div>
        </section>

        <div class="section feedback" id="feedback">
            <div class="container">
                <div class="feedback--wrapper">
                    <div class="feedback__header">
                        <div class="title title--white">Отзывы наших клиентов</div>
                    </div>
                    <div class="slider">
                        <div id="video-slider" class="slider--wrapper">
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/heNgwQOOEts" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Панина<br>Людмила<br>Валентиновна
                                    </div>
                                    <div class="slider__house">
                                        Баня 6х5<br>От 27,02 кв.м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/-HIq0Bsr8mo" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Климов<br>Сергей<br>Петрович
                                    </div>
                                    <div class="slider__house">
                                        Баня 3х4<br>От 9,51 кв.м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/y5xrx7cy554" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Жаранов<br>Михаил<br>Иванович
                                    </div>
                                    <div class="slider__house">
                                        Баня К-150 5х4 м<br>
                                        От 16,81 кв.м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/4lh8PAPv_7I" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Аксенов<br>Евгений<br>Владимирович
                                    </div>
                                    <div class="slider__house">
                                        Баня 5х5<br>
                                        От 22,41 кв.м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/htZVTfZ31t0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Михайлова<br>Вера<br>Васильевна
                                    </div>
                                    <div class="slider__house">
                                        Баня 6х6 СМ<br>
                                        От 32,40 кв.м
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__video">
                                    <iframe width="100%" height="444" src="https://www.youtube.com/embed/zmsoJF4w5nw" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="slider__panel">
                                    <div class="slider__video-name">
                                        Вовк<br>Ангелина<br>Михайловна
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feedback__footer">
                        <div class="group">
                            <h2 class="moto">Удобные бани <br>для&nbsp;вас</h2>
                        </div>
                        <div class="group">
                            <div class="counter">
                                <div class="counter__item days">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">дней</div>
                                </div>
                                <div class="counter__item hours">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">часов</div>
                                </div>
                                <div class="counter__item minutes">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">минут</div>
                                </div>
                                <div class="counter__item seconds">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">секунд</div>
                                </div>
                            </div>
                            <a class="btn btn--big smooth" href="#kitchen">Получить сертификат<br>на мебель</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section materials" id="materials">
            <div class="container">
                <div class="materials--wrapper">
                    <div class="materials__header">
                        <div class="title title--grey">Из чего мы строим?</div>
                    </div>
                    <div class="materials__body">
                        <div class="materials__raw">
                            <div class="item">
                                <div class="item__icon">
                                    <img src="img/wood1.png" alt="Северная сосна">
                                </div>
                                <div class="item__text">Северная<br>сосна</div>
                            </div>
                            <div class="item">
                                <div class="item__icon">
                                    <img src="img/wood2.png" alt="Северная ель">
                                </div>
                                <div class="item__text">Северная<br>ель</div>
                            </div>
                        </div>
                        <div class="materials__tech">
                            <div class="item">
                                <div class="item__img">
                                    <img src="img/1.png" alt="Брус">
                                </div>
                                <div class="item__name">Брус</div>
                                <div class="item__btn">
                                    <a href="/about/expert-opinion/tekhnologii-stroitelstva/#lumber" class="btn btn--grey" target="_blank">Подробнее<br>о технологии</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="item__img">
                                    <img src="img/2.png" alt="Каркас">
                                </div>
                                <div class="item__name">Каркас</div>
                                <div class="item__btn">
                                    <a href="/about/expert-opinion/tekhnologii-stroitelstva/#frame" class="btn btn--grey" target="_blank">Подробнее<br>о технологии</a>
                                </div>
                            </div>
                            <!-- <div class="item">
                                <div class="item__img">
                                    <img src="img/3.png" alt="Кирпич">
                                </div>
                                <div class="item__name">Кирпич</div>
                                <div class="item__btn">
                                    <a href="/about/expert-opinion/tekhnologii-stroitelstva/#brick" class="btn btn--grey">Подробнее<br>о технологии</a>
                                </div>
                            </div> -->
                        </div>


                    </div>
                    <div class="container-flex">
                        <!-- <div class="group"> -->
                            <p class="motto">Удобные бани <br>для&nbsp;вас</p>
                        <!-- </div> -->
                        <div class="group">
                            <div class="counter">
                                <div class="counter__item days">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">дней</div>
                                </div>
                                <div class="counter__item hours">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">часов</div>
                                </div>
                                <div class="counter__item minutes">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">минут</div>
                                </div>
                                <div class="counter__item seconds">
                                    <div class="counter__digits">00</div>
                                    <div class="counter__time">секунд</div>
                                </div>
                            </div>
                            <a class="btn btn--big smooth" href="#kitchen">Получить сертификат<br>на мебель</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section help" id="choose">
            <div class="container">
                <div class="help--wrapper">
                    <div class="title title--grey">
                        Не нашли подходящую баню?
                    </div>
                    <form action="/include/request_handler.php" class="help__form form" data-yagoal="REQUEST_OLIMAR_PROJECT_PICKING_UP_SENT">
                        <div class="form--left">
                            <div class="form__title">
                                Менеджер подберет вам проект
                            </div>
                            <div class="form__collection">
                                <div class="form__collection--left">
                                    <div class="form__checkbox">
                                        <input name="type[]" value="Клееный брус" type="checkbox" id="brus" hidden="">
                                        <label for="brus" class="form__label">клееный брус</label>
                                    </div>
                                    <div class="form__checkbox">
                                        <input name="type[]" value="Каркас" type="checkbox" id="karkas" hidden="">
                                        <label for="karkas" class="form__label">каркас</label>
                                    </div>
                                    <!-- <div class="form__checkbox">
                                        <input name="type[]" value="Кирпич" type="checkbox" id="kirpich" hidden="">
                                        <label for="kirpich" class="form__label">кирпич</label>
                                    </div> -->
                                </div>

                                <div class="form__collecton--right">
                                    <div class="form__checkbox">
                                        <input name="floors[]" type="checkbox" value="Один этаж" id="one_floor" hidden="">
                                        <label for="one_floor" class="form__label">одноэтажная</label>
                                    </div>
                                    <div class="form__checkbox">
                                        <input name="floors[]" value="Два этажа" type="checkbox" id="two_floors" hidden="">
                                        <label for="two_floors" class="form__label">два этажа</label>
                                    </div>
                                    <div class="form__area">
                                        <input name="square" type="text" placeholder="Площадь" id="area" class="form__area--input">
                                        <label for="area" class="form__area--label">кв.м</label>
                                    </div>
                                </div>
                            </div>
                            <div class="group">
                                <h2 class="moto">Удобные бани <br>для&nbsp;вас</h2>
                            </div>
                        </div>
                        <div class="form--right">
                            <div class="form__group">
                                <label for="kitchen-name" class="form__label">
                                    Как к вам удобнее обратиться
                                    <input required name="name" type="text" id="kitchen-name" class="form__input" placeholder="Ваше имя">
                                </label>
                                <label for="kitchen-tel" class="form__label">
                                    Ваш контактный номер
                                    <input required name="phone" type="tel" id="kitchen-tel" class="form__input" data-item="phone" placeholder="+7 495 123-45-67">
                                </label>
                            </div>
                            <div class="form__group" style="margin-top: 40px">
                                <input required type="checkbox" class="checkbox-input" id="help-agree" hidden="">
                                <label for="help-agree" class="form__label form__label--checkbox">
                                    <span>Я принимаю условия <a href="/privacy-policy">Политики обработки данных</a></span>
                                </label>
                            </div>
                            <div class="form__group">
                                <input type="hidden" name="request_type" value="18">
                                <button type="submit" class="btn btn--form">Получить консультацию</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="section partner" id="partner">
            <div class="container">
                <div class="partner--wrapper">
                    <div class="partner--left">
                        <div class="partner__logo">
                            <img src="img/furniture/logo.png" alt="Ajax">
                        </div>
                        <div class="partner__text">
                            <h2 class="partner__title">КОМПАНИЯ OLIMAR</h2>
                            <p>
                                Поставщик и&nbsp;производитель мебели
                            </p>
                            <p>
                                Компания начала свою деятельность еще в&nbsp;1996 году и&nbsp;за&nbsp;время существования наработала деловые связи с&nbsp;лучшими производителями мебели. Начав с&nbsp;поставок индонезийской мебели в&nbsp;Россию, за&nbsp;20&nbsp;с лишним лет OLIMAR обзавелась десятками партнеров среди мебельных фабрик из&nbsp;разных уголков мира и&nbsp;открыла собственное производство. Теперь в&nbsp;магазинах компании можно найти обстановку на&nbsp;любой вкус.
                            </p>
                            <p>
                                Компания специализируется на&nbsp;продажах качественной, долговечной мебели, которая производится из&nbsp;хороших материалов и&nbsp;будет годами служить своим хозяевам. В&nbsp;совместной акции с&nbsp;компанией &laquo;Теремъ&raquo; участвуют комплекты мебели для отдыха из&nbsp;серии KODA. Эта линия производится на&nbsp;российской фабрике, и&nbsp;все предметы в&nbsp;ней выполнены из&nbsp;массива дерева.
                            </p>
                            <!-- <p>
                                Своими гарнитурами мы создаем интерьер для спокойного домашнего уюта и веселых дружеских встреч.
                            </p>
                            <p>
                                «Магия кухни» — сделаем вашу кухню комфортной и стильной!
                            </p> -->
                        </div>
                        <div class="partner__contacts">
                            <p>
                                <a href="tel:+74959215415">+7 495 921-54-15</a>, <a href="tel:+79854104502">+7 985 410-45-02</a><br><a href="http://olimar.ru/">www.olimar.ru</a>
                            </p>
                        </div>
                    </div>
                    <div class="partner--right">
                        <div class="row">
                            <img src="img/furniture/1.jpg" alt="">
                            <img src="img/furniture/2.jpg" alt="">
                        </div>
                        <div class="row">
                            <img src="img/furniture/3.jpg" alt="">
                        </div>
                        <div class="row">
                            <div class="col">
                                <img src="img/furniture/4.jpg" alt="">
                                <img src="img/furniture/5.jpg" alt="">
                            </div>
                            <div class="col">
                                <img src="img/furniture/6.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="section contacts" id="contacts">
            <div class="container">
                <div class="contacts--wrapper">
                    <div class="contacts__map">
                        <div class="contacts__panel">
                            <div class="title title--grey">Контакты</div>
                            <div class="group address">
                                <strong>Адрес:</strong>
                                <span>г. Москва,</span>
                                <span>ул. Зеленодольская, вл.42</span>
                            </div>
                            <div class="group phone">
                                <strong>Телефон:</strong>
                                <span>8 (495) 419-07-78</span>
                            </div>
                            <div class="group email">
                                <strong>E-mail:</strong>
                                <a href="mailto:info@terem-pro.ru">info@terem-pro.ru</a>
                            </div>
                            <div class="group site">
                                <strong>Сайт:</strong>
                                <a href="https://terem-pro.ru">www.terem-pro.ru</a>
                            </div>
                            <div class="socials">
                                <div>
                                    <a href="https://www.facebook.com/terempro" class="fb"></a>
                                    <a href="https://twitter.com/ProTerem" class="tw"></a>
                                    <a href="http://vk.com/pro_terem" class="vk"></a>
                                </div>
                                <div>
                                    <a href="http://ok.ru/v2010godu" class="ok"></a>
                                    <a href="http://instagram.com/pro_terem" class="in"></a>
                                    <a href="http://terem-pro.livejournal.com/" class="lj"></a>
                                </div>
                            </div>
                        </div>
                        <div class="contacts__ymap">
                            <div id="map" style="width: 100%; height: 600px"></div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="section questions">
                <div class="container">
                    <div class="questions--header">
                        <h2 class="questions--heading">
                            Остались вопросы?
                        </h2>
                        <section class="questions--message">
                            <p>Оставьте номер</p>
                            <p>Наши специалисты свяжутся с&nbsp;вами</p>
                        </section>
                    </div>
                    <form action="/include/request_handler.php" class="questions--form form" data-yagoal="REQUEST_OLIMAR_QUESTIONS_LEFT_SENT">
                        <label for="questions--name">
                            Как к вам удобнее обратиться?
                            <input name="name" type="text" id="questions--name" placeholder="Ваше имя">
                        </label>
                        <label for="questions--phone">
                            Ваш контактный номер
                            <input name="phone" type="tel" id="questions--phone" data-item="phone" placeholder="+7 495 123-45-67">
                        </label>
                        <input type="hidden" name="request_type" value="19">
                        <button type="submit">Перезвонить мне</button>
                    </form>
                </div>
            </section>

            <footer class="contacts__footer">
                <div class="container">
                    <p>
                        © Теремъ-про, 2009 - 2018. Все материалы данного сайта являются объектами авторского права (в том числе дизайн). Запрещается
                        копирование, распространение (в том числе путем копирования на другие сайты и ресурсы в Интернете) или любое иное использование
                        информации и объектов без предварительного согласия правообладателя. Cайт не является публичной офертой.
                    </p>
                </div>
            </footer>
        </section>



        <link rel="stylesheet" href="css/main.min.css">
        <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>



    </body>


</html>

<!-- Modals -->

<div id="modal-code" class="modal code-modal" role="dialog">
    <div class="code-modal--container">
        <p class="attention">Внимание!</p>
        <p class="message">Ваш персональный код <br>для получения сертификата на мебель</p>
        <span class="code">######</span>
        <p class="small-text">Сохраните этот код и предъявите менеджеру при покупке дома или бани</p>
        <p class="small-text">Представитель компании свяжется с вами в ближайшее время</p>
        <a class="save">Сохранить</a>
        <p class="error-msg">
            Вы уже получили код
        </p>
        <button class="close">×</button>
    </div>

</div>

<!-- Modal thx -->
<div id="modal-thx" class="modal modal-thx" role="dialog">
    <div class="modal__dialog">
        <button type="button" class="close"></button>
        <div class="modal__header">
            <h3 class="modal__title">
                <span></span>
                <span class="title title--grey">Спасибо!</span>
            </h3>
            <div class="modal__subtitle">
            </div>
        </div>
        <div class="modal__body">
            <span>Ваша заявка принята!<br>Наши менеджеры скоро с вами свяжутся.</span>
        </div>
    </div>
</div>



<!-- Modal rules -->
<div id="modal-rules" class="modal modal-rules" role="dialog">
    <div class="modal__dialog">
        <button type="button" class="close"></button>
        <div class="modal__header">
            <h3 class="modal__title">
                <span></span>
                <span class="title title--grey">ПРАВИЛА ПРОВЕДЕНИЯ АКЦИИ</span>
            </h3>
            <div class="modal__subtitle">
            </div>
        </div>
        <div class="modal__body">
            <ol>
                <li>1. Общие положения
                    <ol>
                        <li>1.1. Организатором Акции «Сертификат на покупку мебели в подарок» является юридическое лицо, созданное в соответствии с законодательством Российской Федерации.  Наименование: Общество с ограниченной ответственностью «Теремъ-про». Юридический адрес: 140170, Московская область, г. Бронницы, ул. Центральная, д.2., ОГРН – 1105040000174, ИНН– 5002096561</li>
                        <li>1.2. Партнером Акции «Сертификат на покупку мебели в подарок» является юридическое лицо, созданное в соответствии с законодательством Российской Федерации.  Наименование: Общество с ограниченной ответственностью ООО «Альт». Юридический адрес: РФ, 350059, Россия, Краснодарский край, г.Краснодар, ул.Новороссийская, д. 236, литер Г, офис №3, ИНН 2312155042, КПП 231201001, ОГРН 1082312010890, адрес обособленного подразделения в Москве: г.Москва, ул.Докукина, д.10, стр.26.</li>

                    </ol>
                </li>
                <li>2. Сроки проведения
                    <ol>
                        <li>2.1. Срок публикации в средствах информации: с 15.01.2018 года по 31.01.2018 года включительно.</li>
                        <li>2.2. Сроки проведения Акции и срок выдачи сертификатов: с 15.01.2018 года по 31.01.2018 года включительно.</li>
                        <li>2.3. Сертификат может быть предъявлен партнеру с момента его получения и до 30.11.2018 года.</li>
                        <li>2.4. Срок снятия с публикации: 31.01.2018 года.</li>
                    </ol>
                </li>
                <li>
                    3. Условия проведения Акции.
                    <ol>
                        <li>3.1. При заключении Клиентом с Организатором договора на строительство Бани в период с 15.01.2018 года по 31.01.2018г., Клиент получает сертификат номиналом 30 000 рублей. При заключении Клиентом с Организатором договора на строительство Дома и Бани в период с 15.01.2018 года по 31.01.2018г., Клиент получает сертификат номиналом 50 000 рублей.</li>
                        <li>3.2. Сертификат нельзя обналичить, продать или обменять.</li>
                        <li>3.3. Сертификат не подлежит возврату и обмену на денежные средства.</li>
                        <li>3.4. Сертификат можно использовать только в рамках одного договора на приобретение товара Партнера</li>
                        <li>3.5. Организатор Акции не несет ответственность за качество товара партнера, сроки предос-тавления партнером услуг.</li>
                        <li>3.6. Партнёр не несёт ответственности за наличие на складе в момент обращения базовых предметов из Комплектов мебели, участвующих в настоящей Акции. Срок исполнения поставки товара оговаривается в каждом конкретном случае (от 25 до 60 календарных дней).<br>Акционный Комплект мебели поставляется упакованным и в собранном виде, кроме кровати.
                        В Комплекте мебели для зоны отдыха кресла, диваны и пуфики комплектуются подушками из базовых тканей (на белые предметы – подушки серый лен, на коричневые предметы подушки бежевая рогожка).
                        В Комплекте мебели для спальни предметы оснащены роликовыми направляющими без доводчиков, ручками коваными испанскими AMIG.
                        Размеры предметов, входящих в Акционные комплекты, базовые, указаны на сайте www.olimar.ru и соответствуют образцам.
                        Изготовление предметов в индивидуальный размер заказчика возможно, но на индивидуальный заказ не будет действовать купон на скидку, цена будет рассчитываться.
                        </li>
                        <li>3.7. Доставка и сборка товара не входит в акционное предложение и могут быть заказаны отдельно.</li>
                        <li>3.8. Доставка осуществляется сторонней организацией в пределах МКАД до подъезда за 1500 руб. Доставка до подъезда по Московской области рассчитывается по формуле: 1500 руб. + 35 руб. за 1 км от МКАД до места доставки. Самовывоз со склада партнера по адресу г. Москва, ул.Докукина, д.10, стр.26 бесплатный с понедельника по пятницу с 10 до 18 часов по предварительной договоренности, партнер обеспечивает своими силами погрузку товара на транспорт Клиента.</li>
                        <li>3.9. По истечении указанного срока сертификат становится недействительным и скидка не предоставляется. </li>
                        <li>3.10. В случае потери, порчи, кражи сертификата он не восстанавливается, дубликат сертификата не выдается. </li>
                        <li>3.11. Товар Партнера, участвующий в Акции: Сертификат на покупку мебели в подарок (далее – товар):
                            <p><b><strong>Комплект мебели для отдыха KODA двойной в базовом цвете (коричневый или белый)</strong></b> (далее – товар):</p>
                            <ol>
                                <li>KODA-04.2   Диван 2-х местный KODA  1 </li>
                                <li>KODA-04.1   Кресло KODA  2 </li>
                                <li>KODA-04.4   Пуфик KODA  1 </li>
                                <li>KODA-01.7   Стол для кофе KODA  1 </li>
                            </ol>
                            <p><b><strong>или<br>Комплект мебели для отдыха KODA тройной в базовом цвете (коричневый или белый) </strong></b> (далее – товар):</p>
                            <ol>
                                <li>KODA-04.3   Диван 3-х местный KODA  2 </li>
                                <li>KODA-04.1   Кресло KODA  2 </li>
                                <li>KODA-04.4   Пуфик KODA  1 </li>
                                <li>KODA-01.7   Стол для кофе KODA 1 </li>
                            </ol>
                            <p>Товар партнера, участвующий в Акции, при покупке дома и бани:</p>
                            <p><b><strong>Комплект мебели KODA для спальни 160 в базовом цвете (коричневый или слоновая кость)</strong></b>  (далее – товар):</p>
                            <ol>
                                <li>Кровать KODA 160х200 см 1 </li>
                                <li>Тумба прикроватная KODA 2 </li>
                                <li>Комод KODA на выбор 4 ящика или 6 ящиков 1 </li>
                                <li>Шкаф для одежды KODA 1 </li>
                                <li>Стол туалетный, письменный KODA 1 </li>
                                <li>Зеркало  - 1шт.</li>
                                <li>Пуфик (стул, табурет на выбор) 1 </li>
                            </ol>
                            <p>Или<br><b><strong>Комплект мебели KODA для спальни 180 в базовом цвете (коричневый или слоновая кость) </strong></b> (далее – товар):</p>
                            <ol>
                                <li>Кровать KODA 180х200 см 1 </li>
                                <li>Тумба прикроватная KODA  2 </li>
                                <li>Комод KODA на выбор 4 ящика или 6 ящиков 1 </li>
                                <li>Шкаф для одежды KODA 1 </li>
                                <li>Стол туалетный, письменный KODA 1 </li>
                                <li>Зеркало  - 1шт.</li>
                                <li>Пуфик (стул, табурет на выбор) 1 </li>
                            </ol>
                        </li>
                        <li>3.12. Клиент вправе использовать сертификат для получения гарантированной скидки при покупке другого Комплекта мебели партнера, не входящего в акционный перечень, на следующих условиях:
                            <p>Скидка в 12% при стоимости товара до 100 000р. включительно<br>
                        + Подарок – беспроигрышная лотерея (мелкая бытовая техника)<br>
                        + Подарок Подсвечник производства Филиппины или Ваза производства Филиппины на выбор.<br>
                       Скидка в 15% при стоимости товара от 100 001р. до 200 000р. включительно<br>
                        + Подарок – беспроигрышная лотерея (мелкая бытовая техника)<br>
                        + Индийская подушка с принтом из натуральной кожи, сшитая из кусочков по принципу пэчворк 40х40 или 60х60)
                       </p>
                       <p>Скидка в 20% при стоимости товара от 200 001р. до 500 000р. включительно<br>
                        + Подарок – беспроигрышная лотерея (мелкая бытовая техника)<br>
                        + Подарок (Индийская подушка с принтом из натуральной кожи, сшитая из кусочков по принципу пэчворк и покрывало кожаное из этой же коллекции)<br>

                       Скидка в 25% при стоимости гарнитура от 500 001р. до 1 000 000р. включительно<br>
                       + Подарок – беспроигрышная лотерея (мелкая бытовая техника)<br>
                       + Подарок (Индийская подушка с принтом из натуральной кожи, сшитая из кусочков по принципу пэчворк и покрывало кожаное из этой же коллекции)<br>
                       + Подарок (Индонезийская картина на выбор, ручная работа, холст, масло)<br>

                       Скидка в 30% при покупке гарнитура свыше 1 000 001р.<br>
                       + Подарок – беспроигрышная лотерея (мелкая бытовая техника)<br>
                       + Эксклюзивный подарок по согласованию с Клиентом.
                       </p>
                        </li>
                    </ol>
                </li>
                <li>
                    4. Территория проведения Акции
                    <ol>
                        <li>4.1. Акция проводится на территории РФ.</li>
                    </ol>
                </li>
                <li>
                    5. Порядок и способ информирования участников Акции.
                    <ol>
                        <li>5.1. Информирование участников Акции и&nbsp;потенциальных участников Акции, о&nbsp;ее&nbsp;условиях, сроках, досрочном прекращении ее&nbsp;проведения будет происходить посредством размещения информации по&nbsp;адресу <a href="https://www.terem-pro.ru/olimar/">https://www.terem-pro.ru/olimar/</a></li>
                        <li>Размещения настоящих Правил Акции в&nbsp;глобальной сети интернет по&nbsp;адресу <a href="https://www.terem-pro.ru/olimar/">https://www.terem-pro.ru/olimar/</a> на&nbsp;весь период срока проведения Акции.</li>
                    </ol>
                </li>
                <li>
                    6. Призовой фонд Акции.
                    <ol>
                        <li>6.1. Призовой фонд Акции формируется за счет средств партнера Акции.</li>
                        <li>6.2. Призовой фонд включает в себя 120 (сто двадцать) сертификатов номиналом 30 000 (тридцать тысяч) рублей каждый; 20 (двадцать) сертификатов номиналом 50 000 (пятьдесят тысяч) рублей каждый.</li>
                    </ol>
                </li>
                <li>
                    7. Участники Акции.
                    <ol>
                        <li>
                            7.1.Участником Акции могут стать лица, соответствующие следующим условиям:
                            <ul>
                                <li>К&nbsp;участию в&nbsp;Акции допускаются только проживающие на&nbsp;территории проведения Акции (п.&nbsp;2.2. Правил) лица в&nbsp;возрасте от&nbsp;18&nbsp;лет.</li>
                                <li>К&nbsp;участию в&nbsp;Акции не&nbsp;допускаются: лица, указавшие некорректную, неточную, недостоверную информацию.</li>
                                <li>Факт участия в&nbsp;Акции подразумевает ознакомление и&nbsp;полное согласие Клиента с&nbsp;настоящими Правилами. Клиент соглашается на&nbsp;обработку его/ее персональных данных Организатором и/или уполномоченными им&nbsp;лицами в&nbsp;целях Акции. Персональные данные предоставляются на&nbsp;добровольной основе, однако несогласие на&nbsp;обработку таких данных делает невозможным участие в&nbsp;Акции.</li>
                            </ul>
                        </li>
                    </ol>
                </li>
                <li>
                    8. Права и обязанности Клиента
                    <ol>
                        <li>8.1 Клиент имеет право:
                            <ul>
                                <li>Ознакомиться Правилами Акции.</li>
                                <li>Принимать участие в&nbsp;Акции в&nbsp;порядке, определенном настоящими Правилами, получать информацию об&nbsp;изменениях в&nbsp;Правилах.</li>
                                <li>Требовать от&nbsp;Организатора Акции получения информации об&nbsp;Акции в&nbsp;соответствии с&nbsp;Правилами Акции.</li>
                                <li>На получение сертификата при заключении Клиентом с Организатором договора на строительство бани, дома и бани в период с 15.01.2018г. до 31.01.2018г.</li>
                                <li>Выполнять все действия, связанные с участием в Акции и получением сертификата в соот-ветствии с настоящими Правилами Акции.</li>
                            </ul>
                        </li>
                    </ol>
                </li>
                <li>
                    9. Права и обязанности Организатора
                    <ol>
                        <li>9.1. Организатор имеет право:
                            <ul>
                <li>Отказать в участии в Акции Клиентам, не соблюдающим настоящие Правила.</li>
                <li>Отказать в выдаче Сертификата Клиенту, не получившему Сертификат в срок, указанный Организатором, по вине Клиента.</li>
                                <li>Передать невостребованные сертификаты Партнеру.</li>
                                <li>Вносить изменения в&nbsp;Правила Акции с&nbsp;обязательной публикацией изменений по&nbsp;интернет-адресу <a href="https://www.terem-pro.ru/">https://www.terem-pro.ru/</a>.</li>
                                <li>Организатор оставляет за&nbsp;собой право не&nbsp;вступать в&nbsp;письменные переговоры либо иные контакты с&nbsp;Клиентами Акции кроме как в&nbsp;случаях, указанных в&nbsp;настоящих Правилах или на&nbsp;основании требований действующего законодательства Российской Федерации.</li>
                                <li>Организатор также имеет и&nbsp;иные права, предусмотренные настоящими Правилами. </li>
                                <li>Организатор оставляет за&nbsp;собой право отказать Клиенту в&nbsp;выдаче сертификата на&nbsp;свое усмотрение. Клиент имеет право обратиться к&nbsp;Организатору за&nbsp;пояснениями причин отказа</li>
                                <li>Организатор оставляет за собой право отказать Клиенту в выдаче сертификата на свое усмотрение. Клиент имеет право обратиться к Организатору за пояснениями причин отказа</li>
                            </ul>
                        </li>
                        <li>9.2. Организатор обязуется:
                            <ul>
                                <li>Провести Акцию в&nbsp;порядке, определенном настоящими Правилами.</li>
                                <li>Выдать сертификаты Клиенту в&nbsp;соответствии с&nbsp;условиями настоящих Правил.</li>
                            </ul>
                        </li>
                    </ol>
                </li>
                <li>
                    10. Порядок и сроки получения сертификата
                    <ol>
                        10.1. Клиент в&nbsp;соответствии с&nbsp;п.&nbsp;3. Правил Акции получает от&nbsp;Организатора Акции сертификат в&nbsp;офисе компании, которая располагается по&nbsp;адресу: г. Москва, ул. Зеленодольская вл. 42, выставочная площадка &laquo;Теремъ&raquo;.
                        10.2.Клиент обязан заполнить и&nbsp;подписать &laquo;акт приёма-передачи подарочного сертификата&raquo; (в&nbsp;двух экземплярах), согласие на&nbsp;обработку персональных данных.
                        10.3. Сертификат не&nbsp;выдается при несоблюдении Клиентом настоящих Правил.
                    </ol>
               </li>
                <li>
                    11. Дополнительные условия
                    <ol>
                        <li>11.1. Организатор не&nbsp;несет ответственности&nbsp;за:
                            <ul>
                                <li>несоблюдение, несвоевременное выполнение Клиентом настоящих Правил;</li>
                                <li>получение от&nbsp;Клиента неполных, некорректных сведений, необходимых для участия в&nbsp;Акции и&nbsp;получения сертификата.</li>
                            </ul>
                        </li>
                        <li>11.2. Персональные данные, полученные от&nbsp;Клиентов в&nbsp;соответствии с&nbsp;данными Правилами, хранятся в&nbsp;базе данных, администрированием которой занимается Организатор Акции. Персональные данные используются и&nbsp;хранятся в&nbsp;целях проведения Акции.</li>
                        <li>11.3. Данные Правила являются единственными официальными Правилами участия в&nbsp;Акции. В&nbsp;случае возникновения ситуаций, допускающих неоднозначное толкование этих Правил, и/или вопросов, не&nbsp;урегулированных этими Правилами, окончательное решение о&nbsp;таком толковании и/или разъяснения принимается непосредственно и&nbsp;исключительно Организатором Акции.</li>
                        <li>11.4. Термины, употребляемые в&nbsp;настоящих Правилах, относятся исключительно к&nbsp;настоящей Акции.</li>
                        <li>11.5. Все спорные вопросы, касающиеся настоящей Акции, регулируются на&nbsp;основе действующего законодательства&nbsp;РФ.</li>
                    </ol>
                </li>
            </ol>
        </div>
    </div>
</div>

<!-- Modal house -->
<div id="modal-house" class="modal" role="dialog">

</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter937330 = new Ya.Metrika({
                    id: 937330,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    ut: "noindex"
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
<div><img src="//mc.yandex.ru/watch/937330?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->
