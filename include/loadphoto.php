<?php

const ATTEMPTS_EXHAUSTED = -1;
const CONFIRMATION_CODE_GIVEN = -2;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if ($_SESSION["CONTEST_CODE"])
{
    // Если код введён 3 раза неверно, преломляем дальнейшие попытки
    
    if (++$_SESSION["CONTEST_ATTEMPTS"] == 3) 
    {
        $_SESSION["CONTEST_FAILED"] = true;
        echo ATTEMPTS_EXHAUSTED; // Сигнал для ajax, что надо вывести сообщение об ошибке
        die();
    }
    
    // Если код введён некорректно, выводим сообщение на экран

    if ($_SESSION["CONTEST_CODE"] != $_POST['confirmation-code'])
    {
        echo 'Введён некорректный код подтверждения!';
    }
    
    // Иначе, завершаем регистрацию заявки и отправляем уведомления клиенту и администрации
    // При успешном завершении в ajax-запрос возвращается id заявки и будет выведено сообщение
    // об успехе
    
    else
    {
        // Создаём массив данных заявки
        
        $el = new CIBlockElement;

        $properties = array();
        $properties["CHILD_NAME"] =  $_SESSION['CHILD_NAME'];
        $properties["CHILD_AGE"] =  $_SESSION['CHILD_AGE'];
        $properties["CHILD_PARENT"] =  $_SESSION['PARENT'];
        $properties["EMAIL"] =  $_SESSION['EMAIL'];
        $properties["LIKES"] =  0;

        $full_picture = CFile::ResizeImageGet($_SESSION['FILE_ID'], array('width'=>800, 'height'=>600), BX_RESIZE_IMAGE_EXACT);
        $square_picture = CFile::ResizeImageGet($_SESSION['FILE_ID'], array('width'=>210, 'height'=>210), BX_RESIZE_IMAGE_EXACT);

        $data = Array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => 35,
            "PROPERTY_VALUES" => $properties,
            "NAME" => $_SESSION['CHILD_NAME'].', '.$_SESSION['CHILD_AGE'].($_SESSION['CHILD_AGE'] > 4 ? ' лет' : ' года'),
            "CODE" => 'PICTURE_'.time(),
            "ACTIVE" => "N",
            "DETAIL_PICTURE" => CFile::MakeFileArray($full_picture['src']),
            "PREVIEW_PICTURE" => CFile::MakeFileArray($square_picture['src']),
        );

        $id = $el->Add($data);
        
        // Отправка уведомления модераторам
        
        $message =  "Поступила новая заявка на участие в конкурсе рисунков.<br>".
                    "ID: $id<br>".
                    "Имя: ".$_SESSION['CHILD_NAME']."<br>";
        
        $topic = "Конкурс рисунков - новая заявка";
        
        Mail::S('rumyancev@terem-pro.ru', $topic, $message);
        Mail::S('zolotarev@terem-pro.ru', $topic, $message);
        
        // Удаляем данные сессии

        unset($_SESSION["CONTEST_CODE"]);
        unset($_SESSION["CONTEST_ATTEMPTS"]);
        unset($_SESSION["CONTEST_FAILED"]);
        unset($_SESSION["CHILD_NAME"]);
        unset($_SESSION["CHILD_AGE"]);
        unset($_SESSION["PARENT"]);
        unset($_SESSION["EMAIL"]);
        
        // Устанавливаем в сессию статус завершённой заявки для предотвращения 
        // единовременной отправки многократных заявок
        
        $_SESSION["CONTEST_SENT"] = true;  
    }
}

// Обработка стадии отправки заявки

else 
{
    $image = $_FILES["image"];

    if ((int)$image["size"] > 7340032)
    {
        echo '<p>Размер файла слишком большой<br>Выберете, пожалуйста другое изображение</p>';
        die();
    }
    
    $image_info = getimagesize($image['tmp_name']);
    
    if (!isset($image_info["mime"]) || !in_array($image_info["mime"], ['image/jpeg', 'image/png']))
    {
        echo '<p>Кажется, Вы пытаетесь загрузить совсем не картинку. Выберете другой файл.</p>';
        die();
    }

    $fid = 0; 

    if (CModule::IncludeModule("iblock")) {


        $tmpFile = Array(
            "name" => $image["name"],
            "size" => $image["size"],
            "tmp_name" => $image["tmp_name"],
            "type" => $image["type"],
            "old_file" => "",
            "del" => "y",
            "MODULE_ID" => "iblock"
        );
        $fid = CFile::SaveFile($tmpFile, "/upload/iblock/");  
    }

    $child_name = resetString(trim($_POST['child-name']));

    if (!preg_match('/[а-я]{4,}/i', $child_name))
    {
        echo '<p>Введите пожалуйста настоящее имя ребёнка!</p>';
        die();
    }

    $parent = resetString(trim($_POST['parent']));

    $email = resetString(trim($_POST['email']));

    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        echo '<p>Введите пожалуйста корректный адрес электронной почты!</p>';
        die();
    }

    $child_age = (int)$_POST['child-age'];

    if (!$child_age)
    {
        echo '<p>Введите пожалуйста настоящий возраст ребёнка!</p>';
        die();
    }
    
    if (($child_age < 14) && !preg_match('/[а-я]{4,}/i', $parent))
    {
        echo '<p>Введите пожалуйста ФИО законного представителя (родителя, опекуна и т.д.)</p>';
        die();
    }

    $_SESSION["CONTEST_CODE"] = rand(1000, 9999);
    $_SESSION["CONTEST_ATTEMPTS"] = 0;
    
    $_SESSION["CHILD_NAME"] = $child_name;
    $_SESSION["CHILD_AGE"] = $child_age;
    $_SESSION["PARENT"] = $parent ?: '';
    $_SESSION["EMAIL"] = $email;
    $_SESSION['FILE_ID'] = $fid;

    // Отправка кода подтверждения на почту клиенту

    $message =  "Ваш рисунок уже почти учавствует в конкурсе!<br>".
                "Для завершения регастрации введите в поле формы следующий код: <span style=\"color:red;\"><b>".
                $_SESSION["CONTEST_CODE"].
                "</b></span><br>".
                "-------------------------------------------------<br><br>".
                'Если Вы не оставляли заявок на сайте <a href="http://terem-pro.ru">terem-pro.ru</a>, просто проигнорируйте данное письмо';

    Mail::S($email, "Код подтверждения заявки на участие в конкурсе", $message);

    // В ajax возвращаем статус того, что код присвоен 

    echo CONFIRMATION_CODE_GIVEN; 

}


