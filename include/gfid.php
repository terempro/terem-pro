<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (CModule::IncludeModule("iblock")) {

    $list = array(
        'ID,Item title,Final URL,Image URL,Item subtitle,Item description,Item category,Price,Sale price,Contextual keywords,Item address,Tracking template,Custom parameter',
    );
    $fp = fopen('file.csv', 'w');
    
    foreach ($list as $line) {
        fputcsv($fp, split(',', $line));
    }

    $HOUSES = Array();

    $arSelect = Array("ID", "NAME", "CODE", "PROPERTY_LINK", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID", "PROPERTY_VARIANTS_HOUSE");
    $arFilter = Array("IBLOCK_ID" => 13, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize" => 5000), $arSelect);
    $cnt = 0;
    while ($ob = $res->Fetch()) {
        $HOUSES[$cnt]['ID'] = $ob["ID"];
        $HOUSES[$cnt]['Item_title'] = $ob["NAME"];
        $HOUSES[$cnt]['Final_URL'] = $ob["IBLOCK_SECTION_ID"];
        $HOUSES[$cnt]['Image_URL'] = 'http://www.terem-pro.ru' . CFile::GetPath($ob["PREVIEW_PICTURE"]);
        $HOUSES[$cnt]['Item_subtitle'] = '';
        $HOUSES[$cnt]['Item_description'] = 'Максимальная длина – 25 символов (12 для языков с двухбайтовыми символами).';
        $HOUSES[$cnt]['Item_category'] = '';
        $HOUSES[$cnt]['Price'] = $ob["PROPERTY_VARIANTS_HOUSE_VALUE"][0];
        $HOUSES[$cnt]['Sale_price'] = $ob["PROPERTY_VARIANTS_HOUSE_VALUE"][0];
        $HOUSES[$cnt]['Contextual_keywords'] = '';
        $HOUSES[$cnt]['Item_address'] = '';
        $HOUSES[$cnt]['Tracking_template'] = '';
        $HOUSES[$cnt]['Custom_parameter'] = '';
        $LINK[$cnt] = $ob["CODE"];
        $cnt++;
    }

    foreach ($HOUSES as $k => $house) {
        $arSelect1 = Array("PREVIEW_TEXT", "PROPERTY_PRICE_HOUSE");
        $arFilter1 = Array("IBLOCK_ID" => 21, "ID" => $house['Price']);
        $res1 = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter1, false, Array("nPageSize" => 5000), $arSelect1);

        while ($ob1 = $res1->Fetch()) {
            $HOUSES[$k]['Item_description'] = "Дом от компании Теремъ";
            $res2 = CIBlockElement::GetProperty(22, $ob1["PROPERTY_PRICE_HOUSE_VALUE"], "sort", "asc", array());
            while ($ob2 = $res2->GetNext()) {
                $PRICE[$k][] = $ob2['VALUE'];
            }
        }

        $HOUSES[$k]['Price'] = number_format($PRICE[$k][1], 3, '.', '.')." RUB";
        $HOUSES[$k]['Sale_price'] = number_format($PRICE[$k][0], 3, '.', '.')." RUB";

        $res = CIBlockSection::GetByID($HOUSES[$k]['Final_URL']);
        if ($ar_res = $res->GetNext()) {
            $HOUSES[$k]['Final_URL'] = 'http://www.terem-pro.ru/catalog/' . $ar_res['CODE'] . '/' . $LINK[$k] . '/';
            $HOUSES[$k]['Item_category'] = $ar_res['NAME'];
        }
    
        fputcsv($fp, split(',', implode(",", $HOUSES[$k])));
    }

    //echo "<pre>";
    //print_r($HOUSES);
    fclose($fp);
}
