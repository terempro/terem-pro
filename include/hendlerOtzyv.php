<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if ($_POST) {
    $user = $_POST['user'];
    $email = $_POST['email'];
    $company = $_POST['company'];
    $city = $_POST['city'];
    $gorod = $_POST['gorod'];
    $title = $_POST['title'];
    $text = $_POST['text'];
	
	$error = false;
    $error2 = false;
    $error3 = false;
	
	
	if (is_string($user)) {
        $user = trim($user);
            if (preg_match("/[а-я]/i", $user)) {
               $user = strip_tags($user);
               $error = true;
            }
    }

    if (CModule::IncludeModule("iblock")) {
        $el = new CIBlockElement;

        $PROP = array();
        $PROP[112] = $city;  // свойству с кодом 12 присваиваем значение "Белый"
        $PROP[113] = $title;        // свойству с кодом 3 присваиваем значение 38
        $PROP[114] = $text;        // свойству с кодом 3 присваиваем значение 38

        $arLoadProductArray = Array(
            "MODIFIED_BY" => 1, // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
            "IBLOCK_ID" => 31,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $user,
            "ACTIVE" => "N", // активен
            "PREVIEW_TEXT" => $text,
        );
        $msg = '';
        if ($PRODUCT_ID = $el->Add($arLoadProductArray)){
            $msg = $PRODUCT_ID;
        }
        else{
            $msg = $el->LAST_ERROR;
        }	
        //var_dump($msg);
    }
	
	// отправляем запрос в парсер /include/request_handler.php
	
	$params = array(
		'request_type' => '36',
		'name' => $user,
		// 'phone' => ,
		'mail' => $email,		
	);
	
	$myCurl = curl_init();
	curl_setopt_array($myCurl, Array(
		CURLOPT_URL => 'https://www.terem-pro.ru/include/request_handler.php',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST => 1,
		CURLOPT_POSTFIELDS => $params,
	));
	$response = curl_exec($myCurl);
	curl_close($myCurl);	
}

