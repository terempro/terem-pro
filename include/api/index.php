<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once 'api.php';

$__get = filter_input(INPUT_GET, 'get', FILTER_SANITIZE_SPECIAL_CHARS);


if(strlen($__get) > 0){
    $o = new ApiClass;
    
    if($__get == 'category'){
        $o->getCategories();
    }
    if($__get == 'series'){
        $o->getSeries();
    }
    if($__get == 'seriesreal'){
        $o->getSeriesReal();
    }
    if($__get == 'tech'){
        $o->getTehnology();
    }
    if($__get == 'house'){
       $o->getHouse();
    }
    if($__get == 'price'){
       $o->getPrice();
    }
    if($__get == 'img'){
       $o->getImage();
    }
    if($__get == 'yf'){
        $o->getFY();
    }
    if($__get == 'tb'){
        $o->table();
    }
    if($__get == 'ind'){
        $o->individual();
    }
}


