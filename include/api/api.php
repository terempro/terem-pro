<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");



class ApiClass {

    private $Categories = [];
    private $Series = [];
    private $Tehnology = [];
    private $House = [];
    private $Prices = [];
    private $Images = [];
    private $SeriesReal = [];

    //Собираем корневые разделы каталога
    private function setCategories() {
        if (CModule::IncludeModule("iblock")) {
            $arFilterCat = Array('IBLOCK_ID' => 13);
            $db_list_cat = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilterCat, true);
            $cnt = 0;
            while ($ar_result_cat = $db_list_cat->GetNext()) {
                $this->Categories[$cnt]['company_id'] = 2;
                $this->Categories[$cnt]['url'] = 'http://terem-pro.ru'.$ar_result_cat["SECTION_PAGE_URL"];
                $this->Categories[$cnt]['name'] = $ar_result_cat["NAME"];
                $cnt++;
            }
        }
    }

    public function individual(){
        if (CModule::IncludeModule("iblock")) {
            $arSelect = Array(
                "ID",
                "NAME",
                "PROPERTY_NAME_H",
                "SECTION",
                "SECTION_ID",
                "DETAIL_PICTURE",
                "PREVIEW_PICTURE",
                "DETAIL_PAGE_URL",
                "PROPERTY_IMG_1",
                "PROPERTY_MODEL",
                "PROPERTY_S1_H",
                "PROPERTY_PLAN1",
                "PROPERTY_PLAN2"
            );

            $arFilter = Array(
                "IBLOCK_ID" => IntVal(28),
                "ACTIVE_DATE" => "Y",
                "ACTIVE" => "Y"
            );
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 500), $arSelect);
            $cnt = 0;

            echo "<table border='1' width='850' style='border-collapse:collapse;'>";
            while ($ob = $res->GetNextElement()) {
                $result = $ob->GetFields();

              
				
				

                    echo "<tr>";
                    echo "<td>";
                    echo $result["PROPERTY_NAME_H_VALUE"].' '.$result["PROPERTY_MODEL_VALUE"];
                    echo "</td>";
                    echo "<td>";
                    $str = $result["PROPERTY_MODEL_VALUE"].'.jpg';
                    echo trim($str);
                    echo "</td>";
                    echo "<td>";
                    echo '<img src="'.CFile::GetPath($result["PROPERTY_IMG_1_VALUE"]).'" alt="" title="" width="150"/>';
                    echo "</td>";
                echo "<td>";
                echo '<img src="'.CFile::GetPath($result["PROPERTY_PLAN1_VALUE"]).'" alt="" title="" width="150"/>';
                echo "</td>";
                echo "<td>";
                echo '<img src="'.CFile::GetPath($result["PROPERTY_PLAN2_VALUE"]).'" alt="" title="" width="150"/>';
                echo "</td>";

            }
            echo "</table>";
			
			  
        }
    }

    public function table(){
        $this->setHouse(600);

      

        echo "<table border='1' width='850' style='border-collapse:collapse;'>";
            foreach($this->House as $item){
               echo "<tr>";
                    echo "<td>";
                        echo $item['title'].' '.$item['width'].'x'.$item['length'];
                    echo "</td>";
                    echo "<td>";
                        $str = $item['title'].''.$item['width'].'x'.$item['length'].'jpg';
                        echo trim($str);
                    echo "</td>";
                    echo "<td>";
                        echo '<img src="'.$item['d_img'].'" alt="" title="" width="150"/>';
                    echo "</td>";



                echo "<td>";
                echo "</td>";
                echo "<td>";

                if($item['plna_1']){
                    echo "<table >";
                    echo "<tr>";
                    echo "<td>";
                    echo "Планировка 2";
                    echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td>";
                    echo '<img src="'.$item['plna_1'].'" alt="" title="" width="150"/>';
                    echo "</td>";
                    echo "</tr>";
                    echo "</table>";
                }



                echo "</td>";


                echo "<td>";
                echo "</td>";
                echo "<td>";
                echo "<table >";
                echo "<tr>";
                    echo "<td>";
                    echo "Планировка 2";
                    echo "</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>";
                    echo '<img src="'.$item['plna_2'].'" alt="" title="" width="150"/>';
                    echo "</td>";
                echo "</tr>";
                echo "</table>";
                echo "</td>";


                echo "<td>";
                echo "</td>";
                echo "<td>";
                echo "<table>";
                foreach($item["interier"] as $v){

                    echo "<tr>";
                    echo "<td>";
                    echo "Интерьер";
                    echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td>";
                    echo '<img src="'.$v.'" alt="" title="" width="150"/>';
                    echo "</td>";
                    echo "</tr>";

                }
                echo "</table>";
                echo "</td>";

                echo "<td>";
                echo "</td>";
                echo "<td>";
                echo "<table>";
                $cnt = 0;
                $cnt2 = 0;
                $all = count($item["exterier"]);

                    foreach($item["exterier"] as $v){
                        if($cnt == 0){
                            echo "<tr>";
                        }

                        echo "<tr>";
                        echo "<td>";
                        echo "Экстерьер";
                        echo "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<td>";
                        echo '<img src="'.$v.'" alt="" title="" width="150"/>';
                        echo "</td>";
                        echo "</tr>";


                    $cnt++;
                        $cnt2++;
                        if($cnt2 == $all || $cnt ==2){
                            $cnt = 0;
                            echo "</tr>";
                        }
                    }



                echo "</table>";
                echo "</td>";

                echo "</tr>";
            }
        echo "</table>";
    }

    public function getFY(){
        $xml = new SimpleXMLElement('<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06"></realty-feed>');
        $xml->formatOutput = TRUE;
        $version = $xml->addChild('generation-date', date('Y-m-d').'T'.date('H:i:s').'+03:00');



        $this->setHouse(500);
        $this->setSeries();
        $this->setSeriesReal();
        $this->setPrice();

        $FinalArr = $this->SeriesReal;
        $NewArr = [];


        foreach($FinalArr as $k => $v){
            if($v['section'] == '29123'){
                $NewArr[] = $v;
            }
        }

        foreach($NewArr as $k => $v){
            foreach($v['house_bitrix_id'] as $val){
                foreach($this->House as $h){
                    if($val == $h['id']){
                        $NewArr[$k][$val]['price_ids'] = $h["price_id"];
                        $NewArr[$k][$val]['picture'] = $h["picture"];
                        $NewArr[$k][$val]['overall_area'] = $h["overall_area"];
                        foreach($this->Prices as $p){
                            if($p['id'] == $h["price_id"]){
                                $NewArr[$k][$val]['price_base'] = $p["price_base"];
                                $NewArr[$k][$val]['price_optimal'] = $p["price_optimal"];
                            }
                        }
                    }
                }
            }
        }

//        echo "<pre>";
//        var_dump($NewArr);
//        die();

        foreach($NewArr as $data) {




            $offer = $xml->addChild('offer');
            $offer->addAttribute('internal-id', $data['id']);
            $type = $offer->addChild('type', 'продажа');
            $property_type = $offer->addChild('property-type', 'жилая');
            $category = $offer->addChild('category', 'коттедж');
            $commercial_type = $offer->addChild('commercial-type', 'free purpose');
            $url = $offer->addChild('url', 'url');
            $creation_date = $offer->addChild('creation-date', date('Y-m-d') . 'T' . date('H:i:s') . '+03:00');
            $last_update_date = $offer->addChild('last-update-date', date('Y-m-d') . 'T' . date('H:i:s') . '+03:00');
            $location = $offer->addChild('location');
            $country = $location->addChild('country', 'Россия');
            $sales_agent = $offer->addChild('sales-agent');
            $name = $sales_agent->addChild('name', 'ООО Теремъ-про');
            $phone = $sales_agent->addChild('phone', '+7 (495) 213-89-44');
            $category = $sales_agent->addChild('category', 'застройщик');
            $url = $sales_agent->addChild('url', 'http://terem-pro.ru/');
            $price = $offer->addChild('price');
            $value = $price->addChild('value', $data[$data['house_bitrix_id'][0]]["price_optimal"].'000');
            $currency = $price->addChild('currency', 'RUB');
            $deal_status = $offer->addChild('deal-status', 'продажа от застройщика');
            $area = $offer->addChild('area');
            $value = $area->addChild('value', $data[$data['house_bitrix_id'][0]]["overall_area"]);
            $unit = $area->addChild('unit', 'кв. м');
            $image = $offer->addChild('image', 'http://terem-pro.ru'. $data[$data['house_bitrix_id'][0]]["picture"]);
            $floors_total = $offer->addChild('floors-total', '2');

        }


        $xml->asXML('feeds/' . 'feed_yandex.xml');




        echo 123;
//        echo "<pre>";
//        var_dump($NewArr);
    }

    private function setSeriesReal($count = 500){
        if (CModule::IncludeModule("iblock")) {

            $arSelect = Array(
              "ID",
              "NAME",
              "CODE",
              "SECTION",
              "SECTION_ID",
              "DETAIL_PAGE_URL",
              "PROPERTY_VARIANTS_HOUSE",
              "PROPERTY_TEHNOLOGY_FILTER",
                "PROPERTY_SERIES"
            );

            $arFilter = Array(
              "IBLOCK_ID" => IntVal(13),
              "ACTIVE_DATE" => "Y",
              "ACTIVE" => "Y"
            );
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => $count), $arSelect);
                $cnt = 0;
                while ($ob = $res->GetNextElement()) {
                    $result = $ob->GetFields();

                    $SECTION_ID = '';
                    $r = CIBlockElement::GetByID($result["ID"]);
                    if($ar_res = $r->GetNext()){
                        $SECTION_ID = $ar_res["IBLOCK_SECTION_ID"];
                    }

                    $this->SeriesReal[$cnt]['company_id'] = 2; 
                    $this->SeriesReal[$cnt]['category_id'] = 0;
                    $this->SeriesReal[$cnt]['name'] = $result["PROPERTY_SERIES_VALUE"];
                    $this->SeriesReal[$cnt]['url'] = 'http://terem-pro.ru'.$result['DETAIL_PAGE_URL'];
                    $cnt++;
                }



        }
    }

    //Собираем все серии
    private function setSeries() {
        if (CModule::IncludeModule("iblock")) {
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => 13, "CODE" => "SERIES"));
            $cnt = 0;
            while ($enum_fields = $property_enums->GetNext()) {
                $this->Series[$cnt]['company_id'] = 2;
                $this->Series[$cnt]['category_id'] = 0;
                $this->Series[$cnt]['url'] = 'http://terem-pro.ru';
                $this->Series[$cnt]['name'] = $enum_fields["VALUE"];
                $cnt++;
            }
        }
    }

    //Собираем все технологии
    private function setTehnology() {
        if (CModule::IncludeModule("iblock")) {
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => 21, "CODE" => "TEHNOLOGY_HOUSE"));
            $cnt = 0;
            while ($enum_fields = $property_enums->GetNext()) {
                $this->Tehnology[$cnt]['id'] = $enum_fields["ID"];
                $this->Tehnology[$cnt]['title'] = $enum_fields["VALUE"];
                $cnt++;
            }
        }
    }

    //Собираем дома
    public function setHouse($count = 500) {
        if (CModule::IncludeModule("iblock")) {

            $arMySelect = Array(
                "ID",
                "SECTION_ID",
                "NAME",
                "DETAIL_PAGE_URL",
				"DETAIL_PICTURE",
				"PREVIEW_PICTURE",
                "PROPERTY_SIZE_HOUSE",
                "PROPERTY_SQUARE_ALL_HOUSE",                
                "PROPERTY_TEHNOLOGY_HOUSE",
                "PROPERTY_FLOOR",
                "PROPERTY_PRICE_HOUSE.PROPERTY_PRICE_1",
                "PROPERTY_PRICE_HOUSE.PROPERTY_PRICE_2",
                "PROPERTY_SALE",
				"PROPERTY_PLANS_HOUSE_1",
				"PROPERTY_PLANS_HOUSE_2",
				"PROPERTY_NITERIER",
				"PROPERTY_EXTERIER",

            );

            $arFilter = Array(
                "IBLOCK_ID" => IntVal(21),
                "ACTIVE_DATE" => "Y",
                "ACTIVE" => "Y"
            );

            //echo "<pre>";
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => $count), $arMySelect);
           
            $cnt = 0;
            while ($ob = $res->GetNextElement()) {
                
                $result = $ob->GetFields();
                
				

                $size = explode("х", $result["PROPERTY_SIZE_HOUSE_VALUE"]);
                $width = round($size[0]);
                $length = round($size[1]);


                
                
                $tech = 'К';
                
                if (preg_match('/рус/', $result["PROPERTY_TEHNOLOGY_HOUSE_VALUE"])) $tech = 'Б';
                if (preg_match('/клееный/', $result["PROPERTY_TEHNOLOGY_HOUSE_VALUE"])) $tech = 'БК';
                if (preg_match('/ирпич/', $result["PROPERTY_TEHNOLOGY_HOUSE_VALUE"])) $tech = 'К';

                $this->House[$cnt]['company_id'] = 2;
                $this->House[$cnt]['category_id'] = 0;
                $this->House[$cnt]['series_id'] = 0;
                $this->House[$cnt]['url'] = 'http://terem-pro.ru/';
                $this->House[$cnt]['name'] = $result['NAME'];
                $this->House[$cnt]['floor'] = (int)$result['PROPERTY_FLOOR_VALUE'];
                $this->House[$cnt]['width'] = $width;
                $this->House[$cnt]['length'] = $length;
                $this->House[$cnt]['area'] = round($result["PROPERTY_SQUARE_ALL_HOUSE_VALUE"]);
                $this->House[$cnt]['technology'] = $tech;
                $this->House[$cnt]['realprice'] = (int)$result['PROPERTY_PRICE_HOUSE_PROPERTY_PRICE_1_VALUE']*1000;
                $this->House[$cnt]['bigprice'] = (int)$result['PROPERTY_PRICE_HOUSE_PROPERTY_PRICE_2_VALUE']*1000;
                $this->House[$cnt]['sale'] = (int)$result['PROPERTY_SALE_VALUE'];
				
				$this->House[$cnt]['d_img'] = CFile::GetPath($result['DETAIL_PICTURE']);
				$this->House[$cnt]['p_img'] = CFile::GetPath($result['PREVIEW_PICTURE']);
				$this->House[$cnt]['plna_1'] = CFile::GetPath($result['PROPERTY_PLANS_HOUSE_1_VALUE']);
				$this->House[$cnt]['plna_2'] = CFile::GetPath($result['PROPERTY_PLANS_HOUSE_2_VALUE']);
				
				if(count($result["PROPERTY_NITERIER_VALUE"])){
					foreach($result["PROPERTY_NITERIER_VALUE"] as $img_id){
						$this->House[$cnt]['interier'][] = CFile::GetPath($img_id);
					}
				}
				
				if(count($result["PROPERTY_EXTERIER_VALUE"])){
					foreach($result["PROPERTY_EXTERIER_VALUE"] as $img_id){
						$this->House[$cnt]['exterier'][] = CFile::GetPath($img_id);
					}
				}
				
				
				
                
                $cnt++;
            }
        }
    }

    //Собираем цены
    private function setPrice($count = 500) {
        if (CModule::IncludeModule("iblock")) {

            $arSelect = Array(
                "ID",
                "NAME",
                "DATE_ACTIVE_FROM",
                "PROPERTY_PRICE_1",
                "PROPERTY_PRICE_2",
                "PROPERTY_OPTIMAL_PRICE",
                "PROPERTY_KEY_PRICE",
            );

            $arFilter = Array(
                "IBLOCK_ID" => IntVal(22),
                "ACTIVE_DATE" => "Y",
                "ACTIVE" => "Y"
            );

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => $count), $arSelect);

            $cnt = 0;
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $this->Prices[$cnt]['id'] = $arFields['ID'];
                $this->Prices[$cnt]['title'] = trim(str_replace('&quot;', "'", $arFields['NAME']));
                $this->Prices[$cnt]['price_base'] = (int) $arFields['PROPERTY_PRICE_1_VALUE'];
                $this->Prices[$cnt]['price_big'] = (int) $arFields['PROPERTY_PRICE_12_VALUE'];
                $this->Prices[$cnt]['price_optimal'] = (int) $arFields['PROPERTY_OPTIMAL_PRICE_VALUE'];
                $this->Prices[$cnt]['price_pacet_key'] = (int) $arFields['PROPERTY_KEY_PRICE_VALUE'];
                $cnt++;
            }
        }
    }

    //Собираем картинки
    private function setImage($count = 500) {


        if (CModule::IncludeModule("iblock")) {

            $arSelect = Array(
                "ID",
                "NAME",
                "DETAIL_PICTURE",
                "PROPERTY_PLANS_HOUSE_1",
                "PROPERTY_PLANS_HOUSE_2",
                "PROPERTY_NITERIER",
                "PROPERTY_EXTERIER",
            );

            $arFilter = Array(
                "IBLOCK_ID" => IntVal(21),
                "ACTIVE_DATE" => "Y",
                "ACTIVE" => "Y"
            );


            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => $count), $arSelect);

            $cnt = 0;
            while ($ob = $res->GetNextElement()) {
                $result = $ob->GetFields();

                $picture = CFile::GetPath($result["DETAIL_PICTURE"]);
                $picture_plan_one = CFile::GetPath($result["PROPERTY_PLANS_HOUSE_1_VALUE"]);
                $picture_plan_two = CFile::GetPath($result["PROPERTY_PLANS_HOUSE_2_VALUE"]);

                $interior = [];
                if (count($result["PROPERTY_NITERIER_VALUE"]) > 0) {
                    foreach ($result["PROPERTY_NITERIER_VALUE"] as $v) {
                        $interior[] = CFile::GetPath($v);
                    }
                }

                $exterior = [];
                if (count($result["PROPERTY_EXTERIER_VALUE"]) > 0) {
                    foreach ($result["PROPERTY_EXTERIER_VALUE"] as $v) {
                        $exterior[] = CFile::GetPath($v);
                    }
                }
                
                $this->Images[$cnt]['house_id'] = $result['ID'];
                $this->Images[$cnt]['picture'] = $picture;
                $this->Images[$cnt]['picture_plan_one'] = $picture_plan_one;
                $this->Images[$cnt]['picture_plan_two'] = $picture_plan_two;
                $this->Images[$cnt]['interior'] = $interior;
                $this->Images[$cnt]['exterior'] = $exterior;


                $cnt++;
            }


        }
    }
    
    
    //Уплавление (геттеры)
    
    public function getCategories() {
        $this->setCategories();
        echo json_encode($this->Categories, JSON_UNESCAPED_UNICODE);
    }

    public function getSeries() {
        $this->setSeries();
        echo json_encode($this->Series, JSON_UNESCAPED_UNICODE);
    }

    public function getSeriesReal(){
        $this->setSeriesReal();
        echo json_encode($this->SeriesReal, JSON_UNESCAPED_UNICODE);
    }

    public function getTehnology() {
        $this->setTehnology();
        echo json_encode($this->Tehnology, JSON_UNESCAPED_UNICODE);
    }

    public function getHouse() {
        $this->setHouse();

        echo json_encode($this->House, JSON_UNESCAPED_UNICODE + JSON_HEX_QUOT + JSON_HEX_APOS);
    }

    public function getPrice() {
        $this->setPrice();
        echo json_encode($this->Prices, JSON_UNESCAPED_UNICODE);
    }

    public function getImage() {
        $this->setImage();
        echo json_encode($this->Images, JSON_UNESCAPED_UNICODE);
    }

}






//$o->getCategories();
//$o->getSeries();
//$o->getTehnology();
//$o->getHouse();
//$o->getPrice();
//$o->getImage();


