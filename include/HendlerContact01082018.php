<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

// ROISTAT BEGIN
$roistatData['roistat'] = isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null;
$roistatData['form'] = null;
$roistatData['name'] = isset($_POST['name']) ? $_POST['name'] : null;
$roistatData['phone'] = isset($_POST['phone']) ? $_POST['phone'] : null;
$roistatData['email'] = isset($_POST['mail']) ? $_POST['mail'] : null;
switch ($_POST['type']) {
    case 'f0':
        $roistatData['form'] = 'Получить бесплатную консультацию';
        break;
    case 'f1':
        $roistatData['form'] = 'Задать вопрос директору';
        break;
    case 'f2':
        $roistatData['form'] = 'Пожаловаться на бригаду';
        break;
    case 'f3':
        $roistatData['form'] = 'Отблагодарить бригаду';
        break;
    case 'f4':
        $roistatData['form'] = 'Обратиться в клиентскую службу';
        break;
    case 'f5':
        $roistatData['form'] = 'Обратиться в рекламный отдел';
        break;
}
if (!empty($roistatData['phone']) || !empty($roistatData['email'])) {
    $connection = curl_init();
    curl_setopt($connection, CURLOPT_URL, 'http://xn----itba2alfdel.su');
    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($connection, CURLOPT_POST, true);
    curl_setopt($connection, CURLOPT_POSTFIELDS, $roistatData);
    $res = curl_exec($connection);
    curl_close($connection);
}
// ROISTAT END

global $APPLICATION;

const ATTEMPTS_EXHAUSTED = -1;
const CONFIRMATION_CODE_GIVEN = -2;
const CLAIM_REGISTERED = -3;
const CRM_REQUEST_REGISTERED = -4;

const QUESTION_TO_DIRECTOR = 1;
const COMPLAIN_ABOUT_BRIGADE = 2;
const THANK_BRIGADE = 3;
const APPLY_TO_CUSTOMER_SERVICE = 4;
const APPLY_TO_ADS_DEPARTMENT = 5;
const CRM_REQUEST = 6;

function CheckClaim($claim_type, $claim_message)
{
    global $mail, $phone;
    
    $_SESSION["CLAIM_CODE"] = rand(1000, 9999);
    $_SESSION["CLAIM_TYPE"] = $claim_type;
    $_SESSION["CLAIM_MESSAGE"] = str_replace('\r\n', '<br>', $claim_message);
    $_SESSION["CLAIM_ATTEMPTS"] = 0;

    if (in_array($claim_type, [CRM_REQUEST, QUESTION_TO_DIRECTOR, COMPLAIN_ABOUT_BRIGADE]))
    {
        $message = "Для подтверждения заявки на сайте terem-pro.ru введите в поле формы следующий код: ".$_SESSION["CLAIM_CODE"];
        $sms = file_get_contents("http://gateway.api.sc/get/?user=terempro8&pwd=QbcEawZw&sadr=TEREM&dadr={$phone}&text=". urlencode($message));       
    }
    else
    {
        $message =  "Ваше онлайн-обращение к администрации почти зарегистрировано.<br>".
                "Для подтверждения заявки введите в поле формы следующий код: <span style=\"color:red;\"><b>".
                $_SESSION["CLAIM_CODE"].
                "</b></span><br>".
                "-------------------------------------------------<br><br>".
                'Если Вы не оставляли заявку на сайте <a href="http://terem-pro.ru">terem-pro.ru</a>, просто проигнорируйте данное письмо';
        Mail::S($mail, 'Код подверждения заявки', $message);
    }
    
    echo CONFIRMATION_CODE_GIVEN; 
}

// Обработка стадии проверки кода подтверждения

if ($_SESSION["CLAIM_CODE"])
{
    // Если код введён 3 раза неверно, преломляем дальнейшие попытки
    
    if (++$_SESSION["CLAIM_ATTEMPTS"] == 3) 
    {
        $_SESSION["CLAIM_FAILED"] = true;
        echo ATTEMPTS_EXHAUSTED; // Сигнал для ajax, что надо вывести сообщение об ошибке
        die();
    }
    
    // Если код введён некорректно, выводим сообщение на экран

    if ($_SESSION["CLAIM_CODE"] != $_POST['confirmation-code'])
    {
        echo 'Введён некорректный код подтверждения!';
    }
    
    // Иначе, завершаем регистрацию заявки и отправляем уведомление администрации
    
    else
    {
        // Сообщение администрации
        
        switch($_SESSION["CLAIM_TYPE"])
        {
            case CRM_REQUEST:
                
                echo CRM_REQUEST_REGISTERED;
                
            break;
            
            case QUESTION_TO_DIRECTOR:
                
            //Mail::S('teremm.pro@yandex.ru', 'ТеремЪ - Вопрос директору', $_SESSION["CLAIM_MESSAGE"]);

			Mail::S('info@terem-pro.ru', 'ТеремЪ - Вопрос директору', $_SESSION["CLAIM_MESSAGE"]);

            
            break;
        
            case COMPLAIN_ABOUT_BRIGADE:
                
                //Mail::S('teremm.pro@yandex.ru', 'ТеремЪ - Жалоба на бриагаду', $_SESSION["CLAIM_MESSAGE"]);
                Mail::S('ivn@terem-pro.ru', 'ТеремЪ - Жалоба на бриагаду', $_SESSION["CLAIM_MESSAGE"]);
				Mail::S('pavlova@terem-pro.ru', 'ТеремЪ - Жалоба на бриагаду', $_SESSION["CLAIM_MESSAGE"]);
				Mail::S('gavrikova@terem-pro.ru', 'ТеремЪ - Жалоба на бриагаду', $_SESSION["CLAIM_MESSAGE"]);
				Mail::S('info@terem-pro.ru', 'ТеремЪ - Жалоба на бриагаду', $_SESSION["CLAIM_MESSAGE"]);
            
            break;
        
            case THANK_BRIGADE:
                
                //Mail::S('teremm.pro@yandex.ru', 'ТеремЪ - Благодарность', $_SESSION["CLAIM_MESSAGE"]);
                Mail::S('ivn@terem-pro.ru', 'ТеремЪ - Благодарность', $_SESSION["CLAIM_MESSAGE"]);
				Mail::S('info@terem-pro.ru', 'ТеремЪ - Благодарность', $_SESSION["CLAIM_MESSAGE"]);
                Mail::S('gavrikova@terem-pro.ru', 'ТеремЪ - Благодарность', $_SESSION["CLAIM_MESSAGE"]);

            
            break;
        
            case APPLY_TO_CUSTOMER_SERVICE:
                
                //Mail::S('teremm.pro@yandex.ru', 'ТеремЪ - Вопрос в клиентский сервис', $_SESSION["CLAIM_MESSAGE"]);
                Mail::S('ivn@terem-pro.ru', 'ТеремЪ - Вопрос в клиентский сервис', $_SESSION["CLAIM_MESSAGE"]);


            
            break;
        
            case APPLY_TO_ADS_DEPARTMENT:
                
                //Mail::S('teremm.pro@yandex.ru', 'ТеремЪ - Вопрос с сайта', $_SESSION["CLAIM_MESSAGE"]);
                Mail::S('gavrikova@terem-pro.ru', 'ТеремЪ - Вопрос с сайта', $_SESSION["CLAIM_MESSAGE"]);


            
            break;
            
        }
        
        // Возвращаем в ajax статус успешной регистрации заявки
        
        if ($_SESSION["CLAIM_TYPE"] != CRM_REQUEST) echo CLAIM_REGISTERED;
        
        // Удаляем данные сессии
  
        unset($_SESSION["CLAIM_MESSAGE"]);
        unset($_SESSION["CLAIM_TYPE"]);
        unset($_SESSION["CLAIM_ATTEMPTS"]);
        unset($_SESSION["CLAIM_FAILED"]);
        
        // Устанавливаем в сессию статус завершённой заявки для предотвращения 
        // единовременной отправки многократных заявок
        
        $_SESSION["CLAIM_SENT"] = true;
    }
}

// Обработка стадии отправки заявки

elseif ($_POST) 
{
    $name = resetString(trim($_POST["name"]));
    if ($_POST["type"] != 'f0') $msg = resetString(trim($_POST["msg"]));
    
    if (!preg_match("/[а-я]{2,}/i", $name)) 
    {
        echo 'Введите пожалуйста своё имя на русском языке';
        die();
    }
    
    if (isset($_POST['phone']))
    {
    
        $phone = preg_replace('/[^\d]/', '', resetString(trim($_POST["phone"])));
    
        if (!$phone || !preg_match('/79(\d{9})/', $phone)) 
        {
            echo 'Перепроверьте введённый номер телефона. Кажется, Вы допустили ошибку';
            die();
        }
        
    }
    
    if (($_POST["type"] != 'f0') && !$APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_sid"]))
    {
        echo 'Код с картинки введён неверно';
        die();
    }
    
    if (isset($_POST['mail']))
    {
    
        $mail = resetString(trim($_POST["mail"]));

        if (!filter_var($mail, FILTER_VALIDATE_EMAIL))
        {
            echo 'Пожалуйста, введите корректный адрес электронной почты!';
            die();
        }
    
    }
    
    switch($_POST["type"]) 
    {
        case 'f0':
            
            CheckClaim(
                CRM_REQUEST,
                ''
                );
            
            break;
            
        case "f1":
            
            CheckClaim(
                QUESTION_TO_DIRECTOR, 
                'Вопрос директору с сайта.\r\nИмя - '.$name.'\r\nТелефон - '.$phone.'\r\n\r\nСообщение - '.$msg
                );
            
            break;
        
        case "f2":
            
            $number = (int)$_POST['number'];
            
            CheckClaim(
                COMPLAIN_ABOUT_BRIGADE, 
                'Жалоба на бригаду с сайта.\r\nИмя - '.$name.'\r\nТелефон - '.$phone.'\r\n\r\nСообщение - '.$msg.'\r\n Номер бригады - '.$number
                );
            
            break;
        
        case "f3":
            
            CheckClaim(
                THANK_BRIGADE, 
                'Благодарность с сайта.\r\nИмя - '.$name.'\r\nПочта - '.$mail.'\r\n\r\nСообщение - '.$msg
                );
            
            break;
        
        case "f4":
            
            CheckClaim(
                APPLY_TO_CUSTOMER_SERVICE, 
                'Вопрос с сайта.\r\nИмя - '.$name.'\r\nПочта - '.$mail.'\r\n\r\nСообщение - '.$msg
                );
            
            break;
        
        case "f5":
            
            CheckClaim(
                APPLY_TO_ADS_DEPARTMENT, 
                'Вопрос с сайта.\r\nИмя - '.$name.'\r\nПочта - '.$mail.'\r\n\r\nСообщение - '.$msg
                );
            
            break;
    }
   
}
