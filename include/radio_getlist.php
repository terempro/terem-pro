<?php
    $list = scandir($_SERVER['DOCUMENT_ROOT'].'/upload/records/');
    header('Content-Type: application/json');
    if (count($list)){
        foreach ($list as $idx => $item){
            $item = preg_replace('/(\d+)\.(.+)/', '$1', $item);
            if (preg_match('/\.+/', $item)){
                unset($list[$idx]);
            } else {
                $list[$idx] = $item;
            }            
        }
        sort($list);
        echo json_encode($list);
    }
?>