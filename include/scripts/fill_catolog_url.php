<?php
	/**
	 * @author Evgeniy Semashko
	 * @version v 0.1
	 * @example fill price ID = 13
	 * root/include/scripts/fill_catalog_url.php?priceID=13
	 *
	 * Script for autofill property "ссылка на дом" of prices infoblock
	 */
	require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

	if ( ! (is_int($_REQUEST["priceID"]) && ($_REQUEST["priceID"] > 0))) {
		exit ("incorrect param priceID, exit...");
	}

	$arSelect = Array(
		"ID",
		"IBLOCK_ID",
		"NAME",
		"DATE_ACTIVE_FROM",
		"PROPERTY_*"
	);//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
	$arFilter = Array("IBLOCK_ID" => $_REQUEST["priceID"]);
	$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		print_r($arFields);
		$arProps = $ob->GetProperties();
		print_r($arProps);
	}

	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");