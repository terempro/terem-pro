<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

/*// ROISTAT BEGIN
$roistatData['roistat']  = isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null;
$roistatData['form'] = 'Рассчитать проект';
$roistatData['name'] = isset($_POST['name']) ? $_POST['name'] : null;
$roistatData['phone'] = isset($_POST['phone']) ? $_POST['phone'] : null;
$roistatData['email']  = isset($_POST['mail']) ? $_POST['mail'] : null;
if (!empty($roistatData['phone']) || !empty($roistatData['email'])) {
    $connection = curl_init();
    curl_setopt($connection, CURLOPT_URL, 'http://xn----itba2alfdel.su');
    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($connection, CURLOPT_POST, true);
    curl_setopt($connection, CURLOPT_POSTFIELDS, $roistatData);
    $res = curl_exec($connection);
    curl_close($connection);
}
// ROISTAT END*/

const ATTEMPTS_EXHAUSTED = -1;
const CONFIRMATION_CODE_GIVEN = -2;

if (!CModule::IncludeModule("iblock")) die();

// Обработка стадии проверки кода подтверждения

if ($_SESSION["INDIVIDUAL_PROJECT_CODE"])
{
    // Если код введён 3 раза неверно, преломляем дальнейшие попытки
    
    if (++$_SESSION["INDIVIDUAL_PROJECT_ATTEMPTS"] == 3) 
    {
        $_SESSION["INDIVIDUAL_PROJECT_FAILED"] = true;
        echo ATTEMPTS_EXHAUSTED; // Сигнал для ajax, что надо вывести сообщение об ошибке
        die();
    }
    
    // Если код введён некорректно, выводим сообщение на экран

    if ($_SESSION["INDIVIDUAL_PROJECT_CODE"] != $_POST['confirmation-code'])
    {
        echo 'Введён некорректный код подтверждения!';
    }
    
    // Иначе, завершаем регистрацию заявки и отправляем уведомления клиенту и администрации
    // При успешном завершении в ajax-запрос возвращается id заявки и будет выведено сообщение
    // об успехе
    
    else
    {
        // Извлекаем данные заявки
        
        $arSelect = Array(
            "ID",
            "PROPERTY_NAME", 
            "PROPERTY_COMMENT", 
            "PROPERTY_PHONE",
            "PROPERTY_EMAIL",
            "PROPERTY_DIMENSIONS",
            "PROPERTY_PICTURE_1",
            "PROPERTY_PICTURE_2",
            "PROPERTY_PICTURE_3",
        );
        $arFilter = Array("ID" => $_SESSION["INDIVIDUAL_PROJECT_NUMBER"]);
        $request_data = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect)->GetNext();
        
        // Сообщение клиенту

        $message =  "Ваша заявка №<b>".$request_data["ID"]."</b> принята<br>".
                    "Мы свяжемся с Вами в течение 24 часов для уточнения деталей и отправим расчет проекта в течение 5 дней<br><br>".
                    "Спасибо, что выбираете нас!";

        Mail::S($request_data["PROPERTY_EMAIL_VALUE"], "Ваша онлайн-заявка зарегистрирована", $message);
        
        // Сообщение администрации
        
        $message =  "<h3>Зарегистрирована новая онлайн-заявка на расчёт стоимости индивидуального проекта</h3>".
                    "Клиент: <b>".$request_data["PROPERTY_NAME_VALUE"].'</b><br>'.
                    "E-mail: <b>".$request_data["PROPERTY_EMAIL_VALUE"].'</b><br>'.
                    "Телефон: <b>".$request_data["PROPERTY_PHONE_VALUE"].'</b><br>';
        
        if ($request_data["PROPERTY_DIMENSIONS"])
        {
            $message .= "Размеры: <b>".$request_data["PROPERTY_DIMENSIONS"].'</b><br>';
        }
        
        if ($request_data["PROPERTY_COMMENT_VALUE"])
        {
            $message .= '<br>Комментарий клиента: <br>'.
                        '<i>'.$request_data["PROPERTY_COMMENT_VALUE"].'</i><br>';
        }
        
        $message .= '<br>-----------------------------------------------<br>'.
                    '<sub>Заявка №'.$request_data["ID"].' от '.date('d.m H:i').'</sub><br>';
        
        $m = new Mail();
        // $m->From("info@terem-pro.ru");
        $m->To('architector@terem-pro.ru');
        // $m->To('zolotarev@terem-pro.ru');
        // $m->To('callcentre@terem-pro.ru');
        // $m->To('piv@terem-pro.ru');
        $m->Subject('Новая заявка на расчёт стоимости индивидуально проекта №'.$request_data["ID"]);
        $m->Body($message,  'html');    
        $m->Priority(3);    // приоритет письма
        $m->smtp_on("ssl://SMTP.mastermail.ru", "terem@terem-pro.ru", "postterempass", 465); 
        
        for ($i = 1; $i <= 3; ++$i)
        {
            if ($request_data["PROPERTY_PICTURE_{$i}_VALUE"]) 
            {
                $m->Attach($_SERVER["DOCUMENT_ROOT"].CFile::GetPath($request_data["PROPERTY_PICTURE_{$i}_VALUE"]), 
                           "Проект_".$request_data["PROPERTY_NAME_VALUE"].'_'.$i.'.'.
                           pathinfo(CFile::GetPath($request_data["PROPERTY_PICTURE_{$i}_VALUE"]), PATHINFO_EXTENSION)
                          );
            }
        }
        
        $m->Send();

        // Присваиваем заявке статус активной
        
        $el = new CIBlockElement();
        $el->Update($request_data["ID"], array('ACTIVE' => 'Y'));
        
        // Возвращаем в ajax номер заявки
        
        echo $_SESSION["INDIVIDUAL_PROJECT_NUMBER"];
        
        // Удаляем данные сессии

        unset($_SESSION["INDIVIDUAL_PROJECT_CODE"]);
        unset($_SESSION["INDIVIDUAL_PROJECT_NUMBER"]);
        unset($_SESSION["INDIVIDUAL_PROJECT_ATTEMPTS"]);
        unset($_SESSION["INDIVIDUAL_PROJECT_FAILED"]);
        
        // Устанавливаем в сессию статус завершённой заявки для предотвращения 
        // единовременной отправки многократных заявок
        
        $_SESSION["INDIVIDUAL_PROJECT_SENT"] = true;
        
        // Заодно удаляем все ранее неактивные заявки
        
        $requests_filter = Array("IBLOCK_ID" => 44, "ACTIVE" => "N", "<=DATE_CREATE" => array(ConvertTimeStamp(time()-3600, "FULL")));
        $requests = CIBlockElement::GetList(Array("SORT"=>"ASC"), $requests_filter, false, false, Array("ID"));
        while($r = $requests->GetNext()) CIBlockElement::Delete($r["ID"]);      
    }
}

// Обработка стадии отправки заявки

else 
{
    $name = resetString(trim($_POST["name"]));

    if (!$name)
    {
        echo 'Пожалуйста, введите своё имя!<br>';
        die();
    }

    if (strlen($name) < 2)
    {
        echo 'Имя не может быть длинной в одну букву! Пожалуйста, введите корректное имя<br>';
        die();
    }

    $phone = resetString(trim($_POST["phone"]));
    
    if (!$phone)
    {
        echo 'Пожалуйста, введите свой номер телефона!';
        die();
    }
    
    $email = resetString(trim($_POST["mail"]));

    if (!$email)
    {
        echo 'Пожалуйста, введите свой адрес электронной почты!';
        die();
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        echo 'Пожалуйста, введите корректный адрес электронной почты!';
        die();
    }
    
    $dimensions = resetString(trim($_POST["dimensions"]));

    $msg = resetString(trim($_POST["msg"]));

    $images = $_FILES["images"];

    $file_ids = array(); // Массив ID сохраняемых файлов

     foreach ($images["name"] as $key => $photo) 
     {
        // Проверяем расширение (допустимы: jpeg, jpg, png, dwg)

        $name_parts = explode('.', $photo);
        $ext = $name_parts[count($name_parts) - 1];
        if (!in_array($ext , array('jpeg', 'jpg', 'png', 'dwg')))
        {
            echo 'Файл '.$photo.' имеет недопустимое расширение и не может быть загружен<br>';
            die();
        }

        // Проверяем реальный тип файла (допустимы: картинки, чертежи из autocad)

        if (!in_array($images["type"][$key], array('application/octet-stream', 'image/jpeg', 'image/png')))
        {
            echo 'Файл '.$photo.' имеет недопустимый формат и не может быть загружен<br>';
            die();
        }

        $tmpFile = Array(
            "name" => $photo,
            "size" => $images["size"][$key],
            "tmp_name" => $images["tmp_name"][$key],
            "type" => $images["type"][$key],
            "old_file" => "",
            "del" => "Y",
            "MODULE_ID" => "iblock"
        );

        $file_ids[] = CFile::SaveFile($tmpFile, "/upload/iblock/");

        if(count($file_ids) == 3) break; // Три картинки и хватит
     }

    // Добавляем заявку

    $props = array();

    $props["NAME"] = $name;
    $props["PHONE"] = $phone;
    $props["EMAIL"] = $email;
    $props["COMMENT"] = $msg;
    $props["DIMENSIONS"] = $dimensions;

    foreach($file_ids as $key => $fid) $props["PICTURE_".($key+1)] = $fid;
    
    // Проверяем, есть ли уже такой e-mail в базе

    $arSelect = Array("ID");
    $arFilter = Array("IBLOCK_ID" => 44, "ACTIVE" => "Y", "PROPERTY_EMAIL" => $email);
    $result = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
    $email_exists = (bool)$result->SelectedRowsCount();

    $el = new CIBlockElement;

    $fields = array(

        "NAME" => "Заявка от ".$name.", ".date('d.m H:i'),
        "CODE" => "INDIVIDUAL_PROJECT_".time(),
        "IBLOCK_ID" => 44,
        "ACTIVE_FROM" => date('d.m.Y H:i:s'),
        "ACTIVE" => $email_exists ? "Y" : "N",
        "PROPERTY_VALUES" => $props

    );

    if (!($id = $el->Add($fields))) echo 'Возникла непредвиденная ошибка!';
    else 
    {
        // Если email есть, минуем стадию подтверждения заявки и сразу отправляем уведомления
        
        if ($email_exists)
        {
            $_SESSION["INDIVIDUAL_PROJECT_SENT"] = true;
            
            // Сообщение клиенту

            $message =  "Добрый день!<br>".
                        "Ваша заявка №<b>".$id."</b> принята<br>".
                        "Мы свяжемся с Вами в течение 24 часов для уточнения деталей и отправим расчет проекта в течение 5 дней<br><br>".
                        "Спасибо, что выбираете нас!";

            Mail::S($email, "Ваша онлайн-заявка зарегистрирована", $message);
            
            // Сообщение администрации
            
            $message =  "<h3>Зарегистрирована новая онлайн-заявка на расчёт стоимости индивидуального проекта</h3>".
                        "Клиент: <b>".$name.'</b><br>'.
                        "E-mail: <b>".$email.'</b><br>'.
                        "Телефон: <b>".$phone.'</b><br>';

            if ($dimensions)
            {
                $message .= "Размеры: <b>".$dimensions.'</b><br>';
            }

            if ($msg)
            {
                $message .= '<br>Комментарий клиента: <br>'.
                            '<i>'.$msg.'</i><br>';
            }

            $message .= '<br>-----------------------------------------------<br>'.
                        '<sub>Заявка №'.$id.' от '.date('d.m H:i').'</sub><br>';

            $m = new Mail();
            $m->From("info@terem-pro.ru");
            $m->To('architector@terem-pro.ru');
            $m->To('zolotarev@terem-pro.ru');
            $m->To('callcentre@terem-pro.ru');
            $m->To('piv@terem-pro.ru');
            $m->Subject('Новая заявка на расчёт стоимости индивидуально проекта');
            $m->Body($message,  'html');    
            $m->Priority(3);    // приоритет письма
            $m->smtp_on("ssl://SMTP.mastermail.ru", "terem@terem-pro.ru", "postterempass", 465); 

            for ($i = 1; $i <= 3; ++$i)
            {
                if ($file_ids[$i-1]) 
                {
                    $m->Attach($_SERVER["DOCUMENT_ROOT"].CFile::GetPath($file_ids[$i-1]), 
                               "Проект_".$name.'_'.$i.'.'.
                               pathinfo(CFile::GetPath($file_ids[$i-1]), PATHINFO_EXTENSION)
                              );
                }
            }

            $m->Send();
            
            // возвращаем в ajax ID-заявки
            
            echo $id;
        }
        
        // Иначе, переходим на стадию подтверждения заявки, сохраняя данные в сессию
        
        else
        {
            $_SESSION["INDIVIDUAL_PROJECT_CODE"] = rand(1000, 9999);
            $_SESSION["INDIVIDUAL_PROJECT_NUMBER"] = $id;
            $_SESSION["INDIVIDUAL_PROJECT_ATTEMPTS"] = 0;
            
            // Отправка кода подтверждения на почту клиенту

            $message =  "Уважаемый $name,<br>Ваша онлайн-заявка на расчёт стоимости индивидуального проекта почти зарегистрирована.<br>".
                        "Для подтверждения заявки введите в поле формы следующий код: <span style=\"color:red;\"><b>".
                        $_SESSION["INDIVIDUAL_PROJECT_CODE"].
                        "</b></span><br>".
                        "-------------------------------------------------<br><br>".
                        'Если Вы не оставляли заявку на сайте <a href="http://terem-pro.ru">terem-pro.ru</a>, просто проигнорируйте данное письмо';

            Mail::S($email, "Код подтверждения онлайн-заявки", $message);
            
            // В ajax возвращаем статус того, что код присвоен 

            echo CONFIRMATION_CODE_GIVEN; 
        }
    }	
}

?>