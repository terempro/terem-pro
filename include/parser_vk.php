<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <style> 
        .btn.start {
            background: #6e1c92;
            color: #e5dde8;
            font-weight: bold;
            text-decoration: none;
            text-shadow: -1px -1px 1px #1a0225;
            padding: 1em 2em;
            border: 2px solid transparent;
            border-left-color: #8e50a9;
            border-top-color: #8e50a9;
            border-right-color: #2e0e3c;
            border-bottom-color: #2e0e3c;
            border-radius: 4px;
        }
        .btn.start:active {
            padding: 1em 2em;
            border: 2px solid #6e1c92;
        }
    </style>
</head>
<body>
<?php
    $continue = 0; // Продолжить выгрузку товаров с указанного порядкового номера при сортировке по имени товара.
    $test = true; // Включает выгрузку в тестовую группу. В интерфейсе надпись кнопки меняется на "ТЕСТ".
    //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    //$APPLICATION->SetTitle("");

    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
    $products = [];


    $client_id = '6718808'; // ID приложения из настроек приложения
    $client_secret = 'oDPWd2XYO5Qhks7v5RYM'; // Защищённый ключ из настроек приложения
    $redirect_uri = 'https://www.terem-pro.ru/include/parser_vk.php'; // Адрес на который ВК отправит code после аторизации https://trata.ta (страница на которой этот код)
    //$service_key = ''; // Сервисный ключ из настроек приложения
    $group_id = '18239188'; // ID группы товара берется из URL адреса страницы грцппы - это либо кастомное имя, если оно было задано, либо автоматически сгенерированное ВК имя, из которого надо взять только цифры.
    $category_id = 502;
    $version = '5.85'; // Версия API

    if ($test) {
        $client_id = '6708249';
        $client_secret = 's4P3aD5Hz5pBwej1cfdc';
        $group_id = '172461111';
    }

    // Описание - https://vk.com/dev/authcode_flow_user
    $url = 'http://oauth.vk.com/authorize';

    $params = array(
        'client_id'     => $client_id,
        'redirect_uri'  => $redirect_uri,
        'scope'         => 'market, photos', // Првоерить синтаксис
        'response_type' => 'code',
        'v'             => $version,
    );

    echo $link = '<div style="text-align:center;padding:20px;"><a href="' . $url . '?' . urldecode(http_build_query($params)) . '" class="btn start">'.($test?' ТЕСТ':'СТАРТ').'</a></div>';

    // Получение токена после авторизации
    if (isset($_GET['code'])) {
        $result = false;
        $params = array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'code' => $_GET['code'],
            'redirect_uri' => $redirect_uri,
            'v'=>$version,
        );

        $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);
        // Если в ответ получен токен
        if (isset($token['access_token'])) {
            // Получаем ИД группы в которую будем грузить фотки товаров - https://vk.com/dev/groups.getById
            $params = array(
                'group_id'      => $group_id, // Имя группы
                'access_token'  => $token['access_token'], // Токен
                'v'             => $version,
            );
            $group_id = json_decode(file_get_contents('https://api.vk.com/method/groups.getById' . '?' . urldecode(http_build_query($params))), true);
            if (isset($group_id['error'])) {
                echo '<pre>'.print_r($group_id).'</pre>';
                die();
            }

            // НАЧАЛО БЛОКА ПОЛУЧЕНИЯ ТОВАРОВ С ВК
            $params = array(
                'owner_id'      => "-".$group_id['response'][0]['id'], // Имя группы
                'count'         => 200,
                'offset'        => 0,
                'access_token'  => $token['access_token'], // Токен
                'v'             => $version,
            );
            $products_vk = [];
            for ($c = $params['count']; $c == $params['count']; $c = count($products_vk)) {
                $products_vk = array_merge($products_vk, json_decode(file_get_contents('https://api.vk.com/method/market.get' . '?' . urldecode(http_build_query($params))), true));
                if (count($products_vk) == $params['count']) {
                    $params['offset'] += $params['count'];
                }
            }
            
            // Поиск дублей для удаления
            if (isset($products_vk['response']['items']) && count($products_vk['response']['items'])) {
                $temp = [];
                $duplicates = [];
                foreach ($products_vk['response']['items'] as $k => $v) {
                    //echo $id = $v['id'];
                    unset($v['id']);
                    if (!in_array($v['title'], $temp)) {
                        $temp[] = $v['title'];
                        //echo print_r($v, 1);
                    } else {
                        $duplicates[] = $k;
                    }
                }
                echo 'Duplicates: '.print_r($duplicates, 1);
                unset($temp);
                // Удаляем дупликаты из ВК и массива продуктов загруженных с ВК
                $params = array(
                    'owner_id'      => "-".$group_id['response'][0]['id'], // -ID группы
                    'access_token'  => $token['access_token'], // Токен
                    'v'             => $version,
                );
                foreach ($duplicates as $id) {
                    $params['item_id'] = $products_vk['response']['items'][$id]['id'];
                    $product_delete = json_decode(file_get_contents('https://api.vk.com/method/market.delete' . '?' . urldecode(http_build_query($params))), true);
                    unset($products_vk['response']['items'][$id]);
                    echo 'Deleted: '.print_r($product_delete, 1);
                }
                unset($duplicates, $id, $product_delete);
            }
            
            
/*            if ($test) {
                echo '<div class="container">';
                    echo '<div class="row">';
                        foreach ($products_vk['response']['items'] as $pv) {
                            echo '<div class="col-12 col-md-6 col-lg-4">';
                                echo '<img class="img-fluid" src="'.$pv['thumb_photo'].'">';
                                echo '<h4>'.$pv['id'].'. '.$pv['title'].'</h4>';
                                echo '<h5>'.$pv['price']['text'].'</h5>';
                                echo '<p>'.$pv['description'].'</p>';
                            echo '</div>';
                        }
                        unset($pv);
                    echo '</div>';
                echo '</div>';
            }*/
            // КОНЕЦ БЛОКА ПОЛУЧЕНИЯ ТОВАРОВ С ВК
            
            // !!!!!!!!!!!!ВРЕМЕННОЕ РЕШЕНИЕ!!!!!!!
            // Удаляем все товары
            
            //
            
            
            // НАЧАЛО БЛОКА ПОЛУЧЕНИЯ ТОВАРОВ С САЙТА
            $catalog = CIBlockElement::GetList(
                [
                    "name"=>"asc",
                ],
                [
                    "IBLOCK_ID" => 13,
                    "ACTIVE"=>"Y",
                ],
                false,
                [
                    
                ],
                [
                    "NAME",
                    "PROPERTY_KARKAS_VARIANTS_HOUSE.ID",
                ]
            );
            $idx = 0;
            $tmp = [];
            while ($catalog_item = $catalog->GetNextElement()) {
                //$products[$idx] = $catalog_item;
                $catalog_item = $catalog_item->GetFields();
                $products[$idx] = $catalog_item;
                /*$products[$idx] = [
                    "NAME" => $catalog_item["NAME"],
                    //"_CATALOG" => $catalog;
                ];*/
                // Выбираем каркасный вариант дома.
                $variant = CIBlockElement::GetList(
                    [],
                    [

                        "IBLOCK_ID" => 21,
                        "ID" => $catalog_item["PROPERTY_KARKAS_VARIANTS_HOUSE_ID"],
                        //"ACTIVE"=>"Y",

                    ],
                    false,
                    [],
                    [
                        "PREVIEW_TEXT",
                        "PROPERTY_PRICE_HOUSE.ID",
                        "PROPERTY_SIZE_HOUSE",
                        "PROPERTY_SQUARE_ALL_HOUSE",
                        "PROPERTY_SQUARE_LIFE_HOUSE",
                        "PROPERTY_EXTERIER",
                        "DETAIL_PICTURE",
                        "PROPERTY_PLANS_HOUSE_1",
                        "PROPERTY_PLANS_HOUSE_2",
                    ]
                )->GetNextElement()->GetFields();
                $products[$idx] = array_merge($products[$idx], $variant);


                if ($products[$idx]["DETAIL_PICTURE"]) {
                    $products[$idx]["DETAIL_PICTURE"] = CFile::GetPath($products[$idx]["DETAIL_PICTURE"]);
                }
                if ($products[$idx]["PROPERTY_PLANS_HOUSE_1_VALUE"]) {
                    $products[$idx]["PROPERTY_PLANS_HOUSE_1_VALUE"] = CFile::GetPath($products[$idx]["PROPERTY_PLANS_HOUSE_1_VALUE"]);
                }
                if ($products[$idx]["PROPERTY_PLANS_HOUSE_2_VALUE"]) {
                    $products[$idx]["PROPERTY_PLANS_HOUSE_2_VALUE"] = CFile::GetPath($products[$idx]["PROPERTY_PLANS_HOUSE_2_VALUE"]);
                }

                // Выбираем цену.
                $price = CIBlockElement::GetList(
                    [],
                    [

                        "IBLOCK_ID" => 22,
                        "ID" => $variant["PROPERTY_PRICE_HOUSE_ID"],
                        //"<"."PROPERTY_PRICE_1_VALUE" => 0,
                        //"ACTIVE"=>"Y",
                    ],
                    false,
                    [],
                    [
                        "NAME",
                        "PROPERTY_PRICE_1",
                    ]
                )->GetNextElement()->GetFields();
                $products[$idx]["FULLNAME"] = $price['NAME'];
                unset($price['NAME']);
                $products[$idx] = array_merge($products[$idx], $price);
                
                // Если цена нулевая, то не обрабатываем.
                if ($products[$idx]['PROPERTY_PRICE_1_VALUE'] < 1) {
                    continue;
                }
                
                if (preg_match("/-150|\s-\s150/", $products[$idx]['FULLNAME'])) {
                    //echo "<li>{$product['FULLNAME']}</li>";
                    $description = preg_replace("/\s+/", " ", strip_tags($products[$idx]['PREVIEW_TEXT']));
                    $tmp[] = [
                        'id'=>$catalog_item["PROPERTY_KARKAS_VARIANTS_HOUSE_ID"],
                        'name'          => $products[$idx]['NAME'].($products[$idx]['PROPERTY_SIZE_HOUSE_VALUE']?" ".$products[$idx]['PROPERTY_SIZE_HOUSE_VALUE']." м":''), // Название товара от 4 до 100 символов
                        'description'   => strlen($description) > 9? $description: 'Описание находится в разработке', // Описание товара
                        'category_id'   => $category_id, // ИД категории
                        'price'         => $products[$idx]['PROPERTY_PRICE_1_VALUE'], // Цена
                        'pictures'      => [
                            $products[$idx]['DETAIL_PICTURE'],
                            $products[$idx]['PROPERTY_PLANS_HOUSE_1_VALUE'],
                            $products[$idx]['PROPERTY_PLANS_HOUSE_2_VALUE'],
                        ], // Массив строковых ссылок на фото товара
                    ];
                    // Массив массивов с данными каждого продука
                }
                $idx++;
            }
            $products = $tmp;
            unset($catalog, $tmp, $idx);
            // КОНЕЦ БЛОКА ПОЛУЧЕНИЯ ТОВАРОВ
            // НАЧАЛО БЛОКА ОБРАБОТКИ ТОВАРОВ
            
            // КОНЕЦ БЛОКА ОБРАБОТКИ ТОВАРОВ



            

            if (isset($test) && $test) {
                echo '<div style="background:#fff;color:#000;"><ul>';
            }
            
            $file_name = 'parser_vk_data.php';
            $file_content = '<?php $data =[';
            foreach ($products as $idx => $product) {
                sleep(1);
                //echo 'Начало загрузки товара: "'.$product['name'].'"';
                if (isset($continue) && ($continue > 0) && ($idx < $continue)) {
                    continue;
                }
                // Если товар есть в ВК, то обновляем его.
                if (isset($products_vk['response']['items']) && count($products_vk['response']['items'])) {
                    foreach ($products_vk['response']['items'] as $product_vk) {
                        if ($product_vk['title'] == $product['name'] && ($product_vk['price']['amount'] != $product['price'] || $product_vk['description'] != $product['description'] )) {
                            // Получаем товар по ID
                            $params = [
                                'item_ids'      => '-'.$group_id['response'][0]['id'].'_'.$product_vk['id'],
                                'extended'      => 1,
                                'access_token'  => $token['access_token'],
                                'v'             => $version,
                            ];
                            $product_existed = json_decode(file_get_contents('https://api.vk.com/method/market.getById'
                                                        .'?'
                                                        .urldecode(http_build_query($params))), true);
                            if (isset($product_existed['error'])) {
                                echo '<pre>'.print_r($product_existed).'</pre>';
                                die();
                            }
                            
                            // Обновляем выбранный товар
                            $photo_ids = [];
                            foreach ($product_existed['response']['items'][0]['photos'] as $photo_idx => $photo) {
                                if ($photo_idx == 0) {
                                    $main_photo_id = $photo['id'];
                                } else {
                                    $photo_ids[] = $photo['id'];
                                }
                            }
                            /*echo "<pre>".print_r([
                                '$photos'=>$product_existed['response']['items'][0]['photos'],
                                '$main_photo_id'=>$main_photo_id,
                            
                            ]
                                         ,1)."</pre>";*/
                            unset($photo_id);
                            
                            $params = [
                                'owner_id'      => '-'.$group_id['response'][0]['id'],
                                'item_id'       => $product_existed['response']['items'][0]['id'],
                                'name'          => urlencode($product['name']), // Название товара от 4 до 100 символов
                                'description'   => urlencode($product['description']), // Описание товара
                                'category_id'   => $category_id,//$product['category_id'], // ИД категории
                                'price'         => $product['price'], // Цена
                                'main_photo_id' => $main_photo_id, // ИД главной фотки
                                'access_token'  => $token['access_token'],
                                'v'             => $version,
                            ];
                            if ($photo_idx > 0) {
                                $params['photo_ids'] = $photo_ids; // Массив ИД остальных фоток
                            }
                            $product_created = json_decode(file_get_contents('https://api.vk.com/method/market.edit'
                                                        .'?'
                                                        .urldecode(http_build_query($params))), true);
                            if (isset($product_created['error'])) {
                                echo '<pre>'.print_r($product_created).'</pre>';
                                die();
                            }
                            
                            unset($params,$product_existed,$product_created,$main_photo_id,$photo_ids);
                        }
                    }
                    //sleep(1);
                    continue;
                }
                
                if (isset($test) && $test) {
                    echo "<li>".$idx."| ".print_r($product, 1)."</li>";
                }
                set_time_limit(30); // Перезапускаем таймер выполнения скрипта
                $photo_ids = [];
                $main_photo_id = '';
                if (count($product['pictures'])) {
                    $picture_idx = 0;
                    foreach ($product['pictures'] as $picture) {
                        // Загрузить можно не более 1 главной фотки и 4 дополнительных
                        if ($picture_idx < 5) {
                            if ($picture) {
                                // Для загрузки фото получаем адрес сервера на который будем грузить - https://vk.com/dev/photos.getMarketUploadServer
                                // (для загрузки нескольких фото надо поместить в цикл)
                                $params = array(
                                    'group_id'      => $group_id['response'][0]['id'],
                                    'main_photo'    => $picture_idx === 0? 1: 0, // Триггер, является ли фотка главной фоткой товара 1 - Да, 0 - Нет. Первая или единственная фотка будет главной
                                    //'crop_x'      => '', // координата x для обрезки фотографии (верхний правый угол).
                                    //'crop_y'      => '', // координата y для обрезки фотографии (верхний правый угол).
                                    //'crop_width'  => '', // ширина фотографии после обрезки в px. (>400)
                                    'access_token'  => $token['access_token'], // Токен
                                    'v'             => $version,
                                );
                                //echo '<pre>'.print_r($params).'</pre>';

                                $upload_url = json_decode(file_get_contents('https://api.vk.com/method/photos.getMarketUploadServer' . '?' . urldecode(http_build_query($params))), true);

                                // Загружаем фото, не менее 400px по любой из сторон
                                if (isset($upload_url['response']['upload_url'])) {
                                    //Здесь методом POST надо отправить фото на полученный в $upload_url['response']['upload_url'] URL
                                    //$post = ;
                                    $curl_file = curl_file_create(
                                        $_SERVER['DOCUMENT_ROOT'].$picture,
                                        //'image/jpeg',
                                        'multipart/form-data',
                                        explode('/', $picture)[count(explode('/', $picture))-1]
                                    );
                                    $post = ["photo" => $curl_file];

                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, $upload_url['response']['upload_url']);
                                    curl_setopt($ch, CURLOPT_POST, 1);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                    $photo = curl_exec($ch);
                                    curl_close($ch);

                                    $photo_array = json_decode($photo, 1);
                                    // После этого полученный массив отправить для сохранения файлов (тут разрабы загнались не на шутку) - https://vk.com/dev/photos.saveMarketPhoto
                                    $params = array(
                                        'group_id'      => $group_id['response'][0]['id'],
                                        'photo'         => stripslashes($photo_array['photo']),
                                        'server'        => $photo_array['server'],
                                        'hash'          => $photo_array['hash'],
                                        //'crop_data'     => $photo_array['crop_data'],
                                        //'crop_hash'     => $photo_array['crop_hash'],
                                        'access_token'  => $token['access_token'], // Токен
                                        'v'             => $version,
                                    );

                                    // Параметры передаваемые только для основного фото
                                    if ($picture_idx === 0) {
                                        $params['crop_data'] = $photo_array['crop_data'];
                                        $params['crop_hash'] = $photo_array['crop_hash'];
                                    }


                                    $file = json_decode(file_get_contents('https://api.vk.com/method/photos.saveMarketPhoto' . '?' . urldecode(http_build_query($params))), true);
                                    //echo '<pre>'.print_r($file,1).'</pre>';
                                    if (isset($file['error'])) {
                                        echo '<pre>'.print_r($file, 1).'</pre>';
                                        if ($picture_idx === 0) {
                                            die();
                                        }
                                    }
                                } elseif (isset($upload_url['error'])) {
                                    echo '<pre>'.print_r($upload_url).'</pre>';
                                }

                                if (isset($file['response'][0]['id'])) {
                                    // Если загружалась одна фотка, то она и будет главной и фотка с пометкой main_photo=1 сохраним ее ИД в
                                    if ($picture_idx === 0) {
                                        $main_photo_id = $file['response'][0]['id'];
                                    } else {
                                        // Ид планировок, если они есть, сохраним в массив
                                        $photo_ids[] = $file['response'][0]['id'];
                                    }
                                    $picture_idx++;
                                }
                                //sleep(1);
                            }
                        }
                    }
                }

                $params = array(
                    'owner_id'      => '-'.$group_id['response'][0]['id'],
                    'name'          => urlencode($product['name']), // Название товара от 4 до 100 символов
                    'description'   => urlencode($product['description']), // Описание товара
                    'category_id'   => $category_id,//$product['category_id'], // ИД категории
                    'price'         => $product['price'], // Цена
                    'main_photo_id' => $main_photo_id, // ИД главной фотки
                    'access_token'  => $token['access_token'],
                    'v'             => $version,
                );
                if ($picture_idx > 0) {
                    $params['photo_ids'] = $photo_ids; // Массив ИД остальных фоток
                }
                $upload = json_decode(file_get_contents('https://api.vk.com/method/market.add'
                                                        .'?'
                                                        .urldecode(http_build_query($params))), true);
                if (isset($upload['error'])) {
                    echo '<pre>'.print_r($upload, 1).'</pre>';
                    die();
                }
                echo "<pre>".print_r($upload, 1)."</pre>";
                $file_content .= '"'.$product['id'].'"=>"'.$upload['response']['market_item_id'].'",';
            }
            $file_content .= ']; ?>';
            file_put_contents(dirname(__FILE__)."/".$file_name, $file_content);
            if (isset($test) && $test) {
                echo "</ul></div>";
            }
            //sleep(1);
        }
    }
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); 
?>
</body>
</html>