<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
// var_dump($_SERVER);
// ROISTAT BEGIN
//  Temporarily disabled that part of RoiStat statistic on 28.06.2018
/*
$roistatData['roistat'] = isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null;
$roistatData['form'] = null;
$roistatData['name'] = isset($_POST['name']) ? $_POST['name'] : null;
$roistatData['phone'] = isset($_POST['phone']) ? $_POST['phone'] : null;
$roistatData['email'] = isset($_POST['email']) ? $_POST['email'] : null;
if (isset($_POST['request_type'])) {
    switch ($_POST['request_type']) {
        case 1:
            $roistatData['form'] = 'Заказать консультацию';
            $roistatData['phone'] = isset($_POST['telephone']) ? $_POST['telephone'] : null;
            break;
        case 3:
            $roistatData['form'] = 'Онлайн заявка';
            break;
        case 4:
            $roistatData['form'] = 'Заявка на достройку';
            $roistatData['name'] = isset($_POST['Name']) ? $_POST['Name'] : null;
            $roistatData['phone'] = isset($_POST['TelephoneClient']) ? $_POST['TelephoneClient'] : null;
            $roistatData['email'] = isset($_POST['MailClient']) ? $_POST['MailClient'] : null;
            break;
        case 5:
            $roistatData['form'] = 'Оставить заявку';
            break;
        case 6:
            $roistatData['form'] = 'Оформить заявку на кредит';
            $roistatData['phone'] = isset($_POST['telephone']) ? $_POST['telephone'] : null;
            $roistatData['email'] = isset($_POST['user']) ? $_POST['user'] : null;
            break;
        case 7:
            $roistatData['form'] = 'Обратный звонок';
            break;
    }
} else {
    $roiForm = 'Оставить отзыв';
    $roistatData['name'] = isset($_POST['user']) ? $_POST['user'] : null;
}
if (!empty($roistatData['phone']) || !empty($roistatData['email'])) {
    $connection = curl_init();
    curl_setopt($connection, CURLOPT_URL, 'http://xn----itba2alfdel.su');
    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($connection, CURLOPT_POST, true);
    curl_setopt($connection, CURLOPT_POSTFIELDS, $roistatData);
    $res = curl_exec($connection);
    curl_close($connection);
}

*/
// ROISTAT END

CModule::IncludeModule('iblock');

// Request types

const REQUEST_HOUSE = 1;
const REQUEST_COMPLETION = 2;
const REQUEST_ENGINEERING = 3;
const REQUEST_ADDITION = 4;
const REQUEST_FENCE = 5;
const REQUEST_CREDIT_AND_INSURANCE = 6;
const REQUEST_BACK_CALL = 7;
const REQUEST_GROUND = 8;

const REQUEST_KITCHEN = 9;
const REQUEST_HOUSE_AND_KITCHEN = 10;
const REQUEST_KITCHEN_CONSULTING = 11;
const REQUEST_PROJECT_PICKING_UP = 12;

const REQUEST_ALARM = 13;
const REQUEST_ALARM_QUESTIONS_LEFT = 14;
const REQUEST_ALARM_PROJECT_PICKING_UP = 15;
const REQUEST_ALARM_HOUSE = 16;

// New request types 

const REQUEST_PROJECT = 21;
const REQUEST_PROJECT_CALL = 22;
const REQUEST_ADDITIONAL_CONS = 23;
const REQUEST_REBUILDING = 24;
const REQUEST_ELECTRO = 25;
const REQUEST_INNER = 26;
const REQUEST_ADDITION2 = 27;
const REQUEST_CREDIT = 28;
const REQUEST_VISIT = 29; // Лендинг
const REQUEST_CONTACTS1 = 30;
const REQUEST_CONTACTS2 = 31;
const REQUEST_CONTACTS3 = 32;
const REQUEST_CONTACTS4 = 33;
const REQUEST_CONTACTS5 = 34;
const REQUEST_CONTACTS6 = 35;
const REQUEST_ANSWERS = 36;


// Credit & Insurance flags

const CREDIT = 0b1;
const INSURANCE = 0b10;

const CREDIT_TYPES =
[
    CREDIT => "Кредит",
    INSURANCE => "Страхование",
    CREDIT | INSURANCE => "Кредит и страхование"
];

// IBlock

const REQUESTS_IBLOCK = 59;

// Getting data from $_POST
$request_type = (int)$_POST['request_type'];



// if ($request_type < 1 || $request_type > 16) die();

// Initial data



$data = array();

$data["date"] = date('d.m.Y H:i:s');
$data["request_type"] = $request_type;
$data['comment'] = '';
$data["attrs"] = array();
$data['credit_type'] = '';
$data['code_bs'] = '';
$data['entrance_source'] = isset($_COOKIE['entrance_source']) ? htmlspecialchars($_COOKIE['entrance_source']) : '';
// $data['entrance2'] = isset($_COOKIE['entrance2']) ? htmlspecialchars($_COOKIE['entrance2']) : '';
$data['utm_campaign'] = isset($_COOKIE['utm2campaign']) ? htmlspecialchars($_COOKIE['utm2campaign']) : '';
$data['utm_content'] = isset($_COOKIE['utm2content']) ? htmlspecialchars($_COOKIE['utm2content']) : '';
$data['utm_term'] = isset($_COOKIE['utm2term']) ? htmlspecialchars($_COOKIE['utm2term']) : '';
$data['utm_source'] = isset($_COOKIE['utm2source']) ? htmlspecialchars($_COOKIE['utm2source']) : '';
$data['utm_medium'] = isset($_COOKIE['utm2medium']) ? htmlspecialchars($_COOKIE['utm2medium']) : '';
$data['ip'] = $_SERVER['REMOTE_ADDR'];
$data['ua'] = $_SERVER['HTTP_USER_AGENT'];

// client ID из куки гугла, в случае если перестанет приходить, поменять строку
$data['clientid'] = isset($_COOKIE['_ga']) ? substr(htmlspecialchars($_COOKIE['_ga']), 6) : '';
// client ID из куки яндекса
$data['yaid'] = isset($_COOKIE['_ym_uid']) ? substr(htmlspecialchars($_COOKIE['_ym_uid']), 6) : '';


if ($data['utm_source'] == '') {

	$addr = 'Unidentified';

	if (isset($_COOKIE['entrance2'])) {	
		if (strpos($_COOKIE['entrance2'], 'google') > 0) {
			$addr = 'Google Organic';		
		} elseif (strpos($_COOKIE['entrance2'], 'yandex') > 0) {
			$addr = 'Yandex Organic';				
		} 
	} else {
		$addr = 'Direct link';		
	}
	
	$data['utm_source'] = $addr;
}

// список форм сайта

	$siteform = array(
		0 => " ",
		1 => "Консультация каталог раздел",
		2 => "Достройка и реконструкция",
		3 => "Инженерка раздел",
		4 => "Услуги пристройка",
		5 => "Инженерка заборы",
		6 => "Услуги кредитный калькулятор",
		7 => "Заказать звонок (меню)",
		21 => "Прислать проект",
		22 => "Заказать звонок (свой проект)",
		23 => "Консультация каталог товар",
		24 => "",
		25 => "Инженерка электромонтаж",
		26 => "Инженерка водоснаб",		
		27 => "Услуги достройка2",
		28 => "Услуги кредитование",
		29 => "Лендинг",
		30 => "Контакты консультация",
		31 => "Контакты директор",
		32 => "Контакты жалоба бригада",		
		33 => "Контакты награда бригада",
		34 => "Контакты клиентская служба",
		35 => "Контакты рекламный отдел",				
		36 => "О компании отзывы"						
	);

// Specifying data for every form
// var_dump($_COOKIE);
// var_dump($data);
// $data["formname"]  - название формы из которой происходит отправка

switch ($request_type)
{
    case REQUEST_HOUSE:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["telephone"]);
        $data["attrs"][] = ["VALUE" => "Дом", "DESCRIPTION" => htmlspecialchars($_POST['house'])];
        $data["attrs"][] = ["VALUE" => "Код 1С", "DESCRIPTION" => htmlspecialchars($_POST['code'])];
        $data['code_bs'] = htmlspecialchars($_POST['code']);

        break;

    case REQUEST_COMPLETION:

        $data["fio"] = htmlspecialchars($_POST["NameClient"]);
        $data["phone"] = htmlspecialchars($_POST["TelephoneClient"]);

        foreach ($_POST["chk"] as $attr)
        {
            $data["attrs"][] = ["VALUE" => htmlspecialchars($attr), "DESCRIPTION" => ""];
        }

        break;

    case REQUEST_ENGINEERING:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);

        foreach ($_POST["chk"] as $attr)
        {
            $data["attrs"][] = ["VALUE" => htmlspecialchars($attr), "DESCRIPTION" => ""];
        }

        $data["attrs"][] = ["VALUE" => "Время звонка", "DESCRIPTION" => htmlspecialchars($_POST['time'])];

        break;

    case REQUEST_ADDITION:

        $data["fio"] = htmlspecialchars($_POST["LastName"].' '.$_POST['Name'].' '.$_POST['Last2Name']);
        $data["phone"] = htmlspecialchars($_POST["TelephoneClient"]);
        $data["email"] = htmlspecialchars($_POST["MailClient"]);
        $data['comment'] = htmlspecialchars($_POST["CommentsClient"]);

        foreach ($_POST["cheks"] as $attr)
        {
            $data["attrs"][] = ["VALUE" => htmlspecialchars($attr), "DESCRIPTION" => ""];
        }

        break;

    case REQUEST_FENCE:

        $data["fio"] = htmlspecialchars($_POST['name']);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
        $data["attrs"][] = ["VALUE" => "Тип забора", "DESCRIPTION" => htmlspecialchars($_POST['fence_type'])];

        break;

    case REQUEST_CREDIT_AND_INSURANCE:

        $data["fio"] = htmlspecialchars($_POST["famaly"].' '.$_POST['name'].' '.$_POST['lastname']);
        $data["phone"] = htmlspecialchars($_POST['telephone']);
        $data["email"] = htmlspecialchars($_POST["user"]);

        $credit_type = 0;
        if ($_POST['inst_status']) $credit_type += 2;
        if ($_POST['credit_status']) $credit_type += 1;

        $data["attrs"][] = ["VALUE" => "Услуги", "DESCRIPTION" => CREDIT_TYPES[$credit_type]];
        $data['credit_type'] = $credit_type;

        if (isset($_POST['date']))
        {
             $data["attrs"][] = ["VALUE" => "Дата рождения", "DESCRIPTION" => htmlspecialchars($_POST['date'])];
        }

        if (isset($_POST['stimost']))
        {
             $data["attrs"][] = ["VALUE" => "Стоимость дома", "DESCRIPTION" => htmlspecialchars($_POST['stimost'])];
        }

        if (isset($_POST['summ']))
        {
             $data["attrs"][] = ["VALUE" => "Сумма кредита", "DESCRIPTION" => htmlspecialchars($_POST['summ'])];
        }

        if (isset($_POST['length']))
        {
             $data["attrs"][] = ["VALUE" => "Срок кредита", "DESCRIPTION" => htmlspecialchars($_POST['length'])];
        }

        break;

    case REQUEST_BACK_CALL:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
        $data["attrs"][] = ["VALUE" => "Город", "DESCRIPTION" => htmlspecialchars($_POST['city'])];

        break;

    case REQUEST_KITCHEN:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);

        break;

    case REQUEST_HOUSE_AND_KITCHEN:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
        $data["attrs"][] = ["VALUE" => "Дом", "DESCRIPTION" => htmlspecialchars($_POST['house'])];
        $data["attrs"][] = ["VALUE" => "Сумма сертификата", "DESCRIPTION" => htmlspecialchars($_POST['certificate'])];
        $data["attrs"][] = ["VALUE" => "Код 1С", "DESCRIPTION" => htmlspecialchars($_POST['code'])];
        $data['code_bs'] = htmlspecialchars($_POST['code']);

        break;

    case REQUEST_KITCHEN_CONSULTING:
    case REQUEST_ALARM_HOUSE:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
        $data["attrs"][] = ["VALUE" => "Дом", "DESCRIPTION" => htmlspecialchars($_POST['house'])];
        $data["attrs"][] = ["VALUE" => "Код 1С", "DESCRIPTION" => htmlspecialchars($_POST['code'])];
        $data['code_bs'] = htmlspecialchars($_POST['code']);

        break;

    case REQUEST_PROJECT_PICKING_UP:
    case REQUEST_ALARM_PROJECT_PICKING_UP:

       $data["fio"] = htmlspecialchars($_POST["name"]);
       $data["phone"] = htmlspecialchars($_POST["phone"]);

       if (isset($_POST['type']) && is_array($_POST['type']))
       {
           $data["attrs"][] = ["VALUE" => "Тип дома", "DESCRIPTION" => htmlspecialchars(implode(', ', $_POST['type']))];
       }

       if (isset($_POST['type']) && is_array($_POST['type']))
       {
           $data["attrs"][] = ["VALUE" => "количество этажей", "DESCRIPTION" => htmlspecialchars(implode(', ', $_POST['floors']))];
       }

       if (isset($_POST['square']))
       {
           $data["attrs"][] = ["VALUE" => "Площадь", "DESCRIPTION" => htmlspecialchars($_POST['square'])];
       }

       break;

    case REQUEST_ALARM_QUESTIONS_LEFT:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);

        break;
	case REQUEST_PROJECT:
	
        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
		$data["email"] = htmlspecialchars($_POST["mail"]);		
		$data["attrs"][] = ["VALUE" => "Сообщение", "DESCRIPTION" => htmlspecialchars($_POST['msg'])];
		
		$images = $_FILES["images"];

		$file_ids = array(); // Массив ID сохраняемых файлов

			foreach ($images["name"] as $key => $photo) {
				// Проверяем расширение (допустимы: jpeg, jpg, png, dwg)

				$name_parts = explode('.', $photo);
				$ext = $name_parts[count($name_parts) - 1];
				if (!in_array($ext , array('jpeg', 'jpg', 'png', 'dwg')))
				{
					echo 'Файл '.$photo.' имеет недопустимое расширение и не может быть загружен<br>';
					die();
				}

				// Проверяем реальный тип файла (допустимы: картинки, чертежи из autocad)

				if (!in_array($images["type"][$key], array('application/octet-stream', 'image/jpeg', 'image/png')))
				{
					echo 'Файл '.$photo.' имеет недопустимый формат и не может быть загружен<br>';
					die();
				}

				$tmpFile = Array(
					"name" => $photo,
					"size" => $images["size"][$key],
					"tmp_name" => $images["tmp_name"][$key],
					"type" => $images["type"][$key],
					"old_file" => "",
					"del" => "Y",
					"MODULE_ID" => "iblock"
				);

				$file_ids[] = CFile::SaveFile($tmpFile, "/upload/iblock/");
				
				$data["attrs"]["DESCRIPTION"] .= "<br>Изображения: <br>";
				$data["attrs"]["DESCRIPTION"] .= " ".CFile::GetPath(array_pop($file_ids))."<br>";

				if(count($file_ids) == 3) break; // Три картинки и хватит
			 }		

	
	break;	
	case REQUEST_PROJECT_CALL:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
        $data["attrs"][] = ["VALUE" => "Город", "DESCRIPTION" => htmlspecialchars($_POST['city'])];
	
	break;
	case REQUEST_ADDITIONAL_CONS:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["telephone"]);
        $data["attrs"][] = ["VALUE" => "Дом", "DESCRIPTION" => htmlspecialchars($_POST['house'])];
        $data["attrs"][] = ["VALUE" => "Код 1С", "DESCRIPTION" => htmlspecialchars($_POST['code'])];
        $data['code_bs'] = htmlspecialchars($_POST['code']);		
	
	break;
	case REQUEST_REBUILDING:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);		
	
	break;
	case REQUEST_ELECTRO:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
        $data["attrs"][] = ["VALUE" => "Время звонка", "DESCRIPTION" => htmlspecialchars($_POST['time'])];		
	
	break;	
	case REQUEST_INNER:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
        $data["attrs"][] = ["VALUE" => "Время звонка", "DESCRIPTION" => htmlspecialchars($_POST['time'])];		
	
	break;
	case REQUEST_ADDITION2:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
	
	break;
	case REQUEST_CREDIT:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
	
	break;
    case REQUEST_VISIT:

        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);
	
	break;
	case REQUEST_ANSWERS:

		$data["fio"] = htmlspecialchars($_POST["user"]);
        // $data["phone"] = htmlspecialchars($_POST["phone"]);
		$data["email"] = htmlspecialchars($_POST["email"]);		
	
	break;
	case REQUEST_CONTACTS1:
		/*
        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);	
        $data["attrs"][] = ["VALUE" => "Время звонка", "DESCRIPTION" => htmlspecialchars($_POST['time'])];		
        $data["email"] = htmlspecialchars($_POST["mail"]);		
		*/
		$data["fio"] = htmlspecialchars($_POST["name"]);
		$data["phone"] = htmlspecialchars($_POST["phone"]);
		$data["email"] = htmlspecialchars($_POST["mail"]);
		// $data["time"] = ;		
		// $data["city"] = ;
	break;
	case REQUEST_CONTACTS2:
        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);	
        $data["attrs"][] = ["VALUE" => "Сообщение", "DESCRIPTION" => htmlspecialchars($_POST['msg'])];		
	break;		
	case REQUEST_CONTACTS3:
        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["phone"] = htmlspecialchars($_POST["phone"]);		
        // $data["email"] = htmlspecialchars($_POST["mail"]);		
        // $data["attrs"][] = ["VALUE" => "Сообщение", "DESCRIPTION" => htmlspecialchars($_POST['msg'])];			
	break;
	case REQUEST_CONTACTS4:
        $data["fio"] = htmlspecialchars($_POST["name"]);
        $data["email"] = htmlspecialchars($_POST["mail"]);				
        // $data["attrs"][] = ["VALUE" => "Сообщение", "DESCRIPTION" => htmlspecialchars($_POST['msg'])];		
	break;	
	case REQUEST_CONTACTS5:
        $data["fio"] = htmlspecialchars($_POST["name5"]);	
        $data["phone"] = htmlspecialchars($_POST["phone5"]);				
        $data["email"] = htmlspecialchars($_POST["mail5"]);				
        $data["attrs"][] = ["VALUE" => "Сообщение", "DESCRIPTION" => htmlspecialchars($_POST['msg5'])];	
	break;
	case REQUEST_CONTACTS6:
        $data["fio"] = htmlspecialchars($_POST["name6"]);
        $data["phone"] = htmlspecialchars($_POST["phone6"]);						
        $data["email"] = htmlspecialchars($_POST["mail6"]);				
        $data["attrs"][] = ["VALUE" => "Сообщение", "DESCRIPTION" => htmlspecialchars($_POST['msg6'])];	
	break;	
}

if ((isset($data["attrs"]["VALUE"])) && ($data["attrs"]["VALUE"] == "Сообщение")) {
	$msg = $data["attrs"]["DESCRIPTION"];
}

if ($request_type >= 9 && $request_type <= 12)
{
    $what = '';
    if ($request_type == 9)
    {
        $what = 'Заявка на кухню';
    }
    elseif ($request_type == 10 || $request_type == 11)
    {
        $what = '[Кухни] '.htmlspecialchars($_POST['house']);
    }
    elseif ($request_type == 12)
    {
        $what = 'Заявка на подбор проекта с кухней';
    }

    $fields = 'name='.$data['fio'].
              '&telephone='.$data['phone'].
              '&house='.$what.
              '&zayavka=catalog_item'.
              '&url=https://terem-pro.ru/kitchen/'.
              '&id_request=kitchen'.
              '&ipadress='.$_SERVER["REMOTE_ADDR"];
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://crm.terem-pro.ru/index.php/welcome/postGiveHouse/');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
    curl_exec($curl);
    curl_close($curl);

}

$section_id = CIBlockSection::GetList(
        [],
        ["IBLOCK_ID" => REQUESTS_IBLOCK, "CODE" => $data['request_type']],
        0,
        ["ID"]
    )->GetNext()["ID"];

// Modifying & Validating data

// Do not accept empty data

// if (!$data["fio"] || !$data["phone"]) die;
if (!$data["fio"]) die;

// Verifying & modifying fio

if (!preg_match('/[а-яА-ЯёЁ]{3,}/i', $data['fio'])) die;

$data['fio'] = trim($data['fio']);

// Verifying & Modifying phone

if ($data['phone']) {

$data['phone'] = preg_replace('/\D/', '', $data['phone']);

if (strlen($data['phone']) == 7)
{
    $data['phone'] = '495'.$data['phone'];
}
else
{
    $data['phone'] = preg_replace('/^(7|8)/', '', $data['phone']);
}
// if (strlen($data['phone']) != 10 || !preg_match('/^(4|9)(\d{9})/', $data['phone'])) die;
$data['phone'] = '8 ('.
                 substr($data['phone'], 0, 3).') '.
                 substr($data['phone'], 3, 3).'-'.
                 substr($data['phone'], 6, 2).'-'.
                 substr($data['phone'], 8, 2);

// Check for same requests with same phone for today
}


$same_request = CIBlockElement::GetList(
            ["ID" => "DESC"],
            [
                "IBLOCK_ID" => REQUESTS_IBLOCK,
                "IBLOCK_SECTION_ID"=>$section_id,
                "PROPERTY_PHONE" => $data['phone']
            ],
            false,
            ['nTopCount'=>1],
            ["TIMESTAMP_X"]
        )->GetNext();

if (substr($same_request["TIMESTAMP_X"], 0, 10) == date('d.m.Y')) die;

// Verifying email if it is

if (isset($data['email']))
{
    $data['email'] = trim($data['email']);
    // if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) die;
}
else
{
    $data['email'] = '';
}

// Saving data to Iblock

$e = new CIBlockElement;

$props = array();

$props["FIO"] = $data['fio'];
$props["PHONE"] = $data['phone'];
$props["EMAIL"] = $data['email'];
$props["COMMENT"] = $data['comment'];
$props["ATTRS"] = $data['attrs'];
$props["UTM_CAMPAIGN"] = $data['utm_campaign'];
$props["UTM_TERM"] = $data['utm_term'];
$props["UTM_CONTENT"] = $data['utm_content'];
$props["ENTRANCE_SOURCE"] = $data['entrance_source'];
$props["IP"] = $data['ip'];
$props["UA"] = $data['ua'];

$fields = array(

    "IBLOCK_ID" => REQUESTS_IBLOCK,
    "IBLOCK_SECTION_ID" => $section_id,
    "NAME" => $data['fio'].', '.$data['phone'].' ('.$data['date'].')',
    "ACTIVE" => "Y",
    "PROPERTY_VALUES" => $props

);

// Creating XML

if ($id = $e->Add($fields))
{

/*
    $xmlstr =   '<?xml version="1.0" encoding="UTF-8"?>'.
                '<Заявка '.
                'ФИО="'.$data['fio'].'" '.
                'Телефон="'.$data['phone'].'" '.
                'ВидЗаявки="'.($data['request_type'] < 9 ? $data['request_type'] : REQUEST_HOUSE).'" '.
                'ДатаЗаявки="'.$data['date'].'" '.
                'Реклама="'.$data['entrance_source'].'" '.
                'РекламаКампания="'.$data['utm_campaign'].'" '.
                'РекламаОбъявление="'.$data['utm_content'].'" '.
                'РекламаКлючевоеСлово="'.$data['utm_term'].'" '.
                'КодБУ="'.$data['code_bs'].'" '.
                'Кредит="'.$data['credit_type'].'" '.
                'СписокДУ="" '.
                'ID="'.$id.'">'.
                '</Заявка>';

*/
	$xmlstr = '<?xml version="1.0" encoding="UTF-8"?>'.'<Заявка ID="'.$id.'" СписокДУ="" Кредит="'.$data['credit_type'].'" КодБУ="'.$data['code_bs'].'" РекламаКлючевоеСлово="'.$siteform[$request_type].'" РекламаОбъявление="'.$data['yaid'].'" РекламаКампания="Сайт" Реклама="'.$data['entrance_source'].'" ДатаЗаявки="'.$data['date'].'" ВидЗаявки="'.($data['request_type'] < 9 ? $data['request_type'] : REQUEST_HOUSE).'" Телефон="'.$data['phone'].'" ФИО="'.$data['fio'].'" email="'.$data['email'].'" utm_source="'.$data['utm_source'].'" utm_medium="'.$data['utm_medium'].'" utm_campaign="'.$data['utm_campaign'].'" utm_content="'.$data['utm_content'].'" utm_term="'.$data['utm_term'].'"  КачествоЗаявки="1" ClientID="'.$data['clientid'].'" КомментарийКлиента="'.$msg.'">'.'</Заявка>';				

    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/xml_requests/'.$id.'.xml', $xmlstr);
    $handle = ftp_connect('u208395.ftp.masterhost.ru');
    ftp_login($handle, 'u208395_ftp', 'n6ssella4lingr');
    ftp_put($handle, 'requests/'.$id.'.xml', $_SERVER["DOCUMENT_ROOT"].'/xml_requests/'.$id.'.xml', FTP_ASCII);
    ftp_close($handle);
	
    // unlink($_SERVER["DOCUMENT_ROOT"].'/xml_requests/'.$id.'.xml');
	
	// отправка email
	//  1 => "client@terem-pro.ru", отключил
	$mailAddress = array( 0 => "evtyushkin@terem-pro.ru",);
	$mailSubject = 'Заявка с сайта: '.$siteform[$request_type];
	$mailText = 'ЗАЯВКА С САЙТА<br>Тип заявки: '.$siteform[$request_type].'<br>Отправитель: '.$data["fio"].'<br>Телефон: '.$data["phone"].'<br> email(если есть): '.$data["email"];
	
	
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

    foreach ($mailAddress as $ml) {
        $m = new Mail;
        $m->From("crm@terem-pro.ru");
        $m->To($ml);
        $m->Subject($mailSubject);
        $m->Body($mailText, "html");
        $m->smtp_on("ssl://smtp.mastermail.ru", "crm@terem-pro.ru", "crmpassword", 465);
        $m->Send();			
    }				

	

}



