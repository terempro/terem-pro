<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

const ATTEMPTS_EXHAUSTED = 1;
const CONFIRMATION_CODE_GIVEN = 2;
const CONFIRMATION_CODE_WAITED = 8;
const CONTEST_SUCCESS = 16;

if (!CModule::IncludeModule("iblock")) die();

// Обработка отправки данных

if (isset($_GET['data']))
{
    $error = '';
    
    $contract = resetString(trim($_POST["contract"]));
    $fio = resetString(trim($_POST["fio"]));

    if (!$fio) $error = 'Пожалуйста, введите свои фамилию и имя!';
    elseif (mb_strlen($fio) < 4) $error = 'Слишком короткие фамилия и имя!';
    elseif (!$contract) $error = 'Пожалуйста, введите номер договора!';
    elseif (!preg_match('/(\d{8,12})/', $contract)) $error = 'Пожалуйста, введите корректный номер договора!';
    else 
    {
        $r = CIBlockElement::GetList([], ["IBLOCK_ID"=>54, "PROPERTY_CONTRACT"=>$contract],0,0,["ID"])->GetNext();
        if ($r["ID"])
        {
            $error = 'Ошибка! Данный договор уже есть в базе заявок на участие в конкурсе!';
        }
    }    
    
    if ($error) echo $error;
    else
    {
        $_SESSION['CONTEST_FIO'] = $fio;
        $_SESSION['CONTEST_CONTRACT'] = $contract;
        
        echo CONFIRMATION_CODE_WAITED;
    }
    	
}

// Обработка заявки на получение кода

elseif (isset($_GET['getcode']))
{
    $phone = preg_replace('/[^\d]/', '', resetString(trim($_POST["phone"])));
    
    if (!$phone || !preg_match('/79(\d{9})/', $phone)) 
    {
        echo 'Введите корректный номенр телефона!';
        die();
    }
    else 
    {
        $r = CIBlockElement::GetList([], ["IBLOCK_ID"=>54, "PROPERTY_PHONE"=>$phone],0,0,["ID"])->GetNext();
        if ($r["ID"])
        {
            echo 'Ошибка! Данный телефон уже есть в базе заявок на участие в конкурсе!';
            die();
        }
    }   
    
    $_SESSION['CONTEST_CODE'] = rand(1000, 9999);
    $_SESSION['CONTEST_PHONE'] = $phone;
    $_SESSION["CONTEST_ATTEMPTS"] = 0;
    
    $code_sms = "Терем. Пароль для подтверждения телефона ".
                $_SESSION["CONTEST_CODE"].
                ". Никому не сообщайте свой пароль и не подтверждайте операции в системе, которые Вы не совершали.";
    
    $sms = file_get_contents("http://gateway.api.sc/get/?user=terempro8&pwd=QbcEawZw&sadr=TEREM&dadr={$phone}&text=". urlencode($code_sms));
    if ($sms) echo CONFIRMATION_CODE_GIVEN;
    else echo 'Неизвестная ошибка отправки СМС. Обратитесь в службу поддержки сайта';
}

// Обработка стадии подтверждения кода

elseif (isset($_GET['acceptcode']))
{ 
    $code  = (int)$_POST['code'];
    
    if (++$_SESSION["CONTEST_ATTEMPTS"] == 3) 
    {
        $_SESSION["CONTEST_FAILED"] = true;
        echo ATTEMPTS_EXHAUSTED; // Сигнал для ajax, что надо вывести сообщение об ошибке
        die();
    }
    
    // Если код введён некорректно, выводим сообщение на экран

    if ($_SESSION["CONTEST_CODE"] != $code)
    {
        echo 'Введён некорректный код подтверждения!';
    }
    
    // Иначе, завершаем регистрацию заявки и отправляем уведомления клиенту и администрации
    // При успешном завершении в ajax-запрос возвращается id заявки и будет выведено сообщение
    // об успехе
    
    else
    {
        
        // Добавляем заявку

        $props = array();

        $props["FIO"] = $_SESSION['CONTEST_FIO'];
        $props["PHONE"] = $_SESSION['CONTEST_PHONE'];
        $props["CONTRACT"] = $_SESSION['CONTEST_CONTRACT'];

        $el = new CIBlockElement;

        $fields = array(

            "NAME" => "Заявка от ".$props["FIO"].", ".date('d.m H:i'),
            "CODE" => "CONTEST_REQUEST_".time(),
            "IBLOCK_ID" => 54,
            "ACTIVE_FROM" => date('d.m.Y H:i:s'),
            "ACTIVE" => "Y",
            "PROPERTY_VALUES" => $props

        );

        if (!($id = $el->Add($fields))) echo 'Возникла непредвиденная ошибка!';
        else 
        {
            // Сообщение администрации
            
            $message =  "<h3>Зарегистрирована новая онлайн-заявка на участие в розыгрыше</h3>".
                        "Клиент: <b>".$_SESSION['CONTEST_FIO'].'</b><br>'.
                        "Номер договора: <b>".$_SESSION['CONTEST_CONTRACT'].'</b><br>'.
                        "Телефон: <b>".$_SESSION['CONTEST_PHONE'].'</b><br>'.
                        '<br>-----------------------------------------------<br>'.
                        '<sub>Заявка №'.$id.' от '.date('d.m H:i').'</sub><br>';
            
            $m = new Mail();
            $m->From("info@terem-pro.ru");
            $m->To('zolotarev@terem-pro.ru');
            $m->Subject('Новая заявка на участие в розыгрыше');
            $m->Body($message,  'html');    
            $m->Priority(3);    // приоритет письма
            $m->smtp_on("ssl://SMTP.mastermail.ru", "terem@terem-pro.ru", "postterempass", 465); 
            $m->Send();
            
            $message = 'Договор зарегистрирован. Сохраняйте код до 05.08.2017. Все подробности по телефону.';
            
            $sms = file_get_contents("http://gateway.api.sc/get/?user=terempro8&pwd=QbcEawZw&sadr=TEREM&dadr=".$_SESSION['CONTEST_PHONE']."&text=".urlencode($message));
            
            // Удаляем данные сессии

            unset($_SESSION["CONTEST_CODE"]);
            unset($_SESSION["CONTEST_FIO"]);
            unset($_SESSION["CONTEST_PHONE"]);
            unset($_SESSION["CONTEST_ATTEMPTS"]);
            unset($_SESSION["CONTEST_FAILED"]);
            
            // Устанавливаем в сессию статус завершённой заявки для предотвращения 
            // единовременной отправки многократных заявок

            $_SESSION["CONTEST_SENT"] = true;
            
            echo CONTEST_SUCCESS;
           
        }
              
    }
}

?>

