<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if ($_POST) {
    if (CModule::IncludeModule("iblock")) {
        global $USER;
        global $DB;

        if ($_POST) {
            if ($_POST['id_request'] == 'dostroyka') {

                $error_dostroyka = false;
                $error_dostroyka2 = false;
                $error_dostroyka3 = false;



                if (strlen($_POST['order']) > 10) {

                    $name = $_POST['NameClient'];
                    $telephone = $_POST['TelephoneClient'];
                    $time = 1;
                    $text = $_POST['selected-items'];

                    if (is_string($name)) {
                        $name = trim($name);
                        if (preg_match("/[а-я]/i", $name)) {
                            $name = strip_tags($name);
                            $error_dostroyka = true;
                        }
                    }

                    if (strlen($telephone) == 18) {
                        $telephone = strip_tags($telephone);
                        $error_dostroyka2 = true;
                    }

                    if (strlen($text) > 0) {
                        $text = strip_tags($text);
                        $error_dostroyka3 = true;
                    }
                }





                if ($error_dostroyka && $error_dostroyka2 && $error_dostroyka3) {


                    $arFields = array(
                        "ACTIVE" => "Y",
                        "IBLOCK_ID" => 33,
                        "NAME" => $name,
                        "PREVIEW_TEXT" => "test",
                        "PROPERTY_VALUES" => array(
                            "TELEPHONE" => $telephone,
                            "TIME" => $time,
                            "USLUGI" => $text,
                            "OBRABOTKA" => "Нет"
                        )
                    );

                    $el = new CIBlockElement;
                    $ID = $el->Add($arFields, false, false, true);
                    //mail
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                    $mails = array(
                        "1" => "zolotarev@terem-pro.ru",
                        "2" => "lfo@terem-pro.ru"
                    );
                    $message = "Имя: " . $name . " Телефон: " . $telephone . " Текст: " . $text;
                    foreach ($mails as $m) {
                        mail("$m", "Заявка на достройку с сайта", $message, $headers);
                    }
                }
            } else {
                if ($_POST['id_request'] == 'pristroiki') {

                    $error_pristroiki = false;
                    $error_pristroiki2 = false;
                    $error_pristroiki3 = false;



                    $name = $_POST['Name'] . " " . $_POST['LastName'] . " " . $_POST["Last2Name"];
                    $telephone = $_POST["TelephoneClient"];
                    $email = $_POST["MailClient"];
                    $text = "";
                    foreach ($_POST["cheks"] as $chek) {
                        $text = $text . ' ,' . $chek;
                    }
                    $text = $text . " Комментарий: " . $_POST["CommentsClient"];


                    if (is_string($name)) {
                        $name = trim($name);
                        if (preg_match("/[а-я]/i", $name)) {
                            $name = strip_tags($name);
                            $error_dostroyka = true;
                        }
                    }



                    if (strlen($telephone) == 18) {
                        $telephone = strip_tags($telephone);
                        $error_dostroyka2 = true;
                    }


                    if (strlen($text) > 0) {
                        $text = strip_tags($text);
                        $error_pristroiki3 = true;
                    }



                    if ($error_pristroiki && $error_pristroiki2 && $error_pristroiki3) {
                        $arFields = array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => 33,
                            "NAME" => $name,
                            "PREVIEW_TEXT" => "test",
                            "PROPERTY_VALUES" => array(
                                "TELEPHONE" => $telephone,
                                "TIME" => '',
                                "USLUGI" => $text,
                                "OBRABOTKA" => "Нет"
                            )
                        );

                        $el = new CIBlockElement;
                        $ID = $el->Add($arFields, false, false, true);
                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                        $mails = array(
                            "1" => "zolotarev@terem-pro.ru",
                            "2" => "lfo@terem-pro.ru"
                        );
                        $message = "Имя: " . $name . " Телефон: " . $telephone . " Текст: " . $text;
                        foreach ($mails as $m) {
                            mail("$m", "Заявка на достройку с сайта", $message, $headers);
                        }
                    }
                }
            }
        }
    }
}
?>

