<?php 
	require 'libmail.php';

	// print_r($_POST);
	// обработка информации из формы обратной связи
	
	class Callback {
	
		// свойства
		
		var $name;
		var $phone;
		var $town;
		var $house;
		
		// методы
		
		// верификация поста		
		public function verifyPost() {			
			// забираем пост. проверяем заполнение имени, города, телефона
						
			$postinfo = strip_tags($_POST);			
			if ((in_array("name", $postinfo) == TRUE) && (in_array("phone", $postinfo) == TRUE) && (in_array("city", $postinfo) == TRUE)) {
				$result = TRUE;
			} else {
				$result = FALSE;
			}
			return $result;		
		}
		
		// заполнение свойств из поста
		public function getPostInfo() {		
		
				$postinfo = $_POST;	
				$this->name = $postinfo['name'];
				$this->phone = $postinfo['phone'];
				$this->town = $postinfo['city'];	
				if (array_key_exists('house', $postinfo)) {
					$this->house = $postinfo['house']; 	
				} else {
					$this->house = ''; 					
				}
				

/*				
			if ($this->verifyPost() == TRUE) {
			
				$postinfo = strip_tags($_POST);	
				$this->name = $postinfo['name'];
				$this->phone = $postinfo['phone'];
				$this->town = $postinfo['city'];
				
				return TRUE;
			
			} else {
			
				return FALSE;
			
			}
*/			
		
		}		
		
		// отправка email функцией mail()		
		public function sendEmail() {

		switch ($this->town) {

//			case 'Брянск':
//				$mails = array(
//					"1" => "info.bryansk@terem-pro.ru",
//					// "2" => "svetlitskaya@terem-pro.ru",
//					"2" => "evtyushkin@terem-pro.ru",
//				);			
//				$message = "Заявка с формы обратной связи сайта Терем: Брянск. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;
//			break;		
			case 'Вологда':
				$mails = array(
					"1" => "info.vologda@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Вологда. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;
			break;		
			case 'Ижевск':
				$mails = array(
					"1" => "info.izh@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Ижевск. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;
			break;			
			/*case 'Краснодар':
				$mails = array(
					"1" => "info.krasnodar@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Краснодар. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;
break;*/
			case 'Владимир':
				$mails = array(
					"1" => "info.vladimir@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Владимир. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;	
			break;
			case 'Ярославль':
				$mails = array(
					"1" => "info.yar@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Ярославль. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;	
			break;		
			case 'Новосибирск':
				$mails = array(
					"1" => "info.nsk@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Новосибирск. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;
			break;
			case 'Саратов':
				$mails = array(
					"1" => "info.saratov@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
					"3" => "teremsar.139522@parser.amocrm.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Саратов. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;			
			break;
			case 'Тула':
				$mails = array(
					"1" => "info.tula@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Тула. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;
			break;	
            case 'Рязань':
				$mails = array(
					"1" => "info.ryazan@terem-pro.ru",
					// "2" => "svetlitskaya@terem-pro.ru",
					"2" => "evtyushkin@terem-pro.ru",
				);			
				$message = "Заявка с формы обратной связи сайта Терем: Рязань. <br>ФИО: " . $this->name . " <br/>Телефон:" . $this->phone . "<br>" . $this->house;
			break;			
			default:
				$mails = array(
					"1" => "evtyushkin@terem-pro.ru"
				);
				$message = "отправка с формы обратной связи.<br>Город: ". $this->town. ". <br>ФИО: " . $this->name . " <br>Телефон:" . $this->phone . "<br>" . $this->house;
			break;
		}

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        // $message = "ФИО " . $this->name . " <br/>Телефон:" . $this->phone ;
        foreach ($mails as $ml) {
            $m = new Mail;
            $m->From("crm@terem-pro.ru");
            $m->To($ml);
            $m->Subject("Заказ обратного звонка терем");
            $m->Body($message, "html");
            $m->smtp_on("ssl://smtp.mastermail.ru", "crm@terem-pro.ru", "crmpassword", 465);
            $m->Send();	
		
        }				
		
		return $message;

		
		}
		
		// создание xml-файла с данными		
		function createXMLdata() {
		
		
		}
		
		
 	
	}
	
	$data = new Callback;
	$meh = $data->getPostInfo();
	$zug = $data->sendEmail();
	
	// echo $data->name.''.$data->phone.' '.$data->town.'<br>';
	print_r($_POST);
	var_dump($meh);
	var_dump($zug);

	

?>