<?php

/*
 * =================================================
 * 		IBlockChangeHandler:
 * 	Данный класс содержит статические методы для
 * 	отслеживания изменений в инфоблоках сайта
 * =================================================
 */

 class IBlockChangeHandler
 {
    const SMALL_BUILDINGS_SECTION_ID = 28731; 
     
    // Список ID инфоблоков, в которых запрещено добавление элементов
    protected static $AdditionForbidden = array();
    // Список ID инфоблоков, в которых запрещено изменение элементов
    protected static $UpdateForbidden = array();	
    // Список ID инфоблоков, в которых запрещено удаление элементов
    protected static $DeletionForbidden = array(36); 
    // Список ID инфоблоков, для которых запускаются ежедневные функции       
    protected static $DailyOperations = array(13,15,21,22,25,51,52,60,61,62,63);

    // Список ID инфоблоков и обработчиков, которые вызываются после добавления элемента в виде ID => "имя обработчика в данном классе"		   
    protected static $AfterAdditionOccured = array(
        //13 => array("UpdateFilter"),
        21 => array("UpdateYandexFeed"),
        22 => array("UpdateYandexFeed"),
    );		
    // Список ID инфоблоков и обработчиков, которые вызываются после изменения элемента в виде ID => "имя обработчика в данном классе"	
    protected static $AfterUpdateOccured = array(
        //13 => array("UpdateFilter"),
        21 => array("UpdateParser", "UpdateYandexFeed"),
        22 => array("UpdateYandexFeed"),
        35 => array("SendContestNotification"),
    );	
    // Список ID инфоблоков и обработчиков, которые вызываются после удаления элемента в виде ID => "имя обработчика в данном классе"
    protected static $AfterDeletionOccured = array(
        13 => array("UpdateFilter"),
        21 => array("UpdateFilter", "UpdateYandexFeed"),
        22 => array("UpdateYandexFeed"),
    );

    protected static function SendContestNotification($arFields)
    {
        if ($arFields['ACTIVE'] == 'Y')
        {
            $res = CIBlockElement::GetProperty(35, $arFields['ID'], [], ['CODE' => 'EMAIL']);
            $email = $res->Fetch()["VALUE"];
            $message =  'Добрый день!<br>'.
                        'Спасибо, что приняли участие в конкурсе детского рисунка  «Дети рисуют дом мечты»<br><br>'.
                        'Информируем Вас о том, что Ваш рисунок прошел модерацию и размещен на <a href="http://terem-pro.ru/loadphoto/">странице конкурса</a>.<br><br>'.
                        'Подробнее о правилах, условиях и сроках конкурса Вы можете узнать по ссылке: <a href="http://terem-pro.ru/pravila/">http://terem-pro.ru/pravila/</a><br><br>'.
                        'С уважением,<br><br>'.
                        'Администрация портала terem-pro.ru'; 

            Mail::S($email, "Ваш рисунок прошёл модерацию", $message);
        }
    }

    protected static function UpdateYandexFeed()
    {
        // Формируем категории домов

        $catsSelect = array("ID", "CODE", "NAME");
        $catsFilter = array('IBLOCK_ID' => 13, 'ACTIVE' => 'Y');

        $cats = CIBlockSection::GetList(Array("SORT" => "ASC"), $catsFilter, false, $catsSelect);

        $cats_array = array();

        // Формируем массив домов

        $houses = array();
        $i = 0;

        while ($c = $cats->Fetch()) 
        {
            $cats_array[$c["ID"]] = $c["NAME"];

            $housesSelect = Array("ID", "CODE", "NAME", "DETAIL_PICTURE", "PROPERTY_KARKAS_VARIANTS_HOUSE"); 
            $housesFilter = Array("IBLOCK_ID" => 13, "SECTION_ID" => $c["ID"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
            $houseList = CIBlockElement::GetList(Array("SORT" => "ASC"), $housesFilter, false, false, $housesSelect);

            while ($h = $houseList->Fetch()) 
            {
                if(count($h["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"]))
                {
                    // Получаем каркасный вариант дома

                    $current_board_width = 200; // значально полагаем ширину доски максимальной

                    foreach($h["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"] as $variant_id)
                    {
                        // Получаем категорию дома

                        switch($c["CODE"])
                        {
                                case 'dachnye-doma': $houses[$i]["CATEGORY"] = "дача"; break;
                                case 'kottedzhi': $houses[$i]["CATEGORY"] = "коттедж"; break;
                                default: $houses[$i]["CATEGORY"] = "дом";
                        } 
                        if ($h["CODE"] == "garazh")	$houses[$i]["CATEGORY"] = "гараж";

                        $variantSelect = Array("ID", "PREVIEW_TEXT", "PROPERTY_PRICE_HOUSE", "PROPERTY_SQUARE_ALL_HOUSE", "PROPERTY_FLOOR");
                        $variantFilter = Array("IBLOCK_ID" => 21, "ID" => $variant_id, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");

                        $variantSelect = Array("ID", "PREVIEW_TEXT", "PROPERTY_PRICE_HOUSE", "PROPERTY_SQUARE_ALL_HOUSE", "PROPERTY_FLOOR", "PROPERTY_TEHNOLOGY_HOUSE");
                        $variantFilter = Array("IBLOCK_ID" => 21, "ID" => $variant_id, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");		
                        $variantList = CIBlockElement::GetList(array("SORT" => "ASC"), $variantFilter, false, false, $variantSelect);

                        // Будем выбирать минимальный вариант

                        while ($v = $variantList->Fetch())
                        {

                            // Получаем наименьшую ширину доски и на её основе строим ценник

                            $board_width = intval(preg_replace('/\D/', "", $v["PROPERTY_TEHNOLOGY_HOUSE_VALUE"]));

                            if ($board_width <= $current_board_width)
                            {
                                $current_board_width = $board_width;
                                $houses[$i]["CATEGORY_ID"] = $c["ID"];
                                $houses[$i]["ID"] = $variant_id;
                                $houses[$i]["DESCRIPTION"] = HTMLToTxt($v["PREVIEW_TEXT"]);	
                                $houses[$i]["AREA"] = (float)$v["PROPERTY_SQUARE_ALL_HOUSE_VALUE"];
                                $houses[$i]["FLOORS"] = $v["PROPERTY_FLOOR_VALUE"];

                                // Получаем дату создания и последнего изменения объявления (одинаковые)

                                $houses[$i]["CREATION_DATE"] = $houses[$i]["LAST_UPDATE_DATE"] = date("Y-m-d\TH:i:s+03:00");

                                // Получаем ценники, откуда берём URL и цену дома

                                $priceSelect = Array("PROPERTY_PRICE_1", "PROPERTY_LINK");
                                $priceFilter = Array("IBLOCK_ID" => 22, "ID" => $v['PROPERTY_PRICE_HOUSE_VALUE'], "ACTIVE" => "Y");
                                $priceList = CIBlockElement::GetList(array("SORT" => "ASC"), $priceFilter, false, false, $priceSelect);

                                while ($p = $priceList->Fetch())
                                {
                                    // Берём минимальную цену из всех
                                    $houses[$i]["PRICE"] = min(array($houses[$i]["PRICE"]?:$p['PROPERTY_PRICE_1_VALUE'], $p['PROPERTY_PRICE_1_VALUE']));
                                    $houses[$i]["LINK"] = $p['PROPERTY_LINK_VALUE'];
                                }

                                $houses[$i]["IMAGE"] = CFile::GetPath($h["DETAIL_PICTURE"]);
                                $houses[$i]["NAME"] = $h["NAME"]; 
                            }
                        } 					                           
                    }
                }
                ++$i;
            }
        }

        $xmlstr = 
        '<?xml version="1.0" encoding="UTF-8"?>'.
        '<yml_catalog date="'.date('Y-m-d H:i').'">'.
        '</yml_catalog>';

        $yf = new SimpleXMLElement($xmlstr);

        $shop = $yf->addChild('shop');
        $shop->addChild('name', 'Теремъ');
        $shop->addChild('company', 'ООО Теремъ-про');
        $shop->addChild('url', 'http://terem-pro.ru');

        $currencies = $shop->addChild('currencies');
        $rubles = $currencies->addChild('currency');
        $rubles->addAttribute('id', 'RUR');
        $rubles->addAttribute('rate', '1');

        $categories = $shop->addChild('categories');

        foreach ($cats_array as $cid => $cname)
        {
            $cat = $categories->addChild('category', $cname);
            $cat->addAttribute('id', $cid);
        }

        $offers = $shop->addChild('offers');

        foreach($houses as $h)
        {
            $offer = $offers->addChild('offer');
            $offer->addAttribute('id', $h["ID"]);
            $offer->addAttribute('type', 'vendor.model');
            $offer->addAttribute('available', 'true');

            $offer->addChild('model', $h["NAME"]);
            $offer->addChild('url', 'http://terem-pro.ru'.$h["LINK"]);
            $offer->addChild('price', $h["PRICE"].'000');
            $offer->addChild('categoryId', $h["CATEGORY_ID"]);
            $offer->addChild('currencyId', 'RUR');
            $offer->addChild('picture', 'http://terem-pro.ru'.$h["IMAGE"]);
            $offer->addChild('description', $h["DESCRIPTION"]);
            $offer->addChild('country_of_origin', 'Россия');
            $offer->addChild('manufacturer_warranty', 'true');
            $offer->addChild('vendor', 'ООО Теремъ-Про');

        }

        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yf.xml", $yf->asXML());
    } 	

    protected static function UpdateParser($arFields)
    {
        CAddServicesParcer::ParceFileData($arFields);
        return $arFields;
    }

    // Функция обнровления основного фильтра на сайте
    // Вызывается методаим ElementUpdateHandler, ElementAdditionHandler, ElementDeletionHandler	

    public static function UpdateFilter($iblock_id = 0)
    {
        global $CITIES_ARRAY, $CITY_CODES_ARRAY;

        $houses = array();
        
        // Получаем все серии

        $arSort = ['NAME' => 'asc'];
        $arFilter = ['IBLOCK_ID' => SERIES_IBLOCK];
        $arSelect = ['ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_SERIES', 'CODE'];
        $series = CIBlockElement::GetList($arSort, $arFilter, 0, 0, $arSelect);

        while ($s = $series->GetNext())
        {
            $categories = [];
            $category_query = CIBlockElement::GetById($s['ID'])->GetNextElement()->GetGroups();
            // Имена категорий
            while ($cat = $category_query->GetNext())
            {
                $categories[] = trim($cat['NAME']);
            }

            // Получаем каркасные варианты
            $karkas_variants = CIBlockElement::GetProperty(SERIES_IBLOCK, $s['ID'], [], ['CODE' => 'KARKAS_VARIANTS_HOUSE']);
            // Получаем брусовые варианты
            $brus_variants = CIBlockElement::GetProperty(SERIES_IBLOCK, $s['ID'], [], ['CODE' => 'BRUS_VARIANTS_HOUSE']);
            // Получаем кирпичные варианты
            $kirpich_variants = CIBlockElement::GetProperty(SERIES_IBLOCK, $s['ID'], [], ['CODE' => 'KIRPICH_VARIANTS_HOUSE']);

            // ID вариантов сохраняем в массив

            $variants_array = [];

            while ($v = $karkas_variants->GetNext()) 
            {
                $variants_array[] = ['ID' => (int)$v['VALUE'], 'TECH' => 'Каркас'];
            }
            while ($v = $brus_variants->GetNext()) 
            {
                $variants_array[] = ['ID' => (int)$v['VALUE'], 'TECH' => 'Брус'];
            }
            while ($v = $kirpich_variants->GetNext()) 
            {
                $variants_array[] = ['ID' => (int)$v['VALUE'], 'TECH' => 'Кирпич'];
            }

            foreach ($variants_array as $v)
            {
                // получаем конкретный вариант
                $arOrder = ['NAME' => 'asc'];
                $arFilter = ['IBLOCK_ID' => VARIANTS_IBLOCK, 'ID' => $v["ID"]];
                $arSelect = [
                    'IBLOCK_ID',
                    'ID',
                    'NAME', 
                    'PREVIEW_PICTURE',
                    'IBLOCK_SECTION_ID',
                    'PROPERTY_SIZE1',
                    'PROPERTY_SIZE2',
                    'PROPERTY_SQUARE_HOUSE',
                    'PROPERTY_SQUARE_ALL_HOUSE',
                    'PROPERTY_SQUARE_LIFE_HOUSE',
                    'PROPERTY_FLOOR',
                    'PROPERTY_TEHNOLOGY_DESCRIPTION',
                    'PROPERTY_ID_CALCULATOR',
                    'PROPERTY_PRICE_HOUSE',
                    'PROPERTY_PRICE_HOUSE_KAZAN',
                    'PROPERTY_PRICE_HOUSE_VOLOGDA',
                    'PROPERTY_PRICE_HOUSE_RYAZAN',
                    'PROPERTY_PRICE_HOUSE_TULA',
                    'PROPERTY_PRICE_HOUSE_BRYANSK',
                    'PROPERTY_PRICE_HOUSE_SARATOV',
                    'PROPERTY_PRICE_HOUSE_NOVOSIBIRSK',
                    'PROPERTY_PRICE_HOUSE_IZHEVSK',
                    'PROPERTY_PRICE_HOUSE_KRASNODAR',
                    'PROPERTY_PRICE_HOUSE_VLADIMIR',
                    'PROPERTY_CODE_1C'
                ];
                   
                $house_variant = CIBlockElement::GetList($arOrder, $arFilter, 0, 0, $arSelect)->GetNext();
          
                foreach (array_keys($CITIES_ARRAY) as $city_code)
                {
                    // Если UpdateFilter запущен на конкретном прайслисте, то 
                    // работаем только с ним
                    if ($iblock_id and $iblock_id != $CITY_CODES_ARRAY[$city_code])
                        continue;
                    
                    if ($city_code == 'MOSCOW')
                    {
                        $url = '';
                        $suffix = '';
                    }
                    else
                    {
                        $url = '/'.strtolower($city_code);
                        $suffix = '_'.$city_code;
                    }
                    
                    $price_real = (int)CIBlockElement::GetProperty($CITY_CODES_ARRAY[$city_code], $house_variant['PROPERTY_PRICE_HOUSE'.$suffix.'_VALUE'], [], ['CODE' => 'PRICE_1'])->GetNext()["VALUE"];
                    $price_big = (int)CIBlockElement::GetProperty($CITY_CODES_ARRAY[$city_code], $house_variant['PROPERTY_PRICE_HOUSE'.$suffix.'_VALUE'], [], ['CODE' => 'PRICE_2'])->GetNext()["VALUE"];
                    $discont1 = CIBlockElement::GetProperty($CITY_CODES_ARRAY[$city_code], $house_variant['PROPERTY_PRICE_HOUSE'.$suffix.'_VALUE'], [], ['CODE' => 'DISCONT1'])->GetNext()["VALUE"] ?: 30;
                    $link = '/catalog/'.CIBlockSection::GetById($s['IBLOCK_SECTION_ID'])->GetNext()['CODE'].'/'.$s['CODE'].'/';
                    
                    if ($price_real)
                    {
                        // Определяем псевдо-варианты
                        $variant = 'Лето';

                        if ($v["TECH"] == 'Кирпич' 
                            || ($v['TECH'] == 'Каркас' && (strpos($house_variant['PROPERTY_TEHNOLOGY_DESCRIPTION_VALUE'], '200')
                            || strpos($house_variant['NAME'], '200')))
                            || ($v['TECH'] == 'Брус' && (strpos($house_variant['PROPERTY_TEHNOLOGY_DESCRIPTION_VALUE'], '180')
                            || strpos($house_variant['NAME'], '180'))))
                            {
                                $variant = 'Для постоянного проживания';
                            }
                        //сортировка в зависимости от частей названия
                        $seriesArr = explode(' ', $s['NAME']);
                        $intSub = 0;
                        //правила сортировки
                        foreach ($seriesArr as $strKey => $subStr)
                        {
                            if($intSub = (int)$subStr)
                            {
                                if($intSub > 9)
                                    $subStr = $intSub + 81;
                                //цифру ставим на вторую позицию
                                if($strKey != 1)
                                    $seriesArr[$strKey] = $seriesArr[1];

                                $seriesArr[1] = $subStr;
                                break(1);
                            }
                        }
                        if($intSub == 0)
                            array_splice($seriesArr, 1, 0, [$intSub]);
                        //поле для сортировки
                        $sortForName = implode(' ', $seriesArr);
                        //дома по городам
                        $houses[strtolower($city_code)][] = [

                            // Основная информация
                            'name' => trim($house_variant['NAME']),
                            'code' => trim($house_variant['PROPERTY_CODE_1C_VALUE']) ?: '',
                            'image' => CFile::GetPath($house_variant['PREVIEW_PICTURE']),
                            'link' => $url.$link,
                            'series' => trim($s['NAME']),
                            'series_id' => (int)$s['ID'],
                            'series_type' => trim($s['PROPERTY_SERIES_VALUE']),
                            'categories' => $categories,

                            // Площадь, этажность, размеры
                            'square' => (int)$house_variant['PROPERTY_SQUARE_HOUSE_VALUE'],
                            'living_square' => (int)$house_variant['PROPERTY_SQUARE_LIFE_HOUSE_VALUE'],
                            'total_square' => (int)$house_variant['PROPERTY_SQUARE_ALL_HOUSE_VALUE'],
                            'floors' => (int)$house_variant['PROPERTY_FLOOR_VALUE'],    
                            'width' => round($house_variant['PROPERTY_SIZE1_VALUE'], 2), 
                            'height' => round($house_variant['PROPERTY_SIZE2_VALUE'], 2), 

                            // Технология и цены
                            'technology_type' => $v["TECH"],
                            'technology_description' => trim($house_variant['PROPERTY_TEHNOLOGY_DESCRIPTION_VALUE']),
                            'variant' => $variant,
                            'real_price' => $price_real,
                            'big_price' => $price_big,
                            'sort_for_name' => $sortForName

                        ];
                        
                       

                        // Если вариант - лето и это не беседки, 
                        // добавляем псевдо-вариант для зимы

                        if ($variant == 'Лето' and $s['IBLOCK_SECTION_ID'] != self::SMALL_BUILDINGS_SECTION_ID)
                        {

                            $file = $_SERVER["DOCUMENT_ROOT"].'/upload/calcs/' . $house_variant["PROPERTY_ID_CALCULATOR_VALUE"] . '/calc_' .$house_variant["PROPERTY_ID_CALCULATOR_VALUE"] . '.data';
                            $f = fopen($file, 'r');

                            if (!$f)
                                continue;

                            $file_data = fread($f, filesize($file));
                            fclose($f);

                            $result = unserialize($file_data);

                            if ($result[0]["NAME"] == 'Пакет Оптимальный' or $result[0]["NAME"] == 'Пакет  Оптимальный')
                            {
                                $price_opt = 0;
                                $test = 0;

                                foreach ($result[0]["ITEMS"] as $i)
                                {
                                    $p = (int)$i['PRICE'];
                                    $test = $test + $p;
                                    $price_opt = $price_opt + ($p - (($p * $discont1) / 100));
                                }
                            } 

                            $houses[strtolower($city_code)][] = [

                                // Основная информация
                                'name' => trim($house_variant['NAME']),
                                'code' => trim($house_variant['PROPERTY_CODE_1C_VALUE']) ? : '',
                                'image' => CFile::GetPath($house_variant['PREVIEW_PICTURE']),
                                'link' => $url . $link,
                                'series' => trim($s['NAME']),
                                'series_id' => (int)$s['ID'],
                                'series_type' => trim($s['PROPERTY_SERIES_VALUE']),
                                'categories' => $categories,

                                // Площадь, этажность, размеры
                                'square' => (int)$house_variant['PROPERTY_SQUARE_HOUSE_VALUE'],
                                'living_square' => (int)$house_variant['PROPERTY_SQUARE_LIFE_HOUSE_VALUE'],
                                'total_square' => (int)$house_variant['PROPERTY_SQUARE_ALL_HOUSE_VALUE'],
                                'floors' => (int)$house_variant['PROPERTY_FLOOR_VALUE'],    
                                'width' => round($house_variant['PROPERTY_SIZE1_VALUE'], 2), 
                                'height' => round($house_variant['PROPERTY_SIZE2_VALUE'], 2), 

                                // Технология и цены
                                'technology_type' => $v["TECH"],
                                'technology_description' => trim($house_variant['PROPERTY_TEHNOLOGY_DESCRIPTION_VALUE']),
                                'variant' => 'Зима',
                                'real_price' => round(((int)($price_real*1000) + $price_opt)/1000, 3),
                                'big_price' => 0,
                                'sort_for_name' => $sortForName

                            ];

                        }
                    }
                }
            }
        }
        
        foreach ($houses as $city => $data)
        {
            $city_files = glob($_SERVER['DOCUMENT_ROOT'].'/include/data/'.$city.'*.json');
            //удаляем старые файлы
            foreach ((array)$city_files as $f)
                unlink($f);
            //сортировка по свойствам
            $sortHouse = [];
            $sortPrice = [];
            //сортировка многомерного массива
            foreach($data as $houseKey => $house)
            {
                $sortHouse[$houseKey] = $house['sort_for_name'];
                $sortPrice[$houseKey] = $house['real_price'];
            }
            array_multisort($sortHouse, $sortPrice, $data);
            //запись в файлы json
            $h = fopen($_SERVER['DOCUMENT_ROOT'].'/include/data/'.$city.'_'.time().'.json', 'w');
            fputs($h, json_encode($data, JSON_UNESCAPED_UNICODE));
            fclose($h);
        }
    }    

    public static function ElementDeletionPreventer($ID)
    {
        global $APPLICATION;
        $res = CIBlockElement::GetById($ID);
        $arRes = $res->GetNext(); 
        if(in_array($arRes["IBLOCK_ID"], self::$DeletionForbidden))
        {
            $APPLICATION->throwException("В этом инфоблоке запрещено удалять элементы!");
            return false;
        }
    }

    public static function ElementAdditionHandler(&$arFields)
    { 
        if (array_key_exists($arFields["IBLOCK_ID"], self::$AfterAdditionOccured)) 
        {
        // Вызываем требуемый обработчик для данного IBlock
        foreach (self::$AfterAdditionOccured[$arFields["IBLOCK_ID"]] as $method) self::$method($arFields); 
        }
    }

    public static function ElementUpdateHandler(&$arFields)
    { 
        if (array_key_exists($arFields["IBLOCK_ID"], self::$AfterUpdateOccured)) 
        {
                // Вызываем требуемый обработчик для данного IBlock
        foreach (self::$AfterUpdateOccured[$arFields["IBLOCK_ID"]] as $method) self::$method($arFields);  
        }
    }

    public static function ElementDeletionHandler($arFields)
    {
        if (array_key_exists($arFields["IBLOCK_ID"], self::$AfterDeletionOccured)) 
        {
                // Вызываем требуемый обработчик для данного IBlock
        foreach (self::$AfterDeletionOccured[$arFields["IBLOCK_ID"]] as $method) self::$method($arFields);  
        }
    }

    public static function LaunchDailyOperations($arFields)
    {
        include_once $_SERVER['DOCUMENT_ROOT'].'/include/tasks/daily.php';
        
        $files = scandir($_SERVER['DOCUMENT_ROOT'].'/include/tasks/daily/');
        unset($files[0], $files[1]);
        
        $h = fopen($_SERVER['DOCUMENT_ROOT'].'/include/tasks/dailylist.txt', 'w');
        foreach ($files as $f) fprintf($h, "%s %s\n", basename($f), date('d.m.Y'));
        fclose($h);
    }
 }
 

