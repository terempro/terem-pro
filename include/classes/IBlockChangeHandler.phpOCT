<?php

/*
 * =================================================
 * 		IBlockChangeHandler:
 * 	Данный класс содержит статические методы для
 * 	отслеживания изменений в инфоблоках сайта
 * =================================================
 */

 class IBlockChangeHandler
 {
    // Список ID инфоблоков, в которых запрещено добавление элементов
    protected static $AdditionForbidden = array();
    // Список ID инфоблоков, в которых запрещено изменение элементов
    protected static $UpdateForbidden = array();	
    // Список ID инфоблоков, в которых запрещено удаление элементов
    protected static $DeletionForbidden = array(36); 
    // Список ID инфоблоков, для которых запускаются ежедневные функции       
    protected static $DailyOperations = array(13,15,21,22,25,51,52,60,61,62,63);

    // Список ID инфоблоков и обработчиков, которые вызываются после добавления элемента в виде ID => "имя обработчика в данном классе"		   
    protected static $AfterAdditionOccured = array(
        13 => array("UpdateFilter"),
        21 => array("UpdateFilter", "UpdateYandexFeed"),
    );		
    // Список ID инфоблоков и обработчиков, которые вызываются после изменения элемента в виде ID => "имя обработчика в данном классе"	
    protected static $AfterUpdateOccured = array(
        13 => array("UpdateFilter"),
        21 => array("UpdateFilter", "UpdateParser", "UpdateYandexFeed"),
        22 => array("UpdateYandexFeed"),
        35 => array("SendContestNotification"),
    );	
    // Список ID инфоблоков и обработчиков, которые вызываются после удаления элемента в виде ID => "имя обработчика в данном классе"
    protected static $AfterDeletionOccured = array(
        13 => array("UpdateFilter"),
        21 => array("UpdateFilter", "UpdateYandexFeed"),	
    );

    protected static function SendContestNotification($arFields)
    {
        if ($arFields['ACTIVE'] == 'Y')
        {
            $res = CIBlockElement::GetProperty(35, $arFields['ID'], [], ['CODE' => 'EMAIL']);
            $email = $res->Fetch()["VALUE"];
            $message =  'Добрый день!<br>'.
                        'Спасибо, что приняли участие в конкурсе детского рисунка  «Дети рисуют дом мечты»<br><br>'.
                        'Информируем Вас о том, что Ваш рисунок прошел модерацию и размещен на <a href="http://terem-pro.ru/loadphoto/">странице конкурса</a>.<br><br>'.
                        'Подробнее о правилах, условиях и сроках конкурса Вы можете узнать по ссылке: <a href="http://terem-pro.ru/pravila/">http://terem-pro.ru/pravila/</a><br><br>'.
                        'С уважением,<br><br>'.
                        'Администрация портала terem-pro.ru'; 

            Mail::S($email, "Ваш рисунок прошёл модерацию", $message);
        }
    }

    protected static function UpdateYandexFeed()
    {
        // Формируем категории домов

        $catsSelect = array("ID", "CODE", "NAME");
        $catsFilter = array('IBLOCK_ID' => 13, 'ACTIVE' => 'Y');

        $cats = CIBlockSection::GetList(Array("SORT" => "ASC"), $catsFilter, false, $catsSelect);

        $cats_array = array();

        // Формируем массив домов

        $houses = array();
        $i = 0;

        while ($c = $cats->Fetch()) 
        {
            $cats_array[$c["ID"]] = $c["NAME"];

            $housesSelect = Array("ID", "CODE", "NAME", "DETAIL_PICTURE", "PROPERTY_KARKAS_VARIANTS_HOUSE"); 
            $housesFilter = Array("IBLOCK_ID" => 13, "SECTION_ID" => $c["ID"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
            $houseList = CIBlockElement::GetList(Array("SORT" => "ASC"), $housesFilter, false, false, $housesSelect);

            while ($h = $houseList->Fetch()) 
            {
                if(count($h["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"]))
                {
                        // Получаем каркасный вариант дома

                        $current_board_width = 200; // значально полагаем ширину доски максимальной

                    foreach($h["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"] as $variant_id)
                    {
                        // Получаем категорию дома

                        switch($c["CODE"])
                        {
                                case 'dachnye-doma': $houses[$i]["CATEGORY"] = "дача"; break;
                                case 'kottedzhi': $houses[$i]["CATEGORY"] = "коттедж"; break;
                                default: $houses[$i]["CATEGORY"] = "дом";
                        } 
                        if ($h["CODE"] == "garazh")	$houses[$i]["CATEGORY"] = "гараж";

                        $variantSelect = Array("ID", "PREVIEW_TEXT", "PROPERTY_PRICE_HOUSE", "PROPERTY_SQUARE_ALL_HOUSE", "PROPERTY_FLOOR");
                        $variantFilter = Array("IBLOCK_ID" => 21, "ID" => $variant_id, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");

                        $variantSelect = Array("ID", "PREVIEW_TEXT", "PROPERTY_PRICE_HOUSE", "PROPERTY_SQUARE_ALL_HOUSE", "PROPERTY_FLOOR", "PROPERTY_TEHNOLOGY_HOUSE");
                        $variantFilter = Array("IBLOCK_ID" => 21, "ID" => $variant_id, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");		
                        $variantList = CIBlockElement::GetList(array("SORT" => "ASC"), $variantFilter, false, false, $variantSelect);

                        // Будем выбирать минимальный вариант

                        while ($v = $variantList->Fetch())
                        {


                            // Получаем наименьшую ширину доски и на её основе строим ценник

                            $board_width = intval(preg_replace('/\D/', "", $v["PROPERTY_TEHNOLOGY_HOUSE_VALUE"]));

                            if ($board_width <= $current_board_width)
                            {
                                $current_board_width = $board_width;
                                $houses[$i]["CATEGORY_ID"] = $c["ID"];
                                $houses[$i]["ID"] = $variant_id;
                                $houses[$i]["DESCRIPTION"] = HTMLToTxt($v["PREVIEW_TEXT"]);	
                                $houses[$i]["AREA"] = (float)$v["PROPERTY_SQUARE_ALL_HOUSE_VALUE"];
                                $houses[$i]["FLOORS"] = $v["PROPERTY_FLOOR_VALUE"];

                                // Получаем дату создания и последнего изменения объявления (одинаковые)

                                $houses[$i]["CREATION_DATE"] = $houses[$i]["LAST_UPDATE_DATE"] = date("Y-m-d\TH:i:s+03:00");

                                // Получаем ценники, откуда берём URL и цену дома

                                $priceSelect = Array("PROPERTY_PRICE_1", "PROPERTY_LINK");
                                $priceFilter = Array("IBLOCK_ID" => 22, "ID" => $v['PROPERTY_PRICE_HOUSE_VALUE'], "ACTIVE" => "Y");
                                $priceList = CIBlockElement::GetList(array("SORT" => "ASC"), $priceFilter, false, false, $priceSelect);

                                while ($p = $priceList->Fetch())
                                {
                                    // Берём минимальную цену из всех
                                    $houses[$i]["PRICE"] = min(array($houses[$i]["PRICE"]?:$p['PROPERTY_PRICE_1_VALUE'], $p['PROPERTY_PRICE_1_VALUE']));
                                    $houses[$i]["LINK"] = $p['PROPERTY_LINK_VALUE'];
                                }

                                $houses[$i]["IMAGE"] = CFile::GetPath($h["DETAIL_PICTURE"]);
                                $houses[$i]["NAME"] = $h["NAME"]; 
                            }
                        } 					                           
                    }
                }
                ++$i;
            }
        }

        $xmlstr = 
        '<?xml version="1.0" encoding="UTF-8"?>'.
        '<yml_catalog date="'.date('Y-m-d H:i').'">'.
        '</yml_catalog>';

        $yf = new SimpleXMLElement($xmlstr);

        $shop = $yf->addChild('shop');
        $shop->addChild('name', 'Теремъ');
        $shop->addChild('company', 'ООО Теремъ-про');
        $shop->addChild('url', 'http://terem-pro.ru');

        $currencies = $shop->addChild('currencies');
        $rubles = $currencies->addChild('currency');
        $rubles->addAttribute('id', 'RUR');
        $rubles->addAttribute('rate', '1');

        $categories = $shop->addChild('categories');

        foreach ($cats_array as $cid => $cname)
        {
            $cat = $categories->addChild('category', $cname);
            $cat->addAttribute('id', $cid);
        }

        $offers = $shop->addChild('offers');

        foreach($houses as $h)
        {
            $offer = $offers->addChild('offer');
            $offer->addAttribute('id', $h["ID"]);
            $offer->addAttribute('type', 'vendor.model');
            $offer->addAttribute('available', 'true');

            $offer->addChild('model', $h["NAME"]);
            $offer->addChild('url', 'http://terem-pro.ru'.$h["LINK"]);
            $offer->addChild('price', $h["PRICE"].'000');
            $offer->addChild('categoryId', $h["CATEGORY_ID"]);
            $offer->addChild('currencyId', 'RUR');
            $offer->addChild('picture', 'http://terem-pro.ru'.$h["IMAGE"]);
            $offer->addChild('description', $h["DESCRIPTION"]);
            $offer->addChild('country_of_origin', 'Россия');
            $offer->addChild('manufacturer_warranty', 'true');
            $offer->addChild('vendor', 'ООО Теремъ-Про');

        }

        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yf.xml", $yf->asXML());
    } 	

    protected static function UpdateParser($arFields)
    {
        CAddServicesParcer::ParceFileData($arFields);
        return $arFields;
    }

    // Функция обнровления основного фильтра на сайте
    // Вызывается методаим ElementUpdateHandler, ElementAdditionHandler, ElementDeletionHandler	

    protected static function UpdateFilter()
    {
        AddTask('UpdateFilter');
    }    

    public static function ElementDeletionPreventer($ID)
    {
        global $APPLICATION;
        $res = CIBlockElement::GetById($ID);
        $arRes = $res->GetNext(); 
        if(in_array($arRes["IBLOCK_ID"], self::$DeletionForbidden))
        {
            $APPLICATION->throwException("В этом инфоблоке запрещено удалять элементы!");
            return false;
        }
    }

    public static function ElementAdditionHandler(&$arFields)
    { 
        if (array_key_exists($arFields["IBLOCK_ID"], self::$AfterAdditionOccured)) 
        {
        // Вызываем требуемый обработчик для данного IBlock
        foreach (self::$AfterAdditionOccured[$arFields["IBLOCK_ID"]] as $method) self::$method($arFields); 
        }
    }

    public static function ElementUpdateHandler(&$arFields)
    { 
        if (array_key_exists($arFields["IBLOCK_ID"], self::$AfterUpdateOccured)) 
        {
                // Вызываем требуемый обработчик для данного IBlock
        foreach (self::$AfterUpdateOccured[$arFields["IBLOCK_ID"]] as $method) self::$method($arFields);  
        }
    }

    public static function ElementDeletionHandler($arFields)
    {
        if (array_key_exists($arFields["IBLOCK_ID"], self::$AfterDeletionOccured)) 
        {
                // Вызываем требуемый обработчик для данного IBlock
        foreach (self::$AfterDeletionOccured[$arFields["IBLOCK_ID"]] as $method) self::$method($arFields);  
        }
    }

    public static function LaunchDailyOperations($arFields)
    {
        include_once $_SERVER['DOCUMENT_ROOT'].'/include/tasks/daily.php';
        
        $files = scandir($_SERVER['DOCUMENT_ROOT'].'/include/tasks/daily/');
        unset($files[0], $files[1]);
        
        $h = fopen($_SERVER['DOCUMENT_ROOT'].'/include/tasks/dailylist.txt', 'w');
        foreach ($files as $f) fprintf($h, "%s %s\n", basename($f), date('d.m.Y'));
        fclose($h);
    }
 }
 

