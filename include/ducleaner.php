<?php
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;
if (!$USER->IsAuthorized()) die;

CModule::IncludeModule("iblock");

$id_calcs = CIBlockElement::GetList([],["IBLOCK_ID"=>21],0,0,["PROPERTY_ID_CALCULATOR"]);

$duarray = [];

while ($c = $id_calcs->GetNext())
{
    $duarray[] = $c["PROPERTY_ID_CALCULATOR_VALUE"];
}

$all_du = scandir('../upload/calcs/');

unset($all_du[0], $all_du[1]);

$count = 0;

foreach ($all_du as $du)
{
    if (!in_array($du, $duarray)) 
    {
        unlink('../upload/calcs/'.$du.'/calc_'.$du.'.data');
        rmdir('../upload/calcs/'.$du);
        ++$count;
    }
}

echo 'ДУ обновлены успешно. Удалено папок: '.$count;
   

?>