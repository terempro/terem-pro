<?php

/*
 * В данном файле глобально объявляеются частоиспользуемые функции для 
 * удобства использования на сайте
 */

// Встраиввание включаемой области

function Embed($what)
{
    global $APPLICATION;
    $APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file", 
        "PATH" => "/include/embeddable/$what.html"
        )
    );
}

/*
 *  Запуск необходимых функций перед выполнением программы
 */

// Создаём автозагрузчик классов

//spl_autoload_register(function($class){
//    include_once $_SERVER["DOCUMENT_ROOT"].'/include/app/classes/'.$class.'.php';
//});

// Добавление свойства раздела типа HTML

//AddEventHandler("main", "OnUserTypeBuildList", array("MultiStringBlock", "getUserTypeDescription"));

