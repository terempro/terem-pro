<?php

class HouseCatalog
{
    static private $variant;
    static private $is_desktop;
    static private $all_variants;
    static private $cheapest_karkas = 0;
    static private $cheapest_brus = 0;
    static private $cheapest_kirpich = 0;
    static private $cheapest_prices = array();

    static private function PriceProperty($value = false)
    {
        $res = City::IsMoscow() ? 'PROPERTY_PRICE_HOUSE' : 'PROPERTY_PRICE_HOUSE_'.CITY_CODE;
        return $value ? $res.'_VALUE' : $res;
    }

    static private function ByPrice()
    {
        return self::PriceProperty() . '.PROPERTY_PRICE_1';
    }

    static private function FindCheapestId($variants_array)
    {
        $variant_id = 0;
        $current_price = PHP_INT_MAX;
        $current_big_price = 0;

        $arOrder = [self::ByPrice() => 'asc'];
        $arFilter = ['IBLOCK_ID' => VARIANTS_IBLOCK, 'ID' => $variants_array];
        $arSelect = ['ID', self::PriceProperty()];
        $result = CIBlockElement::GetList($arOrder, $arFilter, 0, 0, $arSelect);

        while ($r = $result->GetNext())
        {
            // Получаем текущую цену цену

            if ($real_price = CIBlockElement::GetProperty(PRICES_IBLOCK,
                $r[self::PriceProperty(true)], [], ['CODE' => 'PRICE_1'])->GetNext()["VALUE"])
                {
                    if ($real_price < $current_price)
                    {
                        $current_price = $real_price;

                        // Получаем пугающую цену цену
                        $current_big_price = CIBlockElement::GetProperty(PRICES_IBLOCK,
                            $r[self::PriceProperty(true)], [], ['CODE' => 'PRICE_2'])->GetNext()["VALUE"];

                        $variant_id = $r['ID'];
                    }
                }
        }

        self::$cheapest_prices[$variant_id] = [
            'REAL' => number_format($current_price, 0, ' ', ' '),
            'BIG' => number_format($current_big_price, 0, ' ', ' '),
            'PROFIT' => number_format($current_big_price - $current_price, 0, ' ', ' ')
        ];
        return $variant_id;
    }

    static private function GetSliderData()
    {
        $picture_info =  CFile::GetFileArray(self::$variant['DETAIL_PICTURE']);
        $exteriers = $exteriers_small = $interiers = $interiers_small = [];
        
        if (self::$is_desktop)
        {
            $exteriers_array = (array)unserialize(htmlspecialcharsback(self::$variant["PROPERTY_EXTERIER_VALUE"]))['VALUE'];
            
            foreach ($exteriers_array as $image)
            {
                $exteriers[] = CFile::ResizeImageGet($image, Array("width" => 838, "height" => 556))['src'];
                $exteriers_small[] = CFile::ResizeImageGet($image, Array("width" => 120, "height" => 80))['src'];
            }

            $interiers_array = (array)unserialize(htmlspecialcharsback(self::$variant["PROPERTY_INTERIER_VALUE"]))['VALUE'];
            
            foreach ($interiers_array as $image)
            {
                $interiers[] = CFile::ResizeImageGet($image, Array("width" => 838, "height" => 556))['src'];
                $interiers_small[] = CFile::ResizeImageGet($image, Array("width" => 120, "height" => 80))['src'];
            }
        }

        return [
            // Основные данные
            'LINK_TO_CALC' => CITY_PATH.'/additional-services/'.self::$variant['CODE'].'/',

            // Изображения, планировки, экстерьеры и интерьеры
            'PICTURE' => $picture_info['SRC'] ?: 'http://placehold.it/838x556',
            'PICTURE_SMALL' => $picture_info
                ? CFile::ResizeImageGet($picture_info['ID'],
                ["width" => 120, "height" => 80])['src'] : 'http://placehold.it/120x80',
            'PICTURE_DESCRIPTION' => $picture_info['DESCRIPTION'] ? : '',

            '3D' => (self::$variant['PROPERTY_D_RACURS_VALUE'] and self::$is_desktop)
                ? html_entity_decode(self::$variant['PROPERTY_D_RACURS_VALUE']['TEXT']) : '',
            '3D_SMALL' => '/bitrix/templates/.default/assets/img/catalog-item/360.svg',

            'PLANS1' => self::$variant['PROPERTY_PLANS_HOUSE_1_VALUE']
                ? CFile::GetPath(self::$variant['PROPERTY_PLANS_HOUSE_1_VALUE']) : '',
            'PLANS1_SMALL' => self::$variant['PROPERTY_PLANS_HOUSE_1_VALUE']
                ? CFile::ResizeImageGet(self::$variant['PROPERTY_PLANS_HOUSE_1_VALUE'],
                ["width" => 120, "height" => 80])['src'] : 'http://placehold.it/120x80',

            'PLANS1_DESCRIPTION' => self::$variant['PROPERTY_PLANS_HOUSE_1_DESCRIPTION'] ? : '',

            'PLANS2' =>
                self::$variant['PROPERTY_PLANS_HOUSE_2_VALUE']
                ? CFile::GetPath(self::$variant['PROPERTY_PLANS_HOUSE_2_VALUE']) : '',
            'PLANS2_SMALL' => self::$variant['PROPERTY_PLANS_HOUSE_2_VALUE']
                ? CFile::ResizeImageGet(self::$variant['PROPERTY_PLANS_HOUSE_2_VALUE'],
                ["width" => 120, "height" => 80])['src'] : 'http://placehold.it/120x80',

            'PLANS2_DESCRIPTION' => self::$variant['PROPERTY_PLANS_HOUSE_2_DESCRIPTION'] ? : '',

            'INTERIERS' => $interiers,
            'EXTERIERS' => $exteriers,
            'INTERIERS_SMALL' => $interiers,
            'EXTERIERS_SMALL' => $exteriers,

            // Флаги
            'SHOW_INFO_ON_PHOTO' => self::$variant['PROPERTY_TECHNOLOGY_HOUSE_VALUE'] != 'Кирпичный'

        ];
    }

    static private function GetPackagesData()
    {
        $result = array();

        if (self::$cheapest_karkas)
        {
            $result['SHOW_KARKAS'] = true;
            $result['KARKAS_ACTIVITY'] = (!$_GET['tech'] or $_GET['tech'] == 'karkas') ? 'active' : '';
            $result['CHEAPEST_KARKAS'] = self::$cheapest_karkas;
            $result["KARKAS_REAL_PRICE"] = self::$cheapest_prices[self::$cheapest_karkas]['REAL'];
            $result["KARKAS_BIG_PRICE"] = self::$cheapest_prices[self::$cheapest_karkas]['BIG'];
            $result["KARKAS_PROFIT"] = self::$cheapest_prices[self::$cheapest_karkas]['PROFIT'];
        }

        if (self::$cheapest_brus)
        {
            $result['SHOW_BRUS'] = true;
            $result['BRUS_ACTIVITY'] = $_GET['tech'] == 'brus' ? 'active' : '';
            $result['CHEAPEST_BRUS'] = self::$cheapest_brus;
            $result["BRUS_REAL_PRICE"] = self::$cheapest_prices[self::$cheapest_brus]['REAL'];
            $result["BRUS_BIG_PRICE"] = self::$cheapest_prices[self::$cheapest_brus]['BIG'];
            $result["BRUS_PROFIT"] = self::$cheapest_prices[self::$cheapest_brus]['PROFIT'];
        }

        if (self::$cheapest_kirpich)
        {
            $result['SHOW_KIRPICH'] = true;
            $result['KIRPICH_ACTIVITY'] = $_GET['tech'] == 'kirpich' ? 'active' : '';
            $result['CHEAPEST_KIRPICH'] = self::$cheapest_kirpich;
            $result["KIRPICH_REAL_PRICE"] = self::$cheapest_prices[self::$cheapest_kirpich]['REAL'];
            $result["KIRPICH_BIG_PRICE"] = self::$cheapest_prices[self::$cheapest_kirpich]['BIG'];
            $result["KIRPICH_PROFIT"] = self::$cheapest_prices[self::$cheapest_kirpich]['PROFIT'];
        }

        return $result;
    }

    static private function GetPlansData()
    {
        $plans = array();

        for ($i = 3; $i <= 6; ++$i)
        {
            if (!self::$variant['PROPERTY_PLANS_HOUSE_'.$i.'_VALUE'])
                continue;

            $plans[$i] = [

                'IMAGE' => CFile::GetPath(self::$variant['PROPERTY_PLANS_HOUSE_'.$i.'_VALUE']),
                'PREVIEW' => CFile::ResizeImageGet(self::$variant['PROPERTY_PLANS_HOUSE_'.$i.'_VALUE'],
                    ["width" => 120, "height" => 80])['src'],
                'DESCRIPTION' => self::$variant['PROPERTY_PLANS_HOUSE_'.$i.'_DESCRIPTION'] ?: ''

            ];
        }

        return [

            'LINK_TO_CALC' => CITY_PATH.'/additional-services/'.self::$variant['CODE'].'/',
            'PLANS' => $plans,
            'SHOW_INFO_ON_PHOTO' => self::$variant['PROPERTY_TECHNOLOGY_HOUSE_VALUE'] != 'Кирпичный'

        ];
    }

    static private function GetVideoData()
    {
        if (!self::$variant['PROPERTY_VIDEO_VALUE']['TEXT'])
        {
            return ['SHOW_VIDEO' => false];
        }

        $result = array(
            'SHOW_VIDEO' => true,
            'VIDEOS' => [],
            'TITLE' => self::$variant['NAME'],
            'SHOWN' => self::$variant['SHOW_COUNTER']
        );

        $matches = explode('&lt;/video&gt;', self::$variant['PROPERTY_VIDEO_VALUE']['TEXT']);
        foreach ($matches as &$m)
        {
            $m .= '&lt;/video&gt;';
        }
        unset($m, $matches[count($matches)-1]);

        foreach ($matches as $m)
        {
            $result["VIDEOS"][] = html_entity_decode($m);
        }

        return $result;
    }

    static public function GetStartVariant($variants_array)
    {
        self::$all_variants = $variants_array;

        $result_array = array();
        switch ($_GET['tech'])
        {
            case 'brus':
                $result_array = self::$all_variants['BRUS'] ? : array();
                break;
            case 'kirpich':
                $result_array = self::$all_variants['KIRPICH'] ? : array();
                break;
            default:
                $result_array = array_merge(self::$all_variants['KARKAS'] ? : array(),
                    self::$all_variants['BRUS'] ? : array(),
                    self::$all_variants['KIRPICH'] ? : array());
        }
        // Если вариантов не нашлось, возвращаем пустой массив - признак
        // того, что этого дома нет в прайсе и его мы показывать не будем

        if (!$result_array)
            return array();

        $arOrder = [self::ByPrice() => 'asc'];
        $arFilter = ['IBLOCK_ID' => VARIANTS_IBLOCK,
            'ID' => $result_array, '!' . self::PriceProperty() => false];
        $arNavParams = ['nTopCount' => 1];
        $arSelect = [

            // Основные данные

            'CODE',
            'NAME',
            'SHOW_COUNTER',

            // Изображения и планировки
            'DETAIL_PICTURE',
            'PROPERTY_D_RACURS',
            'PROPERTY_PLANS_HOUSE_1',
            'PROPERTY_PLANS_HOUSE_2',
            'PROPERTY_EXTERIER',
            'PROPERTY_INTERIER',

            // Дополнительные планировки
            'PROPERTY_PLANS_HOUSE_3',
            'PROPERTY_PLANS_HOUSE_4',
            'PROPERTY_PLANS_HOUSE_5',
            'PROPERTY_PLANS_HOUSE_6',

            // Видео
            'PROPERTY_VIDEO',

            //Другие свойства
            'PROPERTY_CODE_1C',
            'PROPERTY_SQUARE_ALL_HOUSE',

            // Служебные свойства
            'PROPERTY_TECHNOLOGY_HOUSE',
            self::PriceProperty(),

        ];

        // Получаем вариант
        self::$variant = CIBlockElement::GetList($arOrder,
            $arFilter, 0, $arNavParams, $arSelect)->GetNext();

        // Добавляем текущую цену цену
        self::$variant['REAL_PRICE'] = CIBlockElement::GetProperty(PRICES_IBLOCK,
            self::$variant[self::PriceProperty(true)], [], ['CODE' => 'PRICE_1'])->GetNext()["VALUE"];

        // Добавляем пугающую цену цену
        self::$variant['BIG_PRICE'] = CIBlockElement::GetProperty(PRICES_IBLOCK,
            self::$variant[self::PriceProperty(true)], [], ['CODE' => 'PRICE_2'])->GetNext()["VALUE"];

        self::$cheapest_karkas = self::FindCheapestId(self::$all_variants["KARKAS"]);
        self::$cheapest_brus = self::FindCheapestId(self::$all_variants["BRUS"]);
        self::$cheapest_kirpich = self::FindCheapestId(self::$all_variants["KIRPICH"]);

        // Если не установлена текущая цена, возвращаем пустой массив - признак
        // того, что этого дома нет в прайсе и его мы показывать не будем

        if (!self::$variant or !self::$variant['REAL_PRICE'])
            return array();

        // Определяем, зашёл ли человек с ПК
        self::$is_desktop = !((new Mobile_Detect)->isMobile());

        $result = [

            // Данные для слайдера
            'SLIDER_DATA' => self::GetSliderData(),
            // Данные для набора комплектаций
            'PACKAGES_DATA' => self::GetPackagesData(),
            // Данные для секции с видео
            'VIDEO_DATA' => self::GetVideoData(),
            // Данные для секции с доп. планировками
            'PLANS_DATA' => self::GetPlansData(),

            // Общие данные
            'CODE_1C' => self::$variant['PROPERTY_CODE_1C_VALUE'],
            'TOTAL_SQUARE' => self::$variant['PROPERTY_SQUARE_ALL_HOUSE_VALUE'],
            'BACK_URL' => strpos($_SERVER['HTTP_REFERER'], $_SERVER["HTTP_HOST"].'/catalog/')
                ? $_SERVER['HTTP_REFERER'] : '/catalog/',

            // Флаги
            'SHOW_PLANS' => (bool)self::$variant['PROPERTY_PLANS_HOUSE_3_VALUE'],
            'SHOW_VIDEO' => (bool)self::$variant['PROPERTY_VIDEO_VALUE'],

            // ID самых дешёвых вариантов каждого типа
            'CHEAPEST_KARKAS' => self::$cheapest_karkas,
            'CHEAPEST_BRUS' => self::$cheapest_brus,
            'CHEAPEST_KIRPICH' => self::$cheapest_kirpich,

        ];

        return $result;
    }
}
