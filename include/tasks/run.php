<?php
$tasks = explode(',', file_get_contents($_SERVER["DOCUMENT_ROOT"].'/include/tasks/tasklist.txt'));
if (!$tasks[0]) $tasks = array();

foreach ($tasks as $t)
{
    include_once($_SERVER["DOCUMENT_ROOT"].'/include/tasks/'.$t.'.php');
}

file_put_contents($_SERVER["DOCUMENT_ROOT"].'/include/tasks/tasklist.txt', '');

