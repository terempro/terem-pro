<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

const BANNERS_IBLOCKS = [25, 60, 61];
const EMAIL_LIST = ['evtyushkin@terem-pro.ru', 'panova@terem-pro.ru'];

CModule::IncludeModule("iblock"); 

$banners_string = '';

foreach (BANNERS_IBLOCKS as $iblock)
{
    $arFilter = [
        'IBLOCK_ID' => $iblock, 
        'ACTIVE_DATE' => 'Y', 
        'ACTIVE' => 'Y', 
        '<=DATE_ACTIVE_TO' => ConvertTimeStamp(time()+60*60*24, 'FULL')
    ];
    
    $banners = CIBlockElement::GetList([], $arFilter, false, false, ['ID', 'NAME', 'IBLOCK_CODE', 'DATE_ACTIVE_TO']);
    
    while ($b = $banners->GetNext())
    {
        $banners_string .= 'Баннер <b>'.strip_tags($b["~NAME"]).'</b>'.
                           ' (ID: '.$b['ID'].', ИБ: '.$b['IBLOCK_CODE'].')'.
                           ' закончит свою активность в '.$b['DATE_ACTIVE_TO'].
                           ' и требует незамедлительного обновления!<br>';
    }
}

if ($banners_string)
{
    $message = '<span style="color:red;font:bold 48px \'Arial Black\';">СРОК БАННЕРОВ ИСТЕКАЕТ!</span><br>'.
               $banners_string;
    
    foreach(EMAIL_LIST as $email)
    {
        Mail::S($email, '/!\\ СРОК БАННЕРОВ ИСТЕКАЕТ /!\\', $message);
    }
               
}

    
