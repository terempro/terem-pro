<?php

$h = fopen($_SERVER['DOCUMENT_ROOT'].'/include/tasks/dailylist.txt', 'r');

while ($task = fgets($h))
{
    $data = explode(' ', $task);
    if (strpos($data[1], date('d.m.Y')) === false)
    {
        include_once $_SERVER['DOCUMENT_ROOT'].'/include/tasks/daily/'.$data[0];
    }
}

fclose($h);



