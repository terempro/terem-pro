<?php
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (CModule::IncludeModule("iblock")) 
{
    global $USER, $DB, $CITY_CODES_ARRAY;
    
    foreach ($CITY_CODES_ARRAY as $code => $pblock_id)
    {
        $price_suffix = $code == 'MOSCOW' ? '' : '_'.$code;
        $table_suffix = $code == 'MOSCOW' ? '' : '_'.strtolower($code);
        //Собираем серии

        $SERIES = Array();
        $arSelect = Array(
            "ID",
            "NAME",
            "IBLOCK_SECTION_ID",
            "PROPERTY_KARKAS_VARIANTS_HOUSE",
            "PROPERTY_BRUS_VARIANTS_HOUSE",
            "PROPERTY_KIRPICH_VARIANTS_HOUSE",
            "PROPERTY_VARIANTS_HOUSE",
            "PROPERTY_SERIES"
        );
        $arFilter = Array("IBLOCK_ID" => 13, "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 500), $arSelect);
        $cnt = 0;

        //Присваеваем в массив серий все необходимые занчания для  фильтра из серий

        while ($ob = $res->GetNextElement()) 
        {
            $data = $ob->GetFields();               //echo "<pre>";var_dump($data);die();

                    $ar_groups = array();
                    $db_old_groups = CIBlockElement::GetElementGroups($data["ID"], true);
                    while($group = $db_old_groups->Fetch()) $ar_groups[] = $group;			    
                    //echo "<pre>";
                    //var_dump($ar_groups);

            foreach ($ar_groups as $g)
            {
                    //Получаем и присваеваем имя категории по ID
                    $res_cat = CIBlockSection::GetByID($g['ID']);
                    if ($ar_res_cat = $res_cat->GetNext()) 
                    {
                        $SERIES[$cnt]['category_name'] = $ar_res_cat['NAME'];
                    }

                            $SERIES[$cnt]['id_category'] = (int) $g['ID'];
                    $SERIES[$cnt]['id_series'] = (int) $data['ID'];
                    $SERIES[$cnt]['series_name'] = $data["PROPERTY_SERIES_VALUE"];

                    foreach($data["PROPERTY_KARKAS_VARIANTS_HOUSE_VALUE"]  as $v) $SERIES[$cnt]['houses'][] = $v;
                            foreach($data["PROPERTY_BRUS_VARIANTS_HOUSE_VALUE"]    as $v) $SERIES[$cnt]['houses'][] = $v;
                            foreach($data["PROPERTY_KIRPICH_VARIANTS_HOUSE_VALUE"] as $v) $SERIES[$cnt]['houses'][] = $v;
                    $cnt++;
                    }

            }//die();

        //Собираем дома

        $HOUSE = Array();
        $cnt = 0;
        foreach ($SERIES as $k => $SERIE) 
        {
            foreach ($SERIE['houses'] as $v) 
            {
                $ELEMENT_ID = $v;
                $arSelect = Array(
                    "ID",
                    "NAME",
                    "ACTIVE",
                    "PROPERTY_SQUARE_ALL_HOUSE",
                    "PROPERTY_SQUARE_HOUSE",
                    "PROPERTY_SQUARE_LIFE_HOUSE",
                    "PROPERTY_FLOOR",
                    "PROPERTY_TEHNOLOGY_HOUSE",
                    "PROPERTY_PRICE_HOUSE$price_suffix",
                    "PROPERTY_SIZE1",
                    "PROPERTY_SIZE2",
                );

                $arFilter = Array("IBLOCK_ID" => 21, "ID" => $ELEMENT_ID);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 800), $arSelect);

                while ($ob = $res->GetNextElement()) 
                {
                    $cnt++;
                    $data = $ob->GetFields();

                    $HOUSE[$cnt]['id_house'] = (int) $data['ID'];
                    $HOUSE[$cnt]['house_name'] = $data['NAME'];
                    $HOUSE[$cnt]['active'] = $data['ACTIVE'] == 'Y' ? 1 : 0;

                    $HOUSE[$cnt]['id_category'] = $SERIES[$k]['id_category'];
                    $HOUSE[$cnt]['id_series'] = $SERIES[$k]['id_series'];
                    $HOUSE[$cnt]['series_name'] = $SERIES[$k]['series_name'];
                    $HOUSE[$cnt]['category_name'] = $SERIES[$k]['category_name'];

                    $HOUSE[$cnt]['total_area'] = (float)$data["PROPERTY_SQUARE_ALL_HOUSE_VALUE"];
                    $HOUSE[$cnt]['building_area'] = (float)$data["PROPERTY_SQUARE_HOUSE_VALUE"];
                    $HOUSE[$cnt]['living_area'] = (float)$data["PROPERTY_SQUARE_LIFE_HOUSE_VALUE"];
                    $HOUSE[$cnt]['floors'] = (int)$data["PROPERTY_FLOOR_VALUE"];
                    $HOUSE[$cnt]['technology'] = $data["PROPERTY_TEHNOLOGY_HOUSE_VALUE"];

                    $HOUSE[$cnt]['length'] = $data["PROPERTY_SIZE1_VALUE"];
                    $HOUSE[$cnt]['width'] = $data["PROPERTY_SIZE2_VALUE"];

                    if (stristr($data["PROPERTY_TEHNOLOGY_HOUSE_VALUE"], "аркасный")) $HOUSE[$cnt]['short_technology'] = 'Каркас';
                    if (stristr($data["PROPERTY_TEHNOLOGY_HOUSE_VALUE"], "ирпичный")) $HOUSE[$cnt]['short_technology'] = 'Кирпич'; 
                    if (stristr($data["PROPERTY_TEHNOLOGY_HOUSE_VALUE"], "брус")) $HOUSE[$cnt]['short_technology'] = 'Брус';

                    $PRICE_ID = $data["PROPERTY_PRICE_HOUSE{$price_suffix}_VALUE"];
                    
                    if (!$PRICE_ID)
                    {
                        unset($HOUSE[$cnt]);
                        continue;
                    }
                    
                    $arSelect_price = Array("ID", "NAME", "PROPERTY_PRICE_1", "PROPERTY_OPTIMAL_PRICE");
                    $arFilter_price = Array("IBLOCK_ID" => $pblock_id, "ID" => $PRICE_ID);
                    $res_price = CIBlockElement::GetList(Array(), $arFilter_price, false, Array("nPageSize" => 500), $arSelect_price);
                    while ($ob_price = $res_price->GetNextElement()) 
                    {
                        $data_price = $ob_price->GetFields();
                        $price_default = (int) $data_price["PROPERTY_PRICE_1_VALUE"];
                        $price_optimal = (int) $data_price["PROPERTY_OPTIMAL_PRICE_VALUE"];
                        $HOUSE[$cnt]['id_price'] = (int) $PRICE_ID;
                        $HOUSE[$cnt]['price_value'] = ($price_default <= 0 || $price_default == $price_optimal) 
                                                                ? (int)$price_optimal 
                                                                : (int)$price_default;
                        
                        if ($HOUSE[$cnt]['price_value'] == 0) unset($HOUSE[$cnt]);
                    }
                }
            }
        }

        $clear_table_sql = "TRUNCATE TABLE b_filter_house$table_suffix;";
        $DB->Query($clear_table_sql);

        foreach ($HOUSE as $h) 
        {
            if (!$h['id_price']) continue;
            $insert_one_house_sql = "INSERT INTO `b_filter_house$table_suffix` "
                    . "(`id_category`, `id_series`, `id_house`, `category_name`, `series_name`, `house_name`, `id_price`, `price_value`, `total_area`, `building_area`, `living_area`, `floors`, `technology`, `short_technology`, `active`,`width`,`length`) VALUES "
                    . "({$h['id_category']}, {$h['id_series']}, {$h['id_house']}, '{$h['category_name']}', '{$h['series_name']}', '{$h['house_name']}', {$h['id_price']}, '{$h['price_value']}', {$h['total_area']}, {$h['building_area']}, {$h['living_area']}, {$h['floors']}, '{$h['technology']}', '{$h['short_technology']}', {$h['active']},'{$h['width']}','{$h['length']}');";

            //echo $insert_one_house_sql."<br/>";
            //die();

            $res_sql = $DB->Query($insert_one_house_sql);
            //echo "<pre>";
            //var_dump($res_sql);
        }
        
    }
}	

?>
