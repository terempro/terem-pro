<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iblock');

const ADS_IBLOCK = 59;

// Список всех UTM-данных разбитых, по кампаниям

$other_values_str = preg_replace('/\,\s+/', ',', htmlspecialchars($_GET['is']));

$is = explode(',', $other_values_str);

$value = htmlspecialchars($_GET['utm']);

$utm_list = array();

$utm_request = CIBlockElement::GetList(
        ["ID" => 'desc'], 
        ["PROPERTY_UTM_CAMPAIGN" => "$value%", "IBLOCK_ID" => ADS_IBLOCK],
        false,
        false,
        ["PROPERTY_UTM_CAMPAIGN"]
        );

while ($utm = $utm_request->GetNext())
{
    $utm_list[] = $utm["PROPERTY_UTM_CAMPAIGN_VALUE"];
}

$utm_list = array_unique($utm_list);

foreach ($utm_list as $utm)
{
    if (!count($is) || !in_array($utm, $is))
        echo '<span class="hint" onclick="useHint(this)">'.$utm.'</span>';
}
