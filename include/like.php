<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$param = (int)$_POST['id'];
$cookie_name = 'voitess_' . $param;

$ELEMENT_ID = $param;  // код элемента
$PROPERTY_CODE = "LIKES";


$db_props = CIBlockElement::GetProperty(35, $ELEMENT_ID, array("sort" => "asc"), Array("CODE" => "LIKES"));
if ($ar_props = $db_props->GetNext()) {
    $START_VALUE = IntVal($ar_props["VALUE"]);
}
else
{
    $START_VALUE = 0;
}

$ipprops = CIBlockElement::GetList(array(), ["IBLOCK_ID" => $ELEMENT_ID, "ID" => 271969], 0,0, array("PROPERTY_IP"));

$ips = array();

while ($ipprop = $ipprops->GetNext())
{
    $ips[] = $ipprop["PROPERTY_IP_VALUE"];
}


$PROPERTY_VALUE = $START_VALUE + 1;

if (!$_COOKIE[$cookie_name] && !in_array($_SERVER["REMOTE_ADDR"], $ips)) 
{
    setcookie($cookie_name, 1, time() * 2);
    $ips[] = $_SERVER["REMOTE_ADDR"];

    CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
    CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("IP" => $ips));
    echo $PROPERTY_VALUE;
} 
else 
{
    echo $START_VALUE;
}


