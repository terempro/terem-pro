<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>

<?php if (CModule::IncludeModule("iblock")): ?> 

    <?php

    $IBLOCK_ID = 34;
    $d3 = 29087; //3д забор
    $euro = 29085; //Евроштакетник
    $prof = 29084; //Профнастил
    $rab = 29086; //Рабица
    //Список
    $kraska = "PROPERTY_KRASKA_VALUE";

    //Значение
    $DLINA = "PROPERTY_DLINA";
    $VISOTA = "PROPERTY_VISOTA";
    $SHAG = "PROPERTY_SHAG";
    $COLOR = "PROPERTY_COLOR";
    $W1 = "PROPERTY_W1";
    $W2 = "PROPERTY_W2";


    //Калитка 
    $price_kalitka = 4890;
    //Ворота
    $price_vorota = 9880;


    $arSelect = Array(
        "ID",
        "NAME",
        "PROPERTY_DLINA",
        "PROPERTY_VISOTA",
        "PROPERTY_COLOR",
        "PROPERTY_SHAG",
        "PROPERTY_KRASKA",
        "PROPERTY_W1",
        "PROPERTY_W2",
        "PROPERTY_PRICE"
    );
    ?>

    <?php

    //Начало профнастил
    if ($_POST['type'] == 'prof') {

        //Высота забора
        $DLINA_VAL = $_POST["ProfVisota"];

        //Длина забора
        $VISOTA_VAL = $_POST["ProfZKM"];

        //Толщина
        $W1_VAL = $_POST['ProfTolsh'];

        //Цвет забора 
        $COLOR_VAL = $_POST["ProfColor"];

        //Окрашивание забора
        $kraska_val = $_POST["ProfKraska"];

        //Ворота
        $VOROTA_COUNT_VAL = $_POST["ProfVorota"];

        //Калитка
        $KALITKA_COUNT_VAL = $_POST["ProfKalitka"];

        //Расстояние до участка
        $KMHOUSE_VAL = $_POST["PromKM"];




        //Выбор профнастила
        if ($kraska_val != 'disabled') {
            $arFilter = array(
                "IBLOCK_ID" => IntVal($IBLOCK_ID),
                "SECTION_ID" => 29084,
                "ACTIVE" => "Y",
                array("LOGIC" => "AND",
                    array($DLINA => $DLINA_VAL),
                    array($COLOR => $COLOR_VAL),
                    array($W1 => $W1_VAL),
                    array("=" . $kraska => $kraska_val),
                )
            );
        } else {
            $arFilter = array(
                "IBLOCK_ID" => IntVal($IBLOCK_ID),
                "SECTION_ID" => 29084,
                "ACTIVE" => "Y",
                array("LOGIC" => "AND",
                    array($DLINA => $DLINA_VAL),
                    array($COLOR => $COLOR_VAL),
                    array($W1 => $W1_VAL),
                )
            );
        }



        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 200), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $Prof[] = $ob->GetFields();
        }

        sleep(1);
        if (count($Prof) == 1) {
            $price = (int) $Prof[0]['PROPERTY_PRICE_VALUE'];
            $price2 = (int) $VISOTA_VAL;

            $vorota_price = 0;
            if ($VOROTA_COUNT_VAL != 'none') {
                $vorota_price = (int) $VOROTA_COUNT_VAL * $price_vorota;
            }

            $kalitka_price = 0;
            if ($KALITKA_COUNT_VAL != 'none') {
                $kalitka_price = (int) $KALITKA_COUNT_VAL * $price_kalitka;
            }

            $km_price = (int) $KMHOUSE_VAL;
            if ($km_price > 250) {
                $km_price = ($km_price - 250) * 25;
            } else {
                $km_price = 0;
            }


            $cena = ($price * $price2) + $vorota_price + $kalitka_price + $km_price;
            echo $cena;
        } else {
            $price = 1220;
            $price2 = (int) $VISOTA_VAL;

            $vorota_price = 0;
            if ($VOROTA_COUNT_VAL != 'none') {
                $vorota_price = (int) $VOROTA_COUNT_VAL * $price_vorota;
            }
            $kalitka_price = 0;
            if ($KALITKA_COUNT_VAL != 'none') {
                $kalitka_price = (int) $KALITKA_COUNT_VAL * $price_kalitka;
            }

            $km_price = (int) $KMHOUSE_VAL;
            if ($km_price > 250) {
                $km_price = ($km_price - 250) * 25;
            } else {
                $km_price = 0;
            }

            $cena = ($price * $price2) + $vorota_price + $kalitka_price + $km_price;
            echo $cena;
        }
    }
    //Конец профнастил



    if ($_POST['type'] == 'ero') {
        //Шаг
        $EroShag = $_POST['EroShag'];

        //Высота
        $EroVisota = $_POST['EroVisota'];

        //Цвет
        $EroColor = $_POST['EroColor'];

        //Краска
        $EroKraska = $_POST['EroKraska'];


        $EroRast = $_POST['EroRast'];
        $EroKalitka = $_POST['EroKalitka'];
        $EroVorota = $_POST['EroVorota'];
        $EroDlz = $_POST['EroDlz'];

        $arFilter = array(
            "IBLOCK_ID" => IntVal($IBLOCK_ID),
            "SECTION_ID" => 29085,
            "ACTIVE" => "Y",
            array("LOGIC" => "AND",
                array($DLINA => $EroVisota),
                array($COLOR => $EroColor),
                array($SHAG => $EroShag),
                array("=" . $kraska => $EroKraska),
            )
        );

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 200), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $Ero[] = $ob->GetFields();
        }

        sleep(1);

        if (count($Ero) == 1) {
            $price = (int) $Ero[0]['PROPERTY_PRICE_VALUE'];
            $price2 = (int) $EroDlz;

            $kalitka_price = 0;
            if ($EroKalitka != 'none') {
                $kalitka_price = (int) $EroKalitka * $price_kalitka;
            }

            $vorota_price = 0;
            if ($EroVorota != 'none') {
                $vorota_price = (int) $EroVorota * $price_vorota;
            }


            $km_price = (int) $EroRast;
            if ($km_price > 250) {
                $km_price = ($km_price - 250) * 25;
            } else {
                $km_price = 0;
            }

            $cena = ($price * $price2) + $vorota_price + $kalitka_price + $km_price;
            echo $cena;
        } else {
            echo "ERROR";
        }
    }

    if ($_POST['type'] == 'd3') {
        //Высота
        $D3Visota = $_POST['D3Visota'];

        //Цвет
        $D3Color = $_POST['D3Color'];


        //Ворота
        $D3Vorota = $_POST['D3Vorota'];

        //Калитка
        $D3Kalitka = $_POST['D3Kalitka'];

        //Растояние
        $D3Rast = $_POST['D3Rast'];

        //Длина забора
        $D3Dlin = $_POST['D3Dlin'];


        $arFilter = array(
            "IBLOCK_ID" => IntVal($IBLOCK_ID),
            "SECTION_ID" => 29087,
            "ACTIVE" => "Y",
            array("LOGIC" => "AND",
                array($DLINA => $D3Visota),
                array($COLOR => $D3Color),
            )
        );

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 200), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $D3D[] = $ob->GetFields();
        }

        sleep(1);

        if (count($D3D) == 1) {
            $price = (int) $D3D[0]['PROPERTY_PRICE_VALUE'];
            $price2 = (int) $D3Dlin;

            $kalitka_price = 0;
            if ($D3Kalitka != 'none') {
                $kalitka_price = (int) $D3Kalitka * $price_kalitka;
            }

            $vorota_price = 0;
            if ($D3Vorota != 'none') {
                $vorota_price = (int) $D3Vorota * $price_vorota;
            }


            $km_price = (int) $D3Rast;
            if ($km_price > 250) {
                $km_price = ($km_price - 250) * 25;
            } else {
                $km_price = 0;
            }

            $cena = ($price * $price2) + $vorota_price + $kalitka_price + $km_price;
            echo $cena;
        } else {
            echo "ERROR";
        }
    }


    if ($_POST['type'] == 'rab') {
        $RabVisota = $_POST['RabVisota'];
        $RabVorota = $_POST['RabVorota'];
        $RabKalitaka = $_POST['RabKalitaka'];
        $RabRast = $_POST['RabRast'];
        $RabDlin = $_POST['RabDlin'];



        $arFilter = array(
            "IBLOCK_ID" => IntVal($IBLOCK_ID),
            "SECTION_ID" => 29086,
            "ACTIVE" => "Y",
            $DLINA => $RabVisota
        );

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 200), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $Rabz[] = $ob->GetFields();
        }





        sleep(1);

        if (count($Rabz) == 1) {



            $price = (int) $Rabz[0]['PROPERTY_PRICE_VALUE'];
            $price2 = (int) $RabDlin;

            $kalitka_price = 0;
            if ($RabKalitaka != 'none') {
                $kalitka_price = (int) $RabKalitaka * $price_kalitka;
            }

            $vorota_price = 0;
            if ($RabVorota != 'none') {
                $vorota_price = (int) $RabVorota * $price_vorota;
            }


            $km_price = (int) $RabRast;
            if ($km_price > 250) {
                $km_price = ($km_price - 250) * 25;
            } else {
                $km_price = 0;
            }

            $cena = ($price * $price2) + $vorota_price + $kalitka_price + $km_price;
            echo $cena;
        } else {
            echo "ERROR";
        }
    }
    ?>   




<?php endif; ?>

