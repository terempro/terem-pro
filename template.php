<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$this->setFrameMode(true);

$page = $APPLICATION->GetCurPage();
$href_str = 'doma';

$IBLOCK_ID = 4;
$title1 = 'Проекты бань из срубов';
$title2 = 'Проекты бань по этажности';
$pos_str = "бань";

if (strpos($page, $href_str)) {
    $IBLOCK_ID = 3;
    $title1 = 'Проекты домов из срубов';
    $title2 = 'Проекты домов по этажности';
    $pos_str = "руб";
}

$sections = [];

$arFilter = Array('IBLOCK_ID' => $IBLOCK_ID, 'GLOBAL_ACTIVE' => 'Y');

$db_list = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, true);
while ($ar_result = $db_list->GetNext()) {
    $sections[] = $ar_result;
}

?>
<div class="row">
    <div class="col-xs-12 col-md-4 menu-col">
            <div class="filter left-doma">
                <a href="" class="title-left"><?= $title1 ?></a>
                <div class="top">
                    <ul>
                        <?php foreach ($sections as $arSection): ?>
                            <?php $pos = strpos($arSection['NAME'], $pos_str); ?>
                            <?php if ($pos): ?>
                                <li><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME'] ?></a></li>
                            <?php endif; ?>    
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="filter left-bani">
                <a href="" class="title-left"><?= $title2 ?></a>
                <div class="top">
                    <ul>
                        <?php foreach ($sections as $arSection): ?>
                            <?php $pos1 = strpos($arSection['NAME'], "мансар"); ?>
                            <?php $pos2 = strpos($arSection['NAME'], "этаж"); ?>
                            <?php if ($pos1 || $pos2): ?>
                                <li><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME'] ?></a></li>
                            <?php endif; ?>    
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            
            
        </div>
    <div class="col-xs-12 col-md-8 content-col">
        <div class="content">
            <h1 class="title"><?php $APPLICATION->ShowTitle(); ?></h1>
            <ul class="projects">
                <?php foreach ($arResult['ITEMS'] as $arItem): ?>

                
                <?php
                $file_big = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 800, 'height' => 600), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                ?>
                    <li>
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>" title="<?= $arItem['NAME'] ?>">
                            <span class="price"><?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?> руб.</span>
                            <span class="project-name"><?= $arItem['NAME'] ?></span>
                            <p class="text-section">Размер: <?= $arItem["PROPERTIES"]["SIZE_SRUB"]["VALUE"] ?></p>
                            <p class="text-section"><strong>Подробнее..</strong></p>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>

           <?=  html_entity_decode($arResult["DESCRIPTION"])?>     
        </div>
    </div>
</div>