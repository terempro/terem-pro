<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<script type="text/javascript" scr="/bitrix/templates/.default/assets/js/loadphoto.js"></script>
<section class="content white" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side + my-margin clearfix Warranty">

					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<h1 class="text-uppercase text-center">
								<span>Конкурс детского рисунка</span> – выигрывайте вместе с ребенком гироскутер!
							</h1>
							<div class="title-link text-center"></div>
							<div class="Warranty-steps">
								<div>
									<div class="Warranty-steps-item">
										<div>
											<div>
												<img src="/bitrix/templates/.default/assets/img/pencil.png" alt="">
											</div>
											<div class="right-b">
												<p>
													Нарисуйте с ребенком<br>
													рисунок на тему
												</p>
												<p>
													«ДОМ МОЕЙ<br>МЕЧТЫ»
												</p>
												<p>
													отсканируйте его
												</p>
											</div>
										</div>
									</div>
								</div>
								<div>
									<div class="Warranty-steps-item">
										<div>
											<div>
												<img src="/bitrix/templates/.default/assets/img/w-item-2.png" alt="">
											</div>
											<div>
												<p>ЗАПОЛНИТЕ<br>анкету и приложите фото</p>
											</div>
										</div>
									</div>
								</div>
								<div>
									<div class="Warranty-steps-item">
										<div>
											<div>
												<p>
													Участвуйте в голосовании и<br>
													выиграйте сертификат<br>
													на 20&nbsp;000&nbsp;рублей<br>
													на покупку
												</p>
											</div>
											<div>
												<p>
													ГИРОСКУТЕРА!
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<form id="load-image" action="/include/loadphoto.php" method="post" class="form-send" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-12 col-md-4 col-sm-4">
										<div class="form-group">
											<div class="row">
												<div class="col-xs-12">
													<label for="">Имя и фамилия ребенка</label>
												</div>
												<div class="col-xs-12">
													<input type="text" name="child-name" class="form-control my-input" placeholder="Иван Иванов" required="">
												</div>
												<div class="col-xs-12">
													<div class="help-block with-errors"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-4 col-sm-4">
										<div class="form-group">
											<div class="row">
												<div class="col-xs-12">
													<label for="">ФИО родителя</label>
												</div>
												<div class="col-xs-12">
													<input type="text" name="parent" class="form-control my-input" placeholder="Иван Иванов Иванович" required="">
												</div>
												<div class="col-xs-12">
													<div class="help-block with-errors"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-4 col-sm-4">
										<div class="form-group">
											<div class="row">
												<div class="col-xs-12">
													<label for="">E-mail</label>
												</div>
												<div class="col-xs-12">
													<input type="email" name="email" class="form-control my-input" placeholder="IvanovII@domain.ru" required="">
												</div>
												<div class="col-xs-12">
													<div class="help-block with-errors"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<div class="form-group">
											<div class="row">
												<div class="col-xs-12 col-md-4 col-sm-4">
													<div class="row">
														<div class="col-xs-12">
															<label for="">Возраст ребенка</label>
														</div>
														<div class="col-xs-12">
															<input type="number" name="child-age" class="form-control my-input" placeholder="10 лет" required="">
														</div>
														<div class="col-xs-12">
															<div class="help-block with-errors"></div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-md-8 col-sm-8">
													<div>
														<label for="">Загрузите рисунок</label>
													</div>
													<div class="file_upload">
														<div>
															<mark class="my-input">Загрузите рисунок</mark>
															<span class="button my-input">Выбрать файл</span>
															<input type="file" class="file" name="image" id="upload-images" required="required">
														</div>
													</div>
												</div>
												<!-- <div class="col-xs-12 col-md-8 col-sm-8">
													<div id="images_preview" class="images-preview"></div>
												</div> -->
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-xs-12 col-md-6">
											<p style="padding-top: 3px;">* Отправляя заявку, Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">политикой конфиденциальности и правилами пользования сайтом</a> и даете <a href="/privacy-policy/" target="_blank">согласие  на использование загруженных изображений</a>.</p>
										</div>
										<div class="col-xs-12 col-md-6 text-right">
                                                                                    <span id="response-message" class="respond-message"></span>&nbsp;&nbsp;
                                                                                    <button id="load-image-button" type="submit" class="btn btn-danger text-uppercase" style="pointer-events: all; cursor: pointer;">Отправить</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content white " role="content" data-step="2">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side + my-margin clearfix Warranty">
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<h4 class="text-uppercase">условия участия</h4>
							<ul class="decoratorion">
								<li>Сфотографируйте строение от компании «Теремъ» согласно техническим требованиям и загрузите <strong>8 фотографий</strong> в данном разделе.</li>
								<li>Заполните свои данные: ФИО, e-mail, и № договора (все поля являются обязательными для заполнения).</li>
								<li>Ознакомьтесь с согласием на использование изображений, а также политикой конфиденциальности и правилами пользования сайтом. <br>Подтвердите свое согласие.</li>
								<li>Нажмите кнопку «Отправить».</li>
								<li>На указанный Вами электронный адрес придет сообщение о получении информации и о результатах проверки фотографий на соответствие заявленным техническим требованиям.</li>
								<li>Период модерации – 7 дней.</li>
								<li>Сертификат о продлении гарантии Вы можете получить на выставочной площадке компании «Теремъ» по адресу ул, Зеленодольская, вл. 42 в договорном отделе, сообщив номер своего договора и ФИО заказчика.</li>

							</ul>
							<h4 class="text-uppercase">технические требования к фотографиям </h4>
							<ol>
								<li>
									Съемку производить в светлое время суток. Дом должен быть хорошо освещен.
								</li>
								<li>Ракурсы – не менее 8 отличных друг от друга.</li>
								<li>Ориентация снимка – горизонтальная.</li>
								<li>Рамер фотографии (вес) – не менее 3 мБ и не более 7 мБ.</li>
								<li>Строение должно быть в фокусе. </li>
								<li>Строение не должно перекрываться другими объектами (другими строениями, автомобилями и тд).</li>
								<li>Планируйте кадр так, что бы дом занимал в нем 3/4 объема по горизонтали. Дом не должен быть обрезанным на снимке. <br>Также он не должен быть слишком маленьким.</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
