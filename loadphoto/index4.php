<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<script type="text/javascript" scr="/bitrix/templates/.default/assets/js/loadphoto.js"></script>
<section class="content white" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side + my-margin clearfix Warranty">

					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<h1 class="text-uppercase text-center">
								<span>«Баня в подарок»</span>– выигрывайте вместе с Теремъ!
							</h1>
							
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<img src="1234.jpg" alt=""/>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<form id="load-image" action="/include/loadphoto.php" method="post" class="form-send" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-12 col-md-4 col-sm-4">
										<div class="form-group">
											<div class="row">
												<div class="col-xs-12">
													<label for="">Ваше ФИО</label>
												</div>
												<div class="col-xs-12">
													<input type="text" name="child-name" class="form-control my-input" placeholder="Ваше ФИО" required="">
												</div>
												<div class="col-xs-12">
													<div class="help-block with-errors"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-4 col-sm-4">
										<div class="form-group">
											<div class="row">
												<div class="col-xs-12">
													<label for="">Номер договора</label>
												</div>
												<div class="col-xs-12">
													<input type="text" name="parent" class="form-control my-input" placeholder="Номер договора" required="">
												</div>
												<div class="col-xs-12">
													<div class="help-block with-errors"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-4 col-sm-4">
										<div class="form-group">
											<div class="row">
												<div class="col-xs-12">
													<label for="">Строение</label>
												</div>
												<div class="col-xs-12">
													<input type="email" name="email" class="form-control my-input" placeholder="Строение" required="">
												</div>
												<div class="col-xs-12">
													<div class="help-block with-errors"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<div class="form-group">
											<div class="row">
												<div class="col-xs-12 col-md-4 col-sm-4">
													<div class="row">
														<div class="col-xs-12">
															<label for="">Телефон</label>
														</div>
														<div class="col-xs-12">
															<input type="number" name="child-age" class="form-control my-input" placeholder="Телефон" required="">
														</div>
														<div class="col-xs-12">
															<div class="help-block with-errors"></div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-md-5 col-sm-5">
													<div class="row">
														<div class="col-xs-12">
															<label for="">E-mail*</label>
														</div>
														<div class="col-xs-12">
															<input type="number" name="child-age" class="form-control my-input" placeholder="E-mail" required="">
														</div>
														<div class="col-xs-12">
															<div class="help-block with-errors"></div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-md-3 col-sm-3">
													<div class="row">
														<div class="col-xs-12">
															<label for="">&nbsp;&nbsp;</label>
														</div>
														<div class="col-xs-12">
															 <button id="load-image-button" type="submit" class="btn btn-danger text-uppercase" style="pointer-events: all; cursor: pointer; width:100%;">Отправить</button>
														</div>
														<div class="col-xs-12">
															<div class="help-block with-errors"></div>
														</div>
													</div>
												</div>
												<!-- <div class="col-xs-12 col-md-8 col-sm-8">
													<div id="images_preview" class="images-preview"></div>
												</div> -->
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-xs-12 col-md-6">
											<p style="padding-top: 3px;">* Отправляя заявку, Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">политикой конфиденциальности и правилами пользования сайтом</a> и даете <a href="/privacy-policy/" target="_blank">согласие  на использование загруженных изображений</a>.</p>
										</div>
										<div class="col-xs-12 col-md-6 text-right">
                                                                                    <span id="response-message" class="respond-message"></span>&nbsp;&nbsp;
                                                                                   
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content white " role="content" data-step="2">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side + my-margin clearfix Warranty">
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<h2 class="text-uppercase">условия участия</h2>
							
							<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>

							<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>

							<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
