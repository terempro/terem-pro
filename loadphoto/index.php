<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("КОНКУРС ДЕТСКОГО РИСУНКА – ВЫИГРЫВАЙТЕ ВМЕСТЕ С РЕБЕНКОМ ГИРОСКУТЕР!");
?>
<script type="text/javascript" scr="/bitrix/templates/.default/assets/js/loadphoto.js"></script>
<section class="content white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="contest">
                                <h1 class="text-uppercase text-center contest__title">
                                    Конкурс рисунка <span>«Дети рисуют дом мечты»</span> Выигрывай и получай призы!
                                </h1>
                                <div class="contest__status">
                                    <div class="rigth">
                                        <?php
                                        $dt = "2017-07-03 00:00:00";
                                        $s_dt = substr($dt, 0, 10);
                                        $num_days = ceil((strtotime($s_dt) - time()) / 86400);
                                        ?> <span class="label label-danger">Прием рисунков закончен</span>
                                        <!-- <div class="contest_timer">
                                            Осталось <?= $num_days ?> <?= declension_words($num_days, ["день", "дня", "дней"]) ?>
                                        </div> -->
                                    </div>
                                    <div class="left">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="contest__about">
                    <div class="contest__about--default" id="default-info">
                        <aside></aside>
                        <div class="about my-padding-contest">
                            <h2>
                                О КОНКУРСЕ </h2>
                            <p>
                                Приглашаем принять участие в конкурсе детского рисунка «Дети рисуют дом мечты».<br>
                                <br>
                                Большой дом с красной крышей, окна во всю стену, шторы в мелкий цветочек, надежная
                                дверь, а возле дома счастливая семья – мама, папа и я. Так каждый из нас рисовал дом
                                своей мечты. Мы решили посмотреть, о каком доме сейчас мечтают дети и провести
                                конкурс детского рисунка.<br>
                                <br>
                                Присылайте на конкурс рисунки ваших детей и выигрывайте призы от компании «Теремъ» и
                                официального дилера сигвеев по России и СНГ «Nanosegway».<br>
                                <br>
                                <a href="#" class="concurs_link modal-call" data-target="#concursPrize"
                                   data-toggle="modal">Призы конкурса</a> <a href="#"
                                   class="concurs_link modal-call"
                                   data-target="#concursRules"
                                   data-toggle="modal">Условия участия</a>
                                <a href="/pravila/" class="concurs_link" target="_blank">Правила конкурса</a>
                            </p>
                        </div>
                        <aside><img src="/upload/img/child1.jpg" alt=""></aside>
                    </div>
                </section>
                <div class=" clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <section class="contest__items-b">
                                <div class="contest__send-photo" style="background-color: #d1d1d1;">
                                    <div>
                                        <a href="" class="plus modal-call" disabled style="cursor: not-allowed">+</a>
                                        <p>
                                            Отправить фото <br>
                                            на конкурс
                                        </p>
                                        <button class="modal-call" data-target="#concursLoad" data-toggle="modal" disabled style="cursor: not-allowed">
                                            Отправить фото
                                        </button>
                                    </div>
                                </div>
                                <div class="contest__counter_block">
                                    <?php
                                    $arSelect = Array("ID");
                                    $arFilter = Array("IBLOCK_ID" => IntVal(35), "ACTIVE" => "Y");
                                    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 100), $arSelect);
                                    while ($ob = $res->GetNextElement()) {
                                        $arFields[] = $ob->GetFields();
                                    }
                                    ?>
                                    <p class="counter">
                                        <?= count($arFields) ?>
                                    </p>
                                    <p class="counter_text">
                                        <?= declension_words(count($arFields), ["работа", "работы", "работ"]) ?><br>
                                        участников
                                    </p>
                                </div>
                                <? $APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "concurs_list",
                                array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "N",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "N",
                                "CHECK_DATES" => "Y",
                                "COMPOSITE_FRAME_MODE" => "A",
                                "COMPOSITE_FRAME_TYPE" => "AUTO",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array(
                                0 => "NAME",
                                1 => "PREVIEW_PICTURE",
                                2 => "DETAIL_PICTURE",
                                3 => "",
                                ),
                                "FILTER_NAME" => "",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "35",
                                "IBLOCK_TYPE" => "councurs",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "200",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array(
                                0 => "EMAIL",
                                1 => "CHILD_AGE",
                                2 => "CHILD_NAME",
                                3 => "LIKES",
                                4 => "CHILD_PARENT",
                                5 => "",
                                ),
                                "SET_BROWSER_TITLE" => "Y",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "Y",
                                "SET_META_KEYWORDS" => "Y",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "Y",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "PROPERTY_LIKES",
                                "SORT_BY2" => "NAME",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "ASC",
                                "STRICT_SECTION_CHECK" => "N",
                                "COMPONENT_TEMPLATE" => "concurs_list"
                                ),
                                false
                                ); ?> </section>
                        </div>
                    </div>
                </div>
                <div class=" clearfix">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class=" padding-side clearfix">
                    <div class="bot_nav"><?php $APPLICATION->ShowViewContent('pager_concurs'); ?></div>
                </div>
            </div>
        </div>
    </div>
</section> <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
