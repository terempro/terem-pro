<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

define('CITIES_IBLOCK', 53); 

class City extends CBitrixComponent
{
    static public $city_paths = array();
    static public $city_list = array();
    static public $current_city = array();
    
    /*
     * Function DefineCurrentCity()
     * Определяет текущий город на основе данных сессии или через IP
     */
    
    static private function DefineCurrentCity()
    {
        global $CITIES_ARRAY;
        $city_name = 'Москва';
        
        if ($_SESSION['city'])
            $city_name = trim($_SESSION['city']);
        else
        {
            $SxGeo = new SxGeo();
            $city_name = $SxGeo->City();

            if (in_array($city_name, $CITIES_ARRAY))
                $_SESSION['city'] = $city_name;
        }
        
        $arFilter = ['IBLOCK_ID' => CITIES_IBLOCK, 'NAME' => $city_name];
            
        $arSelect = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'CODE',
            'PROPERTY_PATH',
            
            'PROPERTY_ADDRESS',
            'PROPERTY_EMAIL',
            'PROPERTY_PHONE',
            
            'PROPERTY_PRICES',
            'PROPERTY_NEWS',
            'PROPERTY_SALES',
            'PROPERTY_BANNERS_MAIN',
            'PROPERTY_SQUARE_SLIDER_MAIN',  
            
            'PROPERTY_NAME_PREPOSITIONAL_CASE'
        ];
        self::$current_city = CIBlockElement::GetList([], $arFilter, 0, 0, $arSelect)->GetNext();
        // Если не удалось определить город, то считаем текущим городом Москву



        if (!self::$current_city)
        {
            $arFilter['NAME'] = 'Москва';
            self::$current_city = CIBlockElement::GetList([], $arFilter, 0, 0, $arSelect)->GetNext();
        }
    }
    
    /*
     * Function DefineConstants()
     * Определяет константы, которые будут использоваться повсеместно на сайте
     */
    
    public function DefineConstants()
    {
        // Определяем данные города
        define('CITY_NAME', self::$current_city['NAME']);                                     // Название города
        define('CITY_CODE', self::$current_city['CODE']);                                     // Уникальный код
        define('CITY_PATH', self::$current_city['PROPERTY_PATH_VALUE']);                      // Путь относительно корня
        
        define('CITY_ADDRESS', self::$current_city['PROPERTY_ADDRESS_VALUE']);                // Адрес
        define('CITY_EMAIL', self::$current_city['PROPERTY_EMAIL_VALUE']);                    // Электронная почта
        define('CITY_PHONE', self::$current_city['PROPERTY_PHONE_VALUE']);                    // Телефон
        // Определяем используемые инфоблоки
        define('SERIES_IBLOCK', 13);                                                          // Серии домов
        define('VARIANTS_IBLOCK', 21);                                                        // Варианты домов
        define('PRICES_IBLOCK', self::$current_city["PROPERTY_PRICES_VALUE"]);                // Прайслист
        define('NEWS_IBLOCK', self::$current_city["PROPERTY_NEWS_VALUE"]);                    // Новости
        define('SALES_IBLOCK', self::$current_city["PROPERTY_SALES_VALUE"]);                  // Акции
        define('BANNERS_MAIN_IBLOCK', self::$current_city["PROPERTY_BANNERS_MAIN_VALUE"]);    // Баннеры для главной
        define('SQUARE_SLIDER_IBLOCK', self::$current_city["PROPERTY_SQUARE_SLIDER_VALUE"]);  // Кв. слайдер для главной
        // Определяем падежи имни города
        define('CITY_NAME6', self::$current_city["PROPERTY_NAME_PREPOSITIONAL_CASE_VALUE"]);  // Предложный падеж
    }
    
    /*
     * Function DefineUrl()
     * Определяет URL, на который надо перенаправить пользователя
     */
    
    static public function DefineUrl()
    {
        $path = $_SERVER['REQUEST_URI'];
    
        if (preg_match('/^\/(api|bitrix|include|upload|kitchen)/', $path))
            return;

        $cities_regexp = str_replace('||', '|', strtolower(implode('|', self::$city_paths)));
        $city_matches = array();

        if ($_SESSION['city'] && !preg_match('/^\/'.strtolower($_SESSION['city']).'/', $path)) 
        {
            if (preg_match('/('.$cities_regexp.')/', $path, $city_matches))
                $path = str_replace('/'.$city_matches[0], '', $path);
            
            header('location: //'.$_SERVER['HTTP_HOST'].'/'.strtolower($_SESSION['city']).$path);
        }
        elseif (!$_SESSION['city'] && preg_match('/('.$cities_regexp.')/', $path, $city_matches))
        {
            $path = str_replace('/'.$city_matches[0], '', $path);
            header('location: //'.$_SERVER['HTTP_HOST'].$path);
        }
    }
    
    /*
     * Function GetCommonData()
     * Создаёт общие списки городов
     */
    
    public function GetCommonData()
    {
        $arOrder = ['SORT' => 'asc'];
        $arFilter = ['IBLOCK_ID' => CITIES_IBLOCK];
        $arSelect = ['ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_PATH'];
        $cities = CIBlockElement::GetList($arOrder, $arFilter, 0, 0, $arSelect);
        
        while ($c = $cities->GetNext())
        {
            self::$city_list[$c["ID"]] = ['PATH' => $c["PROPERTY_PATH_VALUE"] ?: '', 'NAME' => $c["NAME"]];
            self::$city_paths[$c["ID"]] = $c["PROPERTY_PATH_VALUE"] ?: '';
        }
    }
    
    public function executeComponent() 
    {
        self::GetCommonData();
        self::DefineCurrentCity();
        self::DefineConstants();
        //self::DefineUrl();
    }
    
    static public function HandleCases(&$str)
    {
        $replaces = ['#CITY_NAME6#' => CITY_NAME6];
        $str = strtr($str, $replaces);
    }
    
    static public function IsMoscow()
    {
        return defined("CITY_CODE") && CITY_CODE == 'MOSCOW';
    }
}

