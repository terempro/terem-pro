<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<section class="content catalogue-body {{ filterStatus }}" ng-cloak>
    <!-- <a class="container"> -->

        <div id="ajax-catalog" class="{{ sectionsStatus }}" ng-cloak>
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12" id="category">
                    <div class="flex-reversed">
                        <?php $cnt = 0; ?>
                        <?php foreach ($arResult["SECTIONS"] as $arSection): ?>
                        <?php if($arSection["DISPLAY"]==1):?>
                            <?php if ($cnt == 0): ?>
                            <div class="row">
                            <?php endif; ?>
                            <div class="col-xs-12 col-md-6 col-sm-12">
                                <div class="my-promotion desgin-3 my-margin">
                                    <div>
                                        <a href="<?= $arSection["SECTION_PAGE_URL"] ?>filter/category-dacha">
                                            <img src="<?= $arSection['PICTURE']['SRC'] ?>" alt="<?= $arSection['NAME'] ?>">
                                        </a>
                                    </div>
                                    <div>
                                        <h4 class="text-uppercase"><?= $arSection['NAME'] ?></h4>
                                        <p><?= $arSection['LIST_DESCRIPTION'] ?></p>
                                    </div>
                                    <a class="all-item-link" href="<?= $arSection["SECTION_PAGE_URL"] ?>"></a>
                                </div>
                            </div>
                            <?php $cnt++; ?>
                            <?php if ($cnt == 2): ?>
                            </div>
                            <?php $cnt = 0; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php
            $region_act = explode('/', $_SERVER['REQUEST_URI']);
            $region     = $region_act[1];
            
            if($region == 'vladimir')
                echo Embed('catalog_text_vladimir');
            elseif($region != 'ryazan')
                echo Embed('catalog_text');

			$APPLICATION->IncludeComponent("bitrix:news.line", ".default", 
            	array(
            		"IBLOCK_TYPE" => "reclama",
            		"IBLOCKS" => array("83"),
            		"NEWS_COUNT" => "60",
            		"FIELD_CODE" => array(
            			0 => "ID",
            			1 => "NAME",
            			2 => "PREVIEW_TEXT",
            			3 => "PREVIEW_PICTURE",
            			4 => "PROPERTY_REGION",
            		),
            		"SORT_BY2" => "ACTIVE_FROM",
            		"SORT_ORDER2" => "DESC",
            		"SORT_BY1" => "SORT",
            		"SORT_ORDER1" => "ASC",
            		"DETAIL_URL" => "",
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"CACHE_TYPE" => "N",
            		"CACHE_TIME" => "300",
            		"CACHE_GROUPS" => "N",
            		"COMPONENT_TEMPLATE" => ".default"
            	), false
            );?>

        </div>


        <section class="houses {{ housesStatus }}" ng-cloak>
            <section class="sortby">
                <span>Сортировка:</span>
                <button class="sort-btn" data-order="up" ng-click="sortBy('real_price', $event)">По цене</button>
                <button class="sort-btn" data-order="up" ng-click="sortBy('total_square', $event)">По площади</button>
                <button class="sort-btn sort-btn__name" data-order="up" ng-click="sortBy('series', $event)">По названию</button>
            </section>

            <ul class="houses--list">
                <li class="houses--item-wrapper" ng-if="house.show" ng-repeat="house in selected">
                    <article class="house">
                            <section class="house--details">
                                <div>
                                    <div class="house--row">
                                        <p class="house--name">
                                            <a href="{{ house.link }}{{ getHouseTech(house) }}">{{ house.series }}</a>
                                        </p>
                                        <button class="free-advice"
                                            data-yagoal="CONSULT_CATALOG_CALLED"
                                            data-target="#formAdvice"
                                            data-code="{{ house.code }}"
                                            data-price="{{ house.real_price}}000"
                                            data-whatever="{{ house.name }}"
                                            data-toggle="modal">Бесплатная консультация
                                                <span><i class="fa fa-phone"></i></span>
                                        </button>

                                    </div>
                                    <section class="house--chars">
                                        <div ng-if="house.width > 0 && house.height > 0">{{ house.width }}&times;{{ house.height }}&nbsp;м</div>
                                        <div ng-if="house.total_square > 0">от {{ house.total_square }} м<sup>2</sup></div>
                                    </section>
                                </div>
                                <div>
                                    <section class="house--price">
                                        <span>от {{ displayPrice(house.real_price) }} 000&nbsp;р.</span>
                                        <span class="house--price__regular" ng-if="house.big_price > 0">{{ displayPrice(house.big_price) }} 000&nbsp;р.</span>
                                    </section>
                                    <a href="{{ house.link }}{{ getHouseTech(house) }}"class="house--more">
                                        Смотреть планировку
                                    </a>
                                </div>
                                <a class="details-bg-link" href="{{ house.link }}{{ getHouseTech(house) }}"></a>
                            </section>
                            <a href="{{ house.link }}{{ getHouseTech(house) }}" class="house--img" style="background-image: url('{{house.image}}')"></a>
                    </article>
                </li>
            </ul>
        </section>

    <!-- </div> -->
</section>
