<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);



?>
<section class="content content-back-side" data-parallax="up" data-opacity="true">
    <div class="container-fluid reset-padding">
        <div class="row reset-margin">
            <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
                <div class="back-img" style="background-image: url('/bitrix/templates/.default/assets/img/bg.jpg');"></div>
            </div>
        </div>
    </div>
</section>
<section class="content white">
    <section class="house-card">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="catalog-item-gallery white-holder my-margin row-eq-height row-eq-height--rwd1200">
                        <div class="col-lg-9 col-md-12 col-xs-12 col--no-gutter">
                            <div class="section-wrapper">
                                <?php $APPLICATION->IncludeComponent('terem:catalog.slider', '.default', $arResult['SLIDER_DATA']) ?>
                                <?php $APPLICATION->IncludeComponent('terem:catalog.packages', '.default', $arResult['PACKAGES_DATA']) ?>
                                <?php if ($arResult["SHOW_VIDEO"]): ?>
                                <?php $APPLICATION->IncludeComponent('terem:catalog.video', '.default', $arResult['VIDEO_DATA']) ?>
                                <?php endif; ?>
                                <?php $APPLICATION->IncludeComponent('terem:catalog.feedback', '.default', $arResult['VIDEO_DATA']) ?>
                                <?php $APPLICATION->IncludeComponent('terem:catalog.plans', '.default', $arResult['PLANS_DATA']) ?>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 col-xs-12 col--no-gutter">
                            <div class="control-panel">
                                <div class="control-panel__fixed-top" itemscope itemtype="http://schema.org/Product">
                                    <div class="house-name">
                                        <span class="house-area"><?=$arResult['SECTION']['NAME'] ?></span><br />
                                        <span class="house-area">
                                            <?= $arResult['PROPERTIES']['SIZER']['VALUE'] ?> м; от <?= $arResult['TOTAL_SQUARE'] ?> кв.м
                                        </span>
                                        <h1 class="house-title" itemprop="name"><?= $arResult['NAME'] ?></h1>
                                        <span class="hidden" itemprop="description"><?= $arResult['META_TAGS']['DESCRIPTION']?></span>
                                    </div>
                                    <span class="my-hr"></span>
                                    <span class="choose-tech__span">Выберите технологию:</span>
                                    <?php $APPLICATION->IncludeComponent('terem:catalog.tabs', '.default', $arResult['PACKAGES_DATA']) ?>
                                    <?php $APPLICATION->IncludeComponent('terem:catalog.rate', '.default', ['URL' => $arResult['DETAIL_PAGE_URL']]) ?>
                                </div>
                                <div class="control-panel__fixed-middle">
                                    <div class="nav-b">
                                        <ul>
                                            <li class="active"><a class=" nav-b__link" data-href="house-slider-tab" href="">Фото и планировки</a></li>
                                            <?php if ($arResult['SHOW_PLANS']): ?>
                                            <li><a class="nav-b__link" data-href="house-slider-tab2" href="">Варианты планировок</a></li>
                                            <?php endif; ?>
                                            <li><a class="nav-b__link complectation" data-href="package-tab" href="" data-house="<?= $arResult['ID'] ?>" data-karkas="<?= $arResult['CHEAPEST_KARKAS'] ?>" data-kirpich="<?= $arResult['CHEAPEST_KIRPICH'] ?>" data-brus="<?= $arResult['CHEAPEST_BRUS'] ?>" >Комплектация</a></li>
                                            <?php if ($arResult["SHOW_VIDEO"]): ?>
                                            <li><a class="nav-b__link" data-href="video-tab" href="">Видео</a></li>
                                            <?php endif; ?>
                                            <li><a class="nav-b__link" data-href="feedback-tab" href="">Отзывы</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="control-panel__fixed-bottom">
                                    <div class="cta-b">
                                        <?php/* if ($arResult['SECTION']['ID'] !== '28731'): ?>
                                            <button type="button" class="cta-promo" data-toggle="modal" data-target="#formGetCode">
                                                Фундамент<br>в подарок
                                            </button>
                                        <?php endif */?>
                                        <?if($_SESSION['city'] != 'Новосибирск'):?>
                                            <button data-yagoal="CONSULT_HOUSE_CARD_CALLED" type="button" class="cta-btn" data-toggle="modal" data-target="#formAdviceItem" data-code="<?= $arResult["CODE_1C"] ?>" data-price="" data-house="<?= $arResult['NAME'] ?>">
                                                Заказать консультацию
                                            </button>
                                        <?endif;?>
                                    </div>
                                    <div class="info-bottom">
                                        <div class="chosen-house">
                                            <span>
                                                Вы смотрите: <span><?= $arResult['NAME'] ?></span> <span id="chosen-house-tech">каркас</span>
                                            </span>
                                        </div>
                                        <div class="back-to-catalog">
                                            <a href="<?= $arResult['BACK_URL'] ?>">Вернуться в каталог</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>