<?php

// Получаем UF_свойства

$arOrder = [];
$arFilter = ['IBLOCK_ID' => SERIES_IBLOCK, 'ID' => $arResult['ID']];
$arSelect = ['ID', 'UF_*'];
$arResult += CIBlockSection::GetList($arOrder, $arFilter, 0, $arSelect)->GetNext();

City::HandleCases($arResult['UF_TITLE']);
City::HandleCases($arResult['UF_DESCRIPTION']);

$arResult['SHOW_DESCRIPTION'] = City::IsMoscow();
