<section class="house-slider-tab slider-previews-ajx2" id="house-slider-tab2" >
    <ul class="slider-wrapper" data-item="slider-item-catalog" data-type="photo-slider">
        <?php foreach ($arParams['PLANS'] as $i => $p): ?>
        <li class="owl-item" id="layout_<?= $i ?>-slide">
            <img src="<?= $p['IMAGE'] ?>">
            <?php if ($arParams['SHOW_INFO_ON_PHOTO']): ?>
            <div class="caption">
                <?= $p["DESCRIPTION"] ?>
                <!-- Варианты пристроек к этому дому в разделе <a href="<?= $arParams['LINK_TO_CALC'] ?>">калькулятор доп.услуг</a> -->
            </div>
            <?php endif; ?>
        </li> 
        <?php endforeach; ?>
    </ul>
    <div class="slider-previews">
        <ul class="slider-previews__list" data-item="slider-catalog-side" data-type="photo-slider-side">
            <?php foreach ($arParams['PLANS'] as $i => $p): ?>
            <li data-link="layout_<?= $i ?>-slide" class="layout_<?= 1+$i%2 ?>-holder" ><img src="<?= $p["PREVIEW"] ?>"></li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

