<?php
// use Bitrix\Main\Page\Asset;
// Asset::getInstance()->addString('<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>');
//Asset::getInstance()->addCss("/bitrix/templates/.default/assets/css/angular/font-awesome.min.css");
//Asset::getInstance()->addCss("/bitrix/templates/.default/assets/css/angular/nouislider.min.css");
//Asset::getInstance()->addCss("/bitrix/templates/.default/assets/css/angular/main.min.css");
?>

<script src="/bitrix/templates/.default/assets/js/angular/rzslider.min.js"></script>
<!-- <script src="/bitrix/templates/.default/assets/js/angular/angular.rangeSlider.js"></script> -->
<link href="/bitrix/templates/.default/assets/css/angular/main.min.css" type="text/css"  rel="stylesheet">
<link href="/bitrix/templates/.default/assets/css/angular/font-awesome.min.css" type="text/css"  rel="stylesheet">
<link href="/bitrix/templates/.default/assets/css/angular/rzslider.min.css" type="text/css"  rel="stylesheet">
<!-- <link href="/bitrix/templates/.default/assets/css/angular/angular.rangeSlider.css" type="text/css"  rel="stylesheet"> -->


<div class="myloader {{ loaderStatus }}" data-test="test"></div>

<div class="filter--container content {{ filterStatus }}" id="filter" ng-cloak>
    <p class="hidden-xs" style="font-size: 36px; margin-bottom: 30px; margin-top: 0; text-align: center; font-weight: 100; font-family: Roboto,Helvetica,Arial,sans-serif;">Выберите параметры дома, который нужен вам</p>
    <?php
    $filter = new \Bitrix\Main\Page\FrameBuffered("filter");
    $filter->begin();
    ?>
    <ul class="filter hidden-xs" data-catalogue="/include/data/<?= $arResult['DATA'] ?>" data-initial="<?= $arResult["FILTER"] ?>">
        <li class="filter--dropdown"
            ng-repeat="parameter in parameters | orderBy: 'order'" resfresh-slider
            ng-if="!parameter.hidden">
            <button class="filter--dropdown-item par-key" ng-click="showMenu($event)"
                ng-if="!chosenInCategory(parameter)">
                <span class="parameter" ng-if="!chosenInCategory(parameter)"
                    ng-bind="parameter.keyCyr">
                </span>
            </button>
            <button ng-if="chosenInCategory(parameter) && parameter.type === 'other'"
                ng-click="removeParam(parameter.key)"
                class="filter--dropdown-item par-key">
                <span class="parameter selected-parameter remove"
                    ng-bind="getValueToDisplay(parameter)">
                    <!-- {{ getValueToDisplay(parameter) }} -->
                    <span ng-if="parameter.key === 'floors' && getValueToDisplay(parameter) === 1">этаж</span>
                    <span ng-if="parameter.key === 'floors' && getValueToDisplay(parameter) > 1 && getValueToDisplay(parameter) < 5">
                        этажа</span>
                </span>
            </button>
            <button ng-if="chosenInCategory(parameter) && parameter.type !== 'other'"
                ng-click="showMenu($event)"
                class="filter--dropdown-item par-key">
                <span class="parameter selected-parameter" ng-if="parameter.type === 'range'">
                    <span ng-if="parameter.key !== 'big_price' && parameter.key !== 'real_price'">
                        <span ng-bind="parameter.min"></span>&ndash;<span ng-bind="parameter.max"></span>
                        <!-- {{ parameter.min }}&ndash;{{ parameter.max }} -->
                    </span>
                    <span ng-if="parameter.key === 'big_price' || parameter.key === 'real_price'">
                        <span ng-bind="displayPrice(parameter.min)"></span>&ndash;<span ng-bind="displayPrice(parameter.max)"></span>&nbsp;тыс.&nbsp;р.
                        <!-- {{ displayPrice(parameter.min) }}&ndash;{{ displayPrice(parameter.max) }}&nbsp;тыс.&nbsp;р. -->
                    </span>
                    <span ng-if="parameter.key === 'total_square'">
                        &nbsp;м<sup>2</sup>
                    </span>
                </span>
                <span class="parameter selected-parameter" ng-if="parameter.type === 'size'">
                    <span ng-bind="parameter.smallest.width"></span>&times;<span ng-bind="parameter.smallest.length"></span>&ndash;<span ng-bind="parameter.biggest.width"></span>&times;<span ng-bind="parameter.biggest.length"></span>
                    <!-- {{ parameter.smallest.width}}&times;{{ parameter.smallest.length}}&ndash;{{ parameter.biggest.width}}&times;{{ parameter.biggest.length}} -->
                </span>
            </button>

            <ul class="filter--dropdown-menu inactive"
                ng-if="(parameter.type === 'other'  && parameter.key === 'categories')">
                <li class="filter--dropdown-menu-item"
					ng-if="value == 'Бани'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li>
                <li class="filter--dropdown-menu-item"
					ng-if="value == 'Дачные дома'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li>
                <li class="filter--dropdown-menu-item"
					ng-if="value == 'Коттеджи'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li><li class="filter--dropdown-menu-item"
					ng-if="value == 'Коттеджи от 170 кв. м'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li><li class="filter--dropdown-menu-item"
					ng-if="value == 'Малые строения, беседки'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li><li class="filter--dropdown-menu-item"
					ng-if="value == 'Садовые дома'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li>
            </ul>			
			
            <ul class="filter--dropdown-menu inactive"
                ng-if="(parameter.type === 'other' && parameter.values.length < 12)">
                <li class="filter--dropdown-menu-item"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
                    <span ng-bind="value"></span>
                    <span ng-if="parameter.key === 'floors' && value == 1">
                        &nbsp;этаж
                    </span>
                    <span ng-if="parameter.key === 'floors' && value > 1 && value < 5">
                        &nbsp;этажа
                    </span>		
                </li>
            </ul>

            <ul class="filter--dropdown-menu inactive filter--dropdown-menu__columns"
                ng-if="(parameter.type === 'other' && parameter.values.length > 12)">
                <li class="filter--dropdown-menu-item"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
                    <span ng-bind="value"></span>
                    <span
                        ng-if="parameter.key === 'floors' && value == 1">
                        &nbsp;этаж
                    </span>
                    <span ng-if="parameter.key === 'floors' && value > 1 && value < 5">
                        &nbsp;этажа
                    </span>
                </li>
            </ul>

            <form action=""
                class="filter--dropdown-menu filter--form inactive"
                ng-if="parameter.type === 'range'">

                <div class="filter--form-row">
                    <div class="form-inputs">
                        <!-- <div class="input-wrapper">
                            <label class="extra" for="{{ parameter.key }}-min">от</label>
                            <input
                                type="number"
                                step="any"
                                ng-init="min = getRangeLimits(current, 'key', parameter.key).min"
                                ng-model="min"
                                value=""
                                min="{{ getRangeLimits(current, 'key', parameter.key).min }}"
                                max="{{ getTerminal(max, getRangeLimits(current, 'key', parameter.key).max) }}"
                                id="{{ parameter.key }}-min"
                            >
                        </div>

                        <div class="input-wrapper">
                            <label class="extra" for="{{ parameter.key }}-max">до</label>
                            <input
                                type="number"
                                step="any"
                                ng-init="max = getRangeLimits(current, 'key', parameter.key).max"
                                ng-model="max"
                                value=""
                                max="{{ getRangeLimits(current, 'key', parameter.key).max }}"
                                min="{{ getTerminal(min, getRangeLimits(current, 'key', parameter.key).min) }}"
                                id="{{ parameter.key }}-max"
                            >
                        </div> -->

                        <div class="input-wrapper">
                            <label class="extra" for="{{ parameter.key }}-min">от</label>
                            <input type="number" step="any"
                                ng-init="min = rangeLimits[parameter.key].min"
                                ng-model="min" value=""
                                min="rangeLimits[parameter.key].min"
                                max="{{ getTerminal(max, rangeLimits[parameter.key].max) }}"
                                id="{{ parameter.key }}-min">
                        </div>

                        <div class="input-wrapper">
                            <label class="extra" for="{{ parameter.key }}-max">до</label>
                            <input type="number" step="any"
                                ng-init="max = rangeLimits[parameter.key].max"
                                ng-model="max" value=""
                                min="{{ getTerminal(min, rangeLimits[parameter.key].min) }}"
                                max="rangeLimits[parameter.key].max"
                                id="{{ parameter.key }}-max">
                        </div>
                        <div class="unit" ng-if="parameter.key === 'real_price'">тыс.&nbsp;р.</div>
                        <div class="unit" ng-if="parameter.key === 'total_square'">м<sup>2</sup></div>
                    </div>
                </div>

                <div class="filter--form-row filter--slider">
                    <rzslider rz-slider-model="min" rz-slider-high="max"
                        rz-slider-options="getSliderOptions(parameter)">
                    </rzslider>
                </div>

                <div class="filter--form-row">
                    <button type="submit"
                        ng-click="showByRange(parameter.key, min, max, parameter.min, parameter.max)">
                        Показать
                    </button>
                </div>
            </form>

            <form action=""
                class="filter--dropdown-menu filter--form filter--form__size inactive"
                ng-if="parameter.type === 'size'">
                <div class="filter--form-row size">
                    <div class="input-wrapper">
                        <span class="extra">от</span>
                        <input type="number" step="any" value="{{ minX }}"
                            ng-init="minX = rangeLimits[parameter.key].smallest.width"
                            ng-model="minX" min="{{ rangeLimits[parameter.key].min }}"
                            max="{{ getTerminal(maxX, rangeLimits[parameter.key].biggest.width) }}">

                        <span class="extra times">&times;</span>

                        <input type="number" step="any" value="{{ minY }}"
                            ng-init="minY = rangeLimits[parameter.key].smallest.length"
                            ng-model="minY" min="{{ rangeLimits[parameter.key].min }}"
                            max="{{ getTerminal(maxY, rangeLimits[parameter.key].biggest.length) }}">
                        <span class="unit">м</span>
                    </div>
                </div>
                <div class="filter--form-row size">
                    <div class="input-wrapper">
                        <span class="extra">до</span>
                        <input type="number" step="any" value="{{ maxX }}"
                            ng-init="maxX = rangeLimits[parameter.key].biggest.width"
                            ng-model="maxX" min="{{ getTerminal(minX, rangeLimits[parameter.key].smallest.width) }}"
                            max="{{ rangeLimits[parameter.key].max }}">
                        <span class="extra times">&times;</span>
                        <input type="number" step="any" value="{{ maxY }}"
                            ng-init="maxY = rangeLimits[parameter.key].biggest.length"
                            ng-model="maxY" min="{{ getTerminal(minY, rangeLimits[parameter.key].smallest.length) }}"
                            max="{{ rangeLimits[parameter.key].max }}">
                        <span class="unit">м</span>
                    </div>
                </div>
                <button type="submit" ng-click="showBySize(minX, minY, maxX, maxY, parameter.min, parameter.max, $event)">
                    Показать
                </button>
            </form>

        </li>
    </ul>
    <div class="row filter visible-xs" data-catalogue="/include/data/<?= $arResult['DATA'] ?>" data-initial="<?= $arResult["FILTER"] ?>">
    <div class="col-xs-12 col-md-12 col-sm-12">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
            <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#take01" aria-expanded="false" aria-controls="take01">
                <div class="my-table">
                    <div>
                        <p class="text-uppercase bold">Подобрать по параметрам</p>
                    </div>
                    <div>
                        <i class="fa fa-sliders"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
            <div class="collapse my-collapse" id="take01">
                <div class="( my-collapse-content + design-main )">
                <li class="filter--dropdown"
            ng-repeat="parameter in parameters | orderBy: 'order'" resfresh-slider
            ng-if="!parameter.hidden">
            <button class="filter--dropdown-item par-key" ng-click="showMenu($event)"
                ng-if="!chosenInCategory(parameter)">
                <span class="parameter" ng-if="!chosenInCategory(parameter)"
                    ng-bind="parameter.keyCyr">
                </span>
            </button>
            <button ng-if="chosenInCategory(parameter) && parameter.type === 'other'"
                ng-click="removeParam(parameter.key)"
                class="filter--dropdown-item par-key">
                <span class="parameter selected-parameter remove"
                    ng-bind="getValueToDisplay(parameter)">
                    <!-- {{ getValueToDisplay(parameter) }} -->
                    <span ng-if="parameter.key === 'floors' && getValueToDisplay(parameter) === 1">этаж</span>
                    <span ng-if="parameter.key === 'floors' && getValueToDisplay(parameter) > 1 && getValueToDisplay(parameter) < 5">
                        этажа</span>
                </span>
            </button>
            <button ng-if="chosenInCategory(parameter) && parameter.type !== 'other'"
                ng-click="showMenu($event)"
                class="filter--dropdown-item par-key">
                <span class="parameter selected-parameter" ng-if="parameter.type === 'range'">
                    <span ng-if="parameter.key !== 'big_price' && parameter.key !== 'real_price'">
                        <span ng-bind="parameter.min"></span>&ndash;<span ng-bind="parameter.max"></span>
                        <!-- {{ parameter.min }}&ndash;{{ parameter.max }} -->
                    </span>
                    <span ng-if="parameter.key === 'big_price' || parameter.key === 'real_price'">
                        <span ng-bind="displayPrice(parameter.min)"></span>&ndash;<span ng-bind="displayPrice(parameter.max)"></span>&nbsp;тыс.&nbsp;р.
                        <!-- {{ displayPrice(parameter.min) }}&ndash;{{ displayPrice(parameter.max) }}&nbsp;тыс.&nbsp;р. -->
                    </span>
                    <span ng-if="parameter.key === 'total_square'">
                        &nbsp;м<sup>2</sup>
                    </span>
                </span>
                <span class="parameter selected-parameter" ng-if="parameter.type === 'size'">
                    <span ng-bind="parameter.smallest.width"></span>&times;<span ng-bind="parameter.smallest.length"></span>&ndash;<span ng-bind="parameter.biggest.width"></span>&times;<span ng-bind="parameter.biggest.length"></span>
                    <!-- {{ parameter.smallest.width}}&times;{{ parameter.smallest.length}}&ndash;{{ parameter.biggest.width}}&times;{{ parameter.biggest.length}} -->
                </span>
            </button>

            <ul class="filter--dropdown-menu inactive"
                ng-if="(parameter.type === 'other'  && parameter.key === 'categories')">
                <li class="filter--dropdown-menu-item"
					ng-if="value == 'Бани'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li>
                <li class="filter--dropdown-menu-item"
					ng-if="value == 'Дачные дома'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li>
                <li class="filter--dropdown-menu-item"
					ng-if="value == 'Коттеджи'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li><li class="filter--dropdown-menu-item"
					ng-if="value == 'Коттеджи от 170 кв. м'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li><li class="filter--dropdown-menu-item"
					ng-if="value == 'Малые строения, беседки'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li><li class="filter--dropdown-menu-item"
					ng-if="value == 'Садовые дома'"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
						<span ng-bind="value"></span>
                </li>
            </ul>			
			
            <ul class="filter--dropdown-menu inactive"
                ng-if="(parameter.type === 'other' && parameter.values.length < 12)">
                <li class="filter--dropdown-menu-item"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
                    <span ng-bind="value"></span>
                    <span ng-if="parameter.key === 'floors' && value == 1">
                        &nbsp;этаж
                    </span>
                    <span ng-if="parameter.key === 'floors' && value > 1 && value < 5">
                        &nbsp;этажа
                    </span>		
                </li>
            </ul>

            <ul class="filter--dropdown-menu inactive filter--dropdown-menu__columns"
                ng-if="(parameter.type === 'other' && parameter.values.length > 12)">
                <li class="filter--dropdown-menu-item"
                    ng-repeat="value in parameter.values | orderBy"
                    ng-click="showByParam(parameter.key, value, parameter.type)">
                    <!-- {{ value }} -->
                    <span ng-bind="value"></span>
                    <span
                        ng-if="parameter.key === 'floors' && value == 1">
                        &nbsp;этаж
                    </span>
                    <span ng-if="parameter.key === 'floors' && value > 1 && value < 5">
                        &nbsp;этажа
                    </span>
                </li>
            </ul>

            <form action=""
                class="filter--dropdown-menu filter--form inactive"
                ng-if="parameter.type === 'range'">

                <div class="filter--form-row">
                    <div class="form-inputs">
                        <!-- <div class="input-wrapper">
                            <label class="extra" for="{{ parameter.key }}-min">от</label>
                            <input
                                type="number"
                                step="any"
                                ng-init="min = getRangeLimits(current, 'key', parameter.key).min"
                                ng-model="min"
                                value=""
                                min="{{ getRangeLimits(current, 'key', parameter.key).min }}"
                                max="{{ getTerminal(max, getRangeLimits(current, 'key', parameter.key).max) }}"
                                id="{{ parameter.key }}-min"
                            >
                        </div>

                        <div class="input-wrapper">
                            <label class="extra" for="{{ parameter.key }}-max">до</label>
                            <input
                                type="number"
                                step="any"
                                ng-init="max = getRangeLimits(current, 'key', parameter.key).max"
                                ng-model="max"
                                value=""
                                max="{{ getRangeLimits(current, 'key', parameter.key).max }}"
                                min="{{ getTerminal(min, getRangeLimits(current, 'key', parameter.key).min) }}"
                                id="{{ parameter.key }}-max"
                            >
                        </div> -->

                        <div class="input-wrapper">
                            <label class="extra" for="{{ parameter.key }}-min">от</label>
                            <input type="number" step="any"
                                ng-init="min = rangeLimits[parameter.key].min"
                                ng-model="min" value=""
                                min="rangeLimits[parameter.key].min"
                                max="{{ getTerminal(max, rangeLimits[parameter.key].max) }}"
                                id="{{ parameter.key }}-min">
                        </div>

                        <div class="input-wrapper">
                            <label class="extra" for="{{ parameter.key }}-max">до</label>
                            <input type="number" step="any"
                                ng-init="max = rangeLimits[parameter.key].max"
                                ng-model="max" value=""
                                min="{{ getTerminal(min, rangeLimits[parameter.key].min) }}"
                                max="rangeLimits[parameter.key].max"
                                id="{{ parameter.key }}-max">
                        </div>
                        <div class="unit" ng-if="parameter.key === 'real_price'">тыс.&nbsp;р.</div>
                        <div class="unit" ng-if="parameter.key === 'total_square'">м<sup>2</sup></div>
                    </div>
                </div>

                <div class="filter--form-row filter--slider">
                    <rzslider rz-slider-model="min" rz-slider-high="max"
                        rz-slider-options="getSliderOptions(parameter)">
                    </rzslider>
                </div>

                <div class="filter--form-row">
                    <button type="submit"
                        ng-click="showByRange(parameter.key, min, max, parameter.min, parameter.max)">
                        Показать
                    </button>
                </div>
            </form>

            <form action=""
                class="filter--dropdown-menu filter--form filter--form__size inactive"
                ng-if="parameter.type === 'size'">
                <div class="filter--form-row size">
                    <div class="input-wrapper">
                        <span class="extra">от</span>
                        <input type="number" step="any" value="{{ minX }}"
                            ng-init="minX = rangeLimits[parameter.key].smallest.width"
                            ng-model="minX" min="{{ rangeLimits[parameter.key].min }}"
                            max="{{ getTerminal(maxX, rangeLimits[parameter.key].biggest.width) }}">

                        <span class="extra times">&times;</span>

                        <input type="number" step="any" value="{{ minY }}"
                            ng-init="minY = rangeLimits[parameter.key].smallest.length"
                            ng-model="minY" min="{{ rangeLimits[parameter.key].min }}"
                            max="{{ getTerminal(maxY, rangeLimits[parameter.key].biggest.length) }}">
                        <span class="unit">м</span>
                    </div>
                </div>
                <div class="filter--form-row size">
                    <div class="input-wrapper">
                        <span class="extra">до</span>
                        <input type="number" step="any" value="{{ maxX }}"
                            ng-init="maxX = rangeLimits[parameter.key].biggest.width"
                            ng-model="maxX" min="{{ getTerminal(minX, rangeLimits[parameter.key].smallest.width) }}"
                            max="{{ rangeLimits[parameter.key].max }}">
                        <span class="extra times">&times;</span>
                        <input type="number" step="any" value="{{ maxY }}"
                            ng-init="maxY = rangeLimits[parameter.key].biggest.length"
                            ng-model="maxY" min="{{ getTerminal(minY, rangeLimits[parameter.key].smallest.length) }}"
                            max="{{ rangeLimits[parameter.key].max }}">
                        <span class="unit">м</span>
                    </div>
                </div>
                <button type="submit" ng-click="showBySize(minX, minY, maxX, maxY, parameter.min, parameter.max, $event)">
                    Показать
                </button>
            </form>

        </li>
               <section class="parameters">
        <!-- <h2>Выбранные параметры</h2> -->
        <p class="variants-chosen" ng-if="current.length > 0">
            Вы выбрали <span ng-bind="showing().count"></span> <span ng-bind="showing().grammar"></span>
            <!-- Вы выбрали {{ showing().count }} {{ showing().grammar }}: -->
        </p>
        <ul class="chosen">

            <li class="chosen--val" ng-if="parameter.type === 'other' || parameter.type === 'season'"
                ng-repeat="parameter in current" ng-click="removeParam(parameter.key, parameter.value)"
                ng-bind="parameter.value">
                <!-- {{ parameter.value }} -->
                <span ng-if="parameter.key === 'floors' && parameter.value == 1">этаж</span>
                <span ng-if="parameter.key === 'floors' && parameter.value > 1 && parameter.value < 5">этажа</span>
            </li>
            <li class="chosen--val" ng-if="parameter.type === 'range'" ng-repeat="parameter in current"
                ng-click="removeParam(parameter.key)">
                <span ng-if="parameter.key !== 'big_price' && parameter.key !== 'real_price'">
                    От <span ng-bind="parameter.min"></span> до <span ng-bind="parameter.max"></span>
                    <!-- От {{ parameter.min }} до {{ parameter.max }} -->
                </span>
                <span ng-if="parameter.key === 'big_price' || parameter.key === 'real_price'">
                    От <span ng-bind="displayPrice(parameter.min)"></span> до <span ng-bind="displayPrice(parameter.max)"></span>
                    <!-- От {{ displayPrice(parameter.min) }} 000 до {{ displayPrice(parameter.max) }} 000 -->
                </span>
                <span ng-if="parameter.key === 'big_price' || parameter.key === 'real_price'">р.</span>
                <span ng-if="parameter.key === 'total_square'"> м<sup>2</sup></span>
            </li>
            <li class="chosen--val" ng-if="parameter.type === 'size'" ng-repeat="parameter in current" ng-click="removeParam(parameter.key)">
                <span>
                    От <span ng-bind="parameter.minBigSide"></span>&times;<span ng-bind="parameter.minSmallSide"></span>&nbsp;м до <span ng-bind="parameter.maxBigSide"></span>&times;<span ng-bind="parameter.maxSmallSide"></span>&nbsp;м
                    <!-- От {{ parameter.minBigSide }}&times;{{ parameter.minSmallSide }}&nbsp;м до {{ parameter.maxBigSide }}&times;{{ parameter.maxSmallSide }}&nbsp;м -->
                </span>
            </li>
        </ul>

        <button class="rm-all" ng-click="reset()" ng-if="current.length > 0">
            Cбросить все фильтры
        </button>
    </section>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
    <?php $filter->end(); ?>
    <section class="parameters hidden-xs">
        <!-- <h2>Выбранные параметры</h2> -->
        <p class="variants-chosen" ng-if="current.length > 0">
            Вы выбрали <span ng-bind="showing().count"></span> <span ng-bind="showing().grammar"></span>
            <!-- Вы выбрали {{ showing().count }} {{ showing().grammar }}: -->
        </p>
        <ul class="chosen">

            <li class="chosen--val" ng-if="parameter.type === 'other' || parameter.type === 'season'"
                ng-repeat="parameter in current" ng-click="removeParam(parameter.key, parameter.value)"
                ng-bind="parameter.value">
                <!-- {{ parameter.value }} -->
                <span ng-if="parameter.key === 'floors' && parameter.value == 1">этаж</span>
                <span ng-if="parameter.key === 'floors' && parameter.value > 1 && parameter.value < 5">этажа</span>
            </li>
            <li class="chosen--val" ng-if="parameter.type === 'range'" ng-repeat="parameter in current"
                ng-click="removeParam(parameter.key)">
                <span ng-if="parameter.key !== 'big_price' && parameter.key !== 'real_price'">
                    От <span ng-bind="parameter.min"></span> до <span ng-bind="parameter.max"></span>
                    <!-- От {{ parameter.min }} до {{ parameter.max }} -->
                </span>
                <span ng-if="parameter.key === 'big_price' || parameter.key === 'real_price'">
                    От <span ng-bind="displayPrice(parameter.min)"></span> до <span ng-bind="displayPrice(parameter.max)"></span>
                    <!-- От {{ displayPrice(parameter.min) }} 000 до {{ displayPrice(parameter.max) }} 000 -->
                </span>
                <span ng-if="parameter.key === 'big_price' || parameter.key === 'real_price'">р.</span>
                <span ng-if="parameter.key === 'total_square'"> м<sup>2</sup></span>
            </li>
            <li class="chosen--val" ng-if="parameter.type === 'size'" ng-repeat="parameter in current" ng-click="removeParam(parameter.key)">
                <span>
                    От <span ng-bind="parameter.minBigSide"></span>&times;<span ng-bind="parameter.minSmallSide"></span>&nbsp;м до <span ng-bind="parameter.maxBigSide"></span>&times;<span ng-bind="parameter.maxSmallSide"></span>&nbsp;м
                    <!-- От {{ parameter.minBigSide }}&times;{{ parameter.minSmallSide }}&nbsp;м до {{ parameter.maxBigSide }}&times;{{ parameter.maxSmallSide }}&nbsp;м -->
                </span>
            </li>
        </ul>

        <button class="rm-all" ng-click="reset()" ng-if="current.length > 0">
            Cбросить все фильтры
        </button>
    </section>
    <section class="question">
        Наши специалисты ответят на ваши вопросы по телефону 
        <?php
        $getCurDir = $APPLICATION->GetCurDir();
        //телефоны городов
        if(strpos($getCurDir, 'izhevsk/'))
        { ?>
            <a href="tel:+73412650623">+7 3412 65-06-23</a>
            <?php
        }
        elseif(strpos($getCurDir, 'novosibirsk/'))
        { ?>
            <a href="tel:+73833121201">+7 383 312-12-01</a>
            <?php
        }
        elseif(strpos($getCurDir, 'saratov/'))
        { ?>
            <a href="tel:+78452338144">+7 8452 33-81-44</a>
            <?php
        }
        elseif(strpos($getCurDir, 'bryansk/'))
        { ?>
            <a href="tel:+74832321381">+7 4832 321-381</a>
            <?php
        }
        elseif(strpos($getCurDir, 'tula/'))
        { ?>
            <a href="tel:+74872751446">+7 4872 751-446</a>
            <?php
        }
        elseif(strpos($getCurDir, 'vologda/'))
        { ?>
            <a href="tel:+78172578245">+7 8172 57-82-45</a>
            <?php
        }
        elseif(strpos($getCurDir, 'kazan/'))
        { ?>
            <a href="tel:+78432070348">+7 843 20-70-348</a>
            <?php
        }
        elseif(strpos($getCurDir, 'ryazan/'))
        { ?>
            <a href="tel:+74912575124">+7 (4912) 575-124</a>
            <?php
        }
        elseif(strpos($getCurDir, 'krasnodar/'))
        { ?>
            <a href="tel:+78432070348">+7 (861) 217-75-27</a>
            <?php
        }
        elseif(strpos($getCurDir, 'vladimir/'))
        { ?>
            <a href="tel:+78432070348">+7 (4922) 49-42-24</a>
            <?php
        }
        elseif(strpos($getCurDir, 'yaroslavl/'))
		{ ?>
            <a href="tel:+74852607229">+7 (485) 260-72-29</a>
			<?php
		}
        else
        { ?>
            <a href="tel:+74954191414">+7 495 461-01-10</a>
            <?php
         } ?>
         или <a href="#" data-target="#call" data-toggle="modal">перезвонят вам через 30&nbsp;секунд</a>
    </section>

    <!-- <button ng-click="sortByPrice()">По цене</button>
    <button ng-click="sortByArea()">По площади</button> -->
</div>
