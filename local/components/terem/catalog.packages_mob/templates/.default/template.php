<section class="package-tab" id="package-tab-mob">
    <?php if ($arParams['SHOW_KARKAS']): ?>
    <div class="package-tab__inner <?= $arParams["KARKAS_ACTIVITY"] ?>" data-item="package-tab-karkas" id="karkas-complect-mob"></div>
    <?php endif; ?>
    <?php if ($arParams['SHOW_BRUS']): ?>
    <div class="package-tab__inner <?= $arParams["BRUS_ACTIVITY"] ?>" data-item="package-tab-brus" id="brus-complect-mob"></div>
    <?php endif; ?>
    <?php if ($arParams['SHOW_KIRPICH']): ?>
    <div class="package-tab__inner <?= $arParams["KIRPICH_ACTIVITY"] ?>" data-item="package-tab-kirpich" id="kirpich-complect-mob"></div>
    <?php endif; ?>
</section>

