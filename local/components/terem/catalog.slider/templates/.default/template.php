<section class="house-slider-tab slider-previews-ajx active" id="house-slider-tab" >
    <ul class="slider-wrapper" data-item="slider-item-catalog" data-type="house-slider">
        <li class="owl-item" id="photo-slide">
            <img src="<?= $arParams['PICTURE'] ?>">
            <?php if ($arParams['SHOW_INFO_ON_PHOTO']): ?>
                <div class="caption">
                    <?= $arParams["PICTURE_DESCRIPTION"] ?>
                    <!-- Варианты пристроек к этому дому в разделе <a href="<?= $arParams["LINK_TO_CALC"] ?>">калькулятор доп.услуг</a> -->
                </div>
            <?php endif; ?>
        </li>
        <?php if ($arParams['3D']): ?>
        <li class="owl-item" id="3D-slide">
            <div id="i3d" class="i3d-gallary"><?= html_entity_decode($arParams['3D']) ?></div>
        </li>
        <?php endif; ?>
        <?php if ($arParams['PLANS1']): ?>
        <li class="owl-item" id="layout_1-slide">
            <img src="<?= $arParams['PLANS1'] ?>">
            <?php if ($arParams['SHOW_INFO_ON_PHOTO']): ?>
            <div class="caption">
                <?= $arParams['PLANS1_DESCRIPTION'] ?>
                <!-- Варианты пристроек к этому дому в разделе <a href=" <?= $arParams["LINK_TO_CALC"] ?>">калькулятор доп.услуг</a> -->
            </div>
            <?php endif; ?>
        </li>
        <?php endif; ?>
        <?php if ($arParams['PLANS2']): ?>
        <li class="owl-item" id="layout_2-slide">
            <img src="<?= $arParams['PLANS2'] ?>">
            <?php if ($arParams['SHOW_INFO_ON_PHOTO']): ?>
            <div class="caption">
                <?= $arParams['PLANS2_DESCRIPTION'] ?>
                <!-- Варианты пристроек к этому дому в разделе <a href=" <?= $arParams["LINK_TO_CALC"] ?>">калькулятор доп.услуг</a> -->
            </div>
            <?php endif; ?>
        </li>
        <?php endif; ?>
        <?php foreach ($arParams["EXTERIERS"] as $image): ?>
        <li class="owl-item"><img src="<?= $image ?>"></li>
        <?php endforeach; ?>
        <?php foreach ($arParams["INTERIERS"] as $image): ?>
        <li class="owl-item"><img src="<?= $image ?>"></li>
        <?php endforeach; ?>
    </ul>
    <div class="slider-previews">
        <ul class="slider-previews__list" data-item="slider-catalog-side" data-type="house-slider-side">
            <li data-link="photo-slide"><img src="<?= $arParams['PICTURE_SMALL'] ?>"></li>
            <?php if ($arParams['3D']): ?>
            <li data-link="3D-slide" class="threesixty-slide"><img src="<?= $arParams['3D_SMALL'] ?>"></li>
            <?php endif; ?>
            <?php if ($arParams['PLANS1']): ?>
            <li data-link="layout_1-slide" class="layout_1-holder"><img src="<?= $arParams['PLANS1_SMALL'] ?>"></li>
            <?php endif; ?>
            <?php if ($arParams['PLANS2']): ?>
            <li data-link="layout_2-slide" class="layout_2-holder"><img src="<?= $arParams['PLANS2_SMALL'] ?>"></li>
            <?php endif; ?>
            <?php foreach ($arParams["EXTERIERS_SMALL"] as $image): ?>
            <li class="owl-item"><img src="<?= $image ?>"></li>
            <?php endforeach; ?>
            <?php foreach ($arParams["INTERIERS_SMALL"] as $image): ?>
            <li class="owl-item"><img src="<?= $image ?>"></li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>