<?php
if (!$arResult['ID'] || !isset($arResult['ID'])){
    http_response_code(404);
    header("HTTP/1.1 404 Not Found");
    //include("http://{$_SERVER['HTTP_HOST']}/404.php"); // provide your own HTML for the error page
    header("Location: http://{$_SERVER['HTTP_HOST']}/404.php");
}

// Получаем UF_свойства

$arOrder = [];
$arFilter = ['IBLOCK_ID' => SERIES_IBLOCK, 'ID' => $arResult['ID']];
$arSelect = ['ID', 'UF_*'];
$arResult += CIBlockSection::GetList($arOrder, $arFilter, 0, $arSelect)->GetNext();

City::HandleCases($arResult['UF_TITLE']);
City::HandleCases($arResult['UF_DESCRIPTION']);
City::HandleCases($arResult['UF_LINKBLOCK_DISPLAY']);
City::HandleCases($arResult['UF_LINKBLOCK']);

$arResult['SHOW_DESCRIPTION'] = City::IsMoscow();
