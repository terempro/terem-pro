<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
$APPLICATION->SetPageProperty("description", $arResult['UF_DESCRIPTION']);
$APPLICATION->SetPageProperty("keywords", $arResult['UF_KEYWORDS']);
$APPLICATION->SetPageProperty("title", $arResult['UF_TITLE']);



?>
<section class="catalogue-body content {{ filterStatus }}" ng-cloak >
    <?php if ($arResult['SHOW_DESCRIPTION']): ?>
    <section class="content {{ descriptionStatus }}" role="content" id="category_cheked" ng-cloak>
        <!-- <div class="container"> -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12" id="category">
                    <div class="row catlog-text">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="catalog-description my-margin">
                                <h1 class="catalog-titile"><?= $arResult['UF_TOP_HEADER'] ?></h1>
                                <p><?= $arResult['UF_TOP_DESCRIPTION'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <?php endif; ?>
    <section class="content">
        <section class="houses {{ housesInSectionStatus }}" ng-cloak>
            <section class="sortby">
                <span>Сортировка:</span>
                <button class="sort-btn" data-order="up" ng-click="sortBy('real_price', $event)">По цене</button>
                <button class="sort-btn" data-order="up" ng-click="sortBy('total_square', $event)">По площади</button>
                <button class="sort-btn sort-btn__name" data-order="up" ng-click="sortBy('series', $event)">По названию</button>
            </section>

            <ul class="houses--list">
                <li class="houses--item-wrapper" ng-if="house.show" ng-repeat="house in selected">
                    <article class="house">
                            <section class="house--details">
                                <div>
                                    <div class="house--row">
                                        <p class="house--name">
                                            <a href="{{ house.link }}{{ getHouseTech(house) }}">{{ house.series }}</a>
                                        </p>
                                        <button class="free-advice"
                                            data-yagoal="CONSULT_CATALOG_CALLED"
                                            data-target="#formAdvice"
                                            data-code="{{ house.code }}"
                                            data-price="{{ house.real_price}}000"
                                            data-whatever="{{ house.name }}"
                                            data-toggle="modal">
                                            Бесплатная консультация
                                            <span>
                                                <i class="fa fa-phone"></i>
                                            </span>
                                        </button>

                                    </div>
                                    <section class="house--chars">
                                        <div ng-if="house.width > 0 && house.height > 0">Внешние размеры: {{ house.width }}&times;{{ house.height }}&nbsp;м</div>
                                        <div ng-if="house.total_square > 0">Площадь строения: от {{ house.total_square }} м<sup>2</sup></div>
                                    </section>
                                </div>
                                <div>
                                    <section class="house--price word_price">
                                        <span><div class="house_category">Цена</div>от {{ displayPrice(house.real_price) }} 000&nbsp;р.</span>
                                        <span class="house--price__regular" ng-if="house.big_price > 0">{{ displayPrice(house.big_price) }} 000&nbsp;р.</span>
                                    </section>
                                    <a href="{{ house.link }}{{ getHouseTech(house) }}" class="house--more">
                                        Смотреть планировку
                                    </a>
                                </div>
                                <a class="details-bg-link" href="{{ house.link }}{{ getHouseTech(house) }}"></a>
                            </section>
                            <a href="{{ house.link }}{{ getHouseTech(house) }}" class="house--img" style="background-image: url('{{house.image}}')"></a>
                    </article>
                </li>
            </ul>
        </section>
    </section>
    <section class="content {{ descriptionStatus }}" role="content" ng-cloak data-test>
<?php if ($arResult['UF_LINKBLOCK_DISPLAY']): ?>
<div class="row catlog-link">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="catalog-link my-margin bottom-text">
            <h1 class="catalog-link-title">С этими домами также смотрят</h1>
            <?=htmlspecialcharsBack($arResult['UF_LINKBLOCK'])?>
        </div>
    </div>
</div>
<?php endif; ?>
<?php if ($arResult['SHOW_DESCRIPTION']): ?>
<div class="row catlog-text">
    <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="catalog-description my-margin bottom-text">
            <?= $arResult['DESCRIPTION'] ?>
        </div>
    </div>
</div>
<?php endif; ?>
    </section>
</section>
