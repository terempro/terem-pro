<?php
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    $this->setFrameMode(true);
?>
    <section class="content content-back-side" data-parallax="up" data-opacity="true">
        <div class="container-fluid reset-padding">
            <div class="row reset-margin">
                <div class="col-xs-12 col-md-12 col-sm-12 reset-padding">
                    <div class="back-img" style="background-image: url('/bitrix/templates/.default/assets/img/bg.jpg');"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="content white">
        <section class="house-card">
            <div class="container">
                <div class="row hidden-xs">
                    <div class="col-xs-12">
                        <div class="catalog-item-gallery white-holder my-margin row-eq-height row-eq-height--rwd1200">
                            <div class="col-lg-9 col-md-12 col-xs-12 col--no-gutter">
                                <div class="section-wrapper">
                                    <?php
                                        $APPLICATION->IncludeComponent('terem:catalog.slider', '.default', $arResult['SLIDER_DATA']);
                                        $APPLICATION->IncludeComponent('terem:catalog.packages', '.default', $arResult['PACKAGES_DATA']);
                                        if ($arResult["SHOW_VIDEO"]) {
                                            $APPLICATION->IncludeComponent('terem:catalog.video', '.default', $arResult['VIDEO_DATA']);
                                        }
                                        $APPLICATION->IncludeComponent('terem:catalog.feedback', '.default', $arResult['VIDEO_DATA']);
                                        $APPLICATION->IncludeComponent('terem:catalog.plans', '.default', $arResult['PLANS_DATA']);
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-12 col-xs-12 col--no-gutter">
                                <div class="control-panel">
                                    <div class="control-panel__fixed-top" itemscope itemtype="http://schema.org/Product">
                                        <div class="house-name">
                                            <span class="house-area"><?=$arResult['SECTION']['NAME'] ?></span><br />
                                            <span class="house-area">
                                            <?= $arResult['PROPERTIES']['SIZER']['VALUE'] ?> м; от <?= $arResult['TOTAL_SQUARE'] ?> кв.м
                                        </span>
                                            <h1 class="house-title" itemprop="name">
                                                <?= $arResult['NAME'] ?>
                                            </h1>
                                            <span class="hidden" itemprop="description">Здесь должно быть описание товара, но на нашем сайте в карточке товара оно не выводится</span>
                                        </div>
                                        <span class="my-hr"></span>
                                        <span class="choose-tech__span">Выберите технологию:</span>
                                        <?php 
                                            $APPLICATION->IncludeComponent('terem:catalog.tabs', '.default', $arResult['PACKAGES_DATA']);
                                            $APPLICATION->IncludeComponent('terem:catalog.rate', '.default', ['URL' => $arResult['DETAIL_PAGE_URL']]);
                                        ?>
                                    </div>
                                    <div class="control-panel__fixed-middle">
                                        <div class="nav-b">
                                            <ul>
                                                <li class="active"><a class=" nav-b__link" data-href="house-slider-tab" href="">Фото и планировки</a></li>
                                                <?php if ($arResult['SHOW_PLANS']): ?>
                                                <li><a class="nav-b__link" data-href="house-slider-tab2" href="">Варианты планировок</a></li>
                                                <?php endif; ?>
                                                <li><a class="nav-b__link complectation" data-href="package-tab" href="" data-house="<?= $arResult['ID'] ?>" data-karkas="<?= $arResult['CHEAPEST_KARKAS'] ?>" data-kirpich="<?= $arResult['CHEAPEST_KIRPICH'] ?>" data-brus="<?= $arResult['CHEAPEST_BRUS'] ?>">Комплектация</a></li>
                                                <?php if ($arResult["SHOW_VIDEO"]): ?>
                                                <li><a class="nav-b__link" data-href="video-tab" href="">Видео</a></li>
                                                <?php endif; ?>
                                                <li><a class="nav-b__link" data-href="feedback-tab" href="">Отзывы</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="control-panel__fixed-bottom">
                                        <div class="cta-b">
                                            <?php/* if ($arResult['SECTION']['ID'] !== '28731'): ?>
                                                <button type="button" class="cta-promo" data-toggle="modal" data-target="#formGetCode">
                                                Фундамент<br>в подарок
                                            </button>
                                                <?php endif */?>
                                                <?if($_SESSION['city'] != 'Новосибирск'):?>
                                                    <button data-yagoal="CONSULT_HOUSE_CARD_CALLED" type="button" class="cta-btn" data-toggle="modal" data-target="#formAdviceItem" data-code="<?= $arResult[" CODE_1C "] ?>" data-price="" data-house="<?= $arResult['NAME'] ?>">
                                                Заказать консультацию
                                            </button>
                                                    <?endif;?>
                                        </div>
                                        <div class="info-bottom">
                                            <div class="chosen-house">
                                                <span>
                                                Вы смотрите: <span><?= $arResult['NAME'] ?></span> <span id="chosen-house-tech"></span>
                                                </span>
                                            </div>
                                            <div class="back-to-catalog">
                                                <a href="<?= $arResult['BACK_URL'] ?>">Вернуться в каталог</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row visible-xs">
                    <div class="col-xs-12">
                        <div class="catalog-card">
                            <div class="house-name">
                                <h1 class="house-title" itemprop="name">
                                    <?= $arResult['NAME'] ?>
                                </h1>
                                <span class="house-area">
                                            <?= $arResult['PROPERTIES']['SIZER']['VALUE'] ?> м от <?= $arResult['TOTAL_SQUARE'] ?> кв.м
                                        </span>
                                <span class="hidden" itemprop="description">Здесь должно быть описание товара, но на нашем сайте в карточке товара оно не выводится</span>
                            </div>
                            <?php $APPLICATION->IncludeComponent('terem:catalog.slider', '.default', $arResult['SLIDER_DATA']) ?>
                            <div class="control-panel_mobile">
                                <span class="choose-tech__span">Выберите технологию:</span>
                                <?php $APPLICATION->IncludeComponent('terem:catalog.tabs_mob', '.default', $arResult['PACKAGES_DATA']) ?>
                                <div class="nav-m">
                                <?php if ($arResult['PLANS_DATA']): ?>
                                    <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                                    <div class="my-table">
                                                        <div>
                                                            <p>Планировка</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-chevron-up"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="faq1">
                                                    <div class="( my-collapse-content + design-main )">
                                                        <?php $APPLICATION->IncludeComponent('terem:catalog.plans_mob', '.default', $arResult['PLANS_DATA']) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="ajaxDataLoader ( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2" data-href="package-tab" href="" data-house="<?= $arResult['ID'] ?>" data-karkas="<?= $arResult['CHEAPEST_KARKAS'] ?>" data-kirpich="<?= $arResult['CHEAPEST_KIRPICH'] ?>" data-brus="<?= $arResult['CHEAPEST_BRUS'] ?>">
                                                    <div class="my-table">
                                                        <div>
                                                            <p >Комплектация</p>
                                                        </div>
                                                        <div>
                                                            <i class="glyphicon glyphicon-chevron-up"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="faq2">
                                                    <div class="( my-collapse-content + design-main )">
                                                    <?php
                                                        $APPLICATION->IncludeComponent('terem:catalog.packages_mob', '.default', $arResult['PACKAGES_DATA']);
                                                    ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="cta-b">
                                    <?php/* if ($arResult['SECTION']['ID'] !== '28731'): ?>
                                        <button type="button" class="cta-promo" data-toggle="modal" data-target="#formGetCode">
                                                Фундамент<br>в подарок
                                            </button>
                                        <?php endif */?>
                                        <?if($_SESSION['city'] != 'Новосибирск'):?>
                                            <button data-yagoal="CONSULT_HOUSE_CARD_CALLED" type="button" class="cta-btn" data-toggle="modal" data-target="#formAdviceItem" data-code="<?= $arResult[" CODE_1C "] ?>" data-price="" data-house="<?= $arResult['NAME'] ?>">
                                                Заказать консультацию
                                            </button>
                                            <?endif;?>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
    </section>