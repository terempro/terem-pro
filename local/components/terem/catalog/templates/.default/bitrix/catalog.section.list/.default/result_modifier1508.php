<?php

// Получаем описание секции для каталога - свойство UF_LIST_DESCRIPTION

foreach ($arResult["SECTIONS"] as $key => $section)
{
    $arOrder = [];
    $arFilter = ['IBLOCK_ID' => SERIES_IBLOCK, 'ID' => $section['ID']];
    $arSelect = ['ID', 'UF_LIST_DESCRIPTION'];

    $data = CIBlockSection::GetList($arOrder, $arFilter, 0, $arSelect)->GetNext();
    $arResult['SECTIONS'][$key]['LIST_DESCRIPTION'] = $data['UF_LIST_DESCRIPTION'];
}
