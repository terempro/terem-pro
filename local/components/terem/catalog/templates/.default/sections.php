<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
/*
$extra_class = '';
if ($_SESSION['extra_class']) $extra_class = $_SESSION['extra_class'];
else $extra_class = $_SESSION['extra_class'] = (rand(0,32765)%2 ? 'filter-app-variant2' : '');
*/



if (City::IsMoscow() && false)
{ ?>
    <section class="catalog-banner content">
        <a href="/promotion/">
            <img src="/upload/banners/catalog_banner.jpg" alt="">
        </a>
    </section>
    <?php
} ?>
<div ng-app="filter-app" ng-controller="filterCtrl" class="filter-app <?//=$extra_class?>">
<?php
$APPLICATION->IncludeComponent("terem:catalog.filter", ".default");


$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "", array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
        "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
        "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
        "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
    ), $component, array("HIDE_ICONS" => "Y")
);
?>
</div>