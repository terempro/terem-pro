<section class="video-tab" id="video-tab">
    <div class="video-wrapper">
        <div class="video-slider" data-item="slider-item-catalog">
            <?php
            foreach ($arParams["VIDEOS"] as $v) echo html_entity_decode($v);
            ?>
        </div>
        <?php if (count($arParams["VIDEOS"]) > 1): ?>
        <div class="video-previews">
            <ul class="slider-previews__list" data-item="slider-catalog-side">
            <?php foreach ($arParams["VIDEOS"] as $v): ?>
                <li class="owl-item"><img src="/upload/video_preview.jpg"></li>
            <?php endforeach; ?>    
            </ul>
        </div>
        <?php endif; ?>
        <div class="video-caption">
            <div class="video-caption__left">
                <span class="video-title"><?= $arParams['TITLE'] ?></span>
                <span class="view-count"><?= $arParams["SHOWN"] ?> просмотров</span>
            </div>
        </div>
    </div>
    <div class="video-description">
        <div class="description-text">
            <img src="/bitrix/templates/.default/assets/img/terem-from-youtube.jpg" class="youtube-terem-icon">
            <p>
                Присоединяйтесь к числу подписчиков канала «Теремъ», чтобы не пропустить ничего интересного.
            </p>
        </div>
        <noindex>
            <button class="g-ytsubscribe" data-channelid="UC1jxLRcdCGn9DstlU9UUSRA" data-layout="default" data-count="default" data-onytevent="onYtEvent"></button>
        </noindex>
    </div>
</section>

