<section class="house-slider-tab slider-previews-ajx active" id="house-slider-tab-mob" >
    <ul class="slider-wrapper" data-item="slider-item-catalog" data-type="house-slider">
        <li class="owl-item" id="photo-slide-mob">
            <img src="<?= $arParams['PICTURE'] ?>">
            <?php if ($arParams['SHOW_INFO_ON_PHOTO']): ?>
                <div class="caption">
                    <?= $arParams["PICTURE_DESCRIPTION"] ?>
                    <!-- Варианты пристроек к этому дому в разделе <a href="<?= $arParams["LINK_TO_CALC"] ?>">калькулятор доп.услуг</a> -->
                </div>
            <?php endif; ?>
        </li>
        <?php if ($arParams['3D']): ?>
        <li class="owl-item" id="3D-slide">
            <div id="i3d" class="i3d-gallary"><?= html_entity_decode($arParams['3D']) ?></div>
        </li>
        <?php endif; ?>
        <?php foreach ($arParams["EXTERIERS"] as $image): ?>
        <li class="owl-item"><img src="<?= $image ?>"></li>
        <?php endforeach; ?>
        <?php foreach ($arParams["INTERIERS"] as $image): ?>
        <li class="owl-item"><img src="<?= $image ?>"></li>
        <?php endforeach; ?>
    </ul>
</section>