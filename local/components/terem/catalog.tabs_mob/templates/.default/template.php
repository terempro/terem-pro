<div class="tech-type">
    <ul>
        <?php if ($arParams['SHOW_KARKAS']): ?>
            <li class="tech-type__elem <?= $arParams['KARKAS_ACTIVITY'] ?>">
                <a href="#tab-karkas" data-toggle="tab" data-link="package-tab-karkas" data-type="karkas"
                    data-house="<?= $arParams['CHEAPEST_KARKAS'] ?>">каркас</a>
            </li>
        <?php endif; ?>
        <?php if ($arParams['SHOW_BRUS']): ?>
            <li class="tech-type__elem <?= $arParams['BRUS_ACTIVITY'] ?>">
                <a href="#tab-brus" data-toggle="tab" data-link="package-tab-brus" data-type="brus"
                    data-house="<?= $arParams['CHEAPEST_BRUS'] ?> ">брус</a>
            </li>
        <?php endif; ?>
        <?php if ($arParams['SHOW_KIRPICH']): ?>
            <li class="tech-type__elem <?= $arParams['KIRPICH_ACTIVITY'] ?>">
                <a href="#tab-kirpich" data-toggle="tab" data-link="package-tab-kirpich" data-type="kirpich"
                    data-house="<?= $arParams['CHEAPEST_KIRPICH'] ?>">кирпич</a>
            </li>
        <?php endif; ?>
    </ul>
</div>
<div class="tech-type__price tab-content clearfix" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
   <span class="hidden" itemprop="priceCurrency">RUB</span>
    <?php if($arParams['SHOW_KARKAS']):?>
        <div class="tab-pane my-tab-pane <?= $arParams['KARKAS_ACTIVITY'] ?>" id="tab-karkas-mob">
            <span class="current-price" itemprop="price"><?= $arParams['KARKAS_REAL_PRICE'] ?> р.</span>
            <?php if ($arParams['KARKAS_REAL_PRICE'] != $arParams['KARKAS_BIG_PRICE']): ?>
                <span class="future-price"><?= $arParams['KARKAS_BIG_PRICE'] ?> р.</span>
                <span class="spread-price hidden-xs">выгода сегодня <?= $arParams['KARKAS_PROFIT'] ?> р.</span>
            <?php endif;?>
        </div>
    <?php endif;?>
    <?php if($arParams['SHOW_BRUS']):?>
        <div class="tab-pane my-tab-pane <?= $arParams['BRUS_ACTIVITY'] ?>" id="tab-brus-mob">
            <span class="current-price" itemprop="price"><?= $arParams['BRUS_REAL_PRICE'] ?> р.</span>
            <?php if ($arParams['BRUS_REAL_PRICE'] != $arParams['BRUS_BIG_PRICE']): ?>
                <span class="future-price"><?= $arParams['BRUS_BIG_PRICE'] ?> р.</span>
                <span class="spread-price hidden-xs">выгода сегодня <?= $arParams['BRUS_PROFIT'] ?> р.</span>
            <?php endif;?>
        </div>
    <?php endif;?>
    <?php if($arParams['SHOW_KIRPICH']):?>
        <div class="tab-pane my-tab-pane <?= $arParams['KIRPICH_ACTIVITY'] ?>" id="tab-kirpich-mob">
            <span class="current-price" itemprop="price"><?= $arParams['KIRPICH_REAL_PRICE'] ?> р.</span>
            <?php if ($arParams['KIRPICH_REAL_PRICE'] != $arParams['KIRPICH_BIG_PRICE']): ?>
                <span class="future-price"><?= $arParams['KIRPICH_BIG_PRICE'] ?> р.</span>
                <span class="spread-price hidden-xs">выгода сегодня <?= $arParams['KIRPICH_PROFIT'] ?> р.</span>
            <?php endif;?>
        </div>
    <?php endif;?>
</div>

