<section class="package-tab" id="package-tab">
    <?php if ($arParams['SHOW_KARKAS']): ?>
    <div class="package-tab__inner__bottom-b hidden-xs">
        <div class="bottom-b__right">
            <a href="" onclick="houseCardPrint('#photo-slide', '.package-tab__inner.active', '#layout_1-slide', '#layout_2-slide'); return false;" class="print-link">Распечатать<span class="glyphicon glyphicon-print"></span></a>
        </div>
    </div>
    <div class="package-tab__inner <?= $arParams["KARKAS_ACTIVITY"] ?>" data-item="package-tab-karkas" id="karkas-complect"></div>
    <?php endif; ?>
    <?php if ($arParams['SHOW_BRUS']): ?>
    <div class="package-tab__inner <?= $arParams["BRUS_ACTIVITY"] ?>" data-item="package-tab-brus" id="brus-complect"></div>
    <?php endif; ?>
    <?php if ($arParams['SHOW_KIRPICH']): ?>
    <div class="package-tab__inner <?= $arParams["KIRPICH_ACTIVITY"] ?>" data-item="package-tab-kirpich" id="kirpich-complect"></div>
    <?php endif; ?>
    <div class="variant__modal" data-id="modal-package" id="formAdvice">
        <div class="variant__modal__container">
            <a href="#close" class="close-cross"></a>
            <h5 class="variant__parameters__title">Пакет инженерных коммуникаций для этого дома</h5>
            <div class="variant__parameters__list" id="variant-modal-ajax"></div>
            <a href="/services/enginering_communications/">Посмотреть подробнее в разделе «Инженерные коммуникаций»</a>
            <form action="http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/" method="post" data-form="send">
                <input type="text" name="name" required="required" placeholder="имя">
                <input type="tel" name="tel" required="required" placeholder="телефон" data-item="phone">
                <button type="submit" class="cta-btn">Получить цены</button>
            </form>
        </div>
    </div>
</section>

