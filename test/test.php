<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<?/*$APPLICATION->IncludeComponent(
	"terem:catalog", 
	".default", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BRAND_USE" => "N",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "DISLIKE",
			1 => "LIKE",
			2 => "PRICE_FROM",
			3 => "SHOW_COUNTER",
			4 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILTER_FIELD_CODE" => array(
			0 => "IBLOCK_ID",
			1 => "IBLOCK_NAME",
			2 => "",
		),
		"FILTER_NAME" => "arrFilter",
		"FILTER_PRICE_CODE" => array(
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_VIEW_MODE" => "VERTICAL",
		"IBLOCK_ID" => "13",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"INSTANT_RELOAD" => "N",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_META_KEYWORDS" => "-",
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"SECTIONS_VIEW_MODE" => "LIST",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_TOP_DEPTH" => "2",
		"SEF_FOLDER" => "/test/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_TOP_ELEMENTS" => "Y",
		"SIDEBAR_DETAIL_SHOW" => "Y",
		"SIDEBAR_PATH" => "",
		"SIDEBAR_SECTION_SHOW" => "Y",
		"TEMPLATE_THEME" => "blue",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_FIELD2" => "sort",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_ORDER2" => "asc",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"USE_COMPARE" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_REVIEW" => "N",
		"USE_STORE" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_ID#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);

*/?>
<?

use Bitrix\Main\Application;

	define("IBLOCK_HOUSES_VARIANTS", 21);
	define("IBLOCK_HOUSES_CATALOG", 13);
	define("BASE_URL", "/catalog/");
	define("POSSIBLE_PROPERTIES", array(
		"PROPERTY_VARIANTS_HOUSE",
		"PROPERTY_KARKAS_VARIANTS_HOUSE",
		"BRUS_VARIANTS_HOUSE",
		"KIRPICH_VARIANTS_HOUSE"
	));

	$cnt = count(POSSIBLE_PROPERTIES);

	$request = Application::getInstance()->getContext()->getRequest();
	$request->get("priceID");

	if (!is_numeric($request->get("priceID")) && (intval($request->get("priceID")) <= 0)) {
		exit ("incorrect param priceID, exit...");
	}

	// Получить все дома из прайса

	$arObjectPrice = array();

	$arSelect = array('PROPERTY_LINK', 'ID', 'NAME');
	$arFilter = array("IBLOCK_ID" => $request->get("priceID"));
	$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while ($ob = $res->GetNext()) {
		$arObjectPrice[] = $ob;		
	}

	//echo '<pre>',print_r($arObjectPrice, true),'</pre>';

	//die();

	$iblockCode = explode("_",$arObjectPrice[0]['IBLOCK_CODE']);
	$city = $iblockCode[1];

	// Получить все варианты домов

	$qqq1 = 0;

	$arObjectVar = array();

	$arSelect = array("ID","NAME","PROPERTY_PRICE_HOUSE_" . $city);
	$arFilter = Array("IBLOCK_ID" => IBLOCK_HOUSES_VARIANTS,">PROPERTY_PRICE_HOUSE_" . $city => "0");
	$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while ($ob = $res->GetNext()) {
		$arObjectVar[$ob['PROPERTY_PRICE_HOUSE_'. $city.'_VALUE']] = $ob;
		$arObjectVarName[$ob['NAME']] = $ob;
		//echo '<pre>',print_r($arObjectVarName, true),'</pre>';
		
	}

	//die();

	// Получить все дома

	$arObject = array();

	$arSelect = array();
	$arFilter   = Array("IBLOCK_ID" => IBLOCK_HOUSES_CATALOG);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFieldsCatalog = $ob->GetFields();
		$arFieldsCatalogP  = $ob->GetProperties();

		foreach($arFieldsCatalogP['VARIANTS_HOUSE']['VALUE'] as $arVarValue){
			$arFieldsCatalog['VALUE_FIND'][] = $arVarValue;
		}
		foreach($arFieldsCatalogP['KARKAS_VARIANTS_HOUSE']['VALUE'] as $arVarValue){
			$arFieldsCatalog['VALUE_FIND'][] = $arVarValue;
		}
		foreach($arFieldsCatalogP['BRUS_VARIANTS_HOUSE']['VALUE'] as $arVarValue){
			$arFieldsCatalog['VALUE_FIND'][] = $arVarValue;
		}
		foreach($arFieldsCatalogP['KIRPICH_VARIANTS_HOUSE']['VALUE'] as $arVarValue){
			$arFieldsCatalog['VALUE_FIND'][] = $arVarValue;
		}


		$arObject[] = $arFieldsCatalog;
		//echo '<pre>',print_r($arFieldsCatalogP['PROPERTY_VARIANTS_HOUSE'], true),'</pre>';
		//die();
	}

	//echo '<pre>',print_r($arObject, true),'</pre>';
	//die();

	// Для каждого дома из прайса найти вариант дома, а для него найт дом - сформировать ссылку


	$vsegoObject = 0;

	foreach ($arObjectPrice as $keyPrice => $valuePrice) {
		
		if(!$valuePrice['PROPERTY_LINK_VALUE']){ // не обрабатывать с ссылкой
		
			echo '<pre>',print_r($valuePrice['NAME'], true),'</pre>';

			//die();


			$nameExplode = explode(" ", $valuePrice['NAME']);

				//echo '<pre>',print_r($nameExplode, true),'</pre>';

				$nameFind = "";

				if($nameExplode[1] == "-"){
					if(is_numeric($nameExplode[2])){
						$nameFind = $nameExplode[0] . " " . $nameExplode[2];
					}elseif(is_numeric($nameExplode[2][0])){
						$nameFind = $nameExplode[0] . " " . $nameExplode[2][0];
					}else{
						$nameFind = $nameExplode[0];
					}
				}else{
					if(is_numeric($nameExplode[1])){
						$nameFind = $nameExplode[0] . " " . $nameExplode[1];
					}elseif(is_numeric($nameExplode[1][0])){
						$nameFind = $nameExplode[0] . " " . $nameExplode[1][0];
					}else{
						$nameFind = $nameExplode[0];
					}
				}
				
				//echo '<pre>',print_r($nameExplode, true),'</pre>';
				//echo '<pre>',print_r($nameFind, true),'</pre>';

			//die();

			$url  = "";
			$valueVar['ID'] = "";
				// найти дом с этип предложением

				$valueVar['ID'] = $arObjectVar[$valuePrice['ID']]['ID'];

				if(!$valueVar['ID']){
					//echo '<pre>',print_r($valuePrice['ID'], true),'</pre>';
				}

				if($arObjectVarName[$valuePrice['NAME']]){
					$vsegoObject++;
				}

				foreach ($arObject as $keyObject => $valueObject) {

					$arProperty = array(
						'LINK' => $valueObject['DETAIL_PAGE_URL'],
					);

					if(in_array($valueVar['ID'], $valueObject['VALUE_FIND'])){
						// собираем ссылку
						$test123++;
						//echo '<pre>',print_r($valuePrice, true),'</pre>';

						//$test = CIBlockElement::SetPropertyValuesEx($valuePrice["ID"], $request->get("priceID"), $arProperty);
												
						break;
					}

					if($valueObject['NAME'] == $valuePrice['NAME']){ // Сравнение по  NAME
						//echo '<pre>',print_r($valueObject, true),'</pre>';
						//echo '<pre>',print_r($valuePrice, true),'</pre>';

						//$test = CIBlockElement::SetPropertyValuesEx($valuePrice["ID"], $request->get("priceID"), $arProperty);

						break;

					}

					if($valueObject['NAME'] == $nameFind){//Сложное сравнение по имени
						//echo '<pre>',print_r($nameFind, true),'</pre>';
						//echo '<pre>',print_r($valueObject['NAME'], true),'</pre>';
						//echo '<pre>',print_r($valuePrice['NAME'], true),'</pre>';
						$test123++;
						//$test = CIBlockElement::SetPropertyValuesEx($valuePrice["ID"], $request->get("priceID"), $arProperty);
						break;
					}

					
					
				
			}

		}
	}

	echo '<pre>',print_r($test123, true),'</pre>';
	echo '<pre>',print_r($vsegoObject, true),'</pre>';
?>






<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>