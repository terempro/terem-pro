var _data;
var anchors = [];
var currentSlide;
var slider1;
var slider2;
ymaps.ready(initYmap);
var myMap;

$('.section').each(function () {
	anchors.push($(this).attr('id'));
	currentSlide = $('#' + anchors[0]);
});

function fetchData() {
	$.ajax({
		dataType: 'json',
		url: 'js/helper/data.json',
		success: function (data) {
			_data = data.items;
			for (var i = 0; i < _data.length; i++) {
				var sliderItem = `
				<div class="slider__item">
					<div class="slider__wrapper">
						<div class="slider__img">
							<a href="#modal-house" data-link="modal">
								<img src="img/house/${_data[i].image}/1.jpg" alt="${_data[i].name}">
							</a>
						</div>
						<div class="slider__caption">
							<div class="slider__caption--left">
								<span class="slider__name" data-code="${_data[i].code}" data-house="${_data[i].name}" data-certificate="${_data[i].certificate}">${_data[i].name}</span><span class="slider__tech">${_data[i].area}</span>
							</div>
							<div class="slider__caption--right">
								<a class="btn btn--slider" href="#modal-house" data-link="modal" style="background-color: #D80000; color: white;">Подробнее о доме</a>
							</div>
						</div>
					</div>
				</div>
			`;

				$('.single-item').append(sliderItem)
			}
		},
		complete: initSigleItemSlider
	});
};

function getCurrentSlide() {
	return currentSlide;
}

function setCurrentSlide(elem) {
	currentSlide = elem;
	var currentLink = ($('.nav').find('a').filter((idx, elem) => $(elem).data('name') === $(currentSlide).attr('id') ? elem : null));
	$(currentLink).addClass('active').siblings().removeClass('active');
}

function debounce(func, wait, immediate) {
	var timeout;
	return function () {
		var context = this,
			args = arguments;
		var later = function () {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function throttle(func, ms) {
	var isThrottled = false,
		savedArgs,
		savedThis;

	function wrapper() {
		if (isThrottled) { // (2)
			savedArgs = arguments;
			savedThis = this;
			return;
		}

		func.apply(this, arguments); // (1)
		isThrottled = true;

		setTimeout(function () {
			isThrottled = false; // (3)
			if (savedArgs) {
				wrapper.apply(savedThis, savedArgs);
				savedArgs = savedThis = null;
			}
		}, ms);
	}
	return wrapper;
}

function initSmoothScrollOnClick() {
	$('.smooth').on('click', function (e) {


		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top
		}, 1000);

		setCurrentSlide($($.attr(this, 'href')));
		if (currentSlide.attr('id') === 'hero') {
			$('.header').addClass('hero')
			$('.logo').addClass('hero')
		} else {
			$('.header').removeClass('hero')
			$('.logo').removeClass('hero')
		}

		return false;
	});
}

function initSmoothSlideScroll() {
	$(window).on('wheel', debounce(function (e) {
		if ($('.modal.active').length) return false;
		if (e.originalEvent.wheelDelta / 120 > 0 || e.originalEvent.deltaY / 3 < 0) {
			//Scroll up
			changeSlide(-1);
			return false;
		} else {
			//Scroll down
			changeSlide(1);
			return false;
		}
		$('body').removeClass('stop-scrolling')
	}, 100))

	if ($(window).width() < 992) {
		$(window).off('wheel');
	}
}

function changeSlide(direction) {
	if (direction === 1) {
		if (getCurrentSlide().hasClass('contacts')) return false;

		$('html, body').animate({
			scrollTop: getCurrentSlide().next().offset().top
		}, 500);
		setCurrentSlide(getCurrentSlide().next());

		if (currentSlide.attr('id') === 'hero') {
			$('.header').addClass('hero')
			$('.logo').addClass('hero')
		} else {
			$('.header').removeClass('hero')
			$('.logo').removeClass('hero')
		}

		return false;

	} else {
		if (getCurrentSlide().hasClass('hero')) return false;

		$('html, body').animate({
			scrollTop: getCurrentSlide().prev().offset().top
		}, 500);
		setCurrentSlide(getCurrentSlide().prev());

		if (currentSlide.attr('id') === 'hero') {
			$('.header').addClass('hero')
			$('.logo').addClass('hero')
		} else {
			$('.header').removeClass('hero')
			$('.logo').removeClass('hero')
		}

		return false;
	}
}

function initMaskedInput() {
	$('[data-item="phone"]').mask("+7 (999) 999-99-99");
}

function initCountdown() {
	$('.counter').countdown('2017/09/21').on('update.countdown', function (event) {
		var $this = $(this).html(event.strftime('<div class="counter__item days"><div class="counter__digits">%D</div><div class="counter__time">дней</div></div>' + '<div class="counter__item hours"><div class="counter__digits">%H</div><div class="counter__time">часов</div></div>' + '<div class="counter__item minutes"><div class="counter__digits">%M</div><div class="counter__time">минут</div></div>' + '<div class="counter__item seconds"><div class="counter__digits">%S</div><div class="counter__time">секунд</div></div>'));
	});
}

function initSigleItemSlider() {
	var singleItem = $('.single-item').bxSlider({
		pager: false,
		nextText: '',
		prevText: '',
		onSliderLoad: function () {
			var current = $(this[0]).find('.slider__item')[1]
			$(current).addClass('active');
		},
		onSlideAfter: function ($slideElement) {
			$slideElement.siblings().removeClass('active');
			$slideElement.addClass('active');
		},
	});
}

function initSliders() {
	slider1 = $('#slider1').bxSlider({
		controls: false,
		pager: false,
		minSlides: 3,
		maxSlides: 3,
		nextText: '',
		prevText: '',
		slideWidth: 340,
		slideMargin: 10
	});

	slider2 = $('#slider2').bxSlider({
		pager: false,
		minSlides: 3,
		maxSlides: 3,
		slideMargin: 20,
		nextText: '',
		prevText: '',
		slideWidth: 340,
		slideMargin: 10
	});

	$('#video-slider').bxSlider({
		nextText: '',
		prevText: ''
	});

	$('.slider2').on('click', '.bx-prev', function () {
		slider1.goToPrevSlide();
	})
	$('.slider2').on('click', '.bx-next', function () {
		slider1.goToNextSlide();
	})

	$('#bx-slider1').bxSlider({
		infiniteLoop: false,
		pagerCustom: '#bx-pager1',
		nextText: '',
		prevText: ''
	});

	$('#bx-slider2').bxSlider({
		infiniteLoop: false,
		pagerCustom: '#bx-pager2',
		nextText: '',
		prevText: ''
	});

	$('#bx-slider3').bxSlider({
		infiniteLoop: false,
		pagerCustom: '#bx-pager3',
		nextText: '',
		prevText: ''
	});
}

function widthHandler() {
	var width = $(window).width();
	if (width < 768) {
		slider1.reloadSlider({
			pager: false,
			minSlides: 1,
			maxSlides: 1,
			slideWidth: 340,
			slideMargin: 10
		});
		slider2.reloadSlider({
			pager: false,
			minSlides: 1,
			maxSlides: 1,
			slideWidth: 340,
			slideMargin: 10
		})
	} else {
		slider1.reloadSlider({
			controls: false,
			pager: false,
			minSlides: 3,
			maxSlides: 3,
			nextText: '',
			prevText: '',
			slideWidth: 340,
			slideMargin: 10
		});
		slider2.reloadSlider({
			pager: false,
			minSlides: 3,
			maxSlides: 3,
			slideMargin: 20,
			nextText: '',
			prevText: '',
			slideWidth: 340,
			slideMargin: 10
		})
	}
};

function isScrolledIntoView(el) {
	var elemTop = el.getBoundingClientRect().top / 1.2;
	var elemBottom = el.getBoundingClientRect().bottom / 1.2;

	var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
	return isVisible;
}

function initFancybox() {
	$('.fancybox').fancybox();
}

function initYmap() {
	myMap = new ymaps.Map('map', {
		center: [55.70200328, 37.76498550],
		zoom: 16,
		controls: []
	});
	myMap.behaviors.disable(['rightMouseButtonMagnifier', 'scrollZoom']);
	myPlacemark = new ymaps.Placemark([55.70200328, 37.76498550], {
		hintContent: [
			''
		].join('')
	}, {
		iconLayout: 'default#image',
		iconImageHref: 'img/map-pin.png',
		iconImageSize: [47, 54],
		iconImageOffset: [-24, -54]
	});
	myMap.geoObjects.add(myPlacemark);
}

function initModals() {
	//Show modal
	$(document).on('click', '[data-link="modal"]', function () {
		var link = $(this).attr('href') || $(this).data('href');
		var modal = $(link);
		var house = $('.single-item .slider__item.active .slider__name').data('house');
		var certificate = $('.single-item .slider__item.active .slider__name').data('certificate');


		$('#modal-call-house-name').text(house);
		$('#modal-call-house-certificate').text(certificate + ' р.');
		$('#modal-call-house-hidden-name').val(house);
		$('#modal-call-house-hidden-certificate').val(certificate);
		$('#modal-call-house-hidden-code').val($('.single-item .slider__item.active .slider__name').data('code'));

		$(modal).addClass('active');
		$('body').addClass('stop-scrolling')

		if ($(this).attr('href') === '#modal-house') {
			handleModalHouse();
		}
	})
	//Hide modal on cross click
	$('.modal').on('click', '.close', function () {
		$(this).closest('.modal').removeClass('active');
		$('body').removeClass('stop-scrolling')
	})
	//Hide modal on empty place click
	// $(document).on('click', function (e) {
	// 	var target = e.target;

	// 	if ($(target).attr('id') === 'modal-house') return false;

	// 	if ($(target).hasClass('modal')) {
	// 		$('.modal').removeClass('active');
	// 	}
	// })
}

function handleModalHouse() {
	var currentSlide = $('.single-item').find('.slider__item.active');
	var currentHouse = $(currentSlide).find('.slider__name').data('house');

	for (var i = 0; i < _data.length; i++) {
		if (currentHouse === _data[i].name) {
			var modal = `
				<div class="modal__dialog">
					<button type="button" class="close"></button>
					<div class="modal__tabs">
						<a href="#house-info" class="active" id="house-button" data-item="tab">${_data[i].name}</a>
						<a href="#kitchen-info1" data-item="tab">Кухня вариант 1</a>
						<a href="#kitchen-info2" data-item="tab">Кухня вариант 2</a>
					</div>
					<div class="modal__content">
						<div class="tab active" id="house-info">
							<div class="modal__content--wrapper">
								<div class="modal__content--left">
									<div class="modal__header">
										<span class="title" id="modal-house-name">${_data[i].name}</span>
										<div class="modal__params">
											<span>Размеры: ${_data[i].size} м</span>
											<span>Площадь: ${_data[i].space} м<sup>2</sup></span>
										</div>
									</div>
									<div class="modal__slider">
										<ul class="bx-slider" id="bx-slider1">
											<li><img src="img/house/${_data[i].image}/1.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/2.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/3.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/4.jpg" /></li>
										</ul>

										<div class="bx-pager" id="bx-pager1">
											<a data-slide-index="0" href=""><img src="img/house/${_data[i].image}/1.jpg" /></a>
											<a data-slide-index="1" href=""><img src="img/house/${_data[i].image}/2.jpg" /></a>
											<a data-slide-index="2" href=""><img src="img/house/${_data[i].image}/3.jpg" /></a>
											<a data-slide-index="3" href=""><img src="img/house/${_data[i].image}/4.jpg" /></a>
										</div>
									</div>
									<!-- <div class="modal__link">
										<a href="#0">Все варианты этого проекта в официальном каталоге компании «Теремъ»</a>
									</div> -->
								</div>
								<div class="modal__content--right">
									<form action="/include/request_handler.php" data-yagoal="REQUEST_KITCHEN_CONSULTING_SENT" onsubmit="sendForm(this);return false" method="post">
										<div class="group">
											<p class="form__title">
												Оставьте заявку
											</p>
											<p class="form__annotation">
												Чтобы получить подробную<br>комплектацию и цену
											</p>
										</div>
										<div class="form__group">
											<label for="project-name" class="form__label">
													Как к вам удобнее обратиться
													<input name="name" required type="text" id="project-name" class="form__input" placeholder="Ваше имя">
												</label>
											<label for="project-tel" class="form__label">
													Ваш контактный номер
													<input name="phone" required type="tel" id="project-tel" class="form__input" data-item="phone" placeholder="+7 (___) ___-__-__">
												</label>
										</div>
										<div class="form__group">
											<input required type="checkbox" class="checkbox-input" id="project-agree" hidden="">
											<label for="project-agree" class="form__label form__label--checkbox">
													<span>Я принимаю условия<br><a href="#0">Политики обработки данных</a></span>
												</label>
										</div>
										<div class="form__group">
                                                                                        <input type="hidden" name="request_type" value="11">
                                                                                        <input type="hidden" name="house" value="${_data[i].name}">
                                                                                        <input type="hidden" name="code" value="${_data[i].code}">
											<button type="submit" class="btn btn--form">Узнать цену</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab" id="kitchen-info1">
							<div class="modal__content--wrapper">
								<div class="modal__content--left">
									<div class="modal__header">
										<span class="title" id="modal-house-name">${_data[i].kitchen1[0].name}</span>
									</div>
									<div class="modal__slider">
										<ul class="bx-slider" id="bx-slider2">
											<li><img src="img/house/${_data[i].image}/k1.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/k2.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/k3.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/k4.jpg" /></li>
										</ul>
										<div class="bx-pager" id="bx-pager2">
											<a data-slide-index="0" href=""><img src="img/house/${_data[i].image}/k1.jpg" /></a>
											<a data-slide-index="1" href=""><img src="img/house/${_data[i].image}/k2.jpg" /></a>
											<a data-slide-index="2" href=""><img src="img/house/${_data[i].image}/k3.jpg" /></a>
											<a data-slide-index="3" href=""><img src="img/house/${_data[i].image}/k4.jpg" /></a>
										</div>
									</div>
									<!-- <div class="modal__link">
										<a href="#0">Все варианты этого проекта в официальном каталоге компании «Теремъ»</a>
									</div> -->
								</div>
								<div class="modal__content--right">
									<div class="group">
										<p class="form__title">
											Характеристики
										</p>
									</div>
									<div class="group">
										<ul class="list">
											<li><strong>Артикул:</strong><span>${_data[i].kitchen1[0].params[0].value}</span></li>
											<li><strong>Материал фасада:</strong><span>${_data[i].kitchen1[0].params[1].value}</span></li>
											<li><strong>Стиль:</strong><span>${_data[i].kitchen1[0].params[2].value}</span></li>
											<li><strong>Цвет верхних ящиков:</strong><span>${_data[i].kitchen1[0].params[3].value}</span></li>
											<li><strong>Цвет нижних ящиков:</strong><span>${_data[i].kitchen1[0].params[4].value}</span></li>
											<li><strong>Каркас:</strong><span>${_data[i].kitchen1[0].params[5].value}</span></li>
											<li><strong>Фарнитура:</strong><span>${_data[i].kitchen1[0].params[6].value}</span></li>
											<li><strong>Столешница:</strong><span>${_data[i].kitchen1[0].params[7].value}</span></li>
											<li><strong>Цоколь:</strong><span>${_data[i].kitchen1[0].params[8].value}</span></li>
											<li><strong>Форм-фактор:</strong><span>${_data[i].kitchen1[0].params[9].value}</span></li>
											<li><strong>Срок производства:</strong><span>${_data[i].kitchen1[0].params[10].value}</span></li>
											<li><strong>Бренд:</strong><span>${_data[i].kitchen1[0].params[11].value}</span></li>
											<li><strong>Страна:</strong><span>${_data[i].kitchen1[0].params[12].value}</span></li>
										</ul>
									</div>
									<div class="form__group">
										<a href="#modal-call" data-link="modal" class="btn btn--form">Забронировать</a>
									</div>
								</div>
							</div>
						</div>
						<div class="tab" id="kitchen-info2">
							<div class="modal__content--wrapper">
								<div class="modal__content--left">
									<div class="modal__header">
										<span class="title" id="modal-house-name">${_data[i].kitchen2[0].name}</span>
									</div>
									<div class="modal__slider">
										<ul class="bx-slider" id="bx-slider3">
											<li><img src="img/house/${_data[i].image}/1k.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/2k.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/3k.jpg" /></li>
											<li><img src="img/house/${_data[i].image}/4k.jpg" /></li>
										</ul>
										<div class="bx-pager" id="bx-pager3">
											<a data-slide-index="0" href=""><img src="img/house/${_data[i].image}/1k.jpg" /></a>
											<a data-slide-index="1" href=""><img src="img/house/${_data[i].image}/2k.jpg" /></a>
											<a data-slide-index="2" href=""><img src="img/house/${_data[i].image}/3k.jpg" /></a>
											<a data-slide-index="3" href=""><img src="img/house/${_data[i].image}/4k.jpg" /></a>
										</div>
									</div>
									<!-- <div class="modal__link">
										<a href="#0">Все варианты этого проекта в официальном каталоге компании «Теремъ»</a>
									</div> -->
								</div>
								<div class="modal__content--right">
									<div class="group">
										<p class="form__title">
											Характеристики
										</p>
									</div>
									<div class="group">
										<ul class="list">
											<li><strong>Артикул:</strong><span>${_data[i].kitchen2[0].params[0].value}</span></li>
											<li><strong>Материал фасада:</strong><span>${_data[i].kitchen2[0].params[1].value}</span></li>
											<li><strong>Стиль:</strong><span>${_data[i].kitchen2[0].params[2].value}</span></li>
											<li><strong>Цвет верхних ящиков:</strong><span>${_data[i].kitchen2[0].params[3].value}</span></li>
											<li><strong>Цвет нижних ящиков:</strong><span>${_data[i].kitchen2[0].params[4].value}</span></li>
											<li><strong>Каркас:</strong><span>${_data[i].kitchen2[0].params[5].value}</span></li>
											<li><strong>Фарнитура:</strong><span>${_data[i].kitchen2[0].params[6].value}</span></li>
											<li><strong>Столешница:</strong><span>${_data[i].kitchen2[0].params[7].value}</span></li>
											<li><strong>Цоколь:</strong><span>${_data[i].kitchen2[0].params[8].value}</span></li>
											<li><strong>Форм-фактор:</strong><span>${_data[i].kitchen2[0].params[9].value}</span></li>
											<li><strong>Срок производства:</strong><span>${_data[i].kitchen2[0].params[10].value}</span></li>
											<li><strong>Бренд:</strong><span>${_data[i].kitchen2[0].params[11].value}</span></li>
											<li><strong>Страна:</strong><span>${_data[i].kitchen2[0].params[12].value}</span></li>
										</ul>
									</div>
									<div class="form__group">
										<a href="#modal-call" data-link="modal" class="btn btn--form">Забронировать</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			`;
			$('#modal-house').html(modal);
			initMaskedInput();
			break;
		}
	}
	initSliders();
}

function initTabs() {
	$('.modal').on('click', '[data-item="tab"]', function () {
		var link = $(this).attr('href');

		$(this).addClass('active').siblings().removeClass('active');
		$(link).addClass('active').siblings().removeClass('active');
	})
}

function sendForm(form) {
	$.ajax(
		'/include/request_handler.php', {
			method: 'POST',
			async: true,
			data: $(form).serialize(),
			success: function success(data) {
				//console.log(data);
				yaCounter937330.reachGoal($(form).data('yagoal'));
				$('.modal.active').removeClass('active');
				$('#modal-thx').addClass('active');
			}
		}
	);
}
function sendForm(form) 
{
    $.ajax(
            '/include/request_handler.php', {
            method: 'POST',
            async: true,
            data:  $(form).serialize(),
            success: function success(data) {
                //console.log(data);
                yaCounter937330.reachGoal($(form).data('yagoal'));
            }
        } 
        );

    $('.modal.active').removeClass('active');
    $('#modal-thx').addClass('active');   

    return false;
}

function onLoad() {

	$(window).on('scroll', function() {
		for (var i = 0; i < anchors.length; i++) {
			if (isScrolledIntoView($('#' + anchors[i])[0])) {
				var currentSlide = $('#' + anchors[i]);
				if (currentSlide.attr('id') === 'hero') {
					$('.header').addClass('hero')
					$('.logo').addClass('hero')
				} else {
					$('.header').removeClass('hero')
					$('.logo').removeClass('hero')
				}
				break;
			}
		}
	});

	

	fetchData();
	initCountdown();
	initSmoothScrollOnClick();
	// initSmoothSlideScroll();
	initModals();
	initTabs();


	$('form.form').on('submit', function () {
		sendForm(this);
		return false;
	});

	initFancybox();
	initSliders();
	initMaskedInput();

}


$(document).ready(function () {
	onLoad();

})