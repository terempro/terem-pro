<?php 
	require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
	// require_once($_SERVER['DOCUMENT_ROOT'] . '/include/libmail.php');

	// константы
	const REQUESTS_IBLOCK = 113;

	$data = array();
	$props = array();	
	
	$data['date'] = date('d.m.Y H:i:s');
	$data['name'] = isset($_POST['fio']) ? htmlspecialchars($_POST['fio']) : '';	
	$data['phone'] = isset($_POST['phone']) ? htmlspecialchars($_POST['phone']) : '';	
	$data['sert'] = isset($_POST['sert']) ? htmlspecialchars($_POST['sert']) : '';	
	$data['email'] = isset($_POST['email']) ? htmlspecialchars($_POST['email']) : '';	
	
	$props["FIO"] = $data['name'];
	$props["PHONE"] = $data['phone'];
	$props['EMAIL'] = $data['email'];
	$props['SERTIFICAT_NUMBER'] = $data['sert'];
	
	// запись в инфоблок
	
	$i = new CIBlockElement;
	
	$fields = array(

		"IBLOCK_ID" => REQUESTS_IBLOCK,
		"NAME" => $data['name'].' '.$data['phone'],
		"ACTIVE" => "Y",
		"PROPERTY_VALUES" => $props

	);	
	
	$id = $i->Add($fields);
	
	// отправка почты

	$mailAddress = $data['email'];
	$mailSubject = 'Регистрация сертификата Теремъ';
	$mailText = '
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Ваш сертификат &laquo;Дом в подарок&raquo; зарегистрирован!</title>
</head>
<body style="color:#000;">
    <div style="background-image: url(https://www.terem-pro.ru/dom-v-podarok/dom-v-podarok.jpg);  height: 802px; width: 1140px; display: block; overflow: hidden;">
        <a href="https://www.terem-pro.ru" target="_blank" style="display: block; height:95px; width:105px; margin: 21px 0 0 79px;"></a>
        <div style="display:block; height:440px;"></div>
        <div style="display: block; height: 100px; width: 700px; float:left; margin: 0 0 0 70px;">
            <h3 style="font-size: 24px; font-family: Arial, sans-serif; font-weight: 500;">Поздравляем!</h3>
            <p style="font-size: 16px; font-family: Arial, sans-serif;">Ваш сертификат зарегистрирован, вы стали ближе к главному призу.<br>Следите за новостями на сайте <a target="_blank" href="https://www.terem-pro.ru/" style="color:#000;">www.terem-pro.ru</a> и в соц сетях.</p>
        </div>
        <div style="display: block; height: 40px;"></div>
        <div style="display: block; overflow: hidden; height: 70px; width: 200px; float:right; margin:0 60px 0 0;">
            <a target="_blank" href="https://www.facebook.com/terempro" style="display:block; float:left; width: 23px; height: 22px; margin:6px 0 0 22px;"></a>
            <a target="_blank" href="http://instagram.com/pro_terem" style="display:block; float:left; width: 23px; height: 22px; margin:6px 0 0 10px;"></a>
            <a target="_blank" href="https://twitter.com/ProTerem" style="display:block; float:left; width: 23px; height: 22px; margin:6px 0 0 9px;"></a>
            <a target="_blank" href="http://vk.com/pro_terem" style="display:block; float:left; width: 23px; height: 22px; margin:6px 0 0 8px;"></a>
            <br clear="all">
            <a target="_blank" href="http://terem-pro.livejournal.com" style="display:block; float:left; width: 23px; height: 22px; margin:12px 0 0 22px;"></a>
            <a target="_blank" href="http://ok.ru/v2010godu" style="display:block; float:left; width: 23px; height: 22px; margin:12px 0 0 7px;"></a>
            <a target="_blank" href="http://www.terem-pro.ru/sitemap.xml" style="display:block; float:left; width: 23px; height: 22px; margin:11px 0 0 8px;"></a>
            <a target="_blank" href="https://www.youtube.com/c/terempro" style="display:block; float:left; width: 26px; height: 26px; margin:8px 0 0 8px;"></a>
        </div>
        <br clear="both">
        <a target="_blank" href="https://www.terem-pro.ru" style=" display:block;font-size: 16px; font-family: Arial, sans-serif; clear: both; margin: 80px 0 0 80px; color:#000; text-decoration: none;">&copy;Теремъ-про, 2009 - 2018</a>
    </div>
</body>
</html>
	';
	
	
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

		$m = new Mail;
        $m->From("crm@terem-pro.ru");
        $m->To($mailAddress);
        $m->Subject($mailSubject);
        $m->Body($mailText, "html");
        $m->smtp_on("ssl://smtp.mastermail.ru", "crm@terem-pro.ru", "crmpassword", 465);
        $m->Send();			
		
		var_dump($m);


?>