<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Регистрация сертификата Дом в подарок");
$APPLICATION->SetPageProperty("keywords", "терем сертификат, терем регистрация сертификата, терем про дом в подарок");
$APPLICATION->SetTitle("Регистрация сертификата &laquo;Дом в подарок&raquo;");


	// получаем список номеров сертификатов

	$certdata = CIBlockElement::GetList(Array("PROPERTY_SERTIFICAT_NUMBER" => "ASC"), Array("IBLOCK_ID" => 113,), false, Array(), Array("PROPERTY_SERTIFICAT_NUMBER",));
	while ($ob = $certdata->GetNextElement()) {
		$data[] = $ob->GetFields();
	}	


// var_dump($data);

?>

    <link href="style.css" rel="stylesheet">
    <section class="content text-page white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="white padding-side + my-margin clearfix">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="my-text-title text-center text-uppercase desgin-h1">
                                    <h1>
                                        Регистрация сертификата &laquo;Дом в подарок&raquo;
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="row">
                                <img src="header_picture.jpg" class="img-responsive">
                            </div>
                        </div>
                        <script>
                                jQuery(function($) {
                                    //создания своего специального символа для маски
                                    $("#sert").mask("0999");
                                });
                            </script>
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="row" style="background-color:#e5e5e5; padding-top:30px; padding-bottom:30px; margin-bottom:30px;">
                                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                                    <div class="reg_form">
                                        <div class="reg_title">Регистрация сертификата</div>
                                        <p><span class="red">Внимание!</span> Для регистрации сертификата корректно введите данные: номер и ваше имя должны совпадать с указанными в сертификате.</p>
                                        <form data-form="send" id="reg_sertificat" class="form-send" method="post" action="func.php">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <input name="sert" id="sert" class="form-control my-input" placeholder="Код сертификата" required="required" pattern="0[0-9]{3}" type="text">
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <input name="fio" class="form-control my-input" placeholder="ФИО" required="required" type="text">
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <input name="phone" class="form-control my-input" placeholder="Ваш телефон" required="required" type="tel">
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <input name="email" class="form-control my-input" placeholder="Ваш email" required="required" type="email">
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <input type="hidden" name="request_type" value="">
                                                <button type="submit" name="btn" class="btn btn-danger" style="pointer-events: all; cursor: pointer; margin-top: 25px;">Отправить заявку на участие в акции</button>
                                                <p class="privacy"><small>Отправляя форму, Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных</a></small></p>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take01" aria-expanded="false" aria-controls="take01">
                                                <div class="my-table">
                                                    <div>
                                                        <strong>Что даёт сертификат?</strong>
                                                    </div>
                                                    <div class="my-appearance-middle">
                                                        <i class="glyphicon glyphicon-menu-up"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="collapse my-collapse" id="take01">
                                                <div class="( my-collapse-content + design-main )">
                                                    <div class="row">
                                                        <p>Сертификат даёт право участвовать в акции «Дом в подарок». Номер сертификата является вашим уникальным номером участника. Не забудьте его зарегистрировать на нашем сайте и взять с собой на розыгрыш главного приза 16 декабря 2018 года. Владелец сертификата может принимать участие в розыгрыше только при наличии акта приёмки-передачи строения, подтверждающего окончание строительства.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take02" aria-expanded="false" aria-controls="take02">
                                                <div class="my-table">
                                                    <div>
                                                        <strong>Где получить сертификат?</strong>
                                                    </div>
                                                    <div class="my-appearance-middle">
                                                        <i class="glyphicon glyphicon-menu-up"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="collapse my-collapse" id="take02">
                                                <div class="( my-collapse-content + design-main )">
                                                    <div class="row">
                                                        <p>Сертификат выдаётся всем клиентам строительной компании «Теремъ» во время подписания договора на строительство дома или бани на территории выставочного комплекса компании по адресу: г. Москва, ул. Зеленодольская, вл. 42. Сертификаты выдаются в период с 6 по 19 сентября 2018 года включительно.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take03" aria-expanded="false" aria-controls="take03">
                                                <div class="my-table">
                                                    <div>
                                                        <strong>Зарегистрированные сертификаты</strong>
                                                    </div>
                                                    <div class="my-appearance-middle">
                                                        <i class="glyphicon glyphicon-menu-up"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <div class="collapse my-collapse" id="take03">
                                                <div class="( my-collapse-content + design-main )">
                                                    <div class="row" style="padding: 0 10px 20px 10px;">
														<div class="col-md-2 col-sm-3 col-xs-6 text-center">
                                                        <?php 
															$i = 0;
															foreach ($data as $s) {
																switch ($i) {																		
																	case 20:	
																		if (!in_array($s["PROPERTY_SERTIFICAT_NUMBER_VALUE"], $doubles) ) {	 
																			$doubles[] = $s["PROPERTY_SERTIFICAT_NUMBER_VALUE"];
																			
																			if ($s["PROPERTY_SERTIFICAT_NUMBER_VALUE"] > 0)  {
																				if ($s["PROPERTY_SERTIFICAT_NUMBER_VALUE"] < 301) 
																					echo $s["PROPERTY_SERTIFICAT_NUMBER_VALUE"].'<br />';																	
																			}	
																		}																																		
																		$i = 0;																	
																		echo '</div>
																			  <div class="col-md-2 col-sm-3 col-xs-6 text-center">';	
																	break;
																	default: 
																		if (!in_array($s["PROPERTY_SERTIFICAT_NUMBER_VALUE"], $doubles) ) {	 
																			$doubles[] = $s["PROPERTY_SERTIFICAT_NUMBER_VALUE"];
																			
																			if ($s["PROPERTY_SERTIFICAT_NUMBER_VALUE"] > 0)  {
																				if ($s["PROPERTY_SERTIFICAT_NUMBER_VALUE"] < 301) 
																					echo $s["PROPERTY_SERTIFICAT_NUMBER_VALUE"].'<br />';																	
																			}	
																		}
																		$i++;																	
																	break;																	
																}

															}																													
														?>
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>							
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                            <a class="pravila" target="_blank" href="/pravila/dom-v-podarok.pdf">
                                                <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take04" aria-expanded="false" aria-controls="take04">
                                                    <div class="my-table">
                                                        <div>
                                                            <strong>Правила акции</strong>
                                                        </div>
                                                        <div class="my-appearance-middle">

                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <h2 class="text-uppercase text-quest">Если остались вопросы по участию в акции</h2>
                            <p style="margin-bottom:50px;">Оставьте телефонный номер, и наши специалисты свяжутся с Вами через 30 секунд.</p>
                            <form class="form-send" data-form="send" method="post" action="">
                                <div class="row">
                                    <div class="col-xs-12 col-md-5 col-sm-6">
                                        <div class="col-xs-12">
                                            <input name="name" class="form-control my-input" placeholder="Ваше имя" required="required" type="text">
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3 col-sm-6">
                                        <div class="col-xs-12">
                                            <input name="phone" class="form-control my-input" placeholder="Ваш телефон" required="required" type="tel">
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-12">
                                        <input type="hidden" name="request_type" value="">
                                        <button type="submit" name="btn" class="btn btn-danger btn-quest text-uppercase" style="pointer-events: all; cursor: pointer;">Заявка на консультацию</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left checkbox-privacy">
                                        <input type="checkbox" name="privacy-policy_check" /> &nbsp;Я принимаю условия <a href="/privacy-policy/">Политики обработки данных</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>