<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "правила строительства терем");
$APPLICATION->SetPageProperty("title", "Полезный раздел | Правила строительства и заказа домов");
$APPLICATION->SetTitle("Полезный раздел | Правила строительства и заказа домов");
?>

	<section class="content text-page white" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="my-text-title text-center text-uppercase desgin-h1">
									<h1>
										ИНФОРМАЦИЯ ПО ОРГАНИЗАЦИИ<br>СТРОИТЕЛЬСТВА В КОМПАНИИ «ТЕРЕМЪ»
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Клиентов компании «Теремъ» очень часто интересуют различные нюансы, касающиеся организации строительного процесса. Мы собрали для Вас всю необходимую информацию, начиная от момента заключения договора, заканчивая сдачей готового объекта, гарантией и работой службы рекламации.
								</p>
								<br>
								<br>
							</div>
						</div>
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
											<div class="my-table">
												<div>
													<p>Формирование цены. Оплата</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq1">
											<div class="( my-collapse-content + design-main )">
												<p>20% при заключении договора</p>
												<p>60% после возведения фундамента</p>
												<p>20% после окончания строительства и подписания акта приёма-передачи объекта</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->

						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
											<div class="my-table">
												<div>
													<p>Доставка и сборка дома</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq2">
											<div class="( my-collapse-content + design-main )">
												<p>Доставка в радиусе 100 км от города Сокол Вологодской области бесплатно, далее рассчитывается индивидуально. Максимальный радиус доставки ‒ до 700 км. </p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq3">
											<div class="my-table">
												<div>
													<p>Кредитование</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq3">
											<div class="( my-collapse-content + design-main )">
												<p>На территории выставочного комплекса можно оформить заявку на ипотеку и подать документы на кредит, рассрочку.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="false" aria-controls="faq4">
											<div class="my-table">
												<div>
													<p>Заключение договора</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq4">
											<div class="( my-collapse-content + design-main )">
												<p>Заключение договора начинается с обозначения клиентом характеристик своего участка. Вам будет необходимо указать:</p>
												<ul>	
													<li><p>район застройки;</p></li>
													<li><p>место строительства;</p></li>
													<li><p>площадь участка;</p></li>
													<li><p>состояние подъезда к участку;</p></li>
													<li><p>тип грунта;</p></li>
													<li><p>необходимость оформления пропуска для проезда автотранспорта;</p></li>
													<li><p>рельеф местности, наличие других строений, деревьев, пеньков;</p></li>
													<li><p>наличие водоснабжения и электроэнергии на участке;</p></li>
													<li><p>наличие жилого помещения для размещения бригады.</p></li>
												</ul>
												<p>
												Отметим, что монтаж фундамента может осуществляться по отдельному договору.
												</p>
												<p>
												При заключении договора также в обязательном порядке необходимо предъявить паспорт и документ, удостоверяющий право собственности на землю.
												</p>
												<p>
												Если у вас нет своего земельного участка, в нашей компании вам могут помочь с выбором и приобретением земли.
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq5" aria-expanded="false" aria-controls="faq5">
											<div class="my-table">
												<div>
													<p>Работа строительной бригады</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq5">
											<div class="( my-collapse-content + design-main )">
												<p>Средний срок строительства каркасного дома может достигать 45 дней. Всё это время рабочие проживают на участке заказчика, который должен разместить их в пригодное для жизни помещение. Если такой возможности нет, то рабочие привезут с собой бытовку.</p>
												<p>
												Клиент не оплачивает проживание бригады!
												</p>
												<p>
												Дата начала работ обязательно обговаривается заранее.
												</p>
												<p>
												Рабочий день строителей может быть ненормированным из-за погодных условий, но, как показывает практика, наши рабочие укладываются в обозначенный срок строительства.
												</p>
												<p>
												За 3-4 дня до назначенной даты начала строительства заказчику необходимо созвониться с диспетчером и договориться о месте и времени встречи с бригадой.
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
						<!--one item-->
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq8" aria-expanded="false" aria-controls="faq8">
											<div class="my-table">
												<div>
													<p>Гарантия качества</p>
												</div>
												<div>
													<i class="glyphicon glyphicon-chevron-up"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<div class="collapse my-collapse" id="faq8">
											<div class="( my-collapse-content + design-main )">
												<p>На всех этапах строительства дома заказчик имеет право осуществлять соответствующий контроль. После окончания строительства клиент осматривает готовое строение и подписывает акт приёмки.
												</p>
												<p>
												В случае обнаружения недостатков заказчик может обратиться с заявлением в компанию «Вологодский Терем». При подтверждении наличия дефектов компания устраняет их бесплатно.
												</p>
												<p>
												На дома из бруса гарантия составляет до 25 лет.
												</p>
												<p>
												На каркасные дома гарантия составляет до 15 лет.
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--one item END-->
							
					</div>
				</div>
			</div>
			
		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>