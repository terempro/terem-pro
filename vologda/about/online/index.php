<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Онлайн трансляция строительства дом «Лидер 9» ВС К-200");
$APPLICATION->SetPageProperty("keywords", "строительство дома");
$APPLICATION->SetPageProperty("title", "Онлайн трансляция строительства дом «Лидер 9» ВС К-200");
$APPLICATION->SetTitle("");
?>
<style>
.text-item h2 {
  font-size: 20px;
  color:#020202;
  margin: 0;
  font-weight: bold;
}
.text-item grey {
  color: #8f8f8f;
}
.text-item grey p {
  color: #8f8f8f;
  line-height:1 !important;
  margin-top: 34px;
}
.text-item {
  margin-top: 10px;
  margin-bottom: 15px;
}
.text-item small {
  font-size: 12px;
}
.text-item.active h2,
.text-item.active p,
.text-item.active grey
{color: #ff0000;}
.text-item p {
  font-size: 14px;
  color:#020202;
  margin: 0;
}
</style>
<section class="content white catalog-item-1 content-catalog-item active" data-item="1">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="catalog-item-gallery white + my-margin">
          <div role="tabpanel" class="tab-panel ( my-tabpanel &amp;&amp; ( design-1 &amp;&amp; appearance-list-left &amp;&amp; bg-grey &amp;&amp; padding-20 ))">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Камера 1</a></li>
              <!-- <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Камера 2</a></li>
              <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Камера 3</a></li> -->
            </ul><!-- Nav tabs END-->
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="tab1">
                <div class="row">
                  <div class="col-xs-12 col-md-9 col-sm-9">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-10 col-sm-10">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase">
                            <span style="color: red">На видео</span> «Лидер 9», 6<span style="text-transform: lowercase;">х</span>9 <span style="text-transform: lowercase;">м</span> c пристроенными террасой и балконом
                          </span>
                          <br>
                          <span style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px">Площадь застройки: 61,71 кв.м; общая площадь: 104,42 кв.м; жилая площадь: 54,45 кв.м</span>
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <iframe width="100%" height="554" src="https://ru.cloud.trassir.com/embed/cIdrFxfjeoqmftmk?lang=en" frameborder="0" allowfullscreen></iframe>
                    <!-- <iframe width="100%" height="554" frameborder="0" seamless="seamless" src="http://ipeye.ru/ipeye_service/api/api.php?dev=s458LKQoHnaREnwZrU6ZyjHpFiwPyG&tupe=rtmp&autoplay=0&logo=1">Ваш браузер не поддерживает фреймы!</iframe> -->
                  </div>
                </div>
              </div>

              <!-- <div role="tabpanel" class="tab-pane" id="tab2">
                <div class="row">
                  <div class="col-xs-12 col-md-9 col-sm-9">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-10 col-sm-10">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase"><span style="text-transform: lowercase;">дом</span> «Боярин» 1 К-200, 8.5<span style="text-transform: lowercase;">х</span>10.5 <span style="text-transform: lowercase;">м</span></span>.  Общая площадь: 149,34 кв.м
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <iframe width="100%" height="554" frameborder="0" seamless="seamless" src="http://ipeye.ru/ipeye_service/api/api.php?dev=dpMoCCIOX2Vva4lGGbaVfm2Dr3oXM3&tupe=rtmp&autoplay=0">Ваш браузер не поддерживает фреймы!</iframe>
                  </div>
                </div>
              </div>

              <div role="tabpanel" class="tab-pane" id="tab3">
                <div class="row">
                  <div class="col-xs-12 col-md-9 col-sm-9">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-10 col-sm-10">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase"><span style="text-transform: lowercase;">дом</span> «Боярин» 4 ВС К-200, 10,5<span style="text-transform: lowercase;">х</span>12,5 <span style="text-transform: lowercase;">м</span></span>. Общая площадь: 198,53 кв.м
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <iframe width="100%" height="554" frameborder="0" seamless="seamless" src="http://ipeye.ru/ipeye_service/api/api.php?dev=qtZzTvWUM9XdHFgfpLGf2QjmOJblG7&tupe=rtmp&autoplay=0&logo=1">Ваш браузер не поддерживает фреймы!</iframe>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>







<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
