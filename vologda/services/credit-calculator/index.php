<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "дома в кредит, кредит на строительство дома");
$APPLICATION->SetPageProperty("title", "Компания ТеремЪ предоставляет услуги кредитования населению на выгодных условиях");
$APPLICATION->SetTitle("Компания ТеремЪ предоставляет услуги кредитования населению на выгодных условиях ");
?><section class="content white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix + my-margin">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h1>Услуги кредитования и страхования</h1>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>Совершая крупную покупку в кредит, все мы сталкиваемся с необходимостью обращаться в несколько банков и, бесконечно заполняя анкеты, беспокоимся об условиях и одобрении заявки.
							</p>
							<p>Каждый клиент может получить консультацию и оформить заявку в офисе продаж.
							</p>
                            <p>Каждый клиент может получить персонального менеджера «Теремъ-финанс», который возьмет на себя все Ваши заботы:</p>
                        </div>
                    </div>
                    <br>

                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-border ) collapsed green" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
                                        <div class="my-table">
                                            <div>
                                                <p class="text-uppercase">страховые риски</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq2">
                                        <div class="( my-collapse-content + design-border + no-padding green )">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-6 col-sm-6">
                                                    <ul class="credit-list-2">
                                                        <li>
                                                            <p>Кража, грабеж, разбой</p>
                                                        </li>
                                                        <li>
                                                            <p>Взрыв газа, используемого в бытовых целях</p>
                                                        </li>
                                                        <li>
                                                            <p>Падение летательных аппаратов, их обломков, частей или груза</p>
                                                        </li>
                                                        <li>
                                                            <p>Наезд транспортных средств, не принадлежащих Вам</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-6 col-sm-6">
                                                    <ul class="credit-list-2">
                                                        <li>
                                                            <p>Пожар, удар молнии</p>
                                                        </li>
                                                        <li>
                                                            <p>Стихийные бедствия</p>
                                                        </li>
                                                        <li>
                                                            <p>Авария водопроводных, отопительных, канализационных систем</p>
                                                        </li>
                                                        <li>
                                                            <p>Противоправные действия третьих лиц</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--one item END-->
                    <br>

                </div>
            </div>
        </div>
    </div>
</section>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>