<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает услуги по прокладке и проектированию внутренних инженерных систем");
$APPLICATION->SetPageProperty("keywords", "внутренние инженерные системы, прокладка внутренних инженерных систем, проектирование внутренних инженерных систем");
$APPLICATION->SetPageProperty("title", "Внутренние инженерные системы: проектирование, монтаж и прокладка");
$APPLICATION->SetTitle("Внутренние инженерные системы: проектирование, монтаж и прокладка");
?>
<section class="content text-page white + my-margin" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side clearfix">
					
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<div class="text-center text-uppercase">
								<h1>
									Внутренние инженерные системы
								</h1>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p>Компания «Вологодский Терем» ‒ официальный партнёр компании «Теремъ» ‒ предлагает своим клиентам возможность устройства системы горячего и холодного водоснабжения, системы канализации в базовой комплектации с разводкой открытым способом по стенам полипропиленовых трубопроводов и установкой согласованного с заказчиком технического оборудования.</p>
						</div>
					</div>
					<br>
					
					<div class="row">
						<div class="col-xs-12 col-md-2 col-sm-2">
							<img src="/bitrix/templates/.default/assets/img/services/item-13.jpg" class="full-width" alt="">
						</div>
						<div class="col-xs-12 col-md-10 col-sm-10">
							<div class="my-item-design-4 border-item arrow-left padding">
								<h3>Гидроаккумуляторы</h3>
								<p>Главное предназначение гидроаккумулятора – это поддержание на одном уровне давление воды, а также ослабление мощности гидроударов, которые могут нанести значительные механические повреждения в протяженных трубопроводах. Чем больше литраж гидроаккумулятора, тем меньше изнашивается насос. Мы предлагаем гидроаккумуляторы объемом от 50 до 100 литров. </p>
							</div>
						</div>
					</div>
					<br>
					<br>
					<div class="row">
						
						<div class="col-xs-12 col-md-10 col-sm-10">
							<div class="my-item-design-4 border-item arrow-right padding">
								<h3>Фильтр очистки воды</h3>
								<p>Компания «Вологодский Терем» ‒ официальный партнёр компании «Теремъ» - предлагает фильтры очистки воды типа «Гейзер» или «BIG BLUE 10», которые предназначены для качественной очистки воды. За счет большого объема данных фильтров проходимость воды будет больше. </p>
							</div>
						</div>
						<div class="col-xs-12 col-md-2 col-sm-2">
							<img src="/bitrix/templates/.default/assets/img/services/item-14.jpg" class="full-width" alt="">
						</div>
					</div>
					<br>
					<br>
					<div class="row">
						<div class="col-xs-12 col-md-2 col-sm-2">
							<img src="/bitrix/templates/.default/assets/img/services/item-15.jpg" class="full-width" alt="">
						</div>
						<div class="col-xs-12 col-md-10 col-sm-10">
							<div class="my-item-design-4 border-item arrow-left padding">
								<h3>Водонагреватели</h3>
								<p>Мы предлагаем водонагреватели от фирмы «Термекс» и «Electrolux» объемом от 30 до 100 литров. Электрический накопительный водонагреватель нагревает воду до 70℃, после чего поддерживает температуру в автоматическом режиме. Поскольку нагрев происходит постепенно, такой аппарат не требует больших электрических затрат. Горячая вода может расходоваться несколькими водоразборными точками одновременно, то есть Вы можете, к примеру, принимать душ и мыть посуду на кухне одновременно.</p>
							</div>
						</div>
					</div>
					<br>
					<br>
					<div class="row">
						
						<div class="col-xs-12 col-md-10 col-sm-10">
							<div class="my-item-design-4 border-item arrow-right padding">
								<h3>Сантехника</h3>
								<p>Вы можете заказать установку сантехники фирмы «Cersanit». Наши специалисты привезут все необходимое для комфортной жизни оборудование и качественно установят его. </p>
							</div>
						</div>
						<div class="col-xs-12 col-md-2 col-sm-2">
							<img src="/bitrix/templates/.default/assets/img/services/item-16.jpg" class="full-width" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>