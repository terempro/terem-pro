<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает электромонтажные работы на вашем дачном учатске или в частном доме.");
$APPLICATION->SetPageProperty("keywords", "электромонтажные работы на даче, электромонтажные работы на дачном участке");
$APPLICATION->SetPageProperty("title", "Электромонтажные работы на даче: расценки и стоимость");
$APPLICATION->SetTitle("Электромонтажные работы на даче: расценки и стоимость");
?>
<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="text-center text-uppercase">
									<h1>
										Электромонтажные работы
									</h1>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>От профессионализма электриков напрямую зависят надёжность работы и долговечность техники в доме. Точность электромонтажных работ определяет степень безопасности всего здания. Заказывайте выполнение электромонтажных работ у профессионалов компании «Вологодский Терем» – официального партнёра компании «Теремъ»!</p>
								<img src="/bitrix/templates/.default/assets/img/services/enginering_communications/electromontaj/item-1.jpg" class="full-width" alt="">
								<p>Монтаж типовых электропроводок выполняется открыто в кабель-каналах. Возможны варианты проводок в электроплинтусе или скрыто на этапе строительства. Электрики компании «Вологодский Терем» обладают необходимыми навыками и обустроят скрытую проводку в деревянном доме с максимальной надёжностью.</p>
								<p>Компания «Вологодский Терем» предлагает готовые проекты электроснабжения, которые соответствуют нормам и правилам прокладки электрических сетей. Электромонтажные работы выполняются по готовым расчётам, что экономит вложения и время. Монтажники сразу начинают работу, разработка отдельного проекта зачастую не требуется. Доступна электропроводка из стандартного ряда мощностей 3, 5, 8, 10, 12, 15 кВт или индивидуальное проектирование по условиям конкретного дома.</p>
								<img src="/bitrix/templates/.default/assets/img/services/enginering_communications/electromontaj/item-2.jpg" class="full-width" alt="">
								<img src="/bitrix/templates/.default/assets/img/services/enginering_communications/electromontaj/item-3.jpg" class="full-width" alt="">
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>