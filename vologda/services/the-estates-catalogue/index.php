<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "благоустройство участка,благоустройство усадьбы");
$APPLICATION->SetPageProperty("description", "Благоустройство участка и усадьбы - разработка спецальных проектов на заказ");
$APPLICATION->SetTitle("Благоустройство участка");
?>
<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="img-wrapp-offset">
									<img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-1.jpg" class="full-width" alt="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="text-center text-uppercase">
									<h1>
										Благоустройство участка
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>Комфортная загородная жизнь начинается с надёжного и уютного дома, но не ограничивается только этим. После окончания строительства многие наши клиенты начинают заниматься уже участком. Кто-то сажает газон, кто-то ставит забор. Но зачастую все эти работы не доводятся до конца – нет столько времени, сил и опыта.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p><strong>Специалисты компании «Вологодский Терем» – официального партнёра компании «Теремъ» – возьмут благоустройство и озеленение вашего участка на себя!</strong></p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>Каждому хочется жить в прекрасном райском уголке. Наша компания создаст такой уголок специально для вас. Благоустройство территории – это не просто высадка нескольких кустарников, это целый комплекс разноплановых работ. В нашей компании работают опытные и талантливые дизайнеры и инженеры. Наши специалисты внимательно выслушают все ваши пожелания и разработают для вас проект благоустройства и озеленения участка.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p><strong>Вы сможете заказать уютную баньку неподалёку от дома, надёжный гараж, беседку-барбекю для отдыха с семьёй и друзьями. Не обойтись и без хорошего забора, наружной и внутренней отделки дома, всех необходимых для жизни инженерных коммуникаций и, конечно, ландшафтного дизайна.</strong></p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>На этапе проектирования учитываются все пожелания заказчика, рождается идея, определяется стиль, территория делится на зоны и т. д. Это очень кропотливая и сложная работа, но результат превосходит все ожидания. Уже на втором этапе начинаются сами работы на участке заказчика. Здесь важны точность и аккуратность воплощения задумки. Вот почему так важно, чтобы над проектом работали исключительно опытные и знающие люди.</p>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-8.jpg" class="full-width" alt="">
							</div>
						</div>
						<br>
						<br>
						<div class="row my-flex">
							<div class="col-xs-12 col-md-3 col-sm-3 my-flex">
								<div class="my-service-item hover my-flex full-width min-height-2 + my-margin">
									<div>
										<a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-2.jpg"></a>
									</div>
									<div>
										<div class="inner text-uppercase">
											<h2><a href="javascript:void(0);">ОГРАЖДЕНИЯ</a></h2>
											<br>
										</div>
										<div class="inner">
											<p>Безопасность Ваших границ</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-3 col-sm-3 my-flex">
								<div class="my-service-item hover my-flex full-width + my-margin">
									<div>
										<a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-3.jpg"></a>
									</div>
									<div>
										<div class="inner text-uppercase">
											<h2><a href="javascript:void(0);">КОММУНИКАЦИИ</a></h2>
											<br>
										</div>
										<div class="inner">
											<p>Когда в доме можно жить без проблем</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-3 col-sm-3 my-flex">
								<div class="my-service-item hover my-flex full-width + my-margin">
									<div>
										<a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-4.jpg"></a>
									</div>
									<div>
										<div class="inner text-uppercase">
											<h2><a href="javascript:void(0);">ЛАНДШАФТ</a></h2>
											<br>
										</div>
										<div class="inner">
											<p>Индивидуальный стиль и неповторимость участка</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-3 col-sm-3 my-flex">
								<div class="my-service-item hover my-flex full-width + my-margin">
									<div>
										
											<a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-5.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="javascript:void(0);">ВНУТРЕННЯЯ ОТДЕЛКА</a></h2>
												<br>
											</div>
											<div class="inner">
												
											</div>
										
									</div>
								</div>
							</div>
						</div>
						<div class="row my-flex">
							<div class="col-xs-12 col-md-3 col-sm-3 my-flex">
								<div class="my-service-item hover my-flex full-width + my-margin">
									<div>
										
										<a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-6.jpg"></a>
										
									</div>
									<div>
										
										<div class="inner text-uppercase">
											<h2><a href="javascript:void(0);">ВНЕШНЯЯ ОТДЕЛКА</a></h2>
											<br>
										</div>
										<div class="inner">
											<p>Уникальный облик Вашего дома</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-3 col-sm-3 my-flex">
								<div class="my-service-item hover my-flex full-width + my-margin">
									<div>
										
											<a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-7.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="javascript:void(0);">БАНЯ</a></h2>
												<br>
											</div>
											<div class="inner">
												<p>Все проекты бань</p>
											</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-3 col-sm-3 my-flex">
								<div class="my-service-item  hover my-flex full-width + my-margin">
									<div>
										
											<a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-9.jpg"></a>
									
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="javascript:void(0);">ГАРАЖ</a></h2>
												<br>
											</div>
											<div class="inner">
												<p>Ваш автомобиль в безопасности</p>
											</div>
									</div>
									
								</div>
							</div>
							<div class="col-xs-12 col-md-3 col-sm-3 my-flex">
								<div class="my-service-item hover my-flex full-width + my-margin">
									<div>
										<a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-10.jpg"></a>
									</div>
									<div>
										<div class="inner text-uppercase">
											<h2><a href="javascript:void(0);">БАРБЕКЮ</a></h2>
											<br>
										</div>
										<div class="inner">
											<p>Отдых на свежем воздухе</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
						<!-- div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 grey">
								<p class="grey"><i>А для тех, кто хочет построить дом, жить и радоваться, не думая о том, что впереди еще столько нужно достроить и доделать, мы предлагаем услугу «Усадьбу «под ключ» – Вы сможете заказать все и сразу, начиная от дома, заканчивая дорожками, инженерными коммуникациями, ландшафтом и т.д.	</i></p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="text-center text-uppercase">
									<h2>Усадьба «под ключ» – это решение для тех, кто хочет <br>снять с себя лишние заботы и переехать жить в дом своей мечты!</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>Если вы решили, что усадьба «под ключ» – это идеальный вариант для вас и вашей семьи, то смело отправляйте заявку или приходите к нам на выставочную площадку в Кузьминках. Вас проконсультируют лучшие специалисты нашей компании, которые помогут во всем разобраться и подскажут, как сделать правильный выбор!</p>
							</div>
						</div -->
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<!-- <section class="content" role="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="flex-reversed">
						<div class="row">
							<div class="col-xs-12 col-md-6 col-sm-12">
								<div class="my-promotion desgin-3 + my-margin">
									<div>
										<a href="#"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-11.jpg"></a>
									</div>
									<div>
										<h4 class="text-uppercase">
											Пакетные предложения
										</h4>
										<p>
											В этом разделе представлены проекты, созданные компанией «Теремъ». Вы можете выбрать для себя любой из понравившихся вариантов.
										</p>
									</div>
									<a class="all-item-link" href="http://t.zolotarev-studio.ru/services/the-estates-catalogue/packages"></a>
								</div>
							</div>
							<div class="col-xs-12 col-md-6 col-sm-12">
								<div class="my-promotion desgin-3 + my-margin">
									<div>
										<a href="#"><img src="/bitrix/templates/.default/assets/img/services/the-estates-catalogue/item-12.jpg"></a>
									</div>
									<div>
										<h4 class="text-uppercase">
											Собери свою<br>усадьбу
										</h4>
										<p>
											Какой будет ваша усадьба, решаете только вы! Выбрать дом и баню, забор и коммуникации, ландшафт и дизайн можно в этом разделе.
										</p>
									</div>
									<a class="all-item-link" href="http://t.zolotarev-studio.ru/services/the-estates-catalogue/designer"></a>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
	</section> -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>