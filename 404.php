<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 Not Found");

?>
<section class="content text-page white" role="content">
<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="text-center text-uppercase">
									<h1>
                                                                            Страница не найдена 
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Если вы ввели адрес вручную в адресной строке браузера, проверьте, всё ли вы написали правильно. 
								</p>
								<p>
                                                                    Если вы пришли по ссылке с другого ресурса, попробуйте перейти на <a href="/">главную страницу</a> или в <a href="/catalog/">каталог домов</a>, вполне вероятно, что там вы найдёте нужный вам материал.
								</p>
								<br>
								<br>
							</div>
						</div>
						
						
						
						
						
					</div>
				</div>
			</div>
			
		</div>
</section>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>