<?php
function d($path){
    $t = explode('/',$path);
    unset($t[count($t)-1]);
    return implode('/', $t);
}
//var_dump(d(__FILE__));
//exit;

//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require(d(__FILE__)."/../bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Прайс лист компании \"ТеремЪ\".");
$APPLICATION->SetPageProperty("keywords", "прайс лист,цена деревянные дома");
$APPLICATION->SetPageProperty("title", "Прайс лист компании \"ТеремЪ\". Всегда свежие цены и актуальная информация");
$APPLICATION->SetTitle("Прайс лист компании \"ТеремЪ\". Всегда свежие цены и актуальная информация");
?>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                   Прайс-лист компании «Теремъ»
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                                Цены действительны на дату отгрузки фундамента. Цены включают стоимость доставки дома в радиусе до 100 км (250 км на все дома серии «Малыш», беседки и бани размером до 6х6 м с мансардой включительно) от нашей производственной базы в г. Бронницы Московской области, а так же сборку и проживание бригады. Цены указаны на базовую комплектацию, подробно с ней ознакомиться можно в карточке дома. 
                            </p>
 <!--<p>*Цены действительны до 12.04.17</p>-->
                            <p>
                               <br>Все подробности по телефону: 8 (495) 721-18-00
                            </p>
                            <br>
                            <br>
                        </div>
                    </div>


                    <?php
                    //Собираем ID и NAME категорий
                    $IBLOCK_ID = 22;

                    $arFilter = Array('IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y');
                    $Sections = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, true);
                    $Categoryes = Array();
                    $cnt = 0;
                    while ($Section = $Sections->Fetch()) {
                        $Categoryes[$cnt]['ID'] = $Section['ID'];
                        $Categoryes[$cnt]['NAME'] = $Section['NAME'];
                        $cnt++;
                    }
                    //Собираем все дома в массив с категориями
                    foreach ($Categoryes as $key => $Ctaegory) {
                        $arSelect = Array("ID", "NAME", "PROPERTY_LINK", "PROPERTY_TECHNOLOGY");
                        $arFilter = Array("IBLOCK_ID" => 22, "SECTION_ID" => $Ctaegory["ID"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
                        $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize" => 500), $arSelect);
                        while ($ob = $res->Fetch()) {
                            $Categoryes[$key]["HOUSES"][] = $ob;
                        }
                    }





                    //Собираем цены к домам в категориях
                    foreach ($Categoryes as $k => $v) {
                        foreach ($v["HOUSES"] as $id => $house) {
                            $res = CIBlockElement::GetProperty(22, $house["ID"], "sort", "asc", array());
                            while ($ob = $res->GetNext()) {
                                $Categoryes[$k]["HOUSES"][$id]['PRICES'][] = $ob['VALUE'];
                            }
                        }
                    }
                    
                   
                    ?>


                    <?php $cnt = 1;?>
                    <?php foreach ($Categoryes as $key => $cat): ?>   
                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="( my-collapse-btn + design-bordered ) collapsed" type="button" data-toggle="collapse" data-target="#price<?=$cnt?>" aria-expanded="false" aria-controls="price<?=$cnt?>">
                                    <div class="my-table">
                                        <div>
                                            <p><?php echo $cat["NAME"]; ?></p>
                                        </div>
                                        <div>
                                            <i class="glyphicon glyphicon-chevron-up"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="collapse my-collapse" id="price<?=$cnt?>">
                                    <div class="( my-collapse-content + design-bordered-none )">
                                                <table class="table table-striped table-prices">
                                                    <?php if($Categoryes[$key]["HOUSES"][0]["PRICES"][2] != NULL):?>
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>
                                                                    <p>Цена базовой комплектации</p>
                                                                </th>
																  <!-- <th>
                                                                    <p>Зима</p>
                                                                </th> -->
                                                                <th>
                                                                    <p>Цена в мае 2017</p>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    <?php else:?>
                                                         <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>
                                                                    <p>Цена базовой комплектации</p>
                                                                </th>
                                                                <th>
                                                                    <p>Цена в мае 2017</p>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    <?php endif;?>
                                                    
                                                    <?php if($Categoryes[$key]["HOUSES"][0]["PRICES"][2] != NULL):?> 
                                                        <?php foreach ($cat["HOUSES"] as $house): ?> 
                                                        
                                                            <?php
                                                            //echo "<pre>";
                                                            //var_dump($house);
                                                            ?>
                                                            <tr>
                                                                <td><p><a href="<?php echo $house["PROPERTY_LINK_VALUE"]; ?>" data-tech="<?php echo $house["PROPERTY_TECHNOLOGY_VALUE"]; ?>" class="price-link-page"><?php echo $house["NAME"]; ?></a></p></td>
																<td ><p><?php echo number_format($house["PRICES"][7], 3, ' ', ' '); ?></p></td>
                                                                <!-- <td><p><?php echo number_format($house["PRICES"][0], 3, ' ', ' '); ?></p></td> -->
                                                                <td><p>
                                                                        
                                                                            <?php echo number_format($house["PRICES"][1], 3, ' ', ' '); ?>
                                                                        
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?> 
                                                    <?php else:?>
                                                        <?php foreach ($cat["HOUSES"] as $house): ?> 
                                                            <tr>
                                                                <td><p><a href="<?php echo $house["PROPERTY_LINK_VALUE"]; ?>" data-tech="<?php echo $house["PROPERTY_TECHNOLOGY_VALUE"]; ?>" class="price-link-page"><?php echo $house["NAME"]; ?></a></p></td>
                                                                <td><p><?php echo number_format($house["PRICES"][0], 3, ' ', ' '); ?></p></td>
                                                                <td><p>
                                                                        
                                                                            <?php echo number_format($house["PRICES"][1], 3, ' ', ' '); ?>
                                                                        
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?> 
                                                    <?php endif;?>    
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="5">
                                                                <button type="button" data-toggle="collapse" data-target="#price<?=$cnt?>" aria-expanded="false" aria-controls="price<?=$cnt?>">свернуть</button>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->
                        <? $cnt++; ?>
                    <?php endforeach; ?> 






                </div>
            </div>
        </div>


    </div>
</section>



<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>