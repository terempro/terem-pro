<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<section class="content white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix + my-margin">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>Архитектурное бюро</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row idividual-text">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p> <big>Вы можете создать собственный проект дома из бруса, каркаса или кирпича. Проектирование будет осуществляться с учетом всех Ваших пожеланий. Для внутренней и наружной отделки мы используем самые разные материалы. </big></p>
                            <ul>
                                <li>
                                    <p>Деревянная обшивка (вагонка, имитация бруса, блокхаус)</p>
                                </li>
                                <li>
                                    <p>Виниловая обшивка (различные виды сайдинга, фасадные панели VinyTherm)</p>
                                </li>
                                <li>
                                    <p>Обшивка гипсокартоном (производится внутри помещений по желанию заказчика)</p>
                                </li>
                                <li>
                                    <p>Наружная обшивка термопанелями с поверхностью из природного камня или кирпича различных цветов</p>
                                </li>
                                <li>
                                    <p>Кровельные материалы (металлочерепица, мягкая кровля различных цветов)</p>
                                </li>
                            </ul>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/expert/sep2.png?14501059151998" alt="arrow-left" class="full-width">
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 text-center col-sm-offset-1 col-md-offset-1">
                                    <p style="font-size:21px; color:#be2a23;line-height: 1.2;margin-bottom:0;">Наши архитекторы воплотят в жизнь любые Ваши идеи по проектированию дома и обустройству участка. Мы создадим для Вас настоящий дом мечты!</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <img src="//terem-pro.ru.opt-images.1c-bitrix-cdn.ru/bitrix/templates/.default/assets/img/expert/sep1.png?14501059152023" alt="arrow-right" class="full-width">
                                </div>
                            </div>
                            <br>
                            <p><big>Мы тщательно просчитываем все, начиная от выбора фундамента, материалов, размера, планировки, заканчивая инженерными коммуникациями и благоустройством участка.</big></p>
                            <br>
                            <button data-target="#call" data-toggle="modal" class="btn btn-default btn-call-from-buro"><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную консультацию</span></button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="content" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12" id="category">
                <div class="flex-reversed">
                    <!--OLD catalog list-->
                    <!--small villages -->
                    <?php
                    $cat_new = [];
                    if (CModule::IncludeModule("iblock")) {
                        $arFilter = Array('IBLOCK_ID' => 27);
                        $db_list = CIBlockSection::GetList(Array(), $arFilter, true);

                        while ($ar_result = $db_list->GetNext()) {
                            $cat_new[$ar_result['ID']] = $ar_result;
                        }
                        
                        echo "<pre>";
                        var_dump($cat_new);
                    }
                    ?>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-1.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        БАНИ
                                    </h4>
                                    <p>
                                        Баня по индивидуальному проекту? Не мечта, а реальность. Бильярдная или тренажёрный зал, комнаты отдыха и барбекю-зона на просторной террасе – всё это и многое другое можно включить в проект Вашей уникальной бани. Обратите внимание на готовые проекты, представленные в этом разделе.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-2.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ДОМА ИЗ БРУСА С ВЫПУСКАМИ

                                    </h4>
                                    <p>
                                        Наши архитекторы создают интересные проекты домов из бруса с выпусками. Такие дома смотрятся очень колоритно и презентабельно за счёт интересной технологии возведения, при которой концы бруса определённой длины выступают за плоскость стен.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                    </div><!--end-->
                    <!--vitaz and gardern-->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-3.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ПРОЕКТИРОВАНИЕ ДОМОВ <span>ДО 100 КВ.М.</span>

                                    </h4>
                                    <p>
                                        Проекты домов площадью до 100 кв. м порадуют инновационным подходом к планированию жилого пространства и оригинальным взглядом на архитектуру современного загородного особняка. Ознакомьтесь с нашими уникальными моделями домов по индивидуальным проектам.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-4.jpg" alt="Оригинальные дачные дома">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ПРОЕКТИРОВАНИЕ ДОМОВ <span>ДО 150 КВ.М.</span>

                                    </h4>
                                    <p>
                                        Большой и красивый дом по индивидуальному проекту – залог по-настоящему комфортной жизни за городом. Дом до 150 кв м. по индивидуальному проекту станет самым любимым и желанным местом отдыха для всей семьи. Сделайте выбор в пользу уюта и неповторимого качества
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                    </div><!--end-->

                    <!--post projivanie-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-promotion ( desgin-2 &amp;&amp; one-column ) + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/buro/item-5.jpg" alt="">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        ЭКСКЛЮЗИВНЫЕ ПРОЕКТЫ ПОД КЛЮЧ <span><br>ОТ 150 КВ.М.</span>

                                    </h4>
                                    <p>
                                        Пришло время претворить мечты о воздушных замках в реальность! Огромный дом по индивидуальному проекту – идеальный выбор для отдыха и семейной жизни за городом круглый год. Наши проекты порадуют даже самых искушённых приверженцев красивой жизни.
                                    </p>
                                </div>
                                <a class="all-item-link" href="#"></a>
                            </div>
                        </div>
                    </div><!--end-->

                </div>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>