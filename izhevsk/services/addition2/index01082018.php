<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пристройки террас и веранд");
?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h1>
                                    Пристройка террас и веранд
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                                Уютная терраса или грамотно спроектированная веранда помогут придать Вашему дому неповторимость и архитектурную завершенность. Пристройка к дому также поможет увеличить площадь основного строения. Такой вариант модернизации дома является оптимальным решением для преображения и благоустройства.
                            </p>
                            <p>
                                Компания «Теремъ» занимается проектированием и строительством любых видов пристроек. Сначала наши специалисты тщательно изучают конструкцию основного дома, начиная от фундамента и заканчивая крышей. Так формируется грамотный вариант пристройки, который подходит именно Вашему дому. Каждый дом уникален, и мы, понимая это, предлагаем подходящее решение, которое обязательно учитывает все пожелания клиентов. 
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <!--<ul class="my-list design-red">-->
                            <ul>
                                <li>
                                    <p>В качестве пристройки может выступать терраса, летняя веранда или помещение для расположения санузла, баня или сауна со всеми удобствами. </p>
                                </li>
                                <li>
                                    <p>Пристройки из дерева подходят ко всем типам домов.</p>
                                </li>
                                <li>
                                    <p>Опытные специалисты помогут Вам спроектировать и построить объект любой сложности.</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="my-service-item my-table full-width + my-margin">
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell">
                                        <a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/additional2/item-1.jpg"></a>
                                    </div>
                                </div>
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell my-appearance-top">
                                        <div class="inner text-uppercase">
                                            <h2><a href="javascript:void(0);">Террасы</a></h2>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="my-table-row">
                                    <div class="my-table-cell my-appearance-bottom">
                                        <div class="inner">
                                            <p>
                                                Открытая площадка, примыкающая к дому. Отлично подойдет для отдыха
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="my-service-item my-table full-width + my-margin">
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell">
                                        <a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/additional2/item-2.jpg"></a>
                                    </div>
                                </div>
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell my-appearance-top">
                                        <div class="inner text-uppercase">
                                            <h2><a href="javascript:void(0);">Веранды</a></h2>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="my-table-row">
                                    <div class="my-table-cell my-appearance-bottom">
                                        <div class="inner">
                                            <p>
                                                Закрытая пристройка к дому, идеально подходит для российского климата
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-4">
                            <div class="my-service-item my-table full-width + my-margin">
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell">
                                        <a href="javascript:void(0);"><img src="/bitrix/templates/.default/assets/img/services/additional2/item-3.jpg"></a>
                                    </div>
                                </div>
                                <div class="my-table-row min-height">
                                    <div class="my-table-cell my-appearance-top">
                                        <div class="inner text-uppercase">
                                            <h2><a href="javascript:void(0);">Террасы-веранды</a></h2>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="my-table-row">
                                    <div class="my-table-cell my-appearance-bottom">
                                        <div class="inner">
                                            <p>
                                                Пристройка, имеющая открытую (терраса) и закрытую (веранда) части
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                        <div class="my-table">
                                            <div>
                                                <p>Компания «Теремъ» предлагает широкий выбор современный материалов и технологий:</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq1">
                                        <div class="( my-collapse-content + design-main )">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Внутренняя отделка</h4>
                                                    <ul class="my-list design-additional">
                                                        <li>
                                                            <p>Вагонка</p>
                                                        </li>
                                                        <li>
                                                            <p>Гипсокартон</p>
                                                        </li>
                                                        <li>
                                                            <p>Ламинат</p>
                                                        </li>
                                                        <li>
                                                            <p>Настил деревянного<br>пола</p>
                                                        </li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Кровля</h4>
                                                    <ul class="my-list design-additional">
                                                        <li>
                                                            <p>Мягкая кровля</p>
                                                        </li>
                                                        <li>
                                                            <p>Металлочерепица</p>
                                                            <ul>
                                                                <li>
                                                                    <p>«Монтеррей»</p>
                                                                </li>
                                                                <li>
                                                                    <p>«Каскад»</p>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <p>Оцинкованный профнастил</p>
                                                        </li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Установка окон</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Пластиковые</p></li>
                                                        <li><p>Ламинированные</p></li>
                                                        <li><p>Деревянные</p></li>
                                                        <li><p>Деревянные стеклопакеты</p></li>
                                                        <li><p>Рольставни</p></li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Стены</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Каркас 150 мм</p></li>
                                                        <li><p>Каркас 200 мм</p></li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Утепление</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Минеральная вата KNAUF</p></li>
                                                        <li><p>Утеплитель Rockwool</p></li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Установка дверей</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Межкомнатные</p></li>
                                                        <li><p>Входные</p></li>
                                                    </ul>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Наружная отделка</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Сайдинг</p></li>
                                                        <li><p>Вагонка</p></li>
                                                        <li><p>Фасадные панели</p></li>
                                                        <li><p>Цокольный сайдинг</p></li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Фундамент</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Свайный</p></li>
                                                        <li><p>Ленточный</p></li>
                                                        <li><p>Столбчатый</p></li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <h4 class="additional-title">Наличники и плинтуса</h4>
                                                    <ul class="my-list design-additional">
                                                        <li><p>Деревянные</p></li>
                                                        <li><p>Пластиковые</p></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>

<section class="content white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white + my-margin">
                    <div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-left && bg-grey && padding-navs-20 && padding-tabs-30-30 ))">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">
                                    Заявка на достройку
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">
                                    Реализованные проекты
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane with-slider active" id="tab1">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <p>
                                            В предложенной ниже заявке Вы можете выбрать интересующую Вас услугу. Оставьте свои контактные данные, и наш специалист свяжется с Вами в ближайшее время.
                                            <br>Заявка не накладывает на Вас никаких обязательств, а является лишь запросом на получение бесплатной консультации.
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <form class="form-send comunication" data-toggle="validator" action="/include/request.php" method="post" role="form" data-form="send" data-roistat="Пристройка">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <label>Фамилия</label>
                                                                <input type="text"  class="form-control my-input" name="Name" placeholder="Иванов" required>
                                                                <input type="hidden" name="id_request" value="pristroiki"/>
                                                                <input type="hidden" name="request_type" value="4">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <label>Имя</label>
                                                                <input type="text"  class="form-control my-input" name="LastName" placeholder="Иван" required>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <label>Отчество</label>
                                                                <input type="text"  class="form-control my-input" name="Last2Name" placeholder="Иванович" required>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <p>Контактная информация:</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <label>Телефон</label>
                                                                <input type="phone" name="TelephoneClient" class="form-control my-input" placeholder="8 920 000 00 00" required>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <label>E-mail</label>
                                                                <input type="email"  name="MailClient" class="form-control my-input" placeholder="Введите Email">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <p>Какой вид пристройки Вас интересует:</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8 col-sm-8">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <div class="checkbox bordered">
                                                                    <input type="checkbox" id="checkbox1" name="cheks[]" value="Терраса">
                                                                    <label for="checkbox1">
                                                                        Терраса
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3 col-sm-3">
                                                                <div class="checkbox bordered">
                                                                    <input type="checkbox" id="checkbox2" name="cheks[]" value="Веранда">
                                                                    <label for="checkbox2">
                                                                        Веранда
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 col-sm-4">
                                                                <div class="checkbox bordered">
                                                                    <input type="checkbox" id="checkbox3" name="cheks[]" value="Терраса-веранда">
                                                                    <label for="checkbox3">
                                                                        Терраса-веранда
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-2 col-sm-2">
                                                                <div class="checkbox bordered">
                                                                    <input type="checkbox" id="checkbox4" name="cheks[]" value="Другое">
                                                                    <label for="checkbox4">
                                                                        Другое
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <p>Комментарии к заявке:</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <textarea class="form-control my-input" name="CommentsClient" placeholder="Комментарии к заявке" rows="4" required></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                                    <p><small>Отправляя заявку, вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных</a></small></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 col-sm-offset-8 col-md-offset-9 col-lg-offset-10">
                                                    <button type="submit" onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;" class="btn btn-danger">ОТПРАВИТЬ ЗАЯВКУ</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane with-slider" id="tab2">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div data-item="slider-item">

                                            <div class="owl-item full-width">
                                                <img src="/bitrix/templates/.default/assets/img/services/additional2/item-4.jpg" class="full-width">
                                                <br>
                                                <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>

                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-5.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-6.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-7.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-8.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-9.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-10.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-11.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-12.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-13.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-14.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-15.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-16.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-17.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-18.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-19.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-20.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-21.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                            <div class="owl-item full-width">
                                                <div class="my-main-img full-width">
                                                    <img src="/bitrix/templates/.default/assets/img/services/additional2/item-22.jpg" class="full-width">
                                                    <br>
                                                    <p><i>Пристройка террасы-веранды с полноценным вторым этажом</i></p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>