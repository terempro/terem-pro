<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "терем индивидуальный проект");
$APPLICATION->SetPageProperty("description", "Терем строит дом по вашему проекту");
$APPLICATION->SetPageProperty("keywords", "терем проект, проект дома, построить дом по проекту");
$APPLICATION->SetPageProperty("title", "Терем индивидуальный проект");
$APPLICATION->SetTitle("Терем индивидуальный проект");

$request_failed = isset($_SESSION["INDIVIDUAL_PROJECT_FAILED"]);

$double_request = isset($_SESSION["INDIVIDUAL_PROJECT_SENT"]) && ($_SESSION["INDIVIDUAL_PROJECT_SENT"] == true);
?>
<section class="content white" role="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="white padding-side clearfix + my-margin my-margin--mb0">
          <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
              <div class="my-text-title text-center text-uppercase desgin-h1">
                <h1>Построим дом по вашему проекту</h1>
              </div>
            </div>
          </div>
          <div class="row idividual-text">
            <div class="col-xs-12 col-md-12 col-lg-6">
                <div class="idividual-text__content">
                    <p>
                        Если вы точно знаете, как должен выглядеть ваш дом -  мы знаем, как его построить по приемлемой цене.
                    </p>
                    <p>
                        Для этого достаточно выслать нам картинку планировки, которая вам нравится, и мы скажем сколько стоит мечта. Картинка может быть профессиональной, или нарисована на бумаге в клеточку - главное, чтобы мы поняли: сколько этажей, комнат и какой размер дома вы хотите.
                    </p>
                    <p>
                        Подробную информацию можно получить также по телефону <br><a href="tel:+73412650623">+7 (3412) 65-06-23</a> или у менеджеров на территории выставочного комплекса компании «Теремъ» по адресу г. Ижевск, Славянское шоссе, 0/14.
                    </p>
                    <div class="attantion">
                        <p class="m-top"><strong>Внимание!</strong> Отправляя заявку, Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-6">
              <form name="multiple_upload_form" class="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="/kazan/include/add_project.php" method="post">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group form-group--mb10">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="file_upload">
                            <div>
                              <mark class="my-input">Загрузите до 3-х изображений проектов*</mark>
                              <span class="button my-input">Выбрать файлы</span>
                              <input type="file" class="file" name="images[]" id="upload-images" multiple required="required">
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div id="images_preview" class="images-preview"></div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-xs-12">
                          <textarea type="text" name="msg" rows="3" class="form-control my-input max" placeholder="Ваши комментарии к проекту..."></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="form-group--paired">
                            <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя*" required="required">
                            <input type="tel" name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="required">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group--paired">
                                <input type="text" name="dimensions" class="form-control my-input" placeholder="Линейные размеры">
                                <input type="email" name="mail" class="form-control my-input" placeholder="Ваш email*" required="required">
                            </div>
                        </div>
                      </div>
                    </div>  
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">                    
                    <div class="row">
                      <div class="col-xs-12 text-right">
                        <div class="flex-wrapper flex-wrapper--flex-end">
                            <span id="response-message" class="respond-message">
                                  <? if($double_request): ?>
                                          Вы уже отправили заявку на рассмотрение.<br>
                                          Наши менеджеры свяжутся с Вами в ближайшее время.
                                  <? endif; ?>
                                  <? if($request_failed): ?>
                                          Вы три раза подряд неправильно ввели проверочный код.<br>
                                          Доступ к сервису временно заблокирован!
                                  <? endif; ?>
                            </span>
                        <? if(!$double_request && !$request_failed): ?>
                        <div id="confirmation-code" class="confirmation-code" hidden="hidden">
                            <span id="confirmation-code-hint">Код подтверждения (выслан на почту):</span>&nbsp;&nbsp;
                            <input size="4" maxlength="4" type="text" name="confirmation-code" class="form-control" autofocus>
                        </div>
                          <button id="project-send-button" type="submit" name="ipadress" value="" class="btn btn-danger" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                          <? endif; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
       <div class="flex-wrapper flex-wrapper--grey"> 
        <div>
          <h2>ЗАКАЗАТЬ ЗВОНОК</h2>
          <div class="attantion">
            <p>
              Мы можем позвонить вам абсолютно бесплатно! Укажите свое имя, номер телефона, и предпочтительное время звонка. Наши специалисты ответят на любые возникшие вопросы!
            </p>
          </div>
        </div>
        <div>
          <form action="http://crm.terem-pro.ru/index.php/welcome/postCallme/" method="post" data-form="send">
            <div>
              <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя" required>
              <input type="tel" name="phone" data-item="phone" class="form-control my-input" placeholder="Введите ваш телефон" required>
              <input type="text" name="city" class="form-control my-input" placeholder="Введите ваш город" required>              
            </div>
            <div>
              <div class="attantion text-right">
                <p>
                  <strong>Внимание!</strong>
                  <br>
                  Все поля обязательны для заполнения.
                </p>
              </div>
              <button type="submit" class="btn btn-danger btn--width-auto">
                ОТПРАВИТЬ
              </button>
            </div>
          </form>
        </div>
       </div>
      </div>
    </div>
  </section>
  <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>