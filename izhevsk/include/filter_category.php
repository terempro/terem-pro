<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
global $DB;
$valid = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
$SECTION_ID = $arResult["VARIABLES"]["SECTION_ID"];
$res = CIBlockSection::GetByID($SECTION_ID );
if($ar_res = $res->GetNext()){
    $SECTION_NAME = $ar_res['NAME'];
}

?>
<?php if (CModule::IncludeModule("iblock")): ?>
    <?php
    $result = [];
    $categories = array($SECTION_ID => $SECTION_NAME);
    $series = [];
    $floors = [];
    $tehnologies = [];
    $areas = [];
    $min_area = 0;
    $max_area = 0;
    $prices = [];
    $min_price = 0;
    $max_price = 0;

    if ($_POST['security'] == $valid) {
        //echo '<pre>';
        //var_dump($_POST);
        $result_sql = "SELECT * FROM b_filter_house WHERE active = '1' AND category_name = '{$SECTION_NAME}' ORDER BY series_name";

//        if ($_POST["category"]) {
//            $cat = $_POST["category"];
//            $result_sql = $result_sql . " AND category_name = '{$cat}'";
//        }
        if ($_POST["model"]) {
            $ser = mysql_escape_string($_POST["model"]);
            $result_sql = $result_sql . " AND series_name = '".$ser?:''."'";
        }

        if ($_POST["house-floors"]) {
            $floor = (int) $_POST["house-floors"];
            $result_sql = $result_sql . " AND floors = '".$floor?:''."'";
        }

        if ($_POST["house-tech"]) {
            $tech = mysql_escape_string($_POST["house-tech"]);
            $result_sql = $result_sql . " AND technology = '".$tech?:''."'";
        }

        if ($_POST["house-areaUpto"] && $_POST["house-areaUpto"] != 'NaN' && $_POST["house-areaUpto"] != '0') {
            $max_area = (int) $_POST["house-areaUpto"];
            $result_sql = $result_sql . " AND total_area <= ".$max_area?:'0';
        }

        if ($_POST["house-areaFrom"] && $_POST["house-areaFrom"] != 'NaN' && $_POST["house-areaFrom"] != '0') {
            $min_area = (int) $_POST["house-areaFrom"];
            $result_sql = $result_sql . " AND total_area >= ".$min_area?:'0';
        }

        if ($_POST["house-priceUpto"] && $_POST["house-priceUpto"] != 'NaN' && $_POST["house-priceUpto"] != '0') {
            $max_price = (int) $_POST["house-priceUpto"];
            $result_sql = $result_sql . " AND price_value <= ".$max_price?:'0';
        }

        if ($_POST["house-priceFrom"] && $_POST["house-priceFrom"] != 'NaN' && $_POST["house-priceFrom"] != '0') {
            $min_price = (int) $_POST["house-priceFrom"];
            $result_sql = $result_sql . " AND price_value >= ".$min_area?:'0';
        }

        $resultSQL = $DB->Query($result_sql);


        $result = [];
        while ($row = $resultSQL->Fetch()) {
            $result[] = $row;
        }

       // echo 'sql';
       // var_dump($result);
        //$result = array_unique($result);

        foreach ($result as $v) {
            
            if (!in_array($v['category_name'], $categories)) {
                $categories[$v['id_category']] = $v['category_name'];
            }
            if (!in_array(trim($v['series_name']), $series)) {

                $series[$v['id_series']] = trim($v['series_name']);
            }
            if (!in_array($v['floors'], $floors)) {
                $floors[] = $v['floors'];
            }
            if (!in_array($v['technology'], $tehnologies)) {
                $tehnologies[] = $v['technology'];
            }
            if (!in_array($v['total_area'], $areas)) {
                $areas[] = round($v['total_area']);
            }
            if (!in_array($v['price_value'], $prices)) {
                $prices[] = round($v['price_value']);
            }
        }

        $min_area = min($areas);
        $max_area = max($areas);



        $min_price = min($prices);
        $max_price = max($prices);


        
    } else {
        $result_sql = $DB->Query("SELECT * FROM b_filter_house WHERE active = 1 AND category_name = '{$SECTION_NAME}' ORDER BY series_name");

        while ($row = $result_sql->Fetch()) {
            $result[] = $row;
        }

        foreach ($result as $v) {
            if (!in_array($v['category_name'], $categories)) {
                $categories[$v['id_category']] = $v['category_name'];
            }
            if (!in_array(trim($v['series_name']), $series)) {

                $series[$v['id_series']] = trim($v['series_name']);
            }
            if (!in_array($v['floors'], $floors)) {
                $floors[] = $v['floors'];
            }
            if (!in_array($v['technology'], $tehnologies)) {
                $tehnologies[] = $v['technology'];
            }
            if (!in_array($v['total_area'], $areas)) {
                $areas[] = round($v['total_area']);
            }
            if (!in_array($v['price_value'], $prices)) {
                $prices[] = round($v['price_value']);
            }
        }

        $min_area = min($areas);
        $max_area = max($areas);



        $min_price = round(min($prices));
        $max_price = round(max($prices));
        
       

    }
    
    sort($series);
  reset($series);
    ?>
    
    <div class="house__filter">
        <form action="" method="post" id="filter" class="filter-form" novalidate>
            <input type='hidden'name='security' value='<?php echo md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);?>'/>
            <div class="dropdown">
                <input type="hidden" name="realmodel" id="realmodel" value="" id="realmodel"/>
                

               
                    <input name="category" id="category" value="<?=$SECTION_NAME?>" class="hidden" type="text">
                    <span class="my-cross" id="categoryDropdown_cross" data-parent="categoryDropdown"></span>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="categoryDropdown" data-checked="false" data-placeholder="<?=$SECTION_NAME?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$SECTION_NAME?>
                        <span class="my-caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu--terem categories-filter" aria-labelledby="dropdownRoad">
                        <span class="my-arrow-top"></span>
                        <?php foreach ($categories as $k => $v): ?>
                            <li data-id="<?=$SECTION_NAME?>" class="category-li-filter"><a href="#" data-name="<?=$SECTION_NAME?>"><?=$SECTION_NAME?></a></li>
                        <?php endforeach; ?>
                    </ul>
             
            </div>

            <div class="dropdown">



                <?php if ($model = HTMLToTxt($_POST['model'])): ?>
                    <input name="model" id="modalinpt" value="<?= $model?:'' ?>" class="hidden" type="text">
                    <span class="my-cross" id="seriesDropdown_cross" style="display: inline;" data-parent="seriesDropdown"></span>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="seriesDropdown" data-checked="false" data-placeholder="Серия" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= $model?:'' ?>
                        <span class="my-caret" style="display: none;"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu--terem series-filter" aria-labelledby="dropdownRoad">
                        <?php foreach ($series as $k => $v): ?>
                            <li data-id="<?= $v ?>" data-value="<?= $v ?>" data-key="<?= $k ?>" class="series-filter-li"><a href="#" data-name="<?= $v ?>" ><?= $v ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: ?>
                    <input name="model" id="modalinpt" value="" class="hidden" type="text">
                    <span class="my-cross" id="seriesDropdown_cross" data-parent="seriesDropdown"></span>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="seriesDropdown" data-checked="false" data-placeholder="Серия" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Серия
                        <span class="my-caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu--terem series-filter" aria-labelledby="dropdownRoad">
                        <?php foreach ($series as $k => $v): ?>
                            <li data-id="<?= $v ?>" data-value="<?= $v ?>" data-key="<?= $k ?>" class="series-filter-li"><a href="#" data-name="<?= $v ?>" ><?= $v ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>


            </div>


            <div class="dropdown">

                <?php if ($_POST["house-areaFrom"] || $_POST["house-areaUpto"]): ?>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="house-areaDropdown" data-placeholder="Площадь" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if ($_POST["house-areaFrom"]): ?> от <?= (int)$_POST["house-areaFrom"] ?> <?php endif; ?> 
                        <?php if ($_POST["house-areaUpto"]): ?> до <?= (int)$_POST["house-areaUpto"] ?> <?php endif; ?>
                    </button>
                    <div class="dropdown-menu dropdown-menu--terem" aria-labelledby="house-areaDropdown">
                        <div class="range-box">
                            <input name="house-areaFrom" data-name="house-area" id="house-area-from" placeholder="от <?= $min_area ?>" data-val="<?= $min_area ?>" value="<?= (int)$_POST["house-areaFrom"] ?>" step="1" type="number" min="<?= $min_area ?>"> 
                            <span>&nbsp;-&nbsp;</span>
                            <input name="house-areaUpto" data-name="house-area" id="house-area-up-to" placeholder="до <?= $max_area ?>" data-val="<?= $max_area ?>" value="<?= (int)$_POST["house-areaUpto"] ?>" step="1" type="number" max="<?= $max_area ?>">
                            <span>м<sup>2</sup></span>
                        </div>
                    </div>
                <?php else: ?>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="house-areaDropdown" data-placeholder="Площадь" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Площадь
                        <span class="my-caret"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu--terem" aria-labelledby="house-areaDropdown">
                        <div class="range-box">
                            <input name="house-areaFrom" data-name="house-area" id="house-area-from" placeholder="от <?= $min_area ?>" data-val="<?= $min_area ?>" value="" step="1" type="number" min="<?= $min_area ?>">
                            <span>&nbsp;-&nbsp;</span>
                            <input name="house-areaUpto" data-name="house-area" id="house-area-up-to" placeholder="до <?= $max_area ?>" data-val="<?= $max_area ?>" value="" step="1" type="number" max="<?= $max_area ?>">
                            <span>м<sup>2</sup></span>
                        </div>
                    </div>
                <?php endif; ?>   

            </div>

            <div class="dropdown">
                <?php if ($_POST["house-floors"]): ?>
                    <input name="house-floors" id="floors" value="<?= (int)$_POST["house-floors"] ?>" class="hidden" type="text">
                    <span class="my-cross" id="house-floorsDropdown_cross" style="display: inline;" data-parent="house-floorsDropdown"></span>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="house-floorsDropdown" data-checked="false" data-placeholder="Этажность" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= (int)$_POST["house-floors"] ?>
                        <span class="my-caret" style="display: none;"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu--terem floors-filter" aria-labelledby="dropdownRoad">
                        <?php foreach ($floors as $k => $v): ?>
                            <li data-id="<?= $v ?>" class="floor-dilter-li" data-real="<?= $k ?>"><a href="#" data-name="<?= $v ?>" data-id="<?= $k ?>"><?= $v ?> <?php echo declension_words($v, ['этаж', 'этажа', 'этажей']); ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: ?>
                    <input name="house-floors" id="floors" value="" class="hidden" type="text">
                    <span class="my-cross" id="house-floorsDropdown_cross" data-parent="house-floorsDropdown"></span>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="house-floorsDropdown" data-checked="false" data-placeholder="Этажность" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Этажность
                        <span class="my-caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu--terem floors-filter" aria-labelledby="dropdownRoad">
                        <?php foreach ($floors as $k => $v): ?>
                            <li data-id="<?= $v ?>" class="floor-dilter-li" data-real="<?= $k ?>"><a href="#" data-name="<?= $v ?>" data-id="<?= $k ?>"><?= $v ?> <?php echo declension_words($v, ['этаж', 'этажа', 'этажей']); ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>




            </div>
            <div class="dropdown">

                <?php if ($_POST["house-tech"]): ?>
                    <input name="house-tech" value="<?= (int)$_POST["house-tech"] ?>" id="tech" class="hidden" type="text">
                    <span class="my-cross" id="house-techDropdown_cross" style="display: inline;" data-parent="house-techDropdown"></span>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="house-techDropdown" data-checked="false" data-placeholder="Технология" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= (int)$_POST["house-tech"] ?>
                        <span class="my-caret" style="display: none;"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu--terem tehnologies-filter" aria-labelledby="dropdownRoad">
                        <?php foreach ($tehnologies as $k => $v): ?>
                            <li  data-id="<?= $v ?>" class="filter-tech-li"><a href="#" data-name="<?= $v ?>"><?= $v ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: ?>
                    <input name="house-tech" value="" id="tech" class="hidden" type="text">
                    <span class="my-cross" id="house-techDropdown_cross" data-parent="house-techDropdown"></span>
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="house-techDropdown" data-checked="false" data-placeholder="Технология" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Технология
                        <span class="my-caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu--terem tehnologies-filter" aria-labelledby="dropdownRoad">
                        <?php foreach ($tehnologies as $k => $v): ?>
                            <li  data-id="<?= $v ?>" class="filter-tech-li"><a href="#" data-name="<?= $v ?>"><?= $v ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>     
            </div>


    




            <?php if ($_POST["house-priceFrom"] || $_POST["house-priceUpto"]): ?>
                <div class="dropdown">
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="house-priceDropdown" data-placeholder="Цена" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if ($_POST["house-priceFrom"]): ?> от <?= (int)$_POST["house-priceFrom"] ?> <?php endif; ?> 
                        <?php if ($_POST["house-priceUpto"]): ?> до <?= (int)$_POST["house-priceUpto"] ?> <?php endif; ?>
                    </button>
                    <div class="dropdown-menu dropdown-menu--terem" aria-labelledby="house-priceDropdown">
                        <div class="range-box">
                            <input name="house-priceFrom" data-name="house-price" id="house-price-from" data-val="<?= $min_price ?>" value="<?= (int)$_POST["house-priceFrom"] ?>" placeholder="от <?= $min_price ?>" type="number" min="<?= $min_price ?>">&nbsp;-&nbsp;<input name="house-priceUpto" data-name="house-price" id="house-price-up-to" data-val="<?= $max_price ?>" value="<?= (int)$_POST["house-priceUpto"] ?>" placeholder="до <?= $max_price ?>" type="number" max="<?= $max_price ?>">тыс.&nbsp;р.
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <div class="dropdown">
                    <button class="btn my-btn-filter btn-lg dropdown-toggle" type="button" id="house-priceDropdown" data-placeholder="Цена" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Цена
                        <span class="my-caret"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu--terem" aria-labelledby="house-priceDropdown">
                        <div class="range-box">
                            <input name="house-priceFrom" data-name="house-price" id="house-price-from" data-val="<?= $min_price ?>" value="" placeholder="от <?= $min_price ?>" type="number">&nbsp;-&nbsp;<input name="house-priceUpto" data-name="house-price" id="house-price-up-to" data-val="<?= $max_price ?>" value="" placeholder="до <?= $max_price ?>" type="number">тыс.&nbsp;р.
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="filter-button">
                <button class="submit-button" type="submit">Найти</button>
                <div class="hint-bubble">посмотреть<br>выбранное</div>
            </div>
        </form>
    </div>

    <?php if ($_POST): ?>
        <div class="row b-reset-button" style="display: block;">
            <div class="col-xs-12">
                <a href="http://www.terem-pro.ru/catalog/" class="refresh-button">Сбросить все фильтры <span class="my-cross--link"></span></a>
            </div>
        </div>
    <?php else: ?>
        <div class="row b-reset-button">
            <div class="col-xs-12">
                <a href="http://www.terem-pro.ru/catalog/" class="refresh-button">Сбросить все фильтры <span class="my-cross--link"></span></a>
            </div>
        </div>
    <?php endif; ?>

<?php endif; ?>