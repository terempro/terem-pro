<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты компании Терем в Москве");
$APPLICATION->SetPageProperty("keywords", "терем контакты, терем телефон, терем про почта");
$APPLICATION->SetTitle("Контакты компании Терем в Москве");
// $APPLICATION->AddHeadScript("/bitrix/templates/.default/assets/js/map.js");
$code = $APPLICATION->CaptchaGetCode();

$request_failed = isset($_SESSION["CLAIM_FAILED"]);
$double_request = isset($_SESSION["CLAIM_SENT"]) && ($_SESSION["CLAIM_SENT"] == true);
?>

<?php /*Выборка из инфоблока Контакты - партнеры*/?>
<?php
    $partnery = CIBlockElement::GetList(
        false, 
        ['IBLOCK_ID' => 120, 'ACTIVE' => 'Y',], 
        false, 
        false, 
        ['NAME','PROPERTY_COMPANY','PROPERTY_CITY','PROPERTY_ADDRESS','PROPERTY_SCHEDULE','PROPERTY_PHONE','PROPERTY_EMAIL','PROPERTY_CITE','PROPERTY_CODE',]
    );
    while ($ob = $partnery->GetNextElement()) {
        $info[] = $ob->GetFields();
    }	
?>

<script type="text/javascript" src="/bitrix/templates/.default/assets/js/map.js"></script>
<script type="text/javascript" src="scroll.js"></script>
<script type="text/javascript" src="mapclick.js"></script>
<link rel="stylesheet" type="text/css" href="style.css" >
<link rel="stylesheet" type="text/css" href="mapclick.css" >

<link href="jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="jquery.vmap.js" type="text/javascript"></script>
<script src="jquery.vmap.russia.js" type="text/javascript"></script>

<script type="text/javascript">
    // Массив всех объектов	
    var data_obj = {
        <?php foreach($info as $p):?>
        '<?php echo $p["PROPERTY_CODE_VALUE"];?>': ['<?php echo $p["PROPERTY_COMPANY_VALUE"];?>'],
        <?php endforeach;?>
        'mc': ['ТеремЪ']
    };


    colorRegion = '#b4b4b4'; // Цвет всех регионов
    focusRegion = '#FF9900'; // Цвет подсветки регионов при наведении на объекты из списка
    selectRegion = '#aa2028'; // Цвет изначально подсвеченных регионов

    highlighted_states = {};

    // Массив подсвечиваемых регионов, указанных в массиве data_obj
    for (iso in data_obj) {
        highlighted_states[iso] = selectRegion;
    }

    $(document).ready(function() {
        $('#vmap').vectorMap({
            map: 'russia',
            backgroundColor: '#ffffff',
            borderColor: '#fff',
            borderWidth: 1,
            color: colorRegion,
            colors: highlighted_states,
            hoverOpacity: 0.5,
            enableZoom: false,
            showTooltip: true,

            // Отображаем объекты если они есть
            onLabelShow: function(event, label, code) {
                name = '<strong>' + label.text() + '</strong><br>';
                if (data_obj[code]) {
                    list_obj = '';
                    for (ob in data_obj[code]) {
                        list_obj += data_obj[code][ob];
                    }
                    list_obj += '';
                } else {
                    list_obj = '';
                }
                label.html(name + list_obj);
                list_obj = '';
            },

            // Клик по региону
            onRegionClick: function(element, code, region, data_obj_redirect) {
                <?php foreach($info as $p):?>
                if (code == '<?php echo $p["PROPERTY_CODE_VALUE"];?>') {
                    $('html, body').animate({
                        scrollTop: $('#<?php echo $p["PROPERTY_CODE_VALUE"];?>').offset().top-100
                    }, 900);
                    $('#<?php echo $p["PROPERTY_CODE_VALUE"];?>').addClass('clickRegion');
                    setTimeout(function(){
                        $('#<?php echo $p["PROPERTY_CODE_VALUE"];?>').addClass('clickTime');
                    },2000);
                    setTimeout(function(){
                        $('#<?php echo $p["PROPERTY_CODE_VALUE"];?>').removeClass('clickTime').removeClass('clickRegion');
                    },3000);
                }
                <?php endforeach;?>
            }
        });
    });

    // Подсветка регионов при наведении на объекты
    $(function() {
        $('.focus-region').mouseover(function() {
            iso = $(this).prop('id');
            fregion = {};
            fregion[iso] = focusRegion;
            $('#vmap').vectorMap('set', 'colors', fregion);
        });
        $('.focus-region').mouseout(function() {
            c = $(this).attr('href');
            cl = (c === '#') ? colorRegion : c;
            iso = $(this).prop('id');
            fregion = {};
            fregion[iso] = cl;
            $('#vmap').vectorMap('set', 'colors', fregion);
        });
    });
</script>

<style>
    .jqvmap-label {
    z-index: 999999;
}
</style>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">	
						<div class="col-xs-12 col-md-6 col-sm-6">	
		                    <div class="text-uppercase gng-contacts-center">
                                <h1>
                                   <span class="red">МОСКВА</span><br>
								   ул. Зеленодольская, вл.42
                                </h1>
                            </div>
						</div>	
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="text-right gng-contacts-center">
                                <div class="h1 gng-contacts__h-margin-mobile">
                                    <span class="red">+7 (495) 461-01-10</span><br>
									<a href="mailto:info@terem-pro.ru"><span class="black" style="color: #000; font-size: 80%; text-decoration: none;">info@terem-pro.ru</span></a>
                                </div>
                            </div>
                        </div>	
					</div>
					<div class="row">
					    <div class="col-xs-12 col-md-12">
                            <div id="map" class="ymap">
							</div>
                        </div>
					</div>		
                    <div class="row">	
						<div class="col-xs-12 col-md-6 col-sm-6">	
		                    <div class="text-uppercase gng-contacts-center">
                                <div class="h2">                                   
								   Работаем без перерывов и выходных
                                </div>
                            </div>
						</div>	
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="text-right text-uppercase gng-contacts-center">
                                <div class="h2 gng-contacts__h-margin-mobile">
									<span class="red">Ждем вас с 10:00 до 20:00</span>
                                </div>
                            </div>
                        </div>	
					</div>					
				</div>
			</div>		
		</div>
	</div>	
</section>

<div id="zoom-map" style="display:none;position:relative;">
<img src="myMap.jpg" class="full-width" alt="">
<div class="btn-zooms">
<button class="btn btn-zoom-in"><i class="glyphicon glyphicon-plus"></i></button>
<button class="btn btn-zoom-out"><i class="glyphicon glyphicon-minus"></i></button>
</div>
</div>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white col-xs-12 + my-margin padding-side clearfix gng-contacts-route how-wey">			
					<div class="row">
						<div class="col-xs-12 col-md-2 col-sm-2">
							<h2 class="text-uppercase gng-contacts-center"><span class="red">Как добраться?</span></h2>
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4 how-wey_text_man">
							<b>Метро «Кузьминки»</b>: последний вагон из центра, в переход налево, выход к магазину «Снежная Королева» далее направо в сторону ул. Маршала Чуйкова, 400 м.
						</div>
						<div class="col-xs-12 col-md-6 col-sm-6 how-wey_text_car">
							<b>Автомобиль</b>: Волгоградский проспект из центра, перед метро «Кузьминки» на «дублер» и направо на ул. Маршала Чуйкова в сторону ул. Юных Ленинцев около 400 м, с левой стороны наш выставочный комплекс. В навигаторе рекомендуем набирать адрес «ул. Маршала Чуйкова, 14», дом расположен напротив входа на выставочную площадку.
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>		
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white col-xs-12 padding-side clearfix gng-contacts-beware">
					<div class="row consultation">
						<div class="col-xs-12 col-md-12 col-lg-3 consultation-header" style="margin-top: -5px;">
							<div class="h2 text-uppercase gng-contacts-center">Остерегайтесь<br>подделок</div>
						</div>
						<div class="col-xs-12 col-md-6 col-lg-5 text">
							Компания «Теремъ» заключает договоры только на территории выставочного комплекса по адресу: г. Москва, ул. Зеленодольская, вл.42
						</div>
						<div class="col-xs-12 col-md-6 col-lg-4 button-grey" style="background: none;" id="free_consult">
							<a href="#feedback" class="text-uppercase consultation_button ">Бесплатная консультация</a>
						</div>					
					</div>			
				</div>
			</div>	
		</div>
	</div>	
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix  + my-margin">
                    <div class="row">	
						<div class="col-xs-12 col-md-6 col-sm-6">	
		                    <div class="text-uppercase gng-contacts-center">
                                <h2 class="h1">
                                   Посетите наш выставочный комплекс<br>рядом со станцией <span class="red">метро «Кузьминки»</span>
                                </h2>
                            </div>
						</div>	
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="text-right text-uppercase gng-contacts-center">
                                <div class="h1 gng-contacts__h-margin-mobile">
                                    Более 40 домов и бань<br>с уютными интерьерами
                                </div>
                            </div>
                        </div>	
					</div>
					<div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <video class="VideoContact" style="position: relative;display: block;  margin: 0px -50px 0px; width: calc(100% + 100px);" controls="" itemprop="url">
                                <source src="/upload/contact.mp4" type="video/mp4">
                                <source src="/upload/contact.webm" type="video/webm">
                                <source src="/upload/contact.flv" type="video/flv">
                                Your browser does not support the video tag.
                            </video>
                        </div>
					</div>	
                    <div class="row">	
						<div class="col-md-12 col-lg-6  gng-contacts-map-button">	
		                    <div class="text-center text-uppercase contacts-button-map">
                                <a href="#mapclick" rel="nofollow" class="modalbox">                                   
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Карта выставочного комплекса
                                </a>
                                <div id="mapclick"><img src="myMap.jpg"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 ">
                            <div class="text-center text-uppercase gng-contacts-tour-button">
                                <a href="/upload/terem-vt/" onclick="yaCounter937330.reachGoal('teremvt'); ga('send', 'event', 'teremvt', 'click'); return true;">  
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Виртуальная 3d экскурсия
                                </a>
                            </div>
                        </div>	
					</div>					
				</div>
			</div>	
		</div>
	</div>	
</section>
<section class="content text-page white hidden-xs hidden-sm" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix  + my-margin">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-uppercase text-center">
                                <div class="h1">
                                    Интерактивная карта партнеров «ТЕРЕМЪ»
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="vmap" style="width: 100%; height: 593px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content text-page white partner-desktop" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="catalog-item-gallery + my-margin">
                    <div class="white my-padding-50 clearfix gng-contacts-partners" style="background: #eeeeee;">
                        <h2 class="my-title my-title--extra-gutter">Партнеры «Теремъ»</h2>
                        <?php /*Динамический вывод партнеров из инфоблока (десктопная версия)*/ ?>
                        <div class="row">
                                <?php foreach($info as $p):?>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="dealer" id="<?=$p['PROPERTY_CODE_VALUE'];?>">
                                            <h3 class="my-title">
                                                <?=$p['NAME'];?>
                                            </h3>
                                            <br>
                                            <p>
                                                <?=$p['PROPERTY_COMPANY_VALUE'];?>
                                            </p>
                                            <ul>
                                                <li>
                                                    <?=htmlspecialcharsBack($p['PROPERTY_CITY_VALUE']);?>,
                                                    <?=htmlspecialcharsBack($p['PROPERTY_ADDRESS_VALUE']);?>
                                                </li>
                                                <li>
                                                    <?=htmlspecialcharsBack($p['PROPERTY_SCHEDULE_VALUE']);?>
                                                </li>
                                                <li>
                                                    <a href="tel:<?=$p['PROPERTY_PHONE_VALUE'];?>">
                                                        <?=$p['PROPERTY_PHONE_VALUE'];?></a><br>
                                                    <a href="mailto:<?=$p['PROPERTY_EMAIL_VALUE'];?>">
                                                        <?=$p['PROPERTY_EMAIL_VALUE'];?></a><br>
                                                    <?php /*Формирование ссылки: ссылка на сайт + если в строке есть упоминание России, то обрезаем строку, приводим ее к виду Ххххх и выводим параметр для редиректа*/ ?>
                                                    <?php 
                                                        $cite=$p['PROPERTY_CITE_VALUE'];
                                                        if (strpos(strtoupper($p['NAME']),', РОССИЯ')){
                                                            $redirect=mb_convert_case(str_replace(', РОССИЯ','',strtoupper($p['NAME'])),MB_CASE_TITLE, "UTF-8");
                                                            $link=$cite.'?city='.$redirect;
                                                        }
                                                        else{
                                                        $link=$cite;   
                                                        }
                                                    ?>
                                                    <a href="https://<?php echo $link;?>">
                                                        <?=$p['PROPERTY_CITE_VALUE'];?></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endforeach;?>
                            </div>						
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content white catalog-item-1 active overflow-v contact-forms partner-mobile" role="content" data-item="1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="catalog-item-gallery white my-padding-50 + my-margin">
                    <div class="tab-panel ( my-tabpanel &amp;&amp; ( design-1 &amp;&amp; appearance-list-center &amp;&amp; bg-grey &amp;&amp; padding-20 ))">
                        <!-- Tab panes -->
                        <div class="contacts-item">
                            <div id="contact3-partner">
                                <h2 class="h1 my-title my-title--extra-gutter">Партнеры «Теремъ»</h2>
                                <div class="panel-group" id="accordion_partner" role="tablist" aria-multiselectable="true">
                                <?php /*Динамический вывод партнеров из инфоблока (мобильная версия)*/ ?>                            
                                <?php foreach($info as $p):?>
                                    <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading_<?=$p['PROPERTY_CODE_VALUE'];?>_partner">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion_partner" href="#<?=$p['PROPERTY_CODE_VALUE'];?>_partner" aria-expanded="false" aria-controls="<?=$p['PROPERTY_CODE_VALUE'];?>_partner">
                                                            <div class="my-table">
                                                                <div>
                                                                    <p><?=$p['NAME'];?></p>
                                                                </div>
                                                                <div>
                                                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                               <div id="<?=$p['PROPERTY_CODE_VALUE'];?>_partner" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?=$p['PROPERTY_CODE_VALUE'];?>_partner">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                            <div class="dealer">
                                                                <br>
                                                                <p>
                                                                    <?=$p['PROPERTY_COMPANY_VALUE'];?>
                                                                </p>
                                                                <ul>
                                                                    <li>
                                                                        <?=htmlspecialcharsBack($p['PROPERTY_CITY_VALUE']);?>,
                                                                        <?=htmlspecialcharsBack($p['PROPERTY_ADDRESS_VALUE']);?>
                                                                    </li>
                                                                    <li>
                                                                        <?=htmlspecialcharsBack($p['PROPERTY_SCHEDULE_VALUE']);?>
                                                                    </li>
                                                                    <li>
                                                                        <a href="tel:<?=$p['PROPERTY_PHONE_VALUE'];?>">
                                                                            <?=$p['PROPERTY_PHONE_VALUE'];?></a><br>
                                                                        <a href="mailto:<?=$p['PROPERTY_EMAIL_VALUE'];?>">
                                                                            <?=$p['PROPERTY_EMAIL_VALUE'];?></a><br>
                                                                        <?php /*Формирование ссылки: ссылка на сайт + если в строке есть упоминание России, то обрезаем строку, приводим ее к виду Ххххх и выводим параметр для редиректа*/ ?>
                                                                        <?php 
                                                                            $cite=$p['PROPERTY_CITE_VALUE'];
                                                                            if (strpos(strtoupper($p['NAME']),', РОССИЯ')){
                                                                                $redirect=mb_convert_case(str_replace(', РОССИЯ','',strtoupper($p['NAME'])),MB_CASE_TITLE, "UTF-8");
                                                                                $link=$cite.'?city='.$redirect;
                                                                            }
                                                                            else{
                                                                                $link=$cite;   
                                                                            }
                                                                        ?>
                                                                        <a href="https://<?php echo $link;?>">
                                                                            <?=$p['PROPERTY_CITE_VALUE'];?></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white col-xs-12 gng-contacts-invite">
					<div class="row" style="background: none;">
						<div class="white col-xs-12 col-lg-offset-3 col-md-6 my-padding-50 gng-contacts-invite_text gng-contacts-center">
							Строительная компания «Теремъ» приглашает партнеров из России и стран СНГ к сотрудничеству: 
						</div>
						<div class="white col-xs-12 col-lg-3 col-md-6 gng-contacts-invite_email gng-contacts-center">
							<div class="h2"><a href="mailto:partner@terem-pro.ru">partner@terem-pro.ru</a></div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>	
</section>
   <section class="content white catalog-item-1 content-catalog-item active overflow-v contact-forms" role="content" data-item="1" id="feedback">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="catalog-item-gallery white my-padding-50 + my-margin">
                        <div class="tab-panel ( my-tabpanel &amp;&amp; ( design-1 &amp;&amp; appearance-list-center &amp;&amp; bg-grey &amp;&amp; padding-20 ))">
                            <!-- Tab panes -->
                            <div class="contacts-item">
                                <div id="contact3">
                                    <h2 class="my-title my-title--extra-gutter">Обратная связь</h2>
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading1">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="( my-collapse-btn + design-main )" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                                            <div class="my-table">
                                                                <div>
                                                                    <p>Получить бесплатную консультацию</p>
                                                                </div>
                                                                <div>
                                                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div>
                                                            <div class="( my-collapse-content + design-main )">
                                                                <p>Мы можем позвонить вам абсолютно бесплатно! Пожалуйста, укажите свое имя, номер телефона и предпочтительное время звонка. Наши специалисты ответят на любые возникшие вопросы!</p>
                                                                <form id="cont1" data-form="send" class="form-send" action="/include/request_handler.php" method="post">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="required" type="text">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="required" type="tel">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <select class="form-control my-input" name="time" required="required">
                                                                                        <option value="" disabled="" selected="" title="Удобное время звонка">Удобное время звонка</option>
                                                                                        <option value="10-00">10-00</option>
                                                                                        <option value="11-00">11-00</option>
                                                                                        <option value="12-00">12-00</option>
                                                                                        <option value="13-00">13-00</option>
                                                                                        <option value="14-00">14-00</option>
                                                                                        <option value="15-00">15-00</option>
                                                                                        <option value="16-00">16-00</option>
                                                                                        <option value="17-00">17-00</option>
                                                                                        <option value="18-00">18-00</option>
                                                                                        <option value="19-00">19-00</option>
                                                                                    </select>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="mail" class="form-control my-input" placeholder="Ваш email" required="required" type="email">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                            <div class="attantion">
                                                                                <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                                <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                            <input name="city" value="<?= $CURRENT_CITY ?>" type="hidden">
                                                                            <input type="hidden" name="request_type" value="30">
                                                                            <button type="submit" name="btn" class="btn btn-danger" style="pointer-events: all; cursor: pointer;" onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;">ОТПРАВИТЬ</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading2">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                                            <div class="my-table">
                                                                <div>
                                                                    <p>Задать вопрос директору</p>
                                                                </div>
                                                                <div>
                                                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div>
                                                            <div class="( my-collapse-content + design-main )">
                                                                <p>Здесь вы можете задать интересующий вас вопрос генеральному директору компании «Теремъ»</p>
                                                                <form data-form="send" id="cont2" class="form-send" action="/include/request_handler.php" method="post">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="required" type="text">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="required" type="tel">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <div class="captcha">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <textarea type="text" name="msg" resize="none" class="form-control my-input max" required="required" style="min-height:99px;" placeholder="Ваш вопрос..."></textarea>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                            <div class="col-xs-12 col-md-6 attantion">
                                                                                <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                                <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                            <input value="f1" name="type" type="hidden"><br>
                                                                            <input type="hidden" name="request_type" value="31">
                                                                            <button type="submit" name="btn" class="btn btn-danger" style="pointer-events: all; cursor: pointer;" onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;">ОТПРАВИТЬ</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php {/*<div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading3">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Пожаловаться на бригаду</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Если у Вас возникли объективные претензии к работе строительной бригады, Вы можете изложить из для скорейшего устранения причины. Все обращения будут рассмотрены в обязательном порядке. Просим обязательно указать номер договора для оперативной обработки данных.</p>
                                                            <form data-form="send" id="cont3" class="form-send" action="/include/request_handler.php" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="required" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="required" type="tel">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="number" class="form-control my-input" placeholder="Номер бригады" required="required" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input min" placeholder="Суть претензии..." required="required"></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!-- <div id="captcha2" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
																		<input type="hidden" name="request_type" value="32">
                                                                        <button type="submit" name="btn" class="btn btn-danger akim" style="pointer-events: all; cursor: pointer;" onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;">ОТПРАВИТЬ</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading4">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Отблагодарить бригаду</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Здесь вы можете отблагодарить бригаду, которая строила Вам дом, за добросовестный труд и высокое качество работы.</p>
                                                            <form data-form="send" id="cont4" class="form-send" action="/include/request_handler.php" method="post">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="required" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="mail" class="form-control my-input" placeholder="Ваш email" required="required" type="email">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" placeholder="Текст..." required="required"></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!-- <div id="captcha3" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
																		<input type="hidden" name="request_type" value="33">
                                                                        <button type="submit" name="btn" class="btn btn-danger" style="pointer-events: all; cursor: pointer;" onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;">ОТПРАВИТЬ</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>*/}?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading5">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                                            <div class="my-table">
                                                                <div>
                                                                    <p>Отдел клиентских коммуникаций</p>
                                                                </div>
                                                                <div>
                                                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div>
                                                            <div class="( my-collapse-content + design-main )">
                                                                <p>По всем вопросам, возникающим в ходе строительства, вы можете обратиться в Отдел клиентских коммуникаций. Все обращения обязательно рассматриваются. Вам помогут с решением любого вопроса.</p>
                                                                <form data-form="send" id="cont5" class="form-send" action="/include/request_handler.php" method="post">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="name5" class="form-control my-input" placeholder="Введите ваше имя *" required="required" type="text">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="mail5" class="form-control my-input" placeholder="Ваш email *" required="required" type="email">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <div class="captcha">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="phone5" class="form-control my-input" placeholder="Введите ваш телефон *" required="required" type="tel">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input id="contract_number" name="contract_number" class="form-control my-input" placeholder="Номер договора" type="text">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                               <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <textarea type="text" name="msg5" resize="none" class="form-control my-input max" style="min-height:99px;" placeholder="Ваш вопрос *" required="required"></textarea>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                            <div class="col-xs-12 col-md-6 attantion">
                                                                                <p class="m-top"><strong>Внимание! </strong>Все поля с отметкой <span class="required="required"_input">*</span> обязательны для заполнения.</p>
                                                                                <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                            </div>
                                                                            <div class="col-xs-12 col-md-6">
                                                                                <!--  <div class="g-recaptcha" id="captcha4" data-sitekey="6LeyiQwUAAAAACJ9_CrHgc9lJpXySHFwEdjjJkgy" /></div> -->
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                            <span class="respond-message">
                                                                        </span>
                                                                            <input type="hidden" name="request_type" value="34">
                                                                            <button type="submit" name="btn" class="btn btn-danger" style="pointer-events: all; cursor: pointer;" onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;">ОТПРАВИТЬ</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                                            <div class="my-table">
                                                                <div>
                                                                    <p>Обратиться в рекламный отдел</p>
                                                                </div>
                                                                <div>
                                                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                                        <div>
                                                            <div class="( my-collapse-content + design-main )">
                                                                <p>Обращения по вопросам сотрудничества и рекламы.</p>
                                                                <form data-form="send" id="cont6" class="form-send" action="/include/request_handler.php" method="post">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="name6" class="form-control my-input" placeholder="Введите ваше имя" required="required" type="text">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="mail6" class="form-control my-input" placeholder="Ваш email" required="required" type="email">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <div class="captcha">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <input name="phone6" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="required" type="tel">
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <textarea type="text" name="msg6" resize="none" class="form-control my-input max" placeholder="Текст..." required="required"></textarea>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="help-block with-errors"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                            <div class="col-xs-12 col-md-6 attantion">
                                                                                <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                                <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                            </div>
                                                                            <div class="col-xs-12 col-md-6">
                                                                                <!-- <div class="g-recaptcha" id="captcha5" data-sitekey="6LeyiQwUAAAAACJ9_CrHgc9lJpXySHFwEdjjJkgy" /></div> -->
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                            <input type="hidden" name="request_type" value="35">
                                                                            <input value="f5" name="type" type="hidden"><br>
                                                                            <button type="submit" name="btn" class="btn btn-danger" style="pointer-events: all; cursor: pointer;" onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;">ОТПРАВИТЬ</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>