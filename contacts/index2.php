<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты компании Терем в Москве");
$APPLICATION->SetPageProperty("keywords", "терем контакты, терем телефон, терем про почта");
$APPLICATION->SetTitle("Контакты компании Терем в Москве");
// $APPLICATION->AddHeadScript("/bitrix/templates/.default/assets/js/map.js");
$code = $APPLICATION->CaptchaGetCode();

$request_failed = isset($_SESSION["CLAIM_FAILED"]);
$double_request = isset($_SESSION["CLAIM_SENT"]) && ($_SESSION["CLAIM_SENT"] == true);
?><script type="text/javascript" src="/bitrix/templates/.default/assets/js/map.js"></script>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">	
						<div class="col-xs-12 col-md-6 col-sm-6">	
		                    <div class="text-uppercase">
                                <h1>
                                   <span class="red">МОСКВА</span><br />
								   ул. Зеленодольская, вл.42
                                </h1>
                            </div>
						</div>	
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="text-right">
                                <h1>
                                    <span class="red">+7 (495) 419-14-14</span><br />
									<a href="mailto:info@terem-pro.ru"><span class="black" style="color: #000; font-size: 80%; text-decoration: none;">info@terem-pro.ru</span></a>
                                </h1>
                            </div>
                        </div>	
					</div>
					<div class="row">
					    <div class="col-xs-12 col-md-12">
                            <div id="map" class="ymap"></div>
                        </div>
					</div>		
                    <div class="row">	
						<div class="col-xs-12 col-md-6 col-sm-6">	
		                    <div class="text-uppercase">
                                <h2>                                   
								   Работаем без перерывов и выходных
                                </h2>
                            </div>
						</div>	
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="text-right text-uppercase">
                                <h2>
									<span class="red">Ждем вас с 10:00 до 20:00</span>
                                </h2>
                            </div>
                        </div>	
					</div>					
				</div>
			</div>		
		</div>
	</div>	
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white col-xs-12 + my-margin padding-side clearfix gng-contacts-route">			
					<div class="row">
						<div class="col-xs-12 col-md-2 col-sm-2">
							<h2 class="text-uppercase"><span class="red">Как добраться?</span></h2>
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							Метро: последний вагон из центра, в переход налево, выход к магазину «Снежная Королева» далее направо в сторону ул. Маршала Чуйкова, 400 м.
						</div>
						<div class="col-xs-12 col-md-6 col-sm-6">
							Автомобиль: Волгоградский проспект из центра, перед метро «Кузьминки» на «дублер» и направо на ул. Маршала Чуйкова в сторону ул. Юных Ленинцев около 400 м, с левой стороны наш выставочный комплекс. В навигаторе рекомендуем набирать адрес «ул. Маршала Чуйкова, 14», дом расположен напротив входа на выставочную площадку.
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>		
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white col-xs-12 padding-side clearfix gng-contacts-beware" style="background: url('contacts_arrow_red.png') no-repeat; background-color: #ab2121; color: #fff; height: 115px;">
					<div class="row">
						<div class="col-xs-12 col-md-3 col-sm-3" style="background: none;">
							<h2 class="text-uppercase">Остерегайтесь<br />подделок</h2>
						</div>
						<div class="col-xs-12 col-md-5 col-sm-5 text" style="background: none;">
							Компания «Теремъ» заключает договоры только на территории выставочного комплекса по адресу: г. Москва, ул. Зеленодольская, вл.42
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4 button-grey" style="background: none;">
							<h2 class="text-uppercase">Бесплатная консультация</h2>
						</div>					
					</div>			
				</div>
			</div>	
		</div>
	</div>	
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix  + my-margin">
                    <div class="row">	
						<div class="col-xs-12 col-md-6 col-sm-6">	
		                    <div class="text-uppercase">
                                <h1>
                                   Посетите наш выставочный комплекс<br />рядом со станцией <span class="red">метро &laquo;Кузьминки&raquo;</span>
                                </h1>
                            </div>
						</div>	
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="text-right text-uppercase">
                                <h1>
                                    Более 40 домов и бань<br />с уютными интерьерами
                                </h1>
                            </div>
                        </div>	
					</div>
					<div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <video class="VideoContact" style="position: relative;display: block;  margin: 0px -50px 0px; width: calc(100% + 100px);" controls="" itemprop="url">
                                <source src="/upload/contact.mp4" type="video/mp4">
                                <source src="/upload/contact.webm" type="video/webm">
                                <source src="/upload/contact.flv" type="video/flv">
                                Your browser does not support the video tag.
                            </video>
                        </div>
					</div>		
                    <div class="row">	
						<div class="col-xs-12 col-md-6 col-sm-6 gng-contacts-map-button">	
		                    <div class="text-center text-uppercase contacts-button-map" style="background: #ab2121;">
                                <h2 style="color: #fff;">                                   
								   Карта выставочного комплекса
                                </h2>
                            </div>
						</div>	
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="text-center text-uppercase gng-contacts-tour-button" style="background: #054477;">
                                <h2 style="color: #fff;">  
									Виртуальная 3d экскурсия
                                </h2>
                            </div>
                        </div>	
					</div>					
				</div>
			</div>	
		</div>
	</div>	
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="catalog-item-gallery + my-margin">
                    <div class="white my-padding-50 clearfix gng-contacts-partners" style="background: #eeeeee;">
                        <h1 class="my-title my-title--extra-gutter">Партнеры «Теремъ»</h1>
                         <div class="row">
                             <div class="col-sm-6 col-md-3">
                                 <div class="dealer">
                                     <h3 class="my-title">Брянск, Россия</h3>
                                     <br>
                                     <p>ООО «Техногазсервис»</p>
                                     <ul>
                                         <li>
											 г. Брянск, проезд Московский, <br>д. 10А, офис 202
                                         </li>
                                         <li>
                                             <a href="tel:+74832321381" title="+7 (4832) 321-381">+7 (4832) 321-381</a>
                                         </li>
                                         <li>
                                             Пн – пт 09:00 — 17:00
                                         </li>
                                         <li>
                                             <a href="mailto:info.bryansk@terem-pro.ru" title="info.bryansk@terem-pro.ru">info.bryansk@terem-pro.ru</a>
                                         </li>
                                     </ul>
                                 </div>
                             </div>
                             <div class="col-sm-6 col-md-3">
                                 <div class="dealer">
                                     <h3 class="my-title">Вологда, Россия</h3>
                                     <br>
                                     <p>ООО «Вологодский ТЕРЕМ»</p>
                                     <ul>
                                         <li>
                                             г. Вологда, Окружное шоссе,
                                             <br>
                                             д. 18, ТЦ «Аксон»
                                         </li>
                                         <li>
                                             <a href="tel:+78172578245" title="+7 (8172) 57-82-45">+7 (8172) 57-82-45</a>
                                         </li>
                                         <li>
                                             Пн – вс 10:00 – 20:00</li>
                                         <li>
                                             <a href="mailto:info.vologda@terem-pro.ru" title="info.vologda@terem-pro.ru">info.vologda@terem-pro.ru</a>
                                         </li>
                                     </ul>
                                 </div>
                             </div>
                             <div class="col-sm-6 col-md-3">
                                 <div class="dealer">
                                     <h3 class="my-title">Ижевск, Россия</h3>
                                     <br>
                                     <p>ООО «ИжЭкоСтрой»</p>
                                     <ul>
                                         <li>
                                             г. Ижевск,<br>Славянское шоссе 0/14
                                         </li>
                                         <li>
                                             <a href="tel:+7341250623" title="+7 (3412) 65-06-23">+7 (3412) 65-06-23</a>
                                         </li>
                                         <li>
                                             Пн – вс 10:00 – 20:00
                                         </li>
                                         <li>
                                             <a href="mailto:info.izh@terem-pro" title="info.izh@terem-pro">info.izh@terem-pro.ru</a>
                                         </li>
                                     </ul>
                                 </div>
                             </div>
                             <div class="col-sm-6 col-md-3">
                                 <div class="dealer">
                                     <h3 class="my-title">Казань, Россия</h3>
                                     <br>
                                     <p>ООО «ЭДОМ»</p>
                                     <ul>
                                         <li>
                                             г. Казань, ул. Аделя Кутуя,
                                             <br>
                                             д. 116, каб. 25
                                         </li>
                                         <li>
                                             <a href="tel:+78432070348" title="+7 (843) 20-70-348">+7 (843) 20-70-348</a>
                                         </li>
                                         <li>
											 Пн – пт 09:00 – 19:00
                                             <br>
                                             Сб 10:00 – 16:00
                                         </li>
                                         <li>
                                             <a href="mailto:info.kazan@terem-pro.ru" title="info.kazan@terem-pro.ru">info.kazan@terem-pro.ru</a>
                                         </li>
                                     </ul>
                                 </div>
                             </div>
                         </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="dealer">
                                    <h3 class="my-title">Республика Беларусь</h3>
                                    <br>
                                    <p>ООО «Терем Запад»</p>
                                    <ul>
                                        <li>
                                            г. Барановичи,<br>ул. Профессиональная, д. 9
                                        </li>
                                        <li>
                                            <a href="tel:+375444665588" title="+375 (44) 466-55-88">+375 (44) 466-55-88</a>
                                        </li>
                                        <li>
                                            Пн – вс 10:00 – 19:00
                                        </li>
                                        <li>
                                            <a href="mailto:info@terem-pro.by" title="info@terem-pro.by">info@terem-pro.by</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="dealer">
                                    <h3 class="my-title">Саратов, Россия</h3>
                                    <br>
                                    <p>ИП Рожков М. А.</p>
                                    <ul>
                                        <li>
                                            г. Энгельс,<br>проспект Строителей, 32Б
                                        </li>
                                        <li>
                                            <a href="tel:+78452338144" title="+7 (8452) 33-81-44">+7 (8452) 33-81-44</a>
                                        </li>
                                        <li>
                                            Пн – вс 09:00 – 18:00
                                             <br>
                                             Сб 09:00 – 14:00
                                        </li>
                                        <li>
                                            <a href="mailto:info@terem-pro.by" title="info.saratov@terem-pro.ru">info.saratov@terem-pro.ru</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="dealer">
                                    <h3 class="my-title">Тула, Россия</h3>
                                    <br>
                                    <p>ООО «Вятич»</p>
                                    <ul>
                                        <li>
                                            г. Тула, ул. Галкина, д. 1
                                        </li>
                                        <li>
                                            <a href="tel:+74872751446" title="+7 (4872) 751-446">+7 (4872) 751-446</a>
                                        </li>
                                        <li>
                                            Пн – пт 09:00 – 19:00<br>Сб – вс 10:00 – 17:00

                                        </li>
                                        <li>
                                            <a href="mailto:info.tula@terem-pro.ru" title="info.tula@terem-pro.ru">info.tula@terem-pro.ru</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white col-xs-12 gng-contacts-invite" style="height: 115px; background: url('contacts_arrow_grey.png') no-repeat; background-color: #fff;">
					<div class="row" style="background: none;">
						<div class="white col-xs-12 col-md-offset-3 col-md-6 col-sm-12  my-padding-50" style="background: none;">
							Строительная компания «Теремъ» приглашает партнеров из России и стран СНГ к сотрудничеству: 
						</div>
						<div class="white col-xs-12 col-md-3 col-sm-12" style="background: none;">
							<h2><a href="mailto:partner@terem-pro.ru">partner@terem-pro.ru</a></h2>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>	
</section>
<section class="content white catalog-item-1 content-catalog-item active overflow-v contact-forms" role="content" data-item="1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="catalog-item-gallery white my-padding-50 + my-margin">
                    <div class="tab-panel ( my-tabpanel &amp;&amp; ( design-1 &amp;&amp; appearance-list-center &amp;&amp; bg-grey &amp;&amp; padding-20 ))">
                        <!-- Tab panes -->
                        <div class="contacts-item">
                            <div id="contact3">
                                <h1 class="my-title my-title--extra-gutter">Обратная связь</h1>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading1">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main )" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Получить бесплатную консультацию</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Мы можем позвонить вам абсолютно бесплатно! Пожалуйста, укажите свое имя, номер телефона и предпочтительное время звонка. Наши специалисты ответят на любые возникшие вопросы!</p>
                                                            <form id="crmRequest" data-formtype="contact-form" class="form-send" action="/include/HendlerContact.php" data-toggle="validator" method="post" data-roistat="Контакты - получить бесплатную консультацию">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="" type="tel">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <select class="form-control my-input" name="time" required="">
                                                                                        <option value="" disabled="" selected="" title="Удобное время звонка">Удобное время звонка</option>
                                                                                        <option value="10-00">10-00</option>
                                                                                        <option value="11-00">11-00</option>
                                                                                        <option value="12-00">12-00</option>
                                                                                        <option value="13-00">13-00</option>
                                                                                        <option value="14-00">14-00</option>
                                                                                        <option value="15-00">15-00</option>
                                                                                        <option value="16-00">16-00</option>
                                                                                        <option value="17-00">17-00</option>
                                                                                        <option value="18-00">18-00</option>
                                                                                        <option value="19-00">19-00</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="mail" class="form-control my-input" placeholder="Ваш email" required="" type="email">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                            <? if($double_request): ?>
                                                                            Спасибо! Ваше обращение зарегистрировано.<br>
                                                                            Мы обязательно свяжемся с Вами в ближайшее время!
                                                                            <? endif; ?>
                                                                            <? if($request_failed): ?>
                                                                            Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                            Доступ к сервису временно заблокирован!
                                                                            <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на телефон):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" name="confirmation-code" class="form-control" autofocus="" type="text">
                                                                        </div>
                                                                        <input value="f0" name="type" type="hidden">
                                                                        <input name="city" value="<?= $CURRENT_CITY ?>" type="hidden">
                                                                        <button type="submit" name="btn" value="f0" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading2">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Задать вопрос директору</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Здесь вы можете задать интересующий вас вопрос генеральному директору компании «Теремъ»</p>
                                                            <form data-formtype="contact-form" action="/include/HendlerContact.php" data-toggle="validator" method="post" data-roistat="Контакты - задать вопрос директору">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="" type="tel">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input name="captcha_word" value="" placeholder="Введите символы с картинки" type="text">
                                                                                        <input name="captcha_sid" value="<?= $code; ?>" type="hidden">
                                                                                        <img alt="CAPTCHA" src="/bitrix/tools/captcha.php?captcha_sid=<?= $code; ?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" required="" placeholder="Ваш вопрос..."></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                            <? if($double_request): ?>
                                                                            Спасибо! Ваше обращение зарегистрировано.<br>
                                                                            Мы обязательно свяжемся с Вами в ближайшее время!
                                                                            <? endif; ?>
                                                                            <? if($request_failed): ?>
                                                                            Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                            Доступ к сервису временно заблокирован!
                                                                            <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на телефон):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" name="confirmation-code" class="form-control" autofocus="" type="text">
                                                                        </div>
                                                                        <input value="f1" name="type" type="hidden"><br>
                                                                        <button type="submit" name="btn" value="f1" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading3">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Пожаловаться на бригаду</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Если у Вас возникли объективные претензии к работе строительной бригады, Вы можете изложить из для скорейшего устранения причины. Все обращения будут рассмотрены в обязательном порядке. Просим обязательно указать номер договора для оперативной обработки данных.</p>
                                                            <form data-formtype="contact-form" action="/include/HendlerContact.php" data-toggle="validator" method="post" data-roistat="Контакты - пожаловаться на бригаду">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="phone" class="form-control my-input" data-item="phone" placeholder="Введите ваш телефон" required="" type="tel">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input name="captcha_word" value="" placeholder="Введите символы с картинки" type="text">
                                                                                        <input name="captcha_sid" value="<?= $code; ?>" type="hidden">
                                                                                        <img alt="CAPTCHA" src="/bitrix/tools/captcha.php?captcha_sid=<?= $code; ?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="number" class="form-control my-input" placeholder="Номер бригады" required="" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input min" placeholder="Суть претензии..." required=""></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!-- <div id="captcha2" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                            <? if($double_request): ?>
                                                                            Спасибо! Ваше обращение зарегистрировано.<br>
                                                                            Мы обязательно свяжемся с Вами в ближайшее время!
                                                                            <? endif; ?>
                                                                            <? if($request_failed): ?>
                                                                            Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                            Доступ к сервису временно заблокирован!
                                                                            <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на телефон):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" name="confirmation-code" class="form-control" autofocus="" type="text">
                                                                        </div>
                                                                        <input value="f2" name="type" type="hidden"><br>
                                                                        <button type="submit" name="btn2" value="f2" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading4">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Отблагодарить бригаду</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Здесь вы можете отблагодарить бригаду, которая строила Вам дом, за добросовестный труд и высокое качество работы.</p>
                                                            <form data-formtype="contact-form" action="/include/HendlerContact.php" data-toggle="validator" method="post" data-roistat="Контакты - отблагодарить бригаду">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="mail" class="form-control my-input" placeholder="Ваш email" required="" type="email">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input name="captcha_word" value="" placeholder="Введите символы с картинки" type="text">
                                                                                        <input name="captcha_sid" value="<?= $code; ?>" type="hidden">
                                                                                        <img alt="CAPTCHA" src="/bitrix/tools/captcha.php?captcha_sid=<?= $code; ?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" placeholder="Текст..." required=""></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!-- <div id="captcha3" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                            <? if($double_request): ?>
                                                                            Спасибо! Ваше обращение зарегистрировано.<br>
                                                                            Мы обязательно свяжемся с Вами в ближайшее время!
                                                                            <? endif; ?>
                                                                            <? if($request_failed): ?>
                                                                            Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                            Доступ к сервису временно заблокирован!
                                                                            <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на почту):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" name="confirmation-code" class="form-control" autofocus="" type="text">
                                                                        </div>
                                                                        <input value="f3" name="type" type="hidden"><br>
                                                                        <button type="submit" name="btn3" value="f3" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading5">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Обратиться в клиентскую службу</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>По всем вопросам, возникшим во время строительства дома в нашей компании, Вы можете обратиться в клиентскую службу через представленную ниже форму.</p>
                                                            <form data-formtype="contact-form" action="/include/HendlerContact.php" data-toggle="validator" method="post" data-roistat="Контакты - обратиться в клиентскую службу">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="mail" class="form-control my-input" placeholder="Ваш email" required="" type="email">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input name="captcha_word" value="" placeholder="Введите символы с картинки" type="text">
                                                                                        <input name="captcha_sid" value="<?= $code; ?>" type="hidden">
                                                                                        <img alt="CAPTCHA" src="/bitrix/tools/captcha.php?captcha_sid=<?= $code; ?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" placeholder="Вопрос..." required=""></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!--  <div class="g-recaptcha" id="captcha4" data-sitekey="6LeyiQwUAAAAACJ9_CrHgc9lJpXySHFwEdjjJkgy" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                            <? if($double_request): ?>
                                                                            Спасибо! Ваше обращение зарегистрировано.<br>
                                                                            Мы обязательно свяжемся с Вами в ближайшее время!
                                                                            <? endif; ?>
                                                                            <? if($request_failed): ?>
                                                                            Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                            Доступ к сервису временно заблокирован!
                                                                            <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на почту):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" name="confirmation-code" class="form-control" autofocus="" type="text">
                                                                        </div>
                                                                        <input value="f4" name="type" type="hidden"><br>
                                                                        <button type="submit" name="btn4" value="f4" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading6">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                                        <div class="my-table">
                                                            <div>
                                                                <p>Обратиться в рекламный отдел</p>
                                                            </div>
                                                            <div>
                                                                <i class="glyphicon glyphicon-chevron-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12 col-sm-12">
                                                    <div>
                                                        <div class="( my-collapse-content + design-main )">
                                                            <p>Обращения по вопросам сотрудничества и рекламы.</p>
                                                            <form data-formtype="contact-form" action="/include/HendlerContact.php" data-toggle="validator" method="post" data-roistat="Контакты - обратиться в рекламный отдел">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="name" class="form-control my-input" placeholder="Введите ваше имя" required="" type="text">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input name="mail" class="form-control my-input" placeholder="Ваш email" required="" type="email">
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="captcha">
                                                                                        <input name="captcha_word" value="" placeholder="Введите символы с картинки" type="text">
                                                                                        <input name="captcha_sid" value="<?= $code; ?>" type="hidden">
                                                                                        <img alt="CAPTCHA" src="/bitrix/tools/captcha.php?captcha_sid=<?= $code; ?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <textarea type="text" name="msg" resize="none" class="form-control my-input max" placeholder="Текст..." required=""></textarea>
                                                                                </div>
                                                                                <div class="col-xs-12">
                                                                                    <div class="help-block with-errors"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                                        <div class="col-xs-12 col-md-6 attantion">
                                                                            <p class="m-top"><strong>Внимание! </strong>Все поля обязательны для заполнения. </p>
                                                                            <p>Отправляя письмо Вы соглашаетесь с <a href="/privacy-policy/">Политикой обработки данных.</a></p>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <!-- <div class="g-recaptcha" id="captcha5" data-sitekey="6LeyiQwUAAAAACJ9_CrHgc9lJpXySHFwEdjjJkgy" /></div> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span class="respond-message">
                                                                            <? if($double_request): ?>
                                                                            Спасибо! Ваше обращение зарегистрировано.<br>
                                                                            Мы обязательно свяжемся с Вами в ближайшее время!
                                                                            <? endif; ?>
                                                                            <? if($request_failed): ?>
                                                                            Вы три раза подряд неправильно ввели проверочный код.<br>
                                                                            Доступ к сервису временно заблокирован!
                                                                            <? endif; ?>
                                                                        </span>
                                                                        <? if(!$double_request && !$request_failed): ?>
                                                                        <div class="confirmation-code" hidden="hidden">
                                                                            <span class="confirmation-code-hint">Код подтверждения (выслан на почту):</span>&nbsp;&nbsp;
                                                                            <input size="4" maxlength="4" name="confirmation-code" class="form-control" autofocus="" type="text">
                                                                        </div>
                                                                        <input value="f5" name="type" type="hidden"><br>
                                                                        <button type="submit" name="btn5" value="f5" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ</button>
                                                                        <? endif; ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>