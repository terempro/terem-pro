<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает электромонтажные работы на вашем дачном учатске или в частном доме.");
$APPLICATION->SetPageProperty("keywords", "электромонтажные работы на даче, электромонтажные работы на дачном участке");
$APPLICATION->SetPageProperty("title", "Электромонтажные работы на даче: расценки и стоимость");
$APPLICATION->SetTitle("Электромонтажные работы на даче: расценки и стоимость");
?>
<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="text-center text-uppercase">
									<h1>
										Электромонтажные работы
									</h1>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>От профессионализма электриков напрямую зависит надежность работы и долговечность техники в доме. Точность электромонтажных работ определяет степень безопасности всего здания. Заказывайте выполнение электромонтажных работ у профессионалов компании «Теремъ»!</p>
								<img src="/bitrix/templates/.default/assets/img/services/enginering_communications/electromontaj/item-1.jpg" class="full-width" alt="">
								<p>Монтаж типовых электропроводок выполняется открыто в кабель-каналах. Возможны варианты проводок в электро-плинтусе или скрыто на этапе строительства. Электрики компании «Теремъ» обладают необходимыми навыками и обустроят скрытую проводку в деревянном доме с максимальной надежностью.</p>
								<p>Компания «Теремъ» предлагает готовые проекты электроснабжения, которые соответствуют  нормам и правилами прокладки электрических сетей. Электромонтажные работы в компании «Теремъ» выполняются по готовым расчетам, что экономит вложения и время. Монтажники сразу начинают работу, разработка отдельного проекта зачастую не требуется. Доступна электропроводка из стандартного ряда мощностей 3, 5, 8, 10, 12, 15кВт или индивидуальное проектирование по условиям конкретного дома.</p>
								<img src="/bitrix/templates/.default/assets/img/services/enginering_communications/electromontaj/item-2.jpg" class="full-width" alt="">
								<img src="/bitrix/templates/.default/assets/img/services/enginering_communications/electromontaj/item-3.jpg" class="full-width" alt="">
								<p>
								<strong>
								Внимание! Установка всех элементов электросети в домах осуществляется квалифицированными электромонтажниками по СНИиП. Для каждого дома разработаны типовые проекты внутреннего электроснабжения. Будьте бдительны! Обращайтесь в компанию «Теремъ», чтобы все электромонтажные работы выполнялись профессионально!
								</strong>
								</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<section class="content white" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white + my-margin">
						<div role="tabpanel" class="tab-panel ( my-tabpanel &amp;&amp; ( design-1 &amp;&amp; appearance-list-left &amp;&amp; bg-grey &amp;&amp; padding-navs-20 &amp;&amp; padding-tabs-30-30 ))">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">
									ОНЛАЙН ЗАЯВКА
								</a></li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="tab1">
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<p>
												В предложенной ниже форме Вы можете выбрать интересующую Вас услугу. Обращаем Ваше внимание, что выбирать можно столько услуг, сколько Вам требуется. Все, что Вы выбрали, будет отражено в строке «Выбрано» в самом конце данной формы. Оставьте свои контактные данные, и в указанное время с Вами свяжется наш специалист, чтобы задать уточняющие вопросы.
											</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-12 col-sm-12">
											<form class="form-send comunication" data-toggle="validator" action="http://crm.terem-pro.ru/index.php/welcome/postIngener/" method="post" role="form" data-form="send" novalidate="true">
												<div class="form-group">
													<div class="row">
														<div class="col-xs-12 col-md-12 col-sm-12">
															<div class="my-form-title-red text-center text-uppercase">
																<h3>Внутренние работы</h3>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-xs-12 col-md-3 col-sm-3">
															<div class="checkbox bordered">
																<input type="checkbox" id="checkbox1" name="chk[]" value="Отделка дома">
																<label for="checkbox1">
																	Отделка дома
																</label>
															</div>
														</div>
														<div class="col-xs-12 col-md-3 col-sm-3">
															<div class="checkbox bordered">
																<input type="checkbox" id="checkbox2" name="chk[]" value="Отопление">
																<label for="checkbox2">
																	Отопление
																</label>
															</div>
														</div>
														<div class="col-xs-12 col-md-3 col-sm-3">
															<div class="checkbox bordered">
																<input type="checkbox" id="checkbox3" name="chk[]" value="Электрификация">
																<label for="checkbox3">
																	Электрификация
																</label>
															</div>
														</div>
														<div class="col-xs-12 col-md-3 col-sm-3">
															<div class="checkbox bordered">
																<input type="checkbox" id="checkbox4" name="chk[]" value="Канализация">
																<label for="checkbox4">
																	Канализация
																</label>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-xs-12 col-md-12 col-sm-12">
															<div class="checkbox bordered">
																<input type="checkbox" id="checkbox5" name="chk[]" value="Горячее/холодное водоснабжение">
																<label for="checkbox5">
																	Горячее/холодное водоснабжение
																</label>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-xs-12 col-md-12 col-sm-12">
															<div class="selected-items" data-item="checked-items">
																<textarea name="selected-items" data-item="checked-items"></textarea>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-xs-12 col-md-12 col-sm-12">
														<p class="my-form-subtitle"><strong>Контактные данные</strong></p>
													</div>
												</div>
												
												<div class="row">
													<div class="col-xs-12 col-md-4 col-sm-4">
														<div class="form-group">
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<p class="my-label">Ваше имя</p>
																</div>
															</div>
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<input type="text" name="name" class="form-control my-input" placeholder="Ваше имя" required="">
																</div>
															</div>
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<div class="help-block with-errors"></div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-xs-12 col-md-4 col-sm-4">
														<div class="form-group">
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<p class="my-label">Телефон для связи</p>
																</div>
															</div>
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<input type="phone" name="phone" class="form-control my-input" placeholder="Телефон для связи" data-item="phone" required="">
																</div>
															</div>
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<div class="help-block with-errors"></div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-xs-12 col-md-4 col-sm-4">
														<div class="form-group">
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<p class="my-label">Время для связи</p>
																</div>
															</div>
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<select class="form-control my-input" name="time" required="">
																		<option value="c 12:00 до 15:00" selected="">c 12:00 до 15:00</option>
																		<option value="c 15:00 до 17:00">c 15:00 до 17:00</option>
																		<option value="c 17:00 до 20:00">c 17:00 до 20:00</option>
																	</select>
																</div>
															</div>
															<div class="row">
																<div class="col-xs-12 col-md-12 col-sm-12">
																	<div class="help-block with-errors"></div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10">
														<p class="grey">Заявка не накладывает на Вас никаких обязательств, а является запросом только на получение информации для предварительного расчета.</p>
													</div>
													<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
														<button type="submit" class="btn btn-danger disabled" style="pointer-events: all; cursor: pointer;">ОТПРАВИТЬ ЗАЯВКУ</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>