<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "3d забор под ключ");
$APPLICATION->SetPageProperty("keywords", "3d забор");
$APPLICATION->SetTitle("3d забор");
/*$APPLICATION->AddHeadScript("/bitrix/templates/.default/assets/js/fense.js");*/
?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    3D забор
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                           <img src="img/item-1.jpg" class="full-width" alt="">

                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <p>3D забор – металлическое ограждение из оцинкованной стали, которое представляет собой сваренные друг с другом железные прутья с волнообразными изгибами. Уникальная конструкция 3D  заборов обеспечивает этому виду заграждений высокую прочность и долгий срок службы. Благодаря выгодной стоимости и серьёзным охранным характеристикам, 3D заборы пользуются большим спросом у владельцев загородных участков. Они составляют серьёзную конкуренцию популярной в России сетке рабице, которой часто отдают предпочтение из-за более низкой стоимости, но по степени защиты огороженного участка, 3 D заборы всё-таки более надёжны, так как они спроектированы специально для защиты от несанкционированного проникновения.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <h3 style="color: #a3372b;" class="text-uppercase">Почему установку 3D забора выгодно заказать у нас</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <p>За максимально короткие сроки специалисты компании «Теремъ» выполнят все необходимые работы по установке 3D забора на Вашем участке. Многолетний опыт и серьёзный подход к делу позволяют нам с уверенностью утверждать: мы справляемся с подобными задачами на самом высоком уровне. Индивидуальный подход к каждому клиенту, гарантия качества выполнения работ, а также большое количество выгодных предложений - отличают нас от большинства компаний, предоставляющих подобные услуги на строительном рынке. </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <h3 style="color: #a3372b;" class="text-uppercase">Как заказать установку 3D забора у нас</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>Если Вы хотите заказать установку 3D забора у нас, вы можете позвонить нам по телефону: + 7 (495) 721 18 00 или заказать <a href="#" class="modal-call" data-target="#call" data-toggle="modal"><br/>«ОБРАТНЫЙ ЗВОНОК»</a>, и в ближайшее время наши менеджеры свяжутся с Вами.</p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                        <div class="my-table">
                                            <div>
                                                <p>Где эффективны 3D заборы:</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq1" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs )">
                                        <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                            <ul>
                                                                <li>
                                                                    <p>3D заборы часто устанавливают на различных  загородных территориях (дачные участки, жилые дома,   базы отдыха, детские лагеря и т.д.)</p>
                                                                </li>
                                                                <li>
                                                                    <p>Огораживание участков земли, спортивных площадок, детских садов и школ</p>
                                                                </li>
                                                                <li>
                                                                    <p>Отлично подходит для ограждения промышленных и коммерческих зон</p>
                                                                </li>
                                                                
                                                            </ul>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
                                        <div class="my-table">
                                            <div>
                                                <p>Технология установки 3D забора:</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq2" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs )">
                                         <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                            <img src="img/item-2.jpg" class="full-width" alt="">
                                                            <br><br>
                                                                <p>За счёт готовности отдельных секций и специальных креплений, 3 D заборы устанавливаются довольно быстро. Монтаж такого вида ограждения выглядит следующим образом: предварительно делается разметка мест размещения опорных столбов, далее столбы устанавливают и надёжно фиксируют в глубоких скважинах, вырытых для опор забора, далее ставятся панели 3 D ограждения.</p>
                                                                <br><br>
                                                                <p>На первый взгляд, установка данного вида ограждения – дело нехитрое, поэтому многие опрометчиво берутся ставить такой забор сами. В целях экономии Ваших сил и средств мы рекомендуем Вам доверить эту задачу профессионалам компании «Теремъ».</p>
                                                                <p>Мы предлагаем Вам выбрать уникальное цветовое решение 3 D ограждения для Вашего участка в табличке, которую Вы найдёте ниже</p>
                                                                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq3">
                                        <div class="my-table">
                                            <div>
                                                <p>Фото наших 3D-заборов</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq3" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs)">
                                         <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <img src="img/item-3.jpg" class="full-width" alt="">
                                                            <p style="font-size: 14.5px;"><i><small>3D забор крашенный</small></i></p>
                                                        </div>
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <img src="img/item-4.jpg" class="full-width" alt="">
                                                            <p style="font-size: 14.5px;"><i><small>3D забор</small></i></p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <!--<div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                                                            <a href="#" class="btn btn-default text-uppercase fensue-btn-works">наши работы</a>
                                                        </div>
                                                    </div>-->
                                                    <br>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <br><br><br>
                    <div class="row">
                        <div class="col-xs-12 col-md-2 col-sm-3">
                            <a href="/services/enginering_communications/fensecalc/" class="btn btn-default text-uppercase fensue-btn-works no-padding full-width redBtn">Отправить заявку</a>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3 col-md-offset-1 col-sm-offset-1">
                            <button type="button" href="#" class="btn btn-primary fensue-call" data-toggle="modal" data-target="#formAdviceItem" data-price="" data-whatever=""><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>