<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Забор из сетки рабицы под ключ");
$APPLICATION->SetPageProperty("keywords", "забор из сетки рабицы");
$APPLICATION->SetTitle("Забор из сетки рабицы");
/*$APPLICATION->AddHeadScript("/bitrix/templates/.default/assets/js/fense.js");*/
?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    забор из сетки рабицы
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                           <img src="img/item-1.jpg" class="full-width" alt="">

                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <p>Заборы из сетки рабицы – самый распространённый тип металлических заграждений в России. Большой спрос на этот строительный материал обусловлен, прежде всего, сочетанием его демократичной стоимости с хорошими охранными характеристиками. Сетка рабица из оцинкованной проволоки – прочный и долговечный материал, не  покраски и особых условий эксплуатации. Такое ограждение обеспечит Вашему участку безопасность, но вот от посторонних глаз его не спрячет. Если есть задача «закрыть» видимость Ваших владений для соседей и прохожих, рекомендуем рассмотреть вариант ограждений из профнастила (это линк, кликаешь на него и переходишь в раздел профнастила) В остальном заборы из сетки рабицы – идеальный вариант для тех, кто хочет хорошее металлическое ограждение с минимальными затратами на материал и его установку.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <h3 style="color: #a3372b;" class="text-uppercase">Почему установку сетки рабицы выгодно заказать у нас</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <p>За максимально короткие сроки специалисты компании «Теремъ» выполнят все необходимые работы по установке забора из сетки рабицы на Вашем участке. Многолетний опыт и серьёзный подход к делу позволяют нам с уверенностью утверждать: мы справляемся с подобными задачами на самом высоком уровне. Индивидуальный подход к каждому клиенту, гарантия качества выполнения работ, а также большое количество выгодных предложений - отличают нас от большинства компаний, предоставляющих подобные услуги на строительном рынке.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <h3 style="color: #a3372b;" class="text-uppercase">как купить забор из сетки-рабицы у нас</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                          <p>Если Вы хотите заказать установку забора из сетки рабицы у нас, вы можете позвонить нам по телефону: + 7 (495) 721 18 00 или нажать на кнопку «ОТПРАВИТЬ ЗАЯВКУ», и в ближайшее время наши менеджеры свяжутся с Вами.</p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                        <div class="my-table">
                                            <div>
                                                <p>Где эффективны заборы из сетки рабицы</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq1" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs )">
                                        <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                            <ul>
                                                                <li>
                                                                    <p>Такие заборы часто устанавливают на различных  загородных территориях (дачные участки, жилые дома,    базы отдыха, детские лагеря и т.д.)</p>
                                                                </li>
                                                                <li>
                                                                    <p>Огораживание участков земли, спортивных площадок, детских садов и школ.</p>
                                                                </li>
                                                                <li>
                                                                    <p>Отлично подходят для ограждения промышленных и коммерческих зон.</p>
                                                                </li>
                                                                
                                                            </ul>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
                                        <div class="my-table">
                                            <div>
                                                <p>Технология установки заборов из сетки рабицы</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq2" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs )">
                                         <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12">
                                                            <img src="img/item-2.jpg" class="full-width" alt=""><br><br>
                                                                <p>Технология установки данного вида заграждений выглядит довольно просто: забить столбы и натянуть рабицу, используя крюки на столбах. И всё же самостоятельно такое заграждение лучше не устанавливать. Во-первых, важно правильно и крепко поставить столбы, на которые будет крепиться сетка, во-вторых, велика вероятность слабого натяжения рабицы. Эта проблема может сделать Ваш забор неэстетичным и, что хуже всего, неэффективным. Поэтому, в целях экономии Ваших сил и средств, мы рекомендуем Вам доверить эту задачу профессионалам компании «Теремъ».</p>
                                                                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq3">
                                        <div class="my-table">
                                            <div>
                                                <p>Фото наших заборов из сетки-рабицы</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-up"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="my-collapse collapse" id="faq3" aria-expanded="false">
                                        <div class="( my-collapse-content + design-main fensue-padding-tabs)">
                                         <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <img src="img/item-3.jpg" class="full-width" alt="">
                                                            <p style="font-size: 14.5px;"><i><small>Сетка-рабица </small></i></p>
                                                        </div>
                                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                                            <img src="img/item-4.jpg" class="full-width" alt="">
                                                            <p style="font-size: 14.5px;"><i><small>Сетка-рабица на производстве перед началом изготовления изгороди</small></i></p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                                                            <a href="#" class="btn btn-default text-uppercase fensue-btn-works">наши работы</a>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                             <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <br><br><br>
                            <div class="row">
                                <div class="col-xs-12 col-md-2 col-sm-3">
                                    <a href="/services/enginering_communications/fensecalc/" class="btn btn-default text-uppercase fensue-btn-works no-padding full-width redBtn">Отправить заявку</a>
                                </div>
                                <div class="col-xs-12 col-md-3 col-sm-3 col-md-offset-1 col-sm-offset-1">
                                    <button type="button" href="#" class="btn btn-primary fensue-call" data-toggle="modal" data-target="#formAdviceItem" data-price="" data-whatever=""><i class="glyphicon glyphicon-earphone"></i><span>Получить бесплатную<br>консультацию</span></button>
                                </div>
                            </div> -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>