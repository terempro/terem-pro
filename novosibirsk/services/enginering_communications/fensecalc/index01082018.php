<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "терем заборы, терем ограждения, заборы, ограждения");
$APPLICATION->SetPageProperty("title", "Ограждения от компании Теремъ, заборы из сетки-рабицы, профнастила и кирпича");
$APPLICATION->SetTitle("Ограждения от компании Теремъ, заборы из сетки-рабицы, профнастила и кирпича");

?>
<section class="content text-page white + my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side fence-padding clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                    Ограждения
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>Неприкосновенность частной жизни и личная безопасность – вот то, к чему стремится каждый человек в наше время. В этом может помочь качественное и надежное ограждение собственного участка.</p>
                            <p>Специально для своих клиентов компания «Теремъ» подготовила выгодное ценовое предложение по установке качественного и надежного ограждения участка.</p>
                            <!--<p>НОВИНКА! 3D забор – всего от 1 100 руб. /п.м. Идеально подходит для ограждения участка со стороны соседей, не создавая ощущение замкнутого пространства, но обеспечивая надежную защиту дому. Образец данного ограждения представлен на площадке, и Вы сможете лично убедиться в его преимуществах!</p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content my-margin" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12" id="category">
                <div class="flex-reversed">
                    <!--OLD catalog list-->
                    <!--small villages -->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/fenseu/item-1.jpg" alt="Евроштакетник">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="">
                                        ЕВРОШТАКЕТНИК<br>от 1 485 РУБ. за п/м
                                    </h4>
                                    <p>
                                        Ограждения из евроштакетника – выбор тех, кто ценит не только надёжность, но и эстетичность в оформлении своего участка. Красивые заборы из евроштакетника обеспечат вашему загородному владению долгосрочную защиту и презентабельный внешний вид.</p>
                                </div>
                                <a class="all-item-link" href="/services/enginering_communications/fensecalc/evroshtaketnik/"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/fenseu/item-2.jpg" alt="ПРОФНАСТИЛ">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="">
                                        ПРОФНАСТИЛ<br>от 900 РУБ. за п/м
                                    </h4>
                                    <p>
                                        Благодаря своей прочности, высокой износостойкости, а также сдержанному внешнему виду,
                                        ограждения из профнастила пользуются большой популярностью в загородном строительстве.
                                        Такой забор подойдёт тем, кто хочет защитить свои владения от посторонних глаз.
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/enginering_communications/fensecalc/profnastil/"></a>
                            </div>
                        </div>
                    </div><!--end-->
                    <!--small villages -->
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/fenseu/item-3.jpg" alt="3D забор">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        3D ЗАБОР<br>от 2 565 РУБ. за п/м
                                    </h4>
                                    <p>
                                        <strong>3D забор</strong> – прекрасное современное охранное ограждение, которое отличается высокой

                                        прочностью, устойчивостью и долговечностью. Отличные защитные характеристики и ценовая

                                        доступность обеспечивают высокий спрос  на данный тип заборов.
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/enginering_communications/fensecalc/3dzabor/"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="my-promotion desgin-3 + my-margin">
                                <div>
                                    <a href="#">
                                        <img src="/bitrix/templates/.default/assets/img/fenseu/item-4.jpg" alt="СЕТКА-РАБИЦА">
                                    </a>
                                </div>
                                <div>
                                    <h4 class="text-uppercase">
                                        СЕТКА РАБИЦА<br>от 324 РУБ. за п/м
                                    </h4>
                                    <p>
                                        Сетка рабица – один из самых распространённых типов проволочных заграждений. Обнести свои

                                        владения сеткой рабицей – недорогой и действенный способ обеспечить свой участок не только

                                        защитой, но и солнечным светом.
                                    </p>
                                </div>
                                <a class="all-item-link" href="/services/enginering_communications/fensecalc/rabicazabor/"></a>
                            </div>
                        </div>
                    </div><!--end-->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content text-page white" role="content" style="margin-top:-5px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12 my-margin">
                <div class="white padding-side fence-padding clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-red">
                                <h2>
                                    ПРЕИМУЩЕСТВА ЗАКАЗА в «ТЕРЕМЕ»
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Бесплатная доставка 250 км от базы
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Работа без предоплаты на стандартный профиль С8
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Бесплатная консультация нашего специалиста
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="fencue-item">
                                Быстрый монтаж и демонтаж старого забора
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p class="text-padding-bottom"> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12 my-margin">
                <div class="white padding-side fence-padding clearfix">
                    <div class="row">
                        <div class="my-text-title text-uppercase desgin-h1">
                            <h2>
                                Оставить заявку
                            </h2>
                            <p>Для отправки заявки заполните пожалуйста все поля.</p>
                        </div>
                        <form id="fenceForm" class="form-send form-fense my-margin new" data-toggle="validator" action="/include/request_handler.php" method="post" data-roistat="Заявка на забор">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label>Введите ваше имя</label>
                                                <input type="text" name="name" class="form-control my-input" placeholder="Иванов Иван Иванович" required="">
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label>Введите телефон</label>
                                                <input type="t" placeholder="8 900 000 00 00"  name="phone" class="form-control my-input"  placeholder="Введите ваш телефон" required="">
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row__with-btn">
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label>Тип забора</label>
                                                <select required name="fence_type" class="form-control my-input">
                                                    <option value="" disabled selected>Выберите тип</option>
                                                    <option value="Евроштакетник">Евроштакетник</option>
                                                    <option value="Профнастил">Профнастил</option>
                                                    <option value="3D Забор">3D забор</option>
                                                    <option value="Сетка рабица">Сетка рабица</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <input type="hidden" name="request_type" value="5">
                                                <button type="submit"  onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;" class="btn btn-danger text-uppercase">Отправить заявку</button>

                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="form-attantion">
                                *Расчет расстояния доставки производится от производственной базы компании «Теремъ», находящейся по адресу: Московская обл., деревня Нижнее Велино, участок 113. В радиусе 250 км. от производственной базы доставка осуществляется бесплатно. Если Ваш участок находиться дальше, то  каждый дополнительный километр тарифицируется по стоимости – 25 руб./км.
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
