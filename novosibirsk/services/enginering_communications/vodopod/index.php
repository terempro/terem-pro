<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "деревянные дома, строительство деревянных домов, терем, деревянные дома под ключ, деревянные дома москва, стоимость деревянного дома, terem");
$APPLICATION->SetPageProperty("title", "Системы и схемы водоснабжения населенных пунктов");
$APPLICATION->SetTitle("Системы и схемы водоснабжения населенных пунктов");
?>
<section class="content text-page white + my-margin" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side clearfix">
					
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<div class="text-center text-uppercase">
								<h1>
									Водоподъем
								</h1>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p>Монтаж системы водоснабжения – сложный и очень ответственный процесс. Компания «Теремъ» предлагает услуги по прокладке зимнего водопровода и подъема воды из Вашего (существующего на участке) колодца. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<img src="/bitrix/templates/.default/assets/img/services/enginering_communications/vodopod/item-1.jpg" class="full-width" alt="">
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
</section>
	
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>