<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Прокладка инженерных коммункаций для частных домов под ключ от компании Терем");
$APPLICATION->SetPageProperty("keywords", "инженерные коммуникации, инженерные коммуникации в деревянном доме, заказать проект инженерных коммуникаций");
$APPLICATION->SetPageProperty("title", "Инженерные системы и коммуникации под ключ");
$APPLICATION->SetTitle("Инженерные системы и коммуникации под ключ");
?>
<section class="content text-page white + my-margin" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white padding-side clearfix">
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<div class="text-center text-uppercase">
								<h1>
									Инженерные системы –<br/>городской комфорт в загородном доме
								</h1>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-sm-12">
							<p>
								В современном мире инженерные коммуникации помогают нам полноценно жить и удовлетворять свои потребности. Компания «Теремъ» оказывает услуги по установке загородных инженерных коммуникаций. У компании имеются ресурсы, опыт и возможности для выполнения работ по монтажу инженерных сетей в деревянных домах различного типа и размера.
							</p>
							<p><strong>Мы поможем Вам подобрать новейшее высокотехнологичное оборудование. Наши специалисты сделают Ваш дом современным и уютным!</strong></p>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="my-service-item my-table full-width + my-margin">
								<div class="my-table-row min-height">
									<div class="my-table-cell">
										<a><img src="/bitrix/templates/.default/assets/img/services/enginering_communications/item-1.jpg"></a>
									</div>
								</div>
								<div class="my-table-row">
									<div class="my-table-cell my-appearance-top">
										<div class="inner text-uppercase">
											<h2><a>Электромонтажные работы</a></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="my-service-item my-table full-width + my-margin">
								<div class="my-table-row min-height">
									<div class="my-table-cell">
										<a><img src="/bitrix/templates/.default/assets/img/services/enginering_communications/item-2.jpg"></a>
									</div>
								</div>
								<div class="my-table-row">
									<div class="my-table-cell my-appearance-top">
										<div class="inner text-uppercase">
											<h2><a>Внутренние инженерные работы</a></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="my-service-item my-table full-width + my-margin">
								<div class="my-table-row min-height">
									<div class="my-table-cell">
										<a><img src="/bitrix/templates/.default/assets/img/services/enginering_communications/item-3.jpg"></a>
									</div>
								</div>
								<div class="my-table-row">
									<div class="my-table-cell my-appearance-top">
										<div class="inner text-uppercase">
											<h2><a>Отопление</a><br><br></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="my-service-item my-table full-width + my-margin">
								<div class="my-table-row min-height">
									<div class="my-table-cell">
										<a><img src="/bitrix/templates/.default/assets/img/services/enginering_communications/item-4.jpg"></a>
									</div>
								</div>
								<div class="my-table-row">
									<div class="my-table-cell my-appearance-top">
										<div class="inner text-uppercase">
											<h2><a>Cептики</a><br><br></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="my-service-item my-table full-width + my-margin">
								<div class="my-table-row min-height">
									<div class="my-table-cell">
										<a><img src="/bitrix/templates/.default/assets/img/services/enginering_communications/item-7.jpg"></a>
									</div>
								</div>
								<div class="my-table-row">
									<div class="my-table-cell my-appearance-top">
										<div class="inner text-uppercase">
											<h2><a>Ограждения</a><br><br></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-4 col-sm-4">
							<div class="my-service-item my-table full-width + my-margin">
								<div class="my-table-row min-height">
									<div class="my-table-cell">
										<a><img src="/bitrix/templates/.default/assets/img/services/enginering_communications/item-5.jpg"></a>
									</div>
								</div>
								<div class="my-table-row">
									<div class="my-table-cell my-appearance-top">
										<div class="inner text-uppercase">
											<h2><a>водоподъем</a><br><br></h2>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>

	</div>
</section>

<? /*<section class="content white" role="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12">
				<div class="white + my-margin">
					<div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-left && bg-grey && padding-navs-20 && padding-tabs-30-30 ))">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">
								ОНЛАЙН ЗАЯВКА
							</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tab1">
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<p>
											В предложенной ниже форме Вы можете выбрать интересующую Вас услугу. Обращаем Ваше внимание, что выбирать можно столько услуг, сколько Вам требуется. Все, что Вы выбрали, будет отражено в строке «Выбрано» в самом конце данной формы. Оставьте свои контактные данные, и в указанное время с Вами свяжется наш специалист, чтобы задать уточняющие вопросы.
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 col-sm-12">
										<form class="form-send comunication" data-toggle="validator" action="https://crm.terem-pro.ru/index.php/welcome/postIngener/" method="post" role="form" data-form="send">
											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-md-12 col-sm-12">
														<div class="my-form-title-red text-center text-uppercase">
															<h3>Внутренние работы</h3>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox1" name="chk[]" value="Отделка дома">
															<label for="checkbox1">
																Отделка дома
															</label>
														</div>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox2" name="chk[]" value="Отопление">
															<label for="checkbox2">
																Отопление
															</label>
														</div>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox3" name="chk[]" value="Электрификация">
															<label for="checkbox3">
																Электрификация
															</label>
														</div>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox4" name="chk[]" value="Канализация">
															<label for="checkbox4">
																Канализация
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-md-12 col-sm-12">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox5" name="chk[]" value="Горячее/холодное водоснабжение">
															<label for="checkbox5">
																Горячее/холодное водоснабжение
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-md-12 col-sm-12">
														<div class="my-form-title-red text-center text-uppercase">
															<h3>Наружные работы</h3>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox6" name="chk[]" value="Септик">
															<label for="checkbox6">
																Септик
															</label>
														</div>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox7" name="chk[]" value="Ввод воды">
															<label for="checkbox7">
																Ввод воды
															</label>
														</div>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox8" name="chk[]" value="Газгольдеры">
															<label for="checkbox8">
																Газгольдеры
															</label>
														</div>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox9" name="chk[]" value="Электричество в грунте">
															<label for="checkbox9">
																Электричество в грунте
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-md-12 col-sm-12">
														<div class="my-form-title-red text-center text-uppercase">
															<h3>Обустройство участка</h3>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox10" name="chk[]" value="Забор">
															<label for="checkbox10">
																Забор
															</label>
														</div>
													</div>
													<div class="col-xs-12 col-md-3 col-sm-3">
														<div class="checkbox bordered">
															<input type="checkbox" id="checkbox11" name="chk[]" value="Дорожки/отмостки">
															<label for="checkbox11">
																Дорожки/отмостки
															</label>
														</div>
													</div>

												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-md-12 col-sm-12">
														<div class="selected-items" data-item="checked-items">
															<textarea name="selected-items" data-item="checked-items"></textarea>
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 col-md-12 col-sm-12">
													<p class="my-form-subtitle"><strong>Контактные данные</strong></p>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 col-md-4 col-sm-4">
													<div class="form-group">
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<p class="my-label">Ваше имя</p>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<input type="text" name="name"  class="form-control my-input" placeholder="Ваше имя" required>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<div class="help-block with-errors"></div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-md-4 col-sm-4">
													<div class="form-group">
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<p class="my-label">Телефон для связи</p>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<input type="phone"  name="phone" class="form-control my-input" placeholder="8 920 000 00 00" required>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<div class="help-block with-errors"></div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-md-4 col-sm-4">
													<div class="form-group">
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<p class="my-label">Время для связи</p>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<select class="form-control my-input" name="time" required>
																	<option value="c 12:00 до 15:00" selected>c 12:00 до 15:00</option>
																	<option value="c 15:00 до 17:00">c 15:00 до 17:00</option>
																	<option value="c 17:00 до 20:00">c 17:00 до 20:00</option>
																</select>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-12 col-md-12 col-sm-12">
																<div class="help-block with-errors"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10">
													<p class="grey">Заявка не накладывает на Вас никаких обязательств, а является запросом только на получение информации для предварительного расчета.</p>
												</div>
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p><strong>Внимание!</strong></p>
                            <p>Отправляя форму Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">Политикой обработки данных.</a></p>
                        </div>
												<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                                                                                                        <input type="hidden" name="request_type" value="3">
													<button type="submit" class="btn btn-danger"  onclick="yaCounter937330.reachGoal('ONE_OF_FORMS_FILLED'); ga('send', 'event', 'button', 'click'); return true;">ОТПРАВИТЬ ЗАЯВКУ</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>*/?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
