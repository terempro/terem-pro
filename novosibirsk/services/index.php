<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "реконструкция домов, достройка домов, пристройка террас");
$APPLICATION->SetPageProperty("title", "Услуги по возведению и реконструкции малоэтажных загородных домов");
$APPLICATION->SetTitle("Услуги по возведению и реконструкции малоэтажных загородных домов ");
?>
<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="text-center text-uppercase">
									<h1>
										СТРОИТЕЛЬСТВО ЗАГОРОДНЫХ ДОМОВ
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>
									Наши специалисты работают на всех этапах строительства, начиная от монтажа фундамента, заканчивая наружной и внутренней отделкой.
								</p>
								<p>
									«Теремъ» заботится о том, чтобы процесс строительства прошел как можно более комфортно для наших клиентов. Мы используем современные технологии, которые позволяют максимально сократить срок строительства, сохранив высокое качество и индивидуальный подход к каждому проекту.
								</p>
								<br>
								<br>
							</div>
						</div>
						
						<div class="row my-flex">
							<!--div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div class="my-table-row">
										<div>
											<a href="/services/dostroika/"><img src="/bitrix/templates/.default/assets/img/services/item-1.jpg"></a>
										</div>
									</div>
									<div >
										
											<div class="inner text-uppercase">
												<h2><a href="/services/dostroika/">достройка<br>и реконструкция</a></h2>
											</div>
											<div class="inner">
												
												<p>
													Мы сделаем Ваш дом таким, каким хотите его видеть Вы
												</p>
											</div>
									</div>
									
								</div>
							</div-->
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/services/enginering_communications/"><img src="/bitrix/templates/.default/assets/img/services/item-2.jpg"></a>
									
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/services/enginering_communications/">Инженерные системы</a></h2>
											</div>
										
											<div class="inner">
											
												<p>
													Наши специалисты сделают Ваш дом современным и уютным
												</p>
											</div>
									</div>
									
								</div>
							</div>
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/services/the-estates-catalogue/"><img src="/bitrix/templates/.default/assets/img/services/item-4.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/services/the-estates-catalogue/">Благоустройство и усадьбы</a></h2>
											</div>
										<div class="inner">
											
												<p>
													Решение для тех, кто хочет жить в доме своей мечты
												</p>
											</div>
									</div>
									
								</div>
							</div>
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									
										<div>
											<a href="/services/credit-calculator/"><img src="/bitrix/templates/.default/assets/img/services/item-7.jpg"></a>
										</div>
									
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/services/credit-calculator/">Услуги кредитования и страхования</a></h2>
											</div>
											<div class="inner">
											
												<p>
													Взять кредит или застраховать дом можно на выставке «Теремъ»
												</p>
											</div>
									</div>
									
								</div>
							</div>
							<!--div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/services/individual_projects/"><img src="/bitrix/templates/.default/assets/img/services/item-3.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/services/individual_projects/">Индивидуальные<br>проекты</a></h2>
											</div>
										<div class="inner">
											
												<p>
													Наши архитекторы разработают проект с учетом всех ваших пожеланий
												</p>
											</div>
									</div>
									
								</div>
							</div-->
						</div>
						<!--div class="row my-flex">
							
							<!--div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/services/brick-house/"><img src="/bitrix/templates/.default/assets/img/services/item-5.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/services/brick-house/">Кирпичные дома</a></h2>
											</div>
										<div class="inner">
											
												<p>
													Красивые и надежные дома из кирпича «под ключ»
												</p>
											</div>
									</div>
									
								</div>
							</div-->
							<!--div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/services/addition2/"><img src="/bitrix/templates/.default/assets/img/services/item-6.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/services/addition2/">Пристройки террас<br>и веранд</a></h2>
											</div>
										<div class="inner">
											
												<p>
													Поможет увеличить площадь и придать дому архитектурную завершенность
												</p>
											</div>
									</div>
									
								</div>
							</div-->
						<!--/div-->
						<div class="row my-flex">
							
							<div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/about/news/zemelnye-uchastki-v-kompanii-terem/" target="_blank"><img src="/bitrix/templates/.default/assets/img/services/item-8.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/about/news/zemelnye-uchastki-v-kompanii-terem/" target="_blank">Земельные участки</a></h2>
											</div>
											<div class="inner">
												<p>
													Мы найдем для Вас уютный уголок
												</p>
											</div>
									</div>
								</div>
							</div>
                            <div class="col-xs-12 col-md-4 col-sm-4 my-flex">
								<div class="my-service-item my-flex full-width + my-margin">
									<div>
										
											<a href="/services/enginering_communications/fensecalc/"><img src="/upload/zabori.jpg"></a>
										
									</div>
									<div>
										
											<div class="inner text-uppercase">
												<h2><a href="/services/enginering_communications/fensecalc/">Заборы</a></h2>
											</div>
											<div class="inner">
												<p>
													Неприкосновенность частной жизни и личная безопасность – вот то, к чему стремится каждый человек в наше время.
												</p>
											</div>
									</div>
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<br>
								<br>
								<p>
									Наши специалисты имеют высокий уровень квалификации, их грамотный и рациональный подход поможет все тщательно спланировать и просчитать. Они помогут Вам лучше осознать свои желания и в конечном итоге переехать жить в дом своей мечты!
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>