<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Усадьбы");
?>
<section class="content text-page white + my-margin" role="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="white padding-side clearfix">
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<div class="my-text-title text-center text-uppercase desgin-h1">
									<h1>
										Собери сам
									</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12">
								<p>Компания «Теремъ» готова построить для Вас усадьбу «под ключ» в любой комплектации на Вашем участке или предложить Вам участок под нее. </p>
								<p>Заполните форму и узнайте, сколько стоит полноценная усадьба с учетом всех Ваших пожеланий. Архитектор нашей компании свяжется с Вами и ответит на самые волнующие вопросы: что и сколько стоит, как долго строить и когда въезжать.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>

	<section class="content white catalog-item-1 content-catalog-item active overflow-v designer-content" role="content" data-item="1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="catalog-item-gallery white + my-margin">
						<div role="tabpanel" class="tab-panel ( my-tabpanel && ( design-1 && appearance-list-center && bg-grey && padding-20 ))">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active">
									<a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">
										<div class="my-flex">
											<div><h3>ШАГ 1 </h3></div>
											<div><p>Основное<br>строение</p></div>
										</div>
									</a>
								</li>
								<li role="presentation">
									<a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">
										<div class="my-flex">
											<div><h3>ШАГ 2 </h3></div>
											<div><p>Дополнительные <br>строения</p></div>
										</div>
									</a>
								</li>
								<li role="presentation">
									<a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">
										<div class="my-flex">
											<div><h3>ШАГ 3 </h3></div>
											<div><p>Земельный <br> участок</p></div>
										</div>
									</a>
								</li>
								<li role="presentation">
									<a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">
										<div class="my-flex">
											<div><h3>ШАГ 4 </h3></div>
											<div><p>Оформить <br> заявку</p></div>
										</div>
									</a>
								</li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
                                <form class="form-send" data-toggle="validator" method="post" role="form" data-form="send">
    								<div role="tabpanel" class="tab-pane active" id="tab1">
    									<div class="row">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<div class="my-table">
    												<div><img src="/bitrix/templates/.default/assets/img/services/item-24.jpg" alt=""></div>
    												<div>
    													<h2>Основное строение</h2>
    													<p class="grey"><small>Укажите, пожалуйста, площадь и тип желаемого дома, а также материал и наличие инженерных коммуникаций.</small></p>
    												</div>
    											</div>
    											<hr>
    										</div>
    									</div>
    									<div class="row">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<div class="row">
    												<!--left col form -->
    												<div class="col-xs-12 col-md-6 col-sm-6">
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Площадь</h3>
    															<p class="grey"><small>Чем больше площадь дома, тем больше в ней будет просторных комнат. Она также влияет на размер гостиной, прихожей, количество санузлов.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox1" required="">
    																			<label for="checkbox1">
    																				до 6x6 м
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox2" required="">
    																			<label for="checkbox2">
    																				от 6x6 до 6x9 м
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox3" required="">
    																			<label for="checkbox3">
    																				более 6x9 м
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Материал</h3>
    															<p class="grey"><small>Если Вам необходим надежный дом в кратчайшие сроки и по доступной цене, то стоит остановиться на каркасном методе строительства. Если же Вам милее привычные дома из бруса, то стоит обратить внимание на этот материал.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				брус
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio2" required="">
    																			<label for="radio2">
    																				каркас
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Тип дома</h3>
    															<p class="grey"><small>Дома для постоянного проживания имеют качественное утепление и подходят для жизни даже в самые холодные зимы. Дома для сезонного проживания используются в качестве дачного варианта.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox4" required="">
    																			<label for="checkbox4">
    																				постоянное проживание
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox5" required="">
    																			<label for="checkbox5">
    																				сезонное проживание
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														
    													</div>
    												</div><!--left col form END-->
    												<!--right col form -->
    												<div class="col-xs-12 col-md-6 col-sm-6">
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Количество проживающих</h3>
    															<p class="grey"><small>Один из основных определяющих факторов при выборе проекта, исходя из общей площади дома и количества комнат.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox4" required="">
    																			<label for="checkbox4">
    																				семья до 4-х человек
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox5" required="">
    																			<label for="checkbox5">
    																				семья более 4-х человек
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Отделка</h3>
    															<p class="grey"><small>Каждый дом в компании «Теремъ» имеют базовую отделку. В данной опции можно выбрать дополнительную чистовую наружную и внутреннюю отделку, в соответствии с дизайнерским проектом.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox4" required="">
    																			<label for="checkbox4">
    																				наружная
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox5" required="">
    																			<label for="checkbox5">
    																				внутренняя
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Инженерные коммуникации для дома</h3>
    															<p class="grey"><small>Мы оказываем услуги по установке всех инженерных коммуникаций.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox4" required="">
    																			<label for="checkbox4">
    																				Да
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox5" required="">
    																			<label for="checkbox5">
    																				Нет
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														
    													</div>
    												</div><!--right col form END-->
    											</div>
    										</div>
    									</div>
    									<div class="row text-right">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" class="btn btn-default">Далее</a>
    										</div>
    									</div>
    								</div>
    								<div role="tabpanel" class="tab-pane" id="tab2">
    									<div class="row">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<div class="my-table">
    												<div><img src="/bitrix/templates/.default/assets/img/services/item-24.jpg" alt=""></div>
    												<div>
    													<h2>Дополнительное строение</h2>
    													<p class="grey"><small>Поставьте галочки напротив дополнительных строений, которые Вы хотите видеть на своем участке.</small></p>
    												</div>
    											</div>
    											<hr>
    										</div>
    									</div>
    									<div class="row">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<div class="row">
    												<!--left col form -->
    												<div class="col-xs-12 col-md-6 col-sm-6">
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Гараж</h3>
    															<p class="grey"><small>Вы можете включить в Ваш проект гараж с окном и роллетными воротами.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox11" required="">
    																			<label for="checkbox11">
    																				
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Баня</h3>
    															<p class="grey"><small>Недалеко от дома можно построить качественную баню для полноценного отдыха с семьей и друзьями.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Площадь:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				до 5х5 м <sub>2</sub>
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				более 5х5 м <sub>2</sub>
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Материал:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				брус
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				каркас
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Отделка:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				наружная
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				внутренняя
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox red bordered">
    																			<input type="checkbox" id="checkbox1" required="">
    																			<label for="checkbox1">
    																				Инженерные коммуникации для бани
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Барбекю</h3>
    															<p class="grey"><small>Место для отдыха на свежем воздухе. В беседке-барбекю можно собраться большой компанией или семьей для вкусного ужина с ароматом дымка.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox11" required="">
    																			<label for="checkbox11">
    																				
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    												</div><!--left col form END-->
    												<!--right col form -->
    												<div class="col-xs-12 col-md-6 col-sm-6">
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Гостевой дом</h3>
    															<p class="grey"><small>Опция включает строительство дополнительного дома из нашего каталога. Это может быть как небольшой садовый дом, так и классическая дача. Отлично подойдет для размещения гостей.</small></p>
    														</div>
    													</div>
    													
    													<div class="row">
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Площадь:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				до 6х6 м <sub>2</sub>
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				до 6х9 м <sub>2</sub>
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				более 6x9 м <sub>2</sub>
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Материал:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				брус
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				каркас
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-4 col-sm-4">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Отделка:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				наружная
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				внутренняя
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Тип дома:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				постоянное проживание <sub>2</sub>
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				сезонное проживание <sub>2</sub>
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Количество проживающих:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				семья до 4-х человек
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				семья более 4-х человек
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox red bordered">
    																			<input type="checkbox" id="checkbox1" required="">
    																			<label for="checkbox1">
    																				Инженерные коммуникации для бани
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    												</div><!--right col form END-->
    											</div>
    										</div>
    									</div>
    									<div class="row text-right">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" class="btn btn-default">Назад</a>
    											<a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" class="btn btn-default">Далее</a>
    										</div>
    									</div>
    								</div>
    								<div role="tabpanel" class="tab-pane" id="tab3">
    									<div class="row">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<div class="my-table">
    												<div><img src="/bitrix/templates/.default/assets/img/services/item-24.jpg" alt=""></div>
    												<div>
    													<h2>Земельный участок</h2>
    													<p class="grey"><small>Если у Вас есть свой земельный участок, укажите его параметры. Если Вы хотите приобрести усадьбу вместе с участком, то приблизительно отметьте желаемую удаленность и его площадь. Также не забудьте поставить галочки, если хотите добавить представленные ниже параметры к Вашей усадьбе.</small></p>
    												</div>
    											</div>
    											<hr>
    										</div>
    									</div>
    									<div class="row">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<div class="row">
    												<!--left col form -->
    												<div class="col-xs-12 col-md-6 col-sm-6">
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Подобрать участок</h3>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Расстояние от МКАД::</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				до 60 км
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				до 100 км
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				до 135 км
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Площадь:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				менее 10 соток
    																			</label>
    																		</div>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="radio bordered">
    																			<input type="radio" name="radio-btn-1" id="radio1" required="">
    																			<label for="radio1">
    																				более 10 соток
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Свой участок</h3>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Расстояние от МКАД::</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="input-group">
    																			<input type="text" class="form-control">
    																			<span class="input-group-addon">км</span>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    														<div class="col-xs-12 col-md-6 col-sm-6">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<h4>Площадь:</h4>
    																	</div>
    																</div>
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="input-group">
    																			<input type="text" class="form-control">
    																			<span class="input-group-addon">соток</span>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Ландшафтный дизайн</h3>
    															<p class="grey"><small>Тщательно разработанный ландшафтный дизайн преобразит Ваш участок, добавив ему целостности и эстетической привлекательности.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox11" required="">
    																			<label for="checkbox11">
    																				
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    												</div><!--left col form END-->
    												<!--right col form -->
    												<div class="col-xs-12 col-md-6 col-sm-6">
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Забор с воротами</h3>
    															<p class="grey"><small>Обеспечивает безопасность участка, а также удобный въезд транспорта.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox11" required="">
    																			<label for="checkbox11">
    																				
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Устройство бетонной отмостки</h3>
    															<p class="grey"><small>Устройство бетонной отмостки обеспечивает защиту от дождя и сырости, помогает продлить срок службы фундамента, цоколя, стен дома.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox11" required="">
    																			<label for="checkbox11">
    																				
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Пешеходные дорожки</h3>
    															<p class="grey"><small>Оборудование необходимого количества пешеходных дорожек по территории всего участка.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox11" required="">
    																			<label for="checkbox11">
    																				
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Площадка под автомобиль</h3>
    															<p class="grey"><small>Персональная парковка возле Вашего дома для транспорта семьи и гостей.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox11" required="">
    																			<label for="checkbox11">
    																				
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    													<hr>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<h3>Желоб для отвода вод</h3>
    															<p class="grey"><small>Выводит лишнюю воду в определенное (по желанию заказчика) место.</small></p>
    														</div>
    													</div>
    													<div class="row">
    														<div class="col-xs-12 col-md-12 col-sm-12">
    															<div class="form-group">
    																<div class="row">
    																	<div class="col-xs-12">
    																		<div class="checkbox bordered">
    																			<input type="checkbox" id="checkbox11" required="">
    																			<label for="checkbox11">
    																				
    																			</label>
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    												</div><!--right col form END-->
    											</div>
    										</div>
    									</div>
    									<div class="row text-right">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" class="btn btn-default">Назад</a>
    											<a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab" class="btn btn-default">Далее</a>
    										</div>
    									</div>
    								</div>
    								<div role="tabpanel" class="tab-pane" id="tab4">
    									<div class="row">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<div class="my-table">
    												<div><img src="/bitrix/templates/.default/assets/img/services/item-24.jpg" alt=""></div>
    												<div>
    													<h2>оформить заявку</h2>
    													<p class="grey"><small>Оставьте свои контактные данные, и в указанное время с Вами свяжется наш менджер. Напоминаем, что заявка не накладывает на Вас никаких обязательств, а является запросом только на получение информации и расчетов.</small></p>
    												</div>
    											</div>
    											<hr>
    										</div>
    									</div>
    									<div class="row">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<h3>Контактные данные</h3>
    										</div>
    									</div>
    									<div class="row">
    										<div class="col-xs-12 col-md-4 col-sm-4">
    											<div class="form-group">
    												<div class="row">
    													<div class="col-xs-12">
    														<label>Ваше имя: </label>
    													</div>
    													<div class="col-xs-12">
    														<input type="text" name="user" class="form-control" placeholder="Иван Иванов" required/>
    													</div>
    													<div class="col-xs-12">
    														<div class="help-block with-errors"></div>
    													</div>
    												</div>
    											</div>
    										</div>
    										<div class="col-xs-12 col-md-4 col-sm-4">
    											<div class="form-group">
    												<div class="row">
    													<div class="col-xs-12">
    														<label>Телефон для связи: </label>
    													</div>
    													<div class="col-xs-12">
    														<input type="text" class="form-control" data-item="phone" placeholder="+7 (999) 999 99 99" name="phone" autocomplete="off">
    													</div>
    													<div class="col-xs-12">
    														<div class="help-block with-errors"></div>
    													</div>
    												</div>
    											</div>
    										</div>
    										<div class="col-xs-12 col-md-4 col-sm-4">
    											<div class="form-group">
    												<div class="row">
    													<div class="col-xs-12">
    														<label>Удобное время для звонка: </label>
    													</div>
    													<div class="col-xs-12">
    														<select name="" id="" class="form-control">
    															<option value="10-13">10:00 - 13:00</option>
    															<option value="11-16">13:00 - 16:00</option>
    															<option value="12-18">16:00 - 18:00</option>
    															<option value="13-20">18:00 - 20:00</option>
    														</select>
    													</div>
    													<div class="col-xs-12">
    														<div class="help-block with-errors"></div>
    													</div>
    												</div>
    											</div>
    										</div>
    									</div>
    									<div class="row text-right">
    										<div class="col-xs-12 col-md-12 col-sm-12">
    											<a  href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" class="btn btn-default">Назад</a>
    											<button type="submit" class="btn btn-danger" value="block-1" name="from">Отправить</button>
    										</div>
    									</div>
    								</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>