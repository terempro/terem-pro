<?php

const PARTNERS_IBLOCK = 99;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "партеры компании терем, партнеры терем");
$APPLICATION->SetPageProperty("title", "Партнеры строительной компании \"ТеремЪ\" Источник: http://terem-pro.ru/about/partnery/");
$APPLICATION->SetTitle("Партнеры строительной компании \"ТеремЪ\"");

$data = array();

$sections = CIBlockSection::GetList(['SORT' => 'asc'], ['IBLOCK_ID' => PARTNERS_IBLOCK, 'ACTIVE' => 'Y','UF_REGION'=>5], false, ['NAME', 'ID']);

while ($s = $sections->GetNext())
{
    $info = ['NAME' => $s['NAME'], 'PARTNERS' => array()];
    
    $partners = CIBlockElement::GetList(
            ['SORT' => 'asc'], 
            ['IBLOCK_ID' => PARTNERS_IBLOCK, 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $s['ID'],'PROPERTY_REGION'=>764], 
            false, 
            false,
            ['NAME', 'PREVIEW_PICTURE', 'PREVIEW_TEXT']
        );
    
    while ($p = $partners->GetNext())
    {
        $info['CONTENT'][] = [
            'NAME' => $p['NAME'],
            'PICTURE' => CFile::ResizeImageGet($p['PREVIEW_PICTURE'], ['width' => 280, 'height' => 210])['src'],
            'TEXT' => $p['PREVIEW_TEXT']
        ];
    }
    
    $data[$s['ID']] = $info;
}

?>

<style>
    .partner-block
    {
        display: flex;
        padding: 5px 2px;
    }
    
    .partner-block > div
    {
        padding: 3px 8px;
    }
    
    .partner-block > div.partner-image
    {
        min-width: 296px;
        margin-top: 35px;
        margin-right: 10px;
    }
</style>


<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side + my-margin clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="my-text-title text-center text-uppercase desgin-h1">
                                <h1>
                                   Партнёры
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                                Компания &laquo;Теремъ&raquo; за годы работы обзавелась надёжными и проверенными партнёрами, в чьём профессионализме и компетентности мы не раз убеждались за время сотрудничества. 
                            </p>
                            <br><br><br>
                        </div>
                    </div>
                    <?php $cnt = 1;?>
                    <?php foreach ($data as $id => $s): ?>
                        <!--one item-->
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="( my-collapse-btn + design-bordered ) collapsed" type="button" data-toggle="collapse" data-target="#s<?=$id?>" aria-expanded="false" aria-controls="price<?=$cnt?>">
                                    <div class="my-table">
                                        <div>
                                            <p><?php echo $s["NAME"]; ?></p>
                                        </div>
                                        <div>
                                            <i class="glyphicon glyphicon-chevron-up"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="collapse my-collapse" id="s<?=$id?>">
                                    <div class="( my-collapse-content + design-bordered-none )">
                                        <?php foreach ($s['CONTENT'] as $p): ?>
                                        <div class="partner-block">
                                            <div class="partner-image">
                                                <img src="<?= $p['PICTURE'] ?>" alt="">
                                            </div>
                                            <div class="partner-text">
                                                <h2><?= $p['NAME'] ?></h2>
                                                <?= $p['TEXT'] ?>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--one item END-->
                    <?php endforeach; ?> 
                </div>
            </div>
        </div>
    </div>
</section>

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>