<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "faq, часто задоваемые вопросы");
$APPLICATION->SetPageProperty("title", "Часто задаваемые вопросы");
$APPLICATION->SetTitle("Хотелось бы остановить выбор на компании ТеремЪ - Задайте вопрос.");
?>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h1>
                                    Часто задаваемые вопросы
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                                На этой странице Вы можете найти ответы на часто задаваемые нашими клиентами вопросы. Уточнить информацию можно у наших менеджеров в офисе и по телефону.
                            </p>
                            <br>
                            <br>
                        </div>
                    </div>
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Вы занимаетесь строительством по всей России или только в Новосибирске?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq1">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                            <p>Мы строим дома в Новосибирске и Новосибирской области. Отгрузка выполняется с производственной базы, расположенной в Новосибирске. Бесплатная доставка производится на расстояние до 50 км. от границ города Новосибирск.</p>
                                            <p>Каждый следующий километр оплачивается заказчиком согласно тарифу. С тарифами доставки по каждому дому вы можете ознакомиться в нашем офисе  по адресу: г. Новосибирск, ул. Фрунзе, д. 80, офис 804.</p>
                                            <p>В скором времени дома будут доступны для заказа в Кемеровской, Томской областях, а также в Алтайском крае и Республике Алтай.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>В рекламе указана стоимость дома. В нее входит цена земельного участка?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq2">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Стоимость участка не входит в стоимость дома. Если у вас пока еще нет земли, то вы можете приобрести её в нашем проекте «Земля». Если предполагается строительство на уже существующем участке, то при заключении договора потребуется документ, подтверждающий право собственности на участок и общегражданский паспорт владельца участка.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="false" aria-controls="faq4">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли использовать материнский капитал при строительстве дома в компании «Теремъ»?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq4">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Вы можете заказать и построить у нас дом за свои средства или с использованием кредитных средств. Далее вы получаете свидетельство на дом. После этого обращаетесь в Пенсионный фонд для реализации материнского капитала.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq6" aria-expanded="false" aria-controls="faq6">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Возможен ли самостоятельный вывоз комплекта дома, материалов, и/или самостоятельная сборка?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq6">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Нет, это невозможно. Мы строительная компания, которая имеет свое производство и строительные бригады. Дома мы строим сами, с использованием современных строительных технологий и качественных материалов. Мы даем гарантию на построенный дом, собранный по собственной технологии и специально обученными специалистами.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq7" aria-expanded="false" aria-controls="faq7">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Компания «Теремъ» занимается инженерными коммуникациями? Входят ли в стоимость дома эти работы?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq7">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>К каждому из проектов домов компании в Отделе инженерных коммуникаций есть план по электропроводке, установке отопительной системы и других инженерных коммуникаций. В офисе (г. Новосибирск, ул. Фрунзе, д. 80, офис 804.) можно заключить отдельные договоры на электромонтажные работы, установку отопления, водоснабжения, канализации и т.д.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq8" aria-expanded="false" aria-controls="faq8">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Какие в компании условия оплаты? Есть ли предоплата?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq8">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Для жителей Новосибирска и Новосибирской области предоплаты у нас нет.</p>
                                            <p>Для клиентов c пропиской за пределами Новосибирска и Новосибирской области есть предоплата.</p>
                                            <p>Подробную информацию можно получить в офисе по адресу: г. Новосибирск, ул. Фрунзе, д. 80, офис 804. или по тел.: 8(383)312-12-01.</p>
                                            <p>Оплата осуществляется по факту выполнения работ в любом отделении Сбербанка.</p>
                                            <ul>
                                                <li><p>после строительства фундамента – 60%,</p></li>
                                                <li><p>после строительства крыши – 40%.</p></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq9" aria-expanded="false" aria-controls="faq9">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Есть ли  возможность купить дом в кредит?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq9">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Есть. Более подробно об условиях кредитования можно ознакомиться в офисе (г. Новосибирск, ул. Фрунзе, д. 80, офис 804) и в разделе «Наши партнеры» на нашем сайте.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq10" aria-expanded="false" aria-controls="faq10">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Выезжают ли ваши специалисты на участок для того, чтобы определить условия застройки и пожелания клиента на месте?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq10">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да, при необходимости выезд специалиста возможен, но только после заключения клиентом договора на строительство. Эта услуга платная. Подробнее Вы можете узнать в офисе по адресу: г. Новосибирск, ул. Фрунзе, д. 80, офис 804 или по тел.: 8(383)312-12-01.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq11" aria-expanded="false" aria-controls="faq11">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли в домах компании «Теремъ» проживать круглогодично?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq11">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да, в домах для постоянного проживания тысячи наших заказчиков живут круглогодично. Эти дома предназначены для строительства на дачных участках и на участках для индивидуального жилищного строительства. Комплектация домов предусматривает его круглогодичное эксплуатацию с использованием стационарных отопительных агрегатов (АОГВ, калориферов, печей и пр.).</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq12" aria-expanded="false" aria-controls="faq12">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Какие установлены сроки строительства домов?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq12">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Минимальный срок – несколько дней, а максимальный - почти 4 месяца. Разные дома имеют различные сроки строительства. Подробнее Вы можете узнать на нашем сайте в разделе «Каталог домов» и по телефону: 8 (383) 312-12-01.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq13" aria-expanded="false" aria-controls="faq13">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Что такое каркасная технология строительства домов?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq13">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Стены каркасного дома выполняются из доски 35х150 мм. Доска устанавливается с шагом 500 мм. (400 мм для домов для постоянного проживания). Между стойками укладывается утеплитель 150 мм (200 мм для домов для постоянного проживания). С двух сторон стены обшиваются вагонкой (дома для постоянного проживания с наружной стороны обшиваются сайдингом), с наружной стороны - по изоспану А, с внутренней стороны - по изоспану Б. Весь конструктив выполняется из сухого материала.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq14" aria-expanded="false" aria-controls="faq14">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Что входит в базовую комплектацию дома?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq14">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>В базовую комплектацию дома (бани) входит:</p>
                                            <ul>
                                                <li>
                                                    <p>Фундамент;</p>
                                                </li>
                                                <li>
                                                    <p>Весь материал для сборки дома;</p>
                                                </li>
                                                <li>
                                                    <p>Окна, двери, кровля, материал для внутренней отделки, лестница;</p>
                                                </li>
                                                <li>
                                                    <p>Доставка;</p>
                                                </li>
                                                <li>
                                                    <p>Работы по установке и сборке дома.</p>
                                                </li>
                                            </ul>
                                            <p>Узнать о каждом конкретном доме более подробно можно в разделе «Каталог домов».</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq15" aria-expanded="false" aria-controls="faq15">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Какую гарантию компания «Теремъ» дает на дома?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq15">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Мы даем гарантию до 25 лет.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                </div>
            </div>
        </div>

    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
