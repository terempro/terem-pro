/*myMap END*/
(function(){
        var container = document.getElementById('zoom-map');
        var map = container.children[0];
        map.style.position = "relative";
        var mapHeight = map.offsetHeight;
        var mapWidth = map.offsetWidth;
        console.log(mapHeight,mapWidth);

        container.style.height = mapHeight - 60 + 'px';
        container.style.cursor = 'move';
        container.style.width = mapWidth - 60 + 'px';
        map.onmousedown = function(e) {
          var mapCoords = getCoords(map);
          var shiftX = e.pageX - mapCoords.left;
          var shiftY = e.pageY - mapCoords.top;
          var containerCoords = getCoords(container);
          document.onmousemove = function(e) {
            var newLeft = e.pageX - shiftX - containerCoords.left;
            var newTop = e.pageY - shiftY - containerCoords.top;
            var rightEdge = map.offsetWidth - container.offsetWidth;
            var bottomEdge = map.offsetHeight - container.offsetHeight;

            if (newLeft > 0) {
              newLeft = 0;
            }
            if(newTop > 0){
                newTop = 0;
            }

            if (newLeft < -rightEdge) {
              newLeft = -rightEdge;
             
            }
            if(newTop < -bottomEdge){
                newTop = -bottomEdge;
            }
            map.style.left = newLeft + 'px';
            map.style.top = newTop + 'px';
          }

          document.onmouseup = function() {
            document.onmousemove = document.onmouseup = null;
          };

           return false;
        };
        var zoomInBtn = document.getElementsByClassName('btn-zoom-in');
        var zoomOutBtn = document.getElementsByClassName('btn-zoom-out');
        map.oncontextmenu = function () {
            map.style.left =  '0px';
            map.style.top =  '0px';
            map.style.height = '100%';
            map.style.width = '100%';
            return false;
        }
        
        map.ondragstart = function() {
          return false;
        };
        map.addEventListener('dblclick', function(e){
            map.style.height = 'auto';
            map.style.width = 'auto';
        });
        $(zoomInBtn).on('click', function(){
            map.style.height = 'auto';
            map.style.width = 'auto';
        });
        $(zoomOutBtn).on('click', function(){
            map.style.left =  '0px';
            map.style.top =  '0px';
            map.style.height = '100%';
            map.style.width = '100%';
        });

        function getCoords(elem) {
          var box = elem.getBoundingClientRect();

          return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset,
            right: box.right - pageXOffset,
            bottom: box.bottom - pageYOffset
          };

        }
    
})();