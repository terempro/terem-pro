<!--Modal:cards item-->
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;
$VISITOR_CMP = $APPLICATION->get_cookie("VISITOR_CMP");
$action = 'http://crm.terem-pro.ru/index.php/welcome/postGiveHouse/';

if ($_GET['SECTION'])
{
    if ($_GET['SECTION'] == '1' or $_GET['SECTION'] == '5' or $_GET['SECTION'] == '2' or $_GET['SECTION'] == '3' or $_GET['SECTION'] == '4')
        $action = '/include/buro.php';
}
$url = $APPLICATION->GetCurPage();
$page = substr($APPLICATION->GetCurPage(), 0, 30);

if ($page == '/services/individual_projects/')
    $action = '/include/buro.php';

$request_failed = isset($_SESSION["CONTEST_FAILED"]);

$double_request = isset($_SESSION["CONTEST_SENT"]) && ($_SESSION["CONTEST_SENT"] == true);

?>
    <noindex>
        <!-- concurs modals -->
        <div class="my-modal modal fade" data-item="shown-item" id="concursRules" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <h3> Условия участия</h3>
                                <p>Нарисовать дом мечты и отсканировать рисунок,</p>
                                <p>Загрузите иллюстрацию в данном разделе.</p>
                                <p>Заполните свои данные: ФИО, e-mail, возраст участника, город (все поля являются обязательными для заполнения).</p>
                                <p>Ознакомьтесь с согласием на использование изображений, а также политикой конфиденциальности и правилами конкурса. </p>
                                <p>Подтвердите свое согласие.</p>
                                <p>Нажмите кнопку «Отправить».</p>
                                <p>На указанный Вами электронный адрес придет сообщение о получении информации и о результатах проверки фотографий на соответствие заявленным техническим требованиям.</p>
                                <p>Период модерации – 7 дней.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="my-modal modal fade" data-item="shown-item" id="concursPrize" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <h3> Призы</h3>
                                <p> <b>1 место:</b> * Скидка до 35 % на приобретение товаров в компании ООО «НаноСигвей», фирменные подарки от компании «Теремъ».</p>
                                <p> <b>2 место:</b> * Скидка до 35 % на приобретение товаров в компании ООО «НаноСигвей», фирменные подарки от компании «Теремъ».</p>
                                <p> <b>3 место:</b> * Скидка до 35 % на приобретение товаров в компании ООО «НаноСигвей», фирменные подарки от компании «Теремъ».</p>
                                <p> <b>4 место:</b> Фирменные подарки от компании «Теремъ».</p>
                                <p> <b>5 место:</b> Фирменные подарки от компании «Теремъ».</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="my-modal modal fade" data-item="shown-item" id="concursLoad" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <h3>Технические требования</h3>
                                <p> Ориентация снимка – горизонтальная</p>
                                <p>Размер фотографии (вес) – не менее 3 Мб и не более 7 Мб.</p>
                                <p>Расширение иллюстрации - jpg 150 dpi</p>

                            </div>
                        </div>
                        <form id="load-image" action="/include/loadphoto.php" method="post" class="form-send" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label for="">Имя и фамилия ребенка</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <input type="text" name="child-name" class="form-control my-input" placeholder="Иван Иванов" required="">
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <script>
                                $('#fioParent').hide();
                                $('[name="child-age"]').on("input", function() {
                                    if ($(this).val() < 14) {
                                        $('#fioParent').show();
                                    } else {
                                        $('#fioParent').hide();
                                    }
                                });
                            </script>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label for="">Возраст ребенка</label>
                                            </div>
                                            <div class="col-xs-12">
                                                <input type="number" name="child-age" class="form-control my-input" placeholder="10 лет" required="">
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="form-group" id="fioParent">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label for="">ФИО законных представителей</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <input type="text" name="parent" class="form-control my-input" placeholder="Иван Иванов Иванович" required="">
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label for="">E-mail</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <input type="email" name="email" class="form-control my-input" placeholder="IvanovII@domain.ru" required="">
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-8">
                                        <div>
                                            <label for="">Загрузите рисунок</label>
                                        </div>
                                        <div class="concurs_file_upload">
                                            <div>

                                                <input type="file" class="file" name="image" id="upload-images" required="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <p style="padding-top: 3px;">* Отправляя заявку, Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">политикой конфиденциальности и правилами пользования сайтом</a> и даете <a href="/privacy-policy/" target="_blank">согласие  на использование загруженных изображений</a>.</p>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">

                                        <span id="response-message" class="respond-message">
                                        <? if($double_request): ?>
                                        Вы уже отправили рисунок на конкурс.<br>
                                        Как только он пройдёт модерацию, на Вашу почту поступит уведомление.
                                        <? endif; ?>
                                        <? if($request_failed): ?>
                                        Вы три раза подряд неправильно ввели проверочный код.<br>
                                        Доступ к сервису временно заблокирован!
                                        <? endif; ?>
                                    </span>
                                        <? if(!$double_request && !$request_failed): ?>
                                            <p style="text-align:center;" id="confirmation-code" class="confirmation-code" hidden="hidden">
                                                <span id="confirmation-code-hint">Код подтверждения (выслан на почту):</span><br>
                                                <input style="width: 100% !important;" size="4" maxlength="4" type="text" name="confirmation-code" class="form-control" autofocus>
                                            </p>
                                            <button id="load-image-button" type="submit" class="concurs_load_btn">Отправить</button>
                                            <? endif; ?>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end -->


        <div class="my-modal modal fade" data-item="shown-item" id="formAdvice" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form class="my-form form-advice" action="/include/callback.php" method="post" data-form="send">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <p>Вы можете оставить предварительную заявку на понравившийся проект.</p>
                                    <p>В ближайшее время с Вами свяжется наш менеджер.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>Имя</label>
                                        <input type="text" name="name" class="form-control my-input" placeholder="Иван" required="required" />
                                        <input type="hidden" name="ipadress" class="" value="<?php echo $_SERVER[" REMOTE_ADDR "]; ?>"/>
                                        <input type="hidden" name="zayavka" class="" value="catalog_item" />
                                        <input type="hidden" name="cmp" class="" value="<?php
                                if ($VISITOR_CMP) {
                                    echo $VISITOR_CMP;
                                }
                                ?>" />
                                        <input type="hidden" name="url" class="" value="<?= $url ?>" />
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>Телефон</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <input type="text" data-item="phone" name="phone" class="form-control my-input phone" placeholder="8 920 000 00 00" required="required" />
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>Дом</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <input type="text" data-item="house" class="form-control my-input recipient-name" name="house" />
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-bottom">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <a href="#" data-dismiss="modal" class="no-thanks">Нет, спасибо.</a>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-sm-6 text-right">
                                        <input type="hidden" name="code" id="codeval" value="" />
                                        <input type="hidden" name="city" value="Новосибирск">
                                        <button type="submit" class="my-btn btn btn-danger" value="">Заказать</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <p><strong>Внимание!</strong><br>Отправляя форму Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">Политикой обработки данных.</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal:cards item END-->
        <!--Modal:call-->

        <div class="modal fade my-modal-call" id="call" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title padding-bottom">Заказать звонок</h4>
                        <p>Мы можем перезвонить в удобное для вас время ежедневно с 10:00 до 19:00. Наши специалисты ответят на любые возникшие вопросы!</p>
                    </div>
                    <div class="modal-body">
                        <form class="form-send" action="/include/callback.php" method="post" data-form="send">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label>Имя</label>
                                                <input type="text" name="name" class="form-control my-input" placeholder="Введите ваше имя" required="">
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label>Телефон</label>
                                                <input type="tel" name="phone" class="form-control my-input" data-item="phone" placeholder="8 920 000 00 00" required="">
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label>Город</label>
                                                <input type="tel" name="city" class="form-control my-input" placeholder="Введите ваш город" value="Новосибирск" required="">
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="time" value="none">
                                    <!--                            <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <select class="form-control my-input" name="time" required>
                                                                        <option value="" disabled selected title="Удобное время звонка">Удобное время звонка</option>
                                                                        <option value="10-00">10-00</option>
                                                                        <option value="11-00">11-00</option>
                                                                        <option value="12-00">12-00</option>
                                                                        <option value="13-00">13-00</option>
                                                                        <option value="14-00">14-00</option>
                                                                        <option value="15-00">15-00</option>
                                                                        <option value="16-00">16-00</option>
                                                                        <option value="17-00">17-00</option>
                                                                        <option value="18-00">18-00</option>
                                                                        <option value="19-00">19-00</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="help-block with-errors"></div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <p><strong>Внимание!</strong><br>Все поля обязательны для заполнения.<br>Отправляя форму Вы соглашаетесь с <a href="/privacy-policy/" target="_blank">Политикой обработки данных.</a></p>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <input type="hidden" name="city" value="Новосибирск">
                                    <button type="submit" name="ipadress" value="" class="btn btn-danger">ОТПРАВИТЬ</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal:call END-->

        <!--Modal:card item-->
        <div class="modal fade my-modal-advice" data-item="shown-item" id="formAdviceItem" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="text-uppercase">получить бесплатную консультацию</h4>
                        <p>Вы можете оставить предварительную заявку на понравившийся Вам проект, и в ближайшее время с Вами свяжется наш менеджер. Заявка не накладывает никаких обязательств, а является запросом только на получение информации.</p>
                    </div>
                    <div class="modal-body">
                        <form class="my-form form-advice" action="/include/callback.php" method="post" data-form="send">

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-xs-12">
                                        <input type="text" name="name" class="form-control my-input user" placeholder="Введите ваше имя" required/>
                                        <input type="hidden" name="ipadress" class="" value="<?php echo $_SERVER[" REMOTE_ADDR "]; ?>"/>
                                        <input type="hidden" name="zayavka" class="" value="catalog_card" />
                                        <input type="hidden" name="cmp" class="" value="<?php
                                if ($VISITOR_CMP) {
                                    echo $VISITOR_CMP;
                                }
                                ?>" />
                                        <input type="hidden" name="url" class="" value="<?= $url ?>" />
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">

                                    <div class="col-xs-12">
                                        <input type="text" data-item="phone" name="telephone" class="form-control my-input phone" placeholder="Введите телефон" required/>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">

                                    <div class="col-xs-12">
                                        <input type="text" class="form-control my-input recipient-name" name="house" />

                                    </div>
                                    <div class="col-xs-12">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-bottom">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 text-right">
                                        <input type="hidden" name="code" id="codeval" value="" />
                                        <input type="hidden" name="city" value="Новосибирск">
                                        <button type="submit" class="my-btn btn btn-danger" value="" name="from">Заказать</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal:card item END-->
        <!--Modal:Thx-->
        <div class="modal fade my-modal-call" id="thx" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title padding-bottom text-center">Спасибо за заявку!</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-center"><big>Наши менеджеры свяжутся с Вами в ближайшее время.</big></p>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal:Thx END-->
        <img src="" />

        <!--Modal:city-->
        <div class="my-modal modal fade" data-item="shown-item" id="city" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content my-city-content">
                    <div class="modal-header">
                        <h4 class="text-uppercase my-title-modal-city">Новосибирск</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form class="my-form form-advice" action="<?= $action ?>" method="post" data-form="send">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <p class="my-text-modal">Мы угадали Ваш город?</p>
                                </div>
                            </div>

                            <div class="form-group margin-bottom">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 col-sm-4">
                                        <button type="submit" data-dismiss="modal" aria-label="Close" class="btn btn-danger my-btn-city">Да</button>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8 ">

                                        <select class="form-control my-input my-select select2 js-example-responsive" id="citiesList" name="city" required="" style="width: 100%">
                                    <option value="" disabled="" selected="" title="0">Нет, выбрать...</option>
                                    <?php foreach ($CITIES_ARRAY as $code => $name): ?>
                                        <option value="<?=$code?>"><?=$name?></option>
                                    <?php endforeach; ?>
                                    <option value="">Другой</option>
                                </select>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal:city END-->


    </noindex>