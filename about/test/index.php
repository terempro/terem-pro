<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "терем отзывы");
$APPLICATION->SetPageProperty("description", "Терем отзывы реальных клиентов о строительной компании и проектах");
$APPLICATION->SetPageProperty("keywords", "терем отзывы");
$APPLICATION->SetPageProperty("title", "Терем отзывы о строительной компании.");
$APPLICATION->SetTitle("Терем отзывы о строительной компании.");
?>
<?php
if ($_GET) {
if ($_GET['PAGEN_1'] != 1 && $_GET['PAGEN_1'] != NULL) {
$TITLE = "Терем отзывы о строительной компании. Страница - " . $_GET['PAGEN_1'];
$APPLICATION->SetTitle($TITLE);
$APPLICATION->SetPageProperty("title", $TITLE);
$str = "Страница " . $_GET['PAGEN_1'];
}
}
?>
<section class="content white">
  <section class="profile">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="profile__card my-margin">
            <div class="profile__panel">
              <div class="panel__header">
                <span>Добрый день,</span>
                <span>Алексей Петрович</span>
              </div>
              <div class="panel__body">
                <div class="panel__controls">
                  <a href="#">Лента</a>
                  <a href="#">Договора и услуги</a>
                  <a href="#">Оплата</a>
                  <a href="#" class="active">Личные данные</a>
                </div>
                <div class="panel__controls">
                  <a href="#">Клиентский менеджер</a>
                  <a href="#">Рекламация</a>
                </div>
                <div class="panel__controls">
                  <a href="#">Задать вопрос</a>
                  <a href="#">Подать жалобу</a>
                  <a href="#">Видео канал Теремъ</a>
                </div>
              </div>
              <div class="panel__footer">
                <span class="panel__current">Вы смотрите: Личные данные</span>
                <a href="#">Вернуться в каталог</a>
              </div>
            </div>
            <div class="profile__content">
              <div class="content__header">
                <h1>Личные данные</h1>
              </div>
              <div class="content__body user">
                <div class="user__info">
                  <div class="content__group">
                    <div class="user__name">
                      <span>Константинопольский</span>
                      <span>Алексей</span>
                      <span>Петрович</span>
                    </div>
                  </div>
                  <div class="content__group">
                    <span>Дата рождения: 00.00.0000</span>
                    <span>Телефон: +7 (000) 000-00-00</span>
                    <span>Электронная почта: xxxxxxxx@xxxxxx.xx</span>
                  </div>
                  <div class="content__group">
                    <span>Привязанные номера договоров:</span>
                    <ol>
                      <li>
                        0000000-00АА
                      </li>
                    </ol>
                  </div>
                  <div class="content__group">
                    <div class="btns-group">
                      <button class="cta-btn">Изменить данные</button>
                      <button class="cta-btn">Задать вопрос</button>
                    </div>
                  </div>
                </div>
                <div class="user__pic--wrapper">
                  <div class="user__pic">
                    <img src="" alt="Юзерпик">
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>

<section class="content white">
  <section class="profile">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="profile__card my-margin">
            <div class="profile__panel">
              <div class="panel__header">
                <span>Добрый день,</span>
                <span>Алексей Петрович</span>
              </div>
              <div class="panel__body">
                <div class="panel__controls">
                  <a href="#">Лента</a>
                  <a href="#">Договора и услуги</a>
                  <a href="#">Оплата</a>
                  <a href="#" class="active">Личные данные</a>
                </div>
                <div class="panel__controls">
                  <a href="#">Клиентский менеджер</a>
                  <a href="#">Рекламация</a>
                </div>
                <div class="panel__controls">
                  <a href="#">Задать вопрос</a>
                  <a href="#">Подать жалобу</a>
                  <a href="#">Видео канал Теремъ</a>
                </div>
              </div>
              <div class="panel__footer">
                <span class="panel__current">Вы смотрите: Личные данные</span>
                <a href="#">Вернуться в каталог</a>
              </div>
            </div>
            <div class="profile__content">
              <div class="content__header">
                <h1>Личные данные</h1>
              </div>
              <div class="content__body user">
                <form action="">
                  <div class="user__info">
                    <div class="content__group">
                      <div class="user__name">
                        <input type="text" name="last_name" placeholder="Фамилия">
                        <input type="text" name="first_name" placeholder="Имя">
                        <input type="text" name="father_name" placeholder="Отчество">
                        <input type="text" name="birthday" placeholder="Дата рождения">
                      </div>
                    </div>
                    <div class="content__group">
                      <span>Контакты</span>
                      <input type="tel" name="tel" placeholder="Телефон">
                      <input type="email" name="email" placeholder="Почта">
                    </div>
                    <div class="content__group">
                      <span>Номера актуальных договоров определяются автоматически по указанному номеру телефона</span>
                      <ol>
                        <li>
                          0000000-00АА
                        </li>
                      </ol>
                    </div>
                    <div class="content__group">
                      <div class="btns-group">
                        <button type="submit" class="cta-btn">Сохранить</button>
                      </div>
                    </div>
                  </div>

                  <div class="user__pic--wrapper">
                    <div class="user__pic">
                      <img src="" alt="Юзерпик">
                    </div>
                    <button class="cta-btn">Загрузить фото</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
