<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "гарантия терем");
$APPLICATION->SetPageProperty("title", "Гарантия ");
$APPLICATION->SetTitle("Гарантия ");
?>

    <section class="content text-page white" role="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="white padding-side clearfix">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="my-text-title text-center text-uppercase desgin-h1">
                                    <h1>
                                        Гарантия от компании «ТЕРЕМЪ»
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <img src="/upload/garantiya/garantia.jpg" class="img-responsive">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <p>Период гарантийного обслуживания домов от компании Теремъ увеличен. Для каркасных домов срок гарантии составляет до 15 лет, для брусовых - 25 лет.</p>
                                <p>Доказываем временем надежность деревянных домов! Увеличенный срок гарантийного обслуживания - решение, основанное на уверенности в качестве материалов и профессионализме строительных бригад.</p>
                                <p>Гарантия от компании Теремъ - это защита ваших вложений. Если в процессе эксплуатации в доме появляются недостатки, то стоит обратиться в отдел рекламации - все исправим бесплатно.</p>
                                <br>
                                <p>Подробности уточныйте по телефону или у менеджеров на выставке компании Теремъ. Ждем вас ежедневно по адресу: г.Москва, ул.Зеленодольская, владение 42.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
								<p>
                                	<button style="background-color:transparent; border:0px; margin-top:25px;" data-target="#call" data-toggle="modal"><img src="/upload/garantiya/garantia_knopka.png" class="img-responsive"></button>
								</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>