<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "Полезная информация терем");
$APPLICATION->SetPageProperty("title", "Полезная информация ");
$APPLICATION->SetTitle("Полезная информация");
?>
    <link href="skhema-raboty-style.css" rel="stylesheet">
    <section class="content text-page white" role="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="white padding-side clearfix">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="my-text-title text-center text-uppercase desgin-h1">
                                    <h1>
                                        6 шагов к вашему дому
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:30px;">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <p>Строительство дома – это не только возведение стен и монтаж крыши. На этой странице мы описали все этапы, которые проходят наши клиенты, прежде чем стать счастливыми обладателями нового дома.</p>
                            </div>
                            <div class="row">
                                <div class="hidden-xs col-md-12 col-sm-12 col-lg-12">
                                    <div class="hidden-sm col-md-3 col-lg-3 step-build" style="margin-left:5px;">
                                        <div class="my-table">
                                            <div>
                                                <strong style="color:black; font-weight: 600; font-size:25px; line-height:1.3em; font-family: Reforma, Arial, sans-serif;">Этапы<br>строительства</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-lg-9" id="steps" style="margin-right: -15px;">
                                        <div class="row" style="margin-left: 1px;">
                                            <a class="step_title_1" href="#step1" type="button" data-toggle="collapse" data-target="#take01" aria-expanded="false" aria-controls="take01">
                                                <div class="col-md-4 col-sm-4 three_string">
                                                    Визит<br>в выставочный<br>комплекс
                                                </div>
                                            </a>
                                            <a class="step_title_2" href="#step2" type="button" data-toggle="collapse" data-target="#take02" aria-expanded="false" aria-controls="take02">
                                                <div class="col-md-4 col-sm-4">
                                                    Заключение<br>договора
                                                </div>
                                            </a>
                                            <a class="step_title_3" href="#step3" type="button" data-toggle="collapse" data-target="#take03" aria-expanded="false" aria-controls="take03">
                                                <div class="col-md-4 col-sm-4">
                                                    Монтаж<br>фундамента
                                                </div>
                                            </a>
                                            <div class="clearfix" style="margin-bottom: 15px;"></div>
                                            <a class="step_title_4" href="#step4" type="button" data-toggle="collapse" data-target="#take04" aria-expanded="false" aria-controls="take04">
                                                <div class="col-md-4 col-sm-4">
                                                    Строительство<br>дома под кровлю
                                                </div>
                                            </a>
                                            <a class="step_title_5" href="#step5" type="button" data-toggle="collapse" data-target="#take05" aria-expanded="false" aria-controls="take05">
                                                <div class="col-md-4 col-sm-4">
                                                    Окончание<br>строительства
                                                </div>
                                            </a>
                                            <a class="step_title_6" href="#step6" type="button" data-toggle="collapse" data-target="#take06" aria-expanded="false" aria-controls="take06">
                                                <div class="col-md-4 col-sm-4">
                                                    Действие гарантии<br>(до 25 лет)
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take01" aria-expanded="false" aria-controls="take01">
                                                    <div class="my-table">
                                                        <div id="step1">
                                                            <strong><span class="step">Этап 1: </span>Визит в выставочный комплекс</strong>
                                                        </div>
                                                        <div class="my-appearance-middle">
                                                            <i class="glyphicon glyphicon-menu-up"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="take01">
                                                    <div class="( my-collapse-content + design-main )">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <p style="text-align: center;"><img class="img-responsive" src="1_stage_246x226.jpg" style="padding-right:15px; margin-bottom:25px;"></p>
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-12 col-xs-12">
                                                                        <p>Даже если вы уже твёрдо решили, какой дом и по какой технологии хотите построить, мы всё равно рекомендуем вам посетить выставочный комплекс компании «Теремъ» в Кузьминках и проконсультироваться с нашими менеджерами. А если вы ещё не определились и только выбираете проект, то вам просто необходимо приехать по адресу г. Москва, улица Зеленодольская, владение 42.</p>
                                                                        <ul>
                                                                            <li>
                                                                                <p>На территории выставочного комплекса компании «Теремъ» представлено более 40 полноразмерных строений с различными вариантами внутренней и внешней отделки.</p>
                                                                            </li>
                                                                            <li>
                                                                                <p>Вы можете осмотреть все дома изнутри и снаружи, оценить реальную площадь домов и удобство планировок.</p>
                                                                            </li>
                                                                            <li>
                                                                                <p>Менеджеры компании «Теремъ» дадут вам полную консультацию о технологиях строительства, возможных комплектациях и особенностях проектов.</p>
                                                                            </li>
                                                                            <li>
                                                                                <p>Вы можете попросить менеджера рассчитать вам стоимость дома с тем или иным набором дополнительных услуг и выбрать наиболее приемлемый для вас вариант.</p>
                                                                            </li>
                                                                            <li>
                                                                                <p>Здесь вы сможете заказать все необходимые дополнительные услуги по организации загородной жизни: от установки забора до монтажа всех необходимых коммуникаций.</p>
                                                                            </li>
                                                                            <li>
                                                                                <p>При желании вы можете заказать экскурсию по выставочному комплексу, тогда в назначенное время вас встретит личный менеджер и познакомит с продукцией и услугами компании «Теремъ». Либо вы можете приехать в выставочный комплекс в удобное для вас время и обратиться к любому менеджеру уже на месте.</p>
                                                                            </li>
                                                                        </ul>
                                                                        <p>Выставочный комплекс работает ежедневно с 10:00 до 20:00.</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 hidden-sm hidden-xs">
                                                                        <button style="border:0px solid transparent; background-color:transparent; padding-top:40px;" class="zemli-button" data-target="#zemli" data-toggle="modal"><img class="img-responsive" src="/upload/news/button.png"></button>
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-12 col-xs-12">
                                                                        <hr style="border-top: 1px solid #ae1c1d;">
                                                                        <p class="step">Все вопросы вы можете задать нам по телефону: +7 (495) 461-05-55<br>Ежедневно: 10:00 - 20:00</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--one item END-->
                                <br>
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take02" aria-expanded="false" aria-controls="take02">
                                                    <div class="my-table">
                                                        <div id="step2">
                                                            <strong><span class="step">Этап 2: </span>Заключение договора</strong>
                                                        </div>
                                                        <div class="my-appearance-middle">
                                                            <i class="glyphicon glyphicon-menu-up"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="take02">
                                                    <div class="( my-collapse-content + design-main )">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <p style="text-align: center;"><img class="img-responsive" src="2_stage_246x226.jpg" style="padding-right:15px; margin-bottom:25px;"></p>
                                                            </div>
                                                            <div class="col-md-9 col-sm-12 col-xs-12">
                                                                <p>После того как вы выберете свой проект, определитесь с комплектацией и технологией строительства, вам следует заключить договор на строительство.</p>
                                                                <ul>
                                                                    <li>
                                                                        <p>Договор подготавливают и подписывают в договорном отделе прямо на территории выставочного комплекса в Кузьминках. При желании сделать это можно сразу после консультации у менеджера, который сделает точный расчёт стоимости дома и проводит вас в договорный отдел.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>В договоре будут прописаны сроки строительства, комплектация дома и итоговая стоимость работ.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>После подписания, даже если начало строительства будет отложено, ваша цена на дом не изменится и никакие подорожания вас не коснутся.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>При заключении договора необходимо предъявить паспорт и документ, удостоверяющий право собственности на землю. Если земельный участок принадлежит не тому, на чьё имя заключается договор на строительство, собственник земли должен предоставить разрешение на проведение строительных работ.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>При заключении договора клиент должен предоставить данные о своём участке: район застройки, место строительства, площадь участка, состояние подъезда к участку, тип грунта (если известен), необходимость оформления пропуска для проезда автотранспорта, рельеф местности, наличие других строений, деревьев, пеньков, наличие водоснабжения и электроэнергии на участке, наличие жилого помещения для размещения бригады (если есть). </p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--one item END-->
                                <br>
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take03" aria-expanded="false" aria-controls="take03">
                                                    <div class="my-table">
                                                        <div id="step3">
                                                            <strong><span class="step">Этап 3: </span>Монтаж фундамента</strong>
                                                        </div>
                                                        <div class="my-appearance-middle">
                                                            <i class="glyphicon glyphicon-menu-up"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="take03">
                                                    <div class="( my-collapse-content + design-main )">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <p style="text-align: center;"><img class="img-responsive" src="3_stage_246x226.jpg" style="padding-right:15px; margin-bottom:25px;"></p>
                                                            </div>
                                                            <div class="col-md-9 col-sm-12 col-xs-12">
                                                                <p>Монтаж фундамента ‒ это первый этап уже непосредственно строительства дома. Обратите внимание, что монтаж фундамента выполняет отдельная бригада, не та, которая позднее будет возводить сам дом. </p>
                                                                <ul>
                                                                    <li>
                                                                        <p>За три дня до начала монтажа необходимо связаться с диспетчерским отделом (тел.: +7 (495) 996-57-05) и подтвердить заказ, согласовать подъезд к месту стройки и место встречи бригады. Диспетчеры уже накануне выезда ещё раз позвонят вам и подтвердят приезд бригады. </p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Отменить или перенести выезд бригады можно только до 6:30 утра запланированного дня выезда бригады. Телефон экстренной отмены выезда: +7 (915) 351-77-60.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Участок должен быть подготовлен к монтажу фундамента. При необходимости его надо выровнять, убрать весь мусор, растущие кустарники и деревья. </p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Сроки монтажа фундамента зависят от вида фундамента, который вы выбрали. </p>
                                                                    </li>
                                                                    <li>
                                                                        <p>По окончании монтажа необходимо принять работы, подписать акт выполненных работ и сделать первый платеж в размере 40% от стоимости дома<small>*</small>. </p>
                                                                    </li>
                                                                </ul>
                                                                <p><small>* Условия действуют для жителей Москвы и Московской области. Форма оплаты 20-60-20 действует при условии соблюдения срока начала строительства, предлагаемого компанией. В остальных случаях действует форма оплаты 40-40-20. Подробности узнавайте в отделе продаж.</small></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--one item END-->
                                <br>
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take04" aria-expanded="false" aria-controls="take04">
                                                    <div class="my-table">
                                                        <div id="step4">
                                                            <strong><span class="step">Этап 4: </span>Строительство дома под кровлю</strong>
                                                        </div>
                                                        <div class="my-appearance-middle">
                                                            <i class="glyphicon glyphicon-menu-up"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="take04">
                                                    <div class="( my-collapse-content + design-main )">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <p style="text-align: center;"><img class="img-responsive" src="4_stage_246x226.jpg" style="padding-right:15px; margin-bottom:25px;"></p>
                                                            </div>
                                                            <div class="col-md-9 col-sm-12 col-xs-12">
                                                                <p>Возведение дома ‒ самый важный и самый волнительный этап. Ведь как только бригада строителей приедет на ваш участок, дом будет расти каждый день буквально на глазах.</p>
                                                                <ul>
                                                                    <li>
                                                                        <p>Дата начала работ обязательно обговаривается заранее.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>До начала работ вы можете изменить перечень дополнительных услуг. Обратитесь к вашему менеджеру минимум за три дня до начала строительства.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Срок строительства дома зависит от размера дома и технологии строительства. В среднем на возведение дома рабочие компании «Теремъ» тратят 45 дней. При этом дома серии «Малыш» возводятся за неделю, а большие загородные коттеджи площадью более 170 кв. м – за три месяца.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Всё это время рабочие проживают на территории заказчика. Если у заказчика нет возможности предоставить помещение для их проживания, рабочие привезут с собой бытовку. Клиент не оплачивает проживание бригады!</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Перед началом строительства вам придёт СМС-сообщение с номером телефона прораба работающей у вас бригады. По всем оперативным вопросам во время строительства вам надо связываться с ним.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Рабочий день строителей может быть ненормированным из-за погодных условий, но, как показывает практика, наши рабочие укладываются в обозначенный срок строительства.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Дома строятся по отработанным технологиям и чётким схемам, которыми снабжаются бригады перед выездом на объект.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>После возведения дома под кровлю клиент делает второй платёж – 40% от общей стоимости дома*.</p>
                                                                    </li>
                                                                </ul>
                                                                <p><small>*Условия действуют для жителей Москвы и Московской области. Форма оплаты 20-60-20 действует при условии соблюдения срока начала строительства, предлагаемого компанией. В остальных случаях действует форма оплаты 40-40-20. Подробности узнавайте в отделе продаж.</small></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--one item END-->
                                    </div>
                                </div>
                                <br>
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) for-bank collapsed" type="button" data-toggle="collapse" data-target="#take05" aria-expanded="false" aria-controls="take05">
                                                    <div class="my-table">
                                                        <div id="step5">
                                                            <strong><span class="step">Этап 5: </span>Окончание строительства</strong>
                                                        </div>
                                                        <div class="my-appearance-middle">
                                                            <i class="glyphicon glyphicon-menu-up"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="my-collapse collapse" id="take05" aria-expanded="false" style="height: 0px;">
                                                    <div class="( my-collapse-content + design-main )">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <p style="text-align: center;"><img class="img-responsive" src="5_stage_246x226.jpg" style="padding-right:15px; margin-bottom:25px;"></p>
                                                            </div>
                                                            <div class="col-md-9 col-sm-12 col-xs-12">
                                                                <ul>
                                                                    <li>
                                                                        <p>Окончанием строительства считается тот момент, когда все работы, прописанные в договоре, выполнены.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>По окончании работ владелец осматривает дом и подписывает акт приёма выполненных работ.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>По окончании строительства оплачивается остаток стоимости дома – 20%.<small>*</small></p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Рабочие должны собрать и вывезти за собой весь строительный мусор. Остатки стройматериалов с участка не вывозятся.</p>
                                                                    </li>
                                                                </ul>
                                                                <p><small>*Условия действуют для жителей Москвы и Московской области. Форма оплаты 20-60-20 действует при условии соблюдения срока начала строительства, предлагаемого компанией. В остальных случаях действует форма оплаты 40-40-20. Подробности узнавайте в отделе продаж.</small></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--one item END-->
                                <br>
                                <!--one item-->
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="( my-collapse-btn + design-main ) collapsed for-bank" type="button" data-toggle="collapse" data-target="#take06" aria-expanded="false" aria-controls="take06">
                                                    <div class="my-table">
                                                        <div id="step6">
                                                            <strong><span class="step">Этап 6: </span>Действие гарантии до 25 лет</strong>
                                                        </div>
                                                        <div class="my-appearance-middle">
                                                            <i class="glyphicon glyphicon-menu-up"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="collapse my-collapse" id="take06">
                                                    <div class="( my-collapse-content + design-main )">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <p style="text-align: center;"><img class="img-responsive" src="6_stage_246x226.jpg" style="padding-right:15px; margin-bottom:25px;"></p>
                                                            </div>
                                                            <div class="col-md-9 col-sm-12 col-xs-12">
                                                                <p>Как только дом построен и принят клиентом, на него начинает распространяться расширенная гарантия от компании «Теремъ».</p>
                                                                <ul>
                                                                    <li>
                                                                        <p>Срок гарантии на каркасные дома составляет до 15 лет, на дома из клеёного бруса ‒ до 25 лет, на дома, построенные из кирпича, – до 20 лет.</p>
                                                                    </li>
                                                                    <li>
                                                                        <p>В случае обнаружения недостатков заказчик может обратиться в отдел клиентских коммуникаций компании «Теремъ» по телефону +7 (495) 461-01-10. При подтверждении наличия дефектов компания устраняет их бесплатно.</p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--one item END-->
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="scroll.js"></script>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>