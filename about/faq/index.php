<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "faq, часто задоваемые вопросы");
$APPLICATION->SetPageProperty("title", "Часто задаваемые вопросы");
$APPLICATION->SetTitle("Хотелось бы остановить выбор на компании ТеремЪ - Задайте вопрос.");
?>

<section class="content text-page white" role="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class="white padding-side clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <div class="text-center text-uppercase">
                                <h1>
                                    Часто задаваемые вопросы
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">
                            <p>
                                На этой странице Вы можете найти ответы на часто задаваемые нашими клиентами вопросы. Уточнить информацию можно у наших менеджеров на выставочной площадке и по телефону.
                            </p>
                            <br>
                            <br>
                        </div>
                    </div>
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq1" aria-expanded="false" aria-controls="faq1">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Вы занимаетесь строительством по всей России или только в Москве?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq1">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="grey text-uppercase faq-title">Ответ:</h3>
                                            <p>Мы строим дома в радиусе 600 км от г. Бронницы Московской области. Отгрузка выполняется с производственной базы, расположенной в г. Бронницы Московской области.</p>
                                            <p>Бесплатная доставка производится:</p>
                                            <ul>
                                                <li>
                                                    <p>Домов всех серии (кроме одноэтажных домов и бань) - в радиусе 100 км от базы</p>
                                                </li>
                                                <li>
                                                    <p>Электрики – в радиусе 100 км от базы</p>
                                                </li>
                                                <li>
                                                    <p>Садовых домов серии «Малыш», а также бань размерами до 6х6 м включительно – в радиусе 250 км от базы. </p>
                                                </li>
                                            </ul>
                                            <p>Каждый следующий километр оплачивается заказчиком согласно тарифу. С тарифами доставки по каждому дому Вы можете ознакомиться на нашей выставочной площадке по адресу: метро Кузьминки, ул. Зеленодольская, вл. 42.</p>
                                            <p>«Теремъ» расширяет свою партнерскую сеть. В скором времени дома будут доступны для заказа в регионах России и других странах. Следите за новостями компании, а также за списком официальных партнеров <a href="/contacts/">здесь</a>.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->

                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq2" aria-expanded="false" aria-controls="faq2">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Входит ли в стоимость земельный участок?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq2">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Стоимость земельного участка не входит в стоимость дома. Если у вас пока ещё нет земли, то вы можете приобрести её в нашем проекте «Земля». Мы предлагаем участки более чем в 100 посёлках по всей Московской и близлежащих областях.  Если предполагается строительство на уже существующем участке, то при заключении договора потребуется документ, подтверждающий право собственности на участок и общегражданский паспорт.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq3" aria-expanded="false" aria-controls="faq3">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Занимаетесь ли вы достройкой и реконструкцией существующего строения? И как можно получить расчет?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq3">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Добрый день. Компания «Теремъ» занимается достройкой и реконструкцией. Расчет можно получить в Отделе достройки и реконструкции на нашей выставочной площадке по адресу: метро Кузьминки, ул. Зеленодольская, вл. 42. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq4" aria-expanded="false" aria-controls="faq4">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли использовать материнский капитал при строительстве дома в компании «Теремъ»?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq4">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да! Часть стоимости дома вы можете оплатить при помощи материнского капитала. При этом его можно использовать в качестве средств для досрочного погашения кредита и в качестве части суммы при наличном расчёте. </p>
                                            <ul><p>Схема при наличном расчёте:</p>
                                            <li><p>Выбрать дом</p></li>
                                            <li><p>Заключить договор </p></li>
                                            <li><p>Подать документы в ПФР </p></li>
                                            <li><p>После перечисления средств материнского капитала на счёт компании «Теремъ» оплатить оставшуюся сумму по договору</p></li>
                                            <li><p>Начать строительство </p></li>
                                            </ul>                                            
                                            <ul><p>Схема при кредитовании:</p>
                                            <li><p>Выбрать дом</p></li>
                                            <li><p>Заключить договор </p></li>
                                            <li><p>Получить кредит в банке – партнёре компании «Теремъ» </p></li>
                                            <li><p>Начать строительство </p></li>
                                            <li><p>Подать документы в ПФР </p></li>
                                            </ul>
                                            <p>За подробностями и дополнительной информацией обращайтесь к специалистам кредитно-страхового отдела. Сделать это можно по телефону или на территории выставочного комплекса в Кузьминках ежедневно с 10:00 до 20:00.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq5" aria-expanded="false" aria-controls="faq5">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли заказать дом по индивидуальному проекту?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq5">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Добрый день. Квалифицированные архитекторы разработают проект с учетом всех Ваших пожеланий. Есть возможность привязки проекта к существующему фундаменту. Дополнительную информацию Вы можете получить в Отделе индивидуального строительства.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq51" aria-expanded="false" aria-controls="faq51">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>У меня есть фундамент, можно ли построить на нем дом по собственному проекту?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq51">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Добрый день. Квалифицированные архитекторы разработают проект с учетом всех Ваших пожеланий. Есть возможность привязки проекта к существующему фундаменту. Дополнительную информацию Вы можете получить в Отделе индивидуального строительства.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq6" aria-expanded="false" aria-controls="faq6">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Возможен ли самостоятельный вывоз комплекта дома, материалов, и/или самостоятельная сборка?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq6">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Нет, это невозможно. Мы строительная компания, которая имеет свое производство и строительные бригады. Дома мы строим сами, с использованием современных строительных технологий и качественных материалов. Мы даем гарантию на построенный дом, собранный по собственной технологии и специально обученными специалистами.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq7" aria-expanded="false" aria-controls="faq7">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Компания «Теремъ» занимается инженерными коммуникациями? Входят ли в стоимость дома эти работы?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq7">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>К каждому из проектов домов компании в Отделе инженерных коммуникаций есть план по электропроводке, установке отопительной системы и других инженерных коммуникаций.
                                                На выставочной площадке  (метро Кузьминки, ул. Зеленодольская, вл. 42) можно заключить отдельные договоры на электромонтажные работы, установку отопления, водоснабжения, канализации и т.д.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq8" aria-expanded="false" aria-controls="faq8">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Какие в компании условия оплаты? Есть ли предоплата?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq8">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Для жителей Москвы и Московской области предоплаты у нас нет.</p>
                                            <p>Для клиентов c пропиской за пределами Москвы и Московской области есть предоплата.</p>
                                            <p>Подробную информацию можно получить на нашей выставочной площадке по адресу: метро Кузьминки, ул. Зеленодольская, вл. 42 или по тел.: 8 (495) 461-01-10. </p>
                                            <p>Оплата осуществляется по факту выполнения работ в любом отделении Сбербанка г. Москвы (за исключением территории «Новой Москвы») – без комиссии и процентов в течение 2-х банковских дней после подписания акта приемки. На коттеджи оплата производится в 3 этапа (в случае, если это не оговорено дополнительно условиями проводимых акций):</p>
                                            <ul>
                                                <li>
                                                    <p>после строительства фундамента – 40%,</p>
                                                </li>
                                                <li>
                                                    <p>после строительства крыши – 40%,</p>
                                                </li>
                                                <li>
                                                    <p>после подписания акта о приемке – оставшиеся 20%.</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq9" aria-expanded="false" aria-controls="faq9">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Есть ли возможность купить дом в кредит?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq9">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Есть. Мы работаем с ведущими банками, и заявку на получение кредита на самых выгодных условиях можно оформить прямо на территории выставочного комплекса в Кузьминках. Специалисты кредитно-страхового отдела «Теремъ-Финанс» помогут вам  получить  кредит и при желании застраховать ваш дом.Более подробную информацию Вы можете получить в разделе «Услуги кредитование» на нашем сайте.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq10" aria-expanded="false" aria-controls="faq10">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>
                                            <div>
                                                <p>Выезжают ли ваши специалисты на участок для того, чтобы определить условия застройки и пожелания клиента на месте?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq10">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да, при необходимости выезд специалиста возможен, но только при условии заключения клиентом договора на строительство. Эта услуга платная. Подробнее Вы можете узнать на выставочной площадке компании по адресу: м. Кузьминки, ул. Зеленодольская, вл. 42 или по тел.: (495) 461-01-10. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq11" aria-expanded="false" aria-controls="faq11">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Можно ли в домах компании «Теремъ» проживать круглогодично?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq11">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Да, в домах для постоянного проживания тысячи наших заказчиков живут круглогодично. Эти дома предназначены для строительства на дачных участках и на участках для индивидуального жилищного строительства. Комплектация домов предусматривает его круглогодичное использование с использованием стационарных отопительных агрегатов (АОГВ, калориферов, печей и пр.).</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq12" aria-expanded="false" aria-controls="faq12">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Какие установлены сроки строительства домов?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq12">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Минимальный срок – несколько дней, а максимальный - почти 4 месяца. Разные дома имеют различные сроки строительства. Подробнее Вы можете узнать на нашем сайте в разделе «Каталог домов» и по телефону: 8 (495) 461-01-10. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq13" aria-expanded="false" aria-controls="faq13">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Что такое каркасная технология строительства домов?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq13">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Стены каркасного дома выполняются из доски 35х150 мм. Доска устанавливается с шагом 500 (400 мм для домов для постоянного проживания) мм. Между стойками укладывается утеплитель 150 мм (200 мм для домов для постоянного проживания). С двух сторон стены обшиваются вагонкой (дома для постоянного проживания с наружной стороны обшиваются сайдингом), с наружной стороны - по изоспану А, с внутренней стороны - по изоспану Б. Весь конструктив выполняется из сухого материала.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq14" aria-expanded="false" aria-controls="faq14">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Что входит в базовую комплектацию дома?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq14">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>В базовую комплектацию дома (бани) входит:</p>
                                            <ul>
                                                <li>
                                                    <p>Фундамент;</p>
                                                </li>
                                                <li>
                                                    <p>Весь материал для сборки дома;</p>
                                                </li>
                                                <li>
                                                    <p>Окна, двери, кровля, материал для внутренней отделки;</p>
                                                </li>
                                                <li>
                                                    <p>Доставка;</p>
                                                </li>
                                                <li>
                                                    <p>Работы по установке и сборке дома.</p>
                                                </li>
                                            </ul>
                                            <p>Узнать о каждом конкретном доме более подробно можно в разделе «Каталог домов».</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq15" aria-expanded="false" aria-controls="faq15">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Какую гарантию компания «Теремъ» дает на дома?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq15">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Мы даем гарантию до 25 лет.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->
                    <!--one item-->
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 + my-margin">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="( my-collapse-btn + design-main ) collapsed" type="button" data-toggle="collapse" data-target="#faq16" aria-expanded="false" aria-controls="faq16">
                                        <div class="my-table third-col">
                                            <div>
                                                <p class="icon">?</p>
                                            </div>

                                            <div>
                                                <p>Где находится выставочная площадка компании «Теремъ»? Как к вам добраться?</p>
                                            </div>
                                            <div>
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <div class="collapse my-collapse" id="faq16">
                                        <div class="( my-collapse-content + design-main )">
                                            <h3 class="text-uppercase grey faq-title">Ответ:</h3>
                                            <p>Наш выставочный комплекс находится по адресу: г. Москва, метро Кузьминки, ул. Зеленодольская, владение 42. Добраться до нас можно как на метро, так и на автомобиле. Подробное описание проезда находится в разделе «Контакты» на нашем сайте.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--one item END-->

                </div>
            </div>
        </div>

    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
