<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Онлайн трансляция строительства дом «Лидер 9» ВС К-200");
$APPLICATION->SetPageProperty("keywords", "строительство дома");
$APPLICATION->SetPageProperty("title", "Онлайн трансляция строительства бани «Тайга»");
$APPLICATION->SetTitle("");
?>
<style>
.text-item h2 {
  font-size: 20px;
  color:#020202;
  margin: 0;
  font-weight: bold;
}
.text-item grey {
  color: #8f8f8f;
}
.text-item grey p {
  color: #8f8f8f;
  line-height:1 !important;
  margin-top: 34px;
}
.text-item {
  margin-top: 10px;
  margin-bottom: 15px;
}
.text-item small {
  font-size: 12px;
}
.text-item.active h2,
.text-item.active p,
.text-item.active grey
{color: #ff0000;}
.text-item p {
  font-size: 14px;
  color:#020202;
  margin: 0;
}
</style>
<section class="content white catalog-item-1 content-catalog-item active" data-item="1">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="catalog-item-gallery white + my-margin">
          <div role="tabpanel" class="tab-panel ( my-tabpanel &amp;&amp; ( design-1 &amp;&amp; appearance-list-left &amp;&amp; bg-grey &amp;&amp; padding-20 ))">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">«Маяковский 5»</a></li>
			  <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">«Тайга 4» и «Егерь 5»</a></li>
             <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">«Премьер 1»</a></li>
              <!-- <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Камера 3</a></li> -->
            </ul><!-- Nav tabs END-->
            <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="tab2">
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-12 col-sm-12">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase">
                          СТРОИТЕЛЬСТВО ДОМА «МАЯКОВСКИЙ 5» В РЕАЛЬНОМ ВРЕМЕНИ
                          </span>
                        </h3>
                        <br>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">На этом видео вы можете видеть строительство нового дома «Маяковский 5» на территории выставочного комплекса компании «Теремъ». Этот проект — новинка нашего каталога, которую отличает современный дизайн и функциональность.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Размер дома — 8x11 м, общая площадь — более <b>145 кв м.</b></p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Вы можете наблюдать за всеми этапами строительства: от фундамента до внешней отделки. Дом возводится по каркасной технологии, он будет двухэтажным с пристроенными террасами. </p>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                   <iframe width="100%" height="554" src="https://ru.cloud.trassir.com/embed/VQZXAaFYnJKWE7KK?lang=en" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <br>
                  <br>
              </div>
               <div role="tabpanel" class="tab-pane" id="tab3">
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-12 col-sm-12">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase">
                          СТРОИТЕЛЬСТВО БАНЬ «ЕГЕРЬ 5» И «ТАЙГА 4» В РЕАЛЬНОМ ВРЕМЕНИ
                          </span>
                        </h3>
                        <br>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">На этом видео прямой эфир строительства сразу двух новых бань: «Тайга4» и «Егерь 5». Обе бани строятся на территории выставочного комплекса компании «Теремъ» в Кузьминках. Эти проекты — новинки прошедшего лета, которые уже завоевали популярность у наших клиентов.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Баня «Егерь 5» строится по каркасной технологии. На видео она находится с левой стороны. Размер бани — 7х7 м, общая площадь 59.7 кв. м. Это проект с мансардным этажом и вторым светом. </p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Баня «Тайга 4», строится из клееного бруса, она правее «Тайги».  Размер бани — 6x9.5 м, общая площадь — 59.5 кв м.</p>
						<br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                   <iframe width="100%" height="554" src="https://ru.cloud.trassir.com/embed/yTypSyGg2EadlnUn?lang=en" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <br>
                  <br>
              </div>
               <div role="tabpanel" class="tab-pane" id="tab4">
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-12 col-sm-12">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase">
                          СТРОИТЕЛЬСТВО ДОМА «ПРЕМЬЕР 1» В РЕАЛЬНОМ ВРЕМЕНИ 
                          </span>
                        </h3>
                        <br>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">На этом видео прямой эфир строительства двухэтажного коттеджа «Премьер 1» на территории выставочного комплекса компании «Теремъ».</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Размер дома — 7x7 м. Дом возводится из клееного бруса.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Вы можете наблюдать за всеми этапами строительства: от фундамента до внешней отделки. </p>
						<br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                   <iframe width="100%" height="554" src="https://ru.cloud.trassir.com/embed/Kua9PEoYapBmwCnx?lang=en" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <br>
                  <br>
              </div>
              <!-- <div role="tabpanel" class="tab-pane" id="tab3">
                <div class="row">
                  <div class="col-xs-12 col-md-9 col-sm-9">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-10 col-sm-10">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase"><span style="text-transform: lowercase;">дом</span> «Боярин» 4 ВС К-200, 10,5<span style="text-transform: lowercase;">х</span>12,5 <span style="text-transform: lowercase;">м</span></span>. Общая площадь: 198,53 кв.м
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <iframe width="100%" height="554" frameborder="0" seamless="seamless" src="http://ipeye.ru/ipeye_service/api/api.php?dev=qtZzTvWUM9XdHFgfpLGf2QjmOJblG7&tupe=rtmp&autoplay=0&logo=1">Ваш браузер не поддерживает фреймы!</iframe>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>







<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
