<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Онлайн трансляция строительства дом «Лидер 9» ВС К-200");
$APPLICATION->SetPageProperty("keywords", "строительство дома");
$APPLICATION->SetPageProperty("title", "Онлайн трансляция строительства бани «Тайга»");
$APPLICATION->SetTitle("");
?>
<style>
.text-item h2 {
  font-size: 20px;
  color:#020202;
  margin: 0;
  font-weight: bold;
}
.text-item grey {
  color: #8f8f8f;
}
.text-item grey p {
  color: #8f8f8f;
  line-height:1 !important;
  margin-top: 34px;
}
.text-item {
  margin-top: 10px;
  margin-bottom: 15px;
}
.text-item small {
  font-size: 12px;
}
.text-item.active h2,
.text-item.active p,
.text-item.active grey
{color: #ff0000;}
.text-item p {
  font-size: 14px;
  color:#020202;
  margin: 0;
}
</style>
<section class="content white catalog-item-1 content-catalog-item active" data-item="1">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12 col-sm-12">
        <div class="catalog-item-gallery white + my-margin">
          <div role="tabpanel" class="tab-panel ( my-tabpanel &amp;&amp; ( design-1 &amp;&amp; appearance-list-left &amp;&amp; bg-grey &amp;&amp; padding-20 ))">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">«Маяковский 5»</a></li>
			  <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">«Бордо 2»</a></li>
             <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">«Лада 1»</a></li>
             <li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">«Маршал 1» и «Премьер 5»</a></li>
              <!-- <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Камера 3</a></li> -->
            </ul><!-- Nav tabs END-->
            <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="tab2">
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-12 col-sm-12">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase">
                          СТРОИТЕЛЬСТВО ДОМА «МАЯКОВСКИЙ 5» В РЕАЛЬНОМ ВРЕМЕНИ
                          </span>
                        </h3>
                        <br>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">На этом видео вы можете видеть строительство нового дома «Маяковский 5» на территории выставочного комплекса компании «Теремъ». Этот проект — новинка нашего каталога, которую отличает современный дизайн и функциональность.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Размер дома — 8x11 м, общая площадь — более <b>145 кв м.</b></p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Вы можете наблюдать за всеми этапами строительства: от фундамента до внешней отделки. Дом возводится по каркасной технологии, он будет двухэтажным с пристроенными террасами. </p>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                   <iframe width="100%" height="554" src="https://ru.cloud.trassir.com/embed/VQZXAaFYnJKWE7KK?lang=en" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <br>
                  <br>
              </div>
               <div role="tabpanel" class="tab-pane" id="tab3">
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-12 col-sm-12">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase">
                          СТРОИТЕЛЬСТВО БАНЬ «БОРДО 2» В РЕАЛЬНОМ ВРЕМЕНИ
                          </span>
                        </h3>
                        <br>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">На этом видео прямой эфир строительства абсолютной новинки – дома «Бордо 2» на территории выставочного комплекса компании «Теремъ» в Кузьминках.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Дом строится по каркасной технологии. </p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Размер дома – 8х8 м, общая площадь – 132.99 кв. м.</p>
						<br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                   <iframe width="100%" height="554" src="https://ru.cloud.trassir.com/embed/yTypSyGg2EadlnUn?lang=en" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <br>
                  <br>
              </div>
               <div role="tabpanel" class="tab-pane" id="tab4">
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-12 col-sm-12">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase">
                          СТРОИТЕЛЬСТВО БАНИ «ЛАДА 1» В РЕАЛЬНОМ ВРЕМЕНИ
                          </span>
                        </h3>
                        <br>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">На этом видео прямой эфир строительства обновленной бани «Лада 1» на территории выставочного комплекса компании «Теремъ» в Кузьминках.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Баня строится по смешанной технологии, когда первый этаж возводится из бруса, а мансардный – по каркасной технологии. Обновленный проект включает большую парную (7, 52 кв. м) и удобную душевую.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Размер бани – 6х8 м, общая площадь – 57.84 кв. м. </p>
						<br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                   <iframe width="100%" height="554" src="https://ru.cloud.trassir.com/embed/Kua9PEoYapBmwCnx?lang=en" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <br>
                  <br>
              </div>
              <div role="tabpanel" class="tab-pane" id="tab5">
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-12 col-sm-12">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase">
                          СТРОИТЕЛЬСТВО ДОМОВ «МАРШАЛ 1» И «ПРЕМЬЕР 5» В РЕАЛЬНОМ ВРЕМЕНИ
                          </span>
                        </h3>
                        <br>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">На этом видео прямой эфир строительства сразу двух обновленных проектов из нашего каталога: «Маршал 1» и «Премьер 5».<br>Двухэтажный загородный коттедж «Маршал 1» строится по каркасной технологии. На видео он находится с правой стороны.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Размер дома — 8х11 м., общая площадь - 160,23 кв. м.</p>
                        <p style="font-family: ProximaNova-Regular, sans-serif; font-size: 16px; padding-left:5px;">Двухэтажный дом «Премьер 5» на видео находится левее, он возводится по каркасной технологии, его размер — 8x10 м, общая площадь - 153,92 кв. м.</p>
						<br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                   <iframe width="1024" height="768" src="https://ru.cloud.trassir.com/tube/lmhFAKdudf42sNNu?lang=en" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <br>
                  <br>
              </div>
              <!-- <div role="tabpanel" class="tab-pane" id="tab3">
                <div class="row">
                  <div class="col-xs-12 col-md-9 col-sm-9">
                    <div class="row" style="padding-top: 14px;">
                      <div class="col-xs-12 col-md-10 col-sm-10">
                        <h3 style="padding-left: 6px;margin-right: -6px;">
                          <span class="text-uppercase"><span style="text-transform: lowercase;">дом</span> «Боярин» 4 ВС К-200, 10,5<span style="text-transform: lowercase;">х</span>12,5 <span style="text-transform: lowercase;">м</span></span>. Общая площадь: 198,53 кв.м
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <iframe width="100%" height="554" frameborder="0" seamless="seamless" src="http://ipeye.ru/ipeye_service/api/api.php?dev=qtZzTvWUM9XdHFgfpLGf2QjmOJblG7&tupe=rtmp&autoplay=0&logo=1">Ваш браузер не поддерживает фреймы!</iframe>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>







<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
