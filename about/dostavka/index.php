<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Теремъ предлагает большой ассортимент деревянных домов, бань, коттеджей");
$APPLICATION->SetPageProperty("keywords", "доставка терем");
$APPLICATION->SetPageProperty("title", "Доставка ");
$APPLICATION->SetTitle("Доставка ");
?>

    <section class="content text-page white" role="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="white padding-side clearfix">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <div class="my-text-title text-center text-uppercase desgin-h1">
                                    <h1>
                                        Доставка дома
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <img src="/upload/dostavka/dostavka.jpg" class="img-responsive">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12">
                                <p>Компания «Теремъ» имеет свое современное и хорошо оборудованное производство, где формируется полный комплект будущего дома. После чего комплект доставляется на участок заказчика. Строительство домов осуществляется в пределах 600 км от г. Бронницы Московской области.</p>
                                <p>Мы предлагаем возможность бесплатной доставки домов, электрики и инженерных коммуникаций в радиусе 100 км от г. Бронницы.<br>Садовые дома и бани доставляются бесплатно в радиусе 250 км.</p>
                                <br>
                                <p>Подробнее об условиях доставки можно узнать на территории выставочного комплекса или по телефону.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button style="background-color:transparent; border:0px; margin-top:25px;" data-target="#call" data-toggle="modal"><img src="/upload/dostavka/dostavka_knopka.png" class="img-responsive"></button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>